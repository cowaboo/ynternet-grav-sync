---
title: 'FLOSS ressources pour l''emploi'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: fr
objectives:
    - 'Fournir un contexte aux compétences FLOSS pour l''emploi'
    - 'Fournir un contexte aux méthodes des ressources humaines'
    - 'Reconnaître les avantages et les contraintes des algorithmes en RH'
    - 'Comprendre les algorithmes ouverts comme un nouveau paradigme'
    - 'Comprendre l''utilisation d''algorithmes dans le processus de recrutement'
    - 'Comprendre comment adapter un CV pour avoir une meilleure acceptation des algorithmes'
    - 'Apprenez à créer un bon CV en ligne, un portfolio ou un profil de médias sociaux'
    - 'Apprenez à créer et à mettre à jour un bon profil de travail en ligne'
    - 'Comprendre à quel point il est important de faire partie d''une communauté en ligne afin de saisir de nouvelles opportunités économiques'
    - 'Comprendre comment le marché évolue et comment les opportunités en ligne peuvent faciliter l''esprit d''entreprise et l''image de marque personnelle'
    - 'Apprenez de nouvelles façons d''être entrepreneur avec les pratiques et les outils FLOSS'
    - 'Savoir mener une campagne de crowdfunding en ligne'
    - 'Comprendre différents modèles économiques comme la culture du don et la wikinomique, entre autres'
    - 'Savoir améliorer ses compétences personnelles en communication, collaboration et image de marque'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté à Internet)'
    - 'Connexion Internet'
    - Beamer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
    - Haut-parleurs
skills:
    'Mots clés':
        subs:
            'Algorithmes et employabilité': null
            'Curriculum Vitae': null
            Recrutement: null
            Compétences: null
            'Opportunités en ligne': null
            'La mise en réseau': null
            'Entrepreneuriat en ligne': null
            'Culture du don': null
            Wikinomics: null
            'Campagnes en ligne': null
            'Collecte de fonds': null
            Crowdsourcing: null
            'Collaboration de masse': null
length: '15 heures'
---

## Introduction
75% des candidatures sont rejetées avant d'être vues par les yeux humains. Avant que votre curriculum vitae ne parvienne aux mains d'une personne vivante, il doit souvent passer par un système de suivi des candidats. Un système de suivi des candidats - ou ATS, pour faire court - est un type de logiciel utilisé par les recruteurs et les employeurs pendant le processus d'embauche pour collecter, trier, numériser et classer les candidatures qu'ils reçoivent pour leurs postes vacants.

Choisir la bonne personne pour un poste peut être difficile. Les algorithmes sont, en partie, nos opinions intégrées dans une forme de code informatique. Les algorithmes considèrent le simple fait que l'embauche est essentiellement un problème de prédiction. Lorsqu'un gestionnaire lit le curriculum vitae des candidats, il essaie de prédire quels candidats réussiront dans le travail et lesquels ne le feront pas. Les algorithmes statistiques sont construits pour la prévision des problèmes / coûts et peuvent être utiles pour améliorer la prise de décision humaine. Les algorithmes peuvent également avoir un côté plus sombre car ils reflètent les préjugés et les préjugés humains qui conduisent à des erreurs d'apprentissage automatique et à des interprétations erronées (perpétuant et renforçant la discrimination dans les pratiques d'embauche). La question n'est plus maintenant de savoir s'il faut utiliser des algorithmes pour sélectionner les candidats, mais comment tirer le meilleur parti des machines.

Les principes fondamentaux de la rédaction de CV restent constants pendant des générations, mais l'évolution des technologies signifie que plus d'aspects des processus de candidature et d'embauche ont lieu en ligne que jamais auparavant. En restant à jour avec les meilleures pratiques actuelles, les participants seront mieux préparés à postuler aux offres d'emploi.
Sur Internet, les possibilités de génération de revenus sont infinies. Nous devons identifier une niche, développer un plan d'affaires et travailler dur pour réussir. Il existe de nombreuses entreprises qu'un entrepreneur Internet peut développer, le modèle d'entreprise ne définit pas un entrepreneur Internet.

Chaque organisation et projet a besoin de sources de revenus pour mettre en œuvre des activités, que ces activités soient en face à face ou en ligne. Dans ce module, nous nous concentrerons également sur la culture du don, la wikinomique, la collecte de fonds et d'autres modèles économiques au cœur du web 2.0
## Contexte
Le but de la session est de démontrer et d'engager les apprenants sur la façon dont:
* Fournir un contexte aux compétences FLOSS pour l'emploi.
* Fournir un contexte aux méthodes des ressources humaines.
* Reconnaître les avantages et les contraintes des algorithmes en RH.
* Comprendre les algorithmes ouverts comme un nouveau paradigme.
* Comprendre l'utilisation d'algorithmes dans le processus de recrutement.
* Comprendre comment adapter un CV pour avoir une meilleure acceptation des algorithmes.
* Apprenez à créer un bon CV en ligne, un portfolio ou un profil de médias sociaux.
* Apprenez à créer et à mettre à jour un bon profil de travail en ligne.
* Comprendre à quel point il est important de faire partie d'une communauté en ligne afin de saisir de nouvelles opportunités économiques.
* Comprendre comment le marché évolue et comment les opportunités en ligne peuvent faciliter l'esprit d'entreprise et l'image de marque personnelle.
* Apprenez de nouvelles façons d'être entrepreneur avec les pratiques et les outils FLOSS.
* Savoir mener une campagne de crowdfunding en ligne.
* Comprendre différents modèles économiques comme la culture du don et la wikinomique, entre autres.
* Savoir améliorer ses compétences personnelles en communication, collaboration et image de marque.

## Séances
### Première séance : Algorithmes et employabilité
L'objectif de la séance est de présenter les concepts d'algorithmes et d'employabilité et comment s'adapter à un nouveau monde RH où en plus d'avoir à impressionner un recruteur d'emploi, les demandeurs d'emploi doivent également être attractifs pour les machines qu'ils utilisent . Cette session va générer des discussions et des débats sur le problème posé par les algorithmes RH et aussi sur les solutions qu'ils apportent. L'objectif principal de cette session est d'ouvrir les participants à l'idée que les algorithmes de recrutement continueront de croître et que nous devons mieux nous y adapter pour réussir.
### Deuxième séance : Comment utiliser les pratiques FLOSS et créer un CV attractif pour les algorithmes
Cette séance se concentrera sur la façon de créer un CV en ligne. Les participants seront invités à créer leur propre CV à l'aide de plateformes et d'outils numériques. De plus, les participants découvriront comment présenter leurs compétences et aptitudes lors de la création d'un bon CV en ligne qui plaira aux humains et aux machines.
### Troisième séance : Opportunités FLOSS et entrepreneuriat en ligne
Dans cette séance, les participants auront la chance de découvrir de nombreuses autres opportunités créées par le monde numérique pour développer une carrière et des idées de projets. Cette session se concentrera principalement sur l'entrepreneuriat en ligne et sur la manière dont le monde numérique crée des opportunités de collecte de fonds. Différents modèles économiques seront explorés comme le crowdfunding, la culture du don et le modèle Freemium.