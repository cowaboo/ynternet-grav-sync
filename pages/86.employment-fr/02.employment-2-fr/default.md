---
title: 'Comment utiliser les pratiques FLOSS et créer un CV attrayant pour les algorithmes'
objectives:
    - 'Fournir une introduction à la façon dont les participants peuvent s''adapter aux nouvelles façons d''embaucher et de postuler à des emplois en ligne'
    - 'Faciliter la transition vers de nouvelles façons de présenter les aptitudes et compétences professionnelles que les algorithmes peuvent reconnaître'
    - 'Découvrir comment créer et mettre à jour un bon profil de travail / profil linkedin en ligne ainsi qu''un bon cv en ligne attrayant pour les algorithmes'
    - 'Motiver les participants à créer de nouveaux CV plus attractifs pour les algorithmes'
---

## Introduction
Discussion de groupe sur les principales difficultés rencontrées par les participants lors de leur dernière candidature à un poste en ligne.
La session débutera par une discussion de groupe sur les difficultés et problèmes rencontrés par les participants lors de leur dernière utilisation et de la plateforme RH ou lors de leur dernière candidature en ligne. L'objectif de cet objectif est de répertorier les difficultés les plus courantes rencontrées par les personnes qui recherchent et postulent pour un travail sur Internet. Il est important d'aider les participants à comprendre que les plateformes ont des limites mais qu'elles offrent également des opportunités.
## Concevoir et planifier un bon CV en ligne
Le formateur commencera cette session en discutant avec les participants quelles sont les plateformes et les outils les plus utilisés pour rechercher des emplois. La discussion vise à réfléchir aux outils et aux tendances les plus populaires que les participants ont utilisés auparavant.
Après cela, le formateur, ainsi que par le biais de discussions de groupe, énumérera les principaux conseils et astuces pour créer un bon CV. Le formateur aidera ensuite les participants à comprendre les différences entre la rédaction d'un CV qui sera lu par un humain et un CV qui sera sélectionné par une machine. Le but de cette session est d'aider les participants à découvrir les conseils et suggestions pour créer un CV qui fait appel aux algorithmes RH.
## Utilisez des mots-clés pour décrire leurs aptitudes et compétences
Après avoir nommé les astuces et conseils les plus importants et les plus populaires pour créer un CV attrayant pour les algorithmes, le formateur guidera les participants à comprendre comment présenter leurs compétences, compétences et expérience de travail lors de la création d'un CV. Pour créer un CV attractif pour les algorithmes, il est très important d'utiliser les bons mots-clés et de présenter au mieux le candidat. Au cours de cette session, les participants apprendront à présenter et à écrire leur expérience de travail d'une manière que l'homme et la machine reconnaissent. Les algorithmes d'embauche exigent des demandeurs d'emploi un réajustement des pratiques et des perspectives afin de s'adapter au marché du travail aujourd'hui.
## Créer un CV en ligne
Après la discussion de remue-méninges, le formateur invitera les participants à choisir une plateforme en ligne et à créer leur CV en ligne en utilisant ce qu'ils ont appris dans les exercices précédents. Le formateur soutiendra les participants en les aidant à surmonter les difficultés et en les aidant à adapter leur CV à leurs besoins. On ne s'attend pas à ce que les participants aient la possibilité de terminer leur CV pendant le cours, mais qu'ils créent une structure osseuse solide pour le document qu'ils peuvent terminer à la maison et quand cela leur convient mieux.
## Conclusion
Pour conclure la session, le formateur demandera aux participants quelles sont les contributions qu'ils retirent de l'expérience et quelles ont été les principales difficultés rencontrées lors de la construction du CV en ligne. C’est un moment pour les participants de partager leurs idées et leurs questions, mais aussi pour apprendre des expériences des uns et des autres.
## Devoirs
Les participants doivent lire l'article et regarder la vidéo ci-dessous et les partager sur n'importe quelle plate-forme de médias sociaux qu'ils utilisent.
* [Battez les robots: comment faire passer votre CV au-delà du système et entre les mains de l'homme](https://www.themuse.com/advice/beat-the-robots-how-to-get-your-resume-past-the-system-into-human-hands).
* [Comment optimiser votre CV pour les algorithmes](https://social.hays.com/2018/01/04/optimise-cv-algorithms/).

## Références
* [Plateforme de recherche d'emploi en ligne](https://www.indeed.ch/).
* [Comment faire passer votre CV au-delà d'un algorithme | WorkingNation](https://www.youtube.com/watch?v=PVK7tG1LRP8).
* [Comment obtenir votre CV Logiciel de dépistage de CV passé (2019)](https://www.youtube.com/watch?v=gxsI-tgM-ZE).