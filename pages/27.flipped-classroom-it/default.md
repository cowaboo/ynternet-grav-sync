---
title: 'Insegnamento capovolto e Apprendimento basato su progetto (PBL)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: it
length: '9 ore'
objectives:
    - 'Comprendere il quadro teorico e i fattori chiave dei 3 metodi/strategie: Insegnamento capovolto, Apprendimento Problem Based e apprendimento basato su progetto'
    - 'L''attenzione sarà focalizzata sull''apprendimento di come progettare o ridisegnare la strategia di insegnamento-apprendimento utilizzando risorse e strumenti aperti'
    - 'creare e valutare le attività basate su questi metodi'
materials:
    - 'Computer Personale computer (Smartphone o tablet connesso ad Internet)'
    - 'Connessione Internet'
    - Proiettore
    - 'Carta e penna'
    - 'Tableau à feuilles'
    - Altoparlanti
skills:
    'Parole chiave':
        subs:
            'Insegnamento capovolto (FC)': null
            'Apprendimento Problem based (PBL)': null
            'Apprendimento basato su progetto (Project BL)': null
            Aperto: null
            Metodi: null
            Continuo: null
            Volontario: null
            Automotivato: null
---

## Introduzione
Insegnamento capovolto, apprendimento basato su progetto, apprendimento problem based... metodi, tecniche o metodologie diverse che hanno qualcosa in comune: la posizione in cui si collocano gli studenti è proattiva, nella gestione del loro potere nel bisogno di apprendere. Se gli studenti lavorano in gruppo, come negli scenari della vita reale, l'apprendimento diventa più significativo. Con queste strategie il protagonismo si colloca nelle persone che hanno voglia di imparare e il ruolo dell'insegnante/formatore è quello di un compagno che ha determinate conoscenze ma che vuole anche imparare con gli studenti. 
In breve, l'**insegnamento capovolto** (Flipped Classroom) è un modello pedagogico che sposta alcuni processi di apprendimento al di fuori della classe e investe il tempo della lezione in classe, insieme all'esperienza dell'insegnante, per promuovere e incentivare altri processi di acquisizione e di pratica della conoscenza all'interno della classe.
Con l'**apprendimento problem based** (PBL), l'insegnante pone una domanda o un problema e gli studenti devono dare una risposta o trovare un modo per risolverlo attraverso un processo; loro, l'insegnante e il gruppo di studenti, stabiliscono tutti insieme una tappa fondamentale nel processo di apprendimento. 
Nell'apprendimento basato su progetto, gli studenti sono anche al centro del processo di apprendimento, come protagonisti in grado di generare soluzioni in risposta alle diverse esigenze; il processo è strettamente legato all'ambiente di lavoro e il risultato principale è un prodotto.
Il modulo si concentrerà su queste strategie, mostrando le idee più rilevanti e i fattori chiave e sfiderà i partecipanti a ripensare le loro pratiche pedagogiche.
## Contesto
L'obiettivo di questi moduli è di comprendere il quadro teorico e i fattori chiave dei 3 metodi/strategie: Insegnamento capovolto, Apprendimento Problem Based e apprendimento basato su progetto.
L'attenzione sarà focalizzata sull'apprendimento di come progettare o ridisegnare la strategia di insegnamento-apprendimento utilizzando risorse e strumenti aperti e creare e valutare le attività basate su questi metodi. 

Risultati didattici:
* Comprendere le basi della strategia Flipped Classroom (FC) ed essere in grado di implementarla e di creare una mappa mentale per spiegarla agli altri.
* Abbozzare la struttura di un nuovo modulo di classe capovolta (o trasformare un modulo classico) per insegnare/imparare un argomento, utilizzando risorse e strumenti aperti.
* Comprendere le basi della strategia di Problem Based Learning ed essere in grado di spiegarle a studenti e colleghi.
* Progettare un'attività PBL (o trasformarne una già esistente) che possa essere applicata per insegnare l'uso di alcuni strumenti software aperti.
* Comprendere le basi della strategia di apprendimento basato su progetto.
* Progettare un'attività di apprendimento basato su progetto  (o trasformarne una già esistente) che possa essere applicata per insegnare l'importanza della promozione del FLOSS.

## Sessioni
### Prima sessione: Basata sulla comprensione della strategia dell’insegnamento capovolto.
We will review the components, stages and phases. Development of resources for the Flipped Classroom with FLOSS technologies, tools and strategies.
Esamineremo i componenti, le varie tappe e le fasi. Sviluppo delle risorse per l'insegnamento capovolto con tecnologie, strumenti e strategie FLOSS.
### Seconda sessione: Incentrata sul metodo dell'apprendimento Problem Based
Fasi. Approccio ai problemi rilevanti per gli studenti. Problemi di brainstorming da risolvere. Risorse open source disponibili per la risoluzione dei problemi. Strumenti consueti.
### Terza sessione: Dedicata alla strategia di apprendimento basato su progetto: componenti, tappe, fasi
Lavorare in situazioni reali. La sfida come motore dell'apprendimento. Collaborazione e cooperazione tra studenti. 
### Quarta sessione: Dedicata al progetto personale o di gruppo