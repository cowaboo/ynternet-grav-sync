---
title: 'Comunitat de pràctica (Béns comuns + gestió col·laborativa)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: ct
length: '9 hores'
objectives:
    - 'Explorar les bones pràctiques de gestió de CoP'
    - 'reconèixer com les CoP són presents en les accions quotidianes i involucrar de forma pràctica a l''alumnat en l''exploració i recerca de casos'
    - 'Conèixer les eines que utilitzen les CoP i les pràctiques de gestió de béns comuns vinculades'
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògraf
    - 'SlideWiki Academy'
skills:
    'Paraules clau':
        subs:
            'Moviment FLOSS': null
            'Cultura del FLOSS': null
            'Comunitats de Pràctica': null
            'Béns comuns': null
            'Gestió col·laborativa': null
            'Empoderament cívic': null
            'Participació de la comunitat': null
            'Eines i habilitats del FLOSS': null
---

## Introducció
En aquest mòdul explorarem Comunitats de pràctica (CoP) com a grups de persones que comparteixen un ofici o una professió. Gran part de la teoria darrere d'el concepte ha estat desenvolupada pel teòric educatiu Etienne Wenger. 
Mentre que un benefici important per a una CoP és la captura de coneixement tàcit, la motivació per participar en una CoP pot incloure retorns tangibles (promoció, augments o bonificacions), retorns intangibles (reputació, autoestima) i interès de la comunitat (intercanvi de coneixements relacionats amb la pràctica, interacció). Les comunitats de pràctica són importants per iniciar i establir una cultura de béns comuns. 
Aquest mòdul està configurat per mostrar-nos com això ja està succeint. Vincularem aquesta àrea amb una presentació dels Béns comuns com a filosofia i pràctica general, i explorarem el paper de la gestió col·laborativa com un element important.
## Context
L'objectiu de la sessió és demostrar i motivar els alumnes a explorar:
* Les bones pràctiques de gestió de les CoP.
* Reconèixer com les CoP són presents en les accions quotidianes.
* Conèixer les eines que fan servir les CoP i les pràctiques de gestió de béns comuns vinculades a elles.

## Sessions
### Primera sessió: Definició de les comunitats de pràctica (CoP)
Aquesta sessió definirà i descriurà les CoP i explorarà la seva relació amb els Béns comuns. Proporcionarà una base i una comprensió dels límits i desafiaments d'iniciar i consolidar les CoP.
### Segona sessió: Béns comuns (Digitals)
La segona sessió es centrarà en un enfocament de pensament crític sobre els béns comuns (digitals) i els mètodes de gestió basats en béns comuns.
### Tercera sessió: Viure en CoP
Aquesta sessió permetrà a les persones participants estudiar iniciatives específiques i com les CoP tenen un paper en elles, conèixer les eines que fan servir les CoP i comprendre els contractes socials darrere de la CoP.