---
title: 'La méthode FLOSS : comment les gens utilisent-ils les pratiques et les outils FLOSS dans leurs projets'
objectives:
    - 'Fournir des exemples de pratiques et d''outils FLOSS dans les projets'
    - 'Analyser l''impact de l''utilisation des pratiques FLOSS'
    - 'Motiver des politiques éduquées dans les organisations'
---

## Discussion de groupe sur «comment va-t’on travailler aujourd'hui?»
Le formateur commencera l'introduction du module en interrogeant les participants sur les initiatives de licence qui les entourent - provenant principalement d'autres organisations ou de l'UE. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## Un cas de communication intentionnelle en ligne: l'exemple Wikipédia
Présentation et analyse de l'[organisation Wikimania](https://wikimania.wikimedia.org/w/index.php?title=2019:Program&dir=prev&action=history).

Le rôle de la communauté, dans Wikipedia, est de:
Organisez et modifiez des pages individuelles
Structure de navigation entre les pages
Résoudre les conflits entre les membres individuels
Se réorganiser - créer des règles et des modèles de comportement
La nécessité de documenter de manière ouverte et réutilisable: [Learning Patterns](https://meta.wikimedia.org/wiki/Learning_patterns).

Quelques principes à comprendre et à développer:
* Empathie: faire le bien.
* Documenter et partager.
* Jardinez votre communauté.
* Les solutions sont au sein de votre groupe: ce que les gens vont dire EST utile.
* Liste des questions auxquelles répondre, main de la main avec votre groupe.
* Concentrez-vous sur des pratiques simples, réutilisables et faciles à reproduire pour vos bénéficiaires réels (responsabilisation).
* Partager les connaissances, inspirer, devenir un motivateur.
* Mener par l'exemple

Les «citoyens en réseau» et les bâtisseurs de communautés sont des acteurs du changement :
* Nouveaux membres.
* Bonnes pratiques.
* Expérimentation.
* Continuité ("Garder les lumières allumées").
* Activités de mesure.
* Récompense et reconnaissance

Lier Wikipédia à la culture FLOSS (comportement et principes) :
* Soyez aux commandes et soyez humble.
* Comprenez le biais.
* Appréciez l'idiosyncratie (le consensus est essentiel).
* Ou, agissez comme Perl.
* Faites de grands projets sur Wikipédia.
* Évitez les cabales.
* Suivez l'esprit et la lettre de la GFDL.
* Soyez respectueux mais fermes

[GNU Free Documentation Licence](https://en.wikipedia.org/wiki/GNU_Free_Documentation_License).
## Méthodologie FLOSS et impact autour de nous
L'adoption des logiciels libres par les entreprises a eu un impact significatif sur le développement commercial et logiciel depuis plus d'une décennie. Par exemple, une recherche antérieure basée sur une enquête menée en Norvège montre que «plus de 30% des répondants à notre enquête ont plus de 40% de leur revenu des services ou logiciels liés aux logiciels libres.» (Hauge et al., 2008, p . 211) En outre, «Dès 2008, Gartner a interrogé 274 entreprises dans le monde et a constaté que 85% avaient déjà adopté l'open source, la plupart des autres s'attendant à le faire au cours de l'année suivante.» (Aberdour, 2011, p. 6).

De plus, des études antérieures ont identifié l'importance des PME pour l'adoption des logiciels libres dans le contexte des entreprises. Par exemple, une étude de l'UE a examiné l'impact des logiciels libres et a rapporté en 2006 que: «Dans le secteur privé, l'adoption des logiciels libres est dirigée par des moyennes et grandes entreprises.» (Ghosh, 2006, p. 9).

Au départ, les entreprises hésitent à adopter les logiciels libres. Comme l'a déclaré Unni (2016): «Les origines des logiciels open source remontent à 1964-1965 lorsque Bell Labs s'est associé au Massachusetts Institute of Technology (MIT) et à General Electric (GE) pour travailler au développement de MULTICS. pour créer un système informatique dynamique et modulaire capable de prendre en charge des centaines d'utilisateurs. Pendant les premiers stades de développement, les logiciels open source ont connu un démarrage très lent, principalement en raison de certaines notions préconçues qui les entourent. »(Unni, 2016, p. 271).

En outre, la sortie ou le passage d'une solution logicielle à une autre peut entraîner des coûts qui doivent être pris en compte dans les scénarios d'adoption des logiciels libres, comme le soutenaient les recherches précédentes: «Pour une organisation, l'abandon d'une solution logicielle spécifique, parfois appelée sortie (ou les coûts de commutation) peuvent entraîner différents types de coûts de personnel. Par exemple, si le personnel existant est formé et expérimenté dans une technologie spécifique, un changement peut entraîner des coûts spécifiques pour l'organisation. » (Lundell, 2012).
## Exemples de logiciels PM Open Source
Les outils de gestion de projet FOSS continuent d'évoluer et d'ajouter des fonctionnalités. Une connectivité réseau plus robuste et des appareils mobiles plus rapides et plus puissants ont étendu leur portée. À titre d'exemple, certains des outils de gestion de projet open source populaires sont brièvement décrits ici.

MyCollab est le logiciel de collaboration multiplateforme qui peut être exécuté dans la plupart des systèmes d'exploitation. Il est bien adapté aux PME et est considéré comme une suite de trois modules de collaboration: gestion de projet, gestion de la relation client (CRM) et logiciel de création et d'édition de documents. L'édition FOSS de ce logiciel ne fournit pas d'option cloud, mais il est complet.

LibrePlan Open Web Planning est une application de gestion de projet basée sur le Web permettant une collaboration complète de l'équipe. Les fonctionnalités incluent la prise en charge de l'allocation des ressources, des diagrammes de Gantt, etc. La conception et l'interface sont intuitives et simples à utiliser. Le logiciel est bien pris en charge avec la documentation ainsi que les rapports. Un support professionnel est également disponible pour les utilisateurs. Libreplan propose une application Web et mobile.

Projectlibre a été fondée pour fournir un logiciel gratuit et open source de remplacement du bureau Microsoft Project. Actuellement, plus de 3 millions de téléchargements du logiciel de bureau avec une solution cloud seront bientôt disponibles. Les développeurs affirment que la version cloud sera légère avec une conception simple. La version de bureau offre des fonctionnalités telles que la gestion des tâches, l'allocation des ressources, le suivi, les diagrammes de Gantt, etc.

Odoo propose une suite d'applications professionnelles FOSS qui comprend un logiciel de gestion de projet en plus du CRM, du commerce électronique, de la comptabilité, de l'inventaire et du point de vente. Odoo est une solution multiplateforme, prenant en charge les distributions GNU / Linux et plusieurs autres systèmes d'exploitation

La dernière version d'OpenProject contient des fonctionnalités de collaboration, telles que l'éditeur de texte WYSIWYG, un flux de travail intelligent et une mise en forme conditionnelle. En plus de cela, OpenProject offre la gestion des tâches, le rapport de temps et de coûts, Scrum etc. Le logiciel est conçu pour soutenir les équipes de projet tout au long du cycle de vie du projet et offre une planification de projet collaborative, des rapports de chronologie, OpenProject fournit une interface utilisateur très intuitive ainsi que documentation de support très complète.
## Devoirs
Voir dans la vidéo suivante : [Logiciel libre, société libre : Richard Stallman à TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM), puis publier dans la section des commentaires une autre vidéo qui explique - démontre des arguments similaires. Partagez vos impressions sur la nouvelle vidéo.
## Références
* Nathan L. Ensmenger (2004) [Open Source’s Lessons for Historians](https://doi.org/10.1109/MAHC.2004.32), IEEE Annals of the History of Computing, Vol. 26, No. 4, pp. 104-103.
* Rishab Aiyer Ghosh (2006) Study on the: Economic impact of open source software on innovation and the competitiveness of the Information and Communication Technologies (ICT) sector in the EU, Final report, Prepared on November 20, 2006.
* Øyvind Hauge, Claudia Ayala, and Reidar Conradi (2010) [Adoption of open source software in software-intensive organizations - A systematic literature review](http://dx.doi.org/10.1016/j.infsof.2010.05.008). Information and Software Technology, Vol. 52, No. 11, pp. 1133-1154.
* Björn Lundell (2012) Why do we need Open Standards?, In M. Orviska & K. Jakobs (Eds.), Proceedings 17th EURAS Annual Standardisation Conference ‘Standards and Innovation’ (227-240). The EURAS Board Series, Aachen, ISBN: 978-3-86130-337-4.
* V.K. Unni (2016) [Fifty Years of Open Source Movement: An Analysis through the Prism of Copyright Law]((https://law.siu.edu/_common/documents/law-journal/articles-2016/8%20-%20Unni%20Article%20Proof%205%20FINAL%20-%20sm.pdf)), Southern Illinois University Law Journal, vol. 40, no. 2, pp. 271-X.