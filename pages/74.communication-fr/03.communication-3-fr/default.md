---
title: 'FLOSS dans votre vie'
objectives:
    - 'Fournir une analyse critique de l''importance des licences FLOSS pour l''enseignement ouvert, l''art et l''apprentissage collectif'
    - 'Examiner l''utilisation et les avantages des licences FLOSS dans l''enseignement et l''art'
    - 'Stimuler l''engagement actif et créatif des apprenants grâce aux licences FLOSS'
    - 'Motiver les participants à adopter des licences FLOSS dans leurs activités éducatives actuelles et futures'
---

## Introduction à la séance : licences, éducation ou art
Les participants réfléchiront sur les licences et les initiatives éducatives qui les entourent. Les réponses seront documentées puis utilisées pour personnaliser le matériel de formation.
## FLOSS est-il pour vous?
### Une discussion sur les avantages potentiels de l'utilisation de FLOSS comprend
* La possibilité de développer davantage vos compétences numériques.
* Amélioration de la qualité des logiciels créée par la puissance collective de membres talentueux de la communauté.
* La possibilité de personnaliser le logiciel pour son propre usage.
* Une concurrence loyale sur le marché des logiciels.
* Absence de blocage des fournisseurs, ce qui signifie qu'il n'y a plus de risque d'être bloqué par le fournisseur ou la technologie, puis d'être à la merci des augmentations de prix des fournisseurs et de manquer de flexibilité à laquelle vous ne pouvez pas facilement et facilement échapper.
* L'énorme potentiel d'augmentation de la sécurité, car il permet des vérifications de sécurité indépendantes beaucoup plus approfondies qui aident à fermer les failles de sécurité plus rapidement.
* Durabilité à long terme des logiciels avec un développement et un dépannage plus rapides, car il existe une communauté puissante lorsque des problèmes surviennent ou la possibilité de choisir un autre fournisseur et potentiellement d'influencer le développement, pas de blocage des fournisseurs, etc.
* Accroître la stabilité et la protection de la vie privée, principalement lorsque le code source est public, car cela augmentera la possibilité de détecter les comportements nuisibles et permettra aux experts de les corriger même si le fournisseur ne le souhaite pas.
* Augmentation de la fiabilité, car plus les yeux sont tournés vers les logiciels libres, le code source a tendance à être supérieur et la sortie très robuste.
* Les entreprises et les utilisateurs ont plus de contrôle sur leurs propres appareils matériels, leurs propres systèmes informatiques et les artefacts numériques associés.
* Que les communautés mondiales et unies améliorent les logiciels libres en introduisant souvent de nouveaux concepts et capacités plus rapidement, mieux et plus efficacement.
* Diminution des coûts des logiciels à moyen et à long terme car les logiciels libres ne nécessitent aucun frais de licence.

### Les modèles commerciaux de FLOSS sont les suivants
* Services auxiliaires.
* Développement et distribution d’entreprise.
* SaaS avec distribution de logiciels serveurs.
* Double licence double / licences d'exception de vente.
* Adhésion et dons.
* Financement participatif.
* La publicité.
* Mettre à jour les abonnements.
* Données d'utilisateur.
* Certification de logiciels.

### Apportez vos ressources
Les logiciels libres et open source ne sont possibles que grâce à ses communautés solides. Il s'agit d'individus, d'organisations, d'entreprises et d'organismes publics bénévoles et non rémunérés. En raison de cette variété, il existe d'innombrables façons dont ces acteurs peuvent soutenir les logiciels libres et les projets logiciels particuliers.

Évidemment, il y a des développeurs. De nombreux projets n'existeraient pas sans des bénévoles qui contribuent au code, aux traductions, aux tests et aux améliorations de conception sur une base non rémunérée pendant leur temps libre. Il existe également des entreprises développant des logiciels libres avec une multitude de modèles commerciaux. Les organisations à but non lucratif et les organismes publics développent également leurs propres logiciels ou améliorent les logiciels existants sur le marché.

Étant donné que les logiciels libres peuvent souvent être utilisés gratuitement par n'importe quel utilisateur et organisation, les projets FOSS dépendent d'un soutien financier. Cela peut se produire grâce à des dons de particuliers, d'entreprises ou de fonds. Les entreprises et les organisations peuvent également soutenir indirectement un projet logiciel en engageant les principaux développeurs ou des entreprises externes à créer de nouvelles fonctionnalités, et en les publiant sous une licence FOSS afin de le fusionner en amont du projet d'origine.
## Regardez la vidéo sur les avantages pour les PME
[Qu'y a-t-il pour moi? Avantages du code Open Sourcing](https://www.youtube.com/watch?v=ZtYJoatnHb8) - Développeurs Open Source @ Google Speaker Series: Ben Collins-Sussman et Brian Fitzpatrick, 25.10.2007.

Étudiez, documentez et discutez ses principaux arguments. Créez un document commun avec tous les participants, partagez-le avec les autres.
## Devoirs
Recherchez dans le référentiel ouvert [Zenodo](https://zenodo.org/) et trouvez vos trois articles préférés sur FLOSS et l'éducation. Documentez et / ou postez un commentaire à leur sujet.
## Références
* [La communication, c'est de l'information en mouvement](https://www.netizen3.org/index.php/La_communication_c%27est_de_l%27information_en_mouvement).

* [La communication expressive dans les forums de discussion: émotions et attitude ironique chez l'adolescent](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiT2_Pm54HrAhWWHHcKHVLPAkYQFjAJegQIChAB&url=http%3A%2F%2Fwww.unine.ch%2Ffiles%2Flive%2Fsites%2Ftranel%2Ffiles%2FTranel%2F57%2F63-82_Aguert%2520%2526%2520Al_def.pdf&usg=AOvVaw3BRZOFxDzg-bzIEBQ8bt6y).