---
title: 'Communication interpersonnelle intentionnelle'
objectives:
    - 'Cartographier et explorer les principaux concepts de la communication interpersonnelle intentionnelle'
    - 'Comprendre l''interdépendance de la communication interpersonnelle intentionnelle et de la culture FLOSS'
    - 'Découvrir les arguments éthiques, juridiques, sociaux, économiques et d''impact de la communication interpersonnelle intentionnelle'
    - 'Pour que les participants conçoivent leur propre scénario et leurs pratiques de communication interpersonnelle intentionnelle'
---

## Discussion de groupe
Le formateur commence l'introduction du module en interrogeant les participants sur leurs expériences actuelles et antérieures sur les pratiques de communication dans les projets, le travail et la vie de famille. L'objectif principal de cette activité est de cartographier la pléthore de pratiques de communication actuelles et de les réutiliser pendant les activités du cours.
## La nécessité d'une communication interpersonnelle intentionnelle
La communication interpersonnelle et les compétences numériques sont fondamentales pour les activités commerciales et de gouvernance, ainsi que pour le développement de réseaux communautaires et d'activités collaboratives. Dans les organisations d’aujourd’hui, les membres de l’organisation doivent trouver des solutions créatives issues d’une réflexion commune qui exploitent les connaissances et les capacités combinées de personnes aux perspectives diverses. La plupart des innovations scientifiques et artistiques mettent l'accent sur la dimension sociale de la créativité à travers les interactions avec les autres, la résolution conjointe de problèmes.
Cette expérience de renégociation des propriétés de communication personnelle avec les pairs et les organisations peut avoir un effet de levier lorsqu'il s'agit de développer des compétences interpersonnelles et de résoudre, en collaboration, les problèmes de gouvernance. Une expérience de compétences interpersonnelles synergiques où l'apprentissage par la pratique devient un pilier de l'acquisition de compétences. La communication interpersonnelle, la présentation de soi et la gestion des conflits sont des questions clés pour des solutions durables à des problèmes complexes, mais aussi des éléments cruciaux pour améliorer l'employabilité et la motivation, tout en réduisant le roulement.

![](comm1.png)
_Schema sur la communication interpersonnelle intentionnelle: Athanasios Priftis, Creative Commons Attribution-Share Alike 4.0 International License._

La communication intentionnelle est un acte par lequel une personne donne ou reçoit d'une autre personne des informations sur ses besoins, ses désirs, ses perceptions, ses connaissances ou ses états affectifs. La communication peut être intentionnelle ou non, peut impliquer des signaux conventionnels ou non conventionnels, peut prendre des formes linguistiques ou non linguistiques, et peut se produire à travers des modèles parlés ou autres. La communication intentionnelle, également appelée communication consciente, se réfère à l'affichage de signaux de communication ou à un comportement de signalisation dans lequel l'expéditeur est déjà conscient de l'effet qu'un tel affichage aura sur sa cible, c'est-à-dire l'auditeur. La communication intentionnelle peut être utilisée pour; faites passer un point, persuadez-en un autre, pour déclencher une action, ou tout simplement pour le plaisir.
Au moment de, toujours sur la communication en ligne, cette approche devient cruciale.

Cependant, la communication intentionnelle n'est pas seulement positive, Wikipedia donne une liste d'exemples pour comprendre comment communiquer dans une communauté :-)

![](comm2.jpg)

## Concevez votre propre scénario de communication intentionnelle (travail de groupe)
Dans cette activité, nous utiliserons les méthodologies de co-conception existantes pour que les participants conçoivent et proposent leur propre scénario de communication intentionnelle.
## Devoirs
Étudiez ces 2 pages de Wikipédia: S'il vous plaît mordez les débutants
* [Page 1](https://en.wikipedia.org/wiki/Wikipedia:Please_bite_the_newbies#How_to_bite_a_newbie)
* [Page 2](https://en.wikipedia.org/wiki/Wikipedia:Give_%27em_enough_rope)

En quoi est-ce utile ou non pour votre vie quotidienne et votre travail?
## Références
* [Resource](https://www.onecommunityglobal.org/conscious-and-conscientious-communication/)
* [Wikipedia:Give 'em enough rope](https://en.wikipedia.org/wiki/Wikipedia:Give_%27em_enough_rope)
* [Livret TOUS Citoyens du Net (CdN) in FR](https://upload.wikimedia.org/wikipedia/commons/b/b4/Livret_TOUS_Citoyens_du_Net_(CdN).pdf )