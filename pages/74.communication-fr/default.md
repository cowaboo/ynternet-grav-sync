---
title: 'Communication interpersonnelle intentionnelle avec FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: fr
objectives:
    - 'Les bonnes pratiques de la communication interpersonnelle intentionnelle'
    - 'Comment la culture FLOSS est liée aux règles des médias sociaux et aux utilisations courantes de l''information'
    - 'Cas de communication intentionnelle en ligne comme la communauté Wikipedia'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté à Internet)'
    - 'Connexion Internet'
    - Bearmer
    - 'Papier et stylos'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    'Mots clés':
        subs:
            'Mouvement FLOSS': null
            'Culture FLOSS': null
            Netizenship: null
            'Communication intentionnelle': null
            'Autonomisation civique': null
            'Engagement communautaire': null
            'Outils et compétences FLOSS': null
length: '10 heures'
---

## Introduction
La technologie numérique a tout changé. Rappelez-vous votre première vision du Web? Vous lisez des pages d'informations, comme la lecture d'un livre. Vous avez traité les messages lors du traitement d'un courrier. Et petit à petit, vous vous êtes rendu compte que vous n'aviez pas à faire face à un moyen de communication tout comme les autres. Utilisateur unique d'un bureau de poste ou d'une vitrine virtuelle, vous vous êtes senti progressivement devenu acteur… Du livre [Citoyens du Net](https://www.ynternet.org/page/livre).

En 2019, le web a 30 ans. Avec ses centaines de millions de forums, wikis, blogs, réseaux sociaux, microblogs instantanés, il justifie désormais de parler de la webosphère. Google, Facebook, Twitter et Wikipedia sont les planètes les plus célèbres. Rempli de centaines de milliards et de milliards d'articles provenant de différents domaines (titre, corps du message, pièces jointes, images, nombre de visiteurs, notes), le web est le cœur qui fait converger tous les outils numériques qui ont une interface ouverte, un accès ouvert standardisé et accessible par tous les outils existants - smartphones, tablettes, ordinateurs et objets connectés. Les endroits les plus intéressants sont ceux où vous pouvez interagir en commentant, modifiant, ajoutant du texte ou des images à ce sujet. La communication intentionnelle pour l'autonomisation civique et l'engagement communautaire avec FLOSS constitue une alternative viable.
## Contexte
Le but de la session est de démontrer et d'engager pratiquement les apprenants à explorer :
* Les bonnes pratiques de la communication interpersonnelle intentionnelle.
* Comment la culture FLOSS est liée aux règles des médias sociaux et aux utilisations courantes de l'information.
* Cas de communication intentionnelle en ligne comme la communauté Wikipedia.

L'apprenant sera capable de décrire le contexte, l'histoire, les caractéristiques et les étapes de la communication intentionnelle en relation avec la culture web. Ils seront en mesure d'articuler l'importance de passer du consommateur à l'acteur de la société de l'information.
## Séances
### Première séance : Communication interpersonnelle intentionnelle
Cette séance démontrera l'utilisation des technologies numériques libres et ouvertes pour la communication intentionnelle dans la vie quotidienne.Elle permettra aux apprenants de réaliser et de développer leurs compétences en communication intentionnelle et d'utiliser les technologies numériques libres et ouvertes pour développer des pratiques et des habitudes de communication intentionnelle.
### Deuxième séance : La méthode FLOSS : comment les gens utilisent-ils les pratiques et les outils FLOSS dans leurs projets?
La deuxième séance se concentrera sur une approche de pensée critique autour de la communication intentionnelle dans divers environnements sociaux et culturels. Il explorera les méthodes de collaboration en ligne dans des activités en petits groupes pour apprendre les uns des autres et soutenir une culture de communication intentionnelle.
### Troisième séance : FLOSS dans votre vie
Cette séance permettra aux participants de développer et de partager leurs préférences de communication intentionnelles. Nous visiterons la communication intentionnelle comme base de la participation synergique (avec l'exemple de la prise de notes collaborative) et la relierons aux outils FLOSS qui peuvent servir les activités actuelles des participants.