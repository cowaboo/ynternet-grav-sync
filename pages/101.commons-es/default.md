---
title: 'Comunidad de práctica (Bienes comunes + gestión colaborativa)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: es
length: '9 horas'
objectives:
    - 'Explorar las buenas prácticas de gestión de CoP'
    - 'Reconocer cómo las CoP están presentes en las acciones cotidianas e involucrar de forma práctica al alumnado en la exploración y búsqueda de casos'
    - 'Conocer las herramientas que utilizan las CoP y las prácticas de gestión de bienes comunes vinculadas a ellas'
materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a Internet'
    - Proyector
    - 'Papel y bolígrafos'
    - Papelógrafo
    - 'Academia Slidewiki'
skills:
    'Palabras clave':
        subs:
            'Movimiento FLOSS': null
            'Cultura del FLOSS': null
            'Comunidades de Practica': null
            'Bienes comunes': null
            'Gestión colaborativa': null
            'Empoderamiento cívico': null
            'Participación de la comunidad': null
            'Herramientas y habilidades de FLOSS': null
---

## Introducción
En este módulo exploraremos Comunidades de práctica (CoP) como grupos de personas que comparten un oficio o una profesión. Gran parte de la teoría detrás del concepto ha sido desarrollada por el teórico educativo Etienne Wenger. 
Mientras que un beneficio importante para una CoP es la captura de conocimiento tácito, la motivación para participar en una CoP puede incluir retornos tangibles (promoción, aumentos o bonificaciones), retornos intangibles (reputación, autoestima) e interés de la comunidad (intercambio de conocimientos relacionados con la práctica, interacción). Las comunidades de práctica son importantes para iniciar y establecer una cultura de bienes comunes. 
Este módulo está configurado para mostrarnos cómo esto ya está sucediendo. Vincularemos esta área con una presentación de los Bienes comunes como filosofía y práctica general, y exploraremos el papel de la gestión colaborativa como un elemento importante.
## Contexto
El objetivo de la sesión es demostrar y motivar a los alumnos a explorar:
* Las buenas prácticas de gestión de las CoP.
* Reconocer cómo las CoP están presentes en las acciones cotidianas.
* Conocer las herramientas que usan las CoP y las prácticas de gestión de bienes comunes vinculadas a ellas.

## Sesiones
### Primera sesión: Definición de las comunidades de práctica (CoP)
Esta sesión definirá y describirá las CoP y explorará su relación con los Bienes comunes. Proporcionará una base y una comprensión de los límites y desafíos de iniciar y consolidar las CoP.
### Segunda sesión: Bienes comunes (Digitales)
La segunda sesión se centrará en un enfoque de pensamiento crítico sobre los bienes comunes (digitales) y los métodos de gestión basados en bienes comunes.
### Tercera sesión: Vivir en CoP
Esta sesión permitirá a las personas participantes estudiar iniciativas específicas y cómo las CoP desempeñan un papel en ellas, conocer las herramientas que usan las CoP y comprender los contratos sociales detrás de la CoP.