---
title: 'Fer consultes a Wikidata'
length: '120 min'
objectives:
    - 'Examineu l''interès del llenguatge de consulta i les eines per obtenir respostes a preguntes complicades'
    - 'Motivar els participants perquè aprenguin a fer consultes bàsiques o fins i tot consultes avançades'
    - 'Apreneu a fer consultes a SPARQL'
---

## Consultes
### Entendre el servei de consultes
El servei de consultes s’haurà introduït molt breument durant la sessió 2. Es veurà més a fons en aquesta sessió.
### Descobrir a fons SPARQL i saber fer consultes
Tanmateix, amb moltes audiències en situacions professionals, és necessari proporcionar un entorn separat per comprendre millor els idiomes Sparql i com crear consultes fortes.
### Els participants creen les seves pròpies consultes (a l'ordinador)
Permeteu que el públic escrigui les seves pròpies consultes amb l'ajuda del Servei de consultes
### Consells
* Recordeu que, per a molts públics, serà la primera vegada que treballa amb un llenguatge de consulta (SQL, SPARQL, etc.). Per tant, és molt important que comenceu pels elements més bàsics i que augmenteu de forma incremental la complexitat de la informació. Una bona tàctica per fer-ho és crear diverses consultes de diferent complexitat davant del públic. Per exemple, consultes, vegeu Wikidata: Servei de consultes SPARQL / Construir una consulta.
* Assegureu-vos de demostrar com prendre un exemple de consulta i modificar-lo per adaptar-lo a una necessitat diferent. Moltes persones no escriuran immediatament consultes de Wikidata des de zero, sinó que modificaran les consultes existents. L’assistent de consultes de Wikidata és particularment útil per al públic que modifica consultes.
* Assegureu-vos d'incloure tant la redacció guiada de consultes, on els instructors guien la redacció de la consulta, com un període de temps perquè el públic pugui escriure les seves pròpies consultes. Fer que els participants escriuen la seva pròpia consulta, reforça el procés i les habilitats de modificar o escriure una consulta, de manera que puguin fer-ho amb confiança en el futur.
* Si el públic és majoritàriament Wikimedians, assegureu-vos de passar temps mostrant-los com aplicar les consultes de Wikidata a diverses eines utilitzades per la comunitat Wikimedia, inclosos Petscan, Listeriabot i Histropedia.

## Resum i preguntes
Per acabar la sessió, la persona a càrrec del taller facilitarà un moment de debriefing on els participants s'animen a expressar les seves preguntes, dubtes, idees i sentiments sobre els temes tractats.
## Deures
Configureu una eina [campanya a ISA] (https://tools.wmflabs.org/isa/campaigns) i demaneu als participants que participin a la campanya abans de la propera sessió.
## Referències
* [Ajuda de consulta] (https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/Wikidata_Query_Help)
* [Exemples] (https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples)
* [Vídeo per a principiants] (https://www.wikidata.org/wiki/File:Querying_Wikidata_with_SPARQL_for_Absolute_Beginners.webm)