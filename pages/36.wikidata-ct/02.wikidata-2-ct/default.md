---
title: 'Editar Wikidata'
length: '180 min'
objectives:
    - 'Comprendre com es recopila i s''afegeix el contingut de Wikidata a la base de dades'
    - 'Apreneu a afegir contingut a Wikidata i a fer addicions massives a Wikidata'
    - 'Descobriu com col·labora la comunitat wikidata per construir la base de dades'
    - 'Comprendre com s''incorporen els processos de qualitat al projecte'
    - 'Conegueu les meravelles d''obtenir respostes a preguntes complicades amb sistemes de consulta'
---

Preguntes sobre l'última sessió i discussió sobre els deures
## Introducció a l'edició
### Edició general i revisió de l'estructura de dades
La secció "Introducció a l'edició" normalment garanteix que els participants puguin:
* Cerca el botó d'edició i contribueix a l'etiqueta i a la descripció d'un element de Wikidata.
* Cerqueu el botó d'edició d'una propietat existent, modifiqueu-la amb un qualificador i afegiu referència a un element de Wikidata.

Revisió de la part fonamental d'un element de WikiData (etiqueta, identificadors, declaracions, qualificatius, referències, propietats i valors)
Demostració de com editar.
L'eina Discovery of Recoin, destinada a identificar la informació que falta sobre un element.

### Activitat d'edició (a l'ordinador)
* Preneu els tutorials de WikiData: creeu un element i afegiu una declaració
* A continuació, creeu o milloreu un element
* Analitzeu alguns elements amb Recoin
* Completar més elements (els estudiants també haurien d'afegir referències i fonts)
* Feedback: informe col·lectiu de cada estudiant sobre el que es va fer, les dificultats assolides, les preguntes, els pensaments.

### Consells
* Heu de demostrar que editeu un element que hàgiu identificat abans de l'esdeveniment, inclosa l'addició d'una etiqueta, una descripció i una propietat comuna per a aquest tipus d'element. Recordeu d’indicar on es troben els botons d’edició per a cada tipus d’edició.
* Heu de demostrar com afegir una referència a una declaració existent. Gran part del valor de Wikidata a llarg termini prové de la capacitat de referenciar declaracions a fonts fiables de material. Igual que quan es demostra la referència durant els tallers d’edició de Viquipèdia, és important preparar la referència amb antelació. Recordeu que heu de descriure els camps més habituals per a referències a Wikidata (si teniu aparells personalitzats per copiar referències o emplenar referències mitjançant Citoid, assegureu-vos de descriure com canvien la interfície).
* Si teniu instal·lats gadgets o eines personalitzats al vostre compte de Wikidata, penseu en desactivar-los o discutir en què la vostra interfície és diferent de la interfície estàndard de Wikidata.

### Lectures
* [Wikidata:Planning a Wikidata Workshop](https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_Workshop/Adding_basic_statements_to_Wikidata_items_by_engaging_newbies)
* [Wikidata Tours](https://www.wikidata.org/wiki/Wikidata:Tours)

## Processos d'edició de Wikidata
L'objectiu d'aquesta secció és entendre millor els processos d'edició de Wikidata, en particular insistint en la transparència i la possibilitat de fer un seguiment de cada edició.

### Presentació de les funcions d’edició i col·laboració
Fer un recorregut per diverses funcions de WikiData, útils per a l'editor: pàgina d'usuari, TalkPage, llista de seguiment, contribucions pròpies.
Fer un recorregut per les funcions útils per a la comunitat per fer un seguiment de l’activitat al lloc: RecentChange, pàgina de l’historial de l’ítem, pàgines d’altres usuaris, interacció amb els usuaris a les seves pàgines de discussió, llista d’altres contribucions dels participants.
Presentació de l’estratègia WikiData basada en SoftSecurity i HardSecurity.
Demostració de les contribucions aportades pels participants durant l'activitat anterior.

### Activitat (a l'ordinador)
Cada estudiant pren el temps per explorar allò que s’acaba de descriure. L’entrenador pot demanar a cada participant que escrigui un missatge a la pàgina de discussió d’un altre participant, etc.

## Qualitat de dades i avaluació de dades

### Presentació de qualitat i avaluació
* Proporcioneu una visió general del contingut que ja està disponible a WikiData.
* Com avaluar la qualitat d'un article.
* Utilització d’elements ShowCase per crear un element.
* Explorar més fonts i cites (com tractar fets conflictius, què és una bona font de dades, què no).
* Descobriment d’eines per ajudar a omplir buits, en profunditat i en àmplia extensió (els exemples poden incloure TABernacle, Mix'n’Match, el joc WikiData, The
* Joc distribuït, Wikishootme, Terminator. A escollir en funció del públic).
* Introducció a una eina d'edició massiva: quickstatements.
* La presentació inclourà debats, preguntes i respostes.

### Consells
Les demostracions solen incloure:
* Mix'n'Match: les entrades de la llista d'eines d'algunes bases de dades externes i permeten als usuaris fer-les coincidir amb els elements de Wikidata
* WikiShootMe: és una eina per mostrar articles de Wikidata, articles de Wikipedia i imatges de Commons amb coordenades, tot al mateix mapa.

Facilita l’addició d’imatges als projectes de Wikimedia.
* Jocs de Wikidata: conjunt de jocs per afegir declaracions ràpidament a Wikidata.

### Lectures
* [Wikidata Game](https://tools.wmflabs.org/wikidata-game/)
* [Distribution](https://tools.wmflabs.org/wikidata-game/distributed/)
* [Mix-n-match](https://tools.wmflabs.org/mix-n-match/#/)
* [WikiShootMe](https://tools.wmflabs.org/wikishootme/#lat=43.35953739293013&lng=5.325894166828497&zoom=15)
* [Quick Statements](https://tools.wmflabs.org/quickstatements/#/)
## Introducció bàsica a les consultes WikiData mitjançant Query Helper
Els participants podran:
* Compreneu com llegir i modificar una consulta bàsica de Wikidata mitjançant SPARQL i l'Assistent de consultes.
* Comprendre com construir consultes súper bàsiques que retornin diverses variables i etiquetes.
* Compreneu quins tipus de visualitzacions estan disponibles per fer servir les consultes de Wikidata.
* Què són les consultes i què és Sparql

### Descobriment del servei de consulta de Wikidata
Demostració amb una consulta senzilla a partir d’exemples i modificació de la consulta inicial. Mostra diferents visualitzacions.

### Activitat (a l'ordinador portàtil si el temps ho permet)
Els participants juguen amb el Servei de consultes: explorant els exemples amb diferents opcions de visualització, modificant els exemples per als més atrevits.

## Resum i preguntes
Per acabar la sessió, l'entrenador facilitarà un moment de debriefing on els participants s'animen a expressar les seves preguntes, dubtes, idees i sentiments sobre els temes tractats.
Introducció a la propera sessió.
La següent sessió es dedicarà a consultes.

## Treball a casa (autònom)
* Cerqueu 3 idees de preguntes de consulta per enviar a les properes sessions
* Llegiu [Glossari de Wikidata] (https://www.wikidata.org/wiki/Wikidata:Glossary)

## Referències
* [Planificació] (https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop).
* [Professional] (https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata).
* [Ajuda] (https://www.wikidata.org/wiki/Help:Contents).
* [Eines] (https://www.wikidata.org/wiki/Wikidata:Tools).
* [Recursos educatius] (https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources).