---
title: 'Aplicabilitat de Wikidata'
length: '120 min'
objectives:
    - 'Identifiqueu el valor aplicat de Wikidata, especialment per a aplicacions com la visualització de dades complexes'
    - 'Identificar les possibles aplicacions de Wikidata en el seu propi treball, ja sigui mitjançant la visualització, incrustant Wikidata al seu propi programari'
    - 'Saber com contactar i connectar-se amb membres de la comunitat de Wikidata'
---

## Preguntes sobre l'última sessió i discussió sobre els deures
Opinions sobre sparql i consultes.
Resultats de la campanya ISA i relació amb la sessió del dia (ús de la informació de wikidata per millorar la descripció de la imatge amb dades estructurades).

## El potencial del valor aplicat de Wikidata
Visió general i aparadors.
Aquesta secció es pot dividir en 3 parts:
* La primera podria proporcionar una visió general del contingut ja disponible a WikiData: xifres i estadístiques per tema.
* La segona podria proporcionar exemples de com s'utilitzen Wikidata pels projectes de Wikimedia.

Alguns exemples suggerits inclouen (per seleccionar en funció del públic):
1. una eina per explorar la bretxa de gènere del contingut a la Viquipèdia, com ara Denelezh.
2. l'ús de Listeria bot per crear llistes vermelles d'articles que falten. Vegeu Categoria: Llistes_basades_en_Wikidata.
3. l'ús de Wikidata per afegir avisos d'autoritat als articles de Wikipedia. Exemple de Doris Day.
4. l'ús de Wikidata per mantenir les caixes d'informació a la Viquipèdia actualitzades. Exemple de Jack Spicer.
5. l'ús de la funció d'analitzador per mostrar directament informació de WikiData als articles (vegeu Com_utilitzar_dades_en_projectes_Wikimedia).
6. l’ús d’informàtics màgics a Viquipèdia (com ara l’ús de {{infobox fromage}} o {{infobox biographies2}} a la Viquipèdia francesa).

* La tercera proporcionaria exemples de com Wikidata es utilitzada per tercers.

Alguns exemples suggerits per presentar són (que es seleccionaran en funció del públic):
1. diverses eines de visualització com ara: El creador de gràfics de Wikidata o Histropedia (aplicació de cronologia basada en Wikidata)
2. crotos, un motor de cerca i visualització d’obres d’art visuals basat en Wikidata i que utilitza fitxers Wikimedia Commons
3. Scholia: un motor de cerca i visualització per a acadèmics basat en Wikidata

### Lectures
* [Denelezh](https://www.denelezh.org)
* [Lists based on Wikidata](https://en.wikipedia.org/wiki/Category:Lists_based_on_Wikidata)
* [Doris Day](https://en.wikipedia.org/wiki/Doris_Day)
* [Jack Spicer](https://en.wikipedia.org/wiki/Jack_Spicer)
* [How to use data on Wikimedia projects](https://www.wikidata.org/wiki/Wikidata:How_to_use_data_on_Wikimedia_projects)
* [Showcases](https://phabricator.wikimedia.org/T168057)
* [Wikidata knowledge as a service](https://www.wikidata.org/wiki/File:Wikidata_Knowledge_as_a_Service_slides_OeRC_Feb2018.pdf)
* [Histropedia](http://histropedia.com)
* [Angryloki](https://angryloki.github.io/wikidata-graph-builder/?property=P279&item=Q5)
* [Crotos](http://zone47.com/crotos/)
* [Sholia](https://tools.wmflabs.org/scholia/)

### Activitat  
Proporcioneu als participants la llista d’eines seleccionades i deixeu-los explorar.
## Connecteu-vos amb la comunitat WikiData
### Com connectar-se
L’objectiu d’aquesta secció és saber com contactar i connectar-se amb membres de la comunitat de Wikidata. Com mantenir-se en contacte amb les darreres novetats del projecte i on trobar recursos.
Reviseu les opcions per discutir amb un wikidatien en una pàgina de discussió.
Introduïu WikiProjects i expliqueu com unir-s'hi o interactuar amb els seus membres. Expliqueu els avantatges d'aquests WikiProjects, però també la seva governança (llibertat per unir-vos, etc.) Idealment escolliu mostrar un wikiprojecte rellevant per al públic.
Suggeriu unir-vos a la llista de correu de wikidata, llistes de correu tècniques, canal d'IRC, telegrama, etc. Interactueu amb el públic per veure què és el més adequat per a ells.
Un punt d’entrada interessant per mostrar com interactua la comunitat és mostrar la pàgina Sol·licitud de comentari.
Suggeriu un contacte amb el capítol local si n’hi ha cap al país i amb el grup d’usuaris de la comunitat wikidata.
Apunteu a la conferència anual de Wikidata, navegueu pel programa i aneu a les presentacions proporcionades (almenys diapositives, de vegades registres). Més generalment esdeveniments.
### Lectures
* [WikiData WikiProjects](https://dashboard.wikiedu.org/training/wikidata-professional/wikidata-wikiprojects)
* [Requests for comment](https://www.wikidata.org/wiki/Wikidata:Requests_for_comment)
* [WikiDataCon 2019](https://www.wikidata.org/wiki/Wikidata:WikidataCon_2019)
* [WikiData Events](https://www.wikidata.org/wiki/Wikidata:Events)
* [WikiData movement affiliates](https://meta.wikimedia.org/wiki/Wikimedia_movement_affiliates)
* [WikiData Community User Group](https://meta.wikimedia.org/wiki/Wikidata_Community_User_Group)

## Treball a casa (autònom)
Cerqueu al dipòsit obert de [Zenodo] (https://zenodo.org/) i trobeu els vostres tres articles preferits sobre FLOSS i educació. Documenta i / o publica un comentari sobre ells.
## Resum
Per acabar la sessió, l'entrenador facilitarà un moment de debriefing on els participants s'animen a expressar les seves preguntes, dubtes, idees i sentiments sobre els temes tractats.
## Referències
* [Planning](https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop).
* [Professional](https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata).
* [Help](https://www.wikidata.org/wiki/Help:Contents).
* [Tools](https://www.wikidata.org/wiki/Wikidata:Tools).
* [Educational resources](https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources).
```