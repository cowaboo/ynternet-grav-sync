---
title: 'Descobrir i comprendre Wikidata'
length: '180 min'
objectives:
    - 'Proporcionar una visió general de qué és Wikidata'
    - 'Explicar com s''emmagatzemen les dades al projecte i per què'
    - 'Descobrir la comunitat wikidata i alguns elements clau de governança comunitària'
    - 'Comprendre la interfície i crear un compte'
---

---

---
## Introducció
### Treball en grup:
El formador començarà la introducció del mòdul preguntant als participants sobre la seva experiència actual i anterior amb wikidata o qualsevol projecte wikimedia. També se'ls convidarà a compartir informació sobre el context de la seva participació a la sessió.
### Fer una consulta
L’objectiu seria ajudar-los a veure com WikiData pot afectar de forma pràactica la seva vida diària i atreure la seva atenció. Mostreu com Wikidata pot ajudar a "respondre" a preguntes que abans seria molt difícil fer en projectes específics, en domini o no de Wikimedia. Penseu en compartir alguns exemples de preguntes a les que Viquipèdia teòricament podria respondre, però que requeririen llegir una gran quantitat de pààgines de Viquipèdia (o altres fonts) per recopilar aquesta informació. Adapteu les consultes d’exemple al públic.

Es proposaran els resultats d’una o dues consultes.
1. Primera demostració
Pregunteu al públic sobre l'alcalde de la seva ciutat. És una dona? Si no ho saben, on poden trobar la informació? esmentar google, però també assistents personals com Siri
Pregunteu a les persones participants sobre les ciutats del seu país amb dones alcaldeses. On o com poden trobar la informació?
Pregunteu-los la llista de països ordenats pel nombre de ciutats amb alcaldessa. On o com poden trobar la informació?
Executeu la consulta. "llista de països ordenats pel nombre de ciutats amb alcaldessa" per demostrar l'ús de les dades allotjades a wikidata.
2. Segona demo (dirigida a un públic professional)
Executeu la llista de consultes d'articles acadèmics amb la paraula "zika" al títol.
Utilitzeu-lo per demostrar l'enllaç de WikiData amb altres bases de dades: (aquí PubMed_ID)
Mostra la reutilització de tercers a [scholia](https://tools.wmflabs.org/scholia/topic/Q202864).
## Visió general de Wikidata
### Introducció a Wikidata
Normalment, aquesta secció garanteix que els participants puguin:
    1. Comprendre els objectius de Wikidata
    2. Entendre com es relaciona Wikidata amb altres projectes de Wikimedia, especialment amb Wikipedia
    3. Comprendre per què haurien d’estar motivats per contribuir o utilitzar Wikidata, especialment en el context del seu propi treball
    4. Els elements que inclouria són:
* Conceptes clau: descripció de les característiques bàsiques de Wikidata (cc0 gratuït, col·laboratiu, multilingüe i agnòstic del llenguatge, interconnectat) i els seus objectius (dipòsit de coneixement humà, suport per a projectes de Wikimedia i tercers, hub al web de dades obertes enllaçat) estadístiques de contingut + estadístiques d'accés general.
* Com serveix Wikidata als projectes de Wikimedia i com omple importants buits en infraestructura per a la comunitat. És a dir, Els enllaços interlingüístics o que ajuden a prevenir les caixes d'informació de la Viquipèdia no estan actualitzats en els Wikis en idioma local, especialment pel coneixement que no és "local" de la llengua (és a dir, un polític d'una altra llengua / cultura). Això hauria d’incloure la història de la creació de WikiData i exemples d’impactes a Wikipedia i Wikimedia Commons (dades estructurades per a Commons).
* The WikiData community (WM DE, qui ho dirigeix, breu resum de governança).
* Per què WikiData és important per a Internet (exemples: la llicència CC0 permet que WD sigui la base de coneixement de moltes IA com Siri o Alexa; les dades d’institucions culturals i la informació cívica són més accessibles, transparents i neutrals).
 
### Consulta i restitucióó
Unes quantes preguntes amb diverses respostes per seleccionar. Es pot fer en parella. Un cop finalitzada, restitució col·lectiva i altres explicacions si cal.
## Estructura de les dades i com s’emmagatzemen les dades a WikiData
### Presentació de l'estructura de dades a WikiData
Normalment, l'objectiu d'aquesta secció garanteix que els participants puguin:
    1. Conegueu el model bàsic de dades semàntiques de Resource Description Framework (RDF) i com es compara amb el model complet de Wikidata.
    2. Comprendre diferents conceptes rellevants per editar Wikidata i com esbrinar quin contingut pertany a aquests camps, inclosos: etiqueta, descripció, declaració, propietat, qualificador i referències.
    3. Comprendre la relació entre "informació llegible per humans" (és a dir, etiquetes i descripcions) i "dades llegibles per màquina" (és a dir, Q # s i P # s) a la interfície de Wikidata.

### Elements coberts
* Vocabulari (tipus de bases de dades, identificadors, dades enllaçades, dades obertes).
* Els avantatges i desavantatges de les dades enllaçades.
* Presentació dels diferents elements de WikiData (ítems, propietats, declaracions, identificadors, qualificadors, referències i rang).
* Lectura de WikiData. Compareu una entrada de Wikipedia, una entrada de Wikidata i una entrada de Reasonator. Comprensió de la interfície de WikiData.

### Activitat 
Individualment, es demana als participants que:
* Aneu a la Q4859840. Què es ? Amb què es podria confondre?
* Ara aneu a la Q1492. En quants llocs s’utilitza la informació sobre W1492?
* Utilitzeu la barra de cerca per trobar un element de WikiData que us interessi. Exploreu les seves propietats i referències. Fixeu-vos en com s’organitza, què pot faltar, si hi ha fonts, etc. Penseu com millorar-lo.
### Consells
* Una de les millors maneres d’introduir el model de dades RDF / Triple és fora de la interfície de Wikidata. Per exemple, a partir d’enunciats que descriuen el món en llenguatge (és a dir, la Terra → punt més alt → Munt Everest) transitant-lo a enunciats amb etiquetes (és a dir, Terra (Q2) → punt més alt (P610) → Munt Everest (Q513)) i que mostren instruccions finals llegibles per màquina (Q2 → P610 → Q513). Adaptar-se al públic.
* Presentació del model de dades amb un element de qualitat (moltes presentacions utilitzen Q42 per a la broma de la cultura Geek associada a l’element; es poden trobar altres articles de qualitat a Showcase_items.

### Fonts d'informació
* https://www.wikidata.org/wiki/Wikidata:In_one_page
* https://www.wikidata.org/wiki/Wikidata:Showcase_items

## WikiData es crea mitjançant la col·laboració
### Comunitat de Wikidata
L’objectiu d’aquesta secció sol assegurar que els participants podran entendre que WikiData és principalment una construcció social.
* Qui afegeix contingut a WikiData (humans, màquines gestionades per humans, altres fonts)
* Qui defineix les regles i com. Els diferents rols de la comunitat.
* Comportament i normes socials.
* Normes editorials. El que entra i el que no. Referències.
* Avantatges de tenir un compte
Consells:
     • Si el vostre públic està interessat a contribuir a Wikidata a escala o amb finalitats professionals, és important explicar com decideix Wikidata quins són el model de dades, les propietats i els elements. Per exemple, és important explicar la dinàmica social de "qualsevol pot crear un número Q, però els números P se sotmeten a un procés de consulta comunitària estretament controlat".

### Primers passos d'edició (a l'ordinador)
Feu que els estudiants creen el seu propi compte (o planifiqueu-ho amb antelació, hi ha un límit per a la creació diària de comptes des de la mateixa IP). Afegiu un missatge a la seva pàgina de discussió, amb un enllaç a la "pàgina de la sessió d'entrenament" configurada amb antelació. Assegureu-vos de recollir tots els noms d’usuari creats durant la sessió. Suggeriments als usuaris:
1. creeu un compte. Presenteu-vos a la pàgina d’usuari.
2. Canvieu les preferències per establir l'idioma preferit.
3. Afegiu gadgets a les preferències: "Recoin", "Reasonator" i "labelLister"
4. Deixeu anar un missatge a la pàgina d'entrenament
5. Utilitzeu la barra de cerca per trobar un element de WikiData que us interessi. Comproveu-ne la finalització amb Recoin.

##  Resum i preguntes / introducció a la propera sessió
Per acabar la sessió, l'entrenador facilitarà un moment de debriefing on els participants s'animen a expressar les seves preguntes, dubtes, idees i sentiments sobre els temes tractats.
La propera sessió tractarà sobre com afegir contingut a WikiData.
## Treballa a casa (autònom)
Abans de la segona sessió, es demanarà al participant que pensi quines entrades li agradaria millorar, que cerqui informació, incloses les referències i les fonts, que es presentaran al grup al principi de la propera sessió.
## Referències
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources
```