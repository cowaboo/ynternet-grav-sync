---
title: 'Intentional communication with FLOSS'
length: '180 min'
objectives:
    - 'Map and explore the main concepts of intentional interpersonal communication'
    - 'Understand the interdependence of intentional interpersonal communication and  FLOSS culture'
    - 'Discover the ethical, legal, social, economic, and impact arguments of intentional  interpersonal communication'
    - 'For participants to design their own scenario and intentional  interpersonal communication practices'
---

## Introduction
Trainer will start the introduction to the module by asking participants about their current and previous experiences on communication practices in projects, working and family life. The main goal of this activity is to map the plethora of current communication practices and reuse it during the course activities
## The need for intentional interpersonal communication
Interpersonal communication and digital skills are fundamental to business and governance activities, as well as for the development of community networking and collaborative activities. In today’s organizations, organizational members must come up with creative solutions that come from a joint thinking which harness the combined knowledge and abilities of people with diverse perspectives. Most scientific and artistic innovations emphasize the social dimension of creativity through interactions with others, joint problem solving.

This experience of re-negotiating personal communication properties both with peers and organisations can have a leveraging effect when it comes to developing interpersonal skills and addressing, collaboratively, governance problems. A synergistic interpersonal skills experience where learning by doing becomes a pillar of skills acquisition. Interpersonal communication, self presentation and conflict management are key issues to sustainable solutions on complex problems, but also crucial elements for improving employability and motivation, while reducing turnover...

![](open%20ae1.jpg)

Communication as intentional is an act whereby one person gives to, or receives from another individual, information about his or her needs, desires, perceptions, knowledge, or affective states. Communication may be intentional or unintentional, may involve conventional or unconventional signals, may take linguistic or non-linguistic forms, and may occur through spoken or other models. Intentional communication, also called conscious communication refers to the display of communicative signals or signalling behaviour in which the sender is already aware of the effect such a display will have on his target i.e. listener. Intentional communication may be used to; get one’s point across, persuade another, to prompt an action, or just for fun.
At the time of, always on online communication this approach becomes crucial. However, intentional communication is not only positive, Wikipedia gives a list of examples to understand how to communicate in a community.

![](open%20ae2.jpg)
## Design your own intentional communication scenario (group work)
In this activity, we will use existing co-design methodologies for participants to design and propose their own intentional communication scenario.
## Homework
Study the Wikipedia: [Please bite the newbies](https://en.wikipedia.org/wiki/Wikipedia:Please_bite_the_newbies#How_to_bite_a_newbie) and [Give 'em enough rope](https://en.wikipedia.org/wiki/Wikipedia:Give_%27em_enough_rope). How is this useful, or not, to your everyday life and work?
## References
* Resource: [One community](https://www.onecommunityglobal.org/conscious-and-conscientious-communication/).
* Wikipedia: [Give 'em enough rope](https://en.wikipedia.org/wiki/Wikipedia:Give_%27em_enough_rope).
* [Livret TOUS Citoyens du Net](https://upload.wikimedia.org/wikipedia/commons/b/b4/Livret_TOUS_Citoyens_du_Net_(CdN).pdf) (CdN) in FR.