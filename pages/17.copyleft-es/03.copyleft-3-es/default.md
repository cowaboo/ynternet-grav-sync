---
title: 'Las licencias FLOSS en los escenarios de la vida real'
length: '180 min'
objectives:
    - 'Proporcionar ejemplos de licencias FLOSS en escenarios de la vida real'
    - 'Analizar el impacto del uso de las licencias FLOSS'
    - 'Conocer la conexión con el marco de la política europea de derechos de autor '
    - 'Motivar la aportación de políticas educadas en el área FLOSS'
---

## Introducción
Discusión en grupo sobre "Cómo está licenciado tu proyecto?
El formador iniciará la introducción al módulo preguntando a los participantes sobre las iniciativas de licencias a su alrededor, que provienen mayoritariamente de otras organizaciones o de la UE. Las respuestas se documentarán y se reutilizarán para personalizar el material de la formación.
## FLOSS en los escenarios de la vida real
¿Qué licencia tienen estos proyectos?
* [Farm Hack](https://farmhack.org/tools).
* [Open Motors](https://www.openmotors.co/).
* [Fold-it](https://fold.it/portal/).
* [Open Insulin Project](https://openinsulin.org/).
* [Wikihouse](https://www.wikihouse.cc/).
* Cualquier artículo de la Vikipedia.

Más licencias y conceptos clave:
* [The Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License).
* El caso de Copyfarleft *[The case of Copyfarleft](https://wiki.p2pfoundation.net/Copyfarleft)*.

## El marco de la política de la UE para las licencias FLOSS
La presentación y análisis de las principales iniciativas de política de la UE FLOSS, incluida la Licencia Pública de la Unión Europea (la "EUPL") se aplica a la obra (tal como se define a continuación), que se proporciona en los términos de esta licencia. Se prohíbe cualquier uso de la obra, que no sea autorizado por esta licencia (en la medida en que este uso esté cubierto por un derecho del titular de los derechos de autor de la obra). Compatibilidad e interoperabilidad.
[*Understanding the EUPL v1.2*](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12) 
[*EUPL or GPLv3? A comparison table of the main characteristics and differences*](https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t)

## Tu licencia para salvar el mundo 
Únete a un grupo para diseñar y compartir una propuesta de política de licencias que penséis que hará que el mundo sea un lugar mejor para vivir. Elegid un tema e intentad encontrar maneras y soluciones para licenciar productos. Utilizaremos herramientas digitales para documentar las ideas y los retos de estas licencias.
## Trabajo en casa 
Visiona el vídeo siguiente: Software libre para una sociedad libre: Richard Stallman en TEDxGeneva 2014: [*Free software, free society: Richard Stallman at TEDxGeneva 2014*](https://www.youtube.com/watch?v=Ag1AKIl_2GM) A continuación, publica en la sección de comentarios otro vídeo que explique - demuestra argumentos similares. Comparte tus pensamientos sobre el nuevo vídeo.
## Referencias
* Cómo elegir una licencia para su propio trabajo". Laboratorio de licencias y cumplimiento de software libre [*How to choose a license for your own work*](https://www.gnu.org/licenses/license-recommendations.html#libraries)
* Software libre para una sociedad libre: Richard Stallman en TEDxGeneva 2014 [*Free software, free society: Richard Stallman at TEDxGeneva 2014*](https://www.youtube.com/watch?v=Ag1AKIl_2GM).
* [The open data institute](https://theodi.org).