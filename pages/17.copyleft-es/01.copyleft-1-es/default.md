---
title: 'Copyqué? Comprensión y uso de las licencias FLOSS'
length: '180 min'
objectives:
    - 'Hacer un mapa y explorar los conceptos principales relacionados con las licencias FLOSS'
    - 'Entender la interdependencia de varias licencias FLOSS y no FLOSS'
    - 'Descubrir los argumentos éticos, legales, sociales, económicos y de impacto a favor y en contra de las licencias FLOSS'
    - 'Decidir qué licencia / plataformas / son más útiles para ellos y su proyecto y comunidad'
---

## Introducción
El formador iniciará la introducción al módulo preguntando a los participantes sobre sus experiencias actuales y anteriores sobre derechos de autor y copyleft. Las respuestas se documentarán y se reutilizarán para personalizar el material de la formación.
## La aparición del copyleft
“*Las redes constituyen la nueva morfología social de nuestras sociedades, y la difusión de la lógica en red modifica sustancialmente las operaciones y los resultados en procesos de producción, experiencia del poder y cultura*" Dice el filósofo Manuel Castells. Este proceso es a la vez una amenaza y oportunidad alrededor de "incluir los bienes de la mente" (véase: J. Boyle, [The Public Domain: Enclosing the Commons of the Mind](http: // www.thepublicdomain.org/download), New Haven, CT: Yale University Press, 2008), pp. 221.
##  Distinguir una licencia copyleft (trabajo en grupo)
En esta actividad, revisaremos los resultados de la documentación de la sesión introductoria para distinguir una licencia copyleft.
## Trabajo en casa
Los participantes estudiarán el artículo de la Wikipedia “*[Copyleft](https://en.wikipedia.org/wiki/Copyle)*” en el idioma que elijan y luego completar la sección de enlaces externos con un ejemplo propio.
## Referencias
* Una colección de recursos relacionados está disponible en espacio social - colectivo de marcadores: [Diigo - eCulture](https://groups.diigo.com/group/e_culture).
* [CNU - Licenses - Copyleft](https://www.gnu.org/licenses/copyleft.html).
* Licencia ética de código abierto *[The Ethical Open Source Licence]*(https://firstdonoharm.dev).