---
title: 'Las licencias FLOSS para la educación abierta, el arte y el aprendizaje colectivo'
length: '240 min'
objectives:
    - 'Proporcionar un análisis crítico sobre la importancia de las licencias FLOSS para la educación abierta, arte y aprendizaje colectivo'
    - 'Examinar el uso y las ventajas de las licencias FLOSS en la educación y en el arte'
    - 'Favorecer la participación activa y creativa de los estudiantes mediante licencias FLOSS'
    - 'Motivar a los participantes a adoptar licencias FLOSS en sus actividades de educación actuales y futuras'
---

## Introducción
Los participantes harán una lluvia de ideas sobre las licencias y las iniciativas de educación a su alrededor. Las respuestas se documentarán y se reutilizarán para personalizar el material de la formación.
## Las licencias FLOSS en el arte y la educación 
En 1999, la novela Q apareció con el nombre de [Luther Blissett](https://en.wikipedia.org/wiki/Luther_Blissett_(nom_de_plume)), conocido anteriormente como "the collective moniker" para un proyecto de medios de comunicación de humor italiano. Este relato alegórico de la subcultura italiana en forma de thriller histórico, situado en Italia del siglo XVI, se convirtió en un bestseller nacional número 1 y, posteriormente, apareció en traducciones francesa, alemana e inglesa. Evidentemente, las ventas no sufrieron en absoluto por el hecho de que la huella del libro permitiera copiarlo libremente con fines no comerciales. El libro no fue publicado por una editorial alternativa, sino por las editoriales bien consolidadas Einaudi en Italia, Editions du Seúl en Francia y Piper en Alemania, entre otros a los que aparentemente no les importaba utilizar  modelos tradicionales de distribución con derechos de autor para un publicación prometedora.

Otro ejemplo, de una manera diferente, es European (un depósito de educación abierta y arte). El Marco de licencias Europeana, por ejemplo, estandariza y armoniza información y prácticas relacionadas con los derechos, en virtud de un acuerdo de intercambio de datos personalizado. El DEA estructura la relación entre Europeana y sus proveedores de datos. El DEA establece que Europeana publica metadatos que recibe de sus proveedores de datos bajo los términos de la Dedicación del dominio público universal de Creative Commons Zero (CC0). Lee más información sobre CC0 y las directrices de uso de datos.

El conocimiento y la creatividad son recursos que, para ser fieles a ellos mismos, deben mantenerse libres, es decir, es una búsqueda fundamental que no está directamente relacionada con una aplicación concreta. Crear significa descubrir lo desconocido, significa inventar una realidad sin hacer caso al realismo. Así, el objeto (objetivo) de (del) arte no equivale al objeto de arte acabado y definido. Este es el objetivo básico de esta Licencia de Arte Gratuito: promover y proteger la práctica artística liberada de las reglas de la economía de mercado.
Licencia de Arte Libre, versión 1.2 [*Free Art License Preamble version 1.2*](http://www.copyleftlicense.com/licenses/free-art-license-(fal)-version-12/view.php).
## Ejemplos y actividades
1. Mira la charla de Gwenn Seemel: En defensa de la imitación [*In defense of imitation*](https://www.tedxgeneva.net/talks/gwenn-seemel-in-defense-of-imitation/).
Estudia, documenta y discute con tus compañeros y compañeras sus argumentos. Explorar como el artista está vivo. Crea un documento común con todos los participantes y compártelo con otras personas. 
(alternativa).
2. Escenarios Colectivos para OEP - Escenarios de Educación Abierta (grupo de trabajo)
La actividad se centrará en realizar una selección ejemplar de aplicaciones del paradigma en prácticas educativas abiertas.

## Trabajo en casa
Busca en el depósito abierto [Zenodo open repository](https://zenodo.org/) y encuentra tus tres artículos preferidos sobre FLOSS y educación.  Documéntalos y/o publica un comentario.
## Referencias
* Guia de licencies de contenido abierto de Lawrence Liang: [*Guide To Open Content Licenses*](https://monoskop.org/images/1/1e/Liang_Lawrence_Guide_to_Open_Content_Licenses_v1_2_2015.pdf)
* [Steal this Film and projects](http://www.stealthisfilm.com/Part2/projects.php).
* Biella Coleman, La creación social de la libertad productiva: piratería de software libre y la redefinición del trabajo, la autoría y la creatividad: [*The Social Creation of Productive Freedom: Free Software Hacking and the Redefinition of Labor, Authorship, and Creativity*](http://www.healthhacker.com/biella/proposal2.html/).
* Unglue (v. t.) 1.Pagar a un autor o editor por haber publicado un libro electrónico de Creative Commons. [unglue.it](https://unglue.it/)
* [DIS](https://dis.art/): El futuro del aprendizaje es mucho más importante que el futuro de la educación. DIS es una plataforma de streaming de entretenimiento y educación - edutainment. Incorpora un listado de artistas y pensadores líderes para ampliar el alcance de las conversaciones clave que se arrastran a través del arte contemporáneo, la cultura, el activismo, la filosofía y la tecnología. Cada vídeo propone algo - una solución, una pregunta - un modo de pensar nuestra realidad cambiante.