---
title: 'Une culture de la protection des données'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Data privacy culture: a FLOSS driven view'
lang_id: 'Data privacy culture: a FLOSS driven view'
lang_title: fr
length: '6 heures'
objectives:
    - 'Présenter le cadre légal et sensibiliser aux enjeux concernant la protection des données d’usages'
    - 'Sensibiliser à la plus-value des logiciels libres pour augmenter la protection des données personnelles d’usages'
    - 'Permettre à l’animateur de situer ses usages et le degré de respect des données personnelles dans son organisation'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté)'
    - Internet
    - Bearmer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
    - 'Connexion mobiles Slidewiki Academy'
skills:
    Keywords:
        subs:
            'Protection des données personnelles': null
            RGPD: null
            'Data privacy': null
            'Big data': null
            Surveillance: null
            Publicités: null
            Manipulation: null
            'Influencer les comportements': null
            FLOSS: null
            Libre: null
            'Open source': null
---

## Introduction
Bien que l’utilisation des données personnelles à des fins commerciales se soit implicitement généralisée, le respect de la vie privée est considéré par l’Europe comme un droit fondamental. Les logiciels libres tentent encore et toujours de résister à la surveillance généralisée « soi-disant volontaire » de nos vies privées. Pour protéger les données d’usages directement à la source, dans le design même de l’outil, la culture FLOSS tente de ne rien cacher dans le logiciel qui nous surveille (et elle le prouve en donnant accès au code source).

Ce scénario de formation encourage des pratiques conscientes des enjeux liés à la collecte des données, en présentant les bonnes pratiques de base et la législation Européenne. Notre objectif est d’inviter les apprenants à considérer la possibilité d’utiliser des logiciels libres ou open source pour augmenter la sécurité de leurs données d’usages. Nous aimerions aussi permettre à l’animateur multimédia de mieux situer les limites de sa pratique professionnelle pour favoriser le respect des données personnelles des animateurs et de leurs bénéficiaires. Globalement, le scénario de formation fournit un rappel du cadre législatif européens, et des enjeux relatifs au profilage des habitudes par la surveillance des usages personnels à des fins publicitaires de manipulation des comportements.
## Contexte
Le but de la session est d'impliquer de manière consciente et pratique les apprenants pour :
* Présenter le cadre légal et sensibiliser aux enjeux concernant la protection des données d’usages. L'apprenant sera en mesure de se situer légalement par rapport au RGPD. Il comprendra les enjeux généraux et les bonnes habitudes liés à la protection des données personnelles.
* Sensibiliser à la plus-value des logiciels libres pour augmenter la protection des données personnelles d’usages. En vue de décider consciemment quels plates-formes / outils / services sont les plus respectueux de la vie privée et de la liberté des usagers.
* Permettre à l’animateur de situer ses usages et le degré de respect des données personnelles dans son organisation. Pour l’encourager à choisir en connaissance de cause des règles et des outils dont le design « Libre » permet plus de confidentialité.

Les séances encourageront l’utilisation du logiciel libre dans l’éducation non formelle des adultes, en stimulant une participation intentionnelle à la culture FLOSS. Les participants procéderont à une analyse critique des possibilités d’utilisation des outils FLOSS dans leur quotidien. En prenant conscience de l’importance d’avoir une politique de protection des données dans leur propre organisation ; pour mieux sécuriser leurs pratiques  professionnelles en inclusion numérique.
## Séances
### Première séance : Principes de base de la Protection des données
Cette session donnera aux participants la possibilité d’explorer les principaux concepts de la législation et les enjeux. Nous lierons les concepts abordés lors de la formation à de ressources éducatives libres (via liens URL). Nous passons en revue les principes de bases et les bonnes habitudes à avoir.
_Phase 1 :_ Chacun ouvre son ordinateur portable. On visionne ensemble sur internet deux petits film d’introduction sur le RGPD avec des informations visuelles simples et accessible pour tous (jeune et adulte).
_Phase 2 :_ On travaille en sous-groupe les thèmes spécifiques qui intéressent les apprenants, toute la matière des ressources pédagogiques proposées ne doit pas être couverte. L’idée est ici de sensibiliser au enjeux et aux bonnes pratiques simples et accessibles. C’est la base des habitudes élémentaires pour augmenter la protection de ses données. Le contenu est valable pour les jeunes et les adultes, et donne aussi une introduction aux risques liés aux réseaux sociaux.
_Phase 3 :_ Discussion en groupe pour conclure la session 1. Le formateur débutera la conclusion collective au module en interrogeant les participants sur leurs expériences actuelles et antérieures en matière de protection des données. Les réponses seront documentées puis utilisées pour personnaliser le matériel de formation.
### Deuxième séance : Le logiciel libre et la protection des données privée ?
La deuxième session visera à établir l’origine historique et l’état de la technique du logiciel libre autour de la protection des données. Les pratiques FLOSS seront brièvement présentées (via l’exemple des outils framasoft et UBlock Origin). En tant qu’apprentissage collectif, réfléchir ensemble le choix de ses outils pour augmenter la protection des données d’usages dans nos activités. Expérimentation d’un outils (bloqueur de publicité UBlock Origin).
### Troisième séance : Situer et limiter sa pratique (et celle de son organisation) pour augmenter la protection des données
Les participants procéderont à une analyse critique des possibilités d’utilisation dans leur quotidien, et élaboreront un analyse initiale sur leur propre organisation pour le respect des consignes légales dans leurs pratiques d’inclusion numérique.
Ces trois sessions permettront une analyse critique du cadre légal et des enjeux. Il poursuivra en examinant l'utilisation et les avantages des technologies numériques libres dans l'éducation non-formelle. En encourageant une prise de conscience facilitant l'engagement actif et créatif des apprenants via les technologies FLOSS, à la fois comme théorie et pratique.