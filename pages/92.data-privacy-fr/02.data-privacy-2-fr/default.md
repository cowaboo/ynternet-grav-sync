---
title: 'Le logiciel libre peut-il m’aider à mieux protéger ma vie privée ?'
lenght: '2 heures'
objectives:
    - 'Fournir un éclairage sur le lien entre FLOSS et protection des données'
    - 'Comprendre la stratégie des initiatives FLOSS'
    - 'Survoler quelques outils FLOSS disponibles (framasoft et uBlock Origin)'
    - 'Motiver les personnes instruites à une pratique respectueuse de la vie privée et du libre arbitre'
---

## Introduction
Le formateur commence l'introduction du module en posant des questions aux participants sur les initiatives FLOSS qui les entourent. Les réponses seront documentées puis utilisées pour personnaliser le matériel de formation.
## Le logiciel libre et la protection des données
Introduction : FLOSS : histoire d’une culture du partage et de la protection des échanges.
L'immense majorité des défenseurs de la culture libre prennent également position pour le droit à la [vie privée](https://fr.wikipedia.org/wiki/Vie_priv%C3%A9e), l'[accès libre](https://fr.wikipedia.org/wiki/Biens_communs_informationnels) à l'information et la liberté d'expression sur Internet. Certains militants ont aussi pu commettre des actes illégaux au cours de leur combat, comme l'infraction au copyright ([The Pirate Bay](https://fr.wikipedia.org/wiki/The_Pirate_Bay), https://fr.wikipedia.org/wiki/Aaron_Swartz) ou bien la divulgation de données confidentielles qu'ils considèrent comme d'intérêt public ([Edward Snowden](https://fr.wikipedia.org/wiki/Edward_Snowden), [Alexandra Elbakyan](https://fr.wikipedia.org/wiki/Alexandra_Elbakyan) avec [Sci-Hub](https://fr.wikipedia.org/wiki/Sci-Hub)). 

Aux environs de 1960, les termes hacking et hacker sont introduits par le [MIT](https://fr.wikipedia.org/wiki/Massachusetts_Institute_of_Technology). Ils désignent le fait de bidouiller et d’expérimenter pour le plaisir. En 1969, ![John Draper](https://fr.wikipedia.org/wiki/John_Draper) parvient, à l’aide d’un sifflet qui possède la même tonalité que le réseau téléphonique américain, à passer des appels longues distances gratuitement lorsqu’il siffle dans le combiné. Cette technique est nommée, par son créateur, [phreaking](https://fr.wikipedia.org/wiki/Phreaking) et va inspirer une nouvelle vague de [hackers](https://fr.wikipedia.org/wiki/Hacker_(sous-culture)) informatiques. Ces derniers vont chercher à modifier et faire évoluer un premier ordinateur.

Ce n’est qu’en 19803 que les médias commencent à publier des articles concernant le hacking. Notamment avec [Kevin Poulsen](https://fr.wikipedia.org/wiki/Kevin_Poulsen), qui réussit à s’introduire dans un réseau réservé à l’armée, aux universités et entreprises. Il y eut également la sortie du film [Wargames](https://fr.wikipedia.org/wiki/Wargames_(film)) dont l’histoire est centrée sur un hacker qui parvient à accéder au système informatique de l’armée américaine. Le premier [virus informatique](https://fr.wikipedia.org/wiki/Virus_informatique) apparaît également dans ces années. En conséquence, les hackers sont parfois vu comme des personnes dangereuses.

De nombreux crackers ont commencé leur activité en essayant de casser les restrictions anti-copie ou en détournant les règles des jeux informatiques avant la généralisation d'Internet qui a alors ouvert de plus larges horizons à leur activité. Mais lorsque les médias ont révélé au début des [années 1990](https://fr.wikipedia.org/wiki/Ann%C3%A9es_1990) que le [Chaos Computer Club France](https://fr.wikipedia.org/wiki/Chaos_Computer_Club_France) était un faux groupe de hackers qui travaillait en collaboration avec la [gendarmerie](https://fr.wikipedia.org/wiki/Gendarmerie), la communauté de hackers français s'est plutôt détournée vers le [logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre) et de nombreuses communautés indépendantes ont vu le jour.

C’est avec la naissance d'[internet](https://fr.wikipedia.org/wiki/Internet), dans les années 19903, que l’on parle pour la première fois de [cybercriminalité](https://fr.wikipedia.org/wiki/Cybercrime). Les adeptes du domaine sont divisés au début. Il y a les [black hat](https://fr.wikipedia.org/wiki/Black_hat) qui mènent des activités criminelles et les [white hat](https://fr.wikipedia.org/wiki/White_hat) qui ne veulent pas nuire mais cherchent les [vulnérabilités informatiques](https://fr.wikipedia.org/wiki/Vuln%C3%A9rabilit%C3%A9_(informatique)) pour les rendre publiques et ainsi les réparer.

L'aspect communautaire forme un des points forts du hacking. L'organisation en communauté permet l’extension du partage d’information, les communautés étant interconnectées la propagation de l'information est très rapide. L'organisation en communauté permet l’entraide entre personnes, mais également aux personnes de jeunes âges qui souhaitent apprendre. L'interconnexion de personnes, qui ne se connaissent pas, permet une aide qui place les individus au même plan, et cela sans jugement de valeur. Cet aspect pousse à la généralisation et au partage du savoir sans que cela se fasse sur la base de critères tels que « la position, l’âge, la nationalité ou les diplômes ».

Nicolas Auray explique cet anonymat comme suit : « En livrant des traces sur un mode « anonyme », [les hackers] refuseraient de comparaître auprès des institutions politico-judiciaires, récusant la légitimité de leur verdict. Ils repousseraient ce qu’acceptent encore un peu les « désobéisseurs » civils : reconnaître la légitimité de la punition et se laisser punir »21.

Conclusion possible :
La communauté des « hackers » est liée à la création et au développement de la culture FLOSS. La priorité est donnée au partage des ressources et des informations au sein des communautés de pratiques. Mais via des méthodes d'“action direct ” pour arranger soi-même les failles du système, et en garantissant au maximum la protection des données personnelles à la source dans le « design » même des outils.
## La particularité des outils FLOSS pour la protection des données
Le logiciel libre dispose de plusieurs atouts :
* Une grande adaptation des logiciels libres à tout type de configurations matérielles et de conditions d’utilisations spécifiques « à la demande ».
* Une évolutivité permanente grâce à une communauté dynamique de développeurs professionnels passionnés.

Mais pour tout à chacun, qu’il soit sous LicenceGnuPublicLicensev3 (GPLv3), FreeSoftware (FS), ou CeCiLL; cela permet la liberté pour les utilisateurs d’exécuter, de copier, de distribuer, d’étudier, de modifier et d’améliorer le logiciel grâce à l’accès au code source.
Il permet aussi une plus grande confidentialité de nos données :
D’une part, côté développement, l’éthique suivie par la communauté de développeurs assure que le code ne contiendra pas de fonctionnalités destinées à faire passer les données par des serveurs intermédiaires de traitement marketing. D’autre part, les mesures de sécurité paramétrées sur les serveurs et les réseaux assurent la distribution des paquets IP au bon destinataire.

![](image.jpg)

## Protection des données et logiciels libres : préserver la sphère privée avec Framasoft
Les services de partage et de communication deviennent de plus en plus incontournables pour rester en contact avec nos proches, dans une société où la sphère familiale et sociale est de plus en plus délocalisée. Des services libres et bien configurés permettent de garder le contrôle sur nos échanges.

Pour faire face à ce traitement envahissant de nos données personnelles, des acteurs engagés du numérique mettent en place des alternatives libres et souvent « gratuites » de fourniture de services internet à destination de chacun.
Ainsi la communauté Framasoft, propose en téléchargement des applications faciles à installer grâce à une documentation fournie.
 Elle met à disposition des services alternatifs libres dont entre autres :
* Framadrive pour disposer d’un espace de stockage, calendrier et agenda en nuage.
* Framasphère, un serveur d’accès au réseau social Diaspora* par exemple.
* Framaestro pour fournir des services d’échanges et de partage de ressources.
* Framabee, comme alternative au moteur de recherche Google (ou ixQuick).
* Framawiki, pour la rédaction de documentations utilisant SGML.
    
## Nos recommandations pour un usage sécurisé d’internet
* Lire les contrats de confidentialité lorsque vous utilisez les services Google, Yahoo, Microsoft, pour être certain des informations privées que vous souhaitez exposer sur le marché économique.
* Accéder à des services internet qui utilisent uniquement des connexions chiffrées via SSL/TLS.
* Utiliser des services de messageries sécurisées pour l’envoi de mail (éviter SMTP).
* Utiliser un OS insensible aux Malwares, RansomWares et virus de façon intrinsèque.
* Mettre en place une protection avec un pare-feu, indépendant du système d’exploitation comme UFW par exemple, pour éviter les services non souhaités.
* Vérifier toujours l’origine de téléchargement des fichiers en utilisant des clés de chiffrement, GPG, SHA256…
* Utiliser un navigateur indépendant comme Firefox de la fondation Mozilla.
* Créer des mots de passe sophistiqués et utiliser un gestionnaire de trousseau (KDE Wallet, KeePass).
* Participer dans la mesure de vos moyens à ces alternatives libres pour pérenniser leur développement à long terme.

Source : https://blog.syloe.com/protection-des-donnees-et-logiciels-libres-nos-recommandations/
## Devoirs
Installation de uBlock Origin
Exercices pratiques
Pourquoi se protéger de la publicité ? La publicité sur le Web est utilisée pour pister l'internaute de site en site au moyen des cookies de sites tiers2, ceci en raison de la nature même du modèle économique de la publicité sur internet, dont les rétributions se font en échange des données personnelles des utilisateurs.

Pour les utilisateurs, le blocage de la publicité permet d'éviter la distraction, voire la surcharge cognitive dues à des contenus indésirables dans les pages Web consultées. Il permet également un affichage plus rapide et plus clair des pages web ainsi débarrassées des publicités, une moindre consommation de ressources (processeur, mémoire vive et bande passante) ainsi qu'une certaine protection de la vie privée par désactivation des systèmes de suivi, de trace numérique, et , et d'analyse mises en œuvre par les régies publicitaires.

Les utilisateurs de forfaits de téléphonie mobile data (données) (utilisant les normes GPRS, 3G, 4G), ainsi que les utilisateurs de l'internet fixe dans certains pays, qui paient au volume de données échangées ou ont une limitation de ce dernier, ont un intérêt financier au blocage de la publicité en ligne, notamment en bloquant les fichiers audio et vidéo, gros consommateurs de bande passante.

![](image%202.png)

uBlock Origin est une extension libre pour les navigateurs web Mozilla Firefox, Google Chrome, Opera et Microsoft Edge chargée de filtrer le contenu des pages web afin d'en bloquer certains éléments, en particulier les bannières de publicité. En plus d'être un logiciel antipub, uBlock bloque la collecte des données de navigation.

uBlock Origin a reçu des prix de sites web qui traitent de l’informatique et se trouve être faible consommateur de mémoire vive par rapport aux autres extensions aux fonctionnalités similaires. Le but d’uBlock Origin est de donner la possibilité à l’utilisateur de renforcer son choix de filtrage du contenu des pages web.

En 2016, uBlock Origin continue à être activement développé et maintenu par le fondateur et développeur principal Raymond Hill.
L'outil est intégré à la liste des logiciels libres préconisés par l’État français dans le cadre de la modernisation globale de ses systèmes d’informations (S.I.).

## Références
* [Protection des données](https://blog.syloe.com/protection-des-donnees-et-logiciels-libres-nos-recommandations/)
* [Ublock, origin](https://fr.wikipedia.org/wiki/UBlock_Origin)
* [Culture libre](https://fr.wikipedia.org/wiki/Culture_libre)
* [Hacking](https://fr.wikipedia.org/wiki/Hacking)