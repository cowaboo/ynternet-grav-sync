---
title: 'Situer sa pratique (et celle de son organisation) pour augmenter la protection des données'
lenght: '2 heures'
objectives:
    - 'Permettre une analyse critique de l''importance de la protection des données dans nos pratiques'
    - 'Examiner les limites de nos pratiques en éducation et en inclusion numérique'
    - 'Encourager les participants, et leur organisation, à s''engager de manière active et créative au travers des technologies FLOSS, à la fois en théorie et en pratique'
    - 'Motiver les participants à adopter une charte, dans leur organisation, qui précise la politique de protection des données'
---

.## Introduction Qui nous sommes ? : Situer et limiter nos pratiques
Traiter vous une de ces situations ? (Si oui comment limiter vous votre intervention ?)
* Effectuer un paiement via une appli ?
* Récupérer un mot de passe ?
* Réparer un problème matériel ?
* S’inscrire sur un réseau social ?
* Apprendre à utiliser un traitement de texte ?
* Envoyer un e-mail à la commune ?
* Installer une appli ?
* Faire une recherche de logement ?
* Remplir une déclaration d’impôts simplifiée sur « Taxe-on-Web » ?

Conclusion intermédiaire :
Faites-vous face à d’autres situation délicates pour la protection des données ?
Si oui, lesquelles et comment limiter vous votre intervention ?

Se réunir par organisation pour situer nos usages : L'activité se concentrera sur une définition du projet de notre propre organisation, pour identifier sa pratique et ses objectifs. En vue d’adapter sur le plus long terme la politique de protection des données personnelles de l’organisation.
En sous-groupe (ou individuellement si impossible) : réunir les animateurs d’une même organisation ensemble. Étude du cas pratique de sa propre organisation par les accompagnants numériques.
(à titre d’exemple de réponse, ci-dessous, les réponses pour  le projet « Informaticien public » de ARC)
### 1.- Stratégie
Quel est le but et le public du dispositif ?
* Accompagner les personnes en situation de fracture numérique profonde en allant à leur rencontre

Quel est le positionnement de l’organisation sur la confidentialité ?
* L’informaticien public répond à toutes les demandes d’usages quotidiens.
* L’expérience sera positive (l’usager ne repart pas bredouille).

### 2.- Qui traite des données personnelles et comment ?
Quel travailleur ou bénévole accompagne l’usager ?
* Un animateur expérimenté de l’EPN de l’ARC (Pleine confiance et responsabilité, Pas AS, pas médecin, pas juriste, pas technicien)
* Un bénévole

Statut de l’animateur, manière d’aborder la question de l’usager ?
* Demande verbalisée par l’usager
* Aborder la confidentialité et la vie privée (responsabilité et autonomie)
* Expliquer la démarche (ce que l’on va faire et ce que l’on va voir)
* Accord de l’usager
* Possibilité de se rétracter et charte de fonctionnement

3.Le cadre organisationnel
Quelle est la nature de l’organisation ?
Une association d’éducation permanente qui s’oppose à:
* Une politique qui crée de la pauvreté culturelle
* Une perte de pouvoir des personnes face au marché
* Une application aveugle des évolutions technologiques

Un Espace Public Numérique (EPN) labélisé par la Région de Bruxelles-Capital
Pourquoi le public vient ?
L’usager se sent démuni face à l’informatique, il ne sait pas (ou ne veut pas) suivre de formation,  il a des besoins et des questions informatiques concrets.

Chacun ouvre son ordinateur.
Description de l'activité Mise en commun et conclusion collective :
Dans le cadre de votre travail d’inclusion numérique, bénéficiez vous d’un cadre qui limite et sécurise vos interventions ?
Sources : [La protection des données à l’école](https://www.jedecide.be/sites/default/files/2018-06/La%20protection%20des%20donnees%20a%20lecole%20en%207%20etapes.pdf) en 7 etapes.pdf de l’Autorité de Protection des Données (APD).
En tant que centre de formation, comment devez-vous traiter des données à caractère personnel ?
Les principes essentiels auxquels une école doit satisfaire lors du traitement de données à caractère personnel sont les suivants :
* Traitez les données à caractère personnel pour des finalités déterminées et légitimes. Utilisez les données à caractère personnel uniquement dans ce but. Exemple : pour des raisons d’administration des élèves, une école connaît l’adresse du domicile de tous les élèves. Ce n’est pas parce qu’une école dispose des données que celles-ci peuvent être transmises à une autre école ou que l’école peut les utiliser pour diffuser une liste d’adresses aux parents.
* Soyez transparent lors du traitement des données à caractère personnel. Expliquez pourquoi l’école va traiter certaines données à caractère personnel.
* Tout traitement de données à caractère personnel n’est légitime que s’il satisfait à au moins un des fondements légaux.

Les principaux fondements légaux sur lesquels une école peut se baser sont :
* L’obligation légale : si la loi l’impose, les données à caractère personnel peuvent être traitées. Il s’agit par exemple de données administratives et d’accompagnement de l’élève.
* Le contrat : les données à caractère personnel des élèves et des enseignants peuvent être traitées si elles sont nécessaires à l’exécution d’un contrat. Par exemple : une photo d’identité d’un élève qui est demandée et qui apparaît sur une carte d’élève afin de lui permettre d’avoir accès à toutes sortes de services proposés par l’école.
* Le consentement : le consentement des élèves ou des parents des élèves de moins de 16 ans est nécessaire au traitement de données à caractère personnel pour certaines finalités. Par exemple : pour publier des photos d’élèves sur le site Internet de l’école, un consentement sera nécessaire.

* Une école ne traite pas plus de données à caractère personnel que nécessaire pour atteindre la finalité déterminée et légitime. Par exemple : lors de l’inscription d’un élève, l’école ne doit pas connaître les revenus des parents.
* Les données à caractère personnel traitées par une école doivent être exactes et pouvoir être corrigées. Par exemple : en cas de déménagement d’un élève, l’école peut adapter l’adresse.
* Ne conservez pas les données à caractère personnel plus longtemps que nécessaire. Pour certaines données d’élèves, un délai de conservation légal s’applique. Respectez le délai de conservation légal des données à caractère personnel.
* En tant qu’école, prenez des mesures appropriées afin de protéger les données à caractère personnel contre les traitements non autorisés. L’autorité scolaire est responsable du respect de ces principes et doit pouvoir le démontrer.
Exercice individuel : identifiez dans la liste ci-dessus, quels sont les éléments que votre organisation a déjà pris en compte. Identifiez les points encore à travailler.

## Devoirs
Nous vous invitons à promouvoir au sein de votre organisation, l’implémentation du Règlement Général de la Protection des Donnée.
Le PDF ci-dessus, propose une méthodologie en 7 étapes pour permettre aux organisations d’enseignement de mettre en place les procédures nécessaires.
Pour ce devoir, nous ne vous demandons pas de réaliser vous-même seul les 7 étapes, mais d’essayer d’impliquer votre organisation dans ce processus, en sensibilisant vos responsables et collègues. Et en proposant à l’ordre du jour de vos réunions de travail une manière de faire.
Sources : [La protection des données à l’école](https://www.jedecide.be/sites/default/files/2018-06/La%20protection%20des%20donnees%20a%20lecole%20en%207%20etapes.pdf) en 7 etapes.pdf de l’Autorité de Protection des Données (APD).
.
Comment s’y prendre :
ÉTAPE 1 — Informer et sensibiliser
ÉTAPE 2 — Désigner un DPO ainsi qu’un point de contact à l’école
ÉTAPE 3 — Tenir un registre des activités de traitement
ÉTAPE 4 — Contrats avec des partenaires 
ÉTAPE 5 — Contrôler si le consentement est nécessaire
ÉTAPE 6 - Sécurité physique et sécurité de l’infrastructure ICT 
  Sécurité physique 
  Sécurité ICT 
  Points d’attention concernant les données à caractère personnel
ÉTAPE 7 - Violations de données à caractère personnel et obligation de notification

## Références