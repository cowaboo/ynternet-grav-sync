---
title: 'Principes de base de la Protection des données'
length: '4 heures'
objectives:
    - 'Lier à la formation de ressources éducatives libres (REL)'
    - 'Découvrir les arguments éthiques, juridiques, sociaux, économiques et d''impact\_; pour et contre l’exploitation des données personnelles à des fins commerciales ou politiques de manipulation'
    - 'Quels sont les règles de base et les bonnes habitudes pour eux-mêmes et leur communauté'
---

## Introduction
Dans cette séance, nous allons revenir sur le cadre légal et les principaux enjeux de la protection de données ; afin pouvoir identifier les bonnes pratiques applicables dans nos usages on-line quotidiens.  
Déroulement de la séance : le formateur débutera l’introduction en présentant le programme du module et en interrogeant les participants sur leurs expériences actuelles et antérieures en matière de protections des données personnelles. Les réponses seront documentées puis utilisées pour personnaliser le matériel de formation.
## Phase 1
Chacun ouvre son ordinateur portable ou si possible projection en groupe. On visionne ensemble sur internet deux petits film d’introduction avec des informations visuelles simples et accessible pour tous (jeune et adulte).
En demandant aux apprenants de lister individuellement (par écrit pour ne pas les oublier) les informations qui leurs semblent importantes ou nouvelles pour eux.
[Un jour Une actu](https://www.1jour1actu.com/info-animee/cest-quoi-la-protection-des-donnees-personnelles/).
[Je décide](https://www.jedecide.be/les-parents-et-lenseignement).
Mise en commun par tour de table, en listant au tableau les informations importantes pour les participants. Ordonnez visuellement les informations sur un support visuel en regroupant les informations par thème.
Synthèse phase 1 : Le rôle de l'autorité de protection des données, qui découle de la réforme de la Commission de la vie privée, leur a été exposé.
### Qu’est-ce que le Règlement général sur la protection des données (RGPD) ?
Le Règlement général sur la protection des données (RGPD) est un nouveau règlement européen qui fixe des règles que les autorités publiques, les entreprises et toutes les autres organisations doivent respecter lorsqu’elles traitent des données à caractère personnel.
Le RGPD offre aux citoyens une meilleure protection lors du traitement de leurs données à caractère personnel. Cette nouvelle législation est basée sur la législation existante en matière de protection de la vie privée mais renforce un certain nombre de règles, apporte des précisions ou constitue une extension à la législation actuelle. Le nouveau Règlement est entré en vigueur le 25 mai 2018.
Les centres de formation traitent de nombreuses données à caractère personnel.
Citons par exemple les données des (anciens) élèves et de leurs parents, des enseignants et du personnel d’encadrement. Dans le cadre du traitement de données à caractère personnel, les Espaces Publics Numériques (EPN) et les centres de formation seront soumis au RGPD.
Quels sont les principaux changements ?
* Une plus grande responsabilité de celui qui traite les données. Le centre de formation devra démontrer elle-même qu’elle traite les données à caractère personnel selon les règles du RGPD.
* Les centres de formation doivent désigner un interlocuteur qui connaît cette législation et qui pourra aider à la mettre en œuvre au sein de l’école.
* Le pouvoir organisateur doit tenir un registre des activités de traitement. Un registre des activités de traitement reprend entre autres quelles données à caractère personnel sont traitées par l’école, pour quelles finalités sont-elles utilisées, d’où proviennent ces données et avec qui elles sont partagées.
* La législation prévoit une obligation de notification en cas de fuites de données. Par exemple, en cas de fuites de données à caractère personnel sensibles, vous êtes obligé, en tant qu’école, de notifier les fuites de données à l’Autorité de protection des données (et éventuellement aux personnes concernées).
* Un contrôle renforcé. En cas de non-respect du RGPD, l’Autorité de protection des données peut imposer des sanctions ainsi que des amendes.

## Phase 2
On travaille en sous-groupe les thèmes spécifiques qui intéressent les apprenants, toutes la matière, des ressources pédagogiques proposées ci-dessous, ne doit pas être couverte. L’idée est ici de sensibiliser au enjeux et aux bonnes pratiques simples et accessibles. On survole les bonnes des habitudes élémentaires pour augmenter la protection de ses données. Le contenu est valable pour les jeunes et les adultes, et donne aussi une introduction aux risques liés aux réseaux sociaux.
Exercices :
Nous proposons au groupe de choisir selon leurs intérêts, chacun pour soi,
un des 8 thèmes proposés sur la plateforme pédagogique "[Je Décide](https://www.jedecide.be/les-parents-et-lenseignement)". On laisse les personnes découvrir individuellement en ligne le contenu des thèmes proposés. On regroupe les gens qui ont choisi le même thème en sous-groupe, pour qu’ils préparent ensemble une présentation sommaire des informations pratiques importantes. Chaque groupe présente aux autres les informations clefs du thème choisi. Les sous-groupes présentent le contenu comme ils veulent (inviter les à être créatif dans la forme du partage). Demandez leur d’identifier les rôle dans le sous-groupe, chacun doit avoir un rôle lors de la présentation (par exemple : x rapporteur, x scribe, x gardien du temps, x support visuel, x recording, x présentateur,…) Peu importe le rôle, c’est au groupe de choisir et à lui de donner un rôle sur scène à chacun lors de la présentation (style mini pièce de théâtre max 3 min). C’est bien entendu aussi un moment de partage pour faire découvrir d’autre bonnes pratiques que celles présentées sur la plateforme pédagogique, l’objectif est dégager une intelligence collective des thèmes abordés, et de la partager avec les autres.
### 1.- [Jouets connectés](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys)
Jouets connectés, par exemple :
* Une poupée connectée non sécurisée qui connaît tous nos secrets.
* Un drone qui prend nos voisins en photos.
* Une montre connectée qui enregistre tous nos mouvements.

Les fêtes approchent à grand pas et la période des achats bat son plein. Vos enfants vous ont transmis leur liste de cadeaux ? L’heure des achats a sonné ! Des voitures télécommandées, des livres d’aventures, des slimes de toutes les couleurs, … vous ne savez plus où donner de la tête. Et tout un coup, vous vous trouvez nez-à-nez avec ces jouets high-tech derniers cris, appelés « jouets connectés ». Ces jouets connectés à Internet sont très attrayants et fort convoités par les enfants, mais savez-vous réellement comment ils fonctionnent, comment vos données personnelles sont traitées ainsi que celles de vos enfants, comment les utiliser ou encore comment les paramétrer pour les sécuriser ? Dans notre dossier thématique vous trouverez quelques conseils « intelligents » quant à leur utilisation.
* Drones
* Enceintes connectées
* Montre connectée
* Robot et poupée connectés
* Tablettes

### 2.- [La vie privée en ligne](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne)
La vie privée en ligne : Internet est partout, c'est "le top". Jouer, écouter de la musique, partager des photos et des vidéos, chatter ou rechercher des informations pour l'école, ... Tout cela, les jeunes le font en ligne. Ce n'est donc pas pour rien qu'on les appelle la génération numérique.
Mais malgré le fait que les jeunes grandissent avec Internet, certains aspects n'en demeurent pas moins complexes pour eux aussi. Ils ne voient dès lors pas toujours les risques que comporte Internet, surtout en ce qui concerne la vie privée ! Ne pas protéger suffisamment sa "vie numérique", c'est courir le risque que d'autres personnes, des entreprises ou même des autorités publiques accèdent à un tas d'informations personnelles. Lorsque cela arrive, Internet devient tout à coup beaucoup moins "top".
Dans ce bloc thématique, vous trouverez des astuces pour protéger votre vie privée ainsi que celle de nos jeunes sur Internet.
* Un environnement respectueux de la vie privée : comment ?
* Protégez votre profil.
* Utilisez des mots de passe forts !
* Réfléchissez avant de poster.
* Sécurisez votre connexion Internet.
* Réseaux sociaux : les avantages.
* Les réseaux sociaux : les inconvénients.
* Cyber-harcèlement.

### 3.- [La vie privée à l'école](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole)
La vie privée à l'école : Le droit à la vie privée ne s'arrête pas à la grille de l'école ! Les animateurs doivent aussi observer plusieurs règles afin de respecter la vie privée de chaque élève. Tant les parents que les enseignants se posent de nombreuses questions à ce sujet : quelles informations notre organisation peut-elle réclamer ? Combien de temps peut-elle les conserver ? Que fait l'école des photos de mon enfant ? Les élèves peuvent-ils filmer le cours et inversement, l'enseignant peut-il filmer la classe ?
Ce bloc thématique apporte une réponse à ces questions et à bien d'autres sur la vie privée de l’apprenant.
* Le dossier de l'élève.
* Photos et vidéos à l'école.
* Les caméras à l'école.
* GSM et smartphone à l'école.
* Alcool et drogue.
* Nouveaux gadgets.
* L'adresse de l'enseignant.
* Mon prof, mon ami Facebook?
* Réunion d'anciens.
* Recrutement de nouveaux élèves.

### 4.- [Photos et vidéos](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos)
Photos & vidéos : L’utilisation de toutes sortes de formes d’images évolue à la vitesse de l'éclair. Aujourd’hui, beaucoup de gens ont toujours un appareil photo en poche… leur smartphone ! Rien de plus banal désormais que de prendre des photos et de filmer et la diffusion de ces images n’a jamais été aussi simple : les réseaux sociaux et les applications visent à partager des informations et des images. Cette évolution fulgurante a pris tout le monde de court et a profondément changé notre mode de vie et notre société. Avant, la communication passait principalement par les mots. Aujourd’hui, nous sommes submergés par un flot d’images sur tout et n'importe quoi : nous-mêmes, nos enfants, notre famille, notre club de sport, notre école, notre classe.
Mais cette évolution ne constitue-t-elle pas un danger pour notre vie privée ? Peut-on refuser d’être filmé, peut-on interdire la diffusion d'images sur lesquelles on apparaît ou est-on contraint de suivre la tendance de la diffusion d’images en ligne ?
* Le principe : demandez toujours le consentement.
* Comment obtenir le consentement ?
* Images d’activités scolaires.

### 5.- [Sexting](https://www.jedecide.be/les-parents-et-lenseignement/sexting)
Sexting : Le sexting  est la pratique qui consiste à envoyer des messages à caractère sexuel par sms, via des médias sociaux ou des services téléphoniques tels que Skype. Ces messages peuvent donc contenir du texte mais aussi une photo ou une vidéo à caractère sexuel. Ce type de message à caractère sexuel s'appelle tout simplement un "sexto".
Le sexting n'est en soi pas si grave. Les jeunes aiment expérimenter, c'est le cas aussi sur le plan sexuel. Mais il faut qu'ils soient conscients que le sexting comporte aussi des risques pour la vie privée. C'est là que les choses risquent de déraper, et pas seulement chez les jeunes... des adultes aussi se font piéger.Dans ce bloc thématique, vous trouverez plus d'informations sur le sexting. Qu'est-ce que c'est ? Que faire en cas de problèmes ? Quelques astuces utiles.
* Sexting : quels sont les risques pour la vie privée ?
* Le sexting est-il punissable ?
* L'histoire de Sophie.
* Astuces pour les enseignants.
* Astuces pour les parents.
* Au secours, ma photo dénudée a été diffusée !!!
* J'ai reçu une photo dénudée, que faire ?
* Astuces pour éviter des problèmes.

### 6.- [Sharenting](https://www.jedecide.be/les-parents-et-lenseignement/sharenting)
Sharenting : Informations pour les jeunes parents et les grands-parents "connectés". Sharenting : même si ce mot vous est encore inconnu, la pratique qu'il désigne ne l'est sans doute pas. Vous vous y adonnez probablement très souvent ou peut-être juste de temps en temps. Et plus d'un s'y livre même sans aucune retenue !
Share + Parenting = Sharenting
Le sharenting, c'est tout simplement le partage de photos et de vidéos de vos enfants ou petits-enfants sur les médias sociaux, souvent sans que les intéressés y aient consenti.
Nous avons presque tous un compte sur un réseau social quelconque, que ce soit Facebook, Instagram, Snapchat, WhatsApp, ... Et tout parent aime immortaliser les événements importants de la vie de sa progéniture. Pas un jour de vacances, pas une fête scolaire, pas une compétition sportive avec nos têtes blondes ne se passe sans être conservé en images. Images qui sont ensuite allègrement partagées sur les réseaux sociaux.
* Réfléchissez avant de poster !
* Pourquoi oui, pourquoi non ?
* Conseils pour pratiquer un "sharenting" avisé.

### 7.- [Smartphones & applications](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications)
Smartphones & applications : Aujourd'hui, beaucoup de jeunes ont un smartphone ou une tablette. Envoyer des SMS, faire des vidéos, prendre des selfies, tester des applis, aller sur Facebook, jouer, surfer sur Internet … tout cela est possible et très facile. Mais parfois, il est difficile de suivre ou de comprendre tous les nouveaux progrès.
Les jeunes utilisent quotidiennement ces technologies, même à l'école. Le présent bloc thématique propose donc aux parents et aux enseignants des informations de base sur l'utilisation des smartphones et des applications ainsi que sur les conséquences pour le respect de la vie privée. Vous y trouverez aussi des astuces sur la manière d'utiliser votre smartphone et votre tablette tout en protégeant votre vie privée. De cette façon, vous pourrez informer et aider des jeunes confrontés à des questions ou à des problèmes sur ce sujet.
* Applications et informations personnelles.
* En tant que parent, puis-je contrôler le smartphone / la tablette de mes enfants ?
* Partage de la position.
* Les applis santé.
* Pour protéger davantage votre vie privée, cryptez vos messages.
* Virus et astuces.
* Astuces de protection de la vie privée pour les applications.

### 8.- [eID](https://www.jedecide.be/les-parents-et-lenseignement/eid)
eID : Dès l'âge de 12 ans, les jeunes disposent d'une carte d'identité électronique (eID) ! Mais il ne s'agit pas seulement d'une carte d'identité ! Aujourd'hui, la carte est utilisée pour tout un tas de choses : en tant que badge d'accès, de bon de garantie, … et même en tant que carte de fidélité. Cependant, il y a des règles à respecter ! Lisez ici pour quelles raisons.
* La carte d’identité électronique.
* Quand faut-il montrer l'eID ?
* L'eID comme carte de fidélité.

Ressources pédagogiques supplémentaires :
* [123 Digit](https://www.123digit.be/).
* [Support pédagogique](www.jedecide.be).

## Phase 3
Discussion en groupe pour conclure session 1 : le formateur débutera la conclusion collective au module en interrogeant les participants sur leurs expériences actuelles et antérieures en matière de protection des données. Les réponses seront documentées puis utilisées pour personnaliser le matériel de formation.
Conclusions possibles :
* Réfléchis avant de publier : ne mets pas tout et n'importe quoi en ligne.
* Le problème : les informations publiées sur Internet sont souvent difficiles à supprimer.
* Une fois que des informations se retrouvent sur Internet (comme votre avis, une photo, une vidéo, …), il est souvent très difficile de les en retirer. Pour protéger sa vie privée en ligne, mieux vaut en avoir bien conscience.

Un exemple :
une photo que vous avez postée sur Internet est téléchargée par un ami. Il existe donc à présent une copie de la photo sur le disque dur de son ordinateur. Supposons à présent que votre ami envoie cette photo à un autre ami qui la transfère ensuite à son tour encore à d'autres personnes, ... il existe alors soudain beaucoup de copies de votre photo.
C'est ainsi que vous perdez le contrôle de vos propres informations personnelles. Comment faire en sorte que toutes les copies de la photo soient supprimées ? Vous ignorez en effet à quels endroits la photo a été sauvegardée. Dans une telle situation, retirer la photo originale d'Internet n'a plus d'utilité. Cela ne vous permet pas de supprimer les copies qui ont été sauvegardées sur les appareils de tous ceux qui ont téléchargé ou reçu la photo.
Il est donc important d'expliquer qu'il y a une différence entre ce que l'on se raconte oralement entre amis et ce que l'on publie sur Internet.

La solution : réfléchissez avant de poster - posez-vous 3 questions
Avant de poster quelque chose sur Internet, posez-vous les questions suivantes:
    **1) Les informations que je mets en ligne sont-elles destinées à tout le monde ?**
Si les informations ne sont pas destinées à tout le monde, mieux vaut ne pas les publier sur Internet où elles seront visibles par tous. Utilisez les paramètres de confidentialité du site Internet pour limiter le nombre de personnes qui peuvent voir votre message ou tout simplement, ne publiez pas les informations sur Internet.

    **2) Est-ce que je ne risque pas de regretter plus tard d'avoir mis ces informations en ligne ?**
Une mauvaise réaction, une photo humoristique, ... ? Gardez à l'esprit qu'une fois que vous avez posté des informations sur Internet, il ne sera sans doute plus possible de les supprimer. Soyez prudent quand vous chattez. Des messages de chat peuvent être sauvegardés par leur destinataire ou par la société qui a conçu l'application de chat.

    **3) Les informations que je veux poster sur Internet sont-elles des informations personnelles relatives à quelqu'un d'autre ?**
Ne mettez jamais en ligne des informations personnelles telles que des photos, des vidéos, une adresse, un numéro de téléphone, ... d'autres personnes sans leur demander au préalable leur consentement. En effet, si vous publiez ces informations sur Internet, elles en perdent le contrôle. Vous n'aimeriez sans doute pas non plus que ça vous arrive. En outre, il est même interdit par la loi de partager en ligne des informations personnelles d'une autre personne sans consentement. Demandez donc toujours d'abord le consentement !

Quelqu'un a publié quelque chose sans mon consentement, que puis-je faire ?
    • Demandez à la personne en question de retirer les informations d'Internet car elles ont été publiées sans votre consentement.
    • Demandez au gestionnaire du site Internet sur lequel les informations ont été postées de les retirer au motif qu'elles ont été publiées sans votre consentement.
    • Contactez-nous en notre qualité de L'Autorité de protection des données veille.

## Devoirs
Saviez-vous que beaucoup de personnes n'utilisent pas les paramètres de confidentialité sur les médias sociaux ? C'est pourtant essentiel. Les informations publiées sur Internet parviennent en effet à beaucoup plus de monde que celles qui sont partagées hors ligne. Ne pas utiliser les paramètres de confidentialité, c'est courir le risque que des personnes inconnues à l'autre bout du monde accèdent par exemple aux photos de vos vies personnelles.
Nous vous prions d’aller vérifier quelles sont vos paramètres de sécurité sur vos outils.
Nous vous prions aussi de lister :
* Quels sont les avantages des réseaux sociaux pour vous ?
* Quels sont désavantages des réseaux sociaux pour vous ?

Réponse possible :
Pourquoi les réseaux sociaux sont-ils si attrayants ?
L'attrait d'avoir une page de profil sur un réseau social réside dans le fait que c'est une manière simple d'entrer en contact avec des amis, des connaissances, voire des groupes de personnes encore plus grands. Les réseaux sociaux permettent aussi d'un simple clic de souris de suivre les faits et gestes d'autres personnes grâce au flot d'informations que ces réseaux sociaux font déferler vers vous, via des photos, des vidéos, des mises à jour de statut, …

Une autre caractéristique qui rend les réseaux sociaux si attrayants, c'est qu'ils offrent un podium virtuel où l'on peut se placer soi-même sous les feux des projecteurs, avec le monde entier comme public potentiel. Vous voulez faire étalage d'une prestation sportive ? Une petite mise à jour de votre statut ou un simple tweet suffit : "Viens de courir 15 km en 1h30". Encore dans l'euphorie de votre brillante performance, vous guettez les messages de félicitations postés en réaction à votre message. "Hé bien, il y a quand même pas mal de mes amis qui sont impressionnés par ma prestation !".
Les avantages
Si l’on observe l’impact des médias sociaux sur les jeunes, on peut affirmer qu’en général, l’utilisation des réseaux sociaux et des médias sociaux offre certainement quelques avantages.
Ainsi, les réseaux sociaux présentent de nombreux avantages psychosociaux car ils sont une manière simple pour les jeunes :
* d’entrer et de rester en contact avec les amis et la famille ; 
* de développer leur capital social (= l’ensemble des avantages que l’on tire des relations et des interactions avec autrui) ;
* de se forger une identité propre.

Outre ces avantages psychosociaux, l’utilisation des réseaux sociaux entraîne aussi plusieurs conséquences sociocognitives et éducatives :
* le développement d’aptitudes créatives sous l’impulsion de la réalisation et du partage d’images ;
* le développement d’un esprit critique et d’une ouverture d’esprit si le réseau en ligne du jeune est suffisamment diversifié pour entrer en contact avec plusieurs opinions et perspectives différentes. Cela stimule le développement d’une ouverture vis-à-vis d’autres visions et avis ;
* le partage de connaissances.

Quels sont les inconvénients des sites de réseaux sociaux ?
Tout comme pour beaucoup d’autres choses, les réseaux sociaux doivent être utilisés de manière responsable si l’on ne veut pas s’exposer aux risques éventuels que comporte leur utilisation.
Les risques éventuels de l’utilisation de réseaux sociaux sont les suivants :
* exposition à du contenu non désiré comme des messages haineux et de la violence ;
* cyber-harcèlement ;
* perte de vie privée en raison d’une perte de contrôle sur des informations personnelles.

Étant donné que les réseaux sociaux sont construits sur la base du principe de l’autoreprésentation, impliquant que les utilisateurs eux-mêmes y révèlent des informations personnelles, on leur reproche souvent de comporter des risques pour la vie privée de leurs utilisateurs. On peut faire une distinction entre les risques pour la vie privée liés aux atteintes à l’identité et à l’image et ceux qui sont liés à ce que les publicitaires, les autorités et les services de renseignement font des informations personnelles.

Atteintes à l’identité et à l’image
Les risques pour la vie privée liés aux atteintes à l’identité et à l’image peuvent être en grande partie maîtrisés en respectant deux consignes :
* utilisez les paramètres de confidentialités disponibles sur les sites de réseaux sociaux ;
* ne mettez pas n’importe quoi en ligne. Réfléchissez à ce que vous publiez sur Internet.
Une mauvaise gestion des paramètres de confidentialité entraîne en effet un effacement de la limite entre informations privées et publiques.  Si vous ne réglez pas ces paramètres sur un niveau de sécurité suffisamment élevé, vous prenez le risque que de parfaits inconnus puissent obtenir un très large aperçu de votre vie privée. Imaginez par exemple que de telles personnes aient libre accès à vos photos de vacances, à votre liste d’amis, qu’ils sachent quels sont vos centres d’intérêt, vos hobbys, …

Hormis la gestion des paramètres de confidentialité, la publication réfléchie d’informations personnelles est aussi très importante : “Réfléchissez avant de publier !”. Les jeunes n’ont souvent pas conscience de la portée qu’ont les réseaux sociaux (ce qu’on appelle le public invisible) et du fait que les informations mises en ligne sont souvent conservées de manière permanente (tous ceux qui ont accès à leur profil peuvent télécharger leurs photos et les enregistrer ou les partager ailleurs, ce qui a pour effet que soudain, beaucoup plus de personnes ont accès à vos informations personnelles !).

Ce qui est publié sur Internet ne peut donc souvent plus être effacé ! Des images ou des commentaires souvent postés en ligne de manière irréfléchie peuvent ainsi, sortis de tout contexte, continuer à causer des problèmes d’image même encore longtemps après la publication, ce qui pourrait porter préjudice par exemple aux chances d’une personne d’obtenir un emploi sur le marché du travail. Tout le monde dit un jour des choses qu’il regrette ensuite d’avoir dites. Le problème en ligne, c’est que des publications irréfléchies ne ‘s’oublient’ souvent pas aussi simplement, comme c’est généralement le cas dans le monde réel.
Matériel pour approfondir :
* Un environnement respectueux de la vie privée : comment ?
* Protégez votre profil
* Utilisez des mots de passe forts !
* Réfléchissez avant de poster
* Sécurisez votre connexion Internet
* Réseaux sociaux : les avantages
* Les réseaux sociaux : les inconvénients
* Cyber-harcèlement
    
## Références
* [1 Jour 1 actu](https://www.1jour1actu.com/info-animee/cest-quoi-la-protection-des-donnees-personnelles/)
* [Je décide](https://www.jedecide.be/les-parents-et-lenseignement)