---
title: 'E-Learning met FLOSS tools'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: nl
length: '9 uur en 30 minuten'
objectives:
    - 'De voordelen van e-leren in het onderwijs te erkennen'
    - 'Om het belangrijkste Open Source Learning Management Systeem te verkennen: MOODLE'
    - 'Om te ontdekken en te profiteren van de Moodle-gemeenschap'
materials:
    - 'Personal computer'
    - 'Internet verbinding'
    - Moodle
skills:
    Sleutelwoorden:
        subs:
            'Levenslang leren': null
            'Leermanagementsysteem (LMS)': null
            'Asynchroon leren': null
            Moodle: null
            MOOC: null
---

# Introduction
_E-Learning maakt gebruik van interactieve technologieën en communicatie systemen om de leerervaring te verbeteren. Het heeft het potentieel om de manier waarop we lesgeven en leren over de hele weg te transformeren. Het kan de normen verhogen en de deelname aan een leven lang leren verbreden. Het kan niet in de plaats komen van leerkrachten en docenten, maar naast de bestaande methoden kan het de kwaliteit en het bereik van hun onderwijs verbeteren en de tijd die aan administratie wordt besteed, verminderen. Het kan elke leerling in staat stellen zijn of haar potentieel te bereiken en kan helpen een onderwijspersoneel op te bouwen dat in staat is te veranderen. Het maakt een echt ambitieus onderwijssysteem voor een toekomstige lerende maatschappij mogelijk._
Naar een uniforme e-Learning strategie, De eenheid e-Learningstrategie van DfES, 2003.

Het trainingsscenario biedt een inleiding op de E-Learning-methodologieën en -tools. Terwijl de E-learning tools de onderwijsomgeving herontworpen, zijn de sessies ontworpen om een overzicht te geven van de e-Learning achtergrond, de belangrijkste kenmerken en de methoden.

De training zal een aantal voordelen van online leren doorlopen. In het bijzonder zal het de moderne leerlingen in staat stellen om het potentieel van E-Learning als een instrument voor het bevorderen van online samenwerking te verkennen. In feite kan eLearning een effectief hulpmiddel zijn voor zowel de lerenden als de opvoeders, met name voor het bevorderen van online samenwerking (E-Learning-platforms). Deelnemers zullen het Moodle-leerplatform ervaren om samen te werken en te leren in forums, wiki's, woordenlijsten, databaseactiviteiten en nog veel meer. De training zal de lerenden een kritisch inzicht geven in het leerproces.

## Context
Het doel van de sessie is om de leerlingen praktisch te demonstreren en te betrekken bij de manier waarop:
* Het erkennen van de voordelen van E-Learning in het onderwijs...
* Het verkennen van het belangrijkste Open Source Learning Management Systeem: MOODLE.
* Om te ontdekken en te profiteren van de Moodle-gemeenschap.

De evolutionaire veranderingen in de onderwijstechnologie hebben het concept van onderwijzen en leren zoals we dat kennen veranderd. 
Er zijn zeker veel voordelen verbonden aan het onderwijzen/leren in een online omgeving. E-Learning kan het leren eenvoudiger, gemakkelijker en effectiever maken. Naast het aanbieden van verschillende leerstijlen is E-Learning ook een manier om snel lessen te geven. Het is ook een relevant instrument in termen van samenwerking en gemeenschapsvorming. In feite hoeft E-Learning geen individuele reis te zijn. Via functies zoals forums en live tutorials kunnen gebruikers toegang krijgen tot anderen in de leergemeenschap. Engagement met andere gebruikers bevordert de samenwerking en de teamcultuur, wat voordelen heeft die verder gaan dan de trainingsomgeving.
Aan het einde van de sessies kunnen de deelnemers gebruik maken van Moodle, een van de belangrijkste Open Source Learning Platform, dat een krachtige set van leerlinggerichte tools en collaboratieve leeromgevingen biedt die zowel het onderwijs als het leren versterken.
## Sessies
### Eerste sessie: E-Learning basics - Achtergrond en evolutie
E-Learning is het inzetten van technologie om het leren te ondersteunen en te verbeteren. Het doel van E-Learning is om mensen in staat te stellen te leren zonder fysiek een traditionele onderwijssetting bij te wonen. Om beter te begrijpen hoe E-Learning vandaag de dag ten goede komt aan zowel opvoeders als leerlingen, is het nuttig om naar het verleden te kijken. De term 'E-Learning' bestaat pas sinds 1999, toen deze voor het eerst werd gebruikt op een CBT-systeemseminar. De onderwijstechnologie-expert Elliott Maisie bedacht de term 'E-Learning' in 1999, waarmee de term voor het eerst professioneel werd gebruikt. De principes achter e-learning zijn echter door de geschiedenis heen goed gedocumenteerd, en er zijn zelfs aanwijzingen dat vroege vormen van e-learning al in de 19e eeuw bestonden. Vandaag de dag is e-learning populairder dan ooit.

De sessie geeft ook een overzicht van enkele van de technologieën die bij E-Learning betrokken zijn, zoals content authoring, LMS en TMS. In het 'informatietijdperk' wordt levenslang leren gezien als de sleutel tot het voortdurende succes van de moderne samenleving. E-Learning wordt door velen beschouwd als de beste haalbare oplossing voor het probleem van het leveren van de middelen die nodig zijn om levenslang leren te vergemakkelijken. Welke factoren hebben ertoe bijgedragen dat e-Learning vandaag de dag de populairste manier is om opleidingen te geven? Waarom E-Learning gebruiken? Deze sessie geeft een overzicht van de omvang van het potentieel en de voordelen van E-Learning.
### Tweede sessie: Klaar om te gaan E-learning! De E-Learning gebruiken om Open Education te promoten
Deze sessie geeft een breed overzicht van wat Moodle kan doen. Het zal het gebruik en de voordelen van Moodle, een van de meest populaire open source learning management systemen (LMS), onderzoeken. De training zal de basisinformatie verschaffen die nodig is voor leerlingen om aan de slag te gaan met het leerplatform en te profiteren van de online ondersteuning van de gebruikers over de hele wereld en het delen van ideeën.

De evolutionaire veranderingen in de onderwijstechnologie hebben het concept van lesgeven en leren zoals we dat kennen, veranderd. Er zijn zeker veel voordelen verbonden aan het onderwijzen/leren in een online omgeving. E-Learning kan het leren eenvoudiger, gemakkelijker en effectiever maken. Naast het aanbieden van verschillende leerstijlen is E-Learning ook een manier om snel lessen te geven. Het is ook een relevant instrument in termen van samenwerking en gemeenschapsvorming. In feite hoeft E-Learning geen individuele reis te zijn. Via functies zoals forums en live tutorials kunnen gebruikers toegang krijgen tot anderen in de leergemeenschap. Engagement met andere gebruikers bevordert de samenwerking en de teamcultuur, wat voordelen heeft die verder gaan dan de trainingsomgeving.

Aan het einde van de sessies kunnen de deelnemers gebruik maken van Moodle, een van de belangrijkste Open Source Learning Platform, dat een krachtige set van leerlinggerichte tools en collaboratieve leeromgevingen biedt die zowel het onderwijs als het leren versterken.