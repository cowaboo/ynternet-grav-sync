---
title: 'Crea la tua storia digitale con le tecnologie digitali aperte!'
objectives:
    - 'Scrivere una bozza e realizzare una storyboard'
    - 'Creare e raccogliere materiali per la storia digitale (immagini, voce, musica, suoni, testi, titoli) attraverso le tecnologie digitali aperte'
    - 'Registrare e modificare l’audio digitale'
    - 'Trovare e scaricare Musica e Suoni royalty-free'
    - 'Riconoscere il materiale con licenza libera sul web'
    - 'Condividere in sicurezza la tua storia digitale sui media'
    - 'Dar forma ad idee creative con applicazioni digitali'
    - 'Creare una storia digitale (audiovisiva)'
    - 'Condividere storie digitali'
---

## Storyboard nel Digital Storytelling
Dopo aver deciso la storia che vogliamo raccontare e aver raccolto il materiale, possiamo cominciare a scrivere una bozza e creare una storyboard, un luogo dove pianificare una storia visiva su due livelli: 1) Tempo - Cosa succede in quale ordine? e 2) Interazione - Come funzionano la voce fuori campo e la musica con le immagini o il video?
Creare uno storyboard è una linea guida, una mappa per immaginare il flusso della vostra storia e delineare le idee migliori dall'inizio alla fine. Lo Storyboard in Digital Storytelling serve solo a supportare gli studenti nella progettazione di una struttura che renda più facile la parte di editing che segue questa fase.
Uno storyboard è una rappresentazione scritta/grafica di tutti gli elementi che saranno inclusi in una storia digitale. Di solito viene creato prima che inizi il lavoro effettivo di creazione della storia digitale e viene aggiunta allo storyboard una descrizione scritta e una rappresentazione grafica degli elementi della storia, come immagini, testo, narrazione, musica, transizioni, ecc. 
Gli storyboard possono essere creati in vari modi, sia in digitale che manualmente su carta o su cartone per artisti. Se gli storyboard vengono sviluppati su un computer, è possibile utilizzare diversi programmi software, come Microsoft Word, Excel e PowerPoint.
## Selezione ed uso di immagini, animazioni e suoni per la tua storia digitale
La selezione e l’utilizzo di immagini, animazioni e suoni quando si crea una storia digitale è fondamentale.  
Quando si creano progetti di media digitali, è molto importante registrare la narrazione di base, modificare l'audio per regolare il volume, rimuovere i suoni indesiderati, dissolvenza audio in entrata o in uscita, ecc. Anche il diritto d'autore gioca un ruolo importante nella narrazione digitale. I narratori digitali devono essere consapevoli del tipo di immagini, suoni e altri media che possono utilizzare senza violare i diritti d'autore altrui. Molti siti web, ad esempio, forniscono musica sotto licenza Creative Commons dove i musicisti regalano la loro musica per determinati scopi non commerciali o rendono la loro musica di pubblico dominio dove può essere usata senza alcuna restrizione. 

Lo scopo di questa sessione è supportare gli studenti nell’utilizzo di software open source (iMovie, Da Vinci Resolve etc.) e strumenti per la produzione di storie digitali personali.
Gli argomenti principali della sessione saranno i seguenti:
* Scrittura e storyboarding - Audience Ownership ed autorizzazione.
* Ruolo del suono (registra una voce fuori campo/aggiungi musica).
* Ruolo della grafica/ Digitalizza i tuoi media.
* Montaggio di storie e condivisione di film.

## Homework
Utilizzando lo script creato nella sessione precedente, strutturate la vostra storia in modo tale che la successiva ricerca di immagini sia più facile: utilizzate uno storyboard. Dipingete le immagini nelle caselle dello storyboard o scrivete i termini di ricerca delle immagini nelle caselle. Lo storyboard serve a strutturare la storia ed è uno strumento per creare il primo collegamento tra testo e immagine.

Cerca immagini. Il numero totale di immagini dovrebbe corrispondere agli storyboard dei partecipanti e alla durata delle sceneggiature. Selezionare le immagini utilizzabili da collezioni personali e da fonti Internet. Modificare le immagini selezionate in Gimp (ritaglio, sfocatura e ombra).

Preparate uno script  finale, pronto per la registrazione, e uno storyboard finale. Andate su https://www.audacityteam.org/ e installate il software. Registrate la vostra voce e salvate il file.

Salvate ed esportate film e file utilizzando MovieMaker o simili.
## Riferimenti
* [Educational uses of digital storytelling](http://digitalstorytelling.coe.uh.edu/page.cfm?id=23&cid=23&sublinkid=37).
* [Digital Story Telling Guide](https://uow.libguides.com/ld.php?content_id=42727880).
* [Digital storytelling in 10 steps](https://www.socialbrite.org/2010/07/15/digital-storytelling-a-tutorial-in-10-easy-steps/).
* [Brights MOOC](http://www.brights-project.eu/en/results/brights-mooc/).
* [Brights Digital Storytelling](https://vimeo.com/showcase/4856060).
* [Digital Storytelling Toolkit](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf).
* [My future in quarantine](https://vimeo.com/107569681).
* [How to realize a storyboard](http://www.brights-project.eu/en/results/brights-mooc/).
* [Storyboard Template](https://www.the-flying-animator.com/storyboard-template.html).
* [Storyboard That](https://www.storyboardthat.com/).
* [Collection and production of materials for digital stories](https://mooc.cti.gr/mod/lesson/view.php?id=512&pageid=821).
* [Licensing consideration](https://creativecommons.org/share-your-work/licensing-considerations/).
* [Copyright and Digital Storytelling](http://digitalstorytelling.coe.uh.edu/archive/copyright.html).