---
title: 'Un’introduzione al DS: background e vantaggi'
objectives:
    - 'Scoprire la storia, caratteristiche e fasi del DS'
    - 'Esplorare il potenziale del DS come metodo per raccontare storie utilizzando le tecnologie digitali aperte'
    - 'Riconoscere cosa è e cosa non è una storia digitale'
    - 'Potenziare le competenze del 21esimo secolo'
---

## Ice breaker: Storytelling creativo
Nella parte introduttiva del modulo l’insegnante chiederà ai partecipanti di estrarre una carta Dixit a caso e di raccontarne la storia.  Ai partecipanti verrà poi chiesto di lavorare in piccoli gruppi e di pescare un’altra carta. L'obiettivo del gruppo è quello di creare collettivamente una storia usando la loro carta Dixit.  Le risposte saranno documentate e poi riutilizzate per personalizzare il materiale didattico.
## L’arte dello storytelling digitale: Storia e contesto
“La narrazione digitale è la manifestazione dell'antica arte della narrazione [...]. Utilizza i media digitali per creare storie visive da mostrare, condividere e, nel processo, conservare come storia. L'impatto delle storie digitali deriva dall'intreccio di immagini, musica, narrazione e voce per dare profondità e dimensione alla storia. Usando Internet e altre forme di distribuzione digitale, le storie digitali possono essere viste aldilà di distanze e confini per creare nuove comunità attraverso un senso di significato condiviso (Marsha Rossiter e Penny A. Garcia, University of Wisconsin, USA 2008). 
Il movimento del DS è stato avviato da Joe Lambert e da Dana Atchley, che erano interessati a mettere a disposizione della "gente comune" la produzione mediatica.  Dagli anni '90, Lambert e i suoi colleghi del Center for Digital Storytelling (Berkley, CA) hanno continuato a sperimentare con il Digital Storytelling Workshop.
In Europa il metodo fu introdotto formalmente nel 2003, quando il servizio regionale BBC Wales ha avviato un proprio programma e un centro basato sulle esperienze di altri corsi di digital storytelling tenuti in varie località.
## Cosa rende il DS unico
Il processo del DS è basato sulle seguenti fasi:  
* Find it – Trova la storia che per te è importante
* Tell it – Raccontala con le tue parole e fotografie
* Share it – Condividila con gli altri, o con chiunque attraverso internet.

Inoltre, quando si produce una storia digitale, i creatori devono pensare allo scopo del progetto, e agire quindi con un obiettivo. Uno dei maggiori punti di forza del DS è la capacità di sviluppare il pensiero critico e le capacità decisionali degli studenti. Acquisire l'abilità di raccontare storie di vita ci aiuta a sentirci più a nostro agio con la nostra vita e ad avere un impatto sulla vita di chi ci circonda. Lo storytelling incoraggia la scrittura creativa, il pensiero creativo e la risoluzione dei problemi. Quindi, il DS può fornire ai partecipanti l'opportunità di conoscere meglio se stessi e gli altri. Può anche essere cruciale per la condivisione e la collaborazione, poiché permette agli utenti di lavorare in gruppo, imparando gli uni dagli altri.
Il Digital Storytelling porta grandi vantaggi. Questa è la ragione per cui la metodologia DS è utilizzata in vari contesti, dall’istruzione (formale e non formale) al business, scopo individuale, sviluppo personale e miglioramento delle capacità creative. 
La creazione di  cortometraggi combinando immagini al computer, testi, narrazioni audio registrate, videoclip e musica per presentare informazioni su vari argomenti è fondamentale anche in termini di empowerment. Infatti, il DS può essere un potente strumento tecnologico per potenziare le competenze tecnologiche e la digital fluency.
## Homework
Scegli l’argomento della storia e rispondi alle seguenti domande:
* Qual è la parte più importante della storia? Perché?
* La storia è comprensibile?
* Quanto è lunga la storia?  

Poi, struttura la storia rispondendo alle seguenti domande:
* Chi sono i personaggi della storia? (Who)?
* Di cosa parla la storia? (Cosa)?
* Dove è ambientata la storia? (Where)?
* Quando ha luogo la storia? (When)?
* Qual è il punto di partenza della storia? (Why)?

Prepara una prima stesura della storia, circa 250 parole. Gli studenti saranno poi invitati a leggere le loro sceneggiature al gruppo durante la lezione. Insieme al copione della storia, trovate e portate con voi fotografie di voi stessi o qualsiasi immagine, lettera, documento che ritenete possa illustrare la vostra storia - Se userete fotografie di amici o familiari, è meglio ottenere prima il loro permesso.
## Riferimenti
* [Wikipedia](https://en.wikipedia.org/wiki/Digital_storytelling).
* [English and Digital Literacies](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf).
* [The history of digital storytelling](https://digitalistortenetmeseles.hu/en/history/).
* [Introduction to Digital Storytelling](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf).
* [StoryCenter: What’s your Story?](https://www.storycenter.org/about).
* [Digital Storytelling Toolkit](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf).
* [Learning through Arts and Technology](Learning through Arts and Technology).