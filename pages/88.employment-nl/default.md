---
title: 'FLOSS-middelen voor werkgelegenheid'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: nl
materials:
    - PC
    - 'Internet verbinding'
    - Projector
    - 'Papier en schrijfgerief'
    - Flipchart
    - Geluidsinstallatie
skills:
    Sleutelwoorden:
        subs:
            'Algoritmen en inzetbaarheid op de arbeidsmarkt': null
            'Curriculum Vitae': null
            Aanwerving: null
            Vaardigheden: null
            Competenties: null
            'Online mogelijkheden': null
            Netwerken: null
            'Online Ondernemerschap': null
            Donatiecultuur: null
            Wikinomics: null
            'Online campagnes': null
            Fondsenwerving: null
            Crowdsourcing: null
            Massasamenwerking: null
objectives:
    - 'Zorgen voor achtergrondinformatie over de FLOSS-vaardigheden voor de werkgelegenheid'
    - 'Zorgen voor achtergrondinformatie over de methoden van Human Resources'
    - 'Erken de voordelen en beperkingen van algoritmen in HR'
    - 'Begrijp open algoritmen als een nieuw paradigma'
    - 'Het gebruik van algoritmen in het rekruteringsproces begrijpen'
    - 'Begrijpen hoe je een CV moet aanpassen om een betere acceptatie van algoritmes te hebben'
    - 'Leer hoe je een goed online CV, portfolio of een social media profiel kunt maken'
    - 'Leer hoe u een goed werkprofiel online kunt maken en bijwerken'
    - 'Begrijpen hoe belangrijk het is om deel uit te maken van een online gemeenschap om nieuwe economische kansen aan te grijpen'
    - 'Begrijpen hoe de markt verandert en hoe online kansen ondernemerschap en personal branding kunnen faciliteren'
    - 'Leer nieuwe manieren om ondernemend te zijn met FLOSS-praktijken en -hulpmiddelen'
    - 'Weet hoe je een crowdfunding campagne online kunt voeren'
    - 'Begrijpen van verschillende economische modellen zoals de donatiecultuur en de wikinomics, onder andere'
    - 'Weten hoe ze hun persoonlijke communicatie-, samenwerkings- en brandingvaardigheden kunnen verbeteren'
length: '15 uren'
---

## Introductie
75 procent van de sollicitaties wordt afgewezen voordat ze door mensenogen worden gezien. Voordat je cv in de handen van een levend persoon komt, moet het vaak eerst worden opgehaald met een zogenaamd systeem voor het volgen van sollicitanten. Een Applicant Tracking System - of kortweg ATS - is een soort software die door recruiters en werkgevers tijdens het aannameproces wordt gebruikt om de sollicitaties die zij ontvangen voor hun openstaande functies te verzamelen, sorteren, scannen en rangschikken. Het kiezen van de juiste persoon voor een functie kan een uitdaging zijn. Algoritmen zijn voor een deel onze meningen ingebed in een vorm van een computercode. Algoritmen houden rekening met het simpele feit dat het aannemen van personeel in wezen een voorspellingsprobleem is.

Wanneer een manager cv's van sollicitanten doorleest, probeert hij/zij te voorspellen welke sollicitanten goed zullen presteren in de functie en welke niet. Statistische algoritmen zijn gebouwd voor het voorspellen van problemen/kosten en kunnen nuttig zijn bij het verbeteren van de menselijke besluitvorming. Algoritmen kunnen ook een donkere kant hebben omdat ze menselijke vooroordelen en vooroordelen weerspiegelen die leiden tot machinale leerfouten en misinterpretaties (waardoor discriminatie bij de aanwerving wordt bestendigd en versterkt). De vraag is nu niet of we algoritmen moeten gebruiken voor het selecteren van sollicitanten, maar hoe we het beste uit de machines kunnen halen. De basisprincipes van het cv-schrijven blijven generaties lang constant, maar evoluerende technologieën betekenen dat meer aspecten van het sollicitatie- en rekruteringsproces online plaatsvinden dan ooit tevoren.

Door op de hoogte te blijven van de huidige best practices, zullen de deelnemers beter voorbereid zijn om te solliciteren op vacatures. In het internet zijn de mogelijkheden om inkomsten te genereren eindeloos. We moeten een niche identificeren, een businessplan ontwikkelen en hard werken om te slagen. Er zijn tal van bedrijven die een internetondernemer kan ontwikkelen, het businessmodel definieert een internetondernemer niet.

Elke organisatie en elk project heeft inkomstenbronnen nodig om activiteiten uit te voeren, of de activiteiten nu face-to-face of online zullen zijn. In deze module richten we ons ook op de donatiecultuur, wikinomics, fondsenwerving en andere economische modellen die de kern van web 2.0 vormen.
## Context
Het doel van de sessie is om de leerlingen praktisch te demonstreren en te betrekken bij de manier waarop:
* Zorgen voor achtergrondinformatie over de FLOSS-vaardigheden voor de werkgelegenheid
* Zorgen voor achtergrondinformatie over de methoden van Human Resources
* Erken de voordelen en beperkingen van algoritmen in HR
* Begrijp open algoritmen als een nieuw paradigma
* Het gebruik van algoritmen in het rekruteringsproces begrijpen
* Begrijpen hoe je een CV moet aanpassen om een betere acceptatie van algoritmes te hebben
* Leer hoe je een goed online CV, portfolio of een social media profiel kunt maken
* Leer hoe u een goed werkprofiel online kunt maken en bijwerken
* Begrijpen hoe belangrijk het is om deel uit te maken van een online gemeenschap om nieuwe economische kansen aan te grijpen
* Begrijpen hoe de markt verandert en hoe online kansen ondernemerschap en personal branding kunnen faciliteren
* Leer nieuwe manieren om ondernemend te zijn met FLOSS-praktijken en -hulpmiddelen
* Weet hoe je een crowdfunding campagne online kunt voeren
* Begrijpen van verschillende economische modellen zoals de donatiecultuur en de wikinomics, onder andere
* Weten hoe ze hun persoonlijke communicatie-, samenwerkings- en brandingvaardigheden kunnen verbeteren

## Sessies
### Eerste sessie: Algoritmen en inzetbaarheid op de arbeidsmarkt
Het doel van de sessie is om de concepten van algoritmen en inzetbaarheid op de arbeidsmarkt te presenteren en hoe aan te passen aan een nieuwe HR-wereld waar werkzoekenden niet alleen indruk moeten maken op een recruiter, maar ook aantrekkelijk moeten zijn voor de machines die ze gebruiken. Deze sessie zal discussies en debatten genereren over het probleem dat HR-algoritmes opwerpen en ook over de oplossingen die ze aanreiken. Het hoofddoel van deze sessie is om de deelnemers open te stellen voor het idee dat de rekruteringsalgoritmes zullen blijven groeien en dat we ons beter moeten aanpassen aan deze algoritmes om succesvol te zijn.
### Tweede sessie: Hoe gebruik je FLOSS-praktijken en hoe maak je een aantrekkelijk CV voor algoritmen?
De focus van deze sessie zal liggen op hoe je een CV online kunt maken. Deelnemers worden uitgenodigd om hun eigen CV te maken met behulp van digitale platformen en tools. Ook zullen de deelnemers ontdekken hoe ze hun competenties en vaardigheden kunnen presenteren bij het maken van een goed online CV dat aantrekkelijk is voor zowel mensen als machines.
### Derde sessie: FLOSS Opportunities and Online Entrepreneurship
In deze sessie krijgen de deelnemers de kans om vele andere mogelijkheden te ontdekken die de digitale wereld creëert voor het ontwikkelen van een carrière en projectideeën. De belangrijkste focus van deze sessie zal liggen op online ondernemerschap en hoe de digitale wereld kansen creëert om geld in te zamelen. Verschillende economische modellen zullen worden verkend, zoals crowdfunding, de donatiecultuur en het Freemium model.