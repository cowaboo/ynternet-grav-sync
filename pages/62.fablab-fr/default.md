---
title: 'Comment exécuter un Fablab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: fr
Materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté à Internet)'
    - 'Connexion Internet'
    - Beamer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
length: '10 heures'
objectives:
    - 'Identifiez ce qu''est un FabLab et la culture qui y est impliquée'
    - 'Être capable de démarrer une initiative FabLab, comprendre quelles sont les premières étapes et quelles technologies sont les plus appropriées pour démarrer'
    - 'Détecter certaines technologies et outils ouverts qui conviennent à un environnement FabLab. Savoir, détecter et savoir utiliser des sources d''informations qui fournissent des ressources utiles dans le cadre d''action d''un fablab'
    - 'Explorez les communautés de pratique du Fablab qui utilisent des technologies et des outils ouverts'
    - 'Explorer le territoire pour faire le lien entre entreprise - université - citoyenneté - administrations publiques (promouvoir la quadruple hélice)'
    - 'Permettre au participant de fournir des informations, des ressources et des modèles aux communautés de pratique des Fablabs. Conception ouverte'
    - 'Accompagner les participants d''un FabLab dans le processus d''apprentissage et de production'
skills:
    'Mots clés':
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            HackLab: null
            'Conception ouverte': null
            'Matériel ouvert': null
            'Logiciel ouvert': null
            'Faites-le vous-même (bricolage)': null
            'Apprendre en faisant (LBD)': null
            PBL: null
---

## Introduction
La culture Fablab est étroitement liée à la culture DIY et Maker, ainsi qu'à FLOSS; c'est toujours quelque chose de «en construction» et pour démarrer un Fablab, vous n'avez pas besoin d'avoir beaucoup d'outils ou de machines, mais la bonne attitude. Un Fablab est un concept, et démarrer un Fablab, c'est travailler selon une philosophie concrète.
Ce module présentera aux participants le concept de ce qui est considéré comme un Fablab et le type d'activités qui peuvent y être effectuées.
## Context
Quand on imagine un Fablab, on pense à un espace de création, à la collaboration, aux technologies ouvertes, en profitant des ressources du territoire le plus proche mais aussi des sources d'information et des ressources Internet. Un espace en croissance continue, avec quelques technologies qui peuvent évoluer avec le temps. Un espace de concours de personnes qui arriveront avec une attitude ouverte et curieuse. Un point de départ qui, s'il est inséré et lié à la communauté locale, attirera différents types de publics et sera riche en créativité et favorisera l'autonomie et l'apprentissage individuel et individualisé des personnes.

Un Fablab est bien plus qu'un espace équipé pour la fabrication numérique à petite échelle. C'est un concept, c'est un point de vue, c'est une nouvelle façon de générer de la richesse et de l'autonomisation au sein d'une communauté. Equipé de technologies et d'outils gratuits, ouverts et gratuits, le Fablab est un espace capable de générer une dynamique étroitement liée à l'apprentissage collaboratif, au partage et à la culture commune et au libre échange de ressources et d'informations. Dans le cadre d'un Fablab, surtout s'il a une orientation communautaire, de nouvelles dynamiques productives collaboratives sont générées; cela fait partie du "Do it Yourself" (DiY) et de Maker Culture.

La création d'un Fablab ne nécessite pas de ressources financières importantes. Commencer est la première étape et dans ce module, nous analyserons les premières étapes et nous planifierons, ensemble, les premières étapes d'un Fablab.
## Séances
### Première séance : Qu'est-ce qu'est un FabLab ?
Fabrication diitale, collaboration, apprendre par le "Faites-le vous-même".
### Deuxième séance : Design communautaire et collaboration
Reconnaitre le potentiel de construire un FabLab dans une communauté. Besoins personnels et communautaires face à la production en masse.
### Troisième séance : Technologies d'un FabLab
Explorer le potentiel de l'utilisation des "matériels ouverts" et des "logiciels ouverts" dans le cadre d'un FabLab. Découvrir les meilleures technologies pour commencer une initiative de FabLab.
### Quatrième séance : Technologies d'un FabLab
### Cinquième séance : Premiers pas...
Créer votre propre FabLab. Design collaboratif d'une communauté FabLab.
### Sixième séance : Revision du design de la communauté d'un FabLab
Premières activités à développer.