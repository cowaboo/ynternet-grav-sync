---
title: 'De FLOSS-fundamenten: motivaties, ideologie en praktijk'
objectives:
    - 'Om de belangrijkste concepten achter de FLOSS-cultuur in kaart te brengen en te verkennen'
    - 'De onderlinge afhankelijkheid van FLOSS en Open Educational Resources (Open Educational Resources) te begrijpen'
    - 'Om ze te koppelen aan de vorming van Open Educational Resources (Open Educational Resources)'
    - 'Om de ethische, juridische, sociale, economische en impactargumenten voor en tegen FLOSS te ontdekken'
    - 'Om te beslissen welke platformen/tools/diensten het meest nuttig zijn voor zichzelf en hun gemeenschap'
---

## Groepsdiscussie
De trainer start de introductie van de module door de deelnemers te vragen naar hun huidige en eerdere ervaringen met de FLOSS-cultuur. De antwoorden worden gedocumenteerd en vervolgens.
## De FLOSS-cultuur
FLOSS zal worden beschreven als een conceptueel fenomeen op basis van twee basiselementen: gemeenschap en modulariteit. Deze elementen worden in een parallelle modus geplaatst waardoor initiatieven dynamisch kunnen worden gerealiseerd naar gelang een derde element: de noodzaak om over te stappen op een commonsgeoriënteerde productie.
Een samenhangend overzicht van FLOSS als sociaal-economisch continuüm gekoppeld aan: ideologie, technologie en de noodzaak van een ingrijpende verandering van productie naar commons georiënteerd systeem zal worden onderzocht.
## FLOSS-argumenten (groepswerk)
In deze activiteit zullen we de documentatieresultaten van de introductiesessie opnieuw bekijken om argumenten (voor en tegen) te vinden en te ontwikkelen ivm het gebruik van FLOSS in ons dagelijks leven. 
## Huiswerk
De deelnemers zouden moeten luisteren naar de “Techno-capitalism’s moral and intellectual calamities” podcast en een comment posten in de commentaarsectie.
Als de deelnemers een andere bron of materiaal vinden, moeten ze dat in [diigo](https://www.diigo.com/) aan de groep toevoegen.
## Referenties
* Een verzameling van gerelateerde bronnen is beschikbaar op een collectieve - sociale bookmarking ruimte : https://groups.diigo.com/group/e_culture 
* Benkler, Y. (2006).The Wealth of Networks: How Social Production Transforms Markets and Freedom. Yale University Press.
* Salus, Peter H. (28 maart 2005). "A History of Free and Open Source". Groklaw. Retrieved 2015-06-22.