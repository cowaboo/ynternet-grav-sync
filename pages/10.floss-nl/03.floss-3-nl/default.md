---
title: 'FLOSS als collectief leren, FLOSS in collectief leren'
objectives:
    - 'Een kritische analyse te geven van het belang van FLOSS en Open Educational Resources op het gebied van non-formele training. Dit zal nog wel even doorgaan'
    - 'Het gebruik en de voordelen van open digitale technologieën in het onderwijs te onderzoeken'
    - 'De actieve en creatieve betrokkenheid van de lerenden te bevorderen door middel van FLOSS-technologieën, zowel in theorie als in de praktijk'
    - 'De deelnemers te motiveren om FLOSS in hun onderwijsactiviteiten op te nemen'
---

## Groepsdiscussie: Inleiding tot de sessie (thema: collectieve intelligentie)
De deelnemers zullen brainstormen over collectieve intelligentie-initiatieven om hen heen. De antwoorden worden gedocumenteerd en vervolgens hergebruikt om het trainingsmateriaal op maat te maken.
## FLOSS-cultuur en collectieve intelligentie
De kloof tussen de conventionele wijsheid over de organisatie van de kennisproductie en de empirische realiteit van de collectieve intelligentie die in "peer production" wordt geproduceerd in projecten zoals Wikipedia, heeft het onderzoek naar fundamentele sociaal-wetenschappelijke vragen over organisatie en motivatie van collegiaal handelen, samenwerking en gedistribueerde kennisproductie gemotiveerd. 
Wikipedia laat bijvoorbeeld zien hoe deelnemers aan veel collectieve inlichtingensystemen waardevolle middelen bijdragen zonder de hiërarchische bureaucratieën of sterke leiderschapsstructuren die staatsagentschappen of bedrijven gemeen hebben en dat dit kan zonder duidelijke financiële stimulansen of beloningen.
## FLOSS en Open Onderwijs
FLOSS gaat hand in hand met een bepaald idee van onderwijs. Een organische kijk op lesgeven en leren - en een nog meer organische kijk op hoe ideeën zich verspreiden. Het proces is niet als een ingenieur die een bouwwerk bouwt volgens specificaties; het is meer als een boer of tuinman die zich bezighoudt met planten, waardoor een omgeving ontstaat waarin de planten tot bloei komen.
## Collectieve scenario's voor OEP - Open Educatiescenario's
De activiteit zal zich richten op een voorbeeldige selectie van toepassingen van het paradigma van open onderwijspraktijken.
## Huiswerk
Zoek in de [Zenodo](https://zenodo.org/) open repository en vind je drie favoriete papers over FLOSS en onderwijs. Documenteer en/of plaats er een commentaar op.
## Referenties
* https://unglue.it/ unglue (v. t.) 1. 1. een auteur of uitgever betalen voor het uitgeven van een Creative Commons ebook.
* https://dis.art/ De toekomst van het leren is veel belangrijker dan de toekomst van het onderwijs. DIS is een streaming platform voor entertainment en onderwijs - edutainment. We schakelen vooraanstaande kunstenaars en denkers in om het bereik van belangrijke gesprekken te vergroten door middel van hedendaagse kunst, cultuur, activisme, filosofie en technologie. Elke video stelt iets voor - een oplossing, een vraag - een manier om na te denken over onze veranderende realiteit.
