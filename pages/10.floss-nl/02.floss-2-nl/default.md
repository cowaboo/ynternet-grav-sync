---
title: 'FLOSS is overal'
objectives:
    - 'Om een state of the art workshop rond FLOSS te bieden'
    - 'Het analyseren van de FLOSS-initiatieven in de EU-landen'
    - 'Om het FLOSS beleidskader in de EU te ontdekken'
    - 'Het motiveren van een educatieve beleidsbijdrage op het gebied van FLOSS'
---

## Groepsdiscussie
De trainer begint de introductie van de module door de deelnemers te vragen naar de FLOSS-initiatieven om hen heen - meestal afkomstig van andere organisaties of de EU. De antwoorden worden gedocumenteerd en vervolgens hergebruikt om het trainingsmateriaal aan te passen.
## FLOSS in de EU en elders (60min)
De FLOSS-cultuur zal worden gekoppeld aan Commons georiënteerde voorbeelden in de volgende gebieden:
* Landbouw
* Productie
* Geneeskunde en gezondheid
* Woningbouw
* Kringloopeconomie
* Stedelijke ontwikkeling
* Waterbeheer
* Crypto-programmering
* Rampenbestrijding
* Kennis
* Communicatie
* IT-infrastructuur

## Een EU-beleidskader voor FLOSS
Presentatie en analyse van de belangrijkste FLOSS-beleidsinitiatieven van de EU, waaronder:
* [Open source software strategie](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#softwarestrategy).
* [Open data beleid](http://data.europa.eu/euodp/en/data/).
* [Open standaarden](https://ec.europa.eu/digital-single-market/en/open-standards).
* [De openbare vergunning van de Europese Unie (de "EUPL")](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12).

## Uw beleidskader voor FLOSS
Sluit je aan bij een groep om uw beleidsvoorstel voor FLOSS te ontwerpen en te delen, zowel op nationaal als op EU-niveau. Wat moeten de mensen doen?
## Huiswerk
Wikificeer uw beleidskader. Gebruik een platform naar keuze om uw beleidsideeën over FLOSS te delen en te bespreken.
## Referenties
FLOSS als Commons. David Bollier, Floss Road Map, 2010
http://www.bollier.org/floss-roadmap-2010-0
