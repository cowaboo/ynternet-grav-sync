---
title: 'Praktijkgemeenschap (Commons + collaboratief beheer)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: nl
length: '9 uren'
objectives:
    - 'De leerlingen praktisch te demonstreren en te betrekken bij het verkennen van de goede praktijken van het beheer van CoP'
    - 'Praktisch te demonstreren en leerlingen te betrekken bij het verkennen van de manier waarop CoP''s aanwezig zijn in de dagelijkse acties'
    - 'Kennis te maken met de instrumenten die de CoP''s gebruiken en de daaraan gekoppelde commonsbeheerpraktijken'
materials:
    - 'PC (Smartphone of Tablet)'
    - Internetverbinding
    - Projector
    - 'Papier en schrijfgerief'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Sleutelwoorden:
        subs:
            FLOSS-beweging: null
            FLOSS-cultuur: null
            Praktijkgemeenschappen: null
            Commons: null
            Samenwerkingsmanagement: null
            Burgeremancipatie: null
            'Betrokkenheid van de Gemeenschap': null
            'FLOSS-instrumenten en -vaardigheden': null
---

## Introductie
In deze module verkennen we Communities of Practice (CoP) als groepen mensen die een ambacht of een beroep delen. Veel van de theorie achter het concept is ontwikkeld door onderwijs theoreticus Etienne Wenger, terwijl een belangrijk voordeel voor een CoP het vastleggen van stilzwijgende kennis is. De motivatie om deel te nemen aan een CoP kan bestaan uit tastbare opbrengsten (promotie, verhogingen of bonussen), immateriële opbrengsten (reputatie, eigenwaarde) en gemeenschapsbelang (uitwisseling van praktijkgerelateerde kennis, interactie). Communities of practice zijn belangrijk om een commons-cultuur te initiëren en op te bouwen. Deze module is bedoeld om ons te laten zien, hoe dit al gebeurt. We zullen dit gebied koppelen aan een presentatie van de Commons als over de algemene filosofie en praktijk en de rol van collaboratief management als een belangrijk element verkennen.
## Context
Het doel van de sessie is om de leerlingen praktisch te demonstreren en te betrekken bij het verkennen van: 
* De goede praktijken van het beheer van CoP.
* Hoe CoP's aanwezig zijn in dagelijkse acties.
* De tools die CoP's gebruiken en de daaraan gekoppelde commons managementpraktijken.

## Sessies
### Eerste sessie: Definiëren van praktijkgemeenschappen (CoP)
Deze sessie zal CoP definiëren en beschrijven en hun relatie met Commons verkennen. Het zal een achtergrond en begrip geven van de grenzen en uitdagingen van het initiëren en uitspitten van CoP's.
### Tweede sessie: (Digitaal) Commons
De tweede sessie zal zich richten op een kritische denkwijze rond (digitale) commons en commons - gebaseerde managementmethoden.
### Derde sessie: Leven met CoP
Deze sessie zal de deelnemers de mogelijkheid bieden om specifieke initiatieven te bestuderen en hoe CoP's daarin een rol spelen, om de instrumenten te leren kennen die CoP's gebruiken en om de sociale contracten achter CoP's te begrijpen.