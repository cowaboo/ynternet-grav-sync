---
title: 'Comment Arduino révolutionne-t-il le monde de l''électronique?'
objectives:
    - 'Découvrir le fond d''Arduino et son histoire, et son lien étroit avec l''Open Source en général'
    - 'Examiner les possibilités offertes par Arduino en termes d''amélioration de la vie'
---

## Discussion de groupe
Pendant une trentaine de minutes, les participants seront répartis en groupes de deux, et il leur sera demandé, sans utiliser internet au départ, de nommer tout ce qui est proche ou éloigné d'Arduino.
Ensuite, le formateur expliquera en quoi consiste Arduino et quels sont les domaines d'application.
## Découvrez le monde d'Arduino
### Qui a inventé l'Arduino?
Le projet Arduino a été lancé à l'Interaction Design Institute Ivrea (IDII) à Ivrea, en Italie. L'objectif du projet était de créer des outils simples et peu coûteux pour créer des projets numériques par des non-ingénieurs, car il était difficile pour les étudiants de s'offrir des cartes mères coûteuses.Le projet de départ s'appelait Wiring, et c'était une plate-forme de développement. La plate-forme de câblage se composait d'une carte de circuit imprimé (PCB) avec un microcontrôleur ATmega168 (il s'agit d'un microcontrôleur qui dispose d'un stockage mémoire pour lancer des programmes), d'un IDE basé sur des fonctions de traitement et de bibliothèque pour programmer facilement le microcontrôleur.

Après quelques mois, les étudiants italiens ont décidé de renommer le projet Wiring by Arduino. Pas à cause du nom rappelant le matériel pour Arduino, mais à cause de la barre où les étudiants avaient l'habitude de se rencontrer pour parler du projet eux-mêmes.

L'équipe principale d'Arduino initiale était composée de Massimo Banzi, David Cuartielles, Tom Igoe, Gianluca Martino et David Mellis.
### Pourquoi Arduino?
Grâce à son expérience utilisateur simple et accessible, Arduino a été utilisé dans des milliers de projets et d'applications différents. Le logiciel Arduino est facile à utiliser pour les débutants, mais suffisamment flexible pour les utilisateurs avancés. Il fonctionne sur Mac, Windows et Linux. Les enseignants et les étudiants l'utilisent pour construire des instruments scientifiques à faible coût. De plus, Arduino a une communauté qui s'améliore dans le monde entier avec des sites multiples comme Instructables ou Arduino lui-même.Le logiciel et la carte sont mis à jour très souvent, et la communauté est nombreuse et assez active.

C'est très bon marché: les cartes Arduino sont relativement peu coûteuses par rapport aux autres plates-formes de microcontrôleurs. Vous pouvez également acheter un kit pour moins de 50 €. Cela contient de multiples addons comme des capteurs, des boutons et quelques chipsets.

C’est une multiplateforme: le logiciel Arduino (IDE) fonctionne sur les systèmes d’exploitation Windows, Macintosh OSX et Linux. La plupart des systèmes de microcontrôleur sont limités à Windows.
C'est facile à utiliser: le langage de programmation est le plus simple, et il suffit de télécharger l'EDI pour commencer la programmation. Vous pouvez également étendre le langage de programmation au C ++ et ajouter plusieurs librairies.
### Qu'est-ce qu'une carte Arduino?
Les cartes Arduino sont ainsi composées d'une carte mère avec un processeur et une mémoire vive. C'est un petit ordinateur capable d'utiliser une variété de microprocesseurs et de contrôleurs. Les cartes sont équipées d'ensembles de broches d'entrée / sortie (E / S) numériques et analogiques qui peuvent être interfacées avec diverses cartes d'extension («blindages») ou cartes de test (pour le prototypage) et d'autres circuits. Les cartes comportent des interfaces de communication série, y compris Universal Serial Bus (USB) sur certains modèles, qui sont également utilisées pour charger des programmes à partir d'ordinateurs personnels.

La carte Arduino de base s'appelle l'Arduino Uno, mais il existe un tas d'autres cartes Arduino, telles que l'Arduino Mega, qui offre un processeur beaucoup plus puissant, ainsi que plus d'entrées et de sorties numériques et analogiques. Il y a le Arudino nano, qui pour presque la même puissance du microcontrôleur, est beaucoup plus petit en taille. Arduino Mini, Arduino Due, Arduino ADK Android, autant de cartes Arduino qui peuvent répondre aux besoins spécifiques de chaque projet.

A noter également qu'Arduino étant un matériel et logiciel open-source, de nombreuses autres cartes similaires de qualités différentes sont apparues sur le marché, et il est donc très facile de les obtenir à des prix inférieurs, comme pour la marque Elegoo, Seeedstudio, Sparkfun, Watterott. Ces autres marques, qui collaborent parfois avec Arduino, peuvent proposer des modules spécifiques, comme une carte avec wifi intégré, une carte spécialement conçue pour communiquer avec un serveur web, etc.

https://www.arduino.cc/
## Devoirs
Les participants auront comme devoir d'aller sur Internet et chercher un projet qui leur plaira, et en apprendront davantage sur le code ainsi que les connexions.
Ils devront expliquer le projet et comment il pourrait être utile dans leur vie de tous les jours, ainsi que les modifications qu'ils pourraient apporter pour rendre le projet plus adapté à leurs demandes.
## Références
* [Arduino](https://www.arduino.cc/)
* [Regardez cette vidéo](https://www.youtube.com/watch?v=ydgLQCQMBuUScratch)
* [Wikipédia](https://fr.wikipedia.org/wiki/Arduino)
