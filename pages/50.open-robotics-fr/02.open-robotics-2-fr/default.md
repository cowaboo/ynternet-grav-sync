---
title: 'Apprendre la logique de la programmation'
objectives:
    - 'Découvrir les bénéfices de la programmation pour le développement des compétences du 21e siècle'
    - 'Concevoir un programme'
    - 'Pour apprendre de manière autonome à partir de tutoriels et de vidéos'
    - 'Pour accéder à presque n''importe quel langage de programmation'
---

## Discussion de groupe sur «Coder avec Arduino?»
Le formateur commencera l'introduction du module en interrogeant les participants sur leur expérience du langage de programmation.
## Vulgarisons la programmation
«La maîtrise du numérique nécessite non seulement la capacité de discuter, de naviguer et d'interagir, mais aussi la capacité de concevoir, créer et inventer avec de nouveaux médias» (…). La capacité de programmer offre des avantages importants. En particulier, la programmation prend en charge la «pensée computationnelle», vous aidant à apprendre d'importantes stratégies de résolution de problèmes et de conception qui ne se retrouvent dans aucun domaine de programmation.
Présentation et analyse des avantages de la programmation.

Il est essentiel d'apprendre la programmation et de comparer ses concepts à des situations réelles. Le formateur doit vulgariser ces concepts autant que possible, comme pour les activités débranchées.

Un exemple pourrait être le sandwich au robot: l'enseignant joue le robot et les participants ont 10 minutes pour dire au robot comment faire un sandwich au chocolat en écrivant toutes les instructions sur papier. Il est important que le robot fasse exactement ce que dit l'instruction et qu'il ne se trompe pas. Le but est de montrer à quels points les lignes de code doivent être précises, complètes et dans le bon ordre.

Ce qui peut également être fait est de représenter graphiquement un concept avec un exemple. Prenons l'exemple de la banque pour illustrer la condition "si ... alors". Les participants sont invités à expliquer, en utilisant la condition «si ... alors», comment un guichet automatique nous distribue de l'argent. Le formateur fera l'exercice avec les participants, puis les participants devront trouver un exemple par eux-mêmes. Voici comment procéder: Insérez la carte de crédit. Si la carte est insérée, allez à l'écran d'accueil et choisissez le montant. S'il y a suffisamment d'argent sur le compte, passez à l'étape de vérification du code. Si le solde n'est pas suffisant, décliner et revenir en arrière pour sélectionner le montant, etc …
![](arduino%201.png)

[Learn to code, code to learn](https://www.youtube.com/watch?v=Ok6LbV6bqaE)
[Why is coding so important](https://www.forbes.com/sites/quora/2018/08/16/why-is-coding-so-important/#3866fc362adc)
[What is computational thinking?](https://www.youtube.com/watch?time_continue=1&v=sxUJKn6TJOI)
## Codons!
Maintenant que nous avons popularisé la programmation, le formateur devra expliquer les fonctions de base et les boucles, ainsi que la syntaxe propre à Arduino. Le langage de base d'Arduino est en C.
Regardons un exemple où nous essayons d'allumer une LED
![](arduino2.png)
![](arduino3.jpg)

### Constantes
Écriture en programmation: const int
Parlons maintenant des constantes. Partons du principe qu'un ordinateur ne peut pas interpréter les phrases. Hormis le vocabulaire précis mentionné ci-dessus, tout le reste ne sera compris que si une valeur est attribuée. Par exemple, si nous connectons une LED à la broche numérique 2, nous devrons déclarer que LED = 2, afin que notre machine puisse comprendre que la broche 2 est associée à la LED. Ces procédures sont également obligatoires afin que nous puissions comprendre nous-mêmes notre code.
  Nous écrivons le nom des constantes en majuscules (cela fait partie des conventions de programmation) pour que vous sachiez qu'il s'agit de constantes. Le nom de notre constante (LED) n'est pas écrit en bleu car ce n'est pas une constante réservée à l'IDE mais à nous.
HIGH, LOW et OUTPUT sont des constantes.
![](arduino4.png)
### Variables
Écriture en programmation: int
Les variables ressemblent à des constantes, mais la valeur ne restera pas fixe. Lorsque vous déclarez une variable, le but est de pouvoir modifier la valeur à votre guise. Par exemple, si entre chaque ligne de code, vous souhaitez mettre des temps de pause variables, vous pouvez intégrer une variable nommée pauseTime. Dans un premier temps, il faut déclarer la variable, mais il faut aussi lui donner une valeur de base, par exemple 0.
Pour les variables, il existe une manière agréable, lisible et surtout conventionnelle de les nommer: vous écrivez le nom tout attaché et mettez en majuscule chaque mot sauf le premier. Voici un exemple de nom de variable:
Prenons l'exemple de la LED. Si nous voulons augmenter l'intervalle de temps entre l'activation et la désactivation, il sera beaucoup plus facile d'utiliser une variable au lieu de tout écrire l'une après l'autre.
![](arduino%205.png)
Notez que même si la variable est déclarée avec le terme int (ce qui signifie intégrer), il existe tout un tas d'autres termes en fonction de la valeur que vous souhaitez affecter. Voici les plus importants:
### Les fonctions
Écriture en programmation: int… {}
![](arduino6.png)
Les fonctions sont des morceaux de code qui sont utilisés si souvent qu'ils sont encapsulés dans certains mots-clés afin que vous puissiez les utiliser plus facilement. Maintenant, comparons notre fonction au jeune enfant pris plutôt comme exemple. Nous allons lui apprendre à faire des toasts au chocolat. Nous essaierons de détailler le plus possible car il n'a jamais appris à le faire. Voici un exemple:
- Sortez le pot de chocolat du placard
- Placez le pot de chocolat sur la table
- Ouvrez le pot de chocolat
- Prendre un couteau dans un tiroir
- Prenez une tranche de pain du sac de pain
- Trempez le couteau dans le chocolat
- Retirez le couteau du pot
- Etaler le chocolat sur la tranche
On peut appeler cette fonction: préparer un toast au chocolat.
![](arduino7.png)
La configuration du vide et la boucle du vide sont toutes deux considérées comme des fonctions.
### Syntaxe en programmation
Pour bien comprendre la synthax, rien de mieux que d'analyser un exemple. Commençons par l'un des programmes les plus simples, qui consiste à afficher sur le moniteur le message "Hello World". Le moniteur est utilisé pour communiquer des informations sur le programme; il est situé dans le coin supérieur droit du logiciel.
![](arduino8.png)
### Double barre oblique et étoile barre oblique (// ou / *… * /)
Ils sont utilisés pour rédiger des commentaires. Il est très important, même pour un programmeur régulier, de décrire tout ce que vous faites. Cela rend le code lisible pour les autres, mais aussi pour soi-même. Tout ce qui est écrit après une double barre oblique n'affectera pas le code. Les doubles barres obliques vous permettent d'écrire un commentaire sur une ligne, mais si vous souhaitez écrire un commentaire sur plusieurs lignes ou même rendre une partie du code inactive sans le supprimer, il vous suffit d'ouvrir le commentaire avec un / * et de le fermer une */.
### Supports et accolades
Les crochets et les accolades sont primordiaux dans la programmation. Après chaque fonction, une accolade à droite puis une accolade à gauche seront ajoutées. Dans ces deux accolades, nous mettrons nos instructions.
Les parenthèses sont placées après chaque ligne de code avant les accolades ou le point-virgule. Dans ces parenthèses, nous mettrons des valeurs qui formeront les paramètres. Les paramètres sont séparés par des virgules s'il y en a plusieurs.
Par exemple, dans Serial.begin (qui correspond à afficher le moniteur sur le logiciel Arduino), il est écrit 9600. Cela signifie que le taux de rafraîchissement est de 9600 images par seconde.
Juste en bas, dans la ligne de code Serial.print, nous mettrons le message que nous voulons afficher entre guillemets entre crochets.
Conditions
La condition est un test que le programme effectuera en se posant une question. S'il peut répondre «vrai», il exécute un bloc d'action (qui sera placé entre accolades), sinon il ne l'exécute pas.
Voici le pseudo code correspondant:
démarrage du programme
si (test)
{
 	ce code s'exécute si nous pouvons répondre vrai au test.
}
le programme continue

Pour illustrer l'exemple, reprenons le programme "Hello World", ajoutons une condition d'affichage.
![](arduino9.png)
Vous remarquerez que l'instruction theSerial.println () affiche le texte entre guillemets et revient à la ligne. Modifiez le programme à l'aide de l'instruction theSerial.print () en supprimant «ln» à la fin. Vous verrez que les mots collent ensemble. Nous retenons donc que:
Serial.println (): affichage avec saut de ligne
Serial.print (): s'affiche sans saut de ligne
On remarque que pour tester une valeur dans une condition on met un double égal: ==

Code  -  Tested condition
==   Equal
>=   Equal or Greater
<=   Equal or Less
>   Greater
<   Lesser
!=   Different (not Equal)

L'erreur d'un débutant est de confondre le = qui vous permet d'attribuer une valeur à une variable ou une constante, et le == qui vous permet de tester l'égalité.
### La boucle "for"
La programmation nous permet de faire des boucles dans un programme. Autrement dit, nous pouvons exécuter un bloc de code plusieurs fois avant de passer au suivant. Cette répétition ouvre un grand nombre de possibilités.
La boucle for permet à un code d'être répété un nombre de fois connu à l'avance.
C'est un peu comme une condition: ça commence par un mot-clé (for), suivi de quelques éléments entre parenthèses, puis des accolades qui contiennent le code à exécuter. Voici un exemple:
pour (int t = 0; t <10; t = t + 1)
{
	code exécutable
}
Le plus difficile à comprendre se trouve entre les crochets. Il existe trois informations importantes séparées par des points-virgules:
Initialisation de la variable qui servira de compteur. Dans notre exemple, nous déclarons et initialisons (en même temps) une variable t de type "int": int t = 0.
Condition à tester pour continuer ou non la répétition. Dans ce cas, le code est exécuté tant que t est inférieur à 10: t <10.
Action sur le compteur à chaque tour de boucle. Dans notre cas, t est augmenté de 1 à chaque passage: t = t + 1.

[Tutoriel pour Arduino](https://create.arduino.cc/projecthub)
[Premiers pas avec Arduino](https://www.arduino.cc/en/Guide/HomePage)
[Youtube officiel d'Arduino](https://www.youtube.com/user/arduinoteam)
[Tutoriel Youtube Arduino](https://www.youtube.com/watch?v=kLd_JyvKV4Y)
## Devoirs
Les participants sont invités à effectuer les tâches suivantes:
Prenons l'exemple de «Hello World» ou celui des LED
Essayer de modifier les paramètres et voir ce qui se passe
Sur la base des concepts que nous avons observés ensemble, essayez d'étendre le code en ajoutant une corrélation entre les deux exemples vus.
## Références
* [Tutorial for Arduino](https://create.arduino.cc/projecthub)
* [Getting started with Arduino](https://www.arduino.cc/en/Guide/HomePage)
* [Arduino official Youtube](https://www.youtube.com/user/arduinoteam)
* [Youtube Arduino tutorial](https://www.youtube.com/watch?v=kLd_JyvKV4Y)
* [OpenClassroom (French)](https://openclassrooms.com/fr/courses/2778161-programmez-vos-premiers-montages-avec-arduino)