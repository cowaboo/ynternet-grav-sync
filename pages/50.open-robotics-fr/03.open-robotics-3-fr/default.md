---
title: 'Réalisation de projets Arduino par auto-formation'
objectives:
    - 'Identifier les avantages de la communauté en ligne Arduino'
    - 'Encourager le partage, la réutilisation et la combinaison de code'
    - 'Favoriser l’apprentissage collaboratif des apprenants'
---

## Rejoignez et profitez de la communauté Arduino
La dernière partie sera la partie projet. Le but est que les participants puissent construire un projet par eux-mêmes en utilisant Instructables ou TinkerCad.
Instructables est un site communautaire qui rassemble plus de projets sur Arduino que sur tout autre site. Tapez simplement Instructables dans google et sur la loupe, tapez le projet que vous souhaitez créer. Les instructions sont extrêmement claires et le code est généralement expliqué en détail. Il s'agit de la plus grande communauté de partage de connaissances en termes d'Arduino, Raspberry Pi, etc …
https://www.instructables.com/

TinkerCad est un logiciel de modélisation 3D de base, mais il y a quelque temps, ils ont intégré un onglet Arduino supplémentaire, qui nous montre le schéma des connexions de manière extrêmement précise. De plus, pour chaque module d'extension, le code est déjà complètement fourni. Les participants n'auront qu'à faire les connexions, copier / coller le code et éventuellement mélanger les codes pour corréler plusieurs modules. TinkerCad est sûrement l'un des meilleurs outils disponibles pour démarrer sur Arduino.
Vous devez vous connecter à TinkerCad en créant un compte, et en cliquant sur l'onglet "Circuits".
https://www.tinkercad.com/circuits

Il est bien entendu également possible d'utiliser l'onglet "Exemples" de l'IDE Arduino.
## Devoirs
Le devoir sera de prendre l'un des projets, et d'atteindre l'objectif de la première session: pouvoir améliorer un projet trouvé sur Internet dans le but de changer la vie de son propre environnement. Ces derniers devoirs feront partie du dernier exercice pour faire des participants des bricoleurs autodidactes!
## Références
* [Instructables](https://www.instructables.com/)
* [TinkerCad Circuits](https://www.tinkercad.com/circuits)
* [Arduino](https://create.arduino.cc/)