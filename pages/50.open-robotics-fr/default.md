---
title: 'Arduino: Robotique avec des instruments ouverts'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open robotics with Arduino'
lang_id: 'Open robotics with Arduino'
lang_title: fr
length: '10 heures'
objectives:
    - 'Devenez complètement autonome, apprenez en autodidacte'
    - 'L''utilisation d''une version en ligne, ainsi que l''utilisation de sites communautaires, est nécessaire pour partager des informations. Ainsi, en plus de la formation, les formateurs eux-mêmes pourront aider la communauté. Les formateurs apprendront eux-mêmes à utiliser des projets pour les modifier à leur guise'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - 'Kit Arduino'
skills:
    'Mots clés':
        subs:
            Arduino: null
            Open-source: null
            Logiciel: null
            Matériel: null
            Projets: null
            Programmation: null
            Codage: null
            'Faites-le vous-même': null
            Science: null
---

## Introduction

## Contexte
Le but de la session est de démontrer de façon pratique aux étudiants, en les impliquant, tels que:
* Devenez complètement autonome, apprenez en autodidacte.
* L'utilisation d'une version en ligne, ainsi que l'utilisation de sites communautaires, est nécessaire pour partager des informations. Ainsi, en plus de la formation, les formateurs eux-mêmes pourront aider la communauté. Les formateurs apprendront eux-mêmes à utiliser des projets pour les modifier à leur guise.

À la fin des séances, les participants pourront créer, planifier et partager du matériel. Apprendre à utiliser Arduino peut aider les utilisateurs à sortir des sentiers battus, à raisonner systématiquement et à travailler en collaboration.
## Séances
### Première séance : Comment Arduino révolutionne-t-il le monde de l'électronique?
Arduino est facilement accessible à tous les publics, des enfants aux adultes. Son langage de programmation simplifié, sa carte mère abordable et sa communauté solide offrent un haut degré de convivialité.
Arduino vous permet de sortir du monde de la "fracture numérique", qui comprend par exemple l'utilisation d'outils électroniques sans savoir comment ils ont été construits ou programmés.
Il est donc possible de recréer un thermomètre, un capteur de mouvement, etc. Au moindre toucher, personnalisé. Arduino est basé sur des projets partagés à grande échelle par la communauté.
Tout d'abord, au début de la séance, l'enseignant demandera aux participants s'ils connaissent Arduino, et si oui, il leur sera alors demandé de fournir plus d'éléments.
L'enseignant demandera ensuite aux participants, après avoir expliqué en profondeur ce qu'est l'Arduino, comment ils pourraient faciliter la vie de tous les jours avec de petits projets.
À la fin de la séance, les participants devraient être en mesure d'identifier une alternative Arduino aux appareils électroniques existants, ils devraient également avoir une courte liste de projets qu'ils aimeraient réaliser.
### Deuxième séance : Apprendre la logique de la programmation
Lorsque vous souhaitez commencer la programmation, il est souvent difficile de comprendre la logique de la programmation. Tous les termes comme si ... alors, les boucles, les intégrations, sont des termes très complexes qui sont intrinsèques à chaque langue. Il est donc important de penser à un moyen facile et accessible d'entrer dans le monde compliqué de la programmation. Pour ce faire, des exemples pratiques et des situations réelles doivent être diffusés autant que possible.
L'objectif de la séance sera d'amener chaque participant à présenter un exemple de concepts de programmation (si .. alors), en le comparant à une situation réelle.
### Troisième séance : Réaliser des projets Arduino par auto-formation
Il est essentiel que les participants apprennent comment découvrir pourquoi même des centaines d'heures de cours ne suffisent pas pour explorer et connaître Arduino.
Au cours de la session, les participants apprendront à utiliser les projets proposés par la communauté sur les sites. Chaque groupe choisira ensuite le plus approprié parmi les projets recherchés.
L'objectif de la séance est de rendre les participants capables de mener à bien un projet en utilisant un site fiable et bien documenté, ainsi que de pouvoir modifier les paramètres, ajouter ou supprimer des modules pour aller plus loin dans l'empirisme et l'auto-apprentissage.