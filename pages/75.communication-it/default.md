---
title: 'Comunicazione interpersonale intenzionale con FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: it
objectives:
    - 'Buone pratiche della comunicazione intenzionale interpersonale'
    - 'Come la cultura FLOSS interagisce con le regole dei social media e l’uso comune delle informazioni'
    - 'Casi di comunicazione intenzionale online, come la community online di Wikipedia'
materials:
    - 'Personal computer (Smartphone o tablet connesso ad Internet)'
    - 'Connessione Internet'
    - Proiettore
    - 'Carta e penna'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    'Parole chiave':
        subs:
            'Movimento FLOSS': null
            'Cultura FLOSS': null
            Netizenship: null
            'Comunicazione intenzionale': null
            'Empowerment civico': null
            'Coinvolgimento della comunità': null
            'Strumenti e capacità FLOSS': null
length: '10 ore'
---

## Introduzione
La tecnologia digitale ha cambiato tutto. Ricordi la tua prima visione del web? Hai letto pagine di informazioni, come se leggessi un libro. Hai esaminato i messaggi così come viene elaborata la posta. E gradualmente, ti sei reso conto che non avevi a che fare con un mezzo di comunicazione come gli altri. Singolo utente di un ufficio postale o di un negozio virtuale, ti sei sentito gradualmente diventare un attore… Dal libro [Citoyens du Net](http://www.ynternet.org/page/livre).
  
Nel 2019, il web ha compiuto 30 anni. Con le sue centinaia di milioni di forum, wiki, blog, social network, microblog, ora è legittimo parlare di websfera. Google, Facebook, Twitter e Wikipedia sono i pianeti più famosi. Popolato da centinaia di miliardi di articoli provenienti da settori diversi (titolo, corpo del messaggio, allegati, immagini, numero di visitatori, note), il web è il cuore dove convergono tutti gli strumenti digitali che hanno un'interfaccia aperta, un accesso aperto standardizzato, ed è accessibile da tutti gli strumenti esistenti - smartphone, tablet, computer e strumenti connessi. Gli spazi più interessanti sono quelli in cui è possibile interagire commentando, modificando, aggiungendo testo o immagini. La comunicazione intenzionale per l'empowerment civico e il coinvolgimento della comunità nel FLOSS rappresenta una valida alternativa.
## Contesto
L’obiettivo della sessione è quello di coinvolgere agli studenti e dare una dimostrazione pratica di come:
* Utilizzare ed integrare media diversi (testo, immagini, suoni, video) in un ambiente online.
* Utilizzare e le tecnologie digitali per creare e condividere storie.
* Produrre e condividere una storia digitale per stimolare la personalizzazione e l’impegno attivo degli studenti.
* Esplorare metodi di collaborazione grazie ad attività in piccoli gruppi per imparare gli uni dagli altri.

L’obiettivo di questo corso formativo è quello di avvicinare i partecipanti al DS e far loro apprendere come utilizzarlo come metodo per raccontare storie, utilizzando tecnologie digitali aperte.  
Alla fine del corso i partecipanti saranno capaci di creare una storyboard della propria storia digitale, creare e raccogliere materiale utile per la stessa (immagini, voce, musica, suoni, testi, titoli) attraverso tecnologie aperte digitali.
I corsi hanno anche lo scopo di far acquisire una maggiore comprensione degli orientamenti etici, copyright, licenza Creative Commons ed autorizzazioni.
## Sessioni
### Prima sessione: Comunicazione interpersonale intenzionale  
Questa sessione mostrerà  l’utilizzo delle tecnologie digitali libere e aperte nella comunicazione intenzionale, nella vita di tutti i giorni. Renderà possibile per gli studenti realizzare e sviluppare le loro capacità di comunicazione ed usare le tecnologie aperte per sviluppare pratiche e abitudini di comunicazione intenzionale  
### Seconda sessione: Il metodo FLOSS: come le persone utilizzano le pratiche e gli strumenti FLOSS nei loro progetti
La seconda sessione sarà caratterizzata da un approccio critico alla comunicazione intenzionale in vari ambienti sociali e culturali. Esplorerà i metodi di collaborazione online in attività in piccoli gruppi per imparare gli uni dagli altri e sostenere una cultura della comunicazione intenzionale.
### Terza sessione: FLOSS nella propria vita
Questa sessione permetterà ai partecipanti di sviluppare e condividere le loro preferenze di comunicazione intenzionale. Analizzeremo la comunicazione intenzionale come base della partecipazione sinergica (con l'esempio della collaborazione nel prendere appunti) e la collegheremo agli strumenti FLOSS che possono servire ai partecipanti per le attività in corso.