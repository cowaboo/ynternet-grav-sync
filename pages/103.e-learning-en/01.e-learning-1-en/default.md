---
title: 'E-Learning basics – background and evolution'
length: '210 min'
objectives:
    - 'Identify the historical perspectives and rise of E-Learning'
    - 'Discover the main E-Learning tools'
    - 'Explore the potential of the different E-Learning Programs'
    - 'Evaluate the pros and cons of the different E-Learning Programs'
    - 'Discover the benefit of E-Learning to meet a wide array of learning needs'
    - 'Identify the effectiveness of E-Learning environments'
    - 'Explore the potential of the E-Learning  for promoting lifelong learning'
---

## Introduction
Trainer will start the introduction to the module by asking participants about their previous knowledge/experience in the field of online learning. Participants will be asked to focus on the advantages and disadvantages of online learning.
## E-Learning Fundamentals
In 1999, during the CBT Systems seminar in the U.S.A., a new word was used for the first time in a professional environment: ‘E-Learning’. Associated with such expressions as 'online learning' or 'virtual learning', this word was meant to qualify "a way to learn based on the use of new technologies allowing access to online, interactive and sometimes personalized training through the Internet or other electronic media (intranet, interactive TV, etc.), so as to develop competencies while the process of learning is independent from time and place".

Nowadays, thanks to the introduction of the computer and internet, E-Learning tools and delivery methods expanded. Today, E-Learning is more popular than ever, with countless individuals realizing the benefits that online learning can offer. Businesses too have begun using E-Learning to train their employees, giving them the chance to improve upon their industry knowledge base and expand their skill sets. At home individuals were granted access to programs that offered them the ability to earn online degrees and enrich their lives through expanded knowledge.

![](elearning1.png)

[eFront learning](https://www.efrontlearning.com/blog/2013/08/a-brief-history-of-elearning-infographic.html)
## Exploring E-Learning Tools
There’s a bunch of technologies involved in E-Learning. They can be split into three key areas: content authoring, LMS and TMS. Before getting started with using E-Learning tools, it is important to know the difference between the different tools used to create, develop, and deliver E-Learning content.
* Content Authoring: an e-learning content authoring tool is a software package which developers use to create and package e-learning content deliverable to end users. A content authoring tool is a software application used to create multimedia content typically for delivery on the World Wide Web. 
* A LMS (Learning Management System) is a software application for the administration, documentation, tracking, reporting, and delivery of educational courses, training programs, or learning and development programs. So, LMS is a software solution which creates and delivers personalized learning environments for students. LMS’s deliver educational material in multiple forms and through a variety of activities such as quizzes, video content and self-paced material. LMSs are focused on online learning delivery but support a range of uses, acting as a platform for online content, including courses, both asynchronous based and synchronous based. An LMS delivers and manages all types of content, including video, courses, and documents. In the education and higher education markets, an LMS will include a variety of functionality that is similar to corporate but will have features such as rubrics, teacher and instructor facilitated learning, a discussion board, and often the use of a syllabus. A syllabus is rarely a feature in the corporate LMS, although courses may start with heading-level index to give learners an overview of topics covered.
The LMS may be used to create professional structured course content. The teacher can add, text, images, tables, links and text formatting, interactive tests, slideshows etc. It helps control which content a student can access; track studying progress and engage student with contact tools. Teachers can manage courses and modules, enroll students or set up self-enrollment, see reports on students and import students to their online classes.
* The TMS (Training Management System) is used by training providers to manage the commercial aspects of their training operations. This includes managing training dates and sessions, taking registration and payment online, promoting courses and communicating with registrants.

## Different types of E-Learning
The better our E-Learnings are aligned with our needs and learning habits, the easier it will be for us to benefit from eLearning tools. 
Aligning our E-Learning approach along our needs is crucial for getting the most out of your learning environment. E-Learning Programs could take many forms, such as: 
* Blended online Learning: it is an approach to education that combines online educational materials and opportunities for interaction online with traditional place-based classroom methods. It requires the physical presence of both teacher and student, with some elements of student control over time, place etc. It allows the immediate feedback and analysis of the results which potentially help the students to identify their errors, weak areas and retain them. The advantages of blended learning involve interaction between larger groups, face-to-face interaction, self-paced learning, and reduced cost.
* Asynchronous online learning: This type of eLearning doesn’t depend neither on location nor time. Asynchronous eLearning is best managed through a Learning Management System (LMS), which enables you to track the progress of each learner. Generally, the interaction occurs through the blogs and the discussion boards rather than the face-to-face element or the class meeting. The users are given the content and the relevant details of the complete coursework and the assessments. Asynchronous eLearning is a learner-centred approach. The learner decides when he wants to tackle a new eLearning piece, when he wants to take a break and which part of eLearning he wants to skip because the knowledge/competence is already there.
* Synchronous online learning: it occurs in real time, which means that both the learners and the trainer have to meet at the same time in a virtual classroom. Learners and facilitator being independent from each other’s location is one of the biggest advantages of synchronous eLearning compared to face-2-face, as long as there is a good internet connection. This saves resources (in terms of travel time and travel costs) for both the learners and the facilitator.

![](elearning2.png)

[MDI Training](https://www.mdi-training.com/blog/blog/elearning/)
## Lifelong E-Learning
Technological developments have contributed considerably to dramatic changes in almost any sector of our life, education included. In such a globalised world, there is an increasing demand for new skills. “ICT Skills are a must in the global market and flexible, lifelong learning is necessary to stay competitive. Open and distance education (ODE) is flexible enough to give answers to the challenge of lifelong learning in the global society” (Bodil Ask, Sven Ake Bjorke, UNU/Global Virtua University, Norway).
## Homework
To get a better idea of the pros and cons of online teaching, participants are asked to watch some and take notes while they watch the videos:
* [Online learning advantaged ad disadvantages](https://www.youtube.com/watch?v=tcb9puhA3fo).
* [The e-learning advantages](https://www.youtube.com/watch?v=nzV1NmhC7ik).

Afterwards, learners are asked to create their pros-and-cons list.
## References
* [History of E-Learning](http://www.leerbeleving.nl/wbts/1/history_of_elearning.html).
* [Introduction to eLearning, Course Authoring, LMS](https://www.youtube.com/watch?v=nEYAWU5fQOs).
* [What are content authoring tools and how can we use them?](https://www.talentlms.com/elearning/what-is-a-content-authoring-tool).
* [LMS vs CMS vs LCMS…What’s the Difference?](https://www.lessonly.com/blog/lms-vs-cms-vs-lcms-whats-difference/).
* [What is a Training Management System?](https://elearningindustry.com/training-management-system-tms).
* [LMS -Wikipedia](https://en.wikipedia.org/wiki/Learning_management_system).
* [Types of E-Learning](https://www.youtube.com/watch?v=XL5gZYqHO3M).
* [E-Learning ways](https://elearningways.com/what-is-e-learning-why-does-it-matter-lets-explore/).