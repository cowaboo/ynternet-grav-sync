---
title: 'Ready to go E-learning! Using the E-Learning to promote Open Education'
length: '360 min'
objectives:
    - 'Provide an overview of the history, development and philosophy of Moodle'
    - 'Recognize the top benefits of Moodle'
    - 'Explore the main features of Moodle'
    - 'Use open digital technologies to get support from the Moodle Community'
    - 'Encourage the involvement in the online community and the development of the open-source software'
---

## Group discussion on Moodle
Trainer will start the introduction to the module by asking participants about their experience in the field of Open Source Learning Management System for education and/or workplace training.
## Introduction to Moodle LMS: Freedom to learn
Moodle is an active and evolving work in progress started by the Australian educator and computer programmer Martin Dougiama.

“I'm committed to continuing my work on Moodle and on keeping it Open and Free. I have a deeply-held belief in the importance of unrestricted education and empowered teaching, and Moodle is the main way I can contribute to the realization of these ideals".

It’s the first Open Source Learning Management System (LMS) brought out to the public in 2002. Moodle 1.0 was officially released in 2002 and originated as a platform to provide educators with the technology to provide online learning in personalized environments that foster interaction, inquiry and collaboration. Since then there has been steady series of new releases adding new features, better scalability and improved performance.
Today, Moodle is the world’s most popular and most used learning management system. With 100 million users (and growing) and over 100,000 Moodle sites deployed worldwide, this user-friendly eLearning platform serves the learning and training needs of all types of organizations in more than 225 countries worldwide.
## Why is Moodle the most widely used learning management system?
Besides being highly-customizable and flexible, Moodle never stop improving. In fact, being an open source project, Moodle is a collaborative effort and is supported by a strong global community. So, developers from all over the world can access the code, and modify it to continually enhance the software and make it more secure.

Originally designed for higher education, Moodle is now used not only in in high schools, primary schools, non-profit organizations, private companies, by independent teachers and even homeschooling parents. From education, Moodle has reached the corporate world, and it is now gaining popularity in the Healthcare sector too.
## Moodle's core features
Introduction to:
* Interface
* Dashboard
* Collaborative tools and activities
* All-in-one calendar
* File management
* Text Management
* Notification
* Track progress

## Contributing to the development of the open-source software
Being an open source learning platform, Moodle gives us the possibilities to contribute to the platform in many different ways, such as:
* User Support: Get support from the community, share ideas and meet others in a [the Community Forum](https://moodle.org/course/)
* Testing: Help test and report bugs via the Moodle Tracker
* Usability: Perform some usability testing and report your results
* Translation: Assist with the translation of Moodle, including contributing a translation/correction
* Social Media: Help raise awareness of the Moodle project and share the latest news and happenings in the Moodle world

In addition, users have the possibility of joining the [Moodle Users Association](https://moodleassociation.org/). The Association is an official non-profit organization dedicated to contributing towards the core development of Moodle. The Moodle Users Association supports the growth of Moodle by providing a strong and united voice to users, giving direction and resources for new developments.
## Homework
From Moodle website, go to the [demo section](https://moodle.org/demo/) and explore [Mount Orange school Moodle](https://school.moodledemo.net/mod/page/view.php?id=45). From there using the credentials (login as 'teacher', password 'moodle'), you can navigate inside a Moodle platform, set  some activities up yourself and then log in as a student (username: student; password: moodle) to try them out.
## References
* [What is Moodle?](https://www.youtube.com/watch?v=wop3FMhoLGs)
* [Introduction to Moodle](https://www.lambdasolutions.net/guides-and-research/moodle-user-guide-intro-to-moodle).
* [Using Moodle](https://support.moodle.com/hc/en-us/categories/115000822747-Using-Moodle).
* [Moodle’s core features](https://docs.moodle.org/37/en/Features).
* [Moodle Dashboard](https://docs.moodle.org/37/en/Dashboard).
* [Moodle Activities](https://docs.moodle.org/37/en/Activities).
* [Moodle Calendar](https://docs.moodle.org/37/en/Calendar).
* [Working with files](https://docs.moodle.org/37/en/Working_with_files).
* [Moodle Text editor](https://docs.moodle.org/37/en/Text_editor).
* [Moodle Messaging](https://docs.moodle.org/37/en/Messaging).
* [Tracking progress](https://docs.moodle.org/37/en/Tracking_progress).
* [Contributing to Moodle](https://docs.moodle.org/dev/Contributing_to_Moodle).
* [Moodle Users Associations](https://moodleassociation.org/).
* [Moodle Community Forums](https://moodle.org/course/).