---
title: 'Imprenditorialità online con gli strumenti FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: it
length: '10 ore'
objectives:
    - 'Pianificare il tuo business'
    - 'Ciclo di vita del progetto'
    - 'Introduzione al budget'
materials:
    - 'Computer ed internet'
    - 'Idea di base delle competenze FLOSS'
skills:
    'Parole chiave':
        subs:
            'Online business': null
            Imprenditorialità: null
---

## Introduzione
Le vendite online non sono mai andate meglio, e la previsione è che continueranno a crescere fino al 2021, come mostrano le statistiche di ‘Statista – The Statistics Portal’. Questo tipo di commercio si sta evolvendo anno dopo anno, acquisendo un numero crescente di sostenitori.  I negozi virtuali non sono altro che vetrine che mostrano I prodotti disponibili; nel mercato delle vendite online è nato il bisogno di investire in strategie di marketing per conoscere I propri consumatori, sapere cosa pensano dei propri prodotti e seguirli nel post-vendita.
Qualsiasi tipo di business può essere messo online; da corsi sportivi a corsi di lingua, consulenza aziendale, coaching personale, catering e artigianato.

Lo shopping online è una delle attività più popolari online nel mondo, e solo nel 2016 le vendite e-commerce al dettaglio sono salite a 1.86 trillioni di dollari USA. Per l’anno 2021, si stima che le vendite online cresceranno fino a 4.48 trilioni di dollari USA. Ciò mostra quanto un imprenditore e il suo business possono trarre vantaggio dall’Era digitale per incrementare gli affari, in qualunque area. 
Anche il numero di acquirenti digitali sta aumentando in tutto il mondo. È cresciuto di oltre 100 milioni tra il 2011 e il 2012 e continua a crescere. Le donne contribuiscono in maggior misura a questa crescita, grazie ad orari di lavoro flessibili che permettono un migliore equilibrio lavoro-vita personale. Nel 2012, un sondaggio Greenfield ha rivelato che le donne contribuiscono per il 58% agli acquisti online negli Stati Uniti.
## Contesto
Questo modulo permetterà all’utente di acquisire conoscenze per insegnare:
* Competenze per lo sviluppo di un business partendo da un’idea.
* Come effettuare un’analisi SWOT.
* Come definire e comprendere il mercato e i diversi tipi di concorrenza che un’impresa si trova a dover gestire.
* Il ciclo di vita del prodotto.
* Il marketing mix.
* Principi di budgeting.

Metodologia:
* Discussione: Gruppi e forum di discussione.
* Esecuzione: Formulazione di piani da parte degli studenti.

## Sessioni
### Prima sessione: Sviluppo e valutazione delle idee di business attraverso l'analisi SWOT delle Mind maps
In questa sessione lavoreremo sull'analisi di un'idea di business utilizzando l'analisi SWOT e le mind map. 
### Seconda sessione: Definizione del mercato
Questa sessione si concentrerà sulla segmentazione dei clienti e sulla comprensione della concorrenza.
### Terza sessione: Budgeting
Cos'è un budget e quali sono dieci passi per seguire un budget.