---
title: 'Create your powerful digital story open digital technologies!'
length: '420 min'
objectives:
    - 'Write a script and realize a storyboard'
    - 'Create and collect relevant materials for the digital story (images, voice, music, sounds, texts, titles) through open digital technologies'
    - 'Record and edit digital audio'
    - 'Find and download royalty-Free Music and Sounds'
    - 'Recognize the free license material on the web'
    - 'Share safely your digital story on the media'
    - 'Shape creative ideas with digital applications'
    - 'Produce a digital (audio visual) story'
    - 'Share the digital stories with others'
---

## Storyboards in Digital Storytelling
After deciding on the story we  want to tell and gathering our materials, we can begin writing our script and create a storyboard,  a place to plan out a visual story on two levels: 1) Time — What happens in what order? and 2) Interaction — How does the voiceover and music work with the images or video?

Making a storyboard is a guideline, a map to imagine the flow of your story and outline the best ideas from start to finish. The Storyboard in Digital Storytelling is only there to support the learners to design a structure making the editing part that follows this phase easier.

A storyboard is a written/graphical representation of the all of the elements that will be included in a digital story. It is usually created before actual work on creating the digital story begins and a written description and graphical depiction of the elements of the story, such as images, text, narration, music, transitions, etc. are added to the storyboard.

Storyboards may be created in a variety of ways, both digitally and manually on paper or artists' board. If storyboards are developed on a computer, a variety of software programs may be used, such as Microsoft Word, Excel and PowerPoint.
## Selection and use of images, animations and sounds for your digital story
Dealing with the selection and utilization of images, animations and sounds when producing a digital story is crucial.  
For creating digital media projects, it is very important to record basic narration, edit audio to adjust the volume, remove unwanted sounds, fade audio in or out, etc.

Copyright also plays a relevant role in digital storytelling. The digital storytellers must be aware of the kind of images, sounds and other media they can use without infringing the copyright of others. Many websites for example, provide music under a Creative Commons license where musicians give away their music for certain non-commercial purposes or place their music in the public domain where it may be used without any restrictions.

The purpose of this session is to support learners to use open source software (iMovie, Da Vinci Resolve etc.)  and tools to produce personal digital stories. The main topics of the session will be the following:
* Script writing and storyboarding - Audience Ownership and consent.
* Role of sound (record a voice over / add music).
* Role of visuals / Digitize your media.
* Story editing and sharing Movie Making.

## Homework
* Using the script created in the previous session, structure your story in such a way that the search for images is easier afterwards: use a storyboard. Paint pictures in the boxes of the storyboard or write search terms for pictures in the boxes. The storyboard serves to structure the story and is a tool to create the first connection between text and image.
* Search images. The total number of images should correspond to the participants’ storyboards and duration of scripts. Select usable images from personal collections as well as internet sources. Edit the images selected in [Gimp](https://www.gimp.org/ù) (crop, blur and shadow).
* Prepare a final script, ready to record and a final storyboard. Go to [Audacityteam](https://www.audacityteam.org/) and install the software. Record your own voice and save the file.
* Save and export films and files using MovieMaker or similar. 

## References
* [Educational uses of digital storytelling](http://digitalstorytelling.coe.uh.edu/page.cfm?id=23&cid=23&sublinkid=37).
* [Digital Story Telling Guide](https://uow.libguides.com/ld.php?content_id=42727880).
* [Digital storytelling in 10 steps](https://www.socialbrite.org/2010/07/15/digital-storytelling-a-tutorial-in-10-easy-steps/).
* [Brights MOOC](http://www.brights-project.eu/en/results/brights-mooc/).
* [Brights Digital Storytelling](https://vimeo.com/showcase/4856060).
* [Digital Storytelling Toolkit](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf).
* [My future in quarantine](https://vimeo.com/107569681).
* [How to realize a storyboard](http://www.brights-project.eu/en/results/brights-mooc/).
* [Storyboard Template](https://www.the-flying-animator.com/storyboard-template.html).
* [Storyboard That](https://www.storyboardthat.com/).
* [Collection and production of materials for digital stories](https://mooc.cti.gr/mod/lesson/view.php?id=512&pageid=821).
* [Licensing consideration](https://creativecommons.org/share-your-work/licensing-considerations/).
* [Copyright and Digital Storytelling](http://digitalstorytelling.coe.uh.edu/archive/copyright.html).