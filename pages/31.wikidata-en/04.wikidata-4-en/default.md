---
title: 'Applying Wikidata'
length: '120 min'
objectives:
    - 'Identify the applied value of Wikidata, particularly for applications like visualization of complex data'
    - 'Identify potential applications for Wikidata in their own work, whether through visualization, embedding Wikidata in their own software'
    - 'Know how to contact and connect to members of the Wikidata community'
---

## Questions about last session and discussion about homework
Feedback on sparql and queries.
Results of the ISA campaign and relationship with the session of the day (use of wikidata info to improve image description with structured data).
## (Potential) applied value of Wikidata
Overview and showcases.
This section can be divided in 3 parts:
* The first could provide an overview of content already available on WikiData : figures and stats per theme.
* The second could provide examples of how Wikidata is used by Wikimedia projects.

Examples suggested include (to be selected based on the audience):
1. a tool to explore the content gender gap on Wikipedia such as Denelezh.
2. the use of Listeria bot to create RedLists of missing articles. See Category:Lists_based_on_Wikidata.
3. the use of Wikidata to add notices of authority on Wikipedia articles. Example of Doris Day.
4. the use of Wikidata to keep infoboxes on Wikipedia up to date. Example of Jack Spicer.
5. the use of the parser function to directly display information from WikiData in the articles (see How_to_use_data_on_Wikimedia_projects).
6. the use of magic infoboxes on Wikipedia (such as the use of {{infobox fromage}} or {{infobox biographies2}} on the French Wikipedia).

* The third would provide examples of how Wikidata is used by third parties.

Examples suggested to present include (to be selected based on the audience):
1. various visualization tools such as: The Wikidata Graph Builder or Histropedia (timeline application based on Wikidata)
2. crotos, a search and display engine for visual artworks based on Wikidata and  using Wikimedia Commons files
3. Scholia: a search and display engine for academics based on Wikidata

### Read
* [Denelezh](https://www.denelezh.org)
* [Lists based on Wikidata](https://en.wikipedia.org/wiki/Category:Lists_based_on_Wikidata)
* [Doris Day](https://en.wikipedia.org/wiki/Doris_Day)
* [Jack Spicer](https://en.wikipedia.org/wiki/Jack_Spicer)
* [How to use data on Wikimedia projects](https://www.wikidata.org/wiki/Wikidata:How_to_use_data_on_Wikimedia_projects)
* [Showcases](https://phabricator.wikimedia.org/T168057)
* [Wikidata knowledge as a service](https://www.wikidata.org/wiki/File:Wikidata_Knowledge_as_a_Service_slides_OeRC_Feb2018.pdf)
* [Histropedia](http://histropedia.com)
* [Angryloki](https://angryloki.github.io/wikidata-graph-builder/?property=P279&item=Q5)
* [Crotos](http://zone47.com/crotos/)
* [Sholia](https://tools.wmflabs.org/scholia/)

### Activity (on laptop)
Provide participants with the list of selected tools and let them explore.
## Connect with the WikiData community
### How to connect
The goal of this section is to know how to contact and connect to members of the Wikidata community. How to stay in touch with the latest news of the project and where to find resources.
Review options to discuss with a wikidatien on a talk page.
Introduce WikiProjects and explain how to join them or interact with its members. Explain the benefits of those WikiProjects, but also their governance (freedom to join etc.) Ideally choose to show a wikiproject relevant to the audience.
Suggest joining the wikidata mailing list, technical mailing lists, irc channel, telegram etc... all available on WikiData main page. Interact with audience to see what would be most appropriate for them.
An interesting point of entry to show how the community interacts is to show the Request for Comment page.
Suggest contact with the local chapter if there is one in the country, and with the wikidata community usergroup.
Point to the Wikidata annual conference, navigate the program and point to the presentations provided (at least slides, sometimes records). More generally events.
### Read
* [WikiData WikiProjects](https://dashboard.wikiedu.org/training/wikidata-professional/wikidata-wikiprojects)
* [Requests for comment](https://www.wikidata.org/wiki/Wikidata:Requests_for_comment)
* [WikiDataCon 2019](https://www.wikidata.org/wiki/Wikidata:WikidataCon_2019)
* [WikiData Events](https://www.wikidata.org/wiki/Wikidata:Events)
* [WikiData movement affiliates](https://meta.wikimedia.org/wiki/Wikimedia_movement_affiliates)
* [WikiData Community User Group](https://meta.wikimedia.org/wiki/Wikidata_Community_User_Group)

## Homework
Search in the [Zenodo](https://zenodo.org/) open repository and find your three favourite papers on FLOSS and education. Document and/or post a comment on them.
## Debriefing
To wrap up the session, the trainer will facilitate a debriefing moment where participants are encouraged to express their questions, doubts, ideas and feelings toward the topics discussed.
## References
* [Planning](https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop).
* [Professional](https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata).
* [Help](https://www.wikidata.org/wiki/Help:Contents).
* [Tools](https://www.wikidata.org/wiki/Wikidata:Tools).
* [Educational resources](https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources).