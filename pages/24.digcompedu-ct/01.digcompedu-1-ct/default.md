---
title: 'El marc DigCompEdu'
objectives:
    - 'Entendre els principis de DigCompEdu per millorar les habilitats digitals en l ensenyament'
    - 'Promoure l’ús de marcs europeus comuns en l’educació d’adults'
duració: '180 minuts'
---

## Introducció
La persona a càrrec de la formació iniciarà la introducció al mòdul preguntant als participants sobre el coneixement del marc i d’altres marcs europeus: (DigComp i DigCompOrg) i locals, com ara l'ACTIC de la Generalitat de Catalunya. Les respostes es documentaran i es reutilitzaran per a personalitzar el material de formació en futures oportunitats.

## “Els principis de DigComEdu”
Les professions s’enfronten a exigències canviants ràpidament, que requereixen un nou conjunt de competències més ampli i sofisticat que abans. La ubiqüitat dels dispositius i aplicacions digitals, en particular, requereix que els educadors desenvolupin la seva competència digital.
El Marc europeu per a la competència digital dels educadors (DigCompEdu) és un marc científicament fonamental que descriu què significa que els educadors siguin competents digitalment.
Proporciona un marc de referència general per donar suport al desenvolupament de competències digitals específiques per a educadors i educadores a Europa. DigCompEdu està dirigit a educadors a tots els nivells d’educació, des de la primera infància fins a l’educació superior i la d’adults, incloent formació i capacitació generals i professionals, educació en el marc de necessitats especials i contextos d’aprenentatge no formal.

![](DigCompEdu.jpg)

DigCompEdu detalla 22 competències organitzades en sis àrees. El focus no es centra en les habilitats tècniques. Més aviat, el marc pretén detallar com es poden utilitzar les tecnologies digitals per millorar i innovar l'educació i la formació.
L’estudi DigCompEdu es basa en treballs anteriors realitzats per definir la [competència digital dels ciutadans](https://ec.europa.eu/jrc/en/digcomp) en general i la competència digital de les organitzacions educatives ([DigCompOrg](https://ec.europa.eu/jrc/en/digcomporg)). Contribueix a l'Agenda de Competències per a Europa (Skills Agenda for Europe) recentment aprovada i a la iniciativa emblemàtica Europa 2020 en relació a les noves habilitats per a nous llocs de treball (Agenda for New Skills for New Jobs).

## Deures
Els participants han d’explorar aquest [document](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu) i utilitzant la graella que es troba en aquest [enllaç](https://ec.europa.eu/jrc/en/digcompedu/framework/proficiency-levels) i que està relacionada amb el nivell de competència, haurien d’autovaluar-se, per a conèixer el seu propi nivell.
Es demana als participants que pensin en les seves competències en relació amb el marc i com els documents poden ser útils per millorar les competències pròpies i les de les seves organitzacions.
Per acabar la sessió, la persona a càrrec de la formació facilitarà un moment de debat, on es motivarà als participants a expressar les seves preguntes, dubtes, idees i sentiments cap als temes tractats. 
Alternativament, o com a complement, les persones participants respondran un breu qüestionari per avaluar el que han après durant la sessió.

## Referències
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu)
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators)
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials)
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community)
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345)