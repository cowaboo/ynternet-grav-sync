---
title: 'Com utilitzar el marc per associar les competències i millorar la vostra competència'
objectives:
    - 'Identificar les pròpies necessitats per convertir-se en competent en la docència digital'
    - 'Identificar els recursos i les eines obertes disponibles, i enllaçar-los a les àrees DigCompEdu'
    - 'Dissenyar i planificar la millora educativa i professional'
    - 'Promoure l’ús d’un marc europeu comú per a les persones educadores'
Durada: '360 minuts'
---

## Introducció
Discussió en grup sobre "On creus que t'agradaria millorar?""
La persona a càrrec de la formació iniciarà la introducció d’aquesta sessió preguntant als participants quines de les sis àrees se senten menys competents i els agradaria millorar a partir de les conclusions de la primera sessió.

## Identifica els teus buits i coneix eines i metodologies
“El professorat necessita actualitzar els seus perfils competencials per als reptes del segle XXI. Les estratègies docents han de canviar i també les competències que ha de desenvolupar els professorat per tal d’apoderar els aprenents del segle XXI ... Això es vincula amb el desenvolupament de competències digitals dels professorat i l'estudiantat i es pot vincular a la creació de capacitats institucionals. Al mateix temps, el marc és prou genèric com per aplicar-se a diferents entorns educatius i permetre l’adaptació a mesura que evolucionen les possibilitats i limitacions tecnològiques”. [Font](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345 ).
Presentació i anàlisi dels avantatges de fer un mapeig de les pròpies competències (i de les organitzacions) en el marc, i introducció d’algunes eines i tecnologies de codi obert identificades pel consorci Open-AE.

## Construïm el nostre propi pla per a incrementar les nostres competències digitals
Com utilitzar el marc per planificar la vostra millora? Amb el suport de la tutoria i després d’haver identificat els seus buits i interessos, les persones participants hauran d’elaborar el seu propi pla d’acord amb els contextos i grups objectiu amb què treballin. Mitjançant una graella proporcionada pel la tutoria, els i les participants podran emplenar el pla i començaran a identificar les eines i les tecnologies de codi obert que voldrien utilitzar per millorar les seves competències. La tutoria donarà informació sobre els resultats de la investigació del projecte Open-AE, és a dir, les eines de codi obert existents disponibles per a les i les formadors.
A més, es presentarà i discutirà el cas d’estudi del projecte [The DOCENT project](https://docent-project.eu/) per tal de proporcionar als participants un exemple concret sobre com es pot utilitzar el marc DigCompEdu per donar forma a un marc de competències digitals d’ensenyament creatiu.

## Per finalitzar la sessió
La formadora o tutora facilitarà un moment de debat on s’animi els participants a expressar les seves preguntes, dubtes, idees i sentiments cap a les activitats pràctiques que es duen a terme.

## Deures
Analitzeu les necessitats de la vostra organització (personal i col·laboradors) i dissenyeu un pla superior. Les persones participants resumeixen els resultats del pla de les seves organitzacions en un informe breu.

## Referències
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu)
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators)
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials)
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community)
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345)
* [The DOCENT project](https://docent-project.eu/)