---
title: 'Como utilizar el marco para asociar las competencias y mejorar vuestra competencia'
objectives:
    - 'identificar las propias necesidades para convertirse en competente en la docencia digital'
    - 'identificar los recursos y las herramientas abiertas disponibles, y enlazarlos a las áreas DigCompEdu'
    - 'diseñar y planificar la mejora educativa y profesional'
    - 'promover el uso de un marco europeo común para las personas educadoras'
length: '360 min'
---

## Introducción
Discusión en grupo sobre "¿donde crees que te gustaría mejorar?"
La persona a cargo de la formación iniciará la introducción de esta sesión preguntando a los participantes, en cuales de las seis áreas se sienten menos competentes y les gustaría mejorar a partir de las conclusiones de la primera sesión.
## Identifica tus carencias y conoce herramientas y metodologias
“El profesorado necesita actualitzar sus perfiles competenciales para los retos del siglo XXI. Las estrategias docentes tienen que cambiar y también las competencias que tiene que desarrollar el profesorado para empoderar a alumnado del siglo XXI ... Esto se vincula con el desarrollo de competencias digitales del profesorado y los y las estudiantes y se puede vincular a la creación de capacidades institucionales. Al mismo tiempo, el marco es lo bastante genérico como para aplicarse a diferentes entornos educativos y permitir la adaptación a medida que evolucionan las posibilidades y limitaciones tecnológicas ”. ([https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345) ) 
Presentación y análisis de las ventajas de hacer un mapeo de las propias competencias (y de las organitzaciones) en el marco, e introducción de algunas herramientas y tecnologias de código abierto identificadas por el consorcio Open-AE.
## Construimos nuestro propio plan para incrementar nuestras competencias digitales
¿Como utilizar el marco para planificar vuestra mejora? Con el soporte de la tutoría y después de haber identificado sus carencias e intereses, las personas participantes tendrán que elaborar su propio plan, de acuerdo con los contextos y grupos objetivo, con los que trabajen. Mediante un cuadro proporcionado por la tutora, los y las participantes podrán rellenar el plan y empezarán a identificar las herramientas y las tecnologías de código abierto que querrían utilizar para mejorar sus competencias. La tutoría dará información sobre los resultados de la investigación del proyecto Open-AE, es decir, las herramientas de código abierto existentes y disponibles para las y los formadores.
Además, se presentará y discutirá el caso de estudio del proyecto [The DOCENT project](https://docent-project.eu/) para proporcionar a los participantes un ejemplo concreto sobre como se puede utilizar el marco DigCompEdu para dar forma a un marco de competencias digitales de enseñanza creativa.
## Deberes
Analizad las necesidades de vuestra organización (personal i colaboradores) y diseñad un plan superior.
## Referencias
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu).
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators).
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials).
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community).
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345).
* [The DOCENT project](https://docent-project.eu/).