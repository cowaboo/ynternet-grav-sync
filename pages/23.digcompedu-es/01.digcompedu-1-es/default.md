---
title: 'El marco competencial DigCompEdu'
objectives:
    - 'Entender los principios de DigCompEdu para mejorar las habilidades digitales en la enseñanza'
    - 'Promover el uso de marcos europeos comunes en la educación de adultos'
length: '150 min'
---

## Introducción 
La persona a cargo de la formación iniciará la introducción al módulo preguntando a los participantes sobre el conocimiento del marco y de otros marcos europeos: (DigComp i DigCompOrg) y locales, como ACTIC de la Generalitat de Catalunya. Las respuestas se documentarán y se reutilizarán para personalizar el material de formación en futuras oportunidades.
## “Los principios de DigComEdu”
Las profesiones se enfrentan a exigencias cambiantes rápidamente, que requieren un nuevo conjunto de competencias más amplio y sofisticado que antes. La ubicuidad de los dispositivos y aplicaciones digitales, en particular, requieren que los y las educadoras desarrollen su competencia digital.
El Marco europeo para la competencia digital de los y las educadoras (DigCompEdu) es un marco científicamente fundamental, que describe qué significa que los y las educadoras sean competentes digitalmente.

![](DigCompEdu.jpg)

Proporciona un marco de referencia general para dar soporte al desarrollo de competencias digitales específicas para educadores y educadoras en Europa. DigCompEdu está dirigido a educadores y educadoras de todos los niveles educativos, desde la primera infancia hasta la educación superior y la de adultos, incluyendo formación y capacitación generales y profesionales, educación en el marco de necesidades especiales y contextos de educación no formal.
DigCompEdu detalla 22 competencias organizadas en seis áreas. El foco no se centra en las habilidades técnicas. Más bien, el marco pretende detallar como se pueden utilizar las tecnologias digitales para mejorar e innovar la educación y la formación.
El estudio DigCompEdu se basa en trabajos anteriores realizados para definir la [competencia digital de los ciudadanos](https://ec.europa.eu/jrc/en/digcomp) en general y las organizaciones de educación competencial digital ([DigCompOrg](https://ec.europa.eu/jrc/en/digcomporg)).
Contribuye a la Agenda de Competencias para Europa (Skills Agenda for Europe) recientemente aprobada y a la iniciativa emblemática Europa 2020 en relación a las nuevas habilidades para nuevos lugares de trabajo (Agenda for New Skills for New Jobs).

## Deberes
Los y las participantes tienen que explorar el [documento](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu) y utilizando el cuadro que se encuentra [en este enlace](https://ec.europa.eu/jrc/en/digcompedu/framework/proficiency-levels) relacionado con el nivel de competencia, tendrian que autoevaluarse, para conocer su propio nivel de competencia.
Se pide a los participantes que piensen en sus competencias en relación con el marco y como los documentos pueden ser útiles para mejorar las competencias propias y las de sus organizaciones.
## Referencias
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu).
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators).
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials).
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community).
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345).