---
title: 'Our needs'
objectives:
    - 'Know different experiences of telecenters, digital-training centers and similar spaces that have decided to work 100% with GNU / Linux'
    - 'Provide guidelines and criteria while choosing software'
    - 'Discover what FLOSS solutions are best suited to our needs'
    - 'Show that migrating and managing a telecentre with FLOSS is within our reach'
lenght: '120 min'
---

## Introduction
Description of the objectives of the session.
In order to successfully migrate to GNU / Linux it is important not to do it alone. It is important to know which networks we have in our environment, and to know who we can count on to take these steps.
* Locate agents from the territory that can be part of this support network
* Map our needs in order to find the best FLOSS solutions for our center.

Let's take a successful migration to GNU / Linux as an example.
## Contact taking
We carry out a presentation wheel where each of the participants must indicate which center they come from, contextualize their action range to the social level and indicate the degree of knowledge about GNU / Linux they have.
The objective of this first dynamic is that the trainer can know the different levels of the participants.
Face-to-face format: presentation wheel. Online format: presentation forum.
## Mapping of actors
The objective of this dynamic is to identify the actors with which each center can count to migrate to GNU / Linux. We will provide a grid for this map, and the different actors will be placed.
* **Public administrations**: what is your telecentre relationship with the administration? Does it depend directly? If the management is private, does it collaborate with the administration? There are PLL promotion programs in your administrative sphere?
* **Private initiatives**: in your area, is there any company providing technological support     • to the small companies? Your center, does use this kind of companies in order to maintain it working properly? Is there a specialization in open licenses? What collaborations do you currently have, and the most important, what could you have?
* **Citizenship**: Is there any local agent -non profit, small company- directed to promote the free program in your area? Surely, locate it, study what alliances can you reach!

Face-to-face format: the participants are grouped by local proximity or administrative type (the activity is run in small groups)
Online format: Depending on the virtual environment that can be used, you can enhance the collaboration between the different participants. Creating a wiki to map collectively is optional, but you can also provide templates to be delivered individually. 
Supporting material: matrix of actors (template).
## Mapping tools
With this activity you want to start working on the selection of FLOSS tools that will be useful for you. The objective of the session is to determine the degree of specialization that each center has and select the appropriate OS for each case.
It works from a grid with two columns:
* What tools do you use mainly today?
* What FLOSS solutions can you find?

In this dynamic the trainer must guide in the search of information and selection of tools for each need.
At the end of the session, the most common OS solutions with pros and cons (support: presentation or web) will be presented.
1. Generalists:
* Ubuntu for its ease management and stability (recommended for this course).
* Linux Mint / ElementaryOS for windows / mac transitions.
* Lubuntu for lightweight equipment.
2.  Specific:
* Ubuntu Studio for audiovisuals.
* Tails for security and privacy.
                
## Homework
If the training is done online, the participants will have to perform the tasks individually:
* Mapping of actors.
* Tool mapping.

## References
[The GNU Operating System](https://www.gnu.org/gnu/gnu.en.html)

[Linux forums](https://www.linux.org/forums/)

The Linux Foundation: [Participating in Open Source Communities](https://www.linuxfoundation.org/resources/open-source-guides/participating-open-source-communities/)

[Guides on Open Source](https://www.linuxfoundation.org/resources/open-source-guides/) 