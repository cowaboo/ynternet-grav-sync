---
title: 'And now what?'
objectives:
    - 'Learn about the main software installation mechanisms'
    - 'Establish guidelines for installation of updates as a basis for good maintenance'
    - 'Compile repositories of reference documentation'
    - 'Know the policy of administrative permissions and users of your OS'
lenght: '90 min'
---

## How do I install new programs?
How do I install new programs? 
The goal is to know the mechanisms and to have guidelines to keep the equipment up to date; and to be able to incorporate the software that better fits our needs.
The trainer will explain: 

- [What is a repository? ](https://help.ubuntu.com/community/Repositories/Ubuntu)

- How do we connect? The software Center (App Store)
Each system or distribution has a software center that connects to the repositories and gives us the possibility to download and install the selected applications, as well as the chance to read and evaluate the recommendations of the community.
There are also some specific application stores that incorporate possible additional security enhancements, or encapsulation, that can facilitate system compatibility. In fact, there are different ways to get a program installed, and depending on the system, some version and some user profile may be more advisable to be chosen. 
We can also install programs by downloading them directly from their webpage, or from their own repository. We will usually do this using specific commands such as "apt-get”. Get more info at:  Use Apt-Get to Install Programs at howtogeek.com 


## Upgrade Policies

Maintaining an up-to-date system is key for security reasons, as well as for the functionality provided by application developers, which often make improvements based on the requests or suggestions of the user community.
There are several ways to set up and keep your system up to date. On the one hand by activating automatic updates to the system configuration, and on the other by using its own manager if necessary such as https://ubuntu.com/livepatch 
**Types of updates:**

- Security Updates: Fix bugs and security holes detected. Very important to make them!

- Recommended updates: Significant but not vital improvements to the operation of our PCs

- Unsupported updates: improvements to programs that are still being tested by the community, helps us to detect bugs. Not recommended in working or training environments.

**How often? (recommendations)**

- Security Updates: Download and install automatically.

- Notice of new versions of the entire Operating System: Only for long-term support versions!

## Reference documentation
Is highly recommended to collaboratively create a documentation repository but, first, think about it:  what reference sites that already exist can you find to gather information? After that, you can start to create your own repository: use a wiki system in order to compile the information you need; that's the best way to collect and share with your team all the information. 

## Users and administrators
To make updates or installations we must always have the appropriate permissions to do so; a possible strategy is to have a “user” with whom we connect only to perform maintenance and update tasks. In this way, none of other users will have permissions or access to perform system modification tasks.. 
This may be too restrictive a proposal, but it will prevent us from having a problem if it is a machine that is accessed by many different users (we do not want to lose control of the state of the machine)
We'll review which users we have on the machine, and how to create a new user, with administrator permissions, in order to make updates.

**Recommended users:**

- User 0: User created during the installation process. You have administrative permissions to install software, perform system configurations, etc. Do not use this user when teaching.

- Desktop user: this user does not have permissions to perform system alterations (software or settings). It’s recommended during the training and also for beginners. We can create as many users as courses we are doing!

## Homework
Create a list of repositories, application stores and package managers, indicating which features they offer us and when it’s recommend their use.

## References

https://ubuntu.com/livepatch 

https://www.howtogeek.com/63997/how-to-install-programs-in-ubuntu-in-the-command-line/

https://snapcraft.io,  which is a software center that uses the snap format (https://blogubuntu.com/que-es-ubuntu-snap ).