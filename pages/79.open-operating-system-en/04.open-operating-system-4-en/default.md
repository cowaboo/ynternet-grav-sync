---
title: 'Promotion and dissemination'
objectives:
    - 'Be aware of how this migration can affect the people participating in each center'
    - 'Set up didactic strategies to work on the new SO'
    - 'Propose strategies for the dissemination of FLOSS'
    - 'Design strategies to adapt the didactic material'
lenght: '120 min'
---

## The first impressions
Discussion forum, in order to ask the participants to share their experience and to present the first reactions of the participants in each center.
Building (all together) some answers to balance the difficulties that have shown up. 
On-line activity or on-site discussion groups
## Set of reasons given with the aim of persuading others that the migration is a good idea and answers to the problems 
Role-playing with 2 different scenarios in order to anticipate possible favorable or contrary responses from users. The group divides in two to role-play these two scenarios:
- scenario 1: You (the teacher) meet with regular participants, they’re used to the old system and suddenly, they find that another OS has been settled. 
Have you notified in advance that a change will be made? How did you do it? What did you explain? Will you explain the advantages of the FLOSS culture and free software? Will you explain the utilities of this type of program to them? Will you tell them about the values of FLOSS?


- scenario 1: You (the teacher) meet with new participants in the center. 
Do you need to explain anything at all? Note that if the OS is not explained when working with Windows or Mac, it may have no sense to explain it, if you are working with GNU / Linux. Just use it!  
Consider if you will explain the advantages of the FLOSS culture and free software, the utilities of this type of program, or if you will tell them about the values of FLOSS?


A representative from each group takes note of the objections and arguments in favor of the FLOSS culture that have been observed during the role-playing and shares their notes with the rest of the group.

Face-to-face format: role-playing.

Online format: discussion forums.

## The didactic material
Finally, the need to adapt the existing teaching material to continue conducting the sessions at each center is addressed.
It is proposed to refocus the existing material in the work on digital skills and not so much on tools (if not already done). For example, you can talk about “office suites” and not about MSOffice.
Each participant is asked to submit a sample of the teaching material to support an activity they are currently carrying out, accompanying it with updated proposals according to the new environment and the new perspective. The trainer provides advice and guidance. The delivery of this material is done in an open way so that it serves as learning for the other participants.

## Homework
The group can organized itself in order to create or update small basic training manuals in order to help users in the use of the free software. They are distributed into small groups and continue the work until it is finished; then, everybody shares their outcomes or deliverables with a FLOSS licence in order to allow others to reuse it. 

## References
[How to Make the Switch From Windows to Linux](https://www.pcmag.com/how-to/how-to-make-the-switch-from-windows-to-linux)  

