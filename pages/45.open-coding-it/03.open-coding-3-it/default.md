---
title: 'L’efficacia di Scratch nella creazione di un ambiente di programmazione collaborativo'
objectives:
    - 'Individuare i vantaggi della community online di Scratch'
    - 'Incentivare la condivisione, il riutilizzo e la combinazione di codici'
    - 'Incentivare l’apprendimento collaborativo degli studenti'
---

## Unisciti alla community online di Scratch  e traine vantaggio
Presentazione e analisi dei benefici della community online di Scratch. Questa si caratterizza per una  [membership ampia e attiva](https://scratch.mit.edu/statistics/), concepita per utenti che vogliono creare e condividere progetti.

Oltre a condividere progetti dall’Editor Online di Scratch, gli utenti possono  visualizzare e modificare il progetto di un altro utente, facendosi allo stesso tempo ispirare dagli altri.
## Homework
Vai a [http://www.scratch.mit.edu](http://www.scratch.mit.edu) e accedi. Seleziona il titolo del progetto da condividere e rendilo accessibile a tutti gli utenti. 

Vai al catalogo Scratch, scegli un progetto e modificalo.
## Riferimenti
* [Linee guida della Community Scratch](https://en.scratch-wiki.info/wiki/Community_Guidelines)
* [Condivisione progettuale](https://en.scratch-wiki.info/wiki/Project_Sharing)
* [Remix](https://en.scratch-wiki.info/wiki/Remix)
