---
title: 'Open coding con il linguaggio grafico di programmazione gratuito Scratch'
objectives:
    - 'Scoprire i vantaggi di Scratch per lo sviluppo delle competenze del 21esimo secolo'
    - 'Progettare storie interattive, giochi e animazioni'
    - 'Apprendere autonomamente da tutorials e video'
    - 'Accedere alle risorse di Scratch e supportarle'
---

## Discussione di gruppo su “Programmare in Scratch?”
L’insegnante introdurrà il modulo chiedendo ai partecipanti della loro esperienza riguardo il linguaggio di programmazione.
## Progetta, crea e inventa con i nuovi media
_“Essere fluenti nel digitale non prevede soltanto la capacità di chattare, navigare ed interagire, ma anche l’abilità di progettare, creare ed inventare con i nuovi media” (…) . La capacità di programmare apporta grandi vantaggi. In particolare, la programmazione supporta il  “pensiero computazionale”, aiutandoti ad apprendere essenziali strategie di progettazione e problem-solving che trascendono l’ambito della programmazione._ ([Scratch: Programmazione per tutti](http://web.media.mit.edu/~mres/papers/Scratch-CACM-final.pdf)).

Presentazione e analisi dei vantaggi della programmazione. La programmazione in Scratch aiuta gli utenti a pensare creativamente, ragionare sistematicamente e collaborare – competenze essenziali per la vita nel 21esimo secolo.
* [Impara a programmare, programma per imparare](https://www.youtube.com/watch?v=Ok6LbV6bqaE)
* [Scratch: Liberare la Creatività attraverso la programmazione](https://www.youtube.com/watch?v=pwtN0DHB7RY)
* [Perchè la programmazione è così importante](https://www.forbes.com/sites/quora/2018/08/16/why-is-coding-so-important/#3866fc362adc)
* [Cos’è il pensiero computazionale?](https://www.youtube.com/watch?time_continue=1&v=sxUJKn6TJOI)

## Programmiamo!
L’introduzione ai fondamentali di Scratch (navigare l’interfaccia: stage area, blocks palette, e un’area di programmazione per posizionare e organizzare i blocchi in script eseguibili; creare sprites e background etc.).
### Attività 1. Cat welcome
I partecipanti si registrano al [Sito web di Scratch](https://scratch.mit.edu/about) (procedura semplice). 
I partecipanti dovranno accedere al loro account e svolgere le seguenti attività: 
1.	Collegarsi al sito web di [Scratch](https://scratch.mit.edu/) e accedere al proprio account. 
2.	Creare un progetto/file denominato Cat Welcome 
3.	Impostare il background e istruire il gatto Sprite usando i blocchi colorati di programmazione Scratch per chiedere “Come ti chiami?” e poi dare il benvenuto a chi risponde con  “Piacere di conoscerti + nome nella risposta”
4.	Una volta finito, aprire lo student report "Cat Welcome" e rispondere.

Come feedback, l’insegnante può mostrare la seguente immagine e/o fornire agli studenti il link al progetto condiviso: https://scratch.mit.edu/projects/306876279/

CODICE SCRATCH PER -CAT WELCOME

![](scratch%201.png)

### Attività 2. Cat Question
I partecipanti devono svolgere le seguenti attività:  
1.	Collegarsi al sito web [Scratch](https://scratch.mit.edu/) e accedere al proprio account. 
2.	Creare un progetto/file chiamato Cat Question
3.	Impostare il background e istruire il gatto Sprite usando I blocchi colorati di programmazione per far dire al gatto “Ciao” e poi fargli chiedere “Quando ha scoperto l’America Colombo?”. Se la risposta è sbagliata, il gatto dice “Riprova”. Se la risposta è giusta (1492) il gatto dice “Molto bene!!”
4.	Una volta finito, aprire lo Student report "Cat Question" e rispondere.

Come feedback, l’insegnante può mostrare l’immagine seguente e/o fornire agli studenti il link al progetto condiviso: https://scratch.mit.edu/projects/306876445/

CODICE SCRATCH PER CAT QUESTION

![](scratch%202.png)

## Accedi a risorse ed assistenza per Scratch
I partecipanti apprenderanno come programmare in Scratch con il supporto di tutorial online e di risorse. 
* [Apprendi a programmare, programma per imparare](https://scratch.mit.edu/projects/31407152)
* [Scratch Tutorial: Crea il tuo primo programma](https://www.youtube.com/watch?v=VIpmkeqJhmQ)
* [Tutorial introduttivo](https://scratch.mit.edu/help/videos/#Introductory%20Tutorials)

## Homework
I partecipanti dovranno svolgere le seguenti attività: 
1.	Collegarsi al sito web Scratch (https://scratch.mit.edu/) ed accedere al prorpio account. 
2.	Creare un progetto/file chiamato Mouse Grid.
3.	Scegliere un topo come Sprite ed una rete come sfondo.
4.	Fai muovere il mouse sulla rete con le frecce direzionali della tastiera: su o giù, destra o sinistra. Una volta finito, apri lo Student report “Mouse grid” e rispondi.

Come feedback, l’insegnante può mostrare la seguente imagine e/o fornire agli studenti il link al progetto condiviso: https://scratch.mit.edu/projects/306876524/

CODICE SCRATCH PER MOUSE GRID

![](scratch%203.png)

I partecipanti devono trovare i tutorial online necessari per svolgere l’attività.
## Riferimenti
* [Muovere i primi passi con Scratch](https://cdn.scratch.mit.edu/scratchr2/static/__6fc1c81d1c536d64cab16bd429100958__/pdfs/help/Getting-Started-Guide-Scratch2.pdf).
* [Tutorials per aiutarti](https://scratch.mit.edu/studios/174756/).
* [Programmare con Scratch](https://anthsperanza.com/2018/05/01/scratch-educator-guide/).