---
title: 'Filosofia Scratch: “Immagina, programma, condividi”'
objectives:
    - 'Far conoscere le basi della filosofia Scratch: I principi per creare, programmare e condividere materiale'
    - 'Esaminare l’utilizzo e i vantaggi della filosofia Scratch nell’educazione degli adulti'
---

## Discussione di gruppo
Nella parte introduttiva del modulo l’insegnante chiederà ai partecipanti della loro esperienza nell’ambito della programmazione. Le risposte verranno documentate e successivamente riutilizzate per personalizzare il materiale didattico.
## “Immagina, programma, condividi”
Il linguaggio di programmazione Scratch è stato sviluppato dal gruppo Lifelong Kindergarten al Massachusetts Institute of Technology (MIT) ed è utilizzabile gratuitamente.
Scratch è un [linguaggio di programmazione di tipo grafico](https://en.wikipedia.org/wiki/Visual_programming_language) e una community online. Nonostante sia stato concepito specificatamente per la fascia d’età 8-16, Scratch viene utilizzato da persone di tutte le età. Milioni di persone stanno creando progetti Scratch in molteplici contesti.
La prima versione di Scratch utilizzabile dal pubblico fu rilasciata nel 2013. La filosofia di Scratch incoraggia la condivisione, il riutilizzo e la combinazione di codici, come emerge dallo slogan “Immagina, Programma, Condividi”.
Il motto di Scratch segue il principio fondamentale della creazione di un progetto: Qualcuno elabora un’idea (“immagina”), programma l’idea in Scratch (“Programma”) e poi la pubblica nella community (“Condividi”). 
Dal lancio di [Scratch 2.0](https://en.scratch-wiki.info/wiki/Scratch_2.0), il motto è stato meno evidente nel sito web; la pagina principale non mostra più il motto, ma una descrizione di ciò che è Scratch.
Programmare in Scratch permette agli utenti di implementare i propri progetti ed utilizzare il progetto di qualcun altro. I progetti creati e modificati con Scratch sono concessi in licenza dalla [Creative Commons Attribution-Share Alike License](https://en.wikipedia.org/wiki/Creative_Commons_Attribution-Share_Alike_License). Scratch riconosce automaticamente il merito all’utente che ha creato il progetto  e il programma originali.
## Homework
I partecipanti esplorano il sito web di [Scratch](https://scratch.mit.edu/) nella lingua da loro scelta. Dovranno successivamente guardare dei video e prendere appunti:  
* [5 motivi per cui dovresti imparare a programmare in Scratch](https://www.youtube.com/watch?v=zCzotctwkwk).
* [Scratch come strumento per l’apprendimento creativo](https://www.youtube.com/watch?v=pDF2vxxK1L4&list=PLpfxVARjkP--eb4ddxhvSYuGZ1QaxSxvr&index=6).

I partecipanti sono invitati ad esprimere le loro opinioni sui principi e i vantaggi della filosofia Scratch.
## Riferimenti
* [Scratch Wiki](https://en.scratch-wiki.info/wiki/Scratch_Wiki)
* [Wikipedia](https://en.wikipedia.org/wiki/Scratch_(programming_language))
* [Scratch website](https://scratch.mit.edu/about)
