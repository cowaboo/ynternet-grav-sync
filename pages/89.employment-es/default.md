---
title: 'Recursos de FLOSS para la empleabilidad'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: es
materials:
    - 'Ordenador personal o teléfono inteligente o tableta digital conectada a Internet'
    - 'Conexión a internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
    - Altavoces
skills:
    'Palabras clave':
        subs:
            'Algoritmos y Empleabilidad': null
            'Curriculum Vitae': null
            Reclutamiento: null
            Habilidades: null
            Competencias: null
            'Oportunidades digitales': null
            Redes: null
            'Emprendimiento digital': null
            'Cultura de la donación': null
            Wikinomics: null
            'Campañas en línea': null
            'Recaudación de fondos': null
            Crowdsourcing: null
            'Colaboración masiva': null
objectives:
    - 'Presentar el marco de las habilidades del FLOSS para el empleo'
    - 'Presentar el marco de funcionamiento de la gestión de los Recursos Humanos'
    - 'Reconocer los beneficios y las limitaciones de los algoritmos en la gestión de recursos humanos'
    - 'Entender los algoritmos abiertos como un nuevo paradigma'
    - 'Comprender el uso de algoritmos en el proceso de reclutamiento de recursos humanos'
    - 'Comprender cómo adaptar un CV para tener una mejor aceptación por parte de los algoritmos'
    - 'Aprender cómo crear un buen CV en línea, un portafolio digital o un perfil de redes sociales'
    - 'Aprender a crear y actualizar un buen perfil laboral digital'
    - 'Comprender cuán importante es ser parte de una comunidad digital para aprovechar nuevas oportunidades económicas'
    - 'Comprender cómo está cambiando el mercado y cómo las oportunidades digitales pueden facilitar el espíritu empresarial y la marca personal'
    - 'Aprender nuevas formas de ser emprendedor con las prácticas y herramientas de FLOSS'
    - 'Saber cómo organizar una campaña de crowdfunding con recursos digitales'
    - 'Comprender diferentes modelos económicos como la cultura de la donación y la wikinomia, entre otros'
    - 'Mejorar la comunicación personal, las habilidades de colaboración y de marca'
length: '15 horas'
---

## Introducción
El 75 por ciento de las solicitudes de empleo son rechazadas antes de que sean vistas por los ojos humanos. Antes de que tu currículum llegue a las manos de una persona viva, a menudo debe aprobarse con lo que se conoce como un sistema de seguimiento de candidatos. Un sistema de seguimiento de solicitantes, o ATS, para abreviar, es un tipo de software utilizado por reclutadores y empleadores durante el proceso de contratación para recopilar, clasificar, escanear y clasificar las solicitudes de empleo que reciben para sus puestos vacantes. 

Elegir a la persona adecuada para un puesto de trabajo puede ser un desafío. Los algoritmos son, en parte, nuestras opiniones integradas en forma de código de computadora. Los algoritmos consideran el simple hecho de que la contratación es esencialmente un problema de predicción. Cuando un gerente lee los currículums vitae de los solicitantes de empleo, está tratando de predecir cuales, de entre las personas solicitantes, tendrán un buen desempeño en el trabajo y cuáles no. Los algoritmos estadísticos están diseñados para predecir problemas / costos y pueden ser útiles para mejorar la toma de decisiones humanas. Los algoritmos también pueden tener un lado más oscuro, ya que reflejan prejuicios y prejuicios humanos que conducen a errores de aprendizaje automático y malas interpretaciones (perpetuando y reforzando la discriminación en las prácticas de contratación). 

Lo que resulta más relevante ahora no es si usar o no algoritmos para seleccionar candidatos, sino cómo aprovechar al máximo las máquinas. Los principios fundamentales de la redacción de currículums permanecen constantes, y así ha sido durante generaciones, pero las tecnologías en evolución muestran que, ahora más que nunca, muchos aspectos de la solicitud y de los procesos de contratación se realizan de forma digital. Al mantenerse al día en relación a las prácticas actuales, las personas participantes estarán mejor preparadas para postularse como candidatos. 

Por otro lado, en internet, las posibilidades de generación de ingresos son infinitas. Necesitamos identificar un nicho, desarrollar un plan de negocios y trabajar duro para tener éxito. Existen numerosas iniciativas, con diferentes modelos de negocio, que una persona emprendedora puede desarrollar en Internet; por otra parte, cada organización y cada proyecto -presencial o digital- necesita fuentes de ingresos. En este módulo también nos centraremos en la cultura de donaciones, wikinomics, en la recaudación de fondos y en otros modelos económicos nacidos en el corazón de la web 2.0.
## Contexto
El objetivo de la sesión es demostrar y motivar al alumnado a explorar:
* Presentar el marco de las habilidades del FLOSS para el empleo.
* Presentar el marco de funcionamiento de la gestión de los Recursos Humanos.
* Reconocer los beneficios y las limitaciones de los algoritmos en la gestión de recursos humanos.
* Entender los algoritmos abiertos como un nuevo paradigma.
* Comprender el uso de algoritmos en el proceso de reclutamiento de recursos humanos.
* Comprender cómo adaptar un CV para tener una mejor aceptación por parte de los algoritmos.
* Aprender cómo crear un buen CV en línea, un portafolio digital o un perfil de redes sociales.
* Aprender a crear y actualizar un buen perfil laboral en línea.
* Comprender cuán importante es ser parte de una comunidad en línea para aprovechar nuevas oportunidades económicas.
* Comprender cómo está cambiando el mercado y cómo las oportunidades en línea pueden facilitar el espíritu empresarial y la marca personal.
* Aprender nuevas formas de ser emprendedor con las prácticas y herramientas de FLOSS.
* Saber cómo ejecutar una campaña de crowdfunding en línea.
* Comprender diferentes modelos económicos como la cultura de la donación y la wikinomia, entre otros.
* Mejorar la comunicación personal, las habilidades de colaboración y de marca.

## Sesiones
### Primera sesión: Algoritmos y empleabilidad
El objetivo de la sesión es presentar los conceptos de algoritmos y empleabilidad y cómo adaptarse al nuevo paradigma que se produce en la selección de los RR. Esta sesión generará debates sobre el problema que plantean los algoritmos de recursos humanos y también sobre las soluciones que aportan. El objetivo principal de esta sesión es plantear a las personas participantes la idea de que los algoritmos de reclutamiento seguirán creciendo y que necesitamos adaptarnos mejor a ellos para tener éxito.
### Segunda sesión: Cómo usar las prácticas de FLOSS y crear un currículum vitae atractivo para algoritmos
El enfoque de esta sesión será cómo crear un currículum en línea. Las participantes serán invitadas a crear su propio CV utilizando plataformas y herramientas digitales. Además, las participantes descubrirán cómo presentar sus competencias y habilidades al crear un buen CV en línea que sea atractivo tanto para humanos como para máquinas.
### Tercera sesión: Oportunidades de FLOSS y emprendimiento en línea
En esta sesión, las participantes tendrán la oportunidad de descubrir muchas otras oportunidades que el mundo digital crea para desarrollar una carrera y ideas de proyectos. El objetivo principal de esta sesión será analizar cómo el emprendimiento en línea y el mundo digital ofrecen oportunidades para recaudar dinero. Se explorarán diferentes modelos económicos como el crowdfunding, la cultura de donaciones y el modelo Freemium.