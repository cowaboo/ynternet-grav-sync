---
title: 'Risorse FLOSS per l’occupazione'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: it
materials:
    - 'Personal computer (Smartphone o tablet connesso ad internet)'
    - 'Connessione Internet'
    - Proiettore
    - 'Carta e penna'
    - Flipchart
    - Casse
skills:
    'Parole chiave':
        subs:
            'Algoritmi e occupazione': null
            'Curriculum Vitae': null
            Assunzione: null
            Capacità: null
            Competenze: null
            'Opportunità Online': null
            Networking: null
            'Imprenditorialità digitale': null
            'Cultura della donazione': null
            Wikinomics: null
            'Campagne online': null
            'Raccolta fondi': null
            Crowdsourcing: null
            'Collaborazione di massa': null
objectives:
    - 'Fornire un background delle competenze FLOSS per l’impiego'
    - 'Fornire un background ai metodi di Risorse Umane'
    - 'Riconoscere benefici e vincoli degli algoritmi in ambito HR'
    - 'Comprendere algoritmi aperti come nuovo paradigma'
    - 'Comprendere l’uso di algoritmi nel processo di recruitment'
    - 'Comprendere come adattare un CV per avere una maggiore probabilità di accettazione da parte degli algoritmi'
    - 'Apprendere come creare un buon CV online, un portfolio o un profilo sui social media'
    - 'Apprendere come creare e aggiornare un buon profile di lavoro online'
    - 'Comprendere quanto è importante far parte di una community online per cogliere nuove opportunità economiche'
    - 'Comprendere i cambiamenti del mercato e come le opportunità online possono facilitare l’imprenditorialità e il personal  branding'
    - 'Apprendere nuove modalità di imprenditorialità con le pratiche e gli strumenti FLOSS'
    - 'Conoscere come gestire una campagna di crowdfunding online'
    - 'Comprendere diversi modelli economici, come, tra le altre, la cultura della donazione e la wikinomics'
    - 'Conoscere come migliorare la comunicazione personale, collaborazione e capacità di branding'
length: '15 ore'
---

## Introduzione
Il 75% delle domande di lavoro viene respinto prima di essere viste da occhi umani. Prima che il tuo curriculum arrivi nelle mani di una persona fisica, spesso deve passare attraverso il cosiddetto sistema di monitoraggio dei candidati. Un sistema di monitoraggio dei candidati - o ATS, in breve - è un tipo di software utilizzato dai selezionatori e dai datori di lavoro durante il processo di assunzione per raccogliere, ordinare, analizzare e classificare le domande di lavoro che ricevono per le posizioni aperte.

Scegliere la persona giusta per una posizione di lavoro può essere difficile. Gli algoritmi sono, in parte, le nostre opinioni incorporate in una forma di codice informatico. Gli algoritmi considerano il semplice fatto che assumere è essenzialmente un problema di previsione. Quando un manager legge il curriculum vitae dei candidati, cerca di prevedere quali candidati si comporteranno bene nel lavoro e quali no. Gli algoritmi statistici sono costruiti per la previsione di problemi/costi e possono essere utili per migliorare il processo decisionale umano. Gli algoritmi possono anche avere un lato oscuro in quanto riflettono inclinazioni e pregiudizi umani che portano a errori di apprendimento automatico e interpretazioni errate (perpetuando e rafforzando la discriminazione nelle pratiche di assunzione). La questione ora non è se usare algoritmi per selezionare i candidati, ma come ottenere il massimo dalle macchine.

I principi fondamentali della scrittura del curriculum rimangono costanti per generazioni, ma l'evoluzione delle tecnologie comporta che sempre un maggior numero di processi di candidatura e assunzione avvengono online. Rimanendo aggiornati con le migliori pratiche attuali, i partecipanti saranno meglio preparati a candidarsi alle offerte di lavoro. 
  
 In Internet, le possibilità di generazione di reddito sono infinite. Abbiamo bisogno di identificare una nicchia, sviluppare un business plan e lavorare sodo per avere successo. Ci sono numerose attività imprenditoriali che un imprenditore digitale può sviluppare, il modello di business non definisce un imprenditore digitale. 
Ogni organizzazione e progetto ha bisogno di fonti di reddito per implementare le attività, sia che si tratti di attività faccia a faccia o online. In questo modulo ci concentreremo anche sulla cultura della donazione, la wikinomics, la raccolta fondi e altri modelli economici al centro del web 2.0.
## Contesto
L’obiettivo della sessione è dimostrare praticamente agli studenti, coinvolgendoli, come:
* Fornire un background delle competenze FLOSS per l’impiego
* Fornire un background ai metodi di Risorse Umane
* Riconoscere benefici e vincoli degli algoritmi in ambito HR
* Comprendere algoritmi aperti come nuovo paradigma
* Comprendere l’uso di algoritmi nel processo di recruitment
* Comprendere come adattare un CV per avere una maggiore probabilità di accettazione da parte degli algoritmi
* Apprendere come creare un buon CV online, un portfolio o un profilo sui social media
* Apprendere come creare e aggiornare un buon profile di lavoro online
* Comprendere quanto è importante far parte di una community online per cogliere nuove opportunità economiche
* Comprendere i cambiamenti del mercato e come le opportunità online possono facilitare l’imprenditorialità e il personal  branding
* Apprendere nuove modalità di imprenditorialità con le pratiche e gli strumenti FLOSS
* Conoscere come gestire una campagna di crowdfunding online
* Comprendere diversi modelli economici, come, tra le altre, la cultura della donazione e la wikinomics
* Conoscere come migliorare la comunicazione personale, collaborazione e capacità di branding

## Sessioni
### Prima sessione: Algoritmi e occupabilità
Lo scopo della sessione è presentare i concetti di algoritmi e occupabilità, e al contempo fornire una guida sul come adattarsi ad un nuovo mondo delle risorse umane in cui oltre a dover fare colpo sui reclutatori, chi cerca lavoro deve risultare qualificato anche per le machine che questi utilizzano. Questa sessione lascerà spazio a discussion e dibattiti sui problemi che gli algoritmi HR comportano, ma anche sulle soluzioni che apportano. L’obiettivo principale è quello di far aprire I partecipanti all’idea che gli algoritmi di recruitment saranno sempre più presenti ed è necessario adattarvisi per avere successo.
### Seconda sessione: Come impiegare le pratiche FLOSS e creare un CV accattivante per gli algoritmi
Il centro di questa sessione sarà come creare un CV online. 
I partecipanti saranno chiamati a creare il loro curriculum usando piattaforme e strumenti digitali; apprenderanno anche come presentare le loro competenze e capacità in un CV online in modo che questo risulti interessante sia per le macchine che per i recruiters.
### Terza sessione: FLOSS Opportunità e Imprenditorialità digitale
In questa sessione i partecipanti avranno la possibilità di scoprire molte altre opportunità offerte dal mondo digitale per lo sviluppo di una carriera e di idee progettuali. 
L’attenzione di questa sessione sarà sull’imprenditorialità digitale e su come il mondo digitale crei opportunità per raccogliere fondi. Verranno trattati diversi modelli economici, come il crowdfunding, le donazioni ed il modello Freemium.