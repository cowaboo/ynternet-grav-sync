---
title: 'Intentional interpersonal communication with FLOSS NL'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: nl
objectives:
    - 'De goede praktijken van opzettelijke interpersoonlijke communicatie'
    - 'Hoe de FLOSS-cultuur zich verhoudt tot de regels van de sociale media en het gemeenschappelijke gebruik van informatie'
    - 'Voorbeelden van opzettelijke online communicatie zoals de Wikipedia-gemeenschap'
materials:
    - 'PC (Smartphone of Tablet)'
    - Internetverbinding
    - Projector
    - 'Papier en schrijfgerief'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    Sleutelwoorden:
        subs:
            FLOSS-beweging: null
            FLOSS-cultuur: null
            Netizenship: null
            'Opzettelijke communicatie': null
            Burgeremancipatie: null
            'Betrokkenheid van de Gemeenschap': null
            'FLOSS-instrumenten en -vaardigheden': null
length: '10 uren'
---

## Introduction
De digitale technologie heeft alles veranderd. Herinner je je je eerste blik op het web? Je leest pagina's met informatie, zoals het lezen van een boek. Je behandelde de berichten zoals een mail wordt verwerkt. En gaandeweg realiseerde je je dat je niet zoals de anderen met een communicatiemiddel te maken had. Een enkele gebruiker van een postkantoor of een virtuele winkel, je voelde je geleidelijk aan een acteur worden... 
Uit het boek [Citoyens du Net](https://www.ynternet.org/page/livre).

In 2019 werd het web 30 jaar oud. Met zijn honderden miljoenen forums, wiki's, blogs, sociale netwerken, microblogs  rechtvaardigt hij nu het praten over de webosfeer. Google, Facebook, Twitter en Wikipedia zijn de bekendste planeten. Het web wordt bevolkt door honderden miljarden en miljarden artikelen uit verschillende velden (titel, berichttekst, bijlagen, afbeeldingen, aantal bezoekers, notities) en is het hart dat alle digitale tools die een open interface hebben, gestandaardiseerde open toegang en toegankelijk zijn voor alle bestaande tools - smartphones, tablets, computers en verbonden dingen - met elkaar in contact brengen. De meest interessante plaatsen zijn die waar je kunt communiceren door commentaar te geven, wijzigingen aan te brengen, tekst of afbeeldingen toe te voegen in deze materie. Opzettelijke communicatie voor civic empowerment en community engagement met FLOSS biedt een levensvatbaar alternatief.
## Context
Het doel van de sessie is om de leerlingen praktisch te demonstreren en te betrekken bij het verkennen: 
* Van de goede praktijken van opzettelijke interpersoonlijke communicatie.
* Hoe de FLOSS-cultuur zich verhoudt tot de regels van de sociale media en het gemeenschappelijke gebruik van informatie.
* Van voorbeelden van opzettelijke communicatie online zoals de Wikipedia-gemeenschap.

De leerling kan de achtergrond, de geschiedenis, de kenmerken en de stappen van de doelbewuste communicatie in relatie tot de webcultuur beschrijven. Hij kan het belang van de overgang van consument naar speler in de informatiemaatschappij onder woorden brengen.
## Sessies
### Eerste sessie: Opzettelijke interpersoonlijke communicatie
Deze sessie zal het gebruik van vrije en open digitale technologieën voor doelbewuste communicatie in het dagelijkse leven demonstreren. Het zal leerlingen in staat stellen om hun doelbewuste communicatievaardigheden te realiseren en te ontwikkelen en vrije en open digitale technologieën te gebruiken om doelbewuste communicatie praktijken en -gewoonten te ontwikkelen.
### Tweede sessie: De FLOSS manier: hoe gebruiken mensen FLOSS-praktijken en -tools in hun projecten
De tweede sessie zal zich richten op een kritische denkwijze rond doelbewuste communicatie in verschillende sociale en culturele omgevingen. Het zal de methoden van online samenwerking in kleine groepsactiviteiten verkennen om van elkaar te leren en een doelbewuste communicatiecultuur te ondersteunen.
## Derde sessie: FLOSS in je leven
Deze sessie zal de deelnemers de mogelijkheid bieden om hun doelbewuste communicatievoorkeuren te ontwikkelen en te delen. We bezoeken intentionele communicatie als basis voor synergetische participatie (met het voorbeeld van collaboratieve notitie nemen) en koppelen dit aan FLOSS tools die de deelnemers kunnen dienen bij de huidige activiteiten.