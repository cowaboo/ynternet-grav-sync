---
title: 'Comunicació interpersonal intencional amb FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: ct
objectives:
    - 'Bones pràctiques de comunicació interpersonal intencial'
    - 'Com la cultura del FLOSS es relaciona amb les regles de les xarxes socials i els usos comuns de la informació'
    - 'Casos de comunicació intencional en línia com la comunitat Viquipèdia'
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògraf
    - 'Acadèmia Slidewiki'
skills:
    'Paraules clau':
        subs:
            'Moviment FLOSS': null
            'Cultura del FLOSS': null
            'La ciutadania': null
            'Comunicació intencional': null
            'Empoderament cívic': null
            'Participació de la comunitat': null
            'Eines i habilitats del FLOSS': null
length: '10 hores'
---

## Introducció
La tecnologia digital ho ha canviat tot. Recordes la teva primera visita a un web? Per aquell temps, llegies les pàgines d'informació com si fos un llibre i els missatges com si fossin una carta postal. I gradualment, t'adonaves que no estaves tractant amb un mitjà de comunicació com els altres. L'usuari d'una oficina de correus o el client d'una botiga virtual ... es van convertir gradualment en actors... Del llibre [Citoyens du Net](https://www.ynternet.org/page/livre).

En 2019, el web (World Wide Web) va complir 30 anys. Amb els seus centenars de milions de fòrums, wikis, blocs, xarxes socials, microblogs instantanis, ara està justificat parlar de la webesfera. Google, Facebook, Twitter i Wikipedia són els "planetes" més famosos. Poblada per centenars de milers de milions de continguts amb diferents camps (títol, cos del missatge, arxius adjunts, imatges, nombre de visitants, notes), el web és el cor en el qual convergeixen totes les eines digitals que tenen una interfície oberta, accés obert estandarditzat i accessible per a totes els recursos existents: telèfons intel·ligents, tauletes, ordinadors i coses connectades. Els llocs més interessants són aquells on es pot interactuar comentant, canviant, afegint text o imatges relacionades amb aquests. La comunicació intencional per l'empoderament cívic i el compromís de la comunitat amb el FLOSS estableix una alternativa viable als mitjans de comunicació tradicionals i als recursos que presenten restriccions a la lliure circulació de les idees i el coneixement.
## Context
L'objectiu de la sessió és demostrar i motivar l'alumnat a explorar:
* Les bones pràctiques de comunicació interpersonal intencional.
* Com es relaciona la cultura de programari lliure amb les regles de les xarxes socials i els usos comuns de la informació.
* Casos de comunicació intencional en línia com la comunitat de Vikipèdia.

L'alumnat podrà descriure els antecedents, la història, les característiques i els passos de la comunicació intencional en relació amb la cultura web. Podrà articular la importància de passar de consumidor a actor de la societat de la informació.
## Sessions
### Primera sessió: Comunicació interpersonal intencional
Aquesta sessió demostrarà l'ús de tecnologies digitals gratuïtes i obertes per a la comunicació intencional en la vida quotidiana. Permetrà a les estudiants prendre consciència de les seves capacitats i desenvolupar les seves habilitats de comunicació intencional i utilitzar tecnologies digitals obertes i gratuïtes per a desenvolupar pràctiques i hàbits de comunicació intencional.
### Segona sessió: L'estil de comunicació FLOSS: com utilitzen les persones les pràctiques i eines del FLOSS en els seus projectes
Aquesta sessió es centrarà en un enfocament de pensament crític al voltant de la comunicació intencional en diversos entorns socials i culturals. Explorarà mètodes de col·laboració en línia en activitats de grups petits per aprendre els uns dels altres i donar suport a una cultura de comunicació intencional.
### Tercera sessió: El FLOSS en la teva vida
Aquesta sessió permetrà a les participants desenvolupar i compartir les seves preferències de comunicació intencional. Analitzarem la comunicació intencional com a base de la participació sinèrgica (prenent com a exemple la presa de notes col·laboratives) i la vincularem a les eines del FLOSS que poden servir als participants en les activitats actuals.