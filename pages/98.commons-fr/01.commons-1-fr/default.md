---
title: 'Définition des communautés de pratique (CoP)'
objectives:
    - 'Explorer la relation de la CoP à Commons'
    - 'Fournir un contexte et une compréhension de la CoP'
    - 'Analyser des études de cas spécifiques'
---

## Introdution
Le formateur commencera l'introduction du module en interrogeant les participants sur leurs expériences actuelles et antérieures sur les communautés de pratique dans les projets, le travail et la vie de famille. L'objectif principal de cette activité est de cartographier la pléthore de pratiques de communication actuelles et de les réutiliser pendant les activités du cours
## Définir CoP
Les communautés de pratique sont formées par des personnes qui s'engagent dans un processus d'apprentissage collectif dans un domaine partagé de l'activité humaine: une tribu apprenant à survivre, un groupe d'artistes à la recherche de nouvelles formes d'expression, un groupe d'ingénieurs travaillant sur des problèmes similaires, une clique d'élèves définissant leur identité à l'école, un réseau de chirurgiens explorant de nouvelles techniques, un rassemblement de nouveaux managers qui s'entraident.

En résumé: les communautés de pratique sont des groupes de personnes qui partagent une préoccupation ou une passion pour quelque chose qu'elles font et qui apprennent à mieux le faire en interagissant régulièrement.
Les communautés développent leur pratique à travers une variété d'activités. Le tableau suivant fournit quelques exemples typiques :
* Résolution de problème.
* Demandes d'informations.
* Recherche d'expérience.
* Réutilisation des actifs.
* Coordination et stratégie.
* Construire un argument.
* Confiance croissante.
* Discuter des développements.
* Documenter les projets.
* Visites.
* Cartographie des connaissances et identification des lacunes.

## Concevez votre propre communauté de pratique (travail en groupe)
Dans cette activité, nous utiliserons les méthodologies de co-conception existantes pour que les participants conçoivent et proposent leur propre scénario de communication intentionnelle.
## Devoirs
Visitez l'initiative XES: le réseau d'économie solidaire de Catalogne et postez un court texte décrivant cette activité.
## Références
* Cultivating communities of practice: a guide to managing knowledge. By Etienne Wenger, Richard McDermott, and William Snyder, Harvard Business School Press, 2002.
* Communities of practice: the organizational frontier. By Etienne Wenger and William Snyder. Harvard Business Review. January-February 2000, pp. 139-145.