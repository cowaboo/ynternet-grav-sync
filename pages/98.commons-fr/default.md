---
title: 'Communauté de pratique (Commons + gestion collaborative)'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Community of practice (Commons + collaborative management)'
lang_id: 'Community of practice (Commons + collaborative management)'
lang_title: fr
length: '9 heures'
objectives:
    - 'Démontrer et engager pratiquement les apprenants à explorer les bonnes pratiques de gestion de la CoP'
    - 'Démontrer et engager pratiquement les apprenants à explorer pour reconnaître la présence des CoP dans les actions quotidiennes'
    - 'Connaître les outils utilisés par les CdP et les pratiques de gestion des biens communs qui leur sont associées'
materials:
    - 'Ordinateur personnel'
    - 'Connexion internet'
    - Projecteur
    - 'Papier and stylos'
    - Flipchart
    - 'Academie Slidewiki'
skills:
    'Mots clés':
        subs:
            'Mouvement FLOSS': null
            'Culture FLOSS': null
            'Communautés de pratique': null
            Commons: null
            'Gestion collaborative': null
            'Autonomisation civique': null
            'Engagement communautaire': null
            'Outils et compétences FLOSS': null
---

## Introduction
Dans ce module, nous explorerons les communautés de pratique (CoP) en tant que groupes de personnes partageant un métier ou une profession. Une grande partie de la théorie derrière le concept a été développée par le théoricien de l'éducation Etienne Wenger, tandis qu'un avantage important pour un CoP est la capture de connaissances tacites. La motivation à participer à un CoP peut inclure des retours tangibles (promotion, augmentations ou bonus), des retours intangibles (réputation, estime de soi) et l'intérêt de la communauté (échange de connaissances liées à la pratique, interaction). Les communautés de pratique sont importantes pour initier et établir une culture commune. Ce module est destiné à nous montrer comment cela se produit déjà. Nous relierons ce domaine à une présentation des Communes sur la philosophie et la pratique globales et explorerons le rôle de la gestion collaborative comme élément important.
## Contexte
Le but de la session est de démontrer et d'engager pratiquement les apprenants à explorer:
* Les bonnes pratiques de gestion de la CoP.
* Reconnaître la présence des CoP dans les actions quotidiennes.
* Apprenez à connaître les outils utilisés par les CdP et les pratiques de gestion des biens communs qui leur sont associées.

## Séances
### Première séance : Définir les communautés de pratique (CoP)
Cette séance permettra de définir et de décrire la CoP et d'explorer leur relation avec Commons. Il fournira un contexte et une compréhension des limites et des défis de l'initiation et du jardinage des CoP
### Deuxième séance : (Digital) Commons
La deuxième séance se concentrera sur une approche de pensée critique autour des biens communs (numériques) et des méthodes de gestion basées sur les biens communs.
### Troisième séance : Vivre avec CoP
Cette séance permettra aux participants d'étudier des initiatives spécifiques et comment les CoP y jouent un rôle, de se familiariser avec les outils utilisés par les CoPs et de comprendre les contrats sociaux qui les sous-tendent.