---
title: 'Vivre avec CoP'
objectives:
    - 'Fournir une analyse critique des cas de CoP'
    - 'Examiner l''utilisation et les avantages de la gestion collaborative'
---

## Les affaires comme communs
Les participants réfléchiront sur différents modèles de gestion d'entreprise autour d'eux. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## Business as communs
Cette section montre que l'avenir est dans les affaires en tant que biens communs. Dans un monde où les modèles économiques évoluent et même la notion traditionnelle de travail a perdu son adéquation avec les paradigmes actuels.
### Gouvernance et organisation de l'Open Source Libre / Libre
Les premières études pour expliquer comment les communautés FLOSS s'organisent sont issues du domaine du génie logiciel, dans les travaux de Raymond (2001) décrivant les deux différents modèles de développement de «La Cathédrale» et «Le Bazar». Le concept de modèle de bazar a ensuite été développé par Demil et Lecocq (2006), dans le domaine des études d'organisation et de gestion. Demil et Lecocq (2006) ont proposé qu'une nouvelle structure de gouvernance générique soit envisagée: la gouvernance du bazar. Ils ont caractérisé cette forme de gouvernance et discuté des forces et des faiblesses de la structure du bazar par rapport à celle du marché. Ils ont suggéré que la façon dont ces communautés sont gouvernées soit répartie, permettant à leurs membres de participer, en tenant compte d'un ensemble diversifié d'intérêts.
### Organisations plates
Une organisation plate (également connue sous le nom d'organisation horizontale) a une structure organisationnelle avec peu ou pas de niveaux de gestion intermédiaire entre le personnel et les cadres. La structure d'une organisation fait référence à la nature de la répartition des unités et des postes en son sein, ainsi qu'à la nature des relations entre ces unités et ces postes. Les organisations hautes et plates diffèrent en fonction du nombre de niveaux de gestion présents dans l'organisation et du niveau de dotation des gestionnaires de contrôle.
### Définitions de gestion davantage axées sur les communs
Production collaborative: se référant au CBPP comme mode de production par lequel un ensemble d'individus produisent quelque chose de précieux qui n'existait pas avant leur interaction.
Basé sur les pairs: l'interaction dans CBPP n'est pas uniquement ou principalement coordonnée par des relations contractuelles, ni coordonnée de manière hiérarchique. Les tâches sont basées sur la création libre et l'auto-affectation. La gamme des motivations est diverse et peut être intrinsèque (par exemple pour le plaisir) ou extrinsèque (par exemple pour gagner du capital social), mais elles ne sont pas principalement basées sur des obligations contractuelles ou des forces de coercition.

Basé sur les biens communs: le CBPP est caractérisé non seulement comme un processus de production par les pairs, mais aussi comme un processus commun, qui est motivé par l'intérêt général. Par exemple, dans les environnements numériques, cela se traduit par l'ouverture des ressources communes.
Favoriser la reproductibilité: caractérisé pour favoriser la reproductibilité des biens créés, ainsi que les méthodologies et les pratiques entre autres.
## Activité en groupe
Regardez la vidéo sur [Business as Commons - Samantha Slade](https://www.youtube.com/watch?v=1qkhWa9XoFo).
Étudiez, documentez et discutez ses principaux arguments. Créez un document commun avec tous les participants, partagez-le avec les autres.
## Devoirs
Recherchez et documentez des experts et / ou des articles sur la gestion collaborative. Publiez deux liens vers notre groupe Diigo de sensibilisation collective : [Diigo](https://groups.diigo.com/group/e_culture)
## Références
* [The future is in business as commons | Samantha Slade | TEDxGeneva](https://www.youtube.com/watch?v=1qkhWa9XoFo)
* Bauwens, M. (2005). [The political economy of peer production](https://mmduvic.ca/index.php/ctheory/article/view/14464/5306)
* Bauwens, M. & Kostakis, V. (2014). From the communism of capital to capital for the commons: towards an open co-operativism. tripleC: Communication, Capitalism & Critique. Open Access Journal for a Global Sustainable Information.
* [Society, 12(1), 356–361](http://triple-c.at/index.php/tripleC/article/view/561) 