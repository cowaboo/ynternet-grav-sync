---
title: 'Communs (numériques)'
objectives:
    - 'Fournir une définition du bien commun'
    - 'Analyser l''impact de Decidim : démocratie participative open source gratuite pour les villes et les organisations'
    - 'Discuter d''autres initiatives sur les biens communs numériques telles que Wikipedia, Drupal et d''autres'
---

## Discussion de groupe sur «les communs dans votre vie aujourd'hui?»
Le formateur commencera l'introduction du module en interrogeant les participants sur les biens communs quotidiens. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## Définir le commun
Dans cette section, nous définirons les biens communs. Le travail d’Ostrom montre comment, dans certaines conditions, les biens communs peuvent en effet être gérés de manière durable par les communautés locales de pairs. Son approche tient compte du fait que les agents individuels ne fonctionnent pas de manière isolée, et ne sont pas uniquement motivés par leur propre intérêt, c'est-à-dire au-delà des approches homo-economicus. Au lieu de cela, elle soutient que les communautés communiquent pour établir des protocoles et des règles communs qui garantissent leur durabilité. Dans le cadre de ce travail, elle a identifié un ensemble de principes (Ostrom, 1990) pour une gestion réussiede ces communs:
1. Limites de la communauté clairement définies: afin de définir qui a les droits et
privilèges au sein de la communauté.
2. Congruence entre les règles et les conditions locales: les règles qui régissent le comportement ou l'utilisation des biens communs dans une communauté doivent être flexibles et basées sur des conditions locales qui peuvent changer avec le temps. Ces règles devraient être intimement associées aux biens communs, plutôt que de s'appuyer sur une réglementation «universelle».
3. Arrangements de choix collectifs: afin de réaliser au mieux la congruence (principe numéro 2), les personnes concernées par ces règles devraient pouvoir participer à leur modification et les coûts de modification devraient être maintenus bas.
4. Surveillance: certaines personnes au sein de la communauté agissent en tant que contrôleurs du comportement conformément aux règles dérivées des accords de choix collectifs, et elles devraient être responsables devant le reste de la communauté.
5. Sanctions graduelles: les membres de la communauté se surveillent activement et se sanctionnent mutuellement lorsqu'un comportement est en conflit avec les règles de la communauté. Les sanctions contre les membres qui enfreignent les règles correspondent à la gravité perçue de l'infraction.
6. Mécanismes de résolution des conflits: les membres de la communauté devraient avoir accès à des espaces à faible coût pour résoudre les conflits.
7. Application locale des règles locales: la compétence locale pour créer et appliquer des règles devrait être reconnue par les autorités supérieures.
8. Plusieurs couches d'entreprises imbriquées: en formant plusieurs couches organisationnelles imbriquées, les communautés peuvent résoudre les problèmes qui affectent différemment la gestion des ressources aux niveaux plus large et local.

Bien que ces principes aient été définis à l'origine pour les biens communs naturels, ils ont également été discuté et adapté pour l'étude des communautés qui développent et maintiennent le numérique communs, tels que Wikipedia (Viégas et al., 2007; Forte et al., 2009) ou des logiciels libres / libres comme Drupal (Rozas, 2017). Ce processus d’utilisation des principes d’Ostrom dans l’étude de la manière dont les communautés gèrent les biens communs numériques nécessitait cependant une analyse de réinterprété dans ce contexte différent.
## Activité de groupe
Visitez la communauté Decidem avec les participants du cours et documentez les résultats dans un document commun.
## Devoirs
Publier un article sur Decidim dans le groupe [Diigo - eculture](https://groups.diigo.com/group/e_culture)
## Références
* Arvidsson, A., Caliandro, A., Cossu, A., Deka, M., Gandini, A., Luise, V., & Anselmi,
G. (2017). [Commons based peer production in the information economy (Report)](https://www.academia.edu/29210209/Commons_Based_Peer_Production_in_the_Information_Economy).
* Ostrom, E. (1990). Governing the Commons: The Evolution of Institutions for Collective Action . Cambridge University Press 
* Ostrom, E. (2000). Collective action and the evolution of social norms. Journal of economic perspectives , 14 (3), 137-158
* Rozas, D. (2017). [Self-organisation in Commons-Based Peer Production: Drupal : "the drop is always moving"](http://epubs.surrey.ac.uk/845121/). (Doctoral dissertation, University of Surrey).