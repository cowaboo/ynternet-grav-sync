---
title: 'Iedereen heeft een verhaal…'
---

## 1.1	Opwarmen met DIXIT
Om een verhaal te vertellen en creatief te werken, is een zekere mate van vertrouwen nodig in de mensen om je heen. Daarom is het belangrijk om de workshop digital storytelling te starten met een inleidende activiteit die de deelnemers motiveert om iets over zichzelf te vertellen ondersteund door “ijsbreker” activiteiten die een vriendelijke sfeer creëren.
![](dixit.png)
Het komt vaak voor dat je met een groep mensen werkt die elkaar niet kennen en zelfs als dat niet het geval is, is het ook mogelijk dat ze niet gewend zijn om in een groep te praten. Een goede ijsbreker is het vertelspel Dixit. Het is van oorsprong een leuk klein spel met prachtige tekeningen die op veel manieren kunnen worden geïnterpreteerd.
Leg de kaarten met de afbeeldingen naar boven op tafel. **Laat iedereen drie kaarten kiezen die de dag tot nu toe illustreren**.

Geef ze wat tijd en als elke persoon drie kaarten heeft, ga dan rond de tafel of in een cirkel zitten. Ga rond, laat iedereen zichzelf presenteren en laat hun korte verhaal van de dag vertellen. Het is een gemakkelijke manier om mensen aan het praten te krijgen, aangezien ze afbeeldingen hebben die hun verhaal ondersteunen. Het creëert ook een sfeer waarin mensen naar elkaar luisteren en met elkaar praten. Met deze kleine vraag komen al een aantal leuke verhaaltjes naar buiten en het is ook (nog) niet te persoonlijk dus het is een makkelijke eerste stap om een veilige omgeving te creëren. Dit is een belangrijke stap in het Digital Storytelling-proces.

In een digitale vertelsessie nam een deelnemer bijvoorbeeld een kaart met een enorm ijzeren hek erop dat maar een klein stukje open was en er kwam wat licht uit. Voor de poort stond een kleine man gekleed als een ridder die door de poort gluurde. Het meisje dat deze kaart nam, zei dat ze zich de ridder voelde, maar ook de deur. Ze voelt zich de ridder door het onbekende avontuur dat achter de deur wacht. En ze voelt zich als de deur omdat ze weet dat ze mensen in haar comfortzone moet laten en ze weet niet zeker of ze de deur al wil openen. Een eenvoudige kaart kan op veel manieren worden geïnterpreteerd en de resultaten zijn meestal verbluffend.
Het spel kan online worden gekocht bij bijvoorbeeld Amazon of een andere winkel die bordspellen verkoopt, aangezien het behoorlijk populair is.

Internationale Amazon-link: 
Om geld te besparen is het mogelijk om gewoon een uitbreidingspakket van Dixit te kopen, aangezien het alleen Dixit-kaarten bevat en niet het scorebord en pionnen die hiervoor niet nodig zijn.
Bijvoorbeeld deze versie: https://www.amazon.com/Dixit-Cover-Art-May-Vary/dp/2914849656/ref%3Dsr_1_1?ie=UTF8&qid=1473839224&sr=8-1&keywords=Dixit

Er zijn andere vertel spellen zoals 'Once upon a time ...' en 'Nonsense' (Belgisch vertelspel: http://www.nonsensethegame.be/) die ook als energizers of breaks kunnen worden gebruikt. We zullen ook andere Dixit-alternatieven zien in het volgende hoofdstuk.
## 1.2	De structuur van de training
### 1.2.1	Hoofdstuk 1 - Iedereen heeft een verhaal ...
In dit eerste hoofdstuk zullen we ons concentreren op wat Digital Storytelling is, hoe het kwam tot de methodologie die we vandaag kennen, hoe te beginnen met het verkennen en verdiepen van het onderwerp met uw groep en hoe u een fantastische vraag kunt maken die verhalen en anekdotes oproept.
### 1.2.2	Hoofdstuk 2 - Samen een persoonlijk verhaal schrijven
In het tweede hoofdstuk bekijken we hoe we het groepsproces van het schrijven en structureren van verhalen met een verhaal cirkel kunnen faciliteren. Zodra onze eerste versies van onze verhalen klaar zijn, kunnen we gaan kijken hoe we afbeeldingen kunnen kiezen op basis van interpretatie en diepere lagen.
### 1.2.3	Hoofdstuk 3 - Een sterk beeld is een beeld dat je zelf maakt of kiest.
Zodra onze verhalen klaar zijn en we een idee hebben hoe onze afbeeldingen in ons digitale verhaal eruit zullen zien, is het tijd om onze eigen afbeeldingen te vinden of, nog beter, te creëren . Hier zullen we de verscheidenheid aan mogelijkheden verkennen en met tips en trucs om een goede afbeelding te maken.
### 1.2.4	Hoofdstuk 4 - Bewerken is als mediteren.
Het deel dat het moeilijkst lijkt, is eigenlijk best ontspannen. Je hebt alle belangrijke ingrediënten om je digitale verhaal te maken ... nu is het gewoon een kwestie van alles samen in de oven leggen en een mooie cake te baken. (psst, de oven is onze bewerkingssoftware in deze metafoor).
### 1.2.5	Hoofdstuk 5 - Extra's en FAQ
Het laatste hoofdstuk bevat alle dingen die we nergens anders zouden kunnen inpassen of waarvan we denken dat het te afleidend zou zijn om toe te voegen aan de andere hoofdstukken. Het bevat enkele tips en trucs voor uw voorbereiding, enkele verspreiding ideeën maar ook enkele interessante gedachten over onze werkethiek bij het maken van digitale verhalen met kwetsbare groepen.
## 1.3	Wat is een digitaal verhaal?
### 1.3.1	Enkele voorbeelden
Het is belangrijk om de deelnemers te vragen wat zij denken dat Digital Storytelling betekent. Ze vinden het meestal moeilijk om te definiëren wat het zou kunnen zijn, maar de meesten voelen de betekenis zeer nauwkeurig aan. De term is ook erg breed, dus het is interessant om verschillende interpretaties in de groep te horen. Wat ze echter niet weten, is hoe onze digitale verhalen er meestal uitzien en meestal zijn ze opgelost met één benadering van digitale verhalen in hun hoofd. Daarom is het belangrijk om een paar voorbeelden te laten zien die divers zijn qua structuur, verhaal, achtergrond, emotie, techniek, muziek, montage, etc.
De voorbeelden die we in deze training laten zien zijn door de makers goedgekeurd om te laten zien als onderdeel van een training Digital Storytelling. Ze zijn niet alles wat Digital Storytelling kan zijn, maar ze laten verschillende kanten zien en openen de mogelijkheden in de geest.
#### [Voorbeeld 1: "One Chair"](https://vimeo.com/261105291)
Het eerste voorbeeld is technisch ongecompliceerd: het verhaal heeft een chronologische structuur met een verhalend begin, midden en einde. This Digital Story was het resultaat van een training van jeugdwerkers tijdens de eerste #metoo-beweging in 2017. We merkten dat dit eerste voorbeeld al
behoorlijk wat sentiment oproept. Het is niet wat de meeste deelnemers in gedachten hadden wat een digitaal verhaal zou moeten zijn. Dat is het doel van deze voorbeelden: de geest verruimen en inspireren.
#### [Voorbeeld 2: “Anastasia”](https://vimeo.com/203315683 Als)
Onderdeel van een uitwisselingsprogramma rond het onderwerp migratie hadden we enkele fascinerende resultaten waarbij jongeren uit Barcelona, Zagreb en Brussel persoonlijke verhalen deelden over het onderwerp. Dit verhaal is een fantastisch voorbeeld van hoe een verhaal kan worden gebruikt als pedagogisch hulpmiddel om een discussie op gang te brengen.
#### [Voorbeeld 3: “Cruise on my tranen”](https://vimeo.com/173594959)
Het derde voorbeeld is qua structuur en idee totaal verschillend. Dit verhaal is gemaakt tijdens een project in de gevangenis in Brussel. De verteller wilde dat zijn video een afspiegeling was van zijn emoties en ervaringen die hij tijdens zijn verblijf hier tegenkwam. Het is niet chronologisch gestructureerd omdat het niet echt een
anekdote vertelt. De tekst is poëtisch en de afbeeldingen zijn niet eenvoudig, ze kunnen verschillende dingen betekenen. Hij vergelijkt zijn verblijf in de gevangenis met een reis op een schip, een cruise die je niet mag wagen. Hij gebruikt ironie en hierdoor is het in het begin niet echt duidelijk waar hij het eigenlijk over heeft.
De deelnemers houden soms van de poëtische kant van dit verhaal, het is anders en benadert emoties op een meer natuurlijke manier. Het was echter niet voor iedereen duidelijk waar deze persoon het over had. De context van de gevangenis moest worden uitgelegd.

#### PAS OP! 
Als onderdeel van een training voor facilitators of coaches van Digital Storytelling (zoals jij) laten we veel voorbeelden zien om de geest open te stellen voor wat het zou kunnen zijn. Maar laat bijna nooit voorbeelden zien aan onze deelnemers. Als we voorbeelden laten zien, kunnen we er zeker van zijn dat iedereen exact dezelfde stijl of hetzelfde idee uit het voorbeeld zal kopiëren en dat is precies wat we willen vermijden. Voor ons moet het eindresultaat authentiek zijn voor de persoon en zoveel mogelijk het product zijn van zijn of haar eigen verbeeldingskracht en gevoel.
### 1.3.2	Geschiedenis en gebruik van Digital Storytelling
Onze methodologie van Digital Storytelling komt niet uit de lucht vallen. Het is gebaseerd op de ervaring van Joe Lambert en zijn Storycenter.org. Een heel vereenvoudigd achtergrondverhaal is dat hij het maken van digitale video's met stilstaande beelden gebruikte voor gebruik in een therapeutische context. Het eindproduct kan mensen met een bepaald trauma de mogelijkheid geven om het te materialiseren en weg te stoppen of er vanuit een ander perspectief naar te kijken. De methodologie was een mild succes in de therapie, maar het waren leraren en jeugdwerkers die gebruik maakten van de eenvoudig te implementeren methoden om hun studenten een stem te geven en tegelijkertijd de digitale vaardigheden te verbeteren.
In zo implementeren we het ook: mensen een stem geven door middel van storytelling. Om de methode zo laagdrempelig mogelijk te houden, werken we al jaren om het digitale deel eenvoudiger en kleiner in tijdsbeperking te maken.
### 1.3.3	Wat is geen digitaal verhaal
Helaas is de term "Digital Storytelling" sinds kort overgenomen door marketing en op de een of andere manier beter bekend bij het publiek dan de officiële term die is bedacht door Ken Burns en Joe Lambert. Daarom is het belangrijk om een aantal dingen heel duidelijk te maken wat geen digitaal verhaal is:
-	fictie
Een digitaal verhaal moet altijd gebaseerd zijn op een bepaalde waarheid en moet realistisch zijn. Met realistisch bedoelen we niet dat je geen metaforen kunt gebruiken zoals het verhaal in een sprookjesachtige setting plaatsen. We bedoelen dat de plot logisch moet zijn en dat de beslissingen van de personages in het verhaal authentiek en geloofwaardig moeten zijn.
Met de waarheid bedoelen we ook niet dat het hele verhaal exact moet zijn zoals het echte verhaal is gebeurd. Je kunt twee lijnen mengen die een bepaalde verbinding hebben, je kunt zelfs enkele details verzinnen om een bepaald punt te maken. Er moet echter altijd een verband zijn met de realiteit van de verteller. We schrijven geen fictie.
-	Publiciteit
Zoals eerder vermeld, is Digital Storytelling een term die momenteel in marketing en reclame wordt gebruikt. Hoewel Digital Storytelling zeker een oproep tot actie, protest of het veranderen van de denkwijze van mensen kan hebben, is het niet bedoeld om een product of dienst te promoten. Dit is moeilijk als we het hebben over positieve ervaringen met betrekking tot de baan of professionele omgeving van de deelnemers, maar het is mogelijk.
### 1.3.4	Wat te vermijden in een digitaal verhaal
Berger zei het heel verstandig: "Vindt het gewone in het bijzonder". Mensen geven graag veel context en “grote waarheden” zonder in te gaan op hun eigen ervaringen. Maar door een persoonlijke ervaring kun je een grotere groep vertegenwoordigen: één verhaal van een vluchteling kan staan voor de ervaring van veel andere vluchtelingen. Probeer het verhaal vanuit je eigen perspectief te vertellen en het zal meer impact hebben dan je denkt.
We vermijden momenteel ook video en muziek in onze digitale verhalen. Dit is soms controversieel en niet gemakkelijk uit te leggen ... maar hier gaan we:
video kan worden gebruikt, we hebben zelfs een aantal fantastische voorbeelden in deze training die video gebruiken, maar het verhoogt de moeilijkheidsgraad aanzienlijk en het kan snel fout worden toegepast.
Muziek is een ander verhaal: allereerst ervaren we dat de meeste muziek deelnemers auteursrechtelijk beschermd willen zijn. Maar zelfs met eigen gemaakte muziek of muziek die is toegestaan (Incompetech,…) maakt het de video's minder persoonlijk en grenst ze aan cheesy of oneerlijk. Het gebruik van een voice-over heeft iets puurs en moois. En zoals bekend: de beste is wat je niet opmerkt… veel succes met het vinden van dat in een korte video van 1 tot 3 minuten.
We raden ook aan om geen geschreven tekst die niet overeenkomt met de voice-over in je video weer te geven, omdat dit verwarring veroorzaakt.
### 1.4	Opbouw van Digital Storytelling workshops - Stapsgewijs programma
Dit is een overzicht van hoe je een Digital Storytelling implementeert met je groep: we beginnen met energizers om mensen aan het praten te krijgen, vertrouwd te raken met het vertellen van verhalen en veiligheid in de groep te installeren. Vergeet ook niet in het onderwerp te duiken (waar we in dit hoofdstuk veel meer op in zullen gaan).
Vervolgens gaan we verder met het vertellende zelf. Je hebt een goede vraag nodig om mensen te inspireren een verhaal te vinden, wat ruimte en tijd om deze vraag met een anekdote te beantwoorden en een verhaal cirkel te installeren. Als de verhalen klaar zijn, richten we ons op afbeeldingen. Welke afbeeldingen zijn sterker dan andere en waar je ze kunt vinden of hoe je ze kunt maken. Als we onze tekst en afbeeldingen hebben, is het tijd om ze samen te brengen en onze video te bewerken!
Net zo belangrijk als alle voorgaande stappen is het organiseren van een screening voor uw project. In sommige gevallen kan dit een openbare vertoning zijn, maar afhankelijk van het project en de deelnemers is een besloten vertoning alleen voor uw deelnemers voldoende. 
### 1.5	Verkenning van het onderwerp
Het is bijna onmogelijk om een vertelproject te starten zonder een bepaalde invalshoek of onderwerp.
In het algemeen: een digitaal verhaal kan over alles gaan. Bijvoorbeeld:
* Een boek- of filmverslag voor een taalles of geschiedenisles op school
* Vertel je eigen levensverhaal of het verhaal over je migratie
* Maak een verhaal over diversiteit (geslacht, seksualiteit, cultuur, afkomst, handicap)
* Vertel een (persoonlijke) verhaal over geweld en geweld tegen vrouwen
* Een evaluatie van een project

We hebben ervaren dat het belangrijk is om ervoor te zorgen dat een onderwerp duidelijk is voor je deelnemers en dat je een bepaald overzicht hebt van de kennis en verbinding van je deelnemers met het onderwerp. Sommige mensen interpreteren of verhouden zich anders tot een onderwerp of hebben een uitgesproken mening zonder dat deze op wetenschappelijke feiten zijn gebaseerd.
Zo voelden veel van onze jongeren zich direct na de terroristische aanslagen in Parijs en Brussel geobserveerd door de hele gemeenschap omdat ze een migratie achtergrond hadden. Ze hadden er iets over te zeggen, ze waren het gewoon niet eens met het debat dat er gaande was en voelden er sterk bij. We gingen naar scholen in de buurt en we vroegen hen wat hun mening was over die dingen, want niemand deed het echt vóór ons. We realiseerden ons al snel dat deze jongeren een uitgesproken mening hadden over bijvoorbeeld het tijdschrift Charlie Hebdo dat een van de slachtoffers was tijdens de aanslagen in Parijs. Toch had geen van hen ooit een exemplaar van dat tijdschrift geopend of gelezen. In de video's in de bijbehorende dia kun je enkele voorbeelden zien van hoe we jongeren de kans gaven om dit onderwerp te verkennen en hun mening met meer diepgang te hervormen.
#### 1.5.1	Oefening: Hot Topic
Brainstorm: welke onderwerpen zijn momenteel belangrijk bij uw deelnemers?
Maak het niet te moeilijk, schrijf gewoon alle ideeën die uit de groep komen op een bord en stem over met welk onderwerp de groep wil werken. 
Inspiratie nodig? Sommige onderwerpen zoals mobiliteit, (gender) ongelijkheid, diversiteit, ecologie, communicatie,… zijn altijd herkenbaar.
#### 1.5.2	Oefening: Bereid een workshop voor om een onderwerp te verkennen
Zodra uw groep een onderwerp heeft gekozen, kunnen we nu enkele oefeningen maken zoals die besproken zijn in de voorbeeld video's rond de terroristische aanslagen in Parijs en Brussel.
Het idee is om een of twee methoden te bedenken die een of meer van de volgende doelen hebben:
* Een mening verdiepen of een bevooroordeeld standpunt veranderen.
* Vind voorbeelden van (oude) verhalen die in je brein begraven liggen.

Enkele tips met betrekking tot deze methoden:
* Geen onderwerp opdringen aan een groep die niets te maken heeft met de groep deelnemers. Als extreem voorbeeld: het voelt voor jongeren niet waardevol om bijvoorbeeld over het leven na het werk te praten.
* Zorg ervoor dat je een methode hebt waarmee zelfs de meest stille en / of introverte deelnemer wordt gehoord.
* Soms heeft een onderwerp diepere problemen, probeer deze dan met uw groep te identificeren en te analyseren.
* Een veilig en altijd succesvol spel is zien hoe de media het onderwerp opblazen of bagatelliseren. Dit levert zelden betere verhalen op, maar het kan een bron van frustratie zijn die moet worden weggenomen.
* Veiligheid eerst (zie daarover meer als we het hebben over Verhalencirkels).
* In veel gevallen hebben uw deelnemers al enkele verhalen die verband houden met het onderwerp, maar het kan helpen om tijdlijnen of kaarten te maken om de ideeën te verbreden en verhalen te vinden die niet voor de hand liggend zijn. Als je het bijvoorbeeld over je carrière hebt, kan een tijdlijn met post-it helpen met antwoorden op vragen als 'Wanneer heb je het meeste collegialiteit gevoeld' of 'Wanneer heb je het hardst gelachen op de werkvloer' . Met kaarten rond onderwerpen van uw relatie met een stad kunnen ze zijn "Wat en waar is uw eerste herinnering in deze stad" of "Waar voel je je het veiligst?".

#### 1.5.3	De vraag
We werken graag met een duidelijke stelling of vraag die verwijst naar het bepaalde onderwerp: Om een voorbeeld te geven zullen we het onderwerp genderongelijkheid gebruiken. We projecteren de volgende vraag zien op een muur waar de groep aan het werk is. 
Op deze manier kunnen ze de taak die voor hen ligt altijd opnieuw lezen als ze vast komen te zitten in een gedachtegang:

**"Wanneer en hoe was de laatste keer dat u persoonlijk met genderongelijkheid werd geconfronteerd?"**

Dit is natuurlijk een zwaar onderwerp, maar deze vraag helpt om een tijdschema te definiëren en staat open genoeg om te inspireren om na te denken over bepaalde anekdotes en verhalen. Het voorbeeld dat we zagen (One Chair) was het resultaat van deze vraag.
Een vraag hoeft niet per se verband te houden met of te wijzen op een negatieve ervaring. De vraag wanneer je praat over hoe je je vrijwilligerswerk in een bepaalde organisatie ervaart, kan zijn:

**"Als je een ervaring zou kunnen delen met een buitenstaander om te laten zien dat je werk bij organisatie het waard is, wat zou dat dan zijn?"**

Als je meer tijd hebt, kun je eigenlijk om meer verhalen vragen. Toen we deze vraag bijvoorbeeld gebruikten om over migratie te praten:
    
**"Zoek twee (of meer) anekdotes, verhalen over iets dat je in je omgeving hebt meegemaakt, gezien of gehoord dat verband houdt met migratie."**

U kunt de drie bovenstaande vragen als sjabloon gebruiken:

**"Wanneer en hoe was de laatste keer dat u persoonlijk geconfronteerd werd met probleem met betrekking tot het onderwerp"**

**"Als u een ervaring zou kunnen delen met een buitenstaander om te laten zien dat uw werk bij organisatie is het waard, wat zou het zijn?"**

**"Zoek twee (of meer) anekdotes, verhalen over iets dat je in je omgeving hebt meegemaakt, gezien of gehoord en dat is gekoppeld aan onderwerp."**

Als je vastloopt met het opbouwen van een verhaal, probeer je vraag dan op een bepaald moment in de tijd te richten (begin met Wanneer?). Je kunt altijd extra vragen stellen als die niet diep genoeg lijken te gaan. Probeer met je groep een goede vraag te bedenken rond het door jou gekozen onderwerp. U kunt de [Fridge Magnet-tool](https://gkjcjg.github.io/fridge_magnets/) gebruiken om er samen een te maken. Zij zullen deze vraag natuurlijk beantwoorden en hiermee in het volgende hoofdstuk een verhaal maken :) 

## 1.6	Dit hoofdstuk afsluiten
### 1.6.1	Huiswerk
"Invisible Ink" door Brian McDonald wordt beschouwd als de bijbel van het vertellen van verhalen en is verplichte lectuur voor iedereen die bij Pixar Studios werkt. Hij heeft ook een zeer interessante podcast genaamd "You Are a Storyteller", waar hij dieper ingaat op de verschillende aspecten van verhalen en hoe het ons allemaal verbindt.
Maar het is de tweede aflevering van de podcast "Carry The Fire" van Dustin Kensrue (zanger van de fenomenale rockband Thrice) die we presenteren als de perfecte introductie tot het werk van Brian McDonald. Het is to the point, kort en verschuift veel vaste ideeën over wat verhalen vertellen is. Iets dat je in de volgende sessie zou kunnen bespreken als je dat wilt of gewoon om te inspireren en je deelnemers aan het denken te zetten over storytelling buiten de werkomgeving.
### 1.6.2	Evaluatie
Elk hoofdstuk sluiten we af met een kleine evaluatie. Dit is natuurlijk niet verplicht of u kunt deze methoden wijzigen met elke actieve beoordelingsmethode die u maar wilt, maar hierbij onze eerste stelling:
laat elke deelnemer de vorm van zijn hand volgen. Elke vinger vertegenwoordigt een andere mening over de sessie.
* Duim: iets dat het meest positief was.
* Wijsvinger: iets dat aan je bleef plakken en je niet gaat vergeten.
* Middelvinger: iets dat je niet leuk vond.
* Ringvinger: iets dat werd gezegd of gedaan dat belangrijk voor je was.
* Pinkervinger: iets waarover je onzeker bent.

Laat elke deelnemer zijn vingers vertellen om het hoofdstuk af te sluiten.
