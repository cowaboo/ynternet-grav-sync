---
title: 'Een sterk beeld is een beeld dat je zelf creëert of kiest.'
---

## 3.1	Warming up
### 3.1.1	Dixit
[50] Derde Dixit-oefening van de module:
Wat is het leukste aspect van je werk (of dagelijkse bezigheid).
Er is geen specifiek doel gekoppeld aan deze Dixit-oefening, hier wordt Dixit enkel gebruikt als intro.
### 3.1.2	Nieuwe voorbeelden
[52]
Weet je nog dat video alles moeilijker maakt en je het waarschijnlijk niet zou moeten gebruiken? Welnu, soms krijg je goede resultaten.
Deze digitale verhalen zijn gemaakt tijdens een Erasmus + uitwisselingsproject met Spaanse en Belgische jongeren die zich specifiek vrijwillig inzetten rond gender en hoe ze het ervaren hebben. Weet je nog dat we het hadden over "het vinden van de generaal door het specifieke"? We denken dat deze resultaten een heel herkenbaar verhaal vertellen voor alle slachtoffers van genderongelijkheid.
#### [Voorbeeld 1: "Speak Up"](https://vimeo.com/351678568/ab7ec22603)
Wederom onze regelset uitdagen, maar ook bewijzen dat elk verhaal een mogelijke individuele benadering heeft. Toen Agnes haar verhaal in het verhaal vertelde, omcirkelden haar emoties, fragiele stem en lichaamstaal de reden dat iedereen kippenvel kreeg (en ook de nodige woede en frustratie
jegens het onderwerp). Na wat testen met beelden en zelfs reenactments voelde het gewoon de meest eerlijke en sterkste optie om haar het verhaal gewoon in de camera te laten vertellen. Het creëren van wat intertekstualiteit en verbindingen met andere video media zoals bloggers en influencers op YouTube of de getuigenissen tijdens de # metoo-periode (die echt nooit is afgelopen, zoals blijkt uit deze video).
#### [Voorbeeld 2: "Lisa"](https://vimeo.com/351677704/50eccd98b6)
#### [Voorbeeld 3: "Kobe"](https://vimeo.com/351781837/01789b1f85)
De voorbeelden van Lisa en Kobe komen uit dezelfde groep als de eerste voorbeeld. Ook gebruik maken van video, maar op een meer traditionele manier van Digital Storytelling, aangezien afbeeldingen worden gemaakt en geacteerd, maar ook diepere lagen in hun beelden hebben.
## 3.2	Verhaal Cirkel met storyboard
[53] [54]
Elke deelnemer heeft de tijd gehad om zijn verhaal te herschrijven met de feedback die in de eerste verhalencirkel is ontvangen en heeft nu waarschijnlijk een aantal ideeën welke soort afbeeldingen ze voor elk hoofdstuk willen gebruiken.
Dit betekent dat het een goed moment is om weer bij elkaar te komen en te kijken waar iedereen zich in het proces bevindt. Maak nog een verhaal cirkel met dezelfde regels en structuur als voorheen met één toevoeging: laat de deelnemers hun storyboard laten zien nadat ze hun nieuwe versie van hun tekst hebben verteld en laat ze uitleggen welke afbeelding waar naar toe gaat. Nu kunnen de deelnemers feedback geven op de nieuwe tekst en de selectie of ideeën voor afbeeldingen.
Als iedereen wat feedback heeft gekregen, is het tijd om te laten zien hoe ze afbeeldingen kunnen maken of online kunnen vinden.
## 3.3	Uw videomateriaal verzamelen
[55]	In deze methodologie presenteren we twee methoden voor het maken of zoeken van afbeeldingen voor uw video:
* Maak uw eigen afbeeldingen.
* Zoek rechtenvrije afbeeldingen op internet.

Uw eigen afbeeldingen maken is verreweg de sterkste weg die u kunt nemen zorg ervoor dat het hele product is "uitgevonden" en gemaakt door de maker. Dit zal de verbinding tussen het digitale verhaal en de maker aanzienlijk verbeteren. Dit kost ook veel meer tijd dan het alternatief, dus houd er rekening mee dat dit een tijdrovende stap in het proces is!
En wat is het alternatief? Zoek ze online. En hoewel dat wat lui klinkt, kun je veel diepgang en interessante hooks toevoegen als je het koppelt aan mediageletterdheid en copyright kwesties.
### 3.3.1	Zelf afbeeldingen maken: foto's
[56]	We gaan niet te diep in fotografie, want “een goede foto maken” is een hele workshop en methodologie op zich. En dat is maar goed ook: een andere tool die uw deelnemers kunnen leren en gebruiken buiten de wereld van Digital Storytelling.
Maar drie belangrijke richtlijnen die u kunt introduceren, zijn 1 & 2 - De gulden snede en regel van derden
Vind	uitleg	hier:	https://www.apogeephoto.com/how-to-use-the-gulden-snede-om-je-fotografie-te-verbeteren/
3 - Landschap in plaats van portret
Vooral jongeren zijn gewend om video en fotografie meestal in portret te maken (Instagram, intuïtief smartphone gebruik, ...) maar om een professioneel videoproduct te maken dat gebruikt kan worden voor vertoningen (tv, bioscoop,…) raden we aan landschapsfoto's te gebruiken.
### 3.3.2	Zelf afbeeldingen maken: Tekeningen
[57]	Het maken van afbeeldingen is waarschijnlijk nog minder tijdrovend dan het maken van foto's, want u kunt ze allemaal op de locatie van uw werkplaats maken. We merkten dat jongeren erg onzeker zijn wanneer ze gedwongen worden om te tekenen en zich kinderachtig gedragen. Iedereen kan echter zijn afbeeldingen tekenen als hij dat echt wil (en ze hebben eigenlijk al conceptversies van hun tekeningen gemaakt in het storyboard).
We raden aan om geen potloden te gebruiken, althans niet als eindresultaat, omdat deze kleuren en lijnen te dun en te licht zijn. Markers, viltstiften, verf of zelfs kleurpotloden maken dikke lijnen met kleuren die eruit springen en veel mooier zijn om naar te kijken.
Het digitaliseren van de afbeeldingen hoeft ook niet moeilijk te zijn, je kunt er gewoon foto's van maken of nog beter als je een scanner ter beschikking hebt. Zorg ervoor dat u bij het maken van een foto de schaduw van uw apparaat of u zelf niet op de tekening ziet.
Probeer ook liggend op je papier te tekenen, want dit maakt de conversie naar video ook gemakkelijker.
### 3.3.3	Gebruik van rechtenvrije afbeeldingen van internet
[58]	In al onze projecten gebruiken we afbeeldingen "zonder" copyright die onder de Creative Commons-licentie vallen. In zekere zin is het niet correct dat media die onder deze licenties vallen geen auteursrecht hebben, maar ze zijn in de meeste gevallen vrij te gebruiken voor persoonlijke projecten wanneer een paar regels worden nageleefd. Er zijn verschillende soorten Creative Commons-licenties die het gebruik van de afbeelding beperken en definiëren. Het is niet altijd duidelijk onder welke licentie een foto valt. Dan is het veiliger om de foto niet te gebruiken. 
## 3.4	Hier heb je een tabel met de meest gebruikte licenties
![](NEb.png)
## 3.5	De meeste van deze licenties worden gecombineerd
![](NEa.png)

Zoals je kunt zien, bevatten bijna alle licenties Naamsvermelding, wat betekent dat de deelnemers krediet moeten geven aan de mensen dat heeft het gehaald en het is niet altijd gemakkelijk om de maker van de inhoud te vinden. Het is het gemakkelijkst om foto's in het publieke domein te zoeken, maar dit geeft problemen omdat de meeste resultaten beperkt zijn.
Bijvoorbeeld: ooit waren veel deelnemers op zoek naar foto's die de emotie van eenzaamheid opriepen en hoewel er veel foto's waren, merkten we dat de meeste deelnemers dezelfde begonnen te gebruiken. We moedigden hen aan om op zoek te gaan naar foto's met zoekopdrachten in hun eigen taal, aangezien dit totaal andere resultaten oplevert.

De Creative Commons-organisatie heeft een soort zoekmachine opgezet om Creative Commons-media gemakkelijker te vinden. Het is geen echte zoekmachine maar meer een soort filter op bestaande echte zoekmachines en online inhoud zoals Google, Soundcloud, Youtube, etc. De website is https://search.creativecommons.org/ en hoewel we de deelnemers aanmoedigen om deze website tijdens het programma te gebruiken, vragen we hen ook om de licentie te controleren die is gebruikt voor de foto, muziek of video die ze willen.

Andere opties met professionelere foto's zijn Pexels, Unsplash, Morguefile en er zijn waarschijnlijk ook vele andere. Voor deze websites gelden dezelfde regels voor uw zoekresultaten.
Hetzelfde als bij het maken van foto's of tekeningen: de voorkeur gaat uit naar landschapsfoto's.
### 3.5.1	Andere mogelijke afbeeldingen
[59]	Op deze dia vindt u enkele andere mogelijkheden met twee voorbeelden. Het stop motion-voorbeeld is gemaakt tijdens een workshop met vrouwen die met geweld te maken hebben gehad. Het Cut-Out Digital-verhaal is gemaakt door een professionele videomaker, dus de productiewaarde ziet er erg goed uit, maar uiteindelijk is het verhaal altijd het belangrijkste. We hebben al eerder andere voorbeelden gezien: zoals het digitale Time-Laps-verhaal "Labyrinth Called Brussels" en enkele videovoorbeelden eerder in dit hoofdstuk rond het onderwerp "Gender".

Deze verhogen allemaal het technische niveau van uw workshop, dus we kunnen het alleen aanbevelen om deze te gebruiken als uw deelnemers een hogere digitale vaardigheden hebben.
### 3.5.2	De afbeeldingen op uw computer
[61]	opslaan Download of bewaar al uw afbeeldingen in een nieuwe map op onze computer en indien mogelijk hernoem ze zodat ze al chronologisch en gemakkelijk te herkennen zijn.

Ze van het ene apparaat naar het andere krijgen is niet altijd gemakkelijk (kijkend naar je Apple-producten) dus hierbij een kleine checklist:

Van internet: rechtstreeks downloaden op je systeem
Vanaf pc of Mac: stuur ze via e-mail of kopieer ze naar een USB-stick Vanaf Android: probeer het via Bluetooth te verzenden, verbind het met een USB-kabel of stuur het via e-mail.
Vanaf een iOS-apparaat: probeer het via Bluetooth te verzenden, sluit het aan met een USB-kabel (mogelijk heeft u iTunes nodig) of stuur het via e-mail. Als je met een Mac werkt, kun je waarschijnlijk ook Airdrop gebruiken.

[62]	Nu is het tijd voor uw deelnemers om zelf afbeeldingen te zoeken of te maken!
## 3.6	Afsluiting van dit hoofdstuk
### 3.6.1	Evaluatie
[63]	Voor de actieve terugblik op dit hoofdstuk gebruiken we onze weerkaarten!

Hang alle weerkaarten aan de muur en laat je deelnemers een weeroverzicht van de dag maken zoals het weerbericht op tv: kies drie weersomstandigheden die de belevingen van de dag weergeven (bijvoorbeeld: mijn dag begon nogal mistig zoals ik niet was. Ik ben niet zeker van mijn project, maar toen verscheen er een regenboog toen ik een idee kreeg,…).
Laat iedere deelnemer voor de zaal staan en maak een weersvoorspelling met hun ervaringen!

[Je kunt de weersjablonen hier vinden](https://drive.google.com/open?id=1lJ9gJiYU3_MK99SRZeyI3dRXzodX4iyG)