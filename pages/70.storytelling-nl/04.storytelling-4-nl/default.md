---
title: 'Bewerken is als mediteren'
---

## 4.1	Warming-up!
### 4.1.1	Dixit
[65] Vierde Dixit-oefening van de module:
Wat was je eerste niet-routinematige gedachte van de dag?
Er is geen specifiek spel gekoppeld aan deze Dixit-oefening, alleen gebruikt als warming-up van de sessie. Misschien is het een goede zaak om ook te vermelden dat u op zoek bent naar de eerste gedachte die niets te maken heeft met vermoeidheid of koffie;)
### 4.1.2	Nieuwe voorbeelden
[67]
#### [Voorbeeld 1: "Chron's Disease"](https://vimeo.com/306170761)
Toen ze rond het onderwerp identiteit werkte, wilde deze jongedame eerst praten over wat haar identiteit niet definieerde, maar een belangrijk onderdeel van haar leven werd: haar ziekte. Ook een mooi voorbeeld van hoe getekende afbeeldingen heel goed kunnen werken.
#### [Voorbeeld 2: “The Backpack”](https://vimeo.com/168442557)
Een zeer intense en gefrustreerde reactie op de vraag aan lokale jongeren uit Brussel hoe zij de terroristische aanslagen in Parijs en Brussel hebben ervaren.
#### [Voorbeeld 3: "Happy Birthday"](https://vimeo.com/141408491)
Een mooie maar emotionele getuigenis van een vrouw die haar land ontvluchtte vanwege de oorlog in het Midden-Oosten. Ze gebruikt één enkel woord met enkele sterke beelden.
Waarschuw uw deelnemer dat deze video verontrustende oorlogsbeelden bevat.
## 4.2	FLOSS-bewerkingssoftware
[68] [69] [70] [71] [72] [73] [74]
Er zijn momenteel drie open-source videobewerkingsprogramma's die kunnen worden gebruikt om digitale verhalen te maken: Kdenlive, Shotcut en OpenShot. Afhankelijk van uw systeem hebben sommige voor- of nadelen, maar in het algemeen raden we u aan om Kdenlive te gebruiken, aangezien dit het meest stabiele en complete pakket van deze drie is. Maar geen van deze programma's heeft een ingebouwde audio-opname-optie, dus we zullen eerst onze audio moeten opnemen.

In een perfecte wereld zou je een (semi-) professioneel audio-opname apparaat hebben, zoals een Zoom of een Roland voicerecorder, maar deze zijn relatief duur en niet super makkelijk in het gebruik. Gelukkig kunnen we onze computer ook gebruiken om onze stem te registreren bij het open source programma Audacity.
[Je kunt Audacity hier downloaden](https://www.audacityteam.org/)
[En je kunt onze tutorial over het opnemen van je stem hier volgen](https://vimeo.com/248116526)

Zorg ervoor dat je een rustige externe locatie hebt waar je kunt neem uw stem in stilte op zonder achtergrondgeluid.Zodra uw stem is opgenomen, kunnen we beginnen met bewerken.
We hebben ook enkele tutorials gemaakt over hoe je een digitaal verhaal kunt maken met Kdenlive.
[Je kunt Kdenlive hier downloaden](https://kdenlive.org/)
[En de tutorials zijn hier te vinden](https://vimeo.com/showcase/6609648)

In dit Vimeo-album vind je 4 video's die je door het hele proces van uw afbeeldingen en audio importeren om uw video te exporteren voor online distributie. Je kunt deze video's in je eigen tempo bekijken: pauzeer wanneer nodig, spoel terug als je nog wat uitleg wilt zien of sla een gedeelte over als je al wat basiskennis hebt over videobewerking. Dus nu is het tijd om alle inhoud bij elkaar te brengen: Verhaal en afbeeldingen en maak het digitale verhaal in dit videobewerkingsprogramma!
## 4.3	Showtime
[75] [76] En tot slot, het tonen van hun films aan het publiek kan een echt succesverhaal zijn voor de deelnemers, ze kunnen trots hun creatie op een groot scherm laten zien en delen op hun Facebook-profiel. Maar een screening hoeft niet openbaar te zijn. Een beetje afhankelijk van de doelgroep en het onderwerp zou je ook gewoon een screening kunnen organiseren met enkel je deelnemers of met een selecte groep vrienden, collega's,… het is aan de groep om te beslissen hoe ze hun resultaten willen verspreiden. Als u doel heeft de resultaten openbaar te maken, moet dit vanaf het begin duidelijk zijn voor uw deelnemers om conflicten of problemen te voorkomen.
Met de show aan het einde is dit in normale gevallen de laatste fase van de digitale vertelmethodiek. In het volgende hoofdstuk hebben we echter nog een aantal dingen te vertellen!
## 4.4	Afsluiting van dit hoofdstuk
### 4.4.1	Evaluatie
[77] Voor de voorlaatste actieve review gebruiken we de drie:
Elke deelnemer krijgt de stam van een boom. Aan de deelnemer om de boom af te werken met stiften en een metafoor te maken van hoe ze deze dag hebben beleefd.Bijvoorbeeld: een boom met een schommel (ik kon met wat ideeën spelen), een boom met een nest en een ei (ik heb een prachtig resultaat gemaakt waar ik trots op ben en heb eindelijk mijn ei kunnen leggen ).
[Je kunt de drie sjablonen hier vinden](https://drive.google.com/open?id=1lJ9gJiYU3_MK99SRZeyI3dRXzodX4iyG)