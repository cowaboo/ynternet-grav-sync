---
title: 'Samen een persoonlijk verhaal schrijven'
---

## 2.1	Warming up!
### 2.1.1	Dixit

Elk hoofdstuk begint met een Dixit-oefening, dus hier gaan we:
laat elke deelnemer zoeken naar een kaart die iets vertegenwoordigt, hoe groot of klein ook, waar hij in de afgelopen 24 uur trots op is geweest.
Laat de groep in de kamer rond gaan met hun kaart in hun handen verborgen. Geef een seintje wanneer ze moeten stoppen. Dit kan een alarm van een smartphone zijn of een simpele klap met je handen. Wanneer ze het signaal horen, moeten ze een praatje maken met de persoon die het dichtst bij hen staat en moeten ze raden waarom de persoon die kaart heeft gepakt, in feite het verhaal raden dat de persoon in zijn of haar hoofd heeft. Geef ze een minuut of twee en laat ze weer mengen. Geef het teken na een paar seconden opnieuw en herhaal het proces een paar keer.
Als het spel is afgelopen, vraag hen hoeveel van hen het goed hebben geraden. Was het moeilijk? Waarom was het moeilijk / gemakkelijk? Het heeft allemaal te maken met de interpretatie van een afbeelding die we later in de module zullen koppelen aan ons gedeelde 'Afbeeldingen zoeken'.
### 2.1.2	Nieuwe voorbeelden
#### [Voorbeeld 1: “The Labyrinth Called Brussels”](https://vimeo.com/173594794)
Om elk idee te geven of wat een digitaal verhaal kan zijn. Deze video toont het verstrijken van de tijd van enkele van de meest drukke plaatsen in Brussel, zoals het centraal station en winkelstraten. De maker maakte een soundscape van woorden die typisch verband hielden met de stad en in vele talen vertaald in plaats van zijn eigen stem op te nemen. Het doel is om de gekke luide mengelmoes van culturen die deze stad is, vast te leggen. 

Het kan voor enige verwarring zorgen. Dit is niet het typische verhaal. Het is een emotie; het is zelfs een eerbetoon, maar vanuit een kritisch oogpunt. De structuur en de beeldtaal zijn niet wat we tot nu toe hebben gezien of gehoord, maar het helpt om de deelnemers een beeld te krijgen.
#### [Voorbeeld 2: "Hero"](https://vimeo.com/358256129)
Maxim is een collega hier bij Maks en beantwoordde de vraag die we eerder gebruikten: "Als je een ervaring zou kunnen delen met een buitenstaander om te laten zien dat je werk bij Maks de moeite waard is het, wat zou het zijn? ”. Weet je nog dat we zeiden dat een digitaal verhaal geen dienst of product mag promoten? We realiseren ons dat
dit daar dichtbij komt, maar het is ook eerlijk en anekdotisch van iets waar hij eigenlijk trots op is. Dus waarom niet?
#### [Voorbeeld 3: "Wie zijn de anderen?"](https://vimeo.com/359771182)
Uit dezelfde groep als Maxim kwam dit resultaat gemaakt door onze trainer Youssef. Het is een zeer ontwapenend verhaal over racisme, discriminatie en hoe een verhaal voor meer dan één persoon kan spreken.
## 2.2	Tijdschrijven!
Zorg ervoor dat je de vraag die de deelnemers in de vorige sessie hebben geconstrueerd aan deze dia hebt toegevoegd en laat ze opnieuw zien.
Geef ze nu de ruimte en tijd om een antwoord op de vraag te schrijven. We zetten altijd wat zachte muziek en zetten wat koffie en thee voor dit moment om een creatieve werksfeer te creëren. Als je met een groep werkt die meer structuur nodig heeft, kun je de elfen dicht techniek gebruiken.
## 2.3	De Story Circle
Een van de belangrijkste onderdelen van Digital Storytelling is de Story Circle. We hebben een manier gevonden waarop je een fantastisch resultaat kunt behalen met slechts twee story circle, waar we je doorheen zullen leiden in deze gids, maar je kunt zoveel verhalen cirkels organiseren als je wilt. De maximale grootte van een story circle is tussen de 6 en 8 personen. Je hebt minimaal vier mensen nodig om het goed te laten werken. Als de groep te groot is, kun je ze opsplitsen in kleinere groepen, maar je hebt voor elke groep een trainer nodig.

Er wordt gezegd dat een story circle betrekking heeft op de tijd waarin mensen rond het kampvuur zitten en verhalen aan elkaar vertellen. Daarom maken we cirkels met stoelen en geen tafels en komt iedereen in de cirkel zitten.
Voordat u begint, moeten er enkele basisregels zijn die verband houden met veiligheid in de groep. Het fysieke, emotionele, sociale en spirituele welzijn van de deelnemers moet centraal staan in alle fasen van de sessies. Het proces van het creëren van verhalen binnen een workshop is net zo belangrijk als de eindproducten (media stukken) die uit de workshop voortkomen. Strategieën om het welzijn van kwetsbare deelnemers te waarborgen, zijn bijzonder belangrijk. De deelnemers die verhalen over bijzonder pijnlijke levenservaringen delen, moeten worden ondersteund bij het benaderen van hun verhalen vanuit een sterke positie in plaats van vanuit een gezichtspunt dat slachtofferschap versterkt. Begeleiders dienen te allen tijde passende grenzen te handhaven en tegelijkertijd open te blijven staan voor luister- en begrip processen.

Het vinden van deze grenzen is niet altijd gemakkelijk. Het is lastig, want wanneer iemand een zeer persoonlijke maar gewichtige ervaringen deelt, klinken ze vaak als een goede basis voor sterke persoonlijke verhalen. In onze verhaal kringen gaan we geen enkel onderwerp of taboe uit de weg.
Dus, zoals eerder vermeld en om een veilig kader te garanderen, is het belangrijk om enkele basisregels vast te stellen voordat je aan de eerste verhaal cirkel begint:
* elk verhaal dat in deze fase wordt verteld, blijft in de groep die deelneemt aan deze specifieke verhalencirkel. Als je groep te groot is, wordt je opgesplitst in meerdere verhalen cirkels, maar elk verhaal blijft als eerste in de verhalencirkel die als eerste wordt verteld. Alleen de verteller kan het buiten de oorspronkelijke verhaal cirkel delen als hij of zij daarvoor kiest. Iedereen moet ermee instemmen vertrouwelijkheid te bewaren over informatie en materialen die worden in deze workshop gedeeld, maar die mogelijk niet in openbaar verspreide verhalen terechtkomen.Niemand kan iemand dwingen om een verhaal te publiceren waarbij ze zich niet op hun gemak voelen. Noch de deelnemers, noch de instructeur kunnen dit doen.
* Sta geen buitenstaanders of hoppers toe in de cirkel, begin en eindig met dezelfde mensen.
* Pauzes. Geef verhalen de tijd om te ademen en in te zinken, dus neem na een paar verhalen een koffiepauze van vijf minuten als je met een grote groep bent. Het helpt bij het verwerken van de verhalen en helpt bij de focus van ieders presentatie.
* Doe dit in een rustige afgesloten ietwat gezellige ruimte maar met weinig afleiding. Het is moeilijk om op een steriele en koude plek een persoonlijk verhaal te vertellen en ernaar te luisteren. Je kunt met stoelen in een kring of rond een tafel zitten. (zet ook de muziek uit die je speelde tijdens de schrijfsessie).
* Alle gevoelens en meningen zijn welkom tijdens een verhaal. Noch is goed, fout, goed noch slecht. Ze worden op geen enkele manier beoordeeld. Iedereen heeft recht op vrijheid van meningsuiting door zichzelf in zijn verhalen te vertegenwoordigen. Ze moeten de ruimte en flexibiliteit krijgen om te beschrijven wat ze hebben meegemaakt.
* Tijdens het vertellen mogen er geen onderbrekingen zijn. Zorg ervoor dat iedereen pen en papier bij de hand heeft (maar niet in hun handen, want dit kan afleidende geluiden, krabbels, enz. Veroorzaken). Als ze vragen hebben, kunnen ze deze opschrijven en vragen nadat het verhaal is verteld. Alle vragen en opmerkingen achteraf moeten constructief en positief zijn.

Herinner iedereen aan deze richtlijnen aan het begin van elke verhaallijn, zelfs als je zeker weet dat ze het eerder hebben gehoord.
Laat nu iedereen een voor een verhaal vertellen en zorg ervoor dat iedereen de ruimte heeft om feedback te geven. Dit is ook een goede oefening om het verhaal hardop te vertellen, wat ze later in het proces nodig zullen hebben als ze hun stem opnemen. Geef dus constructieve feedback over de structuur en hoe de persoon het verhaal vertelt.
In de eerste verhaal cirkel kun je ze alle ideeën laten vertellen die ze tot nu toe over het onderwerp hebben. Het is aan de coach en de cirkel om hen te helpen een beslissing te nemen over welk verhaal de deelnemer wil meenemen en waarmee hij wil werken.

Herinner iedereen aan deze richtlijnen aan het begin van elke verhaallijn, zelfs als je zeker weet dat ze het eerder hebben gehoord.
Laat nu iedereen een voor een verhaal vertellen en zorg ervoor dat iedereen de ruimte heeft om feedback te geven. Dit is ook een goede oefening om het verhaal hardop te vertellen, wat ze later in het proces nodig zullen hebben als ze hun stem opnemen. Geef dus constructieve feedback over de structuur en hoe de persoon het verhaal vertelt.

In de eerste verhaal cirkel kun je ze alle ideeën laten vertellen die ze tot nu toe over het onderwerp hebben. Het is aan de coach en de cirkel om hen te helpen een beslissing te nemen over welk verhaal de deelnemer wil meenemen en waarmee hij wil werken. Niemand kan iemand ertoe aanzetten een verhaal te vertellen of te kiezen dat ze niet op hun gemak voelen bij het vertellen of gebruiken in de video. Noch de deelnemers, noch de instructeur kunnen dit doen.
Sta geen buitenstaanders of hoppers toe in de cirkel, begin en eindig met dezelfde mensen.

Pauzes. Geef verhalen de tijd om te ademen en in te zinken, dus neem na een paar verhalen een koffiepauze van vijf minuten als je met een grote groep bent. Het helpt bij het verwerken van de verhalen en helpt bij de focus van ieders presentatie.
Doe dit in een rustige afgesloten ietwat gezellige ruimte maar met weinig afleiding. Het is moeilijk om op een steriele en koude plek een persoonlijk verhaal te vertellen en ernaar te luisteren. Je kunt met stoelen in een kring of rond een tafel zitten. (zet ook de muziek uit die je speelde tijdens de schrijfsessie).

Alle gevoelens en meningen zijn welkom tijdens een verhaal. Noch is goed, fout, goed noch slecht. Ze worden op geen enkele manier beoordeeld. Iedereen heeft recht op vrijheid van meningsuiting door zichzelf in zijn verhalen te vertegenwoordigen. Ze moeten de ruimte en flexibiliteit krijgen om te beschrijven wat ze hebben meegemaakt.
Tijdens het vertellen mogen er geen onderbrekingen zijn. Zorg ervoor dat iedereen pen en papier bij de hand heeft (maar niet in hun handen, want dit kan afleidende geluiden, krabbels, enz. Veroorzaken). Als ze vragen hebben, kunnen ze deze opschrijven en vragen nadat het verhaal is verteld. Alle vragen en opmerkingen achteraf moeten constructief en positief zijn.

Herinner iedereen aan deze richtlijnen aan het begin van elke verhaallijn, zelfs als je zeker weet dat ze het eerder hebben gehoord.
Laat nu iedereen een voor een verhaal vertellen en zorg ervoor dat iedereen de ruimte heeft om feedback te geven. Dit is ook een goede oefening om het verhaal hardop te vertellen, wat ze later in het proces nodig zullen hebben als ze hun stem opnemen. Geef dus constructieve feedback over de structuur en hoe de persoon het verhaal vertelt.

In de eerste verhaal cirkel kun je ze alle ideeën laten vertellen die ze tot nu toe over het onderwerp hebben. Het is aan de coach en de cirkel om hen te helpen een beslissing te nemen over welk verhaal de deelnemer wil meenemen en waarmee hij wil werken. 
Door de ideeën hardop te vertellen, kan de persoon voelen welk verhaal het beste bij de groep terechtkomt.
Na elk verhaal wordt de deelnemers gevraagd om opmerkingen te maken of feedback te geven. Voorbeelden van vragen:
* Waar gaat het verhaal over?
* Wat was het meest memorabele moment? Waarom?
* Wat verwart je of moet je meer horen om het verhaal zijn boodschap duidelijk te laten verwoorden?
* Werkt de boog van het verhaal goed? Kun je de inleiding overslaan en die informatie door het verhaal vertellen?
* Als het perspectief te breed is, kun je het verhaal dan veranderen zodat we de gebeurtenissen zien (horen) door de ogen van de verteller? Zelfs als het verhaal over iemand anders gaat, probeer het dan vanuit het perspectief van de verteller te vertellen.

Elke deelnemer vertelt zijn of haar verhaal en krijgt feedback. De story circle kan even duren, maar het is een van de belangrijkste fasen in het proces. Met een goed georganiseerde story circle garandeer je de kwaliteit van de digitale verhalen.
Rond elke feedbackronde af met de vraag "Kun je doorgaan en je verhaal aanpassen met deze feedback?"
Als facilitator moet je zoveel mogelijk opschrijven, zeker met een groep mensen die je niet kent. Op deze manier heb je altijd iets om op terug te kijken als je bent vergeten waar een bepaalde persoon over sprak tijdens de sessies.
Als iedereen eenmaal zijn verhaal heeft verteld, heb je zeker een pauze nodig ... maar geef iedereen ook de tijd om na te denken en de feedback die ze hebben op te schrijven.
## 2.4	Een eerste stap in afbeeldingen
In dit hoofdstuk gaan we nog geen afbeeldingen maken of zoeken naar afbeeldingen die we in ons digitale verhaal zullen gebruiken ... maar we gaan nadenken over welk soort afbeelding zou werken met elk onderdeel van ons verhaal. Wat is een betere manier om dat te doen ... dan met een spel.
### 2.4.1	Interpretatie Spel
[34] Geef iedereen een vel papier en een pen. Laat alle foto's van dia [35] tm /[42] zien en laat iedereen opschrijven welke emotie of betekenis elke foto voor hen symboliseert. Als je ze allemaal hebt laten zien, begin dan opnieuw en laat iedereen vertellen wat ze hebben opgeschreven. Ze zullen zien dat elke foto meerdere verhalen kan vertellen. Hier zijn enkele interpretaties van de foto's, maar er zijn er natuurlijk nog veel meer:

![](photo1.png)
Eenzaamheid, Alleen, Avontuur, Nieuw, Bang, Opgewonden, Je klein voelen, ...

![](photo2.png)
Diversiteit, vervuiling, op elkaar gestapeld, zoetheid, verbondenheid, kleurrijk, ...

![](photo3.png)
Verbindingen , wortels, religie, geaard, paden kruisen, leeftijd, ervaring, familie, natuur, ...

![](photo4.png)
Hoop, licht na de storm, gevaar, dreiging, religie, storm komt, licht, duisternis, ...

![](photo5.png)
Nieuw leven, alleen in de menigte, zijn ten eerste, nieuw begin, bang, avontuur, familie, verbinding, ...

![](photo6.png)
![](photo7.png)
Avontuur, veiligheid, nieuw leven, familie, zich veilig voelen, geluk, Humor, ironie, gevaar, veiligheid zit op verrassende plekken, naïviteit, familie, diversiteit, oordeel nooit over een boek op de omslag,…

Waar het op neerkomt is om te laten zien dat beelden soms meer vertellen dan één ding. U kunt een foto soms gebruiken om een emotie te tonen zonder dat u deze letterlijk in uw tekst hoeft te vertellen. Laat ze maar eens nadenken over hoe sommige foto's anders kunnen worden geïnterpreteerd dan oorspronkelijk bedoeld.

[43]	Je kunt hiermee echt spelen en meer diepgang creëren in je digitale verhaal. Laat deze twee voorbeelden bijvoorbeeld zien. Ze zijn hetzelfde verhaal, maar de een gebruikt letterlijke afbeeldingen (laat zien wat er wordt gezegd) terwijl de ander een concept heeft (het zijn allemaal dieren) en drukt de emotie van het moment uit (verrast, blij, op zijn gemak, ...).

[Letterlijke afbeeldingen](https://vimeo.com/346086813)
[Diepere betekenissen](https://vimeo.com/346086728)

### 2.4.2	Storyboard
[44]	[45] Je hebt waarschijnlijk al eerder van storyboards gehoord. Ze worden gebruikt bij professionele filmproductie en worden gebruikt als communicatiedocument tussen de verschillende rollen in het productieproces. Ze worden ook wel de blauwdruk van een film genoemd. Ze bevatten richtlijnen en tekeningen voor het opnemen van elke scène uit een film en een ruwe indicatie voor de mensen die verantwoordelijk zijn voor de postproductie, voor het bewerken van elke scène.
Nadat de eerste ruwe versie van de tekst is opgeschreven en de deelnemer feedback heeft gekregen op zijn verhaal tijdens de verhalencirkel, kan er worden gespeeld met de opbouw van het verhaal. We gebruiken dit storyboard met zes frames voor de meeste van onze projecten, hoewel het gemakkelijk meer of minder kan zijn, afhankelijk van de lengte van het verhaal en de duur van uw Digital Story-sessies. Meer frames = meer tijdrovend maar meer afbeeldingen. Minder frames = minder tijd maar minder afbeeldingen.
Nu geven we onze deelnemers elk een kopie van een leeg storyboard. Dit is een gebruiksvoorwerp en een concept, en zo moet het worden gezien. Vraag de deelnemers om hun verhaal in maximaal zes delen te hakken en laat ze een ideaalbeeld bedenken voor dat deel van het verhaal. Dit kan een afbeelding zijn van iets dat letterlijk wordt gezegd, maar nog beter is een afbeelding die de emoties en sfeer van dat deel van de tekst oproept of ondersteunt.
Het maken van een storyboard is een richtlijn, een kaart om de stroom van je verhaal te laten zien en de beste ideeën van begin tot eind te schetsen. Het is er alleen om de deelnemers te helpen een structuur te creëren en het bewerkings gedeelte dat na deze fase komt gemakkelijker te maken, maar het is geen dogma, je kunt nog steeds afwijken van je schema om je verhaal te wijzigen.
Nu hun verhaal zich in verschillende hoofdstukken afspeelt, kun je ze vragen om met ze te spelen en de volgende vragen te stellen:
* Is elk hoofdstuk logisch? Waarom heb je deze zinnen in één hoofdstuk gecombineerd? Kunnen sommige worden herhaald of kunnen ze van het ene hoofdstuk naar het andere gaan?
* Staan de hoofdstukken in de juiste volgorde? Misschien kun je spelen met de volgorde van de frames die elk een hoofdstuk bevat. Misschien kun je het laatste frame naar de eerste positie verplaatsen om de impact of dynamiek van het verhaal te veranderen. Of meng ze en kijk of het verhaal nog steeds klopt. Dit kan nieuwe inzichten creëren of bevestigen dat uw tekst in de eerste plaats goed is geschreven.

We gebruiken het storyboard hier als bouwsteen en om te laten zien dat je door het veranderen van de volgorde van hoofdstukken een andere dynamiek kunt creëren, de nadruk kunt leggen op andere aspecten van je verhaal of kunt veranderen en nuances kunt toevoegen die je voorheen niet had.
Jongeren hebben soms het gevoel dat deze stap dubbel zoveel werk is: eerst moeten ze tekenen wat ze denken dat verband houdt met hun tekst en dan moeten ze een soortgelijk proces herhalen (of hetzelfde proces als je besluit dat je groep alleen afbeeldingen kan gebruiken die ze zelf hebben gemaakt). Ze realiseren zich niet hoeveel tijd hiermee bespaart wordt en het is een discussie die ook in de groep kan opduiken. Het enige wat we kunnen zeggen is dat we ervaren dat verhalen die eerst met een storyboard zijn gemaakt, beter gestructureerd, gemakkelijker te maken en doordacht zijn.
## 2.5	Afsluiting van dit hoofdstuk
### 2.5.1	Evaluatie
Voor de evaluatie van deze sessie gebruiken we de offline Twitter-methode! Geef elke deelnemer een Twitter-sjabloon en vraag hen hun gevoel of mening over deze tweede sessie in 140 tekens te schrijven. Duw ze om grappige hashtags te gebruiken om het realistischer te maken. De Twitter-bladen zijn anoniem en zodra ze allemaal zijn verzameld, kun je ze in willekeurige volgorde afspelen en hardop voorlezen!
#yolo
[Je kunt de Twitter-sjablonen hier vinden](https://drive.google.com/open?id=1lJ9gJiYU3_MK99SRZeyI3dRXzodX4iyG)

Je kunt ook de offline Instagram-methode gebruiken! Vraag ze om een foto (of tekening) te maken die ze het grootste deel van de dag leuk vonden. Dit kan symbolisch of letterlijk zijn. Laat ze dan gewoon op de projector zien. 