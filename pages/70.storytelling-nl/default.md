---
title: 'Digital Storytelling: maak en deel verhalen met open digitale technologie'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Digital Storytelling: create and share stories using open digital technologies'
lang_id: 'Digital Storytelling: create and share stories using open digital technologies'
lang_title: nl
length: '12 uren'
objectives:
    - 'Het gebruik en de integratie van verschillende media (tekst, beeld, geluid, video) in een naadloze online-omgeving'
    - 'Open digitale technologieën gebruiken om verhalen te creëren en te delen'
    - 'Een digitaal verhaal produceren en delen om de personalisering en actieve betrokkenheid van de leerlingen te stimuleren'
    - 'Het verkennen van samenwerkingsmethoden in kleine groepsactiviteiten om van elkaar te leren'
materials:
    - PC
    - Internetverbinding
    - Beamer
    - 'Prints voor onderwerp verkenning'
    - 'Etiketten voor naamkaartjes'
    - 'Veel (liefst gerecycled) papier om op te schrijven'
    - 'Prints voor actieve review-sessies'
    - 'GDPR / Copyright papieren'
    - 'Pennen, potloden, stiften, (verf? Krijt?)'
    - 'Ander knutselmateriaal, Lego / Playmobil,…'
    - 'Dixit (of alternatieven)'
    - 'Heb alles reserveonderdelen'
    - 'Een laptop'
    - Oplaadkabel
    - 'Projector (controleer aansluitingen: VGA / HDMI / (mini) DP)'
    - 'Audio-installatie (fatsoenlijke luidsprekers)'
    - 'Multi-stopcontacten en verlengers'
    - 'Microfoons indien mogelijk'
    - 'Een laptop met pedagogische dingen op je pc:'
    - 'Software, bijgewerkt en getest'
    - 'Voorbeelden (als je ze gebruikt ) gedownload op uw systeem'
    - 'De hoofdvraag in een PowerPoint-dia'
skills:
    Sleutelwoorden:
        subs:
            Storytellins: null
            'Digital story': null
            Storyboard: null
            'Storytelling circle': null
            Multimedia: null
            'Interactief verhalen delen': null
            'Open source software': null
---

## Introductie
Digital storytelling (DS) is een eenvoudige en toegankelijke manier om een verhaal te vertellen door middel van een korte film. DS is een techniek dat gebruik maakt van de tools van de digitale technologie om verhalen over ons leven te vertellen. De DS is een essentieel onderdeel van het moderne onderwijssysteem, een actief leermiddel dat leerlingen vaardigheden en competenties kan bieden die nodig zijn om open source digitale verhalen te creëren, te beheren en te delen. Om een digitaal verhaal te kunnen produceren, moeten leerlingen een digitaal vertelproces doorlopen:

![](storytelling1.png)

Bij het maken van een digitaal verhaal moeten de makers een videobewerking maken van beelden, geluid, muziek, tekst en stem. Dus, een verscheidenheid aan vaardigheden kunnen worden ontwikkeld en geoefend via het maken van digitale verhalen. Door gebruik te maken van de DS-methodologie hebben leerlingen de mogelijkheid om verhalen te maken en te delen met behulp van open digitale technologieën (bijv. Open source videobewerkingssoftware; Open source audiobewerkingssoftware enz.)

De DS biedt de leerlingen de mogelijkheid om hun eigen persoonlijke ervaringen uit te drukken, deze te organiseren in een verhaal (zelfreflectie) en gebruik te maken van digitale technieken om persoonlijke verhalen te vertellen. Naast het feit dat het een krachtig technologisch instrument is om de digitale competentie van de leerling te verbeteren in het gebruik van open software die een verscheidenheid aan multimedia combineert (tekst, stilstaande beelden, audio, video en webpublicaties), kan het DS ook een effectief instrument zijn dat de leerling de vier essentiële 21e eeuwse vaardigheden biedt (kritisch denken; creativiteit; samenwerking; communicatie).
## Context
Het doel van de sessie is om leerlingen praktisch te demonstreren en te betrekken bij hoe:
* Verschillende media (tekst, afbeeldingen, geluid, video) te gebruiken en integreren in een naadloze online omgeving
* Om open digitale technologieën te gebruiken om verhalen creëren en te delen
* Om te produceren en deel een digitaal verhaal om personalisatie en actieve betrokkenheid leerlingen te stimuleren
* Om methoden van samenwerking in kleine groepsactiviteiten te verkennen om van elkaar te leren.

Het doel van deze training is om deelnemers kennis te laten maken met DS en te leren hoe ze het kunnen gebruiken als een methode voor verhalen vertellen door gebruik te maken van open digitale technologieën.

Aan het einde van de sessies kunnen deelnemers een storyboard maken voor hun eigen digitale verhaal, relevante materialen creëren en verzamelen voor hun digitale verhaal (afbeeldingen, stem, muziek, geluiden, teksten, titels) door middel van open digitale technologieën.

De sessies zijn ook bedoeld om meer inzicht te krijgen in ethische richtlijnen, copyright, Creative Commons-licenties en toestemming.
## Hoofdstukken
Deze handleiding bestaat uit vijf hoofdstukken:
### HOOFDSTUK 1 - IEDEREEN HEEFT EEN VERHAAL…
In dit eerste hoofdstuk zullen we ons concentreren op wat Digital Storytelling is, hoe het tot de methodologie is gekomen die we kennen vandaag, hoe u begint met het verkennen en verdiepen van het onderwerp met uw groep en hoe u een fantastische vraag kunt stellen die verhalen en anekdotes oproept.
### HOOFDSTUK 2 - SAMEN EEN PERSOONLIJK VERHAAL SCHRIJVEN
In het tweede hoofdstuk bekijken we hoe we het groepsproces van het schrijven en structureren van verhalen met een verhaal cirkel kunnen vergemakkelijken. Zodra onze eerste versies van onze verhalen klaar zijn, kunnen we gaan kijken hoe we afbeeldingen kunnen kiezen op basis van interpretatie en diepere lagen.
### HOOFDSTUK 3 - EEN STERK AFBEELDING IS EEN BEELD DAT U ZELF MAAKT OF KIEST
Zodra onze verhalen klaar zijn en we een idee hebben hoe onze afbeeldingen in ons digitale verhaal eruit zullen zien, is het tijd om onze eigen afbeeldingen te vinden of, nog beter, te maken . Hier zullen we de verscheidenheid aan mogelijkheden verkennen en met tips en trucs om een goede afbeelding te maken.
### HOOFDSTUK 4 - BEWERKEN IS ALS MEDITEREN
Het deel dat het moeilijkst lijkt, is eigenlijk best ontspannen. Je hebt alle belangrijke ingrediënten om je digitale verhaal te maken ... nu is het gewoon een kwestie van alles samen in de oven leggen en een mooie cake maken. (psst, de oven is onze bewerkingssoftware in deze metafoor).
### HOOFDSTUK 5 - EXTRA'S EN FAQ
Het laatste hoofdstuk bevat alle dingen die we nergens anders zouden kunnen passen of waarvan we denken dat het te afleidend zou zijn om toe te voegen aan de andere hoofdstukken. Het bevat enkele tips en trucs voor uw voorbereiding, enkele publiciteit ideeën maar ook enkele interessante reflecties over onze werkethiek bij het maken van digitale verhalen met kwetsbare groepen.