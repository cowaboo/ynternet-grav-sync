---
title: 'Extra''s en veelgestelde vragen'
---

## 5.1	Opwarmen!
### 5.1.1	Dixit
[79] De laatste Dixit-oefening is weer een andere:
zoek een kaart om aan een andere deelnemer te geven ... Bedankt voor iets dat die persoon voor je heeft gedaan!
### 5.1.2	Nieuwe voorbeelden
[81]
#### [Voorbeeld 1: "Alles komt goed"](https://vimeo.com/181169020)
Nou, dit is gemaakt door mij, de auteur van deze gids. Direct na de aanslagen in Brussel interviewden we niet alleen jongeren uit de buurt, maar ook onze collega's en hoe die gebeurtenissen een impact hadden op ons leven.
#### [Voorbeeld 2: “Kom je”](https://vimeo.com/186559238)
In de zomer van 2016 bezochten jongeren uit Barcelona, Brussel en Zagreb tijdens een uitwisselingsprogramma een vluchtelingenkamp in Kroatië. Aan het einde van het programma maakten we met hen digitale verhalen over hun ervaringen met migratie. Hannah's ouders tekenden in voor pleegzorg voor een jonge jongen die uit zijn land was gevlucht. Dit digitale verhaal bestaat uit twee delen naarmate het verhaal in de loop van de tijd is geëvolueerd en een tweede deel is toegevoegd. Een mooi voorbeeld hoe deze resultaten ook kunnen evolueren.
#### [Voorbeeld 3: “Sarah”](https://vimeo.com/351782185)
Onderdeel van het uitwisselingsprogramma met jongeren die werken rond het thema Gender. En een geweldige video om aan het einde van deze module de boel weer op te schudden. Omdat meer dan de helft van het verhaal gewoon zwart is en andere met een zeer intense mix van video,
symbolische afbeeldingen en slam-poëzie, is dit digitale verhaal een geweldige manier om de geest te openen voor wat digitale verhalen kunnen zijn.
## 5.2	Ondertitels
[82] We krijgen vaak de vraag hoe we ondertitels aan een digitaal verhaal kunnen toevoegen en het antwoord is niet altijd eenvoudig. We gebruiken momenteel het open-source programma Aegisub, maar het is geen erg elegant programma en we hebben er momenteel ook geen tutorials voor. Als dit verandert, wordt dit aan de module toegevoegd.
U kunt Aegisub hier downloaden: http://www.aegisub.org/
Er is ook een optie om automatisch ondertitels te genereren in YouTube. Daarover meer in het volgende deel van dit hoofdstuk.
## 5.3	Onlineverspreiding
[83] [84] [85] U kunt uw digitale verhalen uploaden op Vimeo of YouTube.
Deelnemers moeten in staat zijn geïnformeerde keuzes te maken over de inhoud, de productie en het gebruik van hun werk. Zij moeten de informatie krijgen die zij nodig hebben om deze keuzes te maken en zij moeten het recht hebben om hun verhalen op elk moment weg te halen van elk platform.

Facilitators moeten ernaar streven om begeleiding te bieden bij deze besluitvormingsprocessen op een manier die de waardigheid en veiligheid van de deelnemers beschermt. Het is aan de facilitators om er zeker van te zijn dat alle video's ook offline zijn, voor het geval de deelnemers ervoor kiezen om hun video privé te plaatsen of zelfs van het internet te verwijderen.

Ze hebben ook het recht om te bepalen of hun namen al dan niet aan hun verhalen verbonden zijn en of de beelden van zichzelf en van anderen wazig zijn om de privacy te beschermen.
De dia's geven een verder overzicht van elk platform en hun respectievelijke boven- en onderzijde.
## 5.4	De training afmaken met wat extra's
### 5.4.1	Resume
[87] Nogmaals een mooi overzicht van alle stappen van het Digital Storytelling proces.

We beginnen met energizers om mensen aan het praten te krijgen, vertrouwd te raken met het vertellen van verhalen en veiligheid in de groep te installeren.
Vergeet ook niet in het onderwerp te duiken.
Vervolgens gaan we verder met het vertellende zelf. Je hebt een goede vraag nodig om mensen te inspireren, een verhaal te vinden, wat ruimte en tijd om deze vraag met een anekdote te beantwoorden en een story circle te installeren.

Als de verhalen klaar zijn, richten we ons op afbeeldingen. Welke afbeeldingen zijn sterker dan andere en waar je ze kunt vinden of hoe je ze kunt maken. Als we onze tekst en afbeeldingen hebben, is het tijd om ze samen te brengen en onze video te bewerken!
Net zo belangrijk als alle voorgaande stappen is het organiseren van een screening voor uw project. In sommige gevallen kan dit een openbare vertoning zijn, maar afhankelijk van het project en de deelnemers is een besloten vertoning alleen voor uw deelnemers voldoende.
### 5.4.2	Tijdsdruk
[88][89][90] De lengte van dit programma wordt aanbevolen om 5 volle dagen van telkens 5 uur te organiseren, maar we realiseren ons dat dit veel tijd is om te vragen van uw deelnemers. We raden echter wel aan om minimaal drie dagen te nemen.
We hebben enige ervaring met Digital Storytelling projecten van één dag, maar het is gewoon te riskant om aan te bevelen. Alles moet volgens plan verlopen: computers kunnen niet uitvallen, het internet moet stabiel zijn, alle ruimtes die je nodig hebt moeten vrij zijn, ... het is veel om mee om te gaan en de resultaten zijn over het algemeen erg week.
### 5.4.3	Deelnemersprofiel
[91]	Over het algemeen is het ideaal om met een groep van 6 tot 7 deelnemers per trainer te werken, maar u kunt maximaal 10 deelnemers als u goed opgeleid bent. Sommige onderdelen zullen in kleinere groepen moeten worden georganiseerd om effect te hebben.

Digital Storytelling is mogelijk voor deelnemers van alle leeftijden. U hoeft alleen de inhoud en de methode van uw activiteit aan te passen aan de leeftijd, de omgeving en de context van de doelgroep. We raden aan om de leeftijd van 12 jaar als minimum te gebruiken om op zijn minst interessante inhoud te hebben.
Het is het beste om de methodologie licht te houden en met voldoende energizers om ze geïnteresseerd te houden. Belangrijk is om de deelnemers vanaf het begin aan de haak te slaan. We laten voorbeelden zien die verschillende technieken gebruiken in storytelling, editing of imaging om hen te laten zien dat je sterke verhalen kunt vertellen die iedereen kunnen imponeren, een sfeer kunnen creëren en emoties kunnen oproepen met eenvoudige methoden.

Het stappenplan is voor elke groep en voor alle leeftijden hetzelfde, maar bij kinderen is de methode speelser dan bij volwassenen. In het geval dat we met onze jongeren werken, werkt de eerste optie het beste.
Uitgebreide ICT-kennis is niet nodig. De enige voorwaarde is dat de deelnemers bekend zijn met computers: deelnemers die nog nooit een muis of toetsenbord hebben gebruikt of niet weten hoe ze met bestanden, mappen en menu's moeten werken, zullen het moeilijk hebben om alle informatie te verwerken. Daarom is het belangrijk om de computer- en ict-vaardigheden van de deelnemers te beoordelen: werken ze vaak met computers? Hebben ze ervaring met multimedia of het gebruik van sociale netwerken? Deze vragen kunnen gesteld worden tijdens de kennismakingsronde wanneer de deelnemers zich voorstellen.

Afhankelijk van het vaardigheidsniveau en de behoeften van je deelnemers kun je meer nadruk leggen op:
* De sociale interactie binnen de groep.
* De therapeutische resultaten van het vertellen van je eigen (levens) verhaal.
* De creatieve aspecten van het maken van een korte film.
* De grafische vormgeving van de beelden.
* Bouwen aan een leuk verhaal.
* De inhoud of het thema van het programma



### 1.1.1	Voorbereiding
[91]	Bekijk de waardevolle tips in de dia's
### 1.1.2	Benodigde materialen
[93] [94] [95]

Analoog materiaal:
* Prints voor onderwerp verkenning.
* Labels voor naamkaartjes.
* Veel (liefst gerecycled) papier om schrijf op.
* Prints voor actieve review-sessies.
* GDPR / Copyright papieren.
* Pennen, potloden, stiften, (verf? Krijt?).
* Ander knutselmateriaal, Lego / Playmobil,…
* Dixit (of alternatieven).
* Hebben van alles reserveonderdelen!

Digitaal materiaal:
Voor elke deelnemer:
* Een laptop (met logo's en software (enz.) Er al op).
* Oplaadkabel.
Heb van alles minstens 3 reserveonderdelen!
* Projector (controleer aansluitingen: VGA / HDMI / (mini) DP).
* Audio-installatie (fatsoenlijke luidsprekers).
* Multi-stopcontacten en verlengers.
* Microfoons indien mogelijk.
* Een laptop met pedagogische dingen op je pc:
* Software, bijgewerkt en getest.
* Voorbeelden (als je ze gebruikt ) gedownload op uw systeem
* De hoofdvraag in een PowerPoint-dia

### 1.1.3	Impact
[96]	Het maken van een digitaal verhaal is geen moeilijke uitdaging met betrekking tot de gebruikte technologie. Daarom is deze methodiek zeer geschikt voor deelnemers die hun eerste filmpje maken. De deelnemers kunnen zich volledig concentreren op het verhaal en de inhoud. Bovendien kunnen ze hun film visueel verbeteren met eenvoudige dingen zoals het creëren van effecten op afbeeldingen, overgangen tussen afbeeldingen, muziek, geluid, het plaatsen van titels op het scherm en meer.
Bovendien is het feit dat ze hun eigen verhaal kunnen vertellen met eenvoudige middelen die een publiek kunnen boeien, voor veel deelnemers een grote ontdekking. Dit kunnen ze alleen bereiken als ze met hun eigen stem de juiste afbeeldingen, muziek en persoonlijk commentaar kiezen.
Een digitaal vertelproject heeft een positief effect op alle aspecten van het leven van de deelnemers:
* Je kunt je eigen verhaal of mening vertellen; het creëert waarde voor andere mensen.
* Je kunt je creatief uitdrukken.
* Een digitaal verhaal maken is een mix van denken met je hoofd en dingen doen met je handen: iets wat mensen graag doen, maar niet altijd de kans krijgen (ze zijn teveel bezig in hun dagelijks leven).
* Het maken van deze films is ook een groepsevenement: je leert elkaar beter kennen, je leert nieuwe mensen kennen en er zijn veel sociale voordelen.
* Door je eigen digitale verhaal te maken kun je wat afstand scheppen tussen jij en je verhaal: dit kan een therapeutische werking hebben.

In al deze gevallen hebben onze deelnemers gemerkt dat andere mensen geïnteresseerd zijn in wat ze te zeggen hebben: dit heeft een positief effect op het zelfbeeld en het zelfvertrouwen van de deelnemer . Hierdoor kan een dergelijk project een emanciperende werking hebben. Daarom is het erg belangrijk om een dergelijk programma af te sluiten met een openbare vertoning, de filmpjes op internet te publiceren (en te promoten op sociale netwerken zoals Facebook) en, indien mogelijk, alle deelnemers een dvd te geven met de eindresultaten van het Digital Storytelling-programma.

Het is ook belangrijk dat facilitators ethiek zien en implementeren als een proces, in plaats van als een eenmalige gelegenheid om 'toestemming te krijgen'. Een voortdurende dialoog tussen deelnemers, trainers en partnerorganisaties / instellingen over hoe een ethisch verantwoord project het beste kan worden ontworpen en geïmplementeerd, is de sleutel tot ethische praktijk.

Maar digitale verhalen kunnen ook los van het creatie proces worden gebruikt. Omdat verhalen reflecteren op interessante en maatschappelijk relevante onderwerpen, zijn ze ook interessant om te gebruiken als gespreksonderwerp of bronmateriaal om in een bepaald onderwerp te duiken. Zoek gerust voorbeelden van digitale verhalen op ons Vimeo-kanaal: https://vimeo.com/maksvzw

### 1.1.1	Ethiek
[96]	
-	De verspreiding moet vanaf het begin duidelijk zijn, je mag geen video's in het openbaar of in een andere context vertonen zonder de toestemming van de maker.
-	Een onderwerp forceren zal nooit werken, zeker niet een onderwerp dat geen verband houdt met je deelnemers.
-	Dezelfde regels van de verhalencirkel zijn van toepassing op de hele methodiek (Veiligheid) voor de trainer.
Belang van stappen: Forceer nooit iemand en respecteer grenzen.
-	Privacy: nogmaals, wees vanaf het begin duidelijk!
-	Voice-overs van andere mensen hebben niet dezelfde impact als de echte verteller. We weten dat mensen hun eigen stem niet leuk vinden. Niemand doet. Maar dit is iets dat… nou… iedereen moet er gewoon overheen komen. We zeiden net dat we niemand moeten dwingen iets te doen wat ze niet willen. Maar het opnemen van je eigen stem is in onze projecten de basisregel en uitzonderingen kunnen alleen worden gemaakt om privacyredenen.

### 1.1.2	Onderwerpen
[97]	Enkele onderwerpen waar wij ervaring mee hebben en voor inspiratie kunt u bij ons terecht.
Mijn job (en ik), Mijn talenten, Identiteit (Wie ben ik?), Solidariteit, Extremisme (en sociale media), Mijn stad, (Gender) identiteit en discriminatie, Diversiteit en inclusie, Ecologie en milieu.
1.1.3	Andere bronnen
[98]	Enkele andere bronnen die u kunt gebruiken voor Digital Storytelling:
Brights: http://www.brights-project.eu

[Digital Welcome](https://digitalwelcome.eu/)
Een verzameling handleidingen om vluchtelingen en nieuwkomers digitale tools aan te leren met een grote module Digital Storytelling (proef in België in de OKAN-klassen van het Sint-Guido Instituut in Anderlecht). Hieraan gekoppeld is ook een animatorcursus om de digitale workshops door te geven aan andere jongeren. DS werd meer als evaluatietechniek gebruikt, maar de handleiding is vrij compleet.
Andere modules gaan over basiscodering en digitale journalistiek.
Digital Welcome heeft dit jaar de "Life Long Learning Award for Best Practice in 'Promoting Values'" van de Europese Commissie gewonnen!
Praktijken: https://practicies-digitalme.org
Handleiding over hoe om te gaan met radicalisering bij jongeren met digitale verhalen
Huristo: http://huristo.eu/
Online toolkit met zelfgemaakte foto's om digitale verhalen te maken met semi - analfabete volwassenen.

## 1.1	Het afsluiten van de opleiding
### 1.1.1	evaluatie
[96]	voor het beëindigen van onze trainingen gebruiken we de methode van The Rose:
je hoeft alleen maar een echte roos of een getrokken / gedrukte afbeelding van een roos op een stuk papier.
* De bloem van de roos staat voor wat je het leukst vond van wat je hebt geleerd.
* De doorn is wat je het meest niet leuk vond of was het moeilijkste / moeilijkste van het project.
* De onderkant van de roos staat voor de kans, wat ga je nu doen met alle opgedane kennis?

Geef de roos als je klaar bent aan iemand waarvan je denkt dat hij de roos heeft verdiend omdat	de
persoon hard heeft gewerkt, zijn of haar grenzen heeft verlegd of omdat je het graag goed zou willen maken met iemand vanwege eerdere problemen.
Een uitgebreid alternatief heet "Backpack, Fridge, Trash Can". Je hebt nodig:
* 1 bal.
* 1 muziekmachine (we gebruiken een smartphone, kan een tablet, laptop, cd-speler, radio, ... zijn).
* 1 ding om een rugzak voor te stellen (we gebruiken een echte rugzak).
* 1 ding om een koelkast (we gebruiken een plastic voedselcontainer).
* 1 ding om een prullenbak voor te stellen (we gebruiken een echte prullenbak).

Als je de laatste drie dingen niet kunt vinden, kun je ze gewoon op het bord of op stukjes papier tekenen en in de midden.
Ga nu met z'n allen om de rugzak, de koelkast en de prullenbak in een cirkel zitten. Blijf de bal geven totdat iemand de muziek stopt. Wie de bal nu vasthoudt, moet het volgende zeggen:
* De rugzak: iets dat je hebt geleerd en dat je de rest van je leven of in ieder geval een tijdje met je meedraagt.
* De koelkast: iets wat je nog niet onder de knie hebt en die je voorlopig in de koelkast zet om er op een bepaald moment later nog eens naar te kijken.
* De prullenbak: iets dat je weggooit, iets dat je echt niet leuk vond of niet relevant vond.

Bijvoorbeeld: “Voor de rugzak kies ik voor Kdenlive, het was zo eenvoudig te gebruiken en superhandig.	Voor de koelkast neem ik de rechtenvrije afbeeldingen. Ik vind dat het beperkend is in je keuzes en ik geef er de voorkeur aan om mijn eigen foto's te maken, maar ik zal er later meer naar kijken. Voor de prullenbak neem ik het trage en instabiele internet, dat creëerde frustratie en vertraagde onze vooruitgang.” Als de persoon klaar is, gaat de bal weer rond en kan de vorige spreker nu beslissen wanneer de muziek stopt!

## 1.1	Vragen en contact
Als je het gevoel hebt dat je meer vragen hebt neem dan gerust contact met ons op via e-mail: jasper@maksvzw.org
We zullen proberen zo snel mogelijk contact met je op te nemen!

