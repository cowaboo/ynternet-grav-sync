---
title: 'Pronti per l’E-learning! Usare l’E-Learning per promuovere l’Educazione Aperta'
objectives:
    - 'Fornire una panoramica della storia, la filosofia e lo sviluppo di Moodle'
    - 'Riconoscere il principale valore aggiunto di  Moodle'
    - 'Esplorare le caratteristiche principali di Moodle'
    - 'Utilizzare le tecnologie digitali aperte per ricevere supporto dalla community Moodle'
    - 'Stimolare il coinvolgimento nella community online e lo sviluppo del software open-source'
---

## Discussione di gruppo su Moodle
L’insegnante comincerà con l’introduzione al modulo, chiedendo ai partecipanti della loro esperienza nell’ambito del Sistema di gestione dell’apprendimento Open Source per l’istruzione e/o la formazione professionale.
## Introduzion al LMS di Moodle: Libertà di apprendere
Moodle è un progetto attivo e in evoluzione avviato da Martin Dougiama, pedagogo e programmatore australiano. 
“Mi impegno a portare avanti il mio lavoro su Moodle ed a mantenere la piattaforma libera e aperta. Sono profondamente convinto dell’importanza dell’istruzione libera e dell’insegnamento, e Moodle è il mezzo principale con cui posso contribuire alla realizzazione di questi ideali”.

È il primo Sistema di gestione dell’apprendimento Open source portato a conoscenza del pubblico nel 2002. Moodle 1.0 è stato lanciato ufficialmente nel 2002 come piattaforma utile ad offrire agli educatori la tecnologia necessaria per fornire apprendimento online in ambienti personalizzati che incentivano l’interazione, la ricerca e la collaborazione. Da allora c’è stata una creazione costante di nuove versioni che hanno aggiunto nuove caratteristiche, migliore scalabilità e performance ottimizzata.

Oggi  Moodle è il Sistema di gestione dell’apprendimento più popolare ed utilizzato. Con 100 milioni di utenti, numero in crescita, e più di 100,000 siti Moodle presenti in tutto il mondo, questa piattaforma eLearning di facile utilizzo soddisfa i bisogni di apprendimento e formazione di tutti i tipi di organizzazioni, in più di 225 paesi in tutto il mondo.
## Perchè Moodle è il Sistema di gestione dell’apprendimento maggiormente utilizzato?
Oltre ad essere ampiamente personalizzabile e flessibile, Moodle non smette mai di migliorare. Infatti, essendo un progetto open source, Moodle è frutto della collaborazione ed è supportato da una solida comunità a livello globale. Sviluppatori da tutto il mondo possono accedere al codice e modificarlo per migliorare continuamente il software e renderlo più sicuro.

Originariamente concepito per l’educazione superiore, Moodle è ora utilizzato non solo in scuole superiori, ma anche in scuole primarie, organizzazioni non-profit, aziende private, insegnanti indipendenti e anche da genitori che studiano a casa. Partendo dall’educazione, Moodle ha raggiunto il mondo delle imprese, e ad oggi sta guadagnando popolarità anche nel settore dell’assistenza sanitaria.
## Caratteristiche principali di Moodle 
Introduzione a:
* Interfaccia
* Dashboard
* Strumenti e attività di collaborazione
* Calendario
* Gestione di file
* Gestione di testo
* Sistema di notifica
* Monitoraggio progresso 

## Contribuire allo sviluppo del software open-source
Essendo una piattaforma di apprendimento open source, Moodle offre la possibilità di contribuire alla piattaforma in tanti modi diversi:
* Supporto utente: Ricevi supporto dalla community, condividi idee e incontra altri utenti nel [Community Forum](https://moodle.org/course/).
* Test: Aiuta a trovare e segnalare i bug tramite il Moodle Tracker.
* Facilità di utilizzo: Esegui dei test sulla facilità di utilizzo e riporta i tuoi risultati.
* Traduzione: Contribuisci alla traduzione di Moodle, fornendo traduzioni/correzioni.
* Social Media: Contribuisci a diffondere la conoscenza del progetto Moodle e condividi notizie ed eventi più recenti del mondo Moodle.

Inoltre, gli utenti hanno la possibilità di aderire all’Associazione Utenti Moodle. L’Associazione è un’organizzazione non-profit ufficiale, dedita a contribuire allo sviluppo di Moodle. L’Associazione Utenti Moodle sostiene la crescita di Moodle dando voce agli utenti e offrendo indicazioni e risorse per nuovi miglioramenti.
## Homework
Dal sito web Moodle, vai allla  [sezione demo](https://moodle.org/demo/) e visita la [Mount Orange school Moodle](https://school.moodledemo.net/mod/page/view.php?id=45). Da lì, usando le credenziali (effettua l’accesso come 'insegnante', password 'moodle'), puoi navigare nella piattaforma Moodle, creare delle  attività e poi effettuare l’accesso come studente (username: student; password: moodle) per provarle.
## Riferimenti
* [What is Moodle?](https://www.youtube.com/watch?v=wop3FMhoLGs)
* [Introduction to Moodle](https://www.lambdasolutions.net/guides-and-research/moodle-user-guide-intro-to-moodle)
* [Using Moodle](https://support.moodle.com/hc/en-us/categories/115000822747-Using-Moodle)
* [Moodle’s core features](https://docs.moodle.org/37/en/Features)
* [Moodle Dashboard](https://docs.moodle.org/37/en/Dashboard)
* [Moodle Activities](https://docs.moodle.org/37/en/Activities)
* [Moodle Calendar](https://docs.moodle.org/37/en/Calendar)
* [Working with files](https://docs.moodle.org/37/en/Working_with_files)
* [Moodle Text editor](https://docs.moodle.org/37/en/Text_editor)
* [Moodle Messaging](https://docs.moodle.org/37/en/Messaging)
* [Tracking progress](https://docs.moodle.org/37/en/Tracking_progress)
* [Contributing to Moodle](https://docs.moodle.org/dev/Contributing_to_Moodle)
* [Moodle Users Associations](https://moodleassociation.org/)
* [Moodle Community Forums](https://moodle.org/course/)