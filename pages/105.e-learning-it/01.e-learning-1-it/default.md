---
title: 'Fondamenti di E-Learning – Background ed Evoluzione'
objectives:
    - 'Identificare le prospettive storiche e e la crescita dell’E-Learning'
    - 'Scoprire i principali strumenti di E-Learning'
    - 'Esplorare le potenzialità dei diversi programmi di E-Learning'
    - 'Valutare pro e contro dei vari programmi di E-Learning'
    - 'Scoprire i vantaggi dell’ E-Learning per la soddisfazione di un’ampia gamma di esigenze di apprendimento'
    - 'Identificare l’efficacia degli ambienti E-Learning'
    - 'Esplorare il potenziale dell’ E-Learning nell’agevolazione dell’apprendimento continuo'
---

## Discussione di gruppo sull’apprendimento online
L’insegnante  comincerà con l’introduzione al modulo, chiedendo ai partecipanti delle loro esperienze/conoscenze precedenti nell’ambito dell’apprendimento online. Verrà loro chiesto, inoltre, di focalizzarsi sui vantaggi e gli svantaggi dell’apprendimento online.
## Nozioni base dell’E-Learning
Nel 1999, durante il seminario dei sistemi CBT negli Stati Uniti, viene usata per la prima volta una nuova parola in un ambiente professionale: 'E-Learning'. Associato a espressioni come 'apprendimento online' o 'apprendimento virtuale', questo termine intendeva qualificare "un modo di apprendere basato sull'uso di nuove tecnologie che consentono l'accesso alla formazione online, interattiva e talvolta personalizzata attraverso Internet o altri mezzi elettronici (intranet, TV interattiva, ecc.), in modo da sviluppare competenze mentre il processo di apprendimento è indipendente dal tempo e dal luogo".

Oggi, grazie all'introduzione del computer e di internet, gli strumenti di E-Learning e le modalità di erogazione si sono ampliati. Oggi l'E-Learning è più popolare che mai, con moltissimi individui che si rendono conto dei vantaggi che l'apprendimento online può offrire. Anche le aziende hanno iniziato ad utilizzare l'E-Learning per formare i propri dipendenti, dando loro la possibilità di migliorare la loro base di conoscenza  del settore ed espandere le loro competenze. A casa gli individui hanno avuto accesso a programmi che offrivano loro la possibilità di conseguire lauree online e arricchire la loro vita attraverso una conoscenza più vasta.

![](e-learning%201.png)

## Esplorare gli strumenti di E-Learning
Ci sono un sacco di tecnologie coinvolte nell'e-learning. Possono essere suddivise in tre aree chiave: creazione di contenuti, LMS e TMS. Prima di iniziare ad utilizzare gli strumenti di E-Learning, è importante conoscere la differenza tra i diversi strumenti utilizzati per creare, sviluppare e fornire contenuti di E-Learning.

1. Authoring di contenuti: uno strumento di authoring di contenuti e-learning è un pacchetto software che gli sviluppatori utilizzano per creare e confezionare contenuti e-learning da distribuire agli utenti finali. Uno strumento di authoring dei contenuti è un'applicazione software utilizzata per creare contenuti multimediali distribuiti generalmente sul World Wide Web. 
2. Un LMS (Learning Management System) è un'applicazione software per l'amministrazione, la documentazione, il monitoraggio, il reporting e l'erogazione di corsi di istruzione, programmi di formazione o programmi di apprendimento e sviluppo. Quindi, LMS è una soluzione software che crea e fornisce ambienti di apprendimento personalizzati per gli studenti, fornisce materiale didattico in molteplici forme e attraverso una varietà di attività come quiz, contenuti video e materiale personalizzato. I LMS sono focalizzati sull'erogazione dell'apprendimento online ma si prestano ad una vasta gamma di usi, fungendo da piattaforma per i contenuti online, compresi i corsi, sia asincroni che sincroni. Un LMS fornisce e gestisce tutti i tipi di contenuti, inclusi video, corsi e documenti. Nei settori dell'istruzione e dell'istruzione superiore, un LMS includerà una varietà di funzionalità simile a quella aziendale, ma avrà caratteristiche come rubriche, apprendimento facilitato per insegnanti e istruttori, un forum di discussione, e spesso l'utilizzo di un programma di studio. Un programma di studio è raramente una caratteristica dell'LMS istituzionale, anche se i corsi possono iniziare con l'indice dei contenuti per dare agli studenti una panoramica degli argomenti trattati.

L'LMS può essere utilizzato per creare contenuti di corsi professionali. L'insegnante può aggiungere testo, immagini, tabelle, link e formattazione del testo, test interattivi, presentazioni ecc. Il Sistema aiuta a controllare a quali contenuti può accedere uno studente, a monitorare i progressi nello studio e a coinvolgere lo studente tramite strumenti di contatto. Gli insegnanti possono gestire corsi e moduli, iscrivere gli studenti o impostare l'auto-iscrizione, vedere i report sugli studenti e “importare” gli studenti nelle loro classi online.
3. Il TMS (Training Management System) è utilizzato dagli enti di formazione per gestire gli aspetti commerciali delle loro operazioni di formazione. Ciò include la gestione delle date e delle sessioni di formazione, la registrazione e il pagamento online, la promozione dei corsi e la comunicazione con gli iscritti.

## Diverse tipologie di E-Learning
Più i nostri E-Learning sono in linea con i nostri bisogni e abitudini di apprendimento, più facile sarà beneficiare degli strumenti di eLearning. 
Allineare il nostro approccio all’ E-Learning con le nostre esigenze è essenziale per ottenere il massimo dall’ambiente di apprendimento. I programmi di e-learning possono assumere molte forme, come ad esempio:
* Apprendimento online misto: è un approccio all’educazione che combina materiale didattico online ed opportunità per l’interazione online con metodi tradizionali basati sull’apprendimento in classe. L’approccio richiede la presenza fisica di insegnante e studente, quest’ultimo ha possibilità (seppur limitate) di controllo su tempo, posti..etc; Questo tipo di apprendimento permette il feedback immediato e l’analisi dei risultati che può potenzialmente aiutare gli studenti ad individuare i propri errori ed aree critiche. I vantaggi dell’apprendimento misto comprendono l’interazione tra gruppi più ampi, interazione faccia a faccia, apprendimento personalizzato e costi ridotti.
* Apprendimento online asincrono: Questa tipologia di eLearning non dipende da luogo o tempo. Il miglior modo di gestire l’eLearning asincrono è tramite un Sistema di gestione dell’apprendimento, che rende possibile monitorare il progresso di ogni studente. Generalmente, l’interazione avviene tramite blog e forum di discussione piuttosto che tramite approccio frontale. Agli utenti viene comunicato il contenuto ed i dettagli più importanti del corso e delle attività utili alla valutazione. L’eLearning asincrono vede lo studente al centro della metodologia. Lo studente decide quando affrontare nuove sezioni dell’eLearning, quando prendere una pausa e quali parti dell’eLearning saltare, nel caso in cui la competenza/conoscenza trattata sia già stata acquisita precedentemente.
* Apprendimento online sincrono: Avviene in tempo reale, in quanto gli studenti e l’insegnante “si incontrano” nello stesso momento in un’aula virtuale. L’indipendenza di studenti e mediatori dalla posizione degli altri individui coinvolti è uno dei più grandi vantaggi dell’apprendimento eLearning sincrono, purchè sia disponibile una buona connessione internet. Si caratterizza, inoltre, per un minore dispendio di risorse ( in termini di tempi e costi di spostamenti) sia per l’insegnante che per gli studenti.

![](e-learning%202.png)
[MDI Training](https://www.mdi-training.com/blog/blog/elearning/)
## E-Learning Permanente
Gli sviluppi tecnologici hanno contribuito considerevolmente a cambiamenti radicali nella maggior parte dei settori della nostra vita, istruzione inclusa. In un mondo così globalizzato, c’è una domanda crescente di nuove capacità. “Le competenze ICT sono imprescindibili nel mercato globale, e la formazione flessibile e permanente è necessaria per rimanere competitivi. L’istruzione aperta a distanza (ODE) è abbastanza flessibile per fornire soluzioni al bisogno di formazione permanente nella società globale” (Bodil Ask, Sven Ake Bjorke, UNU/Global Virtua University, Norway).
## Homework
Per avere un’idea migliore dei pro e dei contro dell’insegnamento online, viene richiesto ai partecipanti di guardare alcuni video a riguardo e prendere appunti:
* [Online learning advantaged ad disadvantages](https://www.youtube.com/watch?v=tcb9puhA3fo).
* [The e-learning advantages](https://www.youtube.com/watch?v=nzV1NmhC7ik).

Successivamente, gli studenti dovranno creare la loro lista di pro e contro.
## Riferimenti
* [History of E-Learning](http://www.leerbeleving.nl/wbts/1/history_of_elearning.html).
* Advantages and disadvantages of E-Learning.
* [Introduction to eLearning, Course Authoring, LMS](https://www.youtube.com/watch?v=nEYAWU5fQOs).
* [What are content authoring tools and how can we use them?](https://www.talentlms.com/elearning/what-is-a-content-authoring-tool).
* [LMS vs CMS vs LCMS…What’s the Difference?](https://www.lessonly.com/blog/lms-vs-cms-vs-lcms-whats-difference/).
* [What is a Training Management System?](https://elearningindustry.com/training-management-system-tms).
* [LMS -Wikipedia](https://en.wikipedia.org/wiki/Learning_management_system).
* [Types of E-Learning](https://www.youtube.com/watch?v=XL5gZYqHO3M).
* [E-Learning ways](https://elearningways.com/what-is-e-learning-why-does-it-matter-lets-explore/).
* [Benefits of Studying online](http://online.illinois.edu/articles/online-learning/item/2017/06/05/5-benefits-of-studying-online-(vs.-face-to-face-classroom)).
* [Pros and Cons of online education](https://www.ies.ncsu.edu/resources/white-papers/pros-and-cons-of-online-education/).
* [Lifelong E-Learning](http://www.eden-online.org/wp-content/uploads/2016/05/Annual_2005_Helsinki_Proceedings.pdf).