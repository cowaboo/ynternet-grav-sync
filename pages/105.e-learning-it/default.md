---
title: 'E-Learning con gli strumenti FLOSS '
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: it
length: '9 ore e 30 minuti'
objectives:
    - 'Riconoscere I vantaggi dell’ E-Learning nell’istruzione'
    - 'Esplorare il principale Sistema di gestione dell''apprendimento open source: MOODLE'
    - 'Scoprire e sfruttare la community Moodle'
materials:
    - 'Personal computer'
    - 'Connessione Internet'
    - Moodle
skills:
    'Parole chiave':
        subs:
            'Educazione permanente': null
            'Learning Management System (LMS)': null
            'Ap-prendimento asincrono': null
            Moodle: null
            MOOC: null
---

# Introduzione
_L’ E-Learning sfrutta le tecnologie interattive ed i sistemi di comunicazione per migliorare l’esperienza di apprendimento. Esso ha il potenziale di trasformare il metodo di apprendimento e insegnamento su tutta la linea. Può alzare gli standard e ampliare la partecipazione all’educazione permanente. Non può rimpiazzare insegnanti e docenti, ma insieme ai metodi esistenti può migliorare la qualità e i risultati del loro lavoro, e ridurre il tempo impiegato nella gestione. L’E-learning può consentire ad ogni studente di realizzare il proprio potenziale e aiutare a costruire una forza lavoro educativa in grado di cambiare. Rende possibile un sistema educativo veramente ambizioso per una futura società conoscitiva._
Verso una strategia unificata di e-Learning, l'unità Strategia di e-Learning del DfES, 2003.

Lo scenario formativo fornisce un'introduzione alle metodologie e agli strumenti dell'E-Learning. Mentre gli strumenti di e-learning hanno ridisegnato l'ambiente educativo, le sessioni sono state progettate per fornire una panoramica del background, delle caratteristiche chiave e dei metodi dell'eLearning. 
  
La formazione passerà attraverso alcuni vantaggi dell'apprendimento online. In particolare, permetterà agli studenti moderni di esplorare il potenziale dell'E-Learning come strumento per promuovere la collaborazione online. Infatti, l'eLearning può essere uno strumento efficace sia per gli studenti che per gli educatori, in particolare per promuovere la collaborazione online (piattaforme di e-learning).
 I partecipanti proveranno la piattaforma di apprendimento Moodle per lavorare e imparare insieme in forum, wiki, glossari, attività di database e molto altro ancora. La formazione fornirà agli studenti una comprensione critica del processo di apprendimento.
## Contesto
L’obiettivo della sessione è dimostrare praticamente agli studenti, coinvolgendoli, come:
* Riconoscere I vantaggi dell’ E-Learning nell’istruzione.
* Esplorare il principale Sistema di gestione dell'apprendimento open source: MOODLE.
* Scoprire e sfruttare la community Moodle.

I cambiamenti evolutivi nella tecnologia educativa hanno cambiato il concetto di insegnamento e di apprendimento così come lo conosciamo.
Ci sono certamente molti vantaggi per l'insegnamento/apprendimento in un ambiente online. L'e-learning può rendere l'apprendimento più semplice, più facile e più efficace. Oltre a servire diversi stili di apprendimento, l'E-Learning è anche un modo per impartire rapidamente le lezioni. È anche uno strumento importante in termini di collaborazione e di costruzione della comunità. Infatti, l'E-Learning non deve essere necessariamente un viaggio individuale. Attraverso funzionalità come forum e tutorial dal vivo, gli utenti hanno la possibilità di raggiungere gli altri membri della comunità di apprendimento. L'impegno con gli altri utenti promuove la collaborazione e la cultura di squadra, che ha ricadute positive anche al di là dell'ambiente di formazione.
Alla fine delle sessioni, i partecipanti potranno utilizzare e sfruttare Moodle, una delle principali piattaforme di apprendimento Open Source, che offre un potente set di strumenti incentrato sullo studente e ambienti di apprendimento collaborativo che danno potere sia all'insegnamento che all'apprendimento.
## Sessioni
### Prima sessione: Fondamenti di E-Learning – Background ed Evoluzione
L'e-Learning è l'impiego di tecnologie per aiutare e migliorare l'apprendimento. Lo scopo dell'E-Learning è quello di permettere alle persone di imparare senza frequentare fisicamente un ambiente educativo tradizionale. 
Per capire meglio come l'E-Learning vada a vantaggio sia degli educatori che degli studenti di oggi, è utile guardare al suo passato. Il termine 'E-Learning' esiste solo dal 1999, quando è stato utilizzato per la prima volta in un seminario sui sistemi CBT. L'esperto di tecnologia educativa Elliott Maisie ha coniato il termine "E-Learning" nel 1999, segnando la prima volta che la frase è stata usata professionalmente. Tuttavia, i principi alla base dell'e-learning sono stati ben documentati nel corso della storia, e vi sono anche prove che suggeriscono che le prime forme di e-learning esistevano già nel XIX secolo. Oggi l'e-learning è più popolare che mai. 

La sessione fornisce anche una panoramica di alcune delle tecnologie coinvolte nell'e-learning, come l'authoring dei contenuti, LMS e TMS.
Nell'"era dell'informazione", l'educazione permanente è vista come la chiave per il continuo successo della società moderna. L'e-Learning è considerato da molti come la migliore soluzione al problema di fornire le risorse necessarie per facilitare l’educazione permanente.
Quali fattori hanno permesso all'eLearning di diventare il metodo più popolare per impartire formazione oggi? Perché utilizzare l'eLearning? Questa sessione fornisce una panoramica delle potenzialità e dei vantaggi dell'e-learning.
### Seconda sessione: Pronti per l’E-learning! Usare l’E-Learning per promuovere l’Educazione aperta
Questa sessione fornirà un'ampia panoramica di ciò che Moodle può fare. Esplorerà l'uso e i benefici di Moodle, uno dei più popolari sistemi di gestione dell'apprendimento open-source (LMS). La formazione fornirà le informazioni di base necessarie agli studenti per iniziare a utilizzare la piattaforma di apprendimento e beneficiare del supporto online degli utenti di tutto il mondo e della condivisione di idee. 