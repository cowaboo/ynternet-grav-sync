---
title: 'Flipped Classroom'
objectives:
    - 'Proporcionar una introducció a Flipped Classroom i els principals conceptes involucrats.'
    - 'Facilitar la transformació d’una formació en una aula invertida.'
    - 'Descobrir la taxonomia de Bloom i les diferències entre les habilitats de pensament de baix nivell i les habilitats de pensament d''alt nivell, així com les diferents categories d’habilitats (coneixement, comprensió, aplicació, anàlisi, síntesi i avaluació)'
    - 'Motivar els estudiants a canviar la seva manera d’ensenyar'
lenght: '150 min'
---

## Introducció
L'objectiu d'aquesta primera part és ressaltar els coneixements previs de el grup. També es convida els participants a compartir informació sobre el context de la seva participació en la sessió (motivació, objectius personals, etc.)
En resum,  “[The Flipped Classroom (FC)](http://www.theflippedclassroom.es/what-is-innovacion-educativa/) és un model pedagògic que desplaça el treball de certs processos d'aprenentatge fora del aula i inverteix el temps de classe, juntament amb l'experiència del innocent, per a promoure i impulsar altres processos d'adquisició i pràctica de el coneixement dins del aula". El formador utilitza els recursos i eines digitals disponibles per proporcionar als alumnes un avanç dels continguts necessaris, que revisaran pel seu compte abans d'assistir a la classe.

Una vegada que els alumnes estan a l'aula, en contacte amb els seus companys i docents, inverteixen el seu temps en treballar amb les qüestions que no van poder resoldre per si mateixos en l'etapa anterior. El suport entre parells és essencial en aquest mètode.

Al utilitzar un model de Flipped Classroom ...
* Els estudiants aprenen contingut nou en línia veient conferències en vídeo, generalment a casa → activitats de comprensió i coneixement.
*  I el que solia ser Treball a casa (deures, problemes assignats) ara es fa a classe amb el professor que ofereix orientació i interacció més personalitzades amb els estudiants. → Aplicació - Anàlisi - Síntesi i Avaluació: activitats.

Comparació:

![](traditional.jpg)
![](flipped.jpg)

## Debat en grupo
En grups petits, els participants treballaran en la taxonomia de Bloom i pensaran en diverses activitats diferents que podrien desenvolupar-se a casa / a classe, d'acord amb el model Flipped Classroom. Després de 5 'de reflexió individual, el capacitador iniciarà una pluja d'idees i prendrà notes sobre la suggeriments dels participants.
Finalment, el formador mostrarà / distribuirà la taxonomia detallada de Bloom i explicarà com funciona:
![](taxonomy.png)

## Tothom obre l'ordinador i... 
Els participants (individualment o en grup) agafaran el document editable amb la taxonomia i afegiran una nova fila, per tal d'afegir les eines digitals, programari o altres recursos digitals que puguin ser útils per aconseguir les "accions" i els "resultats" (tercera fila de el document). El formador farà èmfasi en l'ús de FLOSS i tecnologies obertes i lliures.
Els participants també pensaran quina part de la taxonomia es pot desenvolupar "a casa" o "a classe" i tractaran de suggerir (en abstracte) algunes activitats que podrien realitzar-se durant una formació específica (fer servir un exemple concret pot ser molt útil)
Per exemple:
1. a casa: 
- Relacionats amb el coneixement (lectura integral, emplenament de qüestionaris, creació d'un glossari, etc.)
- Relacionat amb la comprensió: creació d'una infografia, un resum, una publicació, una presentació digital ...


2. A classe, amb companys i professors
- Relacionats amb l'aplicació del aprenentatge: una entrevista a un expert, un escenari virtual, un cronograma amb la seqüència d'un pla d'acció ...
- Relacionats amb l'anàlisi: mapa conceptual, quadre comparatiu ...
- Relacionats amb la Avaluació: discussió, debat, proves o experiments reals o simulats, etc.
- Relacionat amb la creació: vídeo, postcast ...


Després dels 45 ', els participants compartiran les seves troballes amb el grup i comentaran la seva experiència.

## Treball a casa (autònom)
Pensa en una formació que hagis impartit abans i les activitats que vas proposar als teus alumnes: si aplicaràs el model Flipped Classroom, quina part es desenvoluparia a casa i quina part es faria a classe?

## Referèncias: 

BLOOM, B.S. (ed.) (1956) Taxonomy of Educational Objectives: The Classification of Educational Goals. Nueva
York: David McKay Company, pag 201-207.

Sprouts (2015): The Flipped Classroom Model. Video available at https://youtu.be/qdKzSq_t8k8

Acer for education (2019): [6 benefits of the Flipped Classroom model](https://acerforeducation.acer.com/education-trends/6-benefits-of-the-flipped-classroom-model/?gclid=CjwKCAjwn9v7BRBqEiwAbq1Ey-QlosaH2KbYwmzqgRIODBYIjQNkNwZeqtGFepWzXz-0sIGSzT8VcxoC3tAQAvD_BwE). 
```