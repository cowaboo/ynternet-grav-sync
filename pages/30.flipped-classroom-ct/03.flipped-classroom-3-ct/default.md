---
title: 'Aprenentatge basat en Projectes'
objectives:
    - 'proporcionar una Introducció a l’Aprenentatge Basat en Projectes (PrBL)'
    - 'Facilitar el treball amb mètodes d''ensenyament / aprenentatge oberts'
    - 'Ddescobrir nous enfocaments del procés d’ensenyament / aprenentatge i estimular la motivació i el compromís dels participants. Descobrir les avantatges dels mètodes centrats en els estudiants'
    - 'Motivar els estudiants a canviar la seva manera ensenyar i enfrontar nous desafiaments mentre ensenyen'
lenght: '120 min'
---

## Introducció
El formador presentarà les principals característiques d'aquest enfocament: L'Aprenentatge Basat en Projectes és un mètode amb el qual l'alumne està en el centre de el procés d'aprenentatge, com a protagonista capaç de generar solucions en resposta a les diferents oportunitats.
Una metodologia molt relacionada amb l'entorn laboral, i també amb l'emprenedoria. Destaca especialment per instar els estudiants a posar en pràctica un ampli ventall de coneixements, habilitats, destreses i actituds.
Mitjançant l'ús d'una metodologia d'aprenentatge basat en projectes ...
- Necessitem aconseguir un producte (material o intel·lectual). Això és molt important.
- La cooperació i col·laboració entre els estudiants per aconseguir aquest objectiu és imprescindible.
- Promou la iniciativa, la proactivitat, la independència i la innovació en diferents àrees: professional, social i personal.
- El desafiament actua com a motor (motivació i determinació) per aconseguir l'objectiu. 'La compromís és la característica principal!

A l'utilitzar aquest enfocament, passa que: 
- Els estudiants generen valor més enllà del entorn del aula. A mesura que es llança la publicitat, els estudiants reben comentaris de el món real.
- La motivació augmenta amb l'efecte positiu en el seu context social.
- També es millora la seva autoestima.
- Treballen sobre situacions reals que són, o podrien ser, part del context professional.

Amb aquest mètode: Realment podem involucrar el món real. Podem tenir un impacte real en el nostre context social, i l'aprenentatge té un molt bon enfocament d'emprenedoria, no només des del punt de vista econòmic (empresarial), sinó també des d'una perspectiva social. Els estudiants es van convertir en una referència en l'àrea de contingut en què han estat treballant.

**Diferencies entre PBL y PrBL: **
- PBL comença amb un problema estructurat. PrBL comença construint un producte o un artefacte en les seves ments.
- Els problemes d'ABP poden no estar relacionats amb el context real del estudiant. PrBL sempre treballa amb oportunitats de la vida real, ja que el resultat ha de ser útil en el seu propi context.
- PBL utilitza preguntes i solucions. PrBL utilitza productes que es presentaran a la fi del procés d'aprenentatge.
- L'èxit en PBL s'aconsegueix trobant solucions a el problema. L'èxit en PrBL s'aconsegueix creant solucions a el problema i presentant-les a la comunitat.

Després de presentar la informació, el formador demanarà als participants que intercanviïn idees sobre algunes oportunitats que podrien presentar-se a les seves alumnes (per exemple, crear un diari local, crear una aplicació ...). El capacitador prendrà notes a la pissarra / paperògraf.
El formador pot fer una pregunta desafiant als participants: ¿quina part de el cas d'estudi de Technovation Girls és "PBL" i quina part és "PrBL"? És important que els estudiants aprenguin que el "mètode 100% pur" no sempre s'implementa i que poden prendre part dels mètodes i barrejar-los per obtenir resultats diferents. Els mètodes són sempre suggeriments, però el formador ha de ser flexible.

**Diferencias entre PBL y PrBL**: 
- PBL comienza con un problema estructurado. PrBL comienza construyendo un producto o un artefacto en sus mentes.
- Los problemas de ABP pueden no estar relacionados con el contexto real del estudiante. PrBL siempre trabaja con oportunidades de la vida real, ya que el resultado debe ser útil en su propio contexto.
- PBL utiliza preguntas y soluciones. PrBL utiliza productos que se presentarán al final del proceso de aprendizaje.
- El éxito en PBL se logra encontrando soluciones al problema. El éxito en PrBL se logra creando soluciones al problema y presentándolas a la comunidad.

Después de presentar la información, el formador pedirá a los participantes que intercambien ideas sobre algunas oportunidades que podrían presentarse a sus alumnos (por ejemplo, crear un periódico local, crear una aplicación…). El capacitador tomará notas en la pizarra / rotafolio.
El capacitador puede hacer una pregunta desafiante a los participantes: ¿qué parte del caso de estudio de Technovation Girls es “PBL” y qué parte es “PrBL”? Es importante que los estudiantes aprendan que el "método 100% puro" no siempre se implementa y que pueden tomar parte de los métodos y mezclarlos para obtener resultados diferentes. Los métodos son siempre sugerencias, pero el formador debe ser flexible.

## PrBL
El formador presentarà l'esquema PrBL i explicarà que es tracta de seguir el procés:

1. Detecció del oportunitat de treballar.
2. Organització dels equips de treball (diferents perfils, complementaris)
3. Definició final del desafiament, la solució a assolir.
4. Elaboració de el pla.
5. Recerca en formació i informació.
6. Anàlisi i síntesi. Els estudiants comparteixen el seu treball intercanviant idees, discutint solucions, fent suggeriments, etc.
7. Elaboració del producte aplicant tot el que s'ha après.
8. Presentació del producte o projecte.
9. Implementació de millores, si cal.
10. Avaluació i autoavaluació

## Tothom obre l'ordinador i... 
L'objectiu d'aquesta part de la sessió és que els participants construeixin una proposta concreta que es pugui presentar als estudiants. Els participants treballaran en grups i utilitzaran un ordinador per a crear una presentació digital amb la seva proposta.

- Pensaran en oportunitats que puguin ser adequades per treballar amb aquest mètode.
- Pensaran en com organitzar el grup
- Prepararan un pla preliminar
- Identificaran el coneixement que cal explorar (investigació) i suggeririran alguns mètodes diferents
- Pensaran en diversos tipus de productes que podrien crear-se a l'treballar amb els estudiants.
- Suggeriran algunes maneres de "presentacions" per utilitzar (presentació pública, gravació de vídeo, etc.)
- Pensaran en com avaluar i autoavaluar el treball.

Després dels 40' treballant en petits grups, els grups compartiran la seva proposta amb la classe. 

## Trabajo en casa (autónomo)
Després d'escoltar les diferents propostes, pensi en un projecte concret que puguin desenvolupar les seves alumnes a la seva classe. Penseu en els pros i els contra d'usar aquest mètode en la seva classe.

## Referèències
Buck Institute for Education PBLWorks website (2020): [What is PBL?](https://www.pblworks.org/what-is-pbl) 

Phyllis C. Blumenfeld, Elliot Soloway, Ronald W. Marx, Joseph S. Krajcik, Mark Guzdial & Annemarie Palincsar (1991) [Motivating Project-Based Learning](https://www.tandfonline.com/doi/abs/10.1080/00461520.1991.9653139): Sustaining the Doing, Supporting the Learning, Educational Psychologist, 26:3-4, 369-398, DOI: 10.1080/00461520.1991.9653139 
```