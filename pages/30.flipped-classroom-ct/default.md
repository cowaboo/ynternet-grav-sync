---
title: 'Aula invertida i aprenentatge basat en projectes i en problemes'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: ct
length: '9 hores'
objectives:
    - 'Comprendre el marc teòric i els factors clau dels 3 mètodes / estratègies: aula invertida, aprenentatge basat en problemes i aprenentatge basat en projectes'
    - 'Aprendre a dissenyar o redissenyar l''estratègia d''ensenyament-aprenentatge utilitzant eines i fonts obertes'
    - 'Crear i avaluar activitats basades en aquests mètodes'
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògrafs
    - Altaveus
skills:
    'Paraules clau':
        subs:
            'Aula invertida (FC)': null
            'Aprenentatge basat en problemes (PBL)': null
            'Aprenentatge basat en projectes (PrBL)': null
            Obert: null
            Mètode: null
            'Aprenentatge continu': null
            Voluntari: null
            Motivació: null
---

## Introducció
Aula invertida, aprenentatge basat en projectes, aprenentatge basat en problemes ... són diferents mètodes, tècniques o metodologies que tenen alguna cosa en comú: la posició en la qual els estudiants es col·loquen és proactiva, en la gestió del poder de la seva necessitat d'aprendre. Si els estudiants treballen en grups, com en els escenaris de la vida real, l'aprenentatge es torna més significatiu. Amb aquestes estratègies, el protagonisme es col·loca en les persones amb ganes d'aprendre i el paper de la persona que els forma és el d'una companya que té cert coneixement però que també vol aprendre amb els estudiants.
En resum, The Flipped Classroom (FC) o **Classe Invertida** és un model pedagògic que mou el treball de certs processos d'aprenentatge fora de l'aula i inverteix el temps de la sessió de classe, juntament amb l'experiència de la persona formadora, en promoure i impulsar altres processos d'adquisició i pràctica de coneixement dins de l'aula.

Amb l'**Aprenentatge Basat en Problemes** (ABP o, en anglès, PBL), la persona formadora formula una pregunta o un problema, i les persones que participen com a aprenents han de donar una resposta o trobar una manera de resoldre-la a través d'un procés; estableixen totes juntes una fita en el procés d'aprenentatge.

En **Aprenentatge Basat en Projectes** (PrBL, Project Based Learning), l'estudiantat també és al centre de l'procés d'aprenentatge, com a protagonista capaç de generar solucions en resposta a les diferents oportunitats; el procés està estretament relacionat amb l'entorn de treball i el resultat principal és un producte.
El mòdul es centrarà en aquestes estratègies, mostrant les idees més rellevants i els factors clau, i desafiarà a les persones participants a repensar les seves pràctiques pedagògiques.
## Contexto
L'objectiu d'aquest mòdul és comprendre el marc teòric i els factors clau dels 3 mètodes / estratègies: aula invertida, aprenentatge basat en problemes i aprenentatge basat en projectes.

Ens centrarem en aprendre a dissenyar o redissenyar l'estratègia d'ensenyament-aprenentatge utilitzant eines i fonts obertes, i a crear i avaluar les activitats basades en aquests mètodes.
Els resultats de l'aprenentatge seran l'adquisició de les capacitats per a:
* Aprendre els conceptes bàsics de l'estratègia Flipped Classroom (FC) i ser capaç d'implementar-i crear un mapa mental per explicar-ho als altres.
* Redactar l'estructura d'un nou mòdul d'aula invertida (o transformar un mòdul clàssic) per ensenyar / aprendre un tema, utilitzant eines i fonts obertes.
* Comprendre els conceptes bàsics de l'estratègia d'Aprenentatge Basat en Problemes i ser capaç d'explicar-ho als estudiants i col·legues.
* Dissenyar una activitat de PBL (o transformar una ja existent) que podria aplicar-se per ensenyar l'ús d'algunes eines de programari obert.
* Comprendre els conceptes bàsics de l'estratègia d'aprenentatge basat en projectes.
* Dissenyar una activitat de el Projecte BL (o transformar una de ja existent) que podria aplicar-se per ensenyar la importància de promoure FLOSS.
## Sessions
### Primera sessió: Es basarà en la comprensió de l'estratègia Flipped Classroom.
Revisarem els components, les etapes i les fases. Desenvolupament de recursos per a l'aula invertida amb tecnologies, eines i estratègies de FLOSS.
### Segona sessió: Es centrarà en el mètode d'Aprenentatge Basat en Problemes.
Etapes. Enfocament de problemes rellevants per als estudiants. Pluja d'idees de problemes per resoldre. Recursos de codi obert disponibles per a la resolució de problemes. Eines habituals.
### Tercera sessió: Objectiu, parlar sobre l'estratègia d'aprenentatge basat en projectes: components, etapes, fases
Treballant en situacions reals. El desafiament com a motor d'aprenentatge. Col·laboració i cooperació entre estudiants.
### Quarta sessió: Estarà totalment dedicada al projecte personal o grupal