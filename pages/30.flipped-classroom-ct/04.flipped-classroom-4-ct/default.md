---
title: 'Desenvolupament personal o grupal d’un projecte d’aprenentatge / ensenyament'
objectives:
    - 'L’objectiu d’aquesta sessió és facilitar als participants la creació del seu propi projecte, a partir d’alguns dels mètodes presentats a les sessions anteriors.'
lenght: '140 min'
---

## Introducció
El formador parlarà sobre les preguntes que es puguin plantejar després que la sessió anterior comentarà Treball a casa (autònom). Després s'introduirà el contingut i les activitats principals de la sessió.

El formador farà un resum de tot el contingut i els mètodes que s'han presentat i convidarà els participants a formar equips o triar treballar individualment en un pla de formació. Han de seleccionar un grup objectiu i un tema i després començar a dissenyar la seva pròpia capacitació.

Han de pensar i decidir sobre:
- Grup objectiu
- Metes / objectius / habilitats i competències
- Antecedents
- Calendari
- Mètode i estratègia principal.
- Introducció. a la formació
- Descripció de les principals activitats i seqüència de formació.

Es convida als participants a integrar els nous coneixements adquirits gràcies a la formació Open-AE.

## Tothom obre l'ordinador i... 

El capacitador/ora convidarà a les persones / grups a presentar el seu projecte. Després de la primera ronda de presentació, s'organitzarà una Avaluació entre iguals.
- Una de les possibilitats és fer-ho en un format petit: un grup es reuneix amb un altre grup (o un individu amb un altre individu) i comenta, de manera constructiva, fortaleses i debilitats de el projecte, recursos de programari o maquinari oberts que poden utilitzar i altres . detalls complementaris.
- L'altra possibilitat és fer-ho amb la classe grupal com una pluja d'idees. Qualsevol membre de la classe pot enviar preguntes a la persona o persones que han presentat el seu projecte, fer suggeriments i contribuir a la Avaluació entre iguals..

## Conclusiones
Aquesta és la part final d'aquest mòdul (i de la formació, si és possible)
Revisió del que s'ha après i dels compromisos individuals adquirits, si n'hi ha, i foto de grup final. Bona sort i fins aviat!

# Trabajo en casa (autónomo)
Segueix treballant en el teu projecte i fes-ho realitat en la teva classe. Valora el resultat basant-se el grau de satisfacció dels participants i els resultats acadèmics obtinguts. No oblidis que els resultats de les metodologies actives no s'avaluen amb els exàmens tradicionals: hauràs de treballar amb els portafolis dels alumnes i valorar en quina mesura són capaços d'aplicar els nous coneixements i habilitats adquirits.

# References

Yasemin Gülbahar & Hasan Tinmaz (2006) Implementing Project-Based Learning And E-Portfolio Assessment In an Undergraduate Course, Journal of Research on Technology in Education, 38:3, 309-327, DOI: 10.1080/15391523.2006.10782462

Brigid J.S. Barron, Daniel L. Schwartz, Nancy J. Vye, Allison Moore, Anthony Petrosino, Linda Zech & John D. Bransford (1998) Doing With Understanding: Lessons From Research on Problem- and Project-Based Learning, Journal of the Learning Sciences, 7:3-4, 271-311, DOI: 10.1080/10508406.1998.9672056 