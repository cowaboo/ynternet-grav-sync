---
title: 'Aprenentatge basat en problemes'
objectives:
    - 'Proporcionar una introducció a l’aprenentatge basat en problemes'
    - 'Facilitar el treball amb mètodes d''ensenyament / aprenentatge oberts'
    - 'Descobrir nous enfocaments del procés d’ensenyament / aprenentatge i estimular la motivació i el compromís dels participants. Descobrir les avantatges dels mètodes centrats en els estudiants'
    - 'Motivar els estudiants a canviar la seva manera d’ensenyar'
lenght: '120 min'
---

## Introducció
El formador o formadora parlarà sobre els dubtes que puguin sorgir després de la sessió anterior i comentarà el Treball a casa (autònom). A continuació es presentaran els continguts i les principals activitats de la sessió.

El capacitador presentarà el concepte i el mètode d ' "aprenentatge basat en problemes". El punt de partida serà una "revisió de cas" i suggerim utilitzar el projecte "Technovation Girls" (veure en https://technovationchallenge.org/), que és un projecte que desafia a equips de nenes a pensar en problemes locals ja codificar una solució (creen una aplicació). És millor trobar un exemple local, de manera que suggerim a l'capacitador que busqui exemples locals a YouTube buscant "Technovation + nom de país". Després de reproduir alguns dels vídeos disponibles, el capacitador ressaltarà alguns elements d'aquest (o similar) projecte:

- és un desafiament
- les participants treballen en grups
- treballen per resoldre un problema real, però no cal que el problema estigui present en el context dels estudiants (poden treballar en els problemes d'altres)
- aprenen el que necessiten per donar una solució (que no és el mateix per als diferents grups)

El formador o la formadora explicarà que avui en dia és molt fàcil identificar problemes reals, utilitzant els 17 objectius per transformar el nostre món:[the 17 goals to transform our world](https://www.un.org/sustainabledevelopment/sustainable-development-goals/):

![](sustainable.png)

**Problem based learning** ¿Aprenentatge basat en problemes? és un mètode que comença amb el formador / ora plantejant una pregunta o un problema i desafiant als estudiants a donar una resposta a la pregunta o trobar la manera de resoldre el problema. De vegades, el mestre convida els estudiants a pensar en el problema. L'ABP permet el professor i a l'alumne establir una fita en el procés d'aprenentatge, on l'alumne és l'actor principal.

Al utilitzar la metodologia d'aprenentatge basat en problemes, els estudiants ... 
- Aprenen a prendre decisions, individualment o negociant (amb el grup)
- Interactuen amb companys i també amb professors
- Utilitzen la informació del seu context proper
- Busquen la informació que necessiten
- Produeixen i comparteixen idees
- Discuteixen altres possibles solucions i també els mecanismes per trobar-les.


Els problemes han d'estar en relació amb les habilitats dels estudiants i han de ser significatius per a ells. Els problemes sorgeixen del món real i poden ser "reals" o "potencials". Convé utilitzar un escenaris reals per facilitar les coses.

- El problema ha de motivar els estudiants a cercar una comprensió més profunda dels conceptes.
- El problema hauria de requerir que els estudiants prenguin decisions raonades i les defensin.
- El problema ha d'incorporar els objectius del contingut de tal manera que es connecti amb cursos / coneixements previs.
- Si es fa servir per a un projecte grupal, el problema necessita un nivell de complexitat alt per garantir que els estudiants hagin de treballar junts per resoldre-ho.
- Si es fa servir per a un projecte de diverses etapes, els passos inicials de el problema han de ser oberts i atractius per atraure els estudiants a el problema.

(Duch, Groh, and Allen, 2001)

![](gold.png)

[Font de la imatge:](https://www.pblworks.org/blog/gold-standard-pbl-essential-project-design-elements)

Amb aquest mètode, els estudiants poden millorar / canviar els seus coneixements, habilitats i destreses i actituds.
En aquest moment, el capacitador demanarà als participants que intercanviïn idees sobre alguns problemes que podrien presentar-se als seus alumnes. El capacitador prendrà notes a la pissarra / paperògraf.

## L'esquema de treball PBL
El capacitador presentarà l'esquema PBL i explicarà que es tracta de seguir el procés: el primer pas és l'anàlisi de el problema, després els participants busquen la informació necessària que necessiten per comprendre el problema i aprenen les habilitats necessàries per a comprendre la informació. Després poden integrar la informació i aplicar-la amb certa quantitat de coneixement i saviesa per resoldre el problema.
És important comprendre que cada alumne necessita aprendre / adquirir habilitats diferents, ja que el seu coneixement previ és diferent.
Les etapes són:
1. Presentació del problema a resoldre / identificar un problema que li interessa a el grup.
2. Anàlisi dels coneixements previs
3. Identificació del coneixement i els recursos necessaris per desenvolupar una solució.
4. Llista d'accions o activitats que s'han de realitzar per trobar una solució.
5. Elaboració del pla de treball
6. Fer el que s'ha planejat: buscar la informació necessària, realitzar accions, comunicar els resultats parcials
7. Elaboració de la resposta i argumentació sobre el mètode utilitzat per trobar-la.


Com que la formadora ha presentat un exemple (Technovation girls one), l'exemple es transformarà en etapes, perquè els participants s'adonin del fàcil que és fer-ho. 

## Tothom obre l'ordinador i... 
L'objectiu d'aquesta part de la sessió és que els participants construeixin una proposta concreta que es pugui presentar als estudiants. Els participants treballaran en grups i utilitzaran un ordinador per a crear una presentació digital amb la seva proposta.

Ells i elles:
- Identificaran i presentaran un problema molt concret, relacionat amb els ODS, i identificaran un grup potencial d'estudiants (edat, antecedents, necessitats especials ...)
- S'identificaran alguns coneixements / habilitats que els seus estudiants haurien de tenir per resoldre el problema (llavors els estudiants podran identificar si tenen o necessiten aprendre / adquirir part d'aquests coneixements / habilitats).
- Prepararan una llista inicial d'activitats que s'han de realitzar. Per exemple: visitar un lloc, entrevistar algú, buscar a Internet, realitzar una enquesta, fer alguns càlculs amb dades obertes; programar una app o un videojoc, fer una presentació, fer una infografia ...
- Prepararan un pla de treball potencial (que serà modificat pels alumnes quan realitzen l'activitat)

Després dels 40 'treballant en petits grups, els grups compartiran la seva proposta amb la classe. 

## Treball a casa (autònom): 
Després d'escoltar les diferents propostes, pensa en un problema concret a resoldre amb els teus alumnes en la teva classe. Penseu en els pros i els contra d'usar aquest mètode en la seva classe.

## Referències
* Duch, B. J., Groh, S. E, & Allen, D. E. (Eds.). (2001). The power of problem-based learning. Sterling, VA: Stylus.
* Grasha, A. F. (1996). Teaching with style: A practical guide to enhancing learning by understanding teaching and learning styles. Pittsburgh: Alliance Publishers.
* The Center for Innovation in Teaching & Learning (CITL): [Problem-Based Learning (PBL)](https://citl.illinois.edu/citl-101/teaching-learning/resources/teaching-strategies/problem-based-learning-(pbl))
* [PBL through the Institute for Transforming Undergraduate Education at the University of Delaware](http://www1.udel.edu/inst/)
```