---
title: 'Living with CoP'
length: '180 min'
objectives:
    - 'Provide a critical analysis CoP cases'
    - 'Examine the use and advantages of collaborative management'
---

## Introduction
Group discussion: Introduction to the session (theme: business as commons).
Participants will brainstorm on different business management models around them. Answers will be documented and then reused to customize the training material.
## Business as commons
This section demonstrates that the future is in business as commons. In a world where business models are changing and even the traditional notion of work has lost its fit with current paradigms. 
1. Governance and organisation of Free/Libre Open Source
The first studies to explain how FLOSS communities organise themselves arrived from the field of software engineering, in the work of Raymond (2001) describing the two different development models of “The Cathedral” and “The Bazaar”. The concept of the bazaar model was subsequently further developed by Demil and Lecocq (2006), in the field of organisation and management studies. Demil and Lecocq (2006) proposed that a new generic governance structure was being seen: bazaar governance. They characterised this form of governance and discussed the strengths and weaknesses of the bazaar structure with respect to that of the market. They suggested that the way in which these communities are governed is distributed, allowing their members to participate, taking into account a diverse set of interests.
2. Flat organisations
A flat organization (also known as horizontal organization) has an organizational structure with few or no levels of middle management between staff and executives. An organization's structure refers to the nature of the distribution of the units and positions within it, also to the nature of the relationships among those units and positions. Tall and flat organizations differ based on how many levels of management are present in the organization, and how much control managers are endowed with.
3. More commons oriented management definitions:
* Collaborative production: Referring to CBPP as a mode of production by which a set of individuals produce something valuable which did not exist before their interaction.
* Peer-based: the interaction in CBPP is not solely or mainly coordinated by contractual relationships, nor is it coordinated in a hierarchical way. The tasks are based on free creation and self-assignation. The range of motivations is diverse and may be intrinsic (e.g. for fun) or extrinsic (e.g. in order to earn social capital), but they are not mainly based on contractual obligations nor forces of coercion.
* Commons-based: CBPP is characterised for being not only a peer production process, but also a commons process, which is driven by general interest. For example, in digital environments, this results in the openness of the common resources. 
Favouring reproducibility: characterised for favouring the reproducibility of the goods created, as well as the methodologies and the practices among others.

## Video
Watch the video on [Business as Commons](https://www.youtube.com/watch?v=1qkhWa9XoFo) (Samantha Slade).
Study, document and discuss its main arguments. Create a common document with all participants, share it with others.
## Homework
Search and document experts and/or articles on collaborative management. Post two links to [our collective awareness Diigo group](https://groups.diigo.com/group/e_culture).
## References
* [The future is in business as commons](https://www.youtube.com/watch?v=1qkhWa9XoFo) | Samantha Slade | TEDxGeneva.
* Bauwens, M. (2005). [The political economy of peer production](https://mmduvic.ca/index.php/ctheory/article/view/14464/5306). CTheory.
* Bauwens, M. & Kostakis, V. (2014). From the communism of capital to capital for the commons: towards an open co-operativism. tripleC: Communication, Capitalism & Critique. Open Access Journal for a Global Sustainable Information.
* [Society](http://triple-c.at/index.php/tripleC/article/view/561), 12(1), 356–361.