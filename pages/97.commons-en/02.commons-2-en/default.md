---
title: '(Digital) commons'
length: '180 min'
objectives:
    - 'Provide a definition of the Commons'
    - 'Analyse the impact of Decidim: free open-source participatory democracy for cities and organizations'
    - 'Discuss other digital commons initiatives such as Wikipedia, Drupal and others'
---

## Introduction
Group discussion on “the commons in your life today?”
Trainer will start the introduction to the module by asking participants about the everyday commons. Answers will be documented and then reused to customize the training material
## Defining the Commons
In this section we will define the Commons. Ostrom’s work shows how, under certain conditions, commons can indeed be managed in a sustainable way by local communities of peers. Her approach takes into account that individual agents do not operate in isolation, nor are they driven solely by self interest, i.e. beyond homo-economicus approaches. Instead, she argues that communities communicate to build common protocols and rules that ensure their sustainability. As part of this work, she identified a set of principles (Ostrom, 1990) for the successful management of these commons:
1. **Clearly defined community boundaries**: in order to define who has rights and privileges within the community.
2. **Congruence between rules and local conditions**: the rules that govern behaviour or commons use in a community should be flexible and based on local conditions that may change over time. These rules should be intimately associated with the commons, rather than relying on a “one-size-fits-all” regulation.
3. **Collective choice arrangements**: in order to best accomplish congruence (principle number 2), people who are affected by these rules should be able to participate in their modification, and the costs of alteration should be kept low.
4. **Monitoring**: some individuals within the community act as monitors of behaviour in accordance with the rules derived from collective choice arrangements, and they should be accountable to the rest of the community.
5. **Graduated sanctions**: community members actively monitor and sanction one another when behaviour is found to conflict with community rules. Sanctions against members who violate the rules are aligned with the perceived severity of the infraction.
6. **Conflict resolution mechanisms**: members of the community should have access to low-cost spaces to resolve conflicts.
7. **Local enforcement of local rules**: local jurisdiction to create and enforce rules should be recognised by higher authorities.
8. **Multiple layers of nested enterprises**: by forming multiple nested layers of organisation, communities can address issues that affect resource management differently at both broader and local levels.

Although these principles were originally defined for natural commons, they have also been discussed and adapted for the study of communities which develop and maintain digital commons, such as Wikipedia (Viégas et al., 2007; Forte et al., 2009) or Free/Libre Open Source Software such as Drupal (Rozas, 2017). This process of employing Ostrom’s principles in the study of how communities manage digital commons required, however, an analysis of how they may be re-interpreted within this different context.
## Description of the activity
Visit the Decidem community with the participants of the course and document the results in a common document.
## Homework
Post an article related to Decidim to the Diigo - eculture group
## References
* Arvidsson, A., Caliandro, A., Cossu, A., Deka, M., Gandini, A., Luise, V., & Anselmi,
G. (2017). [Commons based peer production in the information economy](https://www.academia.edu/29210209/Commons_Based_Peer_Production_in_the_Information_Economy) (Report).
* Ostrom, E. (1990). Governing the Commons: The Evolution of Institutions for Collective Action . Cambridge University Press. 
* Ostrom, E. (2000). Collective action and the evolution of social norms. Journal of economic perspectives , 14 (3), 137-158.
* Rozas, D. (2017). [Self-organisation in Commons-Based Peer Production: Drupal : "the drop is always moving"](http://epubs.surrey.ac.uk/845121/). (Doctoral dissertation, University of Surrey).