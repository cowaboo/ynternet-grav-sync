---
title: Pressupost
objectives:
    - 'Entendre la declaració que mostra els costos estimats del projecte per donar suport al projecte'
    - 'Un pressupost ha d''incloure tots els costos directes i indirectes necessaris per dur a terme el projecte'
    - 'Costos directes: despeses que s''associen específicament al producte o servei'
---

## Treball en grup
Trieu qualsevol producte que desitgeu i feu una llista de costos directes i indirectes que comporti la seva producció.

![](image13.png)

## Anàlisi cost-benefici
Abans d’assumir un nou projecte, realitzar una anàlisi cost-benefici ajudaria a avaluar tots els costos i ingressos potencials que es puguin generar si el projecte es finalitza. El resultat de l’anàlisi determinarà si el projecte és factible econòmicament o si s’ha de dur a terme un altre projecte. Es realitza una anàlisi cost-benefici en tres passos:

1. Recopila una llista completa de tots els costos i beneficis associats al projecte. 
2. Els costos han d’incloure els costos directes i indirectes, els intangibles, els costos d’oportunitat i el cost dels possibles riscos. 
3. Els beneficis han d'incloure tots els ingressos directes i indirectes i beneficis intangibles, i l'augment de les vendes dels clients.

Apliqueu una mesura monetària comuna a tots els ítems de la llista. Sigues realista; no subestimar els costos ni sobreestimar els beneficis. L’últim pas és comparar quantitativament els resultats dels costos i beneficis per determinar si els beneficis superen els costos. En cas afirmatiu, aleshores la decisió és tirar endavant el projecte. Si no és així, cal fer una revisió del projecte per veure si es poden fer ajustaments per augmentar els beneficis i / o disminuir els costos per fer el projecte viable. Si no, el projecte pot ser abandonat.

![](image14.png)

## Exemple treballat a Anàlisi de beneficis

![](image15.png)

Els 10 passos per desenvolupar un pressupost reeixit

1. Pla estratègic. Totes les organitzacions haurien de tenir un objectiu a complir. Això està escrit a la Declaració de visió i missió. Un pla estratègic és la forma en què l'organització planeja assolir la seva missió. El primer pas en el procés de pressupostació és tenir un pla estratègic escrit. D’aquesta manera es garanteix que els recursos organitzatius s’utilitzin per recolzar l’estratègia i el desenvolupament de l’organització.
2. Objectius empresarials. Per implementar el seu pla estratègic, una organització pren els objectius de negoci anuals i els desenvolupa per poder-los assolir. El pressupost proporciona els recursos financers per assolir aquests objectius. Exemple: una organització necessita ampliar les seves instal·lacions. Cal que hi hagi diners pressupostats perquè això passi.
3. Projeccions d’ingressos. Les projeccions d'ingressos (benefici) s'han de basar en el rendiment financer anterior, així com en els ingressos previstos per al creixement.
El creixement previst pot estar lligat a objectius organitzatius i iniciatives planificades que iniciïn el creixement empresarial. Exemple: Si hi ha un objectiu d’augmentar les vendes un 10%, aquestes projeccions de vendes haurien de formar part de les projeccions d’ingressos de l’any.
4. Projeccions de costos fixos. Projectar els costos fixos són costos mensuals previsibles que no canvien, com ara els costos de compensació dels empleats, les despeses de les instal·lacions, els serveis públics, els pagaments hipotecaris / lloguers, les assegurances, etc. Els costos fixos no canvien i són una despesa mínima que cal finançar en el pressupost.
5. Projeccions de cost variable. Els costos variables són costos que oscil·len de mes en mes, com ara els costos de subministrament, les hores extraordinàries, etc. Són despeses que s’han de pressupostar i controlar. Exemple: s’hauria de pressupostar un augment temporal de les hores extres per vendes de Nadal.
6. Objectiu anual de despeses anual. Els projectes relacionats amb objectius també haurien de rebre pressupostos i incorporar-los al pressupost anual. Les projeccions de costos s’han d’identificar, establir i incorporar al pressupost departamental encarregat de complir l’objectiu. Exemple: Si el departament de vendes té l’objectiu d’augmentar les vendes un 10%, s’haurien d’incorporar al pressupost els costos associats a l’increment de vendes (materials de màrqueting addicionals, viatges, entreteniment).
7. Marge de benefici objectiu. Els beneficis són importants per a totes les organitzacions i els marges de benefici saludables són un fort indicador de la fortalesa d’una organització. Totes les organitzacions, ja siguin sense ànim de lucre o sense ànim de lucre, haurien de tenir un marge de benefici orientat. Els marges de benefici permeten reinvertir a les instal·lacions i el desenvolupament de l’organització.
8. Aprovació del Consell La direcció de l'organització ha d'aprovar el pressupost i mantenir-se al dia amb el rendiment del pressupost. La direcció també ha de revisar els estats financers mensuals per les següents raons. Supervisar el rendiment del pressupost. Conèixer totes les despeses. Protegir l'organització contra la apropiació indeguda de fons o fraus.
9. Revisió del pressupost. Una comissió de revisió del pressupost s'hauria de reunir mensualment per supervisar el rendiment amb objectius. Aquest comitè hauria de revisar les diferències pressupostàries i avaluar els problemes relacionats amb les begudes pressupostàries. És important fer-ho mensualment perquè hi hagi una correcció a la despesa o a la modificació del pressupost, si cal.
Esperar fins a finals de l’exercici per fer correccions podria tenir un impacte negatiu en el resultat del pressupost final.
10. Fer front a les variacions pressupostàries. Les variacions pressupostàries han de revisar-se amb el responsable del departament responsable i s’haurien de plantejar preguntes sobre el que ha provocat la diferència. De vegades es presenten situacions imprevistes que no es poden evitar, per la qual cosa també és important disposar d’un fons d’emergència per ajudar a aquestes despeses no previstes. Exemple: Si un sistema disminueix de sobte i ha de ser substituït, aquesta seria una diferència pressupostària que cal finançar. Els bons processos de pressupost poden ajudar a que l’organització es desenvolupi, mentre que la mala pressupostació i el seguiment dels pressupostos poden blindar una organització i afectar la seva salut financera, la seva viabilitat i, eventualment, els clients.

## Definir el capital necessari per iniciar un negoci
Algunes definicions:
### Un balanç
És una declaració financera que dóna als inversors una idea sobre el que la companyia té i deu en un moment determinat. Es compon de tres segments que són:
-     l’actiu de l’empresa,
-     el passiu de l’empresa i
-     la quantitat invertida pels accionistes (patrimoni del propietari).

### Actius
Un actiu és un recurs amb valor econòmic que una empresa posseeix amb l'expectativa de que proporcionarà beneficis futurs. Els actius es registren en el balanç de l'empresa.
Es pot pensar en un actiu com una cosa que en el futur pot generar flux de caixa, reduir despeses, millorar les vendes i afegir valor a l'empresa.

### Passiu
El passiu és un deute o obligació financera de l’empresa que es deriven durant el desenvolupament de les seves operacions comercials.Es liquida amb el pas del temps mitjançant la transferència de beneficis econòmics incloent diners, béns o serveis.
El passiu inclou préstecs, hipoteques, etc. i es registra al balanç.

### Capital del propietari
Aquest és el valor d’un actiu menys l’import de tots els passius d’aquest actiu. Es pot representar amb l'equació comptable: Actius - Passiu = Capital. En finances, en general, podeu pensar en l'equitat com la propietat del grau de qualsevol actiu després de pagar tots els deutes associats a aquest actiu.
Exemple: Un cotxe o una casa que no té deute pendent es considera completament el patrimoni del propietari perquè pot vendre fàcilment l’article en efectiu i embolicar la suma resultant.

### Requisits de capital 
Aquesta és la suma de fons que necessita la vostra empresa per assolir els seus objectius.
- Quants diners necessiteu fins que el vostre negoci estigui en funcionament?
- Podeu calcular els requisits de capital afegint despeses fundacionals (taxes notarials, honoraris legals, taxes de registre, despeses de corredors immobiliaris, etc.), inversions i costos inicials.
- Restant el vostre capital patrimonial (patrimoni del propietari) dels requisits de capital, calculeu quant de capital extern necessitareu: Patrimoni net - Requeriments de capital = Calç capital extern

La planificació dels requisits de capital està estretament relacionada amb la resta de parts del vostre pla de negoci, perquè cal tenir en compte els seus costos de seguiment en la planificació.

## Consideracions
Cal tenir en compte les despeses d’inici. Per a la majoria de startups, els ingressos dels primers mesos no són suficients per cobrir el cost. Sol estar ocupat per adquirir clients i processar comandes abans que finalment pugui escriure les seves primeres factures i obtenir els seus pagaments. Encara haureu de poder compensar les despeses en aquests primers mesos difícils. El requisit de capital per a la fase d’inici equival al mínim d’efectiu i pèrdua d’efectiu mensual.

No oblideu considerar les despeses d’interès i les amortitzacions en el vostre requisit de capital. Podria ser necessari planificar requisits de capital més alts, de manera que podreu satisfer tant els vostres costos d’explotació com els vostres pagaments hipotecaris. Planifiqueu una reserva per a contingències, com ara comandes retardades, majors despeses de renovació o nous actius no planificats. Calculeu quina desviació produiria un escenari pitjor, tant per a la fase d’inversió com per a la fase d’inici.