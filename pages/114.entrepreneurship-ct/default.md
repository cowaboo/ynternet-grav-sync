---
title: 'Emprenedoria en línia amb eines FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: ct
length: '10 hours'
objectives:
    - 'Planificant el teu negoci'
    - 'Cicle de vida d''el projecte'
    - 'Introducció als pressupostos'
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - 'Nocions bàsiques de les competències FLOSS'
skills:
    'Paraules clau':
        subs:
            'Fer negocis en línia': null
            Emprenedoria: null
---

## Introducció
Les vendes en línia mai van ser tan bones com ara, i la predicció és que continuaran creixent fins al 2021, com mostren les estadístiques de l' 'Statista - The Statistics Portal'. Aquest tipus de comerç ha anat evolucionant any rere any, i aconseguint nous adeptes. Les botigues virtuals no són més que aparadors plens de productes disponibles per a la venda, i el mercat digital ha detectat la necessitat d'invertir en estratègies de màrqueting per conèixer què pensen els clients a l'respecte dels seus productes, i també per fer el procés postvenda.
Qualsevol negoci es pot posar en línia: des de les classes esportives fins a l'aprenentatge d'idiomes, la consultoria de negocis, els entrenadors personals, els coach, els serveis de càtering, els artesans ...
El comerç electrònic és una de les activitats digitals més populars en el món; només en 2016 les vendes minoristes de comerç electrònic van ascendir a 1,86 bilions de dòlars a tot el món. Per a l'any 2021, s'espera que les vendes en línia creixeran fins arribar als 4.48 bilions de dòlars. Això demostra quant poden beneficiar les persones emprenedores i els seus negocis de l'Era digital, especialment per escalar el seu negoci, sense importar en quina àrea.

El nombre total de compradors digitals a tot el món també està augmentant. Va créixer en més de 100 milions entre 2011 i 2012, i ha continuat creixent. La flexibilitat dels horaris per fer les compres permet un millor equilibri entre la vida laboral i personal i, com a conseqüència, les dones són en bona part responsable de l'augment de l'nombre persones compradores (realitzen una doble jornada: la laboral i la de la llar). El 2012, una enquesta de Greenfield va trobar que les dones representen el 58% de la despesa en línia en els EE. UU.

## Context
Aquest mòdul permetrà a la persona participant:
* Ensenyar habilitats per desenvolupar una idea de negoci basades en un cas real.
* Mostrar com s'utilitza l'anàlisi DAFO.
* Entendre el mercat i els diferents tipus de competència que enfronta una empresa.
* Mostrar el cicle de vida del producte.
* Ensenyar conceptes bàsics sobre màrqueting.
* Ensenyar conceptes bàsics sobre pressupostos.

Metodologies:
* Deliberació: grups de discussió i fòrums de discussió.
* Execució: elaboració de plans per part dels alumnes.

## Sessions
### Primera sessió: Desenvolupament i avaluació d'idees de negoci mitjançant mapes mentals Anàlisi DAFO
En aquesta sessió treballarem en l'anàlisi d'una idea de negoci utilitzant anàlisi DAFO i mapes mentals.
### Segona sessió: Definint el mercat
Aquesta sessió se centrarà en la segmentació de el client i en la comprensió de la competència.
### Tercera sessió: Pressupostos
Què és un pressupost i quins són els deu passos per a un bon seguiment?