---
title: 'Anàlisi DAFO'
objectives:
    - 'Com realitzar una anàlisi DAFO (en anglès, SWOT)'
    - 'Preguntes per fer durant una anàlisi DAFO'
    - 'Exemple d’anàlisi DAFO'
lenght: '120 min'
---

## Introducció
DAFO és un acrònim que es refereix a Fortaleses, Debilitats, Oportunitats i Amenaces. L'anàlisi DAFO és una llista organitzada dels punts forts, febleses, oportunitats i amenaces del vostre negoci. Els punts forts i els punts febles són interns de l’empresa (penseu: reputació, patents, ubicació). Podeu canviar-les amb el temps, però no sense feina. Les oportunitats i les amenaces són externes (penseu: proveïdors, competidors, preus): hi ha al mercat, passant tant si voleu com si no. No podeu canviar-les.

Les empreses existents poden utilitzar una anàlisi DAFO en qualsevol moment per avaluar un entorn canviant i respondre de manera proactiva. De fet, recomano realitzar una reunió de revisió d’estratègies com a mínim un cop a l’any que comenci amb una anàlisi DAFO. Les empreses noves han d'utilitzar una anàlisi DAFO com a part del seu procés de planificació. No hi ha cap pla de mida única per a la vostra empresa, i pensar en el vostre nou negoci en termes dels seus “DAFO” exclusius us posarà al camí correcte de seguida i us estalviarà de molts mals de cap més endavant.

## Com realitzar una anàlisi DAFO
Per obtenir resultats més complets i objectius, un grup de persones amb diverses perspectives i participacions a la vostra empresa es realitza amb una anàlisi DAFO. Gestió, vendes, atenció al client i, fins i tot, clients poden aportar informació vàlida. A més, el procés d’anàlisi DAFO és una oportunitat per unir el vostre equip i fomentar la seva participació i l’adhesió a l’estratègia resultant de la vostra empresa. 
L'anàlisi DAFO es realitza normalment mitjançant una plantilla d'anàlisi DAFO de quatre quadrats, però també podeu fer llistes per a cada categoria. Feu servir el mètode que us sigui més fàcil organitzar i comprendre els resultats.
Us recomano fer una sessió de pluja d’idees per identificar els factors de cadascuna de les quatre categories. Com a alternativa, podríeu demanar als membres de l’equip que completessin individualment la nostra plantilla d’anàlisi DAFO gratuïta i, després, es reunissin per discutir i recopilar els resultats. A mesura que treballeu cada categoria, no us preocupeu molt per elaborar-lo al principi. Els punts de bala poden ser la millor manera de començar. Només cal capturar els factors que creieu rellevants en cadascuna de les quatre àrees. Un cop acabat la pluja d’idees, crea una versió final i prioritzada de l’anàlisi DAFO, enumerant els factors de cada categoria en ordre de la màxima prioritat a la prioritat superior a la inferior.
### Preguntes per fer durant una anàlisi DAFO
A continuació he recopilat algunes preguntes per ajudar-vos a desenvolupar cada secció de l’anàlisi DAFO. Hi ha, certament, altres preguntes que podríeu fer; només han de començar.
### Fortaleses
Les fortaleses descriuen els atributs positius, tangibles i intangibles, interns de la vostra organització. Estan sota el vostre control.
* Què fas bé?
* Quins recursos interns teniu? Penseu en el següent:
        Atributs positius de persones, com ara coneixement, antecedents, educació, credencials, xarxa, reputació o habilitats.
        Actius materials de l'empresa, com ara capital, crèdit, clients existents o canals de distribució, patents o tecnologia.
* Quins avantatges teniu respecte a la vostra competició?
* Té una forta capacitat de recerca i desenvolupament? Instal·lacions per a la fabricació?
* Quins altres aspectes positius, interns per al vostre negoci, aporten valor afegit o us ofereixen un avantatge competitiu?

### Debilitats (factors interns, negatius)
Els punts febles són aspectes del vostre negoci que perjudiquen el valor que oferiu o us situen en un desavantatge competitiu. Heu de millorar aquestes àrees per competir amb el millor competidor.
* Quins factors, d'entre els que es troben sota el vostre control, perjudiquen la vostra capacitat d’obtenir o mantenir un avantatge competitiu?
* Quines àrees necessiten millorar per assolir els seus objectius o competir amb el competidor més fort?
* Què li manca al vostre negoci (per exemple, experiència o accés a habilitats o tecnologia)?
* La vostra empresa té recursos limitats?
* La vostra empresa està en una ubicació desfavorida?

### Oportunitats (factors externs, positius)
Les oportunitats són factors atractius externs que representen motius per als quals probablement prosperi el vostre negoci.
* Quines oportunitats existeixen al vostre mercat o l'entorn de què us podeu beneficiar? 
* La percepció del vostre negoci és positiva? 
* Hi ha hagut un creixement recent del mercat o hi ha hagut altres canvis en el mercat que generen una oportunitat? 
* L’oportunitat continua, o només hi ha una finestra? És a dir, quina importància té la vostra cronologia? 

### Amenaces (factors externs, negatius)
Les amenaces inclouen factors externs fora del vostre control que podrien posar en risc la vostra estratègia o el propi negoci. No teniu cap control sobre aquests, però podeu beneficiar-vos dels plans de contingència per fer-los front si hi haguessin lloc.
* Qui són els vostres competidors existents o possibles?
* Quins factors fora del vostre control poden posar en risc la vostra empresa?
* Hi ha reptes creats per una tendència o desenvolupament desfavorables que poden conduir a disminuir els ingressos o beneficis?
* Quines situacions poden amenaçar els vostres esforços de màrqueting?
* Hi ha hagut un canvi important dels preus dels proveïdors o de la disponibilitat de matèries primeres?
* Què passa amb els canvis en el comportament dels consumidors, l’economia o les regulacions governamentals que podrien reduir les vendes?
* S'ha introduït un nou producte o tecnologia que faci que els vostres productes, equips o serveis quedin obsolets?

## Exemples d’anàlisi DAFO
A tall d’il·lustració, aquí teniu un breu exemple DAFO d’un hipotètic magatzem d’ordinadors de mida mitjana als Estats Units (en anglès):

![](image1a.jpg)

Anàlisi TOWS: desenvolupament d’estratègies a partir de l’anàlisi DAFO. Un cop identificats i prioritzats els resultats DAFO, podeu utilitzar-los per desenvolupar estratègies a curt i llarg termini per al vostre negoci. Al cap i a la fi, el veritable valor d’aquest exercici és utilitzar els resultats per maximitzar les influències positives sobre el vostre negoci i minimitzar les negatives.

Però, com es converteixen els resultats DAFO en estratègies? Una manera de fer-ho és considerar com els punts forts, els punts febles, les oportunitats i les amenaces de la companyia es superposen entre ells. A vegades s'anomena anàlisi TOWS. Per exemple, mireu els punts forts que vau identificar i, a continuació, aneu amb maneres d’utilitzar aquests punts forts per maximitzar les oportunitats (es tracta d’estratègies d’oportunitat de força). A continuació, mireu com es poden utilitzar aquests mateixos punts forts per minimitzar les amenaces que vau identificar (es tracta d’estratègies d’amenaça de força).

Continuant aquest procés, utilitzeu les oportunitats que heu identificat per desenvolupar estratègies que minimitzin les febleses (estratègies d’oportunitat feble) o evitin les amenaces (estratègies d’amenaces debilitats).

La taula següent (en anglès) us pot ajudar a organitzar les estratègies de cada àrea:

![](image2a.jpg)

Un cop hàgiu desenvolupat les estratègies i incloses en el vostre pla estratègic, assegureu-vos de programar reunions de revisió periòdiques. Utilitzeu aquestes reunions per parlar de per què els resultats de les vostres estratègies són diferents del que havíeu planificat (perquè ho seran sempre) i decidiu què farà el vostre equip endavant.

## Resum DAFO
![](image3a.png)

## Treball a casa (autònom):
Proveu un exemple utilitzant un producte FLOSS com el Llibre Office

## Referències
[Smart Women Project](http://smartwomenproject.eu/).