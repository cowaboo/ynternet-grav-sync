---
title: 'Negoci i desenvolupament empresarial. Mapes mentals i DAFO '
objectives:
    - 'Definició de la vostra idea de negoci mitjançant mapes mentals'
    - 'Analitzar la idea mitjançant una anàlisi DAFO'
lenght: '120 min'
---

## Introducció
Qui sóm?
Presenteu-vos individualment responent breument aquestes quatre preguntes:
1. Quina és la teva idea de negoci?
2. Quin és el vostre producte?
3. A qui va dirigit el vostre producte? (Qui seran els vostres clients?)
4. On esteu ara mateix?

Example:
"La meva idea de negoci és començar a vendre productes casolans. M'agradaria especialitzar-me en melmelades sense sucre, dirigides a persones que segueixen dietes baixes en sucre / diabètics. Ara mateix estic preparant aquestes melmelades per als meus familiars i amics i, de vegades, com a regals. ”
Ara treballa en el desenvolupament d'aquestes idees fent servir una eina de pluja d'idees, el que us ajudarà a desenvolupar una àmplia gamma d’idees. Comenceu amb una paraula clau i anoteu tots els punts / idees que pugueu pensar. No critiqueu cap idea (si la feu en grup)

Example:

![](image1.png)

## Un enfocament potent per a la presa de notes
Mind Mapping (elaborar mapes mentals) és una tècnica útil que us ajuda a aprendre de manera més eficaç, millora la manera d’enregistrar informació i dóna suport i millora la resolució creativa de problemes. Amb l'ús de Mind Maps, podeu identificar i entendre ràpidament l'estructura d'un subjecte. Podeu veure la forma d’encaixar informació, a més de registrar els fets relatius a les notes normals. Encara més, Mind Maps us ajuda a recordar informació, ja que la mantenen en un format que resulta fàcil de recordar i ràpid de revisar.
Eina FLOSS recomanada: [CMapTools](cmap.ihmc.us/)
### Sobre els Mapes Mentals
Els mapes mentals van ser popularitzats per l’autor i consultor Tony Buzan. Utilitzen una estructura bidimensional, en lloc del format de llista utilitzat convencionalment per prendre notes. 
Els mapes mentals són més compactes que les notes convencionals, sovint ocupant un única cara del full. Ajuda a crear associacions fàcilment i a generar noves idees. Si trobeu més informació després d’haver dibuixat un mapa mental, aleshores podeu integrar-lo fàcilment amb poca interrupció. Més informació a: [New Ideas - Strategies and Techniques](https://www.mindtools.com/pages/article/newCT_88.htm)

A més, això de Mind Mapping  ajuda a desglossar grans projectes o temes en fragments manejables, de manera que pugueu planificar de manera eficaç sense deixar-vos aclaparat i sense oblidar alguna cosa important.
Un bon mapa mental mostra la "essència" del subjecte, la importància relativa dels punts individuals i la forma en què els fets es relacionen entre ells. Això vol dir que són molt ràpids a revisar, ja que sovint podeu refrescar la informació a la vostra ment només amb una ullada. D’aquesta manera, poden ser eficaços mnemònics: recordar la forma i l’estructura d’un Mind Map pot proporcionar-vos les indicacions que calgui per recordar la informació que hi ha.
Quan es crea amb colors, imatges i dibuixos, un mapa mental pot semblar una obra d'art.

Els mapes d’ànim són útils per a:
* Pluja d’idees: individualment i en grup.
* Resum d'informació i presa de notes.
* Consolidar informació de diferents fonts de recerca.
* Pensar mitjançant problemes complexos.
* Presentació d'informació en un format que mostri l'estructura general del tema.
* Estudiar i memoritzar informació.

### Dibuix bàsic de mapes mentals
Per dibuixar un mapa mental, seguiu aquests passos:
1. Escriu el títol del tema que explores al centre de la pàgina i dibuixa un cercle al seu voltant, tal i com es mostra al cercle marcat a la figura 1, a continuació. (El nostre exemple, senzill, mostra les accions necessàries per fer una pluja d’idees per fer una presentació exitosa.) 

![](image2.jpg)**Figure 1**

2. A mesura que trobeu grans subdivisions o subtítols del tema (o fets importants relacionats amb el tema) marqueu línies d’aquest cercle. Etiqueta aquestes línies amb aquestes subdivisions o subtítols. (Vegeu la figura 2, a continuació). 

![](image3.jpg)**Figure 2**

3. A mesura que t'introdueixis en el tema i es descobreixi un altre nivell d'informació (altres subtítols o fets individuals) pertanyents als sub-apartats, dibuixa-les com a línies enllaçades amb les línies de subtítols. Aquests es mostren a la figura 3. 

![](image4.jpg)**Figure 3**

4. A continuació, per a fets o idees individuals, marca les línies de la línia d’encapçalament i etiqueu-les. Aquests es mostren a la figura 4. 

![](image5.jpg)**Figure 4**

5. A mesura que vagis trobant informació nova, enllaça-la al mapa mental. 

Un mapa mental complet pot tenir les línies temàtiques principals que irradien en totes les direccions del centre. Títols i fets es separaran d’aquests, com les branques i les branques del tronc d’un arbre. No us haureu de preocupar de l'estructura que produïu, ja que evolucionarà segons avanceu.

### Utilitzar mapes mentals de forma eficaç
Un cop heu entès com prendre notes en format Mind Map, podeu desenvolupar les vostres convencions. Els suggeriments següents us poden ajudar a dibuixar mapes d’ànim impactants:
* Utilitzeu paraules simples o frases simples, per tal d'assegurar que la informació es transmet en el context correcte i en un format agradable per llegir.
A Maps Mind, les paraules simples i les frases curtes i significatives poden transmetre el mateix significat amb més potència. Les paraules en excés només desordenen el mapa mental.
* Utilitzeu el color per separar idees diferents: us ajudarà a separar les idees quan sigui necessari. També us ajuda a visualitzar el mapa mental per recordar-lo. El color pot ajudar a mostrar l’organització del tema. 
* Utilitzeu Símbols i Imatges: les imatges us poden ajudar a recordar la informació amb més eficàcia que les paraules. 
* Utilitzar enllaços creuats: la informació d'una part d'un mapa mental pot relacionar-se amb una altra part. Aquí podeu dibuixar línies per mostrar els vincles creuats. Això us ajuda a veure com afecta una altra part del tema. 

## Treball a casa
Exemple: Analitzem el tema de "salut"

![](image6.png)

![](image7.png)