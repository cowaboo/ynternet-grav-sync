---
title: 'Definició del mercat '
objectives:
    - 'Segmentació geogràfica'
    - 'Segmentació de clients'
    - Competition
    - 'Maturity of the market'
---

## Segmentació  geogràfica
Què és la segmentació geogràfica? La segmentació geogràfica és quan una empresa divideix el seu mercat en funció de la ubicació. Hi ha diverses maneres de segmentar geogràficament un mercat. Podeu dividir el vostre mercat per zones geogràfiques, com ara per ciutat, comtat, estat, regió, país o regió internacional. També podeu dividir el mercat en segments de mercat rural, suburbà i urbà. I, podeu segmentar un mercat per clima o població total de cada àrea. Avantatges de la segmentació geogràfica.

Quins avantatges té la segmentació geogràfica? Mirem alguns.
* És un enfocament eficaç per a empreses amb grans mercats nacionals o internacionals, perquè diferents consumidors de diferents regions tenen necessitats, necessitats i característiques culturals diferents que poden ser específicament orientades.
* També pot ser un enfocament eficaç per a petites empreses amb pressupostos limitats. Poden centrar-se en la seva àrea definida i no gastar dòlars de màrqueting innecessaris en enfocaments poc adequats per al seu segment geogràfic objectiu.
* Funciona bé en diferents àrees de densitat de població. Els consumidors d’un entorn urbà sovint tenen necessitats i desitjos diferents que els que es troben en entorns rurals i suburbans. Fins i tot hi ha diferències culturals entre aquestes tres àrees.
* És relativament fàcil desglossar el vostre mercat en segments geogràfics.

## Segmentació de clients
Segmentar un mercat és una bona pràctica. Et permet desenvolupar un coneixement més profund dels teus clients i descobrir què els fa marcar. Quan esteu comunicant un missatge, serà més efectiu si el destinatari del missatge el trobe rellevant. La segmentació és simplement una manera d’organitzar els vostres clients en grups més reduïts segons el tipus. Aquests diferents subgrups o segments han de caracteritzar-se per atributs particulars. Ara podeu orientar missatges de màrqueting específics i rellevants a cada grup. I no es tracta només del que dius. Com comunicar també és vital i la segmentació requereix sovint una combinació de màrqueting curosament estructurada. Això és degut a que alguns clients poden preferir l'enfocament directe, com ara el màrqueting telefònic, mentre que d'altres responen millor a una campanya publicitària local.

Primers passos per a la segmentació de clients. 
La segmentació no ha de ser complexa. Per a una petita empresa, es podria tractar de reconèixer que teniu dos o tres tipus de client diferents amb necessitats diferents. La meva filosofia és començar sempre amb la simple pregunta: amb qui volem parlar? La resposta pot ser senzilla: clients. Els principis de segmentació poden afegir diverses capes d'intel·ligència basades en diferencials clau, com ara:
* patrons de despesa 
* gènere 
* on viuen 
* edat 
* grup socioeconòmic 

El que és important no són les diferències de superfície, sinó les diferències que afecten realment el comportament de compra. Què desencadena cada persona a comprar? Si dirigiu un saló de perruqueria, per exemple, el tipus d’ofertes que pugueu fer als grups de clients diferirien segons el sexe i la línia d’edat. Si teniu un negoci de comandes de correu electrònic, us permetrà analitzar els patrons de compra i dividir els clients en grups segons la quantitat que gasten, la freqüència que comprin o quins productes interessen més.

Si augmenteu la comprensió del que compren els vostres clients, també podeu maximitzar les oportunitats de venda creuada o venda més gran. Em recorda el comerciant constructor que ven una tona de maons però no es ven a la venda a través de la sorra i el ciment. Agrupant tots els clients que compren regularment determinats productes, els podeu orientar amb ofertes rellevants que els animin a augmentar la seva despesa.

No només un missatge de màrqueting rellevant és més eficaç com a eina de venda, sinó que també es tracta d’un bon servei d’atenció al client. Un missatge de comunicació que reconeix el que heu comprat i quan té un impacte molt més gran que un missatge de mida única. És més, si sou un client habitual, un missatge orientat mostra que esteu apreciats i valorats. Per contra, un missatge general que no reconeix la costum anterior, podria fer que et sentis desil·lusionat i donat per descomptat.

## Competició
Els quatre tipus d'estructures de mercat.
Hi ha algunes estructures de mercat diferents que poden caracteritzar una economia. Tanmateix, si acabeu de començar amb aquest tema, potser voldreu consultar primer els quatre tipus bàsics d’estructures de mercat. És a dir, competència perfecta, competència monopolista, oligopoli i monopoli. Cadascuna d’elles té el seu propi conjunt de característiques i suposicions, que al seu torn afecten la presa de decisions de les empreses i els beneficis que poden obtenir.

És important tenir en compte que no totes aquestes estructures de mercat existeixen en realitat, algunes d’elles són només construccions teòriques. No obstant això, tenen una importància crítica, perquè poden il·lustrar aspectes rellevants de la presa de decisions de les empreses de competència. Per tant, us ajudaran a comprendre els principis econòmics subjacents. Dit això, vegem-los amb més detall.
### Competència perfecta
La competència perfecta descriu una estructura de mercat, on un gran nombre de petites empreses competeixen entre elles. En aquest escenari, una sola empresa no té cap poder de mercat significatiu. Com a resultat, la indústria en general produeix un nivell de producció socialment òptim, ja que cap de les empreses té la capacitat d’influir en els preus del mercat.

La idea de competència perfecta es basa en diversos supòsits: (1) totes les empreses maximitzen els beneficis (2) hi ha entrada i sortida gratuïta al mercat, (3) totes les empreses venen mercaderies completament idèntiques (és a dir, homogènies), (4) allà no hi ha preferències del consumidor. Si ens fixem en aquests supòsits, es fa obvi que gairebé mai no trobarem competència perfecta en la realitat. Aquest és un aspecte important, perquè és l’única estructura del mercat que pot (teòricament) donar lloc a un nivell de producció socialment òptim. Probablement el millor exemple de mercat amb competència gairebé perfecta que podem trobar en realitat és el mercat de valors. Si voleu obtenir més informació sobre la competència perfecta, també podeu consultar la nostra publicació sobre la competència perfecta i la competència imperfecta.
### Competició monopolística
La competència monopolística també fa referència a una estructura de mercat, on un gran nombre de petites empreses competeixen entre elles. Tanmateix, a diferència de la competència perfecta, les empreses en competència monopolística venen productes similars, però lleugerament diferenciats. Això els proporciona un cert grau de poder del mercat que els permet carregar preus més alts dins d’un determinat interval.

La competència monopolista es basa en els següents supòsits: (1) totes les empreses maximitzen els beneficis (2) hi ha entrada i sortida gratuïta al mercat, (3) les empreses venen productes diferenciats (4) els consumidors poden preferir un producte que l’altre. Ara, aquestes hipòtesis estan una mica més a prop de la realitat que les que miràvem en perfecta competència. Tanmateix, aquesta estructura de mercat deixarà de resultar en un nivell de producció socialment òptim, perquè les empreses tenen més poder i poden influir fins a cert punt en els preus del mercat. Un exemple de competència monopolística és el mercat dels cereals. Hi ha un gran nombre de marques diferents (per exemple, Cap'n Crunch, Lucky Charms, Froot Loops, Apple Jacks). La majoria probablement tenen gust lleugerament diferent, però al final del dia, tots són cereals per esmorzar.
### Oligopoli
Un oligopoli descriu una estructura de mercat que només domina un petit nombre d’empreses. Això resulta en un estat de competència limitada. Les empreses poden competir entre elles o col·laborar. En fer-ho, poden utilitzar el seu poder de mercat col·lectiu per augmentar els preus i obtenir més beneficis.

L’estructura del mercat oligopolista es basa en els següents supòsits: (1) totes les empreses maximitzen els beneficis, (2) els oligopolis poden fixar preus, (3) hi ha barreres d’entrada i sortida al mercat, (4) els productes poden ser homogenis o diferenciats, i (5) només hi ha algunes empreses que dominen el mercat. Malauradament, no es defineix clarament què vol dir exactament unes poques empreses. Com a regla general, diem que un oligopoli consisteix normalment en unes 3-5 empreses dominants.

Per posar un exemple d’oligopoli, mirem el mercat de les consoles de jocs. Aquest mercat està dominat per tres empreses potents: Microsoft, Sony i Nintendo. Això deixa a tots ells una quantitat important de potència del mercat.
### Monopoli
Un monopoli fa referència a una estructura de mercat on una sola empresa controla tot el mercat. En aquest cas, la signatura té el nivell més alt de potència del mercat, ja que els consumidors no tenen alternatives. Com a resultat, els monopilistes sovint redueixen la producció per augmentar els preus i obtenir més beneficis.

Els següents supòsits es fan quan parlem de monopolis: (1) el monopolista maximitza els beneficis, (2) pot fixar el preu, (3) hi ha grans barreres d’entrada i sortida, (4) només hi ha una empresa que domina. tot el mercat.

Des de la perspectiva de la societat, la majoria dels monopolis no solen ser desitjables, perquè donen com a resultat uns rendiments més baixos i uns preus més alts en comparació amb els mercats competitius. Per tant, sovint són regulats pel govern. Un exemple de monopoli de la vida real podria ser Monsanto. Aquesta empresa té la marca comercial al voltant del 80% del total de blat de moro procedent als EUA. Això proporciona a Monsanto un nivell de poder extremadament alt. Podeu trobar informació addicional sobre els monopolis a la nostra publicació sobre el poder del monopoli.

## Maduresa del mercat
Després de les fases d’introducció i creixement, un producte passa a la fase de maduresa. La tercera de les etapes del cicle de vida del producte pot ser un moment força difícil per als fabricants. En les dues primeres etapes, les empreses intenten establir un mercat i, a continuació, augmenten les vendes del seu producte per aconseguir una part tan gran com sigui possible. No obstant això, durant l’etapa de maduresa, l’objectiu principal de la majoria de les empreses serà mantenir la seva quota de mercat davant diversos reptes diferents.
![](image9.jpg)
### Reptes de l’etapa de maduresa
* Volums comercials màxims: Després de l’increment constant de les vendes durant l’etapa de creixement, el mercat comença a saturar-se ja que hi ha menys clients nous. La majoria dels consumidors que sempre van a comprar el producte ja ho han fet.
* Disminució de la quota de mercat: Una altra característica de l'etapa de maduresa és el gran volum de fabricants que competeixen per una part del mercat. Amb aquesta etapa del cicle de vida del producte, que sovint presenta els nivells més alts de competència, és cada vegada més difícil per a les empreses mantenir la seva quota de mercat. 
* Els beneficis comencen a disminuir: Si bé aquesta etapa es pot produir quan el mercat en general obté el màxim benefici, sovint és la part del cicle de vida del producte on molts fabricants poden començar a disminuir els seus beneficis. Els beneficis hauran de repartir-se entre tots els competidors del mercat i és probable que un fabricant que perdi quota de mercat i tingui una caiguda de les vendes probablement produeixi una caiguda posterior dels beneficis. Aquesta disminució dels beneficis es pot agreujar amb la caiguda dels preus que sovint es veu quan el nombre reduït de competidors obliga alguns d’ells a intentar atraure més clients competint en el seu preu. 

### Beneficis de l’etapa de maduresa
* Reducció continuada de costos: de la mateixa manera que les economies d’escala en l’etapa de creixement van contribuir a reduir costos, els desenvolupaments de la producció poden conduir a maneres més eficients de fabricar grans volums d’un determinat producte, ajudant a reduir encara més els costos. 
* Major participació de mercat mitjançant la diferenciació: Si bé el mercat pot arribar a la saturació durant l’etapa de maduresa, els fabricants podrien augmentar la seva quota de mercat i augmentar els beneficis d’altres maneres. Mitjançant l'ús de campanyes de màrqueting innovadores i oferint funcions de producte més diverses, les empreses poden millorar la seva quota de mercat mitjançant la diferenciació i hi ha molts exemples de cicles de vida del producte que poden aconseguir-ho. 

## Gestió del cicle de vida del producte en fase maduresa
L’etapa de maduresa del cicle de vida del producte presenta als fabricants una àmplia gamma de reptes. Si les vendes arriben al seu punt àlgid i el mercat es satura, pot ser molt difícil que les empreses mantinguin els seus beneficis, i molt menys, intentin augmentar-les, sobretot davant la competència generalment força intensa. Durant aquesta etapa, es tracta d’organitzacions que busquen maneres innovadores de fer més atractiu el seu producte per al consumidor que mantindrà, i potser fins i tot augmentarà, la seva quota de mercat.