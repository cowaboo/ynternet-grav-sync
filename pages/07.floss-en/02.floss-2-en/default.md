---
title: 'FLOSS is everywhere'
length: '180 minutes'
objectives:
    - 'Provide a state of the art around FLOSS'
    - 'Analyse FLOSS initiatives in EU countries'
    - 'Discover the FLOSS the policy framework in EU'
    - 'Motivate educated policy contribution in the FLOSS area'
---

The second session will aim at setting the state of the art around FLOSS. FLOSS practices in EU countries will be presented and compared. Participants will engage in a critical analysis of the policy framework of FLOSS in EU and design an initial policy for their own organisation.
## Introduction
Trainer will start the introduction to the module by asking participants about the FLOSS initiatives around them - mostly coming from other organisations or the EU. Answers will be documented and then reused to customize the training material.
## FLOSS in EU and elsewhere
FLOSS culture will be linked to Commons oriented examples under the following areas:
* Agriculture
* Manufacturing
* Medicine and health
* Housing construction
* Circular economy
* Urban development
* Water management
* Crypto-programming
* Disaster response
* Knowledge
* Communication
* IT Infrastructure

## An EU policy framework for FLOSS
Presentation and analysis of the main EU FLOSS policy initiatives including:
* [Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#softwarestrategy).
* [Open data policy](http://data.europa.eu/euodp/en/data/).
* [Open standards](https://ec.europa.eu/digital-single-market/en/open-standards).
* [The European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12) (the ‘EUPL’).

## Your policy framework for FLOSS
Join a group to design and share your policy proposal for FLOSS both nationally and EU level. What should be people doing?
## Homework
Wikify your policy framework. Use a platform of your choice to share and discuss your policy ideas on FLOSS.
## References
* [FLOSS as Commons](http://www.bollier.org/floss-roadmap-2010-0). David Bollier, Floss Road Map, 2010.