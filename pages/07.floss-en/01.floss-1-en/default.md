---
title: 'The FLOSS fundamentals: motivations, ideology and practice'
length: '180 minutes'
objectives:
    - 'Map and explore the main concepts behind FLOSS culture'
    - 'Understand the interdependence of FLOSS and Open Educational Resources (OERs)'
    - 'Link them to the formation of Open Educational Resources (OERs)'
    - 'Discover the ethical, legal, social, economic, and impact arguments for and against FLOSS'
    - 'Decide which platforms/tools/services are most useful for themselves and their community'
---

This session will give the opportunity to its participants to map and explore the main concepts behind FLOSS culture. The different goals within the FLOSS movement will be also examined. During the session, diverse understandings of these concepts, goals and results will be demonstrated. We will link them to the formation of Open Educational Resources (OERs).
## Introduction
Trainer will start the introduction to the module by asking participants about their current and previous experiences to FLOSS culture. Answers will be documented and then reused to customize the training material.
## The FLOSS culture
FLOSS will be reviewed as a conceptual phenomenon based on two basic elements: community and modularity. These elements are set in a parallel mode allowing to dynamically place initiatives and move them according to a third element: need to change to a commons oriented production.
A coherent overview of FLOSS  as socio-economic continuum linked to: ideology, technology and the need for a profound change of production to commons oriented system will be explored.
## FLOSS arguments
In this activity, we will revisit the documentation results of the introductory session in order to find and develop arguments (for and against) using FLOSS in our everyday lives.
## Homework
Participants should listen to the [Techno-capitalism's moral and intellectual calamities](http://radioopensource.org/tech-master-disaster-part-one) podcast and post a comment in the [comments section](http://radioopensource.org/tech-master-disaster-part-one/#comments) 
If participants find any other resource or material, they should add it to the group in [Diigo](https://www.diigo.com/).

## References
* A collection of related resources is available on a [collective-social bookmarking space](https://groups.diigo.com/group/e_culture).
* Benkler, Y. (2006).The Wealth of Networks: How Social Production TransformsMarkets and Freedom. Yale University Press.
* Salus, Peter H. (March 28, 2005). "A History of Free and Open Source". Groklaw. Retrieved 2015-06-22.