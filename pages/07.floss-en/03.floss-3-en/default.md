---
title: 'FLOSS as collective learning, FLOSS in collective learning'
length: '180 minutes'
objectives:
    - 'Provide a critical analysis of the importance of FLOSS and OERs in the field of non-formal training. It will continue by while'
    - 'Examine the use and advantages of open digital technologies in education'
    - 'Foster learners active and creative engagement through FLOSS technologies both as a theory and practice'
    - 'Motivate participants to adopt FLOSS in their education activities'
---

This session will allow for a critical analysis of the importance of FLOSS and OERs in the field of non-formal training. It will continue by examining the use and advantages of open digital technologies in education, while foster learners' active and creative engagement through FLOSS technologies both as a theory and practice.
## Introduction
Participants will brainstorm on  collective intelligence initiatives around them. Answers will be documented and then reused to customize the training material.
## FLOSS culture and collective intelligence
The gaps between conventional wisdom about the organization of knowledge production and the empirical reality of collective intelligence produced in “peer production” projects like Wikipedia have motivated research on fundamental social scientific questions of organization and motivation of collec-tive action, cooperation, and distributed knowledge production. 
Wikipedia, for example, demonstrates how participants in many collective intelligence systems contribute valuable resources without the hierarchical bureaucracies or strong leadership structures common to state agencies or firms and in the absence of clear financial incentives or rewards.
## FLOSS and Open Education
FLOSS has walked hand in hand with a certain idea of education. An organic view of teaching and learning—and an even more organic view on how ideas spread. The process is not like an engineer building a structure according to specifications; it’s more like a farmer or gardener tending to plants, creating an environment in which the plants will flourish.
## Collective scenarios for OEP - Open Education scenarios
The activity will focus on an exemplary selection of applications of the paradigm of open educational practices.
## Homework
Search in the [Zenodo open repository](https://zenodo.org/) and find your three favourite papers on FLOSS and education. Document and/or post a comment on them.
## References
* [Unglue](https://unglue.it/) (v. t.) To pay an author or publisher for publishing a Creative Commons ebook.
* [DIS](https://dis.art/welcome) The future of learning is much more important than the future of education. DIS is a streaming platform for entertainment and education — edutainment. We enlist leading artists and thinkers to expand the reach of key conversations bubbling up through contemporary art, culture, activism, philosophy, and technology. Every video proposes something, a solution, a question, a way to think about our shifting reality.