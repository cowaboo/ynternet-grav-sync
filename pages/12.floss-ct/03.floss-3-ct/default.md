---
title: 'FLOSS com a aprenentatge col·lectiu, FLOSS en aprenentatge col·lectiu'
length: '180 minutes'
objectives:
    - 'Proporcionar una anàlisi crítica de la importància de FLOSS i OER en el camp de la formació no formal'
    - 'Examinar l’ús i els avantatges de les tecnologies digitals obertes en l’educació'
    - 'Fomentar el compromís actiu i creatiu dels estudiants mitjançant les tecnologies FLOSS tant com a teoria i pràctica'
    - 'Motivar els participants a adoptar FLOSS en les seves activitats d’educació'
---

Aquesta sessió permetrà fer una anàlisi crítica de la importància de FLOSS i OER en el camp de la formació no formal. Es continuarà examinant l’ús i els avantatges de les tecnologies digitals obertes en l’educació, alhora que es fomenta el compromís actiu i creatiu dels aprenentatges mitjançant tecnologies FLOSS tant com a teoria com a pràctica.
## Introducció
Els participants treballaran en les iniciatives d’intel·ligència col·lectiva al seu voltant. Les respostes es documentaran i es reutilitzaran per personalitzar el material d’entrenament
## Cultura FLOSS i intel·ligència col·lectiva
Els buits entre la saviesa convencional sobre l’organització de la producció de coneixement i la realitat empírica de la intel·ligència col·lectiva produïda en projectes de “producció entre iguals” com Wikipedia han motivat la investigació sobre qüestions científiques socials fonamentals d’organització i motivació d’acció col·lectiva, cooperació i coneixement distribuït. producció.
Viquipèdia, per exemple, demostra com els participants en molts sistemes d’intel·ligència col·lectius aporten recursos valuosos sense les burocràcies jeràrquiques ni estructures de lideratge sòlides comunes a agències o empreses estatals i a falta d’incentius o recompenses financeres clares.
## FLOSS i educació oberta
FLOSS ha caminat de la mà amb una certa idea d’educació. Una visió orgànica de l’ensenyament i l’aprenentatge, i una visió encara més orgànica sobre com es difonen les idees. El procés no és com un enginyer que construeixi una estructura segons les especificacions; és més com un agricultor o jardiner que tendeix a les plantes, creant un entorn en què floriran les plantes.
## Escenaris col·lectius per a OEP: escenaris d’educació oberta (treball en grup)
L’activitat se centrarà en una selecció exemplar d’aplicacions del paradigma de pràctiques educatives obertes.
## Treball a casa
Busqueu al dipòsit obert [Zenodo open repository](https://zenodo.org/) i trobeu els vostres tres articles preferits sobre FLOSS i educació. Documentar i / o publicar-ne un comentari.
## Referències
* [Unglue](https://unglue.it/) (v. t.)  Pagar un autor o un editor per publicar un llibre electrònic de Creative Commons.
* [DIS](https://dis.art/welcome) El futur de l’aprenentatge és molt més important que el futur de l’educació. DIS és una plataforma de reproducció per a entreteniment i educació. Ens allistem a artistes i pensadors líders per ampliar l’abast de les converses claus que s’arrosseguen a través de l’art contemporani, la cultura, l’activisme, la filosofia i la tecnologia. Cada vídeo proposa alguna cosa - una solució, una pregunta - una manera de pensar la nostra realitat canviant