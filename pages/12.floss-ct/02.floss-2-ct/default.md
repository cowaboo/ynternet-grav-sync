---
title: 'FLOSS és a tot arreu'
length: '180 minutes'
objectives:
    - 'Proporcionar un estat de l’art al voltant de FLOSS'
    - 'Analitzar les iniciatives FLOSS als països de la UE'
    - 'Conèixer el marc de la política FLOSS a la UE'
    - 'Motivar l’aportació de polítiques educades a l’àrea FLOSS'
---

La segona sessió tindrà com a objectiu establir l'estat de l'art al voltant de FLOSS. Es presentaran i es compararan les pràctiques FLOSS als països de la UE. Els participants participaran en una anàlisi crítica del marc de la política de FLOSS a la UE i dissenyaran una política inicial per a la seva pròpia organització.
## Introduction
El formador iniciarà la introducció al mòdul preguntant als participants sobre les iniciatives FLOSS al seu voltant, procedents majoritàriament d’altres organitzacions o de la UE. Les respostes es documentaran i es reutilitzaran per personalitzar el material formatiu.
## FLOSS a la UE i en altres llocs
La cultura FLOSS estarà vinculada a exemples orientats a Commons sota les àrees següents:
* agricultura
* fabricació
* medicina i salut
* construcció d’habitatges
* economia circular
* desenvolupament urbà
* gestió de l’aigua
* programació crypto
* resposta de desastres
* coneixement
* comunicació
* infraestructura informàtica

## El marc de la política de la UE per a FLOSS
Presentació i anàlisi de les principals iniciatives de política de la UE FLOSS que inclouen:
* [Open source software strategy](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#softwarestrategy).
* [Open data policy](http://data.europa.eu/euodp/en/data/).
* [Open standards](https://ec.europa.eu/digital-single-market/en/open-standards).
* [The European Union Public Licence](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12) (the ‘EUPL’).

## El marc de la vostra política per a FLOSS 
Uniu-vos a un grup per dissenyar i compartir la vostra proposta de política per a FLOSS tant a nivell nacional com a nivell comunitari. Què ha de fer la gent?
## Treball a casa
_Wikifiqueu_ el marc de la vostra política. Utilitzeu una plataforma que escolliu per compartir i debatre les vostres idees de política a FLOSS.
## Referències
* [FLOSS as Commons](http://www.bollier.org/floss-roadmap-2010-0). David Bollier, Floss Road Map, 2010.