---
title: 'Els fonaments fonamentals del FLOSS: motivacions, ideologia i pràctica'
length: '180 minutes'
objectives:
    - 'fer un mapa i explorar els principals conceptes de la cultura FLOSS.'
    - 'Entendre la interdependència de FLOSS i de Recursos Educatius Oberts (OER)'
    - 'vincular-los a la formació de recursos educatius oberts (OER)'
    - 'descobrir els arguments ètics, legals, socials, econòmics i d’impacte a favor i en contra de FLOSS.'
    - 'decidir quines plataformes / eines / serveis són més útils per a ells i la seva comunitat'
---

Aquesta sessió donarà l'oportunitat als seus participants de mapar i explorar els principals conceptes que hi ha darrere de la cultura FLOSS. També s’examinaran els diferents objectius del moviment FLOSS. Durant la sessió, es demostraran comprensions diverses d’aquests conceptes, objectius i resultats. Els enllaçarem a la formació de Recursos Educatius Oberts (OER).
## Introducció
El formador iniciarà la introducció al mòdul preguntant als participants sobre les seves experiències actuals i anteriors a la cultura FLOSS. Les respostes es documentaran i es reutilitzaran per personalitzar el material d’entrenament.
## La cultura FLOSS
FLOSS es revisarà com un fenomen conceptual basat en dos elements bàsics: la comunitat i la modularitat. Aquests elements estan configurats en un mode paral·lel que permet situar dinàmicament les iniciatives i traslladar-les segons un tercer element: necessitat de canviar a una producció orientada a un gran format.
S’explorarà una visió general coherent del FLOSS com a continu socioeconòmic vinculat a: ideologia, tecnologia i la necessitat d’un canvi profund de producció cap a un sistema orientat a comunes.
## Arguments FLOSS (treball en grup)
En aquesta activitat, revisarem els resultats de la documentació de la sessió introductòria per tal de trobar i desenvolupar arguments (a favor i en contra) amb FLOSS en la nostra vida quotidiana.
## Treball a casa
Els participants han d’escoltar el podcast de calamitats morals i intel·lectuals del Technocapitalisme i publicar un comentari a [la secció de comentaris](http://radioopensource.org/tech-master-disaster-part-one/#comments) 
Si els participants troben qualsevol altre recurs o material, haurien d’afegir-lo al grup de [Diigo](https://www.diigo.com/).

## Referències
* Una col·lecció de recursos relacionats està disponible en un espai col·lectiu: [marcadors](https://groups.diigo.com/group/e_culture).
* Benkler, Y. (2006). La riquesa de les xarxes: la transformació de la producció social en els mercats i la llibertat. Yale University Press.
* Salus, Peter H. (28 de març del 2005). "Una història de font lliure i lliure". Groklaw. Recuperat el 22/06 2015.