---
title: 'FLOSS comme apprentissage collectif, FLOSS dans l''apprentissage collectif'
length: '180 minutes'
objectives:
    - 'Fournir une analyse critique de l''importance des FLOSS et des REL dans le domaine de la formation non formelle. Cela se poursuivra d''ici peu'
    - 'Examiner l''utilisation et les avantages des technologies numériques ouvertes dans l''éducation'
    - 'Favoriser l''engagement actif et créatif des apprenants grâce aux technologies FLOSS à la fois en tant que théorie et pratique'
    - 'Motiver les participants à adopter FLOSS dans leurs activités éducatives'
---

## Introduction
Les participants réfléchiront sur les initiatives d'intelligence collective qui les entourent. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## Culture FLOSS et intelligence collective
Les écarts entre la sagesse conventionnelle sur l'organisation de la production de connaissances et la réalité empirique de l'intelligence collective produite dans des projets de «production par les pairs» comme Wikipédia ont motivé la recherche sur les questions fondamentales de sciences sociales de l'organisation et la motivation de l'action collective, de la coopération et des connaissances distribuées production.
Wikipedia, par exemple, montre comment les participants à de nombreux systèmes d'intelligence collective contribuent à des ressources précieuses sans les bureaucraties hiérarchiques ou les structures de leadership solides communes aux agences ou entreprises d'État et en l'absence d'incitations financières ou de récompenses claires.
## FLOSS et Open Education
FLOSS a marché main dans la main avec une certaine idée de l'éducation. Une vision organique de l'enseignement et de l'apprentissage - et une vision encore plus organique de la façon dont les idées se répandent. Le processus n'est pas comme un ingénieur construisant une structure selon les spécifications; c'est plus comme un fermier ou un jardinier qui s'occupe des plantes, créant un environnement dans lequel les plantes s'épanouiront.
## Scénarios collectifs pour OEP - Scénarios d'éducation ouverte
L'activité se concentrera sur une sélection exemplaire d'applications du paradigme des pratiques éducatives ouvertes.
## Devoirs
Recherchez dans le référentiel ouvert [Zenodo](https://zenodo.org/) et trouvez vos trois articles préférés sur FLOSS et l'éducation. Documentez et / ou postez un commentaire à leur sujet.
## Références
* [Unglue](https://unglue.it/) (v. t.) Pour payer un auteur ou un éditeur pour la publication d'un livre numérique Creative Commons.
* [DIS](https://dis.art/welcome) L'avenir de l'apprentissage est beaucoup plus important que l'avenir de l'éducation. DIS est une plateforme de streaming pour le divertissement et l'éducation - ludo-éducatif. Nous recrutons des artistes et des penseurs de premier plan pour élargir la portée des conversations clés qui bouillonnent à travers l'art contemporain, la culture, l'activisme, la philosophie et la technologie. Chaque vidéo propose quelque chose - une solution, une question - une façon de penser à notre réalité changeante.