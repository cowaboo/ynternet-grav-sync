---
title: 'FLOSS est partout'
length: '180 minutes'
objectives:
    - 'Fournir un état de l''art autour de FLOSS'
    - 'Analyser les initiatives FLOSS dans les pays de l''UE'
    - 'Découvrir le FLOSS le cadre politique de l''UE'
    - 'Motiver une contribution politique éduquée dans le domaine FLOSS'
---

## Introduction
Le formateur commencera l'introduction du module en interrogeant les participants sur les initiatives FLOSS qui les entourent - provenant principalement d'autres organisations ou de l'UE. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## FLOSS dans UE et ailleurs
La culture FLOSS sera liée à des exemples orientés Commons dans les domaines suivants :
* Agriculture.
* Fabrication.
* Médecine et la santé.
* Construction de logements.
* Économie circulaire.
* Développement urbain.
* Gestion de l'eau.
* Crypto-programmation.
* Réponse désastreuse.
* Connaissance.
* Communication.
* Infrastructure informatique.


## Cadre politique de l'UE pour FLOSS
Présentation et analyse des principales initiatives politiques du FLOSS de l'UE, notamment :
* [Stratégie logicielle open source](https://ec.europa.eu/info/departments/informatics/open-source-software-strategy_en#softwarestrategy).
* [Politique d'ouverture des données](http://data.europa.eu/euodp/en/data/).
* [Normes ouvertes](https://ec.europa.eu/digital-single-market/en/open-standards).
* [La licence publique de l'Union européenne («EUPL»)](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12).

## Votre cadre politique pour FLOSS
Rejoignez un groupe pour concevoir et partager votre proposition de politique pour FLOSS au niveau national et européen. Que devraient faire les gens?
## Devoirs
Wikify votre cadre politique. Utilisez une plateforme de votre choix pour partager et discuter de vos idées politiques sur FLOSS.
## Références
* [FLOSS as Commons](http://www.bollier.org/floss-roadmap-2010-0). David Bollier, Floss Road Map, 2010.