---
title: 'Les fondamentaux de FLOSS'
length: '180 minutes'
objectives:
    - 'Cartographier et explorer les principaux concepts de la culture FLOSS'
    - 'Comprendre l''interdépendance du FLOSS et des ressources éducatives libres (REL)'
    - 'Relier la culture FLOSS à la formation des ressources éducatives libres (REL)'
    - 'Découvrir les arguments éthiques, juridiques, sociaux, économiques et d''impact pour et contre FLOSS'
    - 'Décider quelles plateformes / outils / services sont les plus utiles pour eux-mêmes et leur communauté'
---

## Introduction
Le formateur commencera l'introduction du module en interrogeant les participants sur leurs expériences actuelles et antérieures sur la culture FLOSS. Les réponses seront documentées puis réutilisées pour personnaliser le matériel de formation.
## La culture FLOSS
FLOSS sera examiné comme un phénomène conceptuel basé sur deux éléments de base: la communauté et la modularité. Ces éléments sont placés dans un mode parallèle permettant de placer dynamiquement des initiatives et de les déplacer selon un troisième élément: nécessité de passer à une production orientée vers les communs.

Un aperçu cohérent du FLOSS en tant que continuum socio-économique lié à: l'idéologie, la technologie et la nécessité d'un changement profond de la production vers un système orienté vers les communs sera exploré.
## Arguments FLOSS
Dans cette activité, nous reviendrons sur les résultats de la documentation de la session d'introduction afin de trouver et développer des arguments (pour et contre) l'utilisation de FLOSS dans notre vie quotidienne.
## Devoirs
Les participants doivent écouter [Techno-capitalism's moral and intellectual calamities](http://radioopensource.org/tech-master-disaster-part-one) et poster un commentaire dans la [section des commentaires](http://radioopensource.org/tech-master-disaster-part-one/#comments).
Si les participants trouvent une autre ressource ou matériel, ils doivent l'ajouter au groupe de [Diigo](https://www.diigo.com/).
## Références
* Une collection de ressources connexes est disponible sur un [espace de bookmarking collectif - social](https://groups.diigo.com/group/e_culture).
* Benkler, Y. (2006).The Wealth of Networks: How Social Production TransformsMarkets and Freedom. Yale University Press.
* Salus, Peter H. (March 28, 2005). "A History of Free and Open Source". Groklaw. Retrieved 2015-06-22.