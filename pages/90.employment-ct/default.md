---
title: 'Recursos de FLOSS per a l''ocupabilitat'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'FLOSS resources for employment'
lang_id: 'FLOSS resources for employment'
lang_title: ct
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògraf
    - Altaveus
skills:
    'Paraules clau':
        subs:
            'Algorismes i Ocupabilitat': null
            'Curriculum Vitae': null
            Reclutament: null
            Habilitats: null
            Competències: null
            'Oportunitats digitals': null
            Xarxes: null
            'Emprenedoria digital': null
            'Cultura de la donació': null
            Wikinomics: null
            'Campanyes en línia': null
            'Recaptació de fons': null
            Crowdsourcing: null
            'Col·laboració massiva': null
objectives:
    - 'Presentar el marc de les habilitats de l''FLOSS per a l''ocupació'
    - 'Presentar el marc de funcionament de la gestió dels Recursos Humans'
    - 'Reconèixer els beneficis i les limitacions dels algoritmes en la gestió de recursos humans'
    - 'Entendre els algoritmes oberts com un nou paradigma'
    - 'Comprendre l''ús d''algoritmes en el procés de reclutament de recursos humans'
    - 'Comprendre com adaptar un CV per tenir una millor acceptació per part dels algoritmes'
    - 'Aprendre com crear un bon CV en línia, un portafoli digital o un perfil de xarxes socials'
    - 'Aprendre a crear i actualitzar un bon perfil laboral digital'
    - 'Comprendre com d''important és ser part d''una comunitat digital per aprofitar noves oportunitats econòmiques'
    - 'Comprendre com està canviant el mercat i com les oportunitats digitals poden facilitar l''esperit empresarial i la marca personal'
    - 'Aprendre noves formes de ser una persona emprenedora amb les pràctiques i eines de FLOSS'
    - 'Saber com organitzar una campanya de crowdfunding en línia'
    - 'Comprendre com funcionen diferents models econòmics com la cultura de la donació i la wikinomia, entre d''altres'
    - 'Millorar la comunicació personal, les habilitats de col·laboració i de marca'
length: '15 hores'
---

## Introducció
El 75 per cent de les sol·licituds d'ocupació són rebutjades abans de que siguin vistes pels ulls humans. Abans que el teu currículum arribi a les mans d'una persona real, sovint s'ha d'aprovar amb el que es coneix com un sistema de seguiment de candidats. Un sistema de seguiment de sol·licitants, o ATS, per abreujar, és un tipus de programari utilitzat per reclutadors i ocupadors durant el procés de contractació per recopilar, classificar, escanejar i classificar les sol·licituds d'ocupació que reben en relació als llocs vacants.

Elegir la persona adequada per a un lloc de treball pot ser un desafiament. Els algoritmes són, en part, les nostres opinions integrades en forma de codi d'ordinador. Els algoritmes consideren el simple fet que la contractació és essencialment un problema de predicció. Quan un gerent llegeix els currículums vitae dels sol·licitants d'ocupació, està tractant de predir quines de les persones sol·licitants tindran un bon acompliment en el treball i quines no. Els algoritmes estadístics estan dissenyats per predir problemes / costos i poden ser útils per millorar la presa de decisions humanes. Els algoritmes també poden tenir un costat més fosc, ja que reflecteixen prejudicis i prejudicis humans que condueixen a errors d'aprenentatge automàtic i males interpretacions (perpetuant i reforçant la discriminació en les pràctiques de contractació).

El que resulta més rellevant ara no és si usar o no algoritmes per seleccionar candidats, sinó com aprofitar a l'màxim les màquines. Els principis fonamentals de la redacció de currículums romanen constants, i així ha estat durant generacions, però les tecnologies en evolució mostren que, ara més que mai, molts aspectes de la sol·licitud i dels processos de contractació es realitzen de forma digital. Al mantenir-se a el dia en relació a les pràctiques actuals, les persones participants estaran millor preparades per postular-se com a candidates.

A internet, les possibilitats de generació d'ingressos són infinites. Necessitem identificar un nínxol, desenvolupar un pla de negocis i treballar dur per tenir èxit. Existeixen nombroses empreses, amb diferents models de negoci, que una persona emprenedora pot desenvolupar a Internet; per altra banda, cada organització i cada projecte -presencial o digital- necessita fonts d'ingressos. En aquest mòdul també ens centrarem en la cultura de les donacions, Wikinomics, en la recaptació de fons i en altres models econòmics nascuts al cor del web 2.0.
## Context
L'objectiu de la sessió és demostrar i motivar l'alumnat a explorar:
* Presentar el marc de les habilitats de l'FLOSS per a l'ocupació.
* Presentar el marc de funcionament de la gestió dels Recursos Humans.
* Reconèixer els beneficis i les limitacions dels algoritmes en la gestió de recursos humans.
* Entendre els algoritmes oberts com un nou paradigma.
* Comprendre l'ús d'algoritmes en el procés de reclutament de recursos humans.
* Comprendre com adaptar un CV per tenir una millor acceptació per part dels algoritmes.
* Aprendre com crear un bon CV en línia, un portafoli digital o un perfil de xarxes socials.
* Aprendre a crear i actualitzar un bon perfil laboral digital.
* Comprendre com d'important és ser part d'una comunitat digital per aprofitar noves oportunitats econòmiques.
* Comprendre com està canviant el mercat i com les oportunitats digitals poden facilitar l'esperit empresarial i la marca personal.
* Aprendre noves formes de ser una persona emprenedora amb les pràctiques i eines de FLOSS.
* Saber com organitzar una campanya de crowdfunding en línia.
* Comprendre com funcionen diferents models econòmics com la cultura de la donació i la wikinomia, entre d'altres.
* Millorar la comunicació personal, les habilitats de col·laboració i de marca.

## Sessions
### Primera sessió: Algorismes i ocupabilitat
L'objectiu de la sessió és presentar els conceptes d'algoritmes i ocupabilitat i com adaptar-se a el nou paradigma que es produeix en la selecció dels RR. Aquesta sessió generarà debats sobre el problema que plantegen els algoritmes que s'apliquen a l'àmbit dels recursos humans i també sobre les solucions que aporten. L'objectiu principal d'aquesta sessió és plantejar a les persones participants la idea que els algoritmes de reclutament seguiran creixent i que necessitem adaptar-nos millor a ells per a tenir èxit.
### Segona sessió: Com utilitzar les pràctiques de FLOSS i crear un currículum vitae atractiu per als algoritmes
L'enfocament d'aquesta sessió serà com crear un currículum en línia. Les participants seran convidades a crear el seu propi CV utilitzant plataformes i eines digitals. A més, les participants descobriran com presentar les seves competències i habilitats al crear un bon CV digital que sigui atractiu tant per a humans com per a màquines.
### Tercera sessió: Oportunitats de FLOSS i emprenedoria en línia
En aquesta sessió, les participants tindran l'oportunitat de descobrir moltes altres oportunitats que el món digital crea per a desenvolupar una carrera i diferents idees de projectes. L'objectiu principal d'aquesta sessió serà observar com l'emprenedoria en línia i el món digital ofereixen oportunitats per a recaptar diners. S'exploraran diferents models econòmics com el crowdfunding, la cultura de donacions i el model Freemium.