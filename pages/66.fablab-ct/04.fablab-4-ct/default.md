---
title: 'Necessitats i anàlisi del context real'
objectives:
    - 'Proporcionar una introducció a explorar el potencial de l’ús de maquinari i programari de codi obert en el marc d’un Fablab.'
    - 'Descobrir les tecnologies adequades per iniciar una iniciativa FabLab.'
    - 'Facilitar una visió general del maquinari i programari oberts i una visió general de les màquines relacionades amb la fabricació digital.'
    - 'Descobrir l''ús de diferents tecnologies en diferents tipus d’activitats.'
    - 'Motivar l''ús de tecnologies obertes i obrir i promoure les activitats d’un espai de creació maker dins la nostra entitat.'
lenght: '90 min'
---

## Introducció
El o la formadora parlarà sobre les preguntes que puguin sorgir després de la sessió anterior i comentarà Treball a casa.
Després d’això, el o la docent introduirà el contingut i les activitats principals de la sessió. 	 
## Primera part
L’objectiu d’aquesta secció és garantir que els estudiants entenguin que hi ha centenars de tecnologies obertes que poden ser útils en un FabLab i identificar diferents tècniques i materials de fabricació digital.

El formador/ora presentarà dues pàgines web i s’assegurarà que els participants s’hi posin i gaudeixin fent una ullada a les característiques d’alguns dels productes / tecnologies. 

* Llista de màquines FabLab: [List of FabLab machines](https://www.fablabs.io/machines): Llista de màquines, kits i tipus de tecnologies i màquines que s’utilitzaran en un espai Fab Lab. 
* Diferents materials [Different Materials](https://www.shapeways.com/materials)
* Algunes biblioteques de recursos que podríeu utilitzar, perquè algú els va crear i compartir a Internet:
    * [Gravity sketch](https://www.gravitysketch.com/)
    * [Vectary](https://www.vectary.com/)
    * [Tinkercad](https://www.tinkercad.com/)
    * [Thingiverse](https://www.thingiverse.com/)
    * [Beetle Blocks](http://beetleblocks.com/)
    * [Sahpe Ways](https://www.shapeways.com)

A partir d'una plantilla els estudiants elaboraran la seva pròpia llista de tecnologies preferides seguint aquest esquema:

* Nom de la tecnologia.
* Materials i fonts associades a aquesta tecnologia.
* Com aquesta tecnologia pot ser útil.
* Un possible projecte que es podria desenvolupar utilitzant aquestes fonts.

## Segona part
L’objectiu d’aquesta secció és garantir que els estudiants comencin a pensar en un petit projecte o activitat que es pugui desenvolupar mitjançant algunes tecnologies concretes i obertes relacionades amb la fabricació digital. Consell: la formadora o el formador han d'assegurar-se que el projecte és "petit"
El formador ajudarà els estudiants organitzant una pluja d’idees en grup, mitjançant alguns exemples anteriors i responent a les següents preguntes:
* Repte / projecte a desenvolupar
* Per què: quin problema / oportunitat tindrà.
* Qui s’hauria d’implicar? (perfils).
* Amb quina eina / font / màquina / material.

Després de la pluja d’idees, el formador o la formadora facilitarà el treball en grups reduïts, on els estudiants són convidats a pensar en aquestes idees i seleccionar-ne una per desenvolupar-la.
Al final de la sessió, els participants presentaran la seva idea concreta al grup general i faran / rebran alguns comentaris sobre les seves propostes. El formador o la formadora farà algunes observacions generals sobre les idees: si tenim alguna informació reflectida a cada columna, aleshores tenim un gran punt de partida d’una iniciativa de FabLab.
## Treball a casa (autònom)
Continuar planificant un mini projecte o activitat on s’haurien d’utilitzar diferents màquines.

Ara tenim algunes idees i algun punt de partida. Al Treball a casa de la sessió anterior vam pensar en la gestió dels voluntaris, el retorn social de ser voluntari i la metodologia d’Aprenentatge i Servei. Com podem aplicar aquests conceptes sobre el projecte que hem treballat durant la sessió d’avui?