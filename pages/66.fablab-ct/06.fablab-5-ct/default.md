---
title: 'Disseny col·laboratiu d''un nou FabLab comunitari'
objectives:
    - 'Proporcionar una introducció al procés de dissenyar col·laborativament un nou Fablab comunitari.'
    - 'Facilitar protocols, metodologies i guies per detectar i identificar agents digitals d’innovació social.'
    - 'Descobrir dinàmiques del treball comunitari.'
    - 'Motivar a obrir i promoure les activitats i un espai de creació maker a la pròpia entitat.'
lenght: '90 min'
---

## Introducció
El o la formadora parlarà sobre les preguntes que puguin sorgir després de la sessió anterior i comentarà Treball a casa.
Després d’això, el o la docent introduirà el contingut i les activitats principals de la sessió. 	 
## Primera part: introducció a la complexitat
L’objectiu d’aquesta secció és introduir diversos conceptes clau a tenir en compte a l’hora de treballar en la creació d'un FabLab comunitari o maker Space. 

Visualitzem aquest vídeo com a introducció: [How to Create a Makerspace](https://youtu.be/BEUDgfsy-8k). Presenta diversos conceptes clau: des d’equipaments, recursos, qüestions legals, fins a implicar la comunitat.
A continuació, els participants (treball en grups, amb la mateixa configuració que a la darrera sessió) elaboraran una llista de conceptes clau que afecten els seus projectes grupals. Després de treballar ells mateixos, la informació es compartirà amb el gran grup. Evidentment, també es compartirà la idea principal del projecte / iniciativa, de manera que el grup podrà avaluar i completar la informació fàcilment.
## Segona part: fem-ho, “de debò”
Ara és hora de somiar amb construir el concepte d’un espai on poder desenvolupar “tots” els projectes que els participants han introduït. Ho farem en grup i, per fer-ho més fàcil, pensarem que tenim un patrocinador públic que proporcionarà "prou diners" per al primer any.

El formador demanarà als participants que pensin en aquests projectes (com si algú demanés un espai i recursos per treballar-hi) i que comencin a planificar-los. Utilitzarem un mapa mental per descriure el primer escenari de planificació. Consell: hauríem de tractar de ser realistes.

- Quants projectes podem iniciar el primer any? Com podem prioritzar-los, amb quins criteris?

- Quantes persones haurien d’estar implicades des del primer moment? Quins perfils?

- Quines tecnologies s’han d’aplicar? Quants d’ells es poden compartir entre els projectes que iniciarem el primer any?

- Qui ha de ser conscient de les nostres idees?

- Qui més ens podría patrocinar en aquesta iniciativa?

- Com ens assegurem que la comunitat aprofiti la nostra proposta?

## Treball a casa (treball autònom)
Reflexioneu sobre el que hem fet durant la sessió.