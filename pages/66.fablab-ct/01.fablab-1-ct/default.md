---
title: 'Què és un Fablab'
objectives:
    - 'Proporcionar una introducció al que és un Espai Maker (Makerspace) i a les diferents tipologies (FabLab, Hackspace, HackLab, GrootSpace ...), a la cultura i moviment maker, als espais col·laboratius i a la fabricació digital.'
    - 'Descobrir un nou món d’oportunitats i objectius.'
    - 'Motivar-se per afrontar la cultura oberta i promoure les activitats i espais makers a les seves pròpies organitzacions.'
lenght: '120 min'
---

## Introducció
L’objectiu d’aquesta primera part és ressaltar els coneixements previs que tinguem. També es convida a compartir informació sobre el context o la vostra participació a la sessió (motivació, objectius personals, etc.) respecte aquest tema.

L’El formador/ora organitza un trencaclosques per treballar alguns dels conceptes clau. Comença per preguntar al grup què sabem en relació als conceptes següents (vegeu més amunt). Si es tracta d’una classe presencial, el formador o formadora prendrà nota de les respostes del tauler, subratllant les que siguin més adequades. Si es tracta d’una formació en línia, s’organitza un petit debat a través d’un fòrum.

Introducció al moviment Maker:
* Què en sabem?
* Què es?
* Quan comença?
* Què és un Makerspace?

## Primeres informacions
Aquesta part de la sessió està dissenyada per proporcionar informació bàsica sobre el tema. S’utilitzaran els vídeos proposats o altres vídeos que el formador/ora seleccionarà amb antelació. Aquesta secció normalment garanteix que els participants podran identificar què és un Fablab, què és un espai de creació i tenir una idea més concreta al respecte.

Mirem els vídeos i comentem ...
* [FabLab ¿Qué es un FABLAB?](https://youtu.be/L_15Rc79T_c).
* Què és un Fab Lab?  [What is a Fab Lab?](https://www.youtube.com/watch?v=lPF7zDSf-LA).
* Què és un MakerSpace? [What Is The MakerSpace?](https://www.youtube.com/watch?v=wti6FMvDAE4).
* Espai Santiago Maker [Santiago Maker Space](https://youtu.be/i4OWsLY5RBo).

## Nous conceptes
Els espais Maker són un concepte d’espai on es desenvolupen projectes segons la filosofia del moviment maker. Van sorgir inicialment com a espais de trobada per a persones que volien aprendre, investigar i compartir els seus coneixements sobre fabricació digital. 

Són un reflex directe del que eren els Hakerspaces de la dècada de 1970, quan van aparèixer els primers computadors personals, aplicats al món del maquinari obert.

El concepte de l’Espiral Creativa de Mitchel Resnick és un dels conceptes més significatius pel que fa al moviment maker aplicat a les metodologies d'aprenentatge.

El o la formador/a presentarà l’Espiral Creativa de Mitchel Resnick. Destacarà les idees principals mitjançant la presentació de Wikislides i explicarà quins són els espirals creatius i els seus avantatges en el camp de l'educació (i proposarà llegir l'article disponible aquí com a treball posterior)

Després d’això, el grup es desplaçarà per identificar els punts forts i febles possibles d’un MakerSpace. Els participants treballaran per parelles i desenvoluparan l'activitat mitjançant una plantilla bàsica (dues columnes: punts forts i febles) o utilitzant diferents materials per "representar" punts forts i febles (visualment), mitjançant cartrons i llapis de colors, per exemple, o peces de lego. ...) Després de 15 minuts de treball, compartiran el resultat amb altres participants.

El formador s’assegurarà que s’explorin alguns conceptes: fabricació digital, col·laboració, aprenentatge fent i DIT (Do it Yourself) La cultura creadora.

Si l’activitat s’executa de manera virtual, el suggeriment és fer l’activitat de manera individual i suggerir construir la llista mitjançant una eina infogràfica (com ara [Venngage](https://es.venngage.com/templates) o [Piktochart](https://piktochart.com/formats/infographics/)), tot compartint el resultat través d’un fòrum o llista de correu electrònic.
## Makerspaces
L’objectiu d’aquesta secció garanteix que els participants despleguin una actitud activa buscant espais de confecció.

El formador suggerirà als participants que obrin un navegador d’Internet i que busquin “makerspaces” arreu del món i el seu territori (país / regió / ciutat). Poden fer-ho individualment i compartir el resultat en una llista comuna. Es recomana l’ús de [Padlet](https://padlet.com/) (no cal registrar-se), ja que poden utilitzar aquesta eina per compartir diferents tipus de fonts (text, vídeos, etc.) D’aquesta manera, recopilen i comparteixen informació molt ràpidament.

### Consells
El formador ha d’estimular els participants i ajudar-los durant la sessió.

## Treball a casa (autònom)
Busqueu un espai de creació a la vostra comunitat, indiqueu què fa i on es troba. Si és possible, visiteu-lo i afegiu més informació sobre aquest a l’espai comú de Padlet.