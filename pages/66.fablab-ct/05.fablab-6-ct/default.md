---
title: 'Revisió entre iguals del disseny de Fablab comunitari'
objectives:
    - 'Proporcionar una introducció a la revisió entre iguals'
    - 'Facilitar un esborrany de guia del projecte per començar a implementar un Makerspace / FabLab'
    - 'Descobrir recursos i fonts necessàries de finançament i coneixement'
    - 'Motivar a obrir i promoure activitats i un espai de creació maker a la pròpia entitat'
lenght: '90 min'
---

## Introduction
El o la formadora parlarà sobre les preguntes que puguin sorgir després de la sessió anterior i comentarà Treball a casa.
Després d’això, el o la docent introduirà el contingut i les activitats principals de la sessió. 	
## Primera part
L’objectiu d’aquesta secció és assegurar-se que cada grup ha desenvolupat un projecte “realista” per començar amb la creació d'un espai maker comunitari. El formador/ora presentarà els conceptes següents:
* És necessari definir les necessitats materials i humanes des d'un punt de vista realista. 
* Què podem aconseguir? El projecte ha d’estar molt ben dimensionat.
* Llista de màquines i pressupost.
* És molt útil fer un quadrant d’horaris i activitats bàsiques.

## Segona part
El formador o formadora explicarà que tenim “algunes novetats”. Els grups faran l'activitat de revisió entre iguals. Cada grup de formació es dividirà en dos.
1. Una part del grup "defensarà" el projecte davant del comitè d'avaluació. 
2. L’altra part del grup conformarà la comissió d’avaluació del projecte d’altres grups. 

D’aquesta manera, tots els projectes es revisaran i milloraran mitjançant una estratègia peer-to-peer.
## Tercera part
Treball en grups:
* Cada grup ha de preparar la presentació del seu propi projecte de makerspace.
* Cada grup tindrà 2 minuts per llançar la seva idea (si es tracta d’una activitat d’aprenentatge electrònic, el grup pot crear un vídeo amb el seu elevador de tonalitat) i després rebrà comentaris de la resta de grups sobre aspectes a millorar o sobre potencial de la idea.

## Treball a casa (autònom)
Seguiu treballant en el vostre projecte! Aquesta és la vostra oportunitat de començar un nou FabLab