---
title: 'Com crear un Fablab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: ct
Materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - 'Paper i bolígrafs'
    - Paperògraf
length: '10 hores'
objectives:
    - 'Identificar què és un FabLab i la cultura que n''està involucrada'
    - 'Ser capaç d''iniciar una iniciativa FabLab, comprendre quins són els primers passos i quines tecnologies són més adequades per començar'
    - 'Detectar algunes tecnologies i eines obertes que siguin adequades en un entorn FabLab. Conèixer, detectar i saber com utilitzar fonts d''informació que proporcionen recursos que són útils en el marc d''acció d''un FabLab'
    - 'Explorar les comunitats de pràctica de Fablab que utilitzen tecnologies i eines obertes'
    - 'Explorar el territori per construir el vincle entre empresa - universitat - ciutadania - administracions públiques (promogui hèlix quàdruple)'
    - 'Permetre als participants proporcionar informació, recursos, models a les comunitats de pràctica de Fablabs. Disseny obert. Donar suport a les persones participants d''un FabLab durant el seu procés d''aprenentatge i producció'
skills:
    'Paraules clau':
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            HackLab: null
            'disseny obert': null
            'Maquinari obert': null
            'programari obert': null
            'Fes-t''ho tu mateix (bricolatge) (DIY)': null
            'Aprendre fent (LBD)': null
            PBL: null
---

## Introducció
La cultura Fablab està estretament relacionada amb la cultura DIY i Maker, així com amb el FLOSS; sempre és alguna cosa "en construcció" i per iniciar un Fablab no necessites tenir moltes eines o màquines, sinó l'actitud correcta. Un Fablab és un concepte, i començar un Fablab és treballar sota una filosofia concreta.
Aquest mòdul presentarà als participants el concepte del que es considera un Fablab i quin tipus d'activitats es poden realitzar allà.
## Context
Quan un imagina un Fablab, pensa en un espai de creació, que és col·laboratiu, que utilitza tecnologies obertes, que aprofita els recursos del territori més proper i també les fonts d'informació i recursos d'Internet. Un espai en continu creixement, amb algunes tecnologies que poden augmentar amb el temps. Un espai de concurrència de persones que arribaran amb una actitud oberta i curiosa. Un punt de partida que, si s'insereix i es relaciona amb la comunitat local, atraurà diferents tipus d'audiències i serà ric en creativitat i promourà l'autonomia i l'aprenentatge individual i individualitzat de les persones.

Un Fablab és molt més que un espai equipat per a la fabricació digital a petita escala. És un concepte, és un punt de vista, és una nova forma de generar riquesa i empoderament dins d'una comunitat. Equipat amb tecnologia i eines, obertes i gratuïtes, Fablab és un espai capaç de generar una dinàmica estretament vinculada a l'aprenentatge col·laboratiu, l'intercanvi i la cultura dels béns comuns i el lliure intercanvi de recursos i informació. Dins el marc d'un Fablab, especialment si té una orientació comunitària, es generen noves dinàmiques productives col·laboratives; és part del "Do it Yourself" (DIY, fes-ho tu mateix) i de la Maker Culture.

Crear un Fablab no requereix grans recursos financers. Començar és el primer pas i en aquest mòdul analitzarem aquest primer pas i planificarem, tots junts, les primeres etapes d'un Fablab.
## Sessions
### Prima sessió: Què és un Fablab?
Fabricació digital, col·laboració, aprendre fent i DiT (Do it Yourself). La cultura creadora.
### Segona sessió: Disseny comunitari i col·laboració
El potencial de gestionar un FabLab en una comunitat. Necessitats personals i comunitàries versus producció en massa.
### Tercera sessió: Technologies FabLab
Explorar el potencial de l'ús de maquinari i programari de codi obert en el marc d'un Fablab. Descobrir les tecnologies adequades per començar una iniciativa FabLab.
### Quarta sessió: Tecnologies Fablab
### Cinquena sessió: Primers passos...
Creant el teu propi FabLab. Disseny col·laboratiu d'una nova comunitat FabLab.
### Sisena sessió: Revisió per parells del disseny de la comunitat FabLab
Primeres activitats a desenvolupar.