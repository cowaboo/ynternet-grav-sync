---
title: 'Disseny i col·laboració comunitària'
objectives:
    - 'Proporcionar una introducció per a reconèixer el potencial d’executar un Fablab en una comunitat i Les necessitats personals i comunitàries versus la producció massiva industrial.'
    - 'Facilitar una visió general dels espais col·laboratius i la cultura maker.'
    - 'Descobrir i reflexionar sobre el potencial de la quàdruple hèlix al territori: empresa, universitat, ciutadania i administracions públiques.'
    - 'Motivar la detecció i col·laboració amb diferents agents implicats en l educació, la dinamització comunitària i la tecnologia, per posar en marxa activitats maker.'
lenght: '120 min'
---

## Introducció
El docent parlarà sobre les preguntes que es puguin plantejar després que la sessió anterior i també al Treball a casa. Després s'introduirà el contingut i les activitats principals de la sessió.
## Primera part
L’objectiu d’aquesta secció és entendre millor la cultura maker des d'una visió dels pro-comuns, com a introducció del potencial d’executar un Fablab en una comunitat.
El formador exposa algunes preguntes per avaluar els “coneixements previs” dels participants i obre la discussió sobre el significat de “Disseny i col·laboració comunitaris”. Es suggereix una pluja d’idees per posar al dia el que aquests conceptes signifiquen per als participants. El formador pot utilitzar el tauler (si s’està formant en presència) o una eina virtual per recollir les idees dels participants.
Després d'aquesta pluja d'idees, el formador/ora proposarà llegir (individualment) o veure un vídeo (col·lectivament) que plantegi un "manifest de fabricant" o que expliqui la "cultura creadora". Alguns exemples:
- [Manifest MakerConvent](http://conventagusti.com/maker/manifiesto-makerconvent/)  
- [The maker movement manifesto](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf). Hatch, Mark:  
- [The Maker Movement Manifesto](https://www.youtube.com/watch?v=Fjrtiyq3ECQ) by Mark Hatch   
- [Maker Movement: It’s about creating rather than consuming](https://www.youtube.com/watch?v=I_WQOqKldm4 ) - The Feed  

## Segona part
L’objectiu d’aquesta secció és comprendre millor els avantatges d’estar en un entorn comú i visualitzar alguns exemples, com a introducción del potencial d’executar un Fablab en una comunitat, amb un enfocament comú. 
Anem a veure i discutirem alguns exemples de creació massiva de creadors (es poden proporcionar alguns altres exemples):

- Un [MakerFaire](https://makerfaire.com): què és un Maker Faire? Qui hi va? Quines activitats fan allà?
- Altres xarxes participatives: [Codeclub](https://codeclub.org) i [Coderdojo](https://coderdojo.com). Discussió sobre els seus objectius, activitats i els valors que comporten.

Després d’això, l’formador demanarà als participants que facin un exercici d’imaginació, responent a les següents preguntes (individualment):
- M'agradaria visitar (o no) una Fira Maker perquè ...
- El major avantatge de crear un projecte dins del marc d'un espai Maker o Fablab és ...
- M'agradaria crear un espai de fabricació digital o un FabLab perquè ...
- Si pogués proposar els valors inherents als makerspaces, aquests serien ...
- Les persones que participarien en un espai de fabricació de la meva comunitat serien ... (professors, entitats socials, tècnics de l’ajuntament, empresaris, estudiants ...)
- Si decidís executar un Fablab, convidaria ... 
- Si decideixo no iniciar un Fablab, però començo a fer algunes activitats petites estretament relacionades, començaré per fer ...

## Tercera part
L'objectiu d'aquesta part és explicar i entendre el model d’innovació de la quàdruple hèlix i avaluar les possibilitats d’implementar-lo a la nostra comunitat.
En aquesta part, el formador explicarà com funciona el model d’innovació de quàdruple hèlix (el o la formadora pot reproduir [aquest vídeo](https://www.youtube.com/watch?v=9VoJ3w5ecsw) per presentar-ho breument) i proporcionarà una plantilla a completar, per tal d’identificar i classificar clarament els agents locals en quatre categories (model d’innovació quadruple hèlix). La idea principal és pensar en persones o institucions que podrien ser convidades a unir-se al “futur Fablab”.  
- Empreses
- Universitat
- Ciutadania: líders de la comunitat
- Administracions Públiques

En un segon pas, es convidarà als participants a especificar el paper que podrien exercir aquests agents locals en el nou sistema FabLab.

**Consell**: Ser el més específic i concret possible. 

## Treball a casa (autònom)
Busqueu una iniciativa que faci una crida al voluntariat i:
- mireu el que demanen
- quines dades necessiten
- el que ofereixen

## Referències
- [Mark Hatch](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf) 
- CLIQ project: [how quadruple helix innovation works](https://www.youtube.com/watch?v=9VoJ3w5ecsw ). 
- Yun, J.J.; Liu, Z. [Micro- and Macro-Dynamics of Open Innovation with a Quadruple-Helix Model](https://www.mdpi.com/2071-1050/11/12/3301). Sustainability 2019, 11, 3301. 
