---
title: 'Tecnologies dins d''un Fablab'
objectives:
    - 'Proporcionar una introducció a la Metodologia Fes-ho-tu-mateix (DIY) i Aprendre fent.'
    - 'Facilitar una millor comprensió sobre com funciona un Fablab o un Maker Space.'
    - 'Descobrir algunes bones idees a considerar, i alguns bons exemples a tenir en compte.'
    - 'Motivar els participants per ser (més) ciutadans creatius i actius.'
lenght: '90 min'
---

## Introducció
El o la formadora parlarà sobre les preguntes que puguin sorgir després de la sessió anterior i comentarà Treball a casa.
Després d’això, el o la docent introduirà el contingut i les activitats principals de la sessió.
## Primera part
L’objectiu d’aquesta secció és entendre què és i com funciona el mètode DiY.

"**Do it yourself**" ("**DIY**") Aquest és el mètode de construcció, modificació o reparació de les coses sense l'ajuda directa d'experts o professionals. La investigació acadèmica descriu el "bricolatge" com a comportaments en què "els individus es dediquen a treballar amb matèries primeres i semi-primeres i elements, per produir, transformar o reconstruir possessions de materials, incloses les extretes del medi natural. " ([Wikipedia](https://en.wikipedia.org/wiki/Do_it_yourself)) 
Les motivacions poden ser molt diferents:
* Relacionades amb el mercat: beneficis econòmics, falta de disponibilitat del producte, necessitat de personalització.
* Millora de la identitat o autoestima  (artesania, apoderament, cerca de comunitat, singularitat, diversió ...).

El formador o formadora també pot desafiar els estudiants a mirar vídeos sorprenents en un canal de vídeo (Vimeo, YouTube o similar) basat en la cultura i l’ètica de DiY.
* Busquem… DiY furniture, DiY plants, DiY clothes… etc. 

Després de l’explicació, el formador o la formadora pot fer algunes preguntes (preguntes obertes a l’aula o en un fòrum d’aprenentatge electrònic):
* Tens alguna experiència DIY? Si sí ... com va ser? què vas aconseguir?
* Ja n’heu sentit o assabentat anteriorment? En cas afirmatiu, quin creus que és el valor més important? 
* T’agradaria provar? Pot pensar en alguna activitat per començar?

## Segona part
L’objectiu d’aquesta secció és entendre què és l’Aprenentatge Basat en Projectes i quina relació té amb la cultura maker i les tecnologies DiY.
El o la formadora reproduirà aquests vídeos: 
* Vídeo: Aprenentatge basat en projectes: per què, com i exemples. Project Based Learning: [Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw).
* Vídeo: Els estudiants fabriquen una màquina protètica per a companys de classe mitjançant una impressora 3D [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g).

Després de reproduir els vídeos, es pot realitzar un vídeo-fòrum.
* Alguna vegada has participat en un aprenentatge basat en projectes? Com va ser la teva experiència?
* Si voleu organitzar alguna PBL, quin serà el vostre punt de partida?
* Considerem algunes oportunitats reals. Penseu en problemes i en reptes ...

L’aprenentatge basat en projectes (PBL) és una pedagogia centrada en l’alumnat que implica un enfocament dinàmic en l’aula en què es creu que els estudiants adquireixen un coneixement més profund mitjançant l’exploració activa de reptes i problemes del món real. Els estudiants aprenen sobre un tema treballant durant un període prolongat de temps,  investigant i responent a una pregunta, un repte o un problema complexos. És un estil d’aprenentatge actiu i d’aprenentatge basat en investigacions. El PBL contrasta amb el paper, la memorització de elements o la instrucció dirigida per un professor que presenta fets establerts o retrata un camí suau al coneixement en lloc de plantejar-se preguntes, problemes o escenaris. Més info a [Wikipedia](https://en.wikipedia.org/wiki/Project-based_learning).
## Tercera part: Aprenentatge Basat en projectes amb la impressió 3D
L’objectiu d’aquesta secció és entendre com es pot aplicar el mètode ABP en el context de la impressió 3D i que la impressió 3D no és només una part d’una indústria milionària.

Es pot començar reproduint vídeos inspiradors, que presenten diverses idees molt concretes:
* [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs).
* [15 Things You Didn't Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E).
* [Commercial website about printing and digital printing services](https://www.shapeways.com/create).
* [3D Printing Fashion](https://www.youtube.com/watch?v=3s94mIhCyt4): How I 3D-Printed Clothes at Home.
* [Dinner is printed](https://www.youtube.com/watch?v=B0Ty6wgM8KE): is 3D technology the future of food?

El formador o la formadora organitzarà els participants en 3 focus grup i mostrarà algunes preguntes que s’hauran de respondre: 
* En aquests vídeos, quines oportunitats es mostren?
* Quins són els valors implicats?
* Si parlen d’oportunitats, quines detecteu?
* Si pensem en persones que puguin participar en aquest tipus d’activitats, quin és el perfil? 
* Quants diners podria costar la tecnologia implicada?

Després de 15 minuts treballant en grups, els participants resumeixen les idees principals que han sorgit. Després d'això, el formador/ora organitzarà una discussió: com podem aplicar el que hem après en un Fablab comunitari?
## Treball a casa (autònom)
Planifiqueu un mini projecte o activitat on s’utilitzin diferents màquines i descriviu què faríeu amb cadascuna. Penseu en la gestió dels voluntaris que podrien estar interessats en participar en el nou FabLab. 

Conceptes clau:
* El retorn social de ser voluntari
* Metodologia d'aprenentatge i servei
* Mentoria.