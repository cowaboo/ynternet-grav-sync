---
title: 'Open coding with Scratch'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open coding with Scratch'
lang_id: 'Open coding with Scratch'
lang_title: en
length: '11 hours'
objectives:
    - 'Code in Scratch for developing interactive stories, games and animation'
    - 'Use Scratch for working collaboratively (share a project from the Scratch Online Editor, view and remix another user’s project)'
    - 'Introduce participants to Scratch Philosophy and learn how to code in Scratch'
    - 'Share own projects and use other user’s projects'
materials:
    - 'Personal computer'
    - 'Internet connexion'
    - 'Scratch program'
skills:
    Keywords:
        subs:
            Scratch: null
            'Open coding': null
            'Scratch philosophy': null
            'Sequential processing': null
            'Code block': null
            Blocks: null
            Projects: null
            Stage: null
            Scripts: null
---

## Introduction
Scratch is a free programming language and online community developed by MIT which can be used to create games, animations, songs and share them online.

Scratch is used by people of all ages in a wide variety of settings. The ability to code computer programs is an important part of literacy in today’s society. When people learn to code in Scratch, they learn important strategies for solving problems, designing projects, and communicating ideas.

Statistics on the language's official website show more than 40 million projects shared by over 40 million users, and almost 40 million monthly website visits.
## Context
The goal of the session is to practically demonstrate and engage learners on how:
* to code in Scratch for developing interactive stories, games and animation.
* to use Scratch for working collaboratively (share a project from the Scratch Online Editor, view and remix another user’s project) 

The purpose of this training course is to introduce participants to Scratch Philosophy and learn how to code in Scratch, sharing their own projects and using other user’s projects. 
At the end of the sessions participants will be able to create, program and share material. Open coding with Scratch can help users learn to think outside the box, reason systematically and work collaboratively.
## Sessions
### First session: Scratch Philosophy: “Imagine, program, share”
The philosophy of Scratch encourages the sharing, reuse, and combination of code. This session will provide an overview of the Scratch philosophy "Imagine, Program, Share”.
Users can create their own projects or reuse someone else's project. Projects created and remixed with Scratch are licensed under the Creative Commons Attribution-Share Alike License. 
The session aims to gain greater understanding of Scratch’s background and principles for creating, programming and sharing material. 
At the end of the session, participants will be able to examine the use and advantages of the Scratch Philosophy in adult education.
### Second session: Open coding with the free visual programming Scratch 
When people learn to code in Scratch, they learn important strategies for solving problems, designing projects, and communicating ideas. So, coding in Scratch helps users to think creatively, reason systematically and work collaboratively – essential skills for life in the 21st century.
The second session aims at introducing the programming language developed by MIT.
Participants will learn how to code in Scratch for developing interactive stories, games and animation. 
### Third session: The effectiveness of Scratch in creating collaborative programming environment
This Module aims at illustrating the fundamentals of the online community.
The session aims at supporting learners to identify and profit from the Scratch online community. Participants will learn how to share a project from the Scratch Online Editor, view and remix another user’s project getting inspired by others at the same time.
## Presentation
<iframe src="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129380-1/" width="800" height="400"></iframe><p><a href="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129380-1/">Open Coding with Scratch</a></p>