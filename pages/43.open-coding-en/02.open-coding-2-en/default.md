---
title: 'Open coding with the free visual programming Scratch'
length: '360 min'
objectives:
    - 'Discover the benefits of Scratch for the development of 21st Century skills'
    - 'Design interactive stories, games and animation'
    - 'Learn autonomous from tutorial and videos'
    - 'Access and support resources for Scratch'
---

## Group discussion on “Coding in Scratch?”
Trainer will start the introduction to the module by asking participants about their programming language experience.
## Design, create and invent with new media
“Digital fluency requires not just the ability to chat, browse, and interact but also the ability to design, create, and invent with new media” (…). The ability to program provides important benefits. In particular, programming supports “computational thinking,” helping you learn important problem-solving and design strategies that carry over to no programming domains. (Scratch: Programming for all).
Presentation and analysis of the benefits of programming. Coding in Scratch helps users to think creatively, reason systematically and work collaboratively – essential skills for life in the 21st century.
* [Learn to code, code to learn](https://www.youtube.com/watch?v=Ok6LbV6bqaE)
* [Scratch: Unleashing Creativity through Coding](https://www.youtube.com/watch?v=pwtN0DHB7RY)
* [Why is coding so important](https://www.forbes.com/sites/quora/2018/08/16/why-is-coding-so-important/#3866fc362adc)
* [What is computational thinking?](https://www.youtube.com/watch?time_continue=1&v=sxUJKn6TJOI)

## Let’s code!
Introduction to the basics of Scratch (navigate the interface: stage area, blocks palette, and a coding area to place and arrange the blocks into runnable scripts; create sprites and backgrounds etc).
### Activity 1. Cat welcome
Participants register to the Scratch website (simple procedure). 
Participants are asked to log in to their account and to perform the following tasks: 
1.	Go to the [Scratch website](https://scratch.mit.edu/) and log in to your account. 
2.	Create a project/file called Cat Welcome 
3.	Set the background and instruct the Sprite cat using Scratch coloured coding blocks to ask “What’s your name?” and then to welcome the respondent with “Please to meet you + name in the answer”
4.	When you have finished, open the Student report "Cat Welcome" and reply.

As feedback, the teacher can show the following figure and/or provide students with the link to the shared project (https://scratch.mit.edu/projects/306876279/)

![](scratch1.png)

SCRATCH CODE FOR CAT WELCOME
### Activity 2. Cat Question
Participants are asked to perform the following tasks: 
1.	Go to the [Scratch website](https://scratch.mit.edu/) and log in to your account. 
2.	Create a project/file called Cat Question
3.	Set the background and instruct the Sprite cat using Scratch coloured coding blocks to make the cat say “Hello” and then ask the question “When did Columbus discover Ameri-ca?”. If the answer is wrong, the cat says “Retry”. If the answer is right (1492) the cat says “Very good!!”
4.	When you have finished, open the Student report "Cat Question" and reply.

As feedback, the teacher can show the following figure and/or provide students with the link to the shared project (https://scratch.mit.edu/projects/306876445/)

![](scratch2.png)

SCRATCH CODE FOR CAT QUESTION
### Access support and resources for Scratch
Participants will learn how to code in Scratch with the support of online tutorial and resources. 
* [Learn to code, code to learn](https://scratch.mit.edu/projects/31407152).
* [Scratch Tutorial: make your first program](https://www.youtube.com/watch?v=VIpmkeqJhmQ).
* [Introductory Tutorial](https://scratch.mit.edu/help/videos/#Introductory%20Tutorials).

## Homework
Participants are asked to perform the following tasks: 
1.	Go to the [Scratch website](https://scratch.mit.edu/) and log in to your account. 
2.	Create a project/file called Mouse Grid.
3.	Choose a mouse as Sprite and a grid as a Backdrop.
4.	Make the mouse move with the keyboard’s directional arrows on the grid: up or down, right or left. When you have finished, open the Student report "Mouse Grid" and reply.

As feedback, the teacher can show the following figure and/or provide students with the link to the shared project (https://scratch.mit.edu/projects/306876524/)

![](scratch3.png)

SCRATCH CODE FOR MOUSE GRID
 
Participants are asked to find online tutorials needed for performing the task. 
## References
* [Getting started with Scratch](https://cdn.scratch.mit.edu/scratchr2/static/__6fc1c81d1c536d64cab16bd429100958__/pdfs/help/Getting-Started-Guide-Scratch2.pdf).
* [Tutorials](https://scratch.mit.edu/studios/174756/) to help you.
* [Programming with Scratch](https://anthsperanza.com/2018/05/01/scratch-educator-guide/).