---
title: 'Scratch Philosophy: “Imagine, program, share”'
length: '120 min'
objectives:
    - 'Discover the fundamentals behind the Scratch Philosophy, the principles for creating, programming and sharing material'
    - 'Examine the use and advantaged of the  Scratch Philosophy in adult education'
---

## Group discussion
Trainer will start the introduction to the module by asking participants about their level coding experience.  Answers will be documented and then reused to customize the training material.
## “Imagine, program, share”
The educational programming language Scratch was developed by the Lifelong Kindergarten Group at the Massachusetts Institute of Technology (MIT) and it is provided free of charge.
Scratch is a block-based visual programming language and online community. Although it is designed especially for ages 8 to 16, Scratch is used by people of all ages. Millions of people are creating Scratch projects in a wide variety of settings.
The first version of Scratch available to the public was released in 2013. The philosophy of Scratch encourages the sharing, reuse, and combination of code, as indicated by the team slogan, "Imagine, Program, Share".

Scratch's motto follows the basic principle of creating a project: one comes up with an idea ("Imagine"), programs the idea in Scratch ("Program"), and then publishes it in the community ("Share"). Since the release of Scratch 2.0, the motto has been less apparent throughout the website; the front page no longer has the motto but instead a description of what Scratch is.
Coding in Scratch allows users to develop their own projects and to use someone else's project. Projects created and remixed with Scratch are licensed under the Creative Commons Attribution-Share Alike License. Scratch automatically gives credit to the user who created the original project and program.
## Homework
Participants should explore the Scratch website (https://scratch.mit.edu/) in the language of their choice. Then, they are asked to watch the videos and take notes while they watch the videos:  
* [5 reasons why you should to learn to code in Scratch](https://www.youtube.com/watch?v=zCzotctwkwk).
* [Scratch as a Toll for creative learning](https://www.youtube.com/watch?v=pDF2vxxK1L4&list=PLpfxVARjkP--eb4ddxhvSYuGZ1QaxSxvr&index=6).

Participants are asked to express their own opinions the fundamental behind the Scratch Philosophy and its main benefits.
## References
* [Scratch Wiki](https://en.scratch-wiki.info/wiki/Scratch_Wiki).
* [Wikipedia](https://en.wikipedia.org/wiki/Scratch_(programming_language)).
* [Scratch website](https://scratch.mit.edu/about).

