---
title: 'Un sistema operatiu obert GNU / Linux: la transició a FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: ct
length: '9 hores i 30 minuts'
objectives:
    - 'Proporcionar un entorn segur als participants per migrar a GNU / Linux en els seus espais de treball'
    - 'Crear una xarxa de suport entre els participants'
    - 'Promoure la creació d''una xarxa d''igual a igual, per brindar suport entre els diferents agents del territori involucrats en la difusió de PLL'
materials:
    - 'Ordinador amb connexió a internet'
    - 'Un navegador web actualitzat'
    - 'Un PC on es pugui fer les proves d''instal·lació (pot perdre les dades guardades)'
    - '1 o 2 USB per a instal·lacions de SO'
skills:
    'Paraules clau':
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            'Sistema operatiu': null
            Migració: null
            'Administració de sistemes': null
---

## Introducció
La infraestructura tecnològica d'un telecentre o un centre de capacitació digital és el punt de partida per desenvolupar les seves activitats pedagògiques. El disseny de la infraestructura definirà la política d'intervenció que podrem desenvolupar i els temes en què podrem treballar, així com les perspectives del nostre treball.

Ja sigui que treballem en projectes bàsics d'alfabetització digital o en especialitats digitals més complexes (edició de vídeo digital, món de la creació, habilitats avançades de programació ...) vam acabar ensenyant productes i tecnologies digitals específiques. La transició a treballar amb eines tecnològiques gratuïtes (FLOSS) pot anar acompanyada de treballar, també, amb sistemes operatius gratuïts, oferint la capacitat d'entendre la tecnologia com un coneixement obert i compartit més enllà de les opcions tancades que ofereix el mercat.
## Context
Objectius de la sessió:
* Proporcionar un entorn segur per als participants per a què puguin migrar a GNU / Linux en els seus espais de treball.
* Crear una xarxa de suport entre les persones participants.
* Promoure la creació d'una xarxa d'igual a igual, per brindar suport entre els diferents agents del territori involucrats en la difusió de PLL.

S'utilitzaran dues metodologies diferents durant aquest mòdul:
* **Deliberació**: grups de discussió i fòrums de discussió.
* **Execució**: pràctiques reals en ordinadors proporcionades per estudiants.

## Sessions
### Primera sessió: Les nostres necessitats
En aquesta sessió treballarem a partir de l'anàlisi de les necessitats de cada telecentre / centre de capacitació digital i el ubicarem en el seu context. Farem un mapa dels diferents agents amb els quals podem relacionar-nos i els seus rols: administració pública, comunitats de desenvolupament de programari lliure, universitats, altres centres de capacitació. A més, s'especificaran les eines de FLOSS que poden respondre a les necessitats educatives de cada espai en concret.
### Segona sessió: Els passos a seguir
En funció de les necessitats detectades, dissenyarem un pla de migració, triant el millor SO que pugui satisfer les necessitats. S'analitzaran i avaluaran diferents mecanismes de migració, d'acord amb les necessitats de cada centre. Es realitzarà una instal·lació de prova i es desenvoluparan pautes de manteniment i actualització per garantir que el centre es mantingui en bona forma.
### Tercera sessió: I ara què?
Una vegada que es completi la instal·lació i es resolguin les regles de manteniment i actualització, aprendrem com instal·lar nous programes de manera segura.
### Quarta sessió: Despertar i disseminar
Cal treballar a nivell pedagògic, explicar a les persones que participen en cada centre com funcionen aquestes tecnologies i les principals raons per utilitzar aquest tipus de tecnologia. Estudiarem i desenvoluparem estratègies per a la difusió dels sistemes FLOSS i per adaptar el material didàctic de cada centre, de manera que es pugui cotinuar realitzant la seva tasca principal: l'alfabetització digital o la capacitació en competències digitals.