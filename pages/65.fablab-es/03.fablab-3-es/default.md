---
title: 'Tecnologias dentro de un Fablab'
objectives:
    - 'Proporcionar una introducción a la Metodología Hazlo-tú-mismo (DIY) y Aprender haciendo.'
    - 'Facilitar una mejor comprensión sobre cómo funciona un Fablabs o un Maker Space.'
    - 'Descubrir algunas buenas ideas a considerar, y algunos ejemplos a tener en cuenta.'
    - 'Motivar a los participantes para ser ciudadanos (más) creativos y activos.'
lenght: '90 min'
---

## Introducció
El docente tratará las preguntas que puedan plantearse después de que la sesión anterior y también al Trabajo en casa (trabajo autónomo).
Después se introducirá el contenido y las actividades principales de la sesión.

## Primera parte
El objetivo de esta sección es entender qué es y cómo funciona el método DIY.

"**Do it yourself**" ("**DIY**") Este es el método de construcción, modificación o reparación de las cosas sin la ayuda directa de expertos o profesionales. La investigación académica describe el "bricolaje" como comportamientos en que "los individuos se dedican a treballar con  materias primas y semi-primas y elementos, para producir, transformar o reconstruir posesiones materiales, incluidas las extraídas del medio natural. ([Wikipedia](https://en.wikipedia.org/wiki/Do_it_yourself)) 

Las motivaciones pueden ser muy diferentes :
* Relacionadas con el mercado: beneficios económicos, falta de disponibilidad del producto, necesidad de personalización...
* Mejora de la identidad o autoestima (artesanía, apoderamiento, búsqueda de comunidad, singularidad, diversión...)

El formador o formadora también puede desafiar a los estudiantes a mirar vídeos sorprendentes en un canal de vídeo (Vimeo, YouTube o similar) basado en la cultura y la ética de DIY.
* Buscamos ... DIY furniture, DIY plants, DIY clothes ... etc.

Después de la explicación, el formador o la formadora puede hacer algunas preguntas (preguntas abiertas en el aula o en un foro de aprendizaje electrónico):
* Tienes alguna experiencia DIY? Si sí ... ¿cómo fue? ¿qué conseguiste?
* ¿Habéis oído hablar de ello anteriormente? En caso afirmativo, ¿cuál crees que es el valor más importante?
* ¿Te gustaría probar? ¿Puedes pensar en alguna actividad para empezar?

## Segunda parte
El objetivo de esta sección es entender qué es el Aprendizaje Basado en Proyectos y qué relación tiene con la cultura maker y las tecnologías DIY. 
El o la formadora reproducirá estos vídeos:
* Vídeo: Aprendizaje basado en proyectos: por qué, cómo y ejemplos. Project Based Learning: [Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw).
* Vídeo:Los estudiantes fabrican una máquina protésica para compañeros de clase mediante una impresora 3D [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g).

Después de reproducir los vídeos, se puede realizar un video-forum.
* Alguna vez has participado en un aprendizaje basado en proyectos? ¿Cómo fue tu experiencia?
* Si quieres organizar alguna PBL, ¿cuál será tu punto de partida?
* Consideremos algunas oportunidades reales. Consideremos problemas y retos ..

El aprendizaje basado en proyectos (PBL) es una pedagogía centrada en el alumnado que implica un enfoque dinámico en el aula en la que se cree que los estudiantes adquieren un conocimiento más profundo mediante la exploración activa de retos y problemas del mundo real. Los estudiantes aprenden sobre un tema trabajando durante un período prolongado de tiempo, investigando y respondiendo a una pregunta, un reto o un problema complejos. Es un estilo de aprendizaje activo y de aprendizaje basado en investigaciones. El PBL contrasta con el papel, la memorización de elementos o la instrucción dirigida por un profesor que presenta hechos establecidos o retrata un camino suave al conocimiento en lugar de plantearse preguntas, problemas o escenarios. Más información en la [Wikipedia](https://en.wikipedia.org/wiki/Project-based_learning).

## Tercera parte: Aprendizaje Basado en proyectos con la impresión 3D
El objetivo de esta sección es entender cómo se puede aplicar el método ABP en el contexto de la impresión 3D y que la impresión 3D no es sólo una parte de una industria millonaria.

Se puede comenzar reproduciendo vídeos inspiradores, que presentan varias ideas muy concretas:
* [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs).
* [15 Things You Didn't Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E).
* [Commercial website about printing and digital printing services](https://www.shapeways.com/create).
* [3D Printing Fashion](https://www.youtube.com/watch?v=3s94mIhCyt4): How I 3D-Printed Clothes at Home.
* [Dinner is printed](https://www.youtube.com/watch?v=B0Ty6wgM8KE): is 3D technology the future of food?

El formador o la formadora organizará los participantes en 3 focos grupo y mostrará algunas preguntas que deberán responder:
* En estos vídeos, ¿qué oportunidades se muestran?
* ¿Cuáles son los valores implicados?
* Si hablan de oportunidades, ¿qué detectan?
* Si pensamos en personas que puedan participar en este tipo de actividades, ¿cuál es el perfil?
* ¿Cuánto dinero podría costar la tecnología implicada?

Después de 15 minutos trabajando en grupos, los participantes resumen las ideas principales que han surgido. Después de esto, el formador/ora organizará una discusión: ¿cómo podemos aplicar lo que hemos aprendido en un FabLab comunitario?

## Trabajo en casa (trabajo autónomo)
Planificad un mini proyecto o actividad donde se utilicen diferentes máquinas y describa qué haría con cada una. Considere la gestión de los voluntarios que podrían estar interesados en participar en el nuevo FabLab.

Conceptos clave:
* El retorno social de ser voluntario.
* Metodología de aprendizaje y servicio.
* Mentoría.