---
title: 'Revisión entre iguales del diseño de FabLab comunitario'
objectives:
    - 'Proporcionar una introducción a la revisión entre iguales'
    - 'Facilitar un borrador de guía del proyecto para empezar a implementar un Makerspace / FabLab'
    - 'Descubrir recursos y fuentes necesarias de financiación y conocimiento'
    - 'Motivar a abrir y promover actividades y un espacio de creación maker a la propia entidad'
lenght: '90 min'
---

## Introducción
El docente tratará las preguntas que puedan plantearse después de que la sesión anterior y también al Trabajo en casa (trabajo autónomo). 
Después se introducirá el contenido y las actividades principales de la sesión.	  	
## Primera parte
El objetivo de esta sección es asegurarse de que cada grupo ha desarrollado un proyecto "realista" para comenzar con la creación de un espacio maker comunitario. El formador/a presentará los conceptos siguientes:

* Es necesario definir las necesidades materiales y humanas desde un punto de vista realista.
* ¿Qué podemos conseguir? El proyecto debe estar muy bien dimensionado.
* Lista de máquinas y presupuesto.
* Es muy útil hacer un cuadrante de horarios y actividades básicas.

## Segona part
El formador o formadora explicará que tenemos "algunas novedades". Los grupos harán la actividad de revisión entre iguales. Cada grupo de formación se dividirá en dos.
1. Una parte del grupo "defenderá" el proyecto ante el comité de Evaluación.
2. La otra parte del grupo conformará la comisión de Evaluación del proyecto de otros grupos.

De este modo, todos los proyectos se revisarán y mejorarán mediante una estrategia peer-to-peer.

## Tercera parte
trabajo en grupo/s:
* Cada grupo debe preparar la presentación de su propio proyecto de makerspace.
* Cada grupo tendrá 2 minutos para lanzar su idea (si se trata de una actividad de aprendizaje electrónico, el grupo puede crear un vídeo con su elevador de tonalidad) y luego recibirá comentarios del resto de grupos sobre aspectos a mejorar o sobre potencial de la idea.

## Trabajo en casa (trabajo autónomo) 
Continuación del trabajo en el proyecto. ¡Esta es vuestra oportunidad de empezar un nuevo FabLab!