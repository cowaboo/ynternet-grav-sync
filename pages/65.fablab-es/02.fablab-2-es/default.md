---
title: 'Diseño y colaboración comunitaria'
objectives:
    - 'Proporcionar una introducción para reconocer el potencial de ejecutar un Fablabs en una comunidad y las necesidades personales y comunitarias versus la producción masiva industrial.'
    - 'Facilitar una visión general de los espacios colaborativos y la cultura maker.'
    - 'Descubrir y reflexionar sobre el potencial de la cuádruple hélice en el territorio: empresa - universidad - ciudadanía - administraciones públicas.'
    - 'Motivar la detección y colaboración con diferentes agentes implicados en la educación, la dinamización comunitaria y la tecnología, para poner en marcha actividades maker.'
lenght: '90 min'
---

## Introducción
El docente tratará las preguntas que puedan plantearse después de que la sesión anterior y también al Trabajo en casa (trabajo autónomo). Después se introducirá el contenido y las actividades principales de la sesión.

## Primera parte
El objetivo de esta sección es entender mejor la cultura maker desde una visión de los pro-comunes, como introducción del potencial que tiene platear un Fablab en una comunidad.

El formador expone algunas preguntas para evaluar los "conocimientos previos" de los participantes y abre la discusión sobre el significado de "Diseño y colaboración comunitarios". Se sugiere una lluvia de ideas para poner al día lo que estos conceptos significan para los participantes. El formador puede utilizar la pizarra (si se está formando en presencia) o una herramienta virtual para recoger las ideas de los participantes.

Después de esta lluvia de ideas, el formador propondrá leer (individualmente) o ver un vídeo (colectivamente) que contenga un "manifiesto de fabricante" o que explique la "cultura creadora". Algunos ejemplos:

- [Manifest MakerConvent](http://conventagusti.com/maker/manifiesto-makerconvent/)  
- [The maker movement manifesto](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf). Hatch, Mark:  
- [The Maker Movement Manifesto](https://www.youtube.com/watch?v=Fjrtiyq3ECQ) by Mark Hatch   
- [Maker Movement: It’s about creating rather than consuming](https://www.youtube.com/watch?v=I_WQOqKldm4 ) - The Feed  

## Segunda parte
El objetivo de esta sección es comprender mejor las ventajas de estar en un entorno común y visualizar algunos ejemplos, como introducción del potencial de ejecutar un Fablabs en una comunidad, con un enfoque común.

Vamos a ver y discutiremos algunos ejemplos de creación masiva de creadores (se pueden proporcionar algunos otros ejemplos):

- Un [MakerFaire](https://makerfaire.com): ¿qué es una Maker Faire? Quién va? ¿Qué actividades hacen allí?
- Otras redes participativas: [Codeclub](https://codeclub.org) y [Coderdojo](https://coderdojo.com). Discusión sobre sus objetivos, actividades y los valores que conllevan.

Después de esto, el formador o formadora pedirá a los participantes que hagan un ejercicio de imaginación, respondiendo a las siguientes preguntas (individualmente):
- Me gustaría visitar (o no) una Feria Maker para...
- La mayor ventaja de crear un proyecto dentro del marco de un espacio Maker o FabLab es...
- Me gustaría crear un espacio de fabricació digital o un FabLab porque...
- Si pudiera proponer los valores inherentes a los makerspaces, estos serían...
- Las personas que participarían en un espacio de fabricación en mi comunidad serían... (profesores, entidades sociales, técnicos del ayuntamiento, empresarios, estudiantes...)
- Si decide ejecutar un Fablabs, invitaría...
- Si decido no iniciar un Fablabs, pero començo a hacer algunas actividades pequeñas estrechamente relacionadas, empezaré por hacer...

## Tercera parte
El objetivo de esta parte es explicar y entender el modelo de innovación de la cuádruple hélice, y evaluar las posibilidades de implementarlo en nuestra comunidad.
En esta parte, el formador o formadora explicará cómo funciona el modelo de innovación de cuádruple hélice [el formador puede reproducir este vídeo](https://www.youtube.com/watch?v=9VoJ3w5ecsw) per presentarlo brevemente) y proporcionará una plantilla completar, con el fin de identificar y clasificar claramente los agentes locales en cuatro categorías (modelo de innovación cuádruple hélice). La idea principal es pensar en personas o instituciones que podrían ser invitadas a unirse al "futuro FabLab".  
- Empresas
- Universidad
- Ciudadanía: líderes de la comunidad
- Administraciones Públicas

En un segundo paso, se invitará a los participantes a especificar el papel que podrían desempeñar estos agentes locales en el nuevo sistema FabLab.

**Consejo**: Ser lo más específico y concreto posible.

## Trabajo en casa (trabajo autónomo)
Buscad una iniciativa que haga un llamamiento al voluntariado y:
- mirad lo que piden
- qué datos necesitan
- observad qué ofrecen

Conclusiones: 
Los aspectos clave de la creación de una comunidad: ¿qué nos lleva a participar en cualquier proyecto?

## Referencias

- [Mark Hatch](http://raumschiff.org/wp-content/uploads/2017/08/0071821139-Maker-Movement-Manifesto-Sample-Chapter.pdf) 

- CLIQ project: [how quadruple helix innovation works](https://www.youtube.com/watch?v=9VoJ3w5ecsw ). 

- Yun, J.J.; Liu, Z. [Micro- and Macro-Dynamics of Open Innovation with a Quadruple-Helix Model](https://www.mdpi.com/2071-1050/11/12/3301). Sustainability 2019, 11, 3301. 
