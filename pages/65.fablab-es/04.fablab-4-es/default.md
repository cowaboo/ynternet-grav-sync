---
title: 'Necesidades y análisis del contexto real'
objectives:
    - 'Proporcionar una introducción a la exploración del potencial del uso de hardware y software de código abierto en el marco de un FabLab.'
    - 'Descubrir las tecnologías adecuadas para iniciar una iniciativa FabLab.'
    - 'Facilitar una visión general del hardware y software abiertos y una visión general de las máquinas relacionadas con la fabricación digital.'
    - 'Descubrir uso de diferentes tecnologías en diferentes tipos de actividades.'
    - 'Motivar el uso de tecnologías abiertas y abrir y promover las actividades de un espacio de creación maker en nuestra entidad.'
lenght: '90 min'
---

## Introducción
El docente tratará las preguntas que puedan plantearse después de que la sesión anterior y también al Trabajo en casa (trabajo autónomo). 
Después se introducirá el contenido y las actividades principales de la sesión.

## Primera parte
El objetivo de esta sección es garantizar que los estudiantes entiendan que hay cientos de tecnologías abiertas que pueden ser útiles en un FabLab e identificar diferentes técnicas y materiales de fabricación digital. 

El formador / a presentará dos páginas web y se asegurará de que los participantes se pongan y disfruten echando un vistazo a las características de algunos de los productos / tecnologías. 

* Lista de máquinas FabLab: Lista de máquinas, kits y tipos de tecnologías y máquinas que se utilizarán en un espacio Fab Lab: [List of FabLab machines](https://www.fablabs.io/machines) 
* Diferentes materiales: [Different Materials](https://www.shapeways.com/materials)
* Algunas bibliotecas de recursos que están a nuestra disposición porque alguien los ha creado y compartido en Internet:
    * [Gravity sketch](https://www.gravitysketch.com/)
    * [Vectary](https://www.vectary.com/)
    * [Tinkercad](https://www.tinkercad.com/)
    * [Thingiverse](https://www.thingiverse.com/)
    * [Beetle Blocks](http://beetleblocks.com/)
    * [Sahpe Ways](https://www.shapeways.com)

A partir de una plantilla los estudiantes elaborarán su propia lista de tecnologías preferidas siguiendo este esquema:

* Nombre de la tecnología.
* Materiales y fuentes asociadas a esta tecnología.
* Cómo esta tecnología puede ser útil.
* Un posible proyecto que se podría desarrollar utilizando estas fuentes.

## Segunda parte
El objetivo de esta sección es garantizar que los estudiantes comiencen a pensar en un pequeño proyecto o actividad que se pueda desarrollar mediante algunas tecnologías concretas y abiertas relacionadas con la fabricación digital. Consejo: la formadora o el formador deben asegurarse de que el proyecto es "pequeño" (está bien dimensionado).
El formador ayudará a los estudiantes organizando una lluvia de ideas en grupo, mediante algunos ejemplos anteriores y respondiendo a las siguientes preguntas:
* Reto / proyecto a desarrollar
* Por qué: qué problema / oportunidad tendrá
* Quién debería implicarse? (Perfiles)
* Con qué herramienta / fuente / máquina / material

Después de la lluvia de ideas, el formador facilitará el trabajo en grupo / s reducidos, donde los estudiantes son invitados a pensar en estas ideas y seleccionar una para desarrollarla.
Al final de la sesión, los participantes presentarán su idea concreta al grupo general y harán / recibirán algunos comentarios sobre sus propuestas. El formador hará algunas observaciones generales sobre las ideas: si tenemos alguna información reflejada en cada columna, entonces tenemos un gran punto de partida de una iniciativa de FabLab.
## Trabajo en casa (trabajo autónomo)
Continuar planificando un mini proyecto o actividad donde se deberían utilizar diferentes máquinas.

Ahora tenemos algunas ideas y algún punto de partida. Al Trabajo en casa (trabajo autónomo) de la sesión anterior pensamos en la gestión de los voluntarios, el retorno social de ser voluntario y la metodología de Aprendizaje y Servicio. ¿Cómo podemos aplicar estos conceptos sobre el proyecto que hemos trabajado durante la sesión de hoy?