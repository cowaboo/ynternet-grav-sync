---
title: '¿Qué es un Fablab?'
objectives:
    - 'Proporcionar una introducción a lo que es un Espacio Maker (Makerspace) y a las diferentes tipologías (FabLab, Hackspace, Hacklabs, GrootSpace ...), a la cultura y movimiento maker, los espacios colaborativos y la fabricación digital.'
    - 'Descubrir un nuevo mundo de oportunidades y objetivos.'
    - 'Motivar a los y las participantes para afrontar la cultura abierta y promover las actividades y espacios makers en sus propias organizaciones.'
lenght: '90 min'
---

## Introducción al movimiento Maker
El objetivo de esta primera parte es resaltar los conocimientos previos que tengamos. También se invita a compartir información sobre el contexto o su participación en la sesión (motivación, objetivos personales, etc.) respecto a este tema.

El formador/ora organiza un rompecabezas para trabajar algunos de los conceptos clave. Comienza para preguntar al grupo qué sabemos en relación a los conceptos siguientes (véase más arriba). Si se trata de una clase presencial, el formador tomará nota de las respuestas del tablero, subrayando las que sean más adecuadas. Si se trata de una formación en línea, se organiza un pequeño debate a través de un foro.

- ¿Qué sabemos?
- ¿Qué es?
- ¿Cuando comienza?
- ¿Que es un Makerspace?

## Primeras informaciones
Esta parte de la sesión está diseñada para proporcionar información básica sobre el tema. Se utilizarán los vídeos propuestos u otros vídeos que el formador / a seleccionará con antelación. Esta sección normalmente garantiza que los participantes podrán identificar qué es un Fablabs, que es un espacio de creación y tener una idea más concreta al respecto.

Miramos los vídeos y comentamos ...

- FabLab: [¿Qué es un FABLAB?](https://youtu.be/L_15Rc79T_c)  
- [¿Qué es un Fab Lab?](https://www.youtube.com/watch?v=lPF7zDSf-LA)  
- [¿Qué es un MakerSpace?](https://www.youtube.com/watch?v=wti6FMvDAE4)  
- [Espacio Santiago Maker](https://youtu.be/i4OWsLY5RBo)

## Nuevos conceptos
Los espacios Maker son un concepto de espacio donde se desarrollan proyectos según la filosofía del movimiento maker. Surgieron inicialmente como espacios de encuentro para personas que querían aprender, investigar y compartir sus conocimientos sobre fabricación digital.
Son un reflejo directo de lo que eran los HakerSpaces de la década de 1970, cuando aparecieron los primeros computadores personales, aplicados al mundo del hardware abierto.

El concepto de la Espiral Creativa de Mitchel Resnick es uno de los conceptos más significativos en cuanto al movimiento maker aplicado a las metodologías de aprendizaje.

El o la formador / a presentará la Espiral Creativa de Mitchel Resnick. Destacará las ideas principales mediante la presentación de Wikislides y explicará qué son las espirales creativas y sus ventajas en el campo de la educación (y propondrá leer el artículo disponible aquí como trabajo posterior)

Después de esto, el grupo se desplazará para identificar los puntos fuertes y débiles posibles de un MakerSpace. Los participantes trabajarán por parejas y trabajarán con una plantilla básica (dos columnas: puntos fuertes y débiles) o utilizando diferentes materiales para "representar" puntos fuertes y débiles (visualmente), mediante cartones y lápices de colores, por ejemplo, o piezas de lego...) Después de 15 minutos de trabajo, compartirán el resultado con otros participantes.

El formador se asegurará de que se exploren algunos conceptos: fabricación digital, colaboración, aprender haciendo y DIT (Do it Yourself), la cultura creadora.

Si la actividad se ejecuta de manera virtual, la sugerencia es hacer la actividad de manera individual y sugerir construir la lista mediante una herramienta infográfica (como [Venngage](https://es.venngage.com/templates) o [Piktochart](https://piktochart.com/formatos/Infographic)), compartiendo el resultado través de un foro o lista de correo electrónico.