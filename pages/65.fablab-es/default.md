---
title: 'Cómo crear un Fablab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: es
Materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
length: '10 horas'
objectives:
    - 'Identificar qué es un FabLab y la cultura que está involucrada'
    - 'Ser capaz de iniciar una iniciativa FabLab, comprender cuáles son los primeros pasos y qué tecnologías son más adecuadas para comenzar'
    - 'Detectar algunas tecnologías y herramientas abiertas que sean adecuadas en un entorno FabLab. Conocer, detectar y saber cómo utilizar fuentes de información que proporcionan recursos que son útiles en el marco de acción de un fablab'
    - 'Explorar las comunidades de práctica de Fablab que utilizan tecnologías y herramientas abiertas'
    - 'Explorar el territorio para construir el vínculo entre empresa - universidad - ciudadanía - administraciones públicas (promueva hélice cuádruple)'
    - 'Permitir al participante proporcionar información, recursos, modelos a las comunidades de práctica de Fablabs. Diseño abierto. Prestar soporte a las personas participantes de un FabLab durante su proceso de aprendizaje y producción'
skills:
    'Palabras clave':
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            HackLab: null
            'Diseño abierto': null
            'Hardware abierto': null
            'Software abierto': null
            'Hágalo usted mismo (bricolaje) (DIY)': null
            'Aprender haciendo (LBD)': null
            PBL: null
---

## Introducción
La cultura Fablab está estrechamente relacionada con la cultura DIY y Maker, así como con el FLOSS; siempre es algo "en construcción" y para iniciar un Fablab no necesitas tener muchas herramientas o máquinas, sino la actitud correcta. Un Fablab es un concepto, y comenzar un Fablab es trabajar bajo una filosofía concreta.
Este módulo presentará a los participantes el concepto de lo que se considera un Fablab y qué tipo de actividades se pueden realizar allí.
## Contexto
Cuando uno imagina un Fablab, piensa en un espacio de creación, que es colaborativo, que utiliza tecnologías abiertas, que aprovecha los recursos del territorio más cercano y también las fuentes de información y recursos de Internet. Un espacio en continuo crecimiento, con algunas tecnologías que pueden aumentar con el tiempo. Un espacio de concurrencia de personas que llegarán con una actitud abierta y curiosa. Un punto de partida que, si se inserta y se relaciona con la comunidad local, atraerá diferentes tipos de audiencias y será rico en creatividad y promoverá la autonomía y el aprendizaje individual e individualizado de las personas.

Un Fablab es mucho más que un espacio equipado para la fabricación digital a pequeña escala. Es un concepto, es un punto de vista, es una nueva forma de generar riqueza y empoderamiento dentro de una comunidad. Equipado con tecnología y herramientas, abiertas y gratuitas, Fablab es un espacio capaz de generar una dinámica estrechamente vinculada al aprendizaje colaborativo, el intercambio y la cultura de los bienes comunes y el libre intercambio de recursos e información. Dentro del marco de un Fablab, especialmente si tiene una orientación comunitaria, se generan nuevas dinámicas productivas colaborativas; es parte del "Do it Yourself" (DiY, hazlo tú mismo) y de la Maker Culture.

Crear un Fablab no requiere grandes recursos financieros. Comenzar es el primer paso y en este módulo analizaremos este primer paso y planificaremos, todos juntos, las primeras etapas de un Fablab.
## Sesiones
### Primera sesión: ¿Qué es un Fablab?
Fabricación digital, colaboración, aprender haciendo y DiT (Do it Yourself). La cultura creadora.
### Segunda sesión: Diseño comunitario y colaboración
El potencial de gestionar un fablab en una comunidad. Necesidades personales y comunitarias versus producción en masa.
### Tercera sesión: Tecnologías Fablab
Explorar el potencial del uso de hardware y software de código abierto en el marco de un Fablab. Descubrir las tecnologías adecuadas para comenzar una iniciativa FabLab.
### Cuarta sesión: Tecnologías Fablab
### Quinta sesión: Primeros pasos...
Creando tu propio Fablab. Diseño colaborativo de una nueva comunidad Fablab.
### Sexta sesión: Revisión por pares del diseño de la comunidad Fablab
Primeras actividades a desarrollar.