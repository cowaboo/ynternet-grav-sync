---
title: 'Diseño colaborativo de un nuevo FabLab comunitario'
objectives:
    - 'Proporcionar una introducción al diseño colaborativo de un nuevo FabLab comunitario.'
    - 'Facilitar protocolos, metodologías y guías para detectar e identificar agentes digitales de innovación social'
    - 'Descubrir dinámicas del trabajo comunitario'
    - 'Motivar a abrir y promover las actividades y un espacio de creación maker a la propia entidad.'
lenght: '90 min'
---

## Introducción
El docente tratará las preguntas que puedan plantearse después de que la sesión anterior y también al Trabajo en casa (trabajo autónomo). Después se introducirá el contenido y las actividades principales de la sesión.	 

## Primera parte: introducción a la complejidad
El objetivo de esta sección es introducir varios conceptos clave a tener en cuenta a la hora de trabajar en la creación de un FabLab comunitario o maker Space.

Visualizamos este vídeo como introducción: [How to Create a Makerspace](https://youtu.be/BEUDgfsy-8k). Presenta varios conceptos clave: desde equipamientos, recursos, cuestiones legales, hasta implicar a la comunidad.
A continuación, los participantes (trabajo en grupo / s, con la misma configuración que en la última sesión) elaborarán una lista de conceptos clave que afectan a sus proyectos grupales. Después de trabajar en ellos, la información se compartirá con el gran grupo. Evidentemente, también se compartirá la idea principal del proyecto / iniciativa, por lo que el grupo podrá evaluar y completar la información fácilmente.
## Segunda parte: *let's do it* “de verdad”
Ahora es hora de soñar con construir el concepto de un espacio donde poder desarrollar "todos" los proyectos que los participantes han introducido. Lo haremos en grupo y, para hacerlo más fácil, pensaremos que tenemos un patrocinador público que proporcionará "suficiente dinero" para el primer año.

El formador pedirá a los participantes que piensen en estos proyectos (como si alguien pidiera un espacio y recursos para trabajar) y que empiecen a planificarlos. Utilizaremos un mapa mental para describir el primer escenario de planificación. Consejos: deberíamos tratar de ser realistas.

- ¿Cuántos proyectos podemos iniciar el primer año? ¿Como podemos priorizarlos, con qué criterios?

- ¿Cuántas personas deberían estar implicadas desde el primer momento? ¿Qué perfiles?

- ¿Qué tecnologías se aplicarán? ¿Cuántos de ellos se pueden compartir entre los proyectos que iniciaremos el primer año?

- ¿Quién debe ser consciente / estar informado de nuestras ideas y planes?

- ¿Quien más nos podría patrocinar en esta iniciativa?

- ¿Como nos aseguramos de que la comunidad aproveche nuestra propuesta?

## Trabajo en casa (trabajo autónomo) :
Reflexionamos sobre lo que hemos hecho durante la sesión.