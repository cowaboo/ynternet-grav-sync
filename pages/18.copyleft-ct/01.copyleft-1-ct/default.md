---
title: 'Copyquè? Comprensió i ús de les llicències FLOSS'
length: '180 min'
objectives:
    - 'Fer un mapa i explorar els conceptes principals de les llicències FLOSS'
    - 'Entendre la interdependència de diverses llicències FLOSS i no FLOSS'
    - 'Descobrir els arguments ètics, legals, socials, econòmics i d’impacte a favor i en contra de les llicències FLOSS'
    - 'Decidir quina llicència / plataforma / és més útil per a les persones i el seu projecte i la seva comunitat'
---

## Introducció
Descripció de l’activitat
→ Grup de debat
El formador o formadora iniciarà la introducció al mòdul preguntant als participants sobre les seves experiències actuals i anteriors sobre drets d’autor i copyleft. Les respostes es documentaran i es reutilitzaran per personalitzar el material de la formació.

## L’aparició del copyleft
Les xarxes constitueixen la nova morfologia social de les nostres societats, i la difusió de la lògica en xarxa modifica substancialment les operacions i els resultats en processos de producció, experiència del poder i cultura." diu el filòsof Manuel Castells. Aquest procés és alhora una amenaça i oportunitat al voltant de "incloure els béns de la ment" (vegeu: J. Boyle, [The Public Domain: Enclosing the Commons of the Mind](http://www.thepublicdomain.org/download), New Haven, CT: Yale University Press, 2008, 221). 

Una contraposició per tancar els béns de la ment és l’aparició de llicències copyleft. Una llicència copyleft utilitza llei de copyright per mantenir l'obertura de la propietat intel·lectual. La primera llicència, les Public License General GNU (GNU GPL), va néixer a partir del moviment FLOSS. Un sistema de llicències de contingut obert de Creative Commons (CC) va fer per a contingut cultural el que GNU GPL va fer per programari; mantenir la seva obertura. L’objectiu d’aquestes llicències és maximitzar l’ús d’informació alhora que es minimitzen els costos de transacció.

##Identificar una llicència copyleft (treball en grup)
En aquesta activitat, revisarem els resultats de la documentació de la sessió introductòria per distingir una llicència copyleft.

##Treball a casa
Els participants haurien d’estudiar l’article [Wikipedia Copyleft](https://en.wikipedia.org/wiki/Copyleft) en l’idioma que prefereixin i després completar la part d’enllaços externs, amb un exemple propi.

##Referències
* Una col·lecció de recursos relacionats està disponible en espai social - col·lectiu de marcadors: [Diigo - eCulture](https://groups.diigo.com/group/e_culture).
* [CNU - Licenses - Copyleft](https://www.gnu.org/licenses/copyleft.html).
* [Llicència ètica de codi obert](https://firstdonoharm.dev).