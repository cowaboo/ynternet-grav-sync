---
title: 'Llicències FLOSS per a educació oberta, l’art i l’aprenentatge col·lectiu'
length: '180 min'
objectives:
    - 'Proporcionar una anàlisi crítica sobre la importància de les llicències FLOSS per a educació oberta, l’art i l’aprenentatge col·lectiu.'
    - 'Examinar l’ús i els avantatges de les llicències FLOSS en educació i art'
    - 'Afavorir la participació activa i creativa dels estudiants mitjançant llicències FLOSS'
    - 'Motivar els participants a adoptar llicències FLOSS en les seves activitats d’educació actuals i futures'
---

## Introducció
→ Grup de debat: introducció a la sessió (tema: llicències, educació o art)
Els participants faran una pluja d’idees sobre les llicències i les iniciatives d’educació al seu voltant. Les respostes es documentaran i es reutilitzaran per personalitzar el material de la formació.

## Llicència FLOSS en art i educació
El 1999, la novel·la Q va aparèixer amb el nom de [Luther Blissett](https://en.wikipedia.org/wiki/Luther_Blissett_(nom_de_plume)), conegut anteriorment com a “ the collective moniker”  per un projecte de mitjans de comunicació d'humor italià. Aquest relat al·legòric de la subcultura italiana en forma de thriller històric situat a Itàlia del segle XVI, Q es va convertir en un best-seller nacional número 1 i, posteriorment, va aparèixer en traduccions francesa, alemanya i anglesa. Evidentment, les vendes no van patir en absolut que el fet que l’empremta del llibre permetés a tothom copiar-lo lliurement amb finalitats no comercials. El llibre no va ser publicat per una editorial alternativa, sinó per les editorials ben consolidades Einaudi a Itàlia, Editions du Seul a França i Piper a Alemanya, entre d'altres que aparentment no els importava utilitzar models tradicionals de distribució amb drets d'autor per a un publicació prometedora.

Un altre exemple, d'una manera diferent, és Europeana (un dipòsit d'educació oberta i art). El Marc de llicències Europeana, per exemple, estandarditza i harmonitza informació i pràctiques relacionades amb els drets en virtut d'un acord d'intercanvi de dades personalitzat. El DEA estructura la relació entre Europeana i els seus proveïdors de dades. El DEA estableix que Europeana publica metadades que rep dels seus proveïdors de dades sota els termes de la Dedicació del domini públic universal de Creative Commons Zero (CC0). Llegeix més informació sobre CC0 i les directrius d’ús de dades.

El coneixement i la creativitat són recursos que, per a ser fidels a ells mateixos, han de mantenir-se lliures, és a dir, és una cerca fonamental que no està directament relacionada amb una aplicació concreta. Crear vol dir descobrir allò desconegut, significa inventar una realitat sense fer cas al realisme. Així, l’objecte(tiu) d’art (de l'art) no equival a l’objecte d’art acabat i definit. Aquest és l’objectiu bàsic d’aquesta Llicència d’Art Gratuït: promoure i protegir la pràctica artística alliberada de les regles de l’economia de mercat.
Llicència d’Art Lliure versió preàmbul 1.2: [Free Art License Preamble version 1.2](http://www.copyleftlicense.com/licenses/free-art-license-(fal)-version-12/view.php)

## Descripció de l'activitat
→ Mira la xerrada de Gwenn Seemel: En defensa de la imitació [In defense of imitation](https://www.tedxgeneva.net/talks/gwenn-seemel-in-defense-of-imitation/).
Estudiar, documentar i discutir els seus arguments. Explora com l'artista es viu. Crea un document comú amb tots els participants, compartiu-lo amb altres. 
(alternativa)  
→ Escenaris Col·lectius per a OEP -  Escenaris d’Educació Oberta (grup de treball)
L’activitat se centrarà en una selecció exemplar d’aplicacions del paradigma en pràctiques educatives obertes.

## Treball a casa
Busqueu al dipòsit obert [Zenodo open repository](https://zenodo.org/) i trobeu els vostres tres articles preferits sobre FLOSS i educació. Documentar i / o publicar-ne un comentari.

## Referències
* Guia per a llicències de contingut obert [Guide To Open Content Licenses](https://monoskop.org/images/1/1e/Liang_Lawrence_Guide_to_Open_Content_Licenses_v1_2_2015.pdf) by Lawrence Liang.
* [Steal this Film and projects](http://www.stealthisfilm.com/Part2/projects.php)
* Biella Coleman,  La creació social de la llibertat productiva: pirateria de programari lliure i la redefinició del treball, l'autoria i la creativitat: [The Social Creation of Productive Freedom: Free Software Hacking and the Redefinition of Labor, Authorship, and Creativity](http://www.healthhacker.com/biella/proposal2.html/).
* Unglue (v. t.) 1. Pagar a un autor o editor per haver publicat un llibre electrònic de Creative Commons: *To pay an author or publisher for publishing a Creative Commons ebook* [unglue.it](https://unglue.it/)
* [DIS](https://dis.art/): El futur de l’aprenentatge és molt més important que el futur de l’educació. DIS és una plataforma de streaming d’entreteniment i educació - edutainment. Inclou un llistat d'artistes i pensadors líders per ampliar l’abast de les converses claus que s’arrosseguen a través de l’art contemporani, la cultura, l’activisme, la filosofia i la tecnologia. Cada vídeo proposa alguna cosa - una solució, una pregunta - una manera de pensar la nostra realitat canviant.