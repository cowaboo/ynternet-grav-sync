---
title: 'Les llicències FLOSS en escenaris de la vida real'
length: '240 min'
objectives:
    - 'Proporcionar exemples de llicències FLOSS en escenaris de la vida real'
    - 'Conèixer la connexió amb el marc de la política europea de drets d’autor'
    - 'Analitzar l’impacte de l’ús de les llicències FLOSS'
    - 'Motivar les polítiques educatives en l’àrea FLOSS'
---

## Introducció
Descripció de l’activitat
Discussió en grup sobre "Com està llicenciat el vostre projecte avui?"
El formador iniciarà la introducció al mòdul preguntant als participants sobre les iniciatives de llicències al seu voltant, que provenen majoritàriament d’altres organitzacions o de la UE. Les respostes es documentaran i es reutilitzaran per personalitzar el material de la formació.

## FLOSS en escenaris de la vida real
Quina llicència tenen aquests projectes? 
* [Farm Hack](https://farmhack.org/tools)
* [Open Motors](https://www.openmotors.co/)
* [Fold-it](https://fold.it/portal/)
* [Open Insulin Project](https://openinsulin.org/)
* [Wikihouse](https://www.wikihouse.cc/ )
* Qualsevol article de la wikipedia 

Més llicències i conceptes:
[The Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License)
[El caso de Copyfarleft](https://wiki.p2pfoundation.net/Copyfarleft)

## Un marc de política de la UE per a les llicències FLOSS
La presentació i anàlisi de les principals iniciatives de política de la UE FLOSS, inclosa la Llicència Pública de la Unió Europea (la "EUPL") s'aplica a l'obra (tal com es defineix a continuació), que es proporciona en els termes d'aquesta llicència. Es prohibeix qualsevol ús de l'obra, que no sigui autoritzat per aquesta llicència (en la mesura que aquest ús estigui cobert per un dret del titular dels drets d'autor de l'obra). Compatibilitat i interoperabilitat
[https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12](https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12)
[https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t](https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t)

## La vostra llicència per salvar el món
Uniu-vos a un grup per dissenyar i compartir la vostra proposta de política de llicències, la que penseu que farà que el món sigui un lloc millor per viure. Trieu un tema i proveu de trobar maneres i solucions per a llicenciar productes amb la vostra intenció. Utilitzarem tècniques de disseny de plataformes per documentar idees i reptes d’aquestes llicències.

## Treball a casa
Vegeu al vídeo següent: [Free software, free society: Richard Stallman at TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM), a continuació, publiqueu a la secció de comentaris un altre vídeo que expliqui - demostra arguments similars. Compartiu la vostra reflexió sobre el nou vídeo     

## References
* "Com triar una llicència per al vostre propi treball". Laboratori de llicències i compliment de programari lliure: "[How to choose a license for your own work](https://www.gnu.org/licenses/license-recommendations.html#libraries)". Free Software Foundation's Licensing and Compliance Lab.
* Programari lliure, societat lliure: Richard Stallman a TEDxGeneva 2014: [Free software, free society: Richard Stallman at TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM)
* [The open data institute](https://theodi.org)