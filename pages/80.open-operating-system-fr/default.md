---
title: 'Système d''exploitation ouvert comme transition vers FLOSS: GNU / Linux'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: fr
length: '9 heures and 30 minutes'
objectives:
    - 'Fournir un environnement de sauvegarde aux participants afin de migrer vers GNU / Linux dans leurs espaces de travail'
    - 'Créer un réseau de soutien entre les participants'
    - 'Favoriser la création d''un réseau peer-to-peer, apporter un soutien aux différents acteurs du territoire impliqués dans la diffusion du PLL'
materials:
    - 'Ordinateur avec connexion Internet'
    - 'Mettre à jour le navigateur Web'
    - 'Un PC où vous pouvez faire les tests d''installation (vous pouvez perdre les données enregistrées)'
    - '1 ou 2 installations USB pour OS'
skills:
    'Mots clés':
        subs:
            'GNU / Linux': null
            Linux: null
            OS: null
            'Migration des systèmes d''exploitation': null
            'Administration du système': null
---

## Introduction
L'infrastructure technologique d'un télécentre ou d'un centre de formation numérique est le point de départ pour développer ses activités pédagogiques. La conception de l'infrastructure définira la politique d'intervention que nous pourrons développer et les thèmes sur lesquels nous pourrons travailler, ainsi que les perspectives de notre travail.

Que nous travaillions sur des projets d'alphabétisation numérique de base ou dans des spécialités numériques plus complexes (édition vidéo numérique, monde du fabricant, compétences avancées en codage ...), nous avons fini par enseigner des produits et technologies numériques spécifiques. La transition vers le travail avec des outils technologiques gratuits (FLOSS) peut (doit être?) S'accompagner d'un travail, également, avec des systèmes d'exploitation gratuits, offrant la possibilité de comprendre la technologie comme une connaissance ouverte et partagée au-delà des options fermées offertes par le marché.
## Contexte
Objectifs de la séance :
* Fournir un environnement de sauvegarde aux participants afin de migrer vers GNU / Linux dans leurs espaces de travail.
* Créer un réseau de soutien parmi les participants.
* Promouvoir la création d'un réseau peer-to-peer, pour accompagner les différents acteurs du territoire impliqués dans la diffusion du PLL.

Méthodologie :
* **Délibération**: Groupes de discussion et forums de discussion.
* **Exécution**: pratiques réelles en informatique fournies par les étudiants.

## Séances
### Première séance : Nos besoins
Dans cette séance, nous travaillerons à partir de l'analyse des besoins de chaque télécentre / centre de formation numérique, et nous le situerons dans son contexte. Vous cartographierez les différents agents avec lesquels vous pourrez vous relier et vos rôles: administration publique, communautés de développement de logiciels libres, universités, autres centres de formation. Aussi, les outils FLOSS seront précisés, ceux qui peuvent répondre aux besoins pédagogiques de chaque espace.
### Deuxième séance : Les étapes à donner
En fonction des besoins détectés, nous concevrons un plan de migration, en choisissant le meilleur SO qui puisse répondre aux besoins. Différents mécanismes de migration seront analysés et évalués, en fonction des besoins de chaque centre. Une installation d'essai sera effectuée et des directives de maintenance et de mise à jour seront élaborées pour s'assurer que le centre reste en bon état.
## Troisième séance : Et maintenant quoi?
Une fois l'installation terminée et les règles de maintenance et de mise à jour établies, nous apprendrons comment installer de nouveaux programmes en toute sécurité.
### Quatrième séance : Éveil et diffusion
Il est nécessaire de travailler sur un plan pédagogique, par exemple en expliquant comment cela fonctionne aux personnes participant à chaque centre et les principales raisons d'utiliser ce type de technologie. Nous étudierons et développerons des stratégies de diffusion des systèmes FLOSS et adapterons le matériel didactique de chaque centre pour poursuivre sa mission principale: la culture numérique ou la formation aux compétences numériques.