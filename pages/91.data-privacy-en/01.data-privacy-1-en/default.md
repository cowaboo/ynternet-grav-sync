---
title: 'Basic principles of Data Protection'
objectives:
    - 'link to the training of open educational resources (OER) https://www.jedecide.be/'
    - 'discover the ethical, legal, social, economic and impact arguments; for and against the exploitation of personal data for commercial or political manipulation'
    - 'what are the basic rules and good habits for themselves and their community'
    - null
---

## Introduction
Introduction: “Big Data” is a term which designates digital data sets produced by the use of new technologies for professional or personal purposes. These interconnected databases have become so large that they exceed human analytical capacities, and conventional IT capacities for information management. Several systems for observing our daily lives cross their information, we find corporate data (e-mail, documents, databases, etc.); but also data from content sensors published or consulted on the web (images, videos, sounds, texts, etc.); private exchanges on social networks (Instagram, Snapchat, Facebook, Twitter, ...); and more and more geo-localized biometric data (facial recognition, fingerprint, smart meters, smartphone, connected object, etc.). The GAFAMs (Google, Appel, Facebook, Amazon, Microsoft), and other merchants, then "organize" the internet according to "formatted content bubbles" which filter and adapt the information displayed on our screens according to the data collected about us. The exploitation of digital data on a large set of individuals makes it possible to establish different user profiles. The objective is to influence and predict the behavior of the user to offering them personalized content, generally of a commercial nature, particularly likely to interest them (reference: Le Chaînon "E-santé…tous connecté ?" , n ° 41, December 2017). This type of profiling is akin to manipulation that threatens our free will. This is why it has been regulated in Europe since 2018 by the General Data Protection Regulation (GDPR).

We take advantage of this module to present the [new general EU regulation on data protection](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation). An educational tool to make young people aware of the basic principles of this regulation has been developed, in collaboration with Child Focus (BE) and the Privacy Commission (BE). This educational kit presents, among other things, the general data protection regulation (GDPR), which defines the rules to be observed by any person or company processing personal data. It aims to give people the means to decide for themselves what information they want to share and how.

The educational support, "[I decide](https://www.jedecide.be/)", is intended for young people, parents and teachers. We invite multimedia organizers to adapt and use these resources with their learners as well. [The tool is available online here on the site](https://www.jedecide.be/). This tool is only available in Dutch and French. Te content can be translated with [Deepl](https://www.deepl.com/translator) for your use or use the automatic translate function in your browser as most of the text is easy to translate. If the translation of the tool fails from French to English you could also try [the same website in Dutch](https://www.ikbeslis.be/). The contents of this tool is based on the toolkit of the Canadian Privacy commission and can be found in English here: [Privacy Commissioner of Canada](https://www.priv.gc.ca/en/privacy-topics/information-and-advice-for-individuals/privacy-and-kids/) - Privacy and kids).
## Phase 1
Everyone opens their computer portable or if possible group screening. We watch together on the internet two short introductory films with simple visual information accessible to all (young and adult).
* [Video 1](https://www.1jour1actu.com/info-animee/cest-quoi-la-protection-des-donnees-personnelles/).
* [Video 2](https://www.jedecide.be/les-parents-et-lenseignement).

By asking learners to individually list (in writing so as not to forget them) the information that they think is important or new to them.

English variations:
* [Example 1](https://www.priv.gc.ca/media/3609/gn_e.pdf).
* [Example 2](https://www.schooleducationgateway.eu/en/pub/resources/tutorials/brief-gdpr-guide-for-schools.htm).

Pooling by round table, listing on the table the important information for the participants. Visually organize the information on a visual medium by grouping the information by theme.
Phase 1 summary: The role of the data protection authority, which results from the reform of the Privacy Commission, was explained to them.
### What is the General Data Protection Regulation (GDPR)?
The General Data Protection Regulation (GDPR) is a new European regulation that lays down rules that public authorities, companies and all other organizations must comply with when processing personal data.
The GDPR offers citizens better protection when processing their personal data. This new legislation is based on existing privacy legislation but strengthens a number of rules, clarifies or constitutes an extension to existing legislation. The new Regulations came into force on May 25, 2018.
The training centers process a lot of personal data.
Examples include data from (former) students and their parents, teachers and supervisory staff. As part of the processing of personal data, Digital Public Spaces (EPN) and training centers will be subject to the GDPR.
### What are the main changes?
* Greater responsibility on the part of the data processor. The training center must itself demonstrate that it processes personal data according to the rules of the GDPR.
* The training centers must designate an interlocutor who knows this legislation and who can help to implement it within the school.
* The organizing authority must keep a register of treatment activities. A register of processing activities includes, among other things, which personal data is processed by the school (or the learning center), for what purposes it is used, where it comes from and with whom it is shared.
* The legislation provides for an obligation to notify in the event of data leaks. For example, in the event of sensitive personal data leaks, you, as a school (or a learning center), are obliged to notify the data breach to the Data Protection Authority (and possibly to the persons concerned).
Reinforced control. In the event of non-compliance with the GDPR, the Data Protection Authority may impose sanctions as well.
## Phase 2
We work in a sub-group on the specific themes which interest the learners, all the material, of the educational resources proposed below, should not be covered. The idea here is to raise awareness of the challenges and propose good, simple and accessible practices. We fly over the good  elementary habits to increase the protection of our data. The content is valid for young people and adults, and also gives an introduction to the risks linked to social networks.

We suggest that the group choose according to their interests, each for themselves, **one of the 8 themes offered on the [educational platform](https://www.jedecide.be/les-parents-et-lseignement)**. We let people discover the content of the proposed themes individually online. We group together the people who have chosen the same theme in subgroups, so that they prepare together a summary presentation of important practical information. Each group presents to the others the key information of the chosen theme. The subgroups present the content as they want (invite them to be creative in the form of sharing). Ask them to identify the roles in the sub-group, each must have a role during the presentation (for example: x reporter, x scribe, x timekeeper, x visual support, x recording, x presenter, ...). Little matter the role, it is up to the group to choose and to give a role on stage to everyone during the presentation (mini theater style max 3 min). It is of course also a moment of sharing to discover other good practices than those presented on the educational platform, the objective is to generate a collective intelligence of the themes, and to share it with others.
### 8 Themes proposed
#### 1.- [Connected toys](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys)
For example:
* A connected teddy bear or doll who knows all our secrets.
* A drone which takes our neighbors in photos.
* A connected watch which records all our movements.

The holidays are fast approaching and the period of shopping is in full swing. Have your children sent you their gift list? Shopping time has come! Remote control cars, adventure books, colorful slimes,… you no longer know where to turn. And all of a sudden, you find yourself face to face with these state-of-the-art high-tech toys, called “connected toys”. These Internet-connected toys are very attractive and highly coveted by children, but do you really know how they work, how your personal data is processed, as well as that of your children, how to use it or how to configure it to secure it? In our thematic file you will find some “intelligent” advice on how to use it.
* [Drones](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/drones)
* [Connected speakers](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/enceintes-connectees) 
* [Watch Connected](https://www.jedecide.be/les-parents-et-lenseignement/jouets-connectes/montre-connectee)
* [Robot and doll](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/robot-et-poupee-connectes)
* [Tablets](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/tablettes)

#### 2.- [Online](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne)
**Privacy online**: the Internet is everywhere, it's "the best". Play, listen to music, share photos and videos, chat or search for information for school, ... All this, young people do online. It is therefore not for nothing that they are called digital generation.

But despite the fact that young people are growing up with the Internet, there are some aspects that are no less complex for them too. They therefore do not always see the risks of the Internet, especially with regard to privacy! Not protecting one's "digital life" enough means running the risk of other people, businesses or even public authorities accessing a lot of personal information. When this happens, the Internet suddenly becomes much less "top". 

In this thematic block, you will find tips to protect your privacy and that of our young people on the Internet.
* [An environment respectful of privacy: how?](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/un-environnement-respectueux-de-la-vie-privee).
* [Protect your profile](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/protegez-votre-profil).
* [Use strong passwords!](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/utilisez-des-mots-de-passe-forts).
* [Think before posting](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/reflechissez-avant-de-poster).
* [Secure your Internet connection](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/securisez-votre-connexion-internet).
* [Social networks: the advantages](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/reseaux-sociaux-les-avantages).
* [Social networks: the disadvantages](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/les-reseaux-sociaux-les-inconvenients).
* [Cyber-harassment](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/cyber-harcelement).

#### 3.- [Privacy at school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole)
**Privacy at school**: The right to privacy does not stop at the school gate! The facilitators must also observe several rules in order to respect the privacy of each student. Both parents and teachers have many questions about this: what information can our organization request? How long can she keep them? What does school do with my child's photos? Can the students film the lesson and vice versa, can the teacher film the class?

This thematic block provides an answer to these questions and many others on the learner's private life.
* [Student's file](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/le-dossier-de-leleve).
* [Photos and videos at school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/photos-et-videos-lecole).
* [Cameras at school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/les-cameras-lecole).
* [GSM and smartphone at school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/gsm-et-smartphone-lecole).
* [Alcohol and drugs](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/alcool-et-drogue).
* [New gadgets](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/nouveaux-gadgets).
* [Teacher's address](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/ladresse-de-lenseignant).
* [My teacher, my Facebook friend?](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/mon-prof-mon-ami-facebook)
* [Meeting of former](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/reunion-danciens).
* [Recruiting new students](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/recrutement-de-nouveaux-eleves).

#### 4.- [Photos and videos](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos)
**Photos & Videos**: The use of all kinds of image forms is evolving at lightning speed. Today, many people have a camera in their pocket ... their smartphone! Nothing is more trivial now than taking photos and filming and the dissemination of these images has never been easier: social networks and applications aim to share information and images. This rapid development took everyone by surprise and profoundly changed our way of life and our society. Before, communication was mainly through words. Today, we are overwhelmed by a flood of images on everything and anything: ourselves, our children, our family, our sports club, our school, our class.

But isn't this development a danger to our privacy? Can we refuse to be filmed, can we prohibit the dissemination of images on which we appear or are we forced to follow the trend of the dissemination of images online?
* [The principle: always ask for consent](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/le-principe-demandez-toujours-le-consentement).
* [How to obtain consent?](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/comment-obtenir-le-consentement)
* [Images of school activities](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/images-dactivites-scolaires).

#### 5.- [Sexting](https://www.jedecide.be/les-parents-et-lenseignement/sexting)
**Sexting**: Sexting is the practice of sending messages of a sexual nature by sms, via social media or telephone services such as Skype. These messages can therefore contain text but also a photo or video of a sexual nature. This type of sexual message is simply called a "sext"

The sexting is not in itself so serious. Young people like to experiment, this is also the case on a sexual level. But they need to be aware that sexting also carries privacy risks. This is where things are likely to get out of hand, and not only among young people ... adults too are trapped. In this thematic block, you will find more information on sexting. What is that ? What to do in case of problems? Some useful tips.
* [Sexting: what are the risks for privacy?](https://www.jedecide.be/les-parents-et-lenseignement/sexting/sexting-quels-sont-les-risques-pour-la-vie-privee)
* [Is sexting punishable?](https://www.jedecide.be/les-parents-et-lenseignement/sexting/le-sexting-est-il-punissable)
* [Sophie's story](https://www.jedecide.be/les-parents-et-lenseignement/sexting/lhistoire-de-sophie).
* [Tips for teachers](https://www.jedecide.be/les-parents-et-lenseignement/sexting/astuces-pour-les-enseignants).
* [Tips for parents](https://www.jedecide.be/les-parents-et-lenseignement/sexting/astuces-pour-les-parents).
* [Help, my naked photo has been posted !!!](https://www.jedecide.be/les-jeunes/sexting/au-secours-ma-photo-denudee-ete-diffusee)
* [I received a naked photo, what should I do?](https://www.jedecide.be/les-jeunes/sexting/jai-recu-une-photo-denudee-que-faire)
* [Tips to avoid problems](https://www.jedecide.be/les-jeunes/sexting/astuces-pour-eviter-des-problemes).

#### 6.- [Sharenting](https://www.jedecide.be/les-parents-et-lenseignement/sharenting)
**Sharenting**: Information for young parents and "connected" grandparents. Sharenting: even if this word is still unknown to you, the practice which it designates is probably not. You probably do it very often or maybe just once in a while. And more than one does it even without any restraint!

Share + Parenting = Sharenting

Sharenting is simply sharing photos and videos of your children or grandchildren on social media, often without the consent of those concerned. We almost all have an account on some social network, be it Facebook, Instagram, Snapchat, WhatsApp, ... And any parent likes to immortalize the important events in the life of their offspring. Not a day of vacation, not a school party, not a sporting competition with our blond heads happens without being preserved in images. Images which are then cheerfully shared on social networks.
* [Think before you post!](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/reflechissez-avant-de-poster)
* [Why yes, why not?](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/pourquoi-oui-pourquoi-non)
* [Tips for practicing wise sharenting](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/conseils-pour-pratiquer-un-sharenting-avise).

#### 7.- [Smartphones & applications](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications)
**Smartphones & applications**: Today, many young people have a smartphone or a tablet. Send SMS, make videos, take selfies, test apps, go to Facebook, play, surf the Internet… all of this is possible and very easy. But sometimes it is difficult to follow or understand all the new progress.
Young people use these technologies daily, even at school. This thematic block therefore offers parents and teachers basic information on the use of smartphones and applications as well as on the consequences for respecting privacy. You will also find tips on how to use your smartphone and tablet while protecting your privacy. In this way, you will be able to inform and help young people confronted with questions or problems on this subject.
* [Applications and personal information](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/applications-et-informations-personnelles).
* [As a parent, can I control my children's smartphone / tablet?](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/en-tant-que-parent-puis-je-controler-le)
* [Sharing the position](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/partage-de-la-position).
* [Health apps](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/les-applis-sante).
* [To further protect your privacy, encrypt your messages](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/pour-proteger-davantage-votre-vie-privee).
* [Viruses and tips](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/virus-et-astuces).
* [Privacy](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/astuces-de-protection-de-la-vie-privee-pour)

#### 8.- [eID](https://www.jedecide.be/les-parents-et-lenseignement/eid)
**eID applications**: From the age of 12, young people have a card electronic identity (eID)! But it's not just an identity card! Today, the card is used for a whole lot of things: as an access badge, a guarantee voucher, ... and even as a loyalty card. However, there are rules to follow! Read here why.
* [The electronic identity card](https://www.jedecide.be/les-parents-et-lenseignement/eid/la-carte-didentite-electronique).
* [When to show the eID?](https://www.jedecide.be/les-parents-et-lenseignement/eid/quand-faut-il-montrer-leid)
* [The eID as a loyalty card](https://www.jedecide.be/les-parents-et-lenseignement/eid/leid-comme-carte-de-fidelite).

#### (9.-) Additional educational resources
**English**:
* [GDPR for dummies](https://termly.io/resources/articles/gdpr-for-dummies/)
* [How to explain GDPR to a 5 year old](https://www.sysaid.com/blog/entry/how-to-explain-gdpr-to-a-5-year-old)

**French**:
* [123 Digit](https://www.123digit.be/)
* [Discover security on the internet](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet).
* [Animation frame to prepare your workshop](https://www.123digit.be/fr/accompagner/competences-numeriques-1#collapsedecouvrir-securite-internet).
* [Interactive training individually or collectively](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet-fiche).
* [Summary sheet to give to the learner](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet-1).
* [How to create a secure password?](https://www.123digit.be/fr/accompagner/competences-numeriques-1#collapsecreer-mot-passe-securise)
* [Animation framework to prepare your workshop](https://www.123digit.be/fr/ressources-pedagogiques/comment-creer-un-mot-de-passe-securise).
* [Interactive training individually or collectively](https://www.123digit.be/fr/ressources-pedagogiques/comment-creer-un-mot-de-passe-securise-1).
* [Summary sheet to give to the learner](https://www.jedecide.be/les-parents-et-lenseignement).
* [Educational support](www.jedecide.be).

## Phase 3
The trainer will start the collective conclusion to the module by asking participants about their current and previous experiences in data protection . Responses will be documented and then used to personalize the training material.
Possible conclusions:
**Think before you publish: don't put anything and everything online**
_The problem_: information published on the Internet is often difficult to delete.
Once information is found on the Internet (such as your opinion, a photo, a video, etc.), it is often very difficult to remove them. To protect your privacy online, it's best to be aware of it.

An example: _a photo that you posted on the Internet is uploaded by a friend. So there is now a copy of the photo on his computer's hard drive. Suppose now that your friend sends this photo to another friend who then in turn transfers it to other people, ... there are suddenly many copies of your photo.
This is how you lose control of your own personal information. How do I get all copies of the photo deleted? You do not know where the photo was saved. In such a situation, removing the original photo from the Internet is no longer useful. This does not allow you to delete the copies that have been saved on the devices of everyone who downloaded or received the photo._
It is therefore important to explain that there is a difference between what is said orally between friends and what one publishes on the Internet.

The solution: **think before posting - ask yourself 3 questions**
Before posting something on the Internet, ask yourself the following questions:
1. Is the information I put online intended for everyone?
If the information is not intended for everyone, it is better not to publish it on the Internet where it will be visible to all. Use website privacy settings to limit the number of people who can see your message, or simply don't post the information on the Internet.
2. Will I not regret later having put this information online?
A bad reaction, a humorous photo, ...? Keep in mind that once you have posted information on the Internet, it may no longer be possible to delete it. Be careful when you chat. Chat messages can be saved by the recipient or by the company that designed the chat application.
3. Is the information I want to post on the Internet personal information about someone else?
Never put online personal information such as photos, videos, an address, a telephone number, ... of other people without asking their consent beforehand. Indeed, if you publish this information on the Internet, it loses control of it. You probably wouldn't like it to happen to you either. In addition, it is even prohibited by law to share personal information of another person online without consent. So always ask for consent first!

Someone posted something without my consent, what can I do?
* Ask the person in question to remove the information from the Internet because it was published without your consent.
* Ask the manager of the website on which the information was posted to remove it on the grounds that it was published without your consent.
* Contact your local Data Protection Authority.

## Homework
### Intro: Did you know that a lot of people don't use privacy settings on social media? It is however essential. Information published on the Internet reaches many more people than that which is shared offline. Do not use the privacy settings, it runs the risk that unknown people on the other side of the world access for example the photos of your personal lives.
Please go check your security settings on your tools.
Please also list:
What are the benefits of social media for you?
What are the disadvantages of social media for you?
Possible answer to the question: Why are social networks so attractive?
The attraction of having a profile page on a social network is that it is an easy way to get in touch with friends, acquaintances, and even larger groups of people. Social networks also allow you to follow the actions of other people with a simple click of the mouse thanks to the flood of information that these social networks send to you, via photos, videos, status updates. ,…
Another feature that makes social networks so attractive is that they offer a virtual podium where you can place yourself in the spotlight, with the whole world as a potential audience. Do you want to display a sports service? A small update of your status or a simple tweet is enough: "Just run 15 km in 1h30". Still in the euphoria of your brilliant performance, you watch for the congratulatory messages posted in response to your message. "Well, there are still a lot of my friends who are impressed by my performance!".
### The advantages
If we look at the impact of social media on young people, we can say that in general, the use of social networks and social media certainly offers some advantages.
Social networks have many psychosocial advantages because they are a simple way for young people:
* To get in and stay in touch with friends and family.
* To develop their social capital (= the set of advantages that one derives from relationships and interactions with others).
* To forge their own identity.

In addition to these psychosocial advantages, the use of social networks also has several socio-cognitive and educational consequences:
* The development of creative skills under the impulse of producing and sharing images.
* The development of a critical mind and an open mind if the young person's online network is diverse enough to come into contact with several different opinions and perspectives. This stimulates the development of an opening towards other visions and opinions.
* Knowledge sharing.

What are the disadvantages of social networking sites?
As with many other things, social media must be used responsibly if you do not want to expose yourself to the possible risks involved in using it.

The possible risks of using social networks are:
* Exposure to unwanted content such as hate messages and violence.
* Cyber harassment.
* Loss of privacy due to loss of control over personal information.
Given that social networks are built on the principle of self-representation, implying that the users themselves reveal personal information, they are often accused of involving risks for the privacy of their users. A distinction can be made between the risks to privacy linked to identity and image damage and those linked to what advertisers, authorities and intelligence services do with personal information.
### Identity and image damage
The risks to privacy linked to identity and image damage can be largely controlled by respecting two instructions:
* use the confidentiality settings available on network sites social;
* don't put anything online. Think about what you post on the Internet.

Poor management of privacy settings effectively erases the boundary between private and public information. If you do not set these settings to a sufficiently high level of security, you run the risk that complete strangers may gain a very large overview of your privacy. Imagine, for example, that such people have free access to your holiday photos, to your friends list, that they know what your interests are, your hobbies, ...

Aside from managing privacy settings, thoughtful publication Personal information is also very important: “Think before you publish!”. Young people are often not aware of the reach of social networks (what is called the invisible public) and of the fact that the information posted online is often kept permanently (all those who have access to their profile can upload their photos and save or share them elsewhere, which suddenly means that a lot more people have access to your personal information!).
### What is published on the Internet can therefore often no longer be deleted!
Thoughtless images or comments often posted online may, out of context, continue to cause image problems even long after the publication, which could harm a person's chances, for example: get a job in the labor market. Everyone says things one day that they then regret having said. The online problem is that thoughtless posts often don't 'get forgotten' as simply, as is usually the case in the real world.

### Materials for further study:
* [Protect your profile](https://medium.com/@AxelUnlimited/how-to-protect-your-personal-information-on-social-media-c3aa6242f1cf).
* [Use strong passwords](https://howsecureismypassword.net/)!
* [Think before posting](https://www.kidscape.org.uk/advice/advice-for-young-people/dealing-with-cyberbullying/think-before-you-post/).
* [Secure your Internet connection](https://www.wired.com/story/secure-your-wi-fi-router/).
* [Cyber-harassment](https://www.kidscape.org.uk/advice/advice-for-young-people/dealing-with-cyberbullying/).

## References
* [Je Décide](https://www.jedecide.be/les-parents-et-lenseignement).