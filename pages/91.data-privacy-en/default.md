---
title: 'Data privacy culture: a FLOSS driven view'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Data privacy culture: a FLOSS driven view'
lang_id: 'Data privacy culture: a FLOSS driven view'
lang_title: en
length: '7 hours'
objectives:
    - 'Present the legal framework and raise awareness of the issues concerning the protection of usage data'
    - 'Raise awareness of the added value of free software to increase the protection of personal use data'
    - 'Allow the facilitator to locate their uses and the degree of respect for personal data in their organization'
materials:
    - 'Personal computer (smartphone or tablet connected)'
    - Internet
    - Bearmer
    - 'Paper and pens'
    - Flipchart
    - 'Mobile connection to Slidewiki Academy'
skills:
    Keywords:
        subs:
            'Protection of personal data': null
            'GDPR (General Data Protection Regulation)': null
            'Data privacy': null
            'Big data': null
            Surveillance: null
            Advertisements: null
            Manipulation: null
            'Influence behavior': null
            FLOSS: null
            Free: null
            'Open source': null
---

## Introduction
Although the use of personal data for commercial purposes is widespread, respect for privacy is considered by Europe as a fundamental right. Free software tries again and again to resist the "supposedly voluntary" surveillance of our private lives. To protect data privacy directly at the source, in the design of the tool, the FLOSS culture tries to hide nothing in the software that monitors us (and it proves this by giving access to the source code). This training scenario encourages practices that are aware of the challenges linked to data collection, by **presenting basic good practices and European legislation. Our goal is to invite learners to consider the possibility of using free or open source software to increase the security of their personal data. We would also like to allow the STEM trainer to better situate the limits of his professional practice to encourage respect for the personal data of their beneficiaries**. Overall, the training scenario provides a reminder of the European legislative framework, and of the challenges relating to the profiling of habits by monitoring personal uses for the purpose of advertising behavior manipulation.
## Context
The aim of the session is to involve learners in a conscious and practical manner in order to:
* Present the legal framework and raise awareness of the issues concerning the protection of usage data. The learner will be able to situate themselves legally in relation to the GDPR. He will understand the general issues and good habits related to the protection of personal data.
* Raise awareness of the added value of free software to increase the protection of personal use data. In order to consciously decide which platforms / tools / services are most respectful of the privacy and freedom of users.
* Allow the facilitator to locate their uses and the degree of respect for personal data in their organization. To encourage him to make an informed choice of rules and tools whose “Free” design allows more confidentiality.

The sessions will encourage the use of free software in non-formal adult education, by stimulating intentional participation in the FLOSS culture. The participants will carry out a critical analysis of the possibilities of using FLOSS tools in their daily life. By becoming aware of the importance of having a data protection policy in their own organization; to better secure their professional practices in digital inclusion.
## Sessions
### First session: Basic principles of Data Protection
This session will give participants the opportunity to explore the main concepts of legislation and the issues. We will link the concepts covered during the training to free educational resources (via URL links). We review the basics and good habits to have.
* Phase 1: Everyone opens their laptop. We watch together on the internet two short introductory films on the GDPR with simple visual information accessible to all (young and adult).

* Phase 2: We work in a sub-group on the specific topics of interest to the learners, all the subject of the educational resources offered should not be covered. The idea here is to raise awareness of the challenges and propose good, simple and accessible practices. It is the basis of basic habits to increase the protection of your data. The content is valid for young people and adults, and also gives an introduction to the risks linked to social networks.

* Phase 3: Group discussion to conclude session 1. The trainer will start the collective conclusion to the module by asking the participants about their current and previous experiences in data protection. Responses will be documented and then used to personalize the training material.
### Second session: Free software and data protection
The second session will aim to establish the historical origin and the state of the art of free software around data protection. FLOSS practices will be briefly presented (via the example of the framasoft and UBlock Origin tools). As a collective learning, think together about the choice of tools to increase the protection of data in our activities. Experimentation with a tool (UBlock Origin ad blocker).
### Third session: Locate and limit your practice (and the one of your organization) to increase data protection
Participants will carry out a critical analysis of the possibilities of use in their daily lives, and will develop an initial analysis on their own organization for compliance with legal guidelines in their digital inclusion practices.
These three sessions will allow a critical analysis of the legal framework and the issues. He will continue by examining the use and benefits of free digital technologies in non-formal education. By promoting awareness that facilitates the active and creative engagement of learners via FLOSS technologies, both as theory and practice.
## Presentation
<iframe src="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129390-1/" width="800" height="400"></iframe><p><a href="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129390-1/">Data privacy culture: a FLOSS driven view</a></p>