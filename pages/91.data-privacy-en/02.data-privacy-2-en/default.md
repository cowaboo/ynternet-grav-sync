---
title: 'Can free software help me better protect my privacy?'
objectives:
    - 'Shed light on the link between FLOSS and data protection'
    - 'Understand the strategy of FLOSS initiatives'
    - 'Fly over some available FLOSS tools (framasoft and uBlock Origin)'
    - 'Motivate educated people to a practice respectful of privacy and free will'
---

## Group discussion
The trainer begins the introduction of the module by asking questions of the participants about the FLOSS initiatives around them. Responses will be documented and then used to personalize the training material.
## FLOSS: history of a culture of sharing and data protection
The vast majority of defenders of free culture also take a stand for the [right to privacy](https://en.wikipedia.org/wiki/Privacy), [free access to information](https://en.wikipedia.org/wiki/Knowledge_commons) and freedom of expression on the Internet. Some activists have also been able to commit illegal acts during their fight, such as copyright infringement ([The Pirate Bay](https://en.wikipedia.org/wiki/The_Pirate_Bay), [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)) or the disclosure of confidential data which they consider to be of public interest ([Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden), [Alexandra Elbakyan](https://en.wikipedia.org/wiki/Alexandra_Elbakyan) with Sci-Hub).

Around 1960, the terms hacking  introduced by [MIT](https://en.wikipedia.org/wiki/Massachusetts_Institute_of_Technology). They refer to hacking and experimenting for fun. In 1969, [John Draper](https://en.wikipedia.org/wiki/John_Draper) managed to make free long distance calls when he whistled in the handset using a whistle that had the same tone as the American telephone network. This technique is named, by its creator [phreaking](https://en.wikipedia.org/wiki/Phreaking) and will inspire a new wave of [hackers](https://en.wikipedia.org/wiki/Hacker) computer. They will seek to modify and develop a first computer.

It was not until 1980 that the media began to publish articles on hacking. In particular with [Kevin Poulsen](https://en.wikipedia.org/wiki/Kevin_Poulsen), who succeeds in breaking into a network reserved for the army, universities and businesses. There was also the release of the film [Wargames](https://en.wikipedia.org/wiki/WarGames), the story of which centers on a hacker who manages to gain access to the computer system of the American army. The first [computer virus](https://en.wikipedia.org/wiki/Computer_virus) also appeared in these years. As a result, hackers are sometimes seen as dangerous people.

Many hackers started their activity by trying to break the anti-copy restrictions or by diverting the rules of the computer games before the generalization of Internet which then opened wider horizons to their activity. But when the media revealed at the beginning of [the 1990s](https://en.wikipedia.org/wiki/1990s) that the [Chaos Computer Club France](https://en.wikipedia.org/wiki/Chaos_Computer_Club#Fake_Chaos_Computer_Club_France) was a false group of hackers who worked in collaboration with the [gendarmerie](https://en.wikipedia.org/wiki/National_Gendarmerie), the community of French hackers rather turned to [free software](https://fr.wikipedia.org/wiki/Logiciel_libre) and many communities have emerged.
It was with the birth of the [internet](https://fr.wikipedia.org/wiki/Internet), in the 1990s, that we first spoke of [cybercrime](https://en.wikipedia.org/wiki/Cybercrime). The followers are divided at the beginning. There are [black hats](https://en.wikipedia.org/wiki/Black_hat_(computer_security)) who carry out criminal activities and [white hats](https://en.wikipedia.org/wiki/White_hat_(computer_security)) who do not want to harm but seek [computer vulnerabilities](https://en.wikipedia.org/wiki/Vulnerability_(computing)) to make them public and thus repair them.

One of the strengths of hacking is the **community aspect**. The community organization allows the extension of information sharing, the communities being interconnected the propagation of information is very fast. Community organization allows mutual aid between people, but also to people of young ages who wish to learn. The interconnection of people, who do not know each other, allows help that puts individuals on the same level, and this without value judgment.**_This aspect pushes for the generalization and sharing of knowledge without this being done on the basis of criteria such as "position, age, nationality or diplomas_**".

Nicolas Auray explains this anonymity as follows: “By delivering traces in an“ anonymous ”mode, [the _hackers_] would refuse to appear before political-judicial institutions, challenging the legitimacy of their verdict. They would reject what is still a little accepted by civil "disobedients": to recognize the legitimacy of the punishment and to allow oneself to be punished ".

**Possible conclusion**:
The community of “hackers” is linked to the creation and development of the FLOSS culture. Priority is given to sharing resources and information within communities of practice. But via “direct action” methods to fix the flaws in the system yourself, and by ensuring maximum protection of personal data at source in the “design” of the tools.
## Online download
Brief presentation of the technical principles and available FLOSS tools. Then choose the software to install and try, advise by the facilitators according to the specific needs of the learners. Voluntary practice as needed, try for example the Framasoft tools.
### The particularity of FLOSS tools for data protection
**Free software has several advantages**:
A great adaptation of free software to all types of hardware configurations and specific conditions of use "on demand".
Permanent scalability thanks to a dynamic community of passionate professional developers.

But for everyone, whether it is under LicenseGnuPublicLicensev3 (GPLv3), FreeSoftware (FS), or CeCiLL; this allows freedom for users to run, copy, distribute, study, modify and improve the software through access to source code.

**It also allows greater confidentiality of our data**:
On the one hand, on the development side, the ethics followed by the developer community ensures that the code will not contain functionalities intended to pass data through intermediary marketing processing servers . On the other hand, the security measures configured on servers and networks ensure the distribution of IP packets to the correct recipient.

![](datapriv1.jpg)

### Data protection and free software: preserving the private sphere with Framasoft
Sharing and communication services are becoming more and more essential to stay in touch with our loved ones, in a society where the family and social sphere is more and more delocalized. Free and well-configured services allow you to keep control over our exchanges.
To deal with this pervasive processing of our personal data, committed digital players are implementing free and often “gratis” alternatives to providing internet services to everyone.
The community [Framasoft](https://framasoft.org/) offers downloads of applications that are easy to install thanks to the documentation provided.

It provides free alternative services including, among others:
* Framadrive to have storage space, calendar and agenda in the cloud.
* Framasphère, a server for access to the social network ([Diaspora](https://diasporafoundation.org/) *for example).
* Framaestro to provide exchange services and for sharing resources.
* Framabee, as an alternative to the Google search engine.
* Framawiki, for writing documentation using SGML (Structured documentation is built upon structured elements: chapters, sections, paragraphs, etcetera, where all elements are clearly labeled for what they are: references, program output, etc. This allows for automatic processing of the documents, without waiting for AI systems. It encourages authors to concentrate on structure, which conveys meaning.).

### Our recommendations for secure use of the Internet
![](darapriv2.png)

* **Read confidentiality contracts** when using Google, Yahoo, Microsoft, to be sure of the private information you want to expose in the economic market.
* Access internet services that only use **encrypted connections via SSL / TLS**.
* Use **secure messaging services** for sending mail (avoid SMTP)
* Use an **OS insensitive to Malware**, RansomWares and viruses intrinsically.
* Set up a protection with a firewall, independent of the operating system like UFW for example, to avoid unwanted services.
* Always check the **source of download of files** using encryption keys, GPG, SHA256 ...
* Use an **independent browser like Firefox** from the Mozilla foundation.
* Create **sophisticated passwords** and use a keychain manager (KDE Wallet, KeePass).
* Participate as far as you can in these free alternatives to perpetuate their long-term development.

Source: https://blog.syloe.com/protection-des-donnees-et-logiciel-libres-nos-recommandations/
## Homework
Installation of uBlock Origin
**Practical exercises**
Why protect yourself from advertising? Advertising on the Web is used to [track](https://en.wikipedia.org/wiki/HTTP_cookie#Tracking) the user from site to site using [cookies](https://en.wikipedia.org/wiki/HTTP_cookie) from third-party sites, this due to the nature of the business model of internet advertising, the compensation for which is in exchange for data user’s personal information.

For users, blocking [advertising](https://en.wikipedia.org/wiki/Advertising) helps avoid distraction or even cognitive overload due to unwanted content in the web pages they visit. It also allows a faster and clearer display of web pages thus cleared of advertisements, a lower consumption of resources ([processor](https://en.wikipedia.org/wiki/Central_processing_unit), [RAM](https://en.wikipedia.org/wiki/Random-access_memory) and [bandwidth](https://en.wikipedia.org/wiki/Bandwidth_(signal_processing))) as well as a certain protection of [privacy](https://en.wikipedia.org/wiki/Privacy) by disabling tracking systems, [digital footprint](https://en.wikipedia.org/wiki/Digital_footprint), and analysis implemented by [advertising agencies](https://en.wikipedia.org/wiki/Advertising_network).

Users of phone [mobile](https://en.wikipedia.org/wiki/Mobile_telephony) data (using [GPRS](https://en.wikipedia.org/wiki/General_Packet_Radio_Service), [3G](https://en.wikipedia.org/wiki/3G), [4G](https://en.wikipedia.org/wiki/4G) and [5G](https://en.wikipedia.org/wiki/5G) standards), as well as users of the fixed internet in certain countries, who pay for the volume of data exchanged or have a limitation thereof, have a financial interest in blocking advertising [online](https://en.wikipedia.org/wiki/Online_advertising), including by blocking video and audio files, large consumers of bandwidth.

uBlock Origin
![](datapriv3.png)

uBlock Origin is an [extension](https://en.wikipedia.org/wiki/Plug-in_(computing)) [free](https://en.wikipedia.org/wiki/Free_software) for [web browsers](https://en.wikipedia.org/wiki/Web_browser) ([Mozilla](https://en.wikipedia.org/wiki/Firefox) Firefox, [Google](https://en.wikipedia.org/wiki/Google_Chrome) Chrome, [Opera](https://en.wikipedia.org/wiki/Opera_(web_browser)) and [Microsoft Edge](https://en.wikipedia.org/wiki/Microsoft_Edge)) responsible for [filtering](https://en.wikipedia.org/wiki/Content-control_software) the content [web page](https://en.wikipedia.org/wiki/Web_page) in order to block certain elements, particularly [banners](https://en.wikipedia.org/wiki/Web_banner), [advertising](https://en.wikipedia.org/wiki/Online_advertising). In addition to being [Ad Blocking software](https://en.wikipedia.org/wiki/Ad_blocking), uBlock blocks the collection of navigation data.

uBlock Origin has received awards from websites that deal with IT, due to a low consumer memory compared to other extensions with similar functionality. The purpose of uBlock Origin is to give the user the possibility of strengthening their choice of filtering the content of web pages.
In 2016, uBlock Origin continues to be actively developed and maintained by founder and lead developer Raymond Hill.
The tool is included in the list of [free software recommended by the French State as part of the overall modernization of its information systems](https://fr.wikipedia.org/wiki/Socle_interministériel_de_logiciels_libres).
## References
* [Protection des donnés](https://blog.syloe.com/protection-des-donnees-et-logiciel-libres-nos-recommandations/).
* [UBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin).
* [Free culture movement](https://en.wikipedia.org/wiki/Free-culture_movement).
* [Hacker](https://en.wikipedia.org/wiki/Hacker).