---
title: 'Situate your practice (and the one of your organization) to increase data protection'
objectives:
    - 'Allow a critical analysis of the importance of data protection in our practices'
    - 'Examine the limits of our practices in education and digital inclusion'
    - 'Encourage participants, and their organization, to engage actively and creatively through FLOSS technologies, both in theory and in practice'
    - 'Motivate participants to adopt a charter, in their organization, which specifies the data protection policy'
---

## Who are we?: Locate and limit our practices
Do you deal with one of these situations with your beneficiaries ? (If yes, how do you limit your intervention?)
* Make a payment via an app?
* Recover a password?
* Repair a hardware problem?
* Register on a social network?
* Learn to use a word processor?
* Send an email to the municipality?
* Install an app?
* Search for accommodation?
* Fill out a simplified tax return on “Taxe-on-Web”?
* Giving access to e-health information ?

**Intermediate conclusion**:
Do you face other delicate situations for data protection in your work?
If yes, which ones and how do you limit your intervention?
## Meet by organization to locate our uses
The activity will focus on defining the project of our own organization, to identify its practice and its objectives. In order to adapt the organization's personal data protection policy over the long term. 
In a sub-group (or individually if not possible): bring the workers of the same organization together. Study of the practical case of its own organization (as an example of a response, below, the responses for the ARC project "informaticien public").
### 1.- Strategy
What is the purpose and the audience of the system? 
* Support people experiencing a deep digital divide by meeting them

What is the organization's position on confidentiality?
* The public IT responds to all requests for daily use. 
* The experience will be positive (the user does not leave empty-handed).

### 2.- Who processes personal data and how?
Which worker or volunteer accompanies the user?
* An experienced facilitator from the EPN of the CRA (Full confidence and responsibility, Not SA, not doctor, not lawyer, not technician).
* A volunteer.

Status of the animator, how to approach the question of the user?
* Request verbalized by the user.
* Address confidentiality and private life (responsibility and autonomy).
* Explain the process (what we will do and what we will see).
* User agreement.
* Possibility of retract and operating charter.

### 3.- The organizational framework
What is the nature of the organization? 
* A lifelong learning association which opposes:
   * A policy that creates cultural poverty
   * A loss of power for people facing the market
   * A blind application of technological developments
* A Digital Public Space (EPN) labeled by the Brussels Region -Capital

Why the public comes?
* The user feels helpless in the face of IT, he does not know (or does not want to) follow training, he has concrete IT needs and questions.

### Exercises by organization: 
1. Describe yourself in 1, 2 and 3
2. According to to point "Meet by organization to locate our uses", classify requests (4) in "I treat" or "I do not treat".
Based on the list above: list the type of request processed and the type of request not processed (adding examples if necessary).

## Pooling and collective conclusion
As part of your digital inclusion work, do you benefit from a framework that limits and secures your interventions?
Sources: [Data protection at school in 7 steps](https://www.jedecide.be/sites/default/files/2018-06/La%20protection%20des%20donnees%20a%20lecole%20en%207%20etapes.pdf).

As a training center, how should you to process personal data?
1. The main principles which a school must comply with when processing personal data are as follows:
* Process personal data for specific and legitimate purposes. Use personal data only for this purpose. Example: for reasons of student administration, a school knows the home address of all students. It is not because a school has the data that it can be transmitted to another school or that the school can use it to distribute a list of addresses to parents.
* Be transparent when processing personal data. Explain why the school will process certain personal data.
* Any processing of personal data is only legitimate if it meets at least one of the legal foundations.
2. The main legal foundations on which a school can rely are:
	**Legal obligation**: if required by law, personal data can be processed. This is for example administrative data and student support.
	**The contract**: the personal data of students and teachers can be processed if it is necessary for the performance of a contract. For example: a photo ID of a student which is requested and which appears on a student card in order to allow him to have access to all kinds of services offered by the school.
	**Consent**: the consent of pupils or parents of pupils under the age of 16 is necessary for the processing of personal data for certain purposes. For example: to publish photos of students on the school website, consent will be required.
* A school does not process more personal data than is necessary to achieve the determined and legitimate purpose. For example: when registering a student, the school should not know the parents' income.
* Personal data processed by a school must be accurate and capable of being corrected. For example: if a student moves, the school can change the address.
* Do not keep personal data for longer than necessary. For certain student data, a legal retention period applies. Respect the legal retention period for personal data.
* As a school, take appropriate measures to protect personal data against unauthorized processing. The school authority is responsible for respecting these principles and must be able to demonstrate this.
Individual exercise: identify in the list above, what are the elements that your organization has already taken into account. Identify the points still to be worked on.

## Homework
We invite you to promote within your organization, the implementation of the General Data Protection Regulations.
The PDF above offers a 7-step methodology to allow educational organizations to set up the necessary procedures.

For this assignment, we do not ask you to carry out the 7 steps yourself, but to try to involve your organization in this process, by educating your managers and colleagues. And by proposing a way of doing things on the agenda of your work meetings.
* [Data protection at school in 7 steps](https://www.jedecide.be/sites/default/files/2018-06/La%20protection%20des%20donnees%20a%20lecole%20en%207%20etapes.pdf).

How to go about it:
STEP 1 - Inform and raise awareness
STEP 2 - Designate a DPO and a contact point at school
STEP 3 - Keep a register of processing activities
STEP 4 - Contracts with partners 
STEP 5 - Check whether consent is required
STEP 6 - Physical security and security of the ICT infrastructure, Points of attention regarding personal data
STEP 7 - Violations of personal data and obligation to notify
## References