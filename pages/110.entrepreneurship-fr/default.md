---
title: 'Entrepreneuriat en ligne avec les outils FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: fr
length: ''
objectives:
    - 'Planifier votre entreprise'
    - 'Cycle de vie du projet'
    - 'Introduction aux budgets'
materials:
    - 'Ordinateur et internet'
    - 'Idée de base des compétences FLOSS'
skills:
    'Mots clés':
        subs:
            'Commerce en ligne': null
            Entrepreneuriat: null
---

## Introduction
Les ventes en ligne n'ont jamais été aussi bonnes, et la prédiction est qu'elles continueront de croître jusqu'en 2021, comme le montrent les statistiques de «Statista - The Statistics Portal». Ce type de commerce évolue d'année en année et attire de nouveaux fans. Les boutiques virtuelles ne sont rien de plus que des vitrines pleines de produits disponibles à la vente, le marché de la vente en ligne a remarqué la nécessité d'investir dans des stratégies marketing pour savoir ce que les clients pensent de vos produits et suivre le processus après-vente.
Toute entreprise peut être mise en ligne, des cours de sport à l'apprentissage des langues, aux consultants en affaires, aux coachs personnels et personnels, traiteurs, artisans
Les achats en ligne sont l'une des activités en ligne les plus populaires au monde et en 2016 seulement, les ventes au détail de commerce électronique dans le monde ont atteint 1,86 billion de dollars américains. Pour l'année 2021, les ventes en ligne devraient atteindre 4,48 billions de dollars américains. Cela montre à quel point un entrepreneur et son entreprise peuvent bénéficier de l'ère numérique pour faire croître l'entreprise, quel que soit le domaine.

Le nombre total d'acheteurs numériques dans le monde augmente également. Il a augmenté de plus de 100 millions entre 2011 et 2012 et a continué de croître. Et les femmes y jouent un grand rôle grâce à des horaires flexibles permettant un meilleur équilibre vie professionnelle / vie privée. En 2012, une enquête de Greenfield a révélé que les femmes représentent 58% des dépenses en ligne aux États-Unis.
## Contexte
Ce module permettra à un utilisateur de savoir enseigner :
* Compétences pour développer une idée d'entreprise en une véritable entreprise.
* Enseignement de l'analyse SWOT.
* Aider les apprenants à définir et à comprendre le marché et les différents types de concurrence auxquels une entreprise est confrontée.
* Le cycle de vie du produit.
* Le marketing mix.
* Enseigner les éléments de base sur la budgétisation.

Méthodologie :
* Délibération: groupes de discussion et forums de discussion.
* Exécution: élaboration des plans par les étudiants.

## Séances
### Première séance : Développement et évaluation d'idées commerciales par Mind Maps Analyse SWOT
Dans cette séance, nous travaillerons sur l'analyse d'une idée d'entreprise à l'aide de l'analyse SWOT et des cartes mentales.
### Deuxième séance : Définir le marché
Cette séance se concentrera sur la segmentation des clients et la compréhension de la concurrence.
### Troisième séance : Budgétisation
Qu'est-ce qu'un budget et quelles sont les dix étapes pour suivre un budget.