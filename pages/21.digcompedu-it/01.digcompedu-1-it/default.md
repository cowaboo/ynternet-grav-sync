---
title: 'Il quadro europeo delle competenze DigCompEdu'
objectives:
    - 'Comprendere i principi di DigCompEdu per il miglioramento delle competenze digitali nell''insegnamento'
    - 'Incentivare l’utilizzo del Quadro Comune Europeo nell’ambito dell’educazione degli adulti'
---

## Introduzione
Discussione di gruppo
In sede di introduzione al modulo l’insegnante chiederà ai partecipanti della loro conoscenza del
quadro DigCompEdu e di altri quadri europei, più precisamente DigComp e DigCompOrg. Le risposte
verranno documentate e riutilizzate per personalizzare il materiale didattico.
## I principi di DigComEdu
La professione dell’insegnamento deve far fronte ad una domanda in rapida evoluzione, che richiede oggi un insieme più ampio e sofisticato di competenze rispetto al passato. L’ubiquità di dispositivi e applicazioni digitali, in particolar modo, impone agli educatori di affinare le proprie competenze digitali.

Il Quadro Europeo per le Competenze Digitali degli Educatori (DigCompEdu) è un quadro scientificamente solido che descrive cosa significa essere digitalmente competente per gli educatori.
Esso fornisce una generale struttura di riferimento come supporto allo sviluppo delle competenze digitali specifiche degli educatori in Europa. DigCompEdu è rivolto agli educatori di tutti i livelli dell’istruzione, dalla prima infanzia all’istruzione superiore e degli adulti, compresa l'istruzione e la formazione generale e professionale, l’istruzione per persone con esigenze speciali e i contesti di apprendimento non formale.

DigCompEdu descrive 22 competenze organizzate in sei Aree. Il focus non è sulle competenze tecniche. Il framework punta piuttosto a descrivere come le tecnologie digitali possono essere utilizzate per incentivare ed innovare l’istruzione e la formazione.

![](digcompedu.png)
Lo studio di DigCompEdu si basa sul lavoro precedentemente svolto per definire le Competenze digitali generali dei cittadini, e le Digitally Competent Education Organizzazioni formative digitalmente competenti  (DigCompOrg). Contribuisce all’Agenda delle Competenze per l’Europa recentemente approvata e all’iniziativa faro Europa 2020 “Agenda per Nuove Competenze per Nuovi Lavori”.
## Homework
I partecipanti dovranno esaminare il [documento](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu) e utilizzando la griglia relativa al livello di competenza, disponibile a [questo link](https://ec.europa.eu/jrc/en/digcompedu/framework/proficiency-levels), dovranno autovalutare il proprio livello.
Ai participanti viene richiesto di cominciare a riflettere sulle loro competenze in relazione al quadro di riferimento e su come tale documento potrebbe essere utile per migliorare le proprie competenze e quelle delle loro organizzazioni.
## Riferimenti
* [Framework DigCompEdu](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu)
* [Presentazione DigComEdu](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators)
* [Materiale di supporto DigCompEdu](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials)
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community)
* Allineare il quadro delle competenze degli insegnanti alle sfide del 21esimo secolo: Il caso del [Quadro Europeo delle Competenze Digitali per gli Educatori](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345)