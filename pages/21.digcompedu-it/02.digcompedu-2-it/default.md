---
title: 'Come utilizzare il quadro europeo per mappare le tue competenze e svilupparle'
objectives:
    - 'Individuare le proprie esigenze per diventare digitalmente competente nell’insegnamento'
    - 'Individuare gli strumenti e le risorse aperte disponibili, collegarli alle aree DIgCompEdu'
    - 'Progettare e pianificare il proprio miglioramento didattico e professionale'
    - 'Favorire l’uso di un quadro comune Europeo per gli educatori'
---

## Discussione di gruppo su “Dove pensi di voler migliorare”
In sede di introduzione al modulo l’insegnante chiederà ai partecipanti in quali delle sei aree si sentono meno competenti e vorrebbero migliorare, sulla base delle conclusioni della prima sessione.
## Individua le tue lacune e apprendi metodologie e strumenti
_“Gli insegnanti devono aggiornare i loro profili delle competenze per le sfide del 21esimo secolo. Le strategie di insegnamento devono cambiare, e così le competenze che gli insegnanti devono maturare per formare nel miglior modo gli studenti del 21esimo secolo… Collega l’implementazione delle competenze digitali di studenti e insegnanti, e può essere collegato al rafforzamento delle capacità istituzionali. Allo stesso tempo, il quadro è abbastanza generico e può essere applicato a diversi contesti educativi, inoltre permette l’adattamento in funzione dell’evoluzione delle possibilità e dei vincoli tecnologici”._ [Online Library](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345).

Presentazione ed analisi dei vantaggi che derivano dal mappare le proprie competenze (e quelle dell’organizzazione) rispetto al framework e introduzione ad alcuni strumenti e tecnologie open source individuati dalla partnership Open-AE.
## Costruisci il tuo personale piano di miglioramento delle competenze!
Come utilizzare il quadro europeo per pianificare la riqualificazione delle tue competenze? Con il support del tutor e dopo aver individuato le loro lacune ed i loro interessi, i partecipanti dovranno costruire il loro personale programma, secondo anche i contesti e i target groups con cui lavorano. Utilizzando una griglia fornita dal tutor, i partecipanti struttureranno il piano e successivamente individueranno strumenti e tecnologie open source che vorrebbero utilizzare per migliorare le proprie competenze. I risultati della ricerca del progetto Open-AE, ovvero gli strumenti open source esistenti a disposizione dei formatori.

Verrà inoltre presentato e discusso il caso di studio del  progetto DOCENT così da fornire ai partecipanti un esempio concreto di come il quadro europeo DigCompEdu può essere impiegato per modellare un quadro comune di competenze  necessarie agli educatori per l’integrazione della creatività digitale nell’ insegnamento.
## Compiti
Analizza le esigenze della tua organizzazione (staff e collaboratori) e implementa un piano di riqualificazione delle competenze. 
## Riferimenti
* [Framework DigCompEdu](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu).
* [Presentazione DigComEdu](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators).
* [Materiale di supporto DigCompEdu](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials).
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community).
* [Allineare il quadro delle competenze degli insegnanti alle sfide del 21esimo secolo: Il caso del quadro Europeo delle Competenze Digitali per gli Educatori](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345).
* [Progetto DOCENT](https://docent-project.eu/).
