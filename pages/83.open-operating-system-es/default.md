---
title: 'Un sistema operativo abierto GNU / Linux: la transición a FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: es
length: '9 horas y 30 minutos'
objectives:
    - 'Proporcionar un entorno seguro a los participantes para migrar a GNU / Linux en sus espacios de trabajo'
    - 'Crear una red de apoyo entre los participantes'
    - 'Promover la creación de una red de igual a igual, para brindar apoyo entre los diferentes agentes del territorio involucrados en la difusión de PLL'
materials:
    - 'Ordenador con conexión a internet'
    - 'Un navegador web actualizado'
    - 'Un PC donde se pueda hacer las pruebas de instalación (puede perder los datos guardados)'
    - '1 o 2 USB para instalaciones de SO'
skills:
    'Palabras clave':
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            'Sistema operativo': null
            Migración: null
            'Administración de sistemas': null
---

## Introducción
La infraestructura tecnológica de un telecentro o un centro de capacitación digital es el punto de partida para desarrollar sus actividades pedagógicas. El diseño de la infraestructura definirá la política de intervención que podremos desarrollar y los temas en los que podremos trabajar, así como las perspectivas de nuestro trabajo.

Ya sea que trabajemos en proyectos básicos de alfabetización digital o en especialidades digitales más complejas (edición de video digital, en el mundo de la creación, habilidades avanzadas de programación ...) terminamos enseñando productos y tecnologías digitales específicas. La transición a trabajar con herramientas tecnológicas gratuitas (FLOSS) puede ir acompañada de trabajar, también, con sistemas operativos gratuitos, ofreciendo la capacidad de entender la tecnología como un conocimiento abierto y compartido más allá de las opciones cerradas que ofrece el mercado.
## Contexto
Objetivos de la sesión: 
* Proporcionar un entorno seguro para que las personas participantes puedan migrar a GNU / Linux en sus espacios de trabajo.
* Crear una red de apoyo entre las personas participantes.
* Promover la creación de una red de igual a igual, para brindar apoyo entre los diferentes agentes del territorio involucrados en la difusión de PLL.

Se utilizarán dos metodologías diferentes durante este módulo:
* **Deliberación**: grupos de discusión y foros de discusión.
* **Ejecución**: prácticas reales en computadoras proporcionadas por estudiantes.

## Sesiones
### Primera sesión: Nuestras necesidades
En esta sesión trabajaremos a partir del análisis de las necesidades de cada telecentro / centro de capacitación digital y lo ubicaremos en su contexto. Mapearemos los diferentes agentes con los que podemos relacionarnos y sus roles: administración pública, comunidades de desarrollo de software libre, universidades, otros centros de capacitación. Además, se especificarán las herramientas de FLOSS que pueden responder a las necesidades educativas de cada espacio en concreto.
### Segunda sesión: Los pasos a seguir
En función de las necesidades detectadas, diseñaremos un plan de migración, eligiendo el mejor SO que pueda satisfacer las necesidades. Se analizarán y evaluarán diferentes mecanismos de migración, de acuerdo con las necesidades de cada centro. Se realizará una instalación de prueba y se desarrollarán pautas de mantenimiento y actualización para garantizar que el centro se mantenga en buena forma.
### Tercera sesión: ¿Y ahora qué?
Una vez que se complete la instalación y se resuelvan las reglas de mantenimiento y actualización, aprenderemos cómo instalar nuevos programas de manera segura.
### Cuarta sesión: Despertar y diseminar
Es necesario trabajar a nivel pedagógico, explicar a las personas que participan en cada centro cómo funcionan estas tecnologías y las principales razones para utilizar este tipo de tecnología. Estudiaremos y desarrollaremos estrategias para la difusión de los sistemas FLOSS y para adaptar el material didáctico de cada centro, de forma que se pueda continuar realizando la tarea principal: la alfabetización digital o la capacitación en competencias digitales.