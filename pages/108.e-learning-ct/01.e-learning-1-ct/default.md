---
title: 'Bases de l’aprenentatge electrònic - antecedents i evolució'
length: '210 min'
objectives:
    - 'identificar les perspectives històriques i l’augment de l’aprenentatge electrònic'
    - 'descobrir les principals eines d’aprenentatge electrònic'
    - 'explorar el potencial dels diferents programes d’aprenentatge electrònic'
    - 'avaluar els pros i els contres dels diferents programes d’aprenentatge electrònic'
    - 'descobrir els avantatges de l’aprenentatge electrònic per satisfer una àmplia gamma de necessitats d’aprenentatge'
    - 'identificar l’eficàcia dels entorns d’aprenentatge electrònic '
    - 'explorar el potencial de l’aprenentatge electrònic per promoure l’aprenentatge al llarg de la vida '
---

## Introducció
El formador iniciarà la introducció al mòdul preguntant als participants sobre els seus coneixements / experiència anteriors en el camp de l’aprenentatge en línia. Es demanarà als participants que se centrin en els avantatges i els desavantatges de l'aprenentatge en línia. 
## Fonaments d'aprenentatge electrònic
El 1999, durant el seminari CBT Systems als EUA, es va utilitzar per primera vegada una paraula nova en un entorn professional: "Aprenentatge electrònic". Associada a expressions com "aprenentatge en línia" o "aprenentatge virtual", aquesta paraula tenia la intenció de qualificar "una manera d'aprendre basada en l'ús de noves tecnologies que permetin accedir a una formació en línia, interactiva i de vegades personalitzada a través d'Internet o altres mitjans electrònics. (intranet, TV interactiva, etc.), per tal de desenvolupar competències mentre que el procés d’aprenentatge sigui independent del temps i del lloc ".

Avui en dia, gràcies a la introducció d’ordinador i internet, s’han ampliat les eines d’aprenentatge electrònic i els mètodes d'impartició. Avui en dia, l’E-Learning o aprenentatge electrònic és més popular que mai, ja que moltes persones s’adonen dels avantatges que l’aprenentatge en línia pot oferir. Les empreses també han començat a utilitzar e-Learning per formar als seus empleats, donant-los la possibilitat de millorar la seva base de coneixement del sector i ampliar els seus coneixements. A casa, els individus tenen accés a programes que els ofereixen la capacitat d'aconseguir titulacions en línia i enriquir la vida mitjançant un coneixement ampli.

![](elearning1.png)

[eFront learning](https://www.efrontlearning.com/blog/2013/08/a-brief-history-of-elearning-infographic.html)
## Exploració de les eines d’aprenentatge electrònic
Hi ha un munt de tecnologies relacionades amb l'aprenentatge electrònic. Es poden dividir en tres àrees clau: creació de contingut, LMS i TMS. Abans de començar a parlar de l’ús d’eines d’aprenentatge electrònic, és important conèixer la diferència entre les diferents eines que s’utilitzen per crear, desenvolupar i impartir contingut a través de l'aprenentatge electrònic.

* Autoria de contingut: una eina d'autoria de continguts d’aprenentatge electrònic és un paquet de programari que els desenvolupadors utilitzen per crear i empaquetar continguts d’aprenentatge electrònic que es poden lliurar als usuaris finals. Una eina d'autoria de contingut és una aplicació de programari usada per crear contingut multimèdia normalment per a la seva publicació a la World Wide Web.

* Un LMS (Learning Management System) és una aplicació de programari per a l'administració, documentació, seguiment, informes i impartició de cursos educatius, programes de capacitació o programes d'aprenentatge i desenvolupament. Per tant, un LMS és una solució de programari que crea i ofereix entorns d'aprenentatge personalitzats per als estudiants. Els LMS lliuren material educatiu en múltiples formes i a través d'una varietat d'activitats, com qüestionaris, contingut de vídeo i material al seu propi ritme. Els LMS se centren en el l'ensenyament i l''aprenentatge en línia, però admeten una varietat d'usos, actuant com una plataforma per al contingut en línia, inclosos els cursos, tant asíncrons com sincrònics. Un LMS lliurament i administra tot tipus de contingut, inclosos vídeos, cursos i documents. En els àmbits de l'educació primària, secundària i superior, un LMS inclourà una varietat de funcionalitats similars a les corporatives, però tindrà característiques com rúbriques, aprenentatge facilitat per docents i instructors, un panell de discussió i, sovint, un programa formatiu. Un programa d'estudis poques vegades és una característica en el LMS corporatiu, encara que els cursos poden començar amb un índex per donar als estudiants una visió general dels temes coberts.
El LMS es pot usar per crear contingut de curs estructurat professional. El professor pot afegir text, imatges, taules, enllaços i format de text; així com proves interactives, presentacions de diapositives, etc. Ajuda a controlar a quin contingut pot accedir un estudiant; a realitzar un seguiment de el progrés de l'estudi i involucrar l'alumne amb les eines de comunicació. Els mestres poden administrar cursos i mòduls, inscriure estudiants o configurar la autoinscripció, veure informes sobre els estudiants i importar estudiants a les seves classes en línia.

* El TMS (Training Management System) s'utilitza pels proveïdors de formació per tal de gestionar els aspectes comercials de les seves operacions formatives. Inclou la gestió de dates i sessions de formació, les inscripcions i el pagament en línia, la promoció de cursos i la comunicació amb les persones inscrites.

## Diferents tipus d’aprenentatge electrònic
Com millor els nostres aprenentatges electrònics s’ajustin a les nostres necessitats i hàbits d’aprenentatge, més fàcil serà per a nosaltres beneficiar-nos de les eines d’aprenentatge electrònic.
Alinear el nostre enfocament d'aprenentatge electrònic segons les nostres necessitats és crucial per treure el màxim partit al vostre entorn d'aprenentatge. Els programes d'aprenentatge electrònic poden adoptar moltes formes, com ara:
* Aprenentatge en línia mixt: és un enfocament a l’educació que combina materials educatius en línia i oportunitats d’interacció en línia amb mètodes tradicionals basats en aules. Requereix la presència física tant del professor com de l’alumne, amb alguns elements de control de l’alumnat (temps invertit, el lloc, etc.) Permet la retroalimentació immediata i l’anàlisi dels resultats que poden ajudar els estudiants a identificar els seus errors, a les zones febles i a mantenir-se involucrats. Els avantatges de l'aprenentatge mixt comporten la interacció entre grups més grans, la interacció cara a cara, l'aprenentatge autònom i el cost reduït.
* Aprenentatge en línia asíncron: aquest tipus d’aprenentatge electrònic no depèn ni de la ubicació ni del temps. L'aprenentatge electrònic asincrònic es gestiona millor mitjançant un sistema de gestió d’aprenentatge (LMS) que  permet fer un seguiment del progrés de cada aprenent. Generalment, la interacció es produeix a través dels blocs i els taulers de discussió que substitueixen l’element presencial o la reunió de classe. Els usuaris reben el contingut i els detalls rellevants del treball realitzat, així com les avaluacions. L'aprenentatge electrònic asíncron és un enfocament centrat en els aprenentatges. L’aprenent decideix quan vol abordar una nova peça d’aprenentatge electrònic, quan vol fer una pausa i quina part d’aprenentatge electrònic vol saltar perquè el coneixement / competència ja està assolit. 
* Aprenentatge en línia sincrònic: es produeix en temps real, cosa que significa que tant els aprenents com l’entrenador s’han de reunir alhora en una aula virtual. La trobada entre els estudiants i el facilitador, independentment de la seva ubicació, és un dels majors avantatges de l’aprenentatge electrònic sincrònic en comparació amb l'anterior, sempre que hi hagi una bona connexió a Internet. D’aquesta manera es produeix un estalvi de recursos (en termes de temps i costos de desplaçaments) tant per als aprenents com per al dinamitzador. 

![](elearning2.png)

[MDI Training](https://www.mdi-training.com/blog/blog/elearning/)
## Aprenentatge permanent
Els desenvolupaments tecnològics han contribuït considerablement als importants canvis que s'han produït en gairebé qualsevol sector de la nostra vida, inclosa l'educació. En un món tan globalitzat, hi ha una demanda creixent de noves habilitats i capacitats. “Les habilitats TIC són una necessitat imprescindible en el mercat global i un aprenentatge permanent i flexible és necessari per mantenir-se competitius. L’educació oberta i a distància (ODE) és prou flexible per donar respostes al repte de l’aprenentatge al llarg de la vida a la societat global ”(Bodil Ask, Sven Ake Bjorke, UNU / Global Virtua University, Noruega).
## Treball a casa (autònom)
Per tenir una millor idea dels pros i els contres de l’ensenyament en línia, es demana als participants que mirin alguns recursos i que prenguin notes mentre vegin els vídeos:
* Avantatges i desavantatges de l’aprenentatge en línia  [Online learning advantaged ad disadvantages](https://www.youtube.com/watch?v=tcb9puhA3fo).
* Els avantatges de l'aprenentatge electrònic [The e-learning advantages](https://www.youtube.com/watch?v=nzV1NmhC7ik).

Després, es demana als estudiants que generin una seva llista de pros i contres.

## References
* [History of E-Learning](http://www.leerbeleving.nl/wbts/1/history_of_elearning.html).
* [Introduction to eLearning, Course Authoring, LMS](https://www.youtube.com/watch?v=nEYAWU5fQOs).
* [What are content authoring tools and how can we use them?](https://www.talentlms.com/elearning/what-is-a-content-authoring-tool).
* [LMS vs CMS vs LCMS…What’s the Difference?](https://www.lessonly.com/blog/lms-vs-cms-vs-lcms-whats-difference/).
* [What is a Training Management System?](https://elearningindustry.com/training-management-system-tms).
* [LMS -Wikipedia](https://en.wikipedia.org/wiki/Learning_management_system).
* [Types of E-Learning](https://www.youtube.com/watch?v=XL5gZYqHO3M).
* [E-Learning ways](https://elearningways.com/what-is-e-learning-why-does-it-matter-lets-explore/).