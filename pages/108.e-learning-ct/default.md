---
title: 'E-Learning i FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: ct
length: '9 hores i 30 minuts'
objectives:
    - 'Reconèixer els avantatges de l''E-Learning en educació'
    - 'Explorar el principal sistema de gestió d''aprenentatge de codi obert: MOODLE'
    - 'Descobrir i beneficiar-se de la comunitat Moodle'
materials:
    - 'Ordinador personal (telèfon intel·ligent o tauleta connectada a Internet)'
    - 'Connexió a Internet'
    - Projector
    - Moodle
skills:
    'Paraules clau':
        subs:
            'Aprenentatge permanent': null
            'Sistema de gestió d''aprenentatge (LMS)': null
            'Aprenentatge asincrònic': null
            Moodle: null
            MOOC: null
---

# Introducció
_E-Learning explota tecnologies interactives i sistemes de comunicació per millorar l'experiència d'aprenentatge. Té el potencial de transformar la manera com ensenyem i aprenem a tots els àmbits. Pot elevar els estàndards i ampliar la participació en l'aprenentatge permanent. No pot reemplaçar a les persones docents, però juntament amb els mètodes existents pot millorar la qualitat i l'abast del seu ensenyament i reduir el temps dedicat a la gestió. Pot permetre a cada estudiant participant assolir el seu potencial i ajudar a construir una força laboral educativa capacitada per fer canvis. Fa possible un sistema educatiu veritablement ambiciós per a una futura societat d'aprenentatge millorada_
**Towards a Unified e-Learning Strategy, The DfES e-Learning Strategy Unit, 2003.**

Aquest escenari de capacitació proporciona una introducció a les metodologies i eines d'E-Learning. Tenint en compte que les eines d'e-learning redissenyar l'entorn educatiu, les sessions estan dissenyades per oferir una visió general dels antecedents de l'aprenentatge electrònic, les seves característiques i els mètodes clau.
La formació analitzarà alguna de les avantatges de l'aprenentatge en línia. En particular, permetrà a les estudiants explorar el potencial de l'E-Learning com una eina per fomentar la col·laboració en línia. De fet, eLearning pot ser una eina efectiva tant per a estudiants com per a educadors, especialment per promoure la col·laboració en línia (plataformes d'aprenentatge electrònic). Les persones participants provaran el funcionament de la plataforma d'aprenentatge Moodle per treballar i aprendre col·laborativa i cooperativament a través de fòrums, wikis, glossaris, activitats de bases de dades i molt més. La capacitació proporcionarà a les alumnes una comprensió crítica de l'procés d'aprenentatge.
## Context
Els canvis evolutius en la tecnologia educativa han canviat el concepte d'ensenyament i aprenentatge tal com el coneixem.
Certament, hi ha molts beneficis per a l'ensenyament / aprenentatge en un entorn en línia. E-Learning pot fer que l'aprenentatge sigui més simple, més fàcil i més efectiu. A més de adequar-se a diferents estils d'aprenentatge, l'e-Learning també és una forma de proporcionar una ràpida implementació de la formació. També és una eina rellevant en termes de col·laboració i desenvolupament comunitari. De fet, els processos d'e-Learning no han de ser un viatges individuals. A través de recursos com fòrums i tutorials en directe, les persones participants poden tenir contacte amb altres persones en la comunitat d'aprenentatge. El compromís amb altres persones promou la col·laboració i la cultura de l'equip, el que té beneficis més enllà de l'entorn de capacitació.
A la fi de les sessions, les participants podran fer servir i beneficiar-se d'Moodle, una de les principals plataformes d'aprenentatge de codi obert que ofereix un poderós conjunt d'eines centrades en l'alumnat i en els entorns d'aprenentatge col·laboratiu que potencien tant l'ensenyament com el aprenentatge.
### Sessions
### Primera sessió: Conceptes bàsics d'E-Learning: antecedents i evolució
L'e-Learning utilitza la tecnologia per ajudar i millorar l'aprenentatge. El propòsit de l'e-Learning és permetre que les persones aprenguin sense assistir físicament a un entorn educatiu tradicional. Per comprendre millor com l'e-Learning beneficia tant a les persones educadores com a les estudiants avui dia resulta molt útil conèixer els seus antecedents. El terme 'e-Learning' existeix des de 1999, quan es va utilitzar per primera vegada en un seminari de sistemes de TCC. L'expert en tecnologia educativa Elliott Maisie va encunyar el terme "e-Learning" el 1999. No obstant això, els principis darrere de l'aprenentatge electrònic han estat ben documentats al llarg de la història, i fins i tot hi ha evidència que suggereix que existien formes primerenques d'aprenentatge electrònic des del segle XIX. Avui, l'e-Learning és més popular que mai.

La sessió també ofereix una visió general d'algunes de les tecnologies involucrades en e-Learning, com la creació de contingut, LMS i TMS. A la "era de la informació", l'aprenentatge permanent es considera clau per a l'èxit continu de la societat moderna. L'e-Learning és considerat per molts com la millor solució viable per facilitar la posada a disposició dels recursos necessaris per a promoure l'aprenentatge permanent. Quins factors han facilitat que e-Learning s'hagi convertit en la forma més popular d'impartir formació avui dia? Per què utilitzar E-Learning? Aquesta sessió ofereix una visió general de l'abast potencial i els avantatges de l'e-Learning.
### Segona sessió: 'A punt per començar a aprendre en línia! Usant l'e-Learning per promoure la educació oberta
Aquesta sessió proporcionarà una visió general àmplia del que pot fer Moodle. Explorarà l'ús i els beneficis de Moodle, un dels sistemes de gestió d'aprenentatge (LMS) de codi obert més populars. La capacitació proporcionarà la informació bàsica necessària perquè els alumnes comencin amb la plataforma d'aprenentatge i es beneficiïn de l'assistència en línia i de l'intercanvi d'idees de les persones usuàries de tot el món.

A la finalització de les sessions, les persones participants podran fer servir i beneficiar-se d'Moodle, una de les principals plataformes d'aprenentatge de codi obert que ofereix un poderós conjunt d'eines centrades en l'alumne i entorns d'aprenentatge col·laboratiu que potencien tant l'ensenyament com l'aprenentatge.