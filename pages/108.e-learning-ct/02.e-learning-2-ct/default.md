---
title: 'Preparat per iniciar l’aprenentatge electrònic! L’ús de l’aprenentatge electrònic per promoure l’educació oberta'
length: '360 min'
objectives:
    - 'proporcionar una visió general de la història, desenvolupament i filosofia de Moodle'
    - 'reconèixer els principals beneficis de Moodle'
    - 'explorar les principals característiques de Moodle'
    - 'utilitzar tecnologies digitals obertes per obtenir suport de la comunitat Moodle'
    - 'fomentar la implicació a la comunitat en línia i el desenvolupament de programari de codi obert'
---

##  Discussió en grup sobre Moodle
El formador iniciarà la introducció al mòdul preguntant als participants sobre la seva experiència en el camp del sistema de gestió de l'aprenentatge de codi font obert per a l'educació i / o la formació laboral.
## Introducció al Moodle LMS: Llibertat per aprendre
Moodle és una plataforma activa y en evolució, iniciada per l’educador i programador informàtic australià Martin Dougiama.

“Em comprometo a continuar amb el meu treball a Moodle i a mantenir-lo obert i gratuït. Crec profundament en la importància de l’educació sense restriccions i l’ensenyament apoderat, i Moodle és la principal forma de contribuir a la realització d’aquests ideals".

És el primer sistema de gestió d'aprenentatge de codi font obert (LMS) publicat al públic el 2002. Moodle 1.0 va ser llançat oficialment el 2002 i es va originar com a plataforma per proporcionar als educadors la tecnologia per proporcionar aprenentatge en línia en entorns personalitzats que fomentin la interacció, la investigació i col·laboració. Des de llavors hi ha hagut sèries constants de nous llançaments afegint noves funcions, una millor escalabilitat i un millor rendiment.
Avui en dia, Moodle és el sistema de gestió d’aprenentatge més popular i més utilitzat del món. Amb 100 milions d’usuaris (i en creixement) i més de 100.000 llocs Moodle desplegats a tot el món, aquesta plataforma d'aprenentatge electrònic fàcil d’utilitzar serveix a les necessitats d’aprenentatge i de formació de tot tipus d’organitzacions en més de 225 països del món.
## Per què Moodle és el sistema de gestió d’aprenentatge més utilitzat?
A més de ser molt personalitzable i flexible, Moodle mai deixa de millorar. De fet, en ser un projecte de codi obert, Moodle és un esforç col·laboratiu i compta amb el suport d’una forta comunitat global. Així, els desenvolupadors de tot el món poden accedir al codi i modificar-lo per millorar el programari i fer-lo més segur.

Originalment dissenyat per a l'educació superior, Moodle s'utilitza no només en centres de secundària, escoles primàries, organitzacions sense ànim de lucre, empreses privades, per professors independents i fins i tot per famílies a l'àmbit de l'educació domèstica. Des de l'educació, Moodle ha arribat al món corporatiu, i ara també està guanyant popularitat en el sector de la salut. 
## Característiques bàsiques de Moodle
Introducció a:
* Interfície
* Panell
* Eines i activitats col·laboratives
* Calendari tot en un (all-in-one)
* Gestió de fitxers
* Gestió de textos
* Notificació
* Seguiment del progrés

## Contribuir al desenvolupament del programari de codi obert 
Al ser una plataforma d’aprenentatge de codi obert, Moodle ens ofereix les possibilitats de contribuir a la plataforma de moltes maneres diferents, com ara:
* Assistència a l'usuari: obtenit suport de la comunitat, compartit idees i coneixer els altres a: [the Community Forum](https://moodle.org/course/)
* Prova: ajudar a provar i informar d'errors a través del Moodle Tracker
* Usabilitat: realitzar algunes proves d’usabilitat i informar dels resultats
* Traducció: ajudar amb la traducció de Moodle, inclosa la contribució de traducció / correcció
* Social Media: ajudar a conscienciar el projecte Moodle i compartir les novetats i esdeveniments del món Moodle

A més, els usuaris tenen la possibilitat d’unir-se a [Moodle Users Association](https://moodleassociation.org/), una organització sense ànim de lucre dedicada a contribuir al desenvolupament princial de Moodle. L’associació d’usuaris de Moodle dóna suport al creixement de Moodle proporcionant una veu forta i unida als usuaris, donant direcció i recursos per als nous desenvolupaments.
## Treball a casa (autònom)
Des del lloc web de Moodle, aneu a la secció [demo section](https://moodle.org/demo/) i exploreu [Mount Orange school Moodle](https://school.moodledemo.net/mod/page/view.php?id=45).  A partir d’aquí, utilitzant les credencials (inicieu la sessió com a 'professor', contrasenya 'moodle'), podeu navegar dins d'una plataforma Moodle, configurar algunes activitats i, després, iniciar la sessió com a estudiant (nom d'usuari: estudiant; contrasenya: moodle) per provar-les.
## Referències
* [What is Moodle?](https://www.youtube.com/watch?v=wop3FMhoLGs)
* [Introduction to Moodle](https://www.lambdasolutions.net/guides-and-research/moodle-user-guide-intro-to-moodle).
* [Using Moodle](https://support.moodle.com/hc/en-us/categories/115000822747-Using-Moodle).
* [Moodle’s core features](https://docs.moodle.org/37/en/Features).
* [Moodle Dashboard](https://docs.moodle.org/37/en/Dashboard).
* [Moodle Activities](https://docs.moodle.org/37/en/Activities).
* [Moodle Calendar](https://docs.moodle.org/37/en/Calendar).
* [Working with files](https://docs.moodle.org/37/en/Working_with_files).
* [Moodle Text editor](https://docs.moodle.org/37/en/Text_editor).
* [Moodle Messaging](https://docs.moodle.org/37/en/Messaging).
* [Tracking progress](https://docs.moodle.org/37/en/Tracking_progress).
* [Contributing to Moodle](https://docs.moodle.org/dev/Contributing_to_Moodle).
* [Moodle Users Associations](https://moodleassociation.org/).
* [Moodle Community Forums](https://moodle.org/course/).