---
title: 'Tecnologies d''impressió 3D de codi obert'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: ct
length: '6 hores'
objectives:
    - 'Beneficiar-se de la fabricació digital'
    - 'Utilitzar tecnologies digitals obertes per crear, manipular, preparar i publicar contingut 3D'
    - 'Utilitzar tecnologies digitals obertes per trobar i aprofitar el repositori de models 3D'
materials:
    - 'Ordinador personal'
    - 'Connexió a Internet'
    - Tinkercard
    - Scketchfab
skills:
    'Paraules clau':
        subs:
            'Fabricació additiva': null
            'Fabricació digital': null
            'Impressora 3D de codi obert': null
---

## Introducció
_Els significatius avenços en les tecnologies de fabricació additiva (AM), comunament conegudes com impressió 3D, han transformat en l'última dècada les formes en què els productes es dissenyen, desenvolupen, fabriquen i distribueixen._

_La capacitat i els avantatges de la impressió 3D sobre la fabricació tradicional obren moltes oportunitats per a les empreses verticals (aquelles que ofereixen béns i serveis específics per a una indústria, comerç, professió o un altre grup de clients amb necessitats especialitzades): des del disseny i desenvolupament de productes, el servei de personalització, fins a la reestructuració de la cadena de subministrament per a una major eficiència._ 
**Comissió Europea valora positivament la naturalesa disruptiva de la impressió 3D**

La introducció de la fabricació additiva (AM per les seves sigles en anglès), millor coneguda com impressió 3D, sorgeix com una tecnologia disruptiva que porta amb si mateixa diversos canvis i impactes en la producció tradicional. Un nombre creixent d'empreses i nous models de negoci basats en l'AM estan sorgint, creant grans oportunitats per a l'economia i la societat. Després de proporcionar a l'alumnat una formació bàsica sobre l'AM, fabricació digital i impressió 3D de codi obert (primera sessió), el curs implicarà un aprenentatge intensiu de maquinari i programari de codi obert i plataformes de codi obert per a la creació d'objectes 3D.

Les sessions permetran a l'alumnat adquirir algunes habilitats tècniques necessàries per aprofitar al màxim totes les oportunitats que ofereixen les tecnologies 3D. Les persones participants experimentaran el procés de disseny dissenyant un objecte utilitzant tecnologies d'impressió 3D de codi obert.
## Context
Les impressores 3D s'estan convertint ràpidament en un dispositiu de fabricació àmpliament utilitzat que ha revolucionat la indústria manufacturera. El propòsit d'aquest curs de capacitació és presentar als participants l'AM i els beneficis més poderosos de les tecnologies d'impressió 3D i l'ús de tecnologies digitals obertes.
En finalitzar les sessions, les persones participants podran crear objectes 3D utilitzant tecnologies digitals obertes, compartint els seus propis projectes i utilitzant els projectes d'altres usuaris.
## Sessions
### Primera sessió: Introducció
Descripció breu dels antecedents, la història i les funcions de la FA.
### Segona sessió: Primera aproximació
Comprendre els principis bàsics del funcionament de les impressores 3D.
### Tercera sessió: Dissenyar i produir.
Explorar el potencial de les tecnologies digitals i obertes com a eina per afrontar els reptes de la transformació digital.
### Segona sessió: Dissenyar, tallar, imprimir
Explorar la configuració del programari d'impressió 3D.
### Segona sessió: Compartir i reinventar models
Reconèixer els principis de disseny de models per a la impressió 3D.
### Segona sessió: Les persones creadores pensen diferent
Promoure de manera col·laborativa una idea de negoci d'impressió 3D