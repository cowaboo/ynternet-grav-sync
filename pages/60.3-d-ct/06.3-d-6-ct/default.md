---
title: 'El cel és el límit?'
length: '150 min'
objectives:
    - 'Proporcionar una introducció al disseny de projecte'
    - 'Facilitar recursos i elements clau per a la gestió del projecte'
    - 'Descobrir diferents activitats que es poden dur a terme en el camp de la fabricació digital'
    - 'Motivar el desenvolupament d’una activitat o projecte dintre d’un espai maker o educatiu'
---

## Introducció
Hem de parlar. Després de veure el proper vídeo sobre la indústria de la impressió 3D, és hora de pensar: no és només per a multimilionaris, no? Hi ha alguna cosa inspiradora per fer impulsar projectes socials?
Video: [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs) (12:12)
Video: [15 Things You Didn’t Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E) (14:33)

## Projectes d’impressió 3D
Els dos primers vídeos estan relacionats amb l’Aprenentatge Basat en Projectes com a estratègia educativa, des de la teoria fins a la pràctica. A més relacionats amb alguna persona que va experimentar amb una idea i la va impulsar fins assolir l’objectiu desitjat, utilitzant la impressió 3D com a eina principal ... i algunes aplicacions inesperades d’aquesta. Potser no és fàcil, potser no és breu.
Video: [Project Based Learning: Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw) (4:05)
Video: [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g) (2:28)
Video: [3D Printing Fashion: How I 3D-Printed Clothes at Home](https://www.youtube.com/watch?v=3s94mIhCyt4) (1:50)
Video: [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE) (3:50)

Cada persona ha de preparar la presentació d’una activitat o projecte basat en el disseny i la impressió 3D. Es valoraran les propostes i la seva metodologia.
Cadascuna tindrà 2 minuts per presentar la seva idea, i després rebrà comentaris de la resta del grup sobre aspectes a millorar o sobre el potencial d’aquesta.
## Treball a casa (autònom)
Busca un espai o un lloc on es pugui implementar la idea d’un projecte o activitat.
## Referències
* Video: [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs) (12:12)
* Video: [15 Things You Didn’t Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E) (14:33)
* Video: [Project Based Learning: Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw) (4:05)
* Video: [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g) (2:28)
* Video: [3D Printing Fashion: How I 3D-Printed Clothes at Home](https://www.youtube.com/watch?v=3s94mIhCyt4) (1:50)
* Video: [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE) (3:50)