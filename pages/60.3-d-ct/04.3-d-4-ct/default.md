---
title: '4a sessió Dissenyar, laminar, imprimir'
length: '150 min'
objectives:
    - 'Conèixer la interfície de Tinkercad, crear un disseny nou i conèixer eines bàsiques a Tinkercad'
    - 'Facilitar la creació de dissenys propis'
    - 'Descobrir les creacions d’altres persones que utilitzen les mateixes eines'
    - 'Crear dissenys complexos basats en formes senzilles i entendre que no hi ha grans problemes, només una suma de petits'
---

## Comencem
Després de fer el nostre compte de Tinkercad, iniciem la sessió i podem fer una ullada al nostre Tauler (ara està buit), a la biblioteca de projectes (buida també) o a la galeria de models d’altres persones (per trobar inspiració). Els models d’altres persones ens mostren un resum, etiquetes, llicències ... de cada model i són un bon exemple de com les persones comparteixen les seves creacions.
És una bona idea també fer una ullada al blog de Tinkercad.
- Web: [Tinkercad Gallery](https://www.tinkercad.com/things)
- Web: [Tinkercad Blog ](https://blog.tinkercad.com/)

## Primer contacte: Com de fàcil i abastable és crear els nostres propis objectes 3D
Des del nostre tauler de comandament de Tinkercad, creem un nou disseny mitjançant el botó amb el mateix nom. Ara intentarem crear un objecte nou. És important provar amb formes senzilles. La creació d’un clauer és un bon punt de partida. Consultem alguns exemples a Thingiverse.
Web: [Thingiverse](https://www.thingiverse.com/search?q=keyring&dwh=125e4bb44042b6e). Paraula clau: keyring 
Tasques:
- Creem la forma principal del nostre clauer mitjançant el menú Formes bàsiques com a caixa o cilindre.
- Canviem les dimensions predeterminades a les dimensions desitjades mitjançant nanses. Tingueu en compte models reals.
- Podem transformar les seves dimensions utilitzant proporcionalment les nanses.
- Parem especial atenció a l’alçada de l’objecte, perquè també creixi proporcionalment. En aquest cas, podem canviar l’alçada de l’objecte utilitzant només el node o nansa de l’eix Z o, en lloc d’utilitzar el node, utilitzem mides específiques per a la dimensió Z només canviant el nombre que apareix a l’eix Z.

## Realització del nostre primer disseny 3D
Tasques:
- Creem un forat de cilindre de 5x5 mm on es col·locarà l’anell. Seguim els consells per crear una forma proporcional.
- Podem moure el forat a la seva posició final mitjançant les tecles de cursor (fletxes) o el ratolí.
- Movem el forat al lloc adequat que volem que sigui. Els moviments es relacionen amb la graella. Canvieu la precisió de la graella fent servir el desplegable a la cantonada inferior dreta per fer moviments més petits o més grans.
- Ara, alineem el forat al centre dret o esquerre de la forma base mitjançant l’eina d’alineació (menú superior dret).
- Després de la col·locació final, no oblidem d’agrupar la forma base i la forma del forat per fer el forat actiu.
- Utilitzem l’eina Text per escriure el nostre nom. Després ho col·loquem sobre la forma de la base.
- Podem canviar les dimensions del text per adaptar-nos a la forma base de la nostra clau. Utilitzeu els mateixos consells per canviar-los usant les nanses, però proporcionalment. Recordeu que l’alçada també augmenta / disminueix.
- Ara, els agrupem tot utilitzant l’eina de grup (menú superior dret).
- Finalment, canviem el nom del nostre disseny. Tinkercad n’ha donat un a l’atzar com a “Rottis-Luulia Espectacular” que (potser) no significa res per a nosaltres.
- Tornem al nostre tauler amb el logotip de Tinkercad (a la part superior esquerra)

Mans a l’obra!
Ara toca desafiar a les nostres persones participants a crear el seu propi clauer (o potser una cosa diferent) que inclogui almenys:
- Dues formes diferents.
- Un forat actiu.
- Un text.
- Dimensions reals (i proporcionals si cal).
- Formes ben alineades i agrupades.

Tinkercad ens ofereix una manera senzilla d’iniciar el disseny 3D. Com a docents, ens dóna la possibilitat de fer de la nostra pròpia aula per compartir projectes. A més, realitza un recorregut d’aprenentatge molt útil per conèixer les seves principals característiques.
Podem crear tants projectes com vulguem, relacionats amb els nostres projectes d’aula. Els estudiants poden compartir les seves creacions i com les han fet.
Tinkercad es basa en crear objectes complexos a partir de formes senzilles. A més de les formes bàsiques, té una àmplia biblioteca de formes. També podem crear la nostra pròpia col·lecció de peces per guardar objectes que utilitzem freqüentment i que haguem creat abans.
## Treball a casa (autònom)
Acabem el disseny que hem començat a l’aula. Compartim el disseny.
## Referències
- Web: [Tinkercad Gallery](https://www.tinkercad.com/things)
- Web: [Tinkercad Blog ](https://blog.tinkercad.com/)