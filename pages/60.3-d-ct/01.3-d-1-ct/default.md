---
title: 'Introducció: breu descripció dels antecedents, la història i les funcions de la FA'
length: '150 min'
objectives:
    - 'Conèixer la filosofia de fons de la FA, saber com va començar l’FA: qui, quan, on, per què i conèixer les principals característiques del bricolatge basat en l’FA: nous artesans'
    - 'Facilitar una visió general d’aquest tipus de fabricació'
    - 'Descobrir com fan les noves persones artesanes i quines són les seves motivacions'
    - 'Motivar l''exploració, el disseny i la creació dels seus propis projectes de bricolatge basats en l’FA'
---

## Group discussion
Diàleg – Trencar el gel basat-nos en aprenentatges significatius:
     • Què en sabeu de l’FA?
     • Coneixeu alguna experiència de FA al món?
     • Coneixes algú relacionat amb tu relacionat amb la FA?
     • Teniu una impressora 3D? Esteu contents amb ella? Per què?## “What is Additive Manufacturing?”

##  És realment útil la impressió 3D?
Visualització dels següents vídeos:
- Vídeo: [What Is 3D Printing and How Does It Work? | Mashable Explains](https://www.youtube.com/watch?v=Vx0Z6LplaMU) () (2:21)
- Vídeo: [Science of Innovation: 3D Bioprinting ](https://www.youtube.com/watch?v=FjjYHwwT73k) (5:34)
- Vídeo: [The biggest 3d printed building ](https://www.youtube.com/watch?v=69HrqNnrfh4) (1:41)
- Vídeo: [Dinner is printed: is 3D technology the future of food? ](https://www.youtube.com/watch?v=B0Ty6wgM8KE) (3:50).  
- Vídeo: [Precious plastic ](https://www.youtube.com/watch?v=8J7JZcsoHyA) (3:01)
- Vídeo: [Pellet extruder ](https://www.youtube.com/watch?v=LHa2iFAzlQE) (0:57)
- Vídeo: [Top 10 3D Printing Ideas For Your Office ](https://www.youtube.com/watch?v=ZVHbyNAcQB0) (10:24)
Tot seguit, es llença la pregunta: Què és el què us ha semblat més interessant veient aquests vídeos?
## Cerquem estris útils: Thingiverse
Un dels més rellevants sobre els nous artesans és que la majoria comparteixen tot el que dissenyen i creen, i està disponible gratuïtament des de llocs com Thingiverse.com sota llicències Creative Commons.
Els participants faran servir el seu navegador per visitar Thingiverse.com i buscar alguna cosa útil a casa. Han de parar especial atenció a la llicència de creacions.
Tot seguit, el formador/ora convida als participants a passar a l'acció:
- Proveu de trobar alguna cosa útil per a casa vostra, utilitzant Thingiverse com a font principal.
- Penseu en coses que feu servir habitualment com: estands per a mòbils, paelles portàtils, portàtil, clauer, pot per a bolígraf, caixes, joguines (per exemple, trencaclosques) ...

Encara es necessari tenir un compte d’usuari per utilitzar Thingiverse, però cal tenir en compte que en el futur serà útil per a compartir les creacions.
## Màquines RepRap o autoreplicacions
RepRap és un projecte comunitari sobre fabricació de màquines autoreplicatives. RepRap va ser la primera de les impressores 3D de baix cost i el projecte RepRap va iniciar la revolució de les impressores 3D de codi obert. S’ha convertit en la impressora 3D més utilitzada entre els membres mundials de la comunitat Maker. (Font: RepRap.org)
Vídeo: [Reprap](https://vimeo.com/5202148)
## Treball a casa (autònom)
Busqueu a tot el món una experiència de tecnologia basada en la solidaritat. Per exemple: https://ayudame3d.org/en/home/
Envia l’URL per compartir-la amb el grup.
## References
* Vídeo: [What Is 3D Printing and How Does It Work? | Mashable Explains](https://www.youtube.com/watch?v=Vx0Z6LplaMU) () (2:21)
* Vídeo: [Science of Innovation: 3D Bioprinting ](https://www.youtube.com/watch?v=FjjYHwwT73k) (5:34)
* Vídeo: [The biggest 3d printed building ](https://www.youtube.com/watch?v=69HrqNnrfh4) (1:41)
* Vídeo: [Dinner is printed: is 3D technology the future of food? ](https://www.youtube.com/watch?v=B0Ty6wgM8KE) (3:50).  
* Vídeo: [Precious plastic ](https://www.youtube.com/watch?v=8J7JZcsoHyA) (3:01)
* Vídeo: [Pellet extruder](https://www.youtube.com/watch?v=LHa2iFAzlQE) (0:57)
* Vídeo: [Top 10 3D Printing Ideas For Your Office ](https://www.youtube.com/watch?v=ZVHbyNAcQB0) (10:24)