---
title: 'Dissenyar & Produir'
length: '150 min'
objectives:
    - 'Explorar el potencial de les tecnologies digitals i obertes com a eina per afrontar els reptes de la transformació digital. Conèixer recursos gratuïts relacionats amb les tecnologies digitals FA i obertes, saber fer un compte per utilitzar recursos gratuïts: Thingiverse i Tinkercad i conèixer els fonaments bàsics de les llicències Creative Commons i del disseny 3D mitjançant Tinkercad'
    - 'Facilitar l’accés als recursos disponibles gratuïts i la creació de col·leccions pròpies d’objectes / projectes inspiradors que es troben a Thingiverse'
    - 'descobrir què fan altres artesans amb les tecnologies FA per resoldre els seus reptes quotidians i com és de fàcil crear els seus primers objectes en 3D i compartir-los'
    - 'motivar per crear els seus propis objectes en 3D i per compartir les seves pròpies creacions amb llicències Creative Commons com a estratègia guanyadora'
---

## Introducció
En primer lloc, visitarem la web Thingiverse i intentarem trobar alguns objectes relacionats amb les nostres aficions. A continuació, intentarem trobar objectes relacionats amb les nostres necessitats diàries. Un cop escollit un objecte concret, revisarem les seves dades: resum, configuració d’impressió, llicència ...
- Web: [Thingiverse.com](https://www.thingiverse.com)

##  Voleu crear?
tilitzant el compte de la persona docent, farem un recorregut per Tinkercad: les seves principals característiques i possibilitats.

- Vídeo: [Welcome to Tinkercad!](https://www.youtube.com/watch?v=LrU2zm_g7lE&feature=emb_logo) (1:30)
- Vídeo: [Tinkercad Tutorial Video](https://www.youtube.com/watch?v=MwjWT-EvKSU&list=PLV6cmKvnKRs6Wn2MOvWGddURlJ0nvCzIP&index=7) (2:44)
- Web: [Tinkercad.com](https://www.tinkercad.com/) 

## En volem formar part
Un dels més rellevants sobre els nous artesans és que la majoria comparteixen tot el que dissenyen i creen, i està disponible gratuïtament des de llocs com Thingiverse.com sota llicències Creative Commons.Després de la introducció, farem un cop d’ull al procés de registre a Thingiverse i Tinkercad: qui és el propietari, a què necessitem unir-nos… i fem un compte individual. A Tinkercad escollim un compte de professors, que ens permetrà crear les nostres pròpies aules. Totes dues necessiten un compte de correu electrònic actiu i poder accedir-hi.

Web: [Thingiverse.com](https://www.thingiverse.com)

Web: [Tinkercad.com](https://www.tinkercad.com)

Al nostre compte de Thingiverse crearem dues col·leccions diferents. Alguna cosa com "coses per la classe" o "coses guais". No cal la paraula «coses».
Un cop connectats a Thingiverse i Tinkercad (i hem iniciat la sessió), prendrem el primer contacte amb la interfície de Tinkercad i seguirem el tutorial per saber com crear un disseny.

Video: [Tinkercad learning](https://www.tinkercad.com/learn/project-gallery;collectionId=OPC41AJJKIKDWDV)

## Treball a casa (autònom)
Com que ja tenim un compte de Thingiverse, ara crearem dues col·leccions diferents. La primera col·lecció ha de tenir dos objectes relacionats amb les nostres aficions i la segona, dos objectes relacionats amb les nostres necessitats diàries. Envieu una captura d’imatges d’aquestes dues col·leccions amb una petita descripció relacionada amb totes dues.
## References
* Web: [Thingiverse.com](https://www.thingiverse.com)
* Vídeo: [Welcome to Tinkercad!](https://www.youtube.com/watch?v=LrU2zm_g7lE&feature=emb_logo) (1:30)
* Vídeo: [Tinkercad Tutorial Video](https://www.youtube.com/watch?v=MwjWT-EvKSU&list=PLV6cmKvnKRs6Wn2MOvWGddURlJ0nvCzIP&index=7) (2:44)
* Web: [Tinkercad.com](https://www.tinkercad.com/) 
* Video: [Tinkercad learning](https://www.tinkercad.com/learn/project-gallery;collectionId=OPC41AJJKIKDWDV)