---
title: 'Compartir i reinventar models'
length: '150 min'
objectives:
    - 'Proporcionar una introducció a com importar i exportar fitxers per a la impressió 3D i els formats, com comunicar-nos amb la impressora 3D: programari de disseny + programari per laminar i com fer la configuració bàsica del programari de laminar.'
    - 'Facilitar la comprensió de la diferència entre un programari de disseny i un programari per laminar'
    - 'Descobrir la importància de fer una bona configuració per obtenir una bona impressió'
    - 'Motivar l''exploració dels diferents paràmetres del procés de laminat i comprendre com afecten el resultat final de la impressió'
---

## Volem imprimir!
Després de dissenyar el nostre clauer (potser altres coses), hem d’exportar les nostres creacions mitjançant el format STL (Stereolithography), com a format més comú. Però aquesta no és compatible amb les impressores 3D, no ho entenen perquè les impressores 3D necessiten informació addicional relacionada amb funcions específiques: qualitat d’impressió, farciment, perímetres, temperatura, refrigeració ... i totes les impressores tenen les seves pròpies funcions o perfil d’impressora: boquilla. diàmetre, dimensions del llit, tipus de filament suportats i el diàmetre específic de ... De vegades, els fabricants de maquinari faciliten els perfils d’impressora per a les seves impressores i aquests perfils es poden importar des del programari de tallat.

- Web: [STL file format](https://en.wikipedia.org/wiki/STL_(file_format)#Use_in_3D_printing) 
- Video: [3D Printing - Introduction to Slicing](https://www.youtube.com/watch?v=8sxlyuN6xso) (4:36)
- Video: [A Noob’s 3D Printing Guide - Episode 5 - Introducing Slicers](https://www.youtube.com/watch?v=JwCzKRbxC_8) (7:29)

## Del món virtual al món real!
Fem servir Tinkercad per exportar una de les nostres creacions. Després d’això, revisarem els diferents programes gratuïts per convertir els fitxers STL a Gcode.
- Video: [How to export .stl files from Tinkercad](https://www.youtube.com/watch?v=ltXYun0BLWY) (1:03)
- Video: [Which free slicer is best? Slicer battles - best quality print?](https://www.youtube.com/watch?v=WUAmnG-c_mI) (10:20)

## Laminem-ho!
Ha arribat el moment de tallar el fitxer STL exportat de Tinkercad amb un dels programes de laminat més coneguts, complets i gratuïts: Cura. L’hem de descarregar al nostre ordinador atenent les característiques de la nostra màquina i el sistema operatiu que estem utilitzant. Cura està disponible per a GNU / Linux, Windows i Mac.
- Video: A Noob’s 3D Printing Guide - [Episode 6 - Setting Up Your Printer Profile In Cura](https://www.youtube.com/watch?v=FpjO_czqqfg)(7:33) 
- Video: A Noob’s 3D Printing Guide - [Episode 7 - Introduction To Cura](https://www.youtube.com/watch?v=ptU21CdyqkE) (10:06) 
- Video: A Noob’s 3D Printing Guide - [Episode 8 - Slicing Your First Model In Cura](https://www.youtube.com/watch?v=5zymO3JVIrI)(6:24)

## El moment de la veritat
- Baixem la versió més recent de Cura (https://ultimaker.com/software/ultimaker-cura) i instal·leu-la al nostre ordinador.
- Intentem de trobar el perfil d’impressora adequat a la nostra impressora 3D. Sovint el podem trobar al lloc web oficial. Un cop descarregat, desem-ho en un lloc segur.
- Exportem un disseny propi de Tinkercad en format STL.
- Obrim el fitxer STL des de Cura.
- Ara configurem la nostra impressora a Cura mitjançant el menú Configuració - Impressores - Gestiona les impressores
   - Afegim impressora: ens donarà l’oportunitat d’escollir la nostra impressora d’una llista àmplia. Si no hi és, podem definir la nostra pròpia impressora a l’opció “Impressora FFF personalitzada”. Els valors que hem d’especificar es defineixen generalment a la guia de l’usuari de la nostra impressora.
   - Perfils: podem importar el perfil de la impressora que hem baixat fa uns instants. De vegades, aquest fitxer conté més d’un perfil: esborrany, mitjà o estàndard i bo. Hem d’importar-los un per un. Tots els perfils contenen especificacions per imprimir.
- Provem el nostre perfil d’impressora. Fixem-nos especialment en el diàmetre del broquet (el més habitual és 0,4), el diàmetre del filament (el més habitual és 1,75) i les dimensions del llit (el més comú es troba entre 200x200x210 i 220x220x240).
- Fem la nostra configuració d’impressió al Cura atenent els paràmetres següents:
   - Tipus de filament: escollim el correcte (el més comú ha de ser el PLA).
   - Definim la qualitat d’impressió a normal / mitjà o 0,15 mm per capa (aprox.).
   - Establim la temperatura del broquet segons les especificacions del filament (varia segons el tipus de filament, la marca, el color ...).

   - Establim la temperatura del llit segons el tipus de filament.
   - Si fem servir PLA, ajustem la temperatura del llit un nivell de 5º més que per a la primera capa.
   - Si fem servir PLA, configurem el ventilador de capa a 0 a la primera capa i 100% per a la segona capa i superior.
   - Establim el farcit en un 15%.
   - Definim l’adhesió de la placa de construcció a Brim, i el Brim a 5.
   - Activem els suports a tot arreu.
- A continuació, fem clic al botó Slice i vegem què passa.
- Cura mostrarà el temps que trigarà la impressió de l’objecte i el material que necessitarem.
- Desem el fitxer Gcode en una targeta SD / USB pendrive (depèn de quina impressora estiguem utilitzant).

L’FA és un procés complex que ens condueix des del món virtual al real.
El disseny 3D és la base per a la impressió FDM o SLA, però està disponible de forma gratuïta mitjançant aplicacions com Tinkercad o altres similars. Aquest programari pot exportar fitxers STL (com el més utilitzat per a la impressió 3D), però aquests fitxers s’han de convertir a un tipus de fitxer específic: Gcode.
Aquesta conversió es realitza mitjançant aplicacions com Cura.
Els fitxers Gcode contenen informació sobre el nostre disseny, però sovint contenen informació relacionada exclusivament amb les funcions de la impressora 3D que estem utilitzant. Cada impressora té les seves pròpies especificacions relacionades amb les seves dimensions, diàmetre del broquet, diàmetre del filament ...
A més, cada impressora té els seus propis perfils d’impressió. Sovint disponible al lloc web dels fabricants. Aquests perfils d’impressió es poden modificar segons les nostres necessitats o crear-ne de derivats.

## Treball a casa (autònom)
Proveu de crear un nou disseny mitjançant Tinkercad i exporteu-lo com a fitxer STL. Si encara no ho heu fet, descarregueu la versió més recent de Cura segons les especificacions del nostre maquinari i instal·leu-la.
Obriu el fitxer STL a Cura i creeu (o importeu) un perfil d’impressora d’acord amb la impressora en què imprimireu. Modifiqueu els paràmetres segons el què hem fet a l’aula.
Lamineu-ho! ... i genereu el fitxer Gcode i envieu-lo al correu electrònic de la persona docent.

## Referències
* Web: [STL file format](https://en.wikipedia.org/wiki/STL_(file_format)#Use_in_3D_printing) 
* Video: [3D Printing - Introduction to Slicing](https://www.youtube.com/watch?v=8sxlyuN6xso) (4:36)
* Video: [A Noob’s 3D Printing Guide - Episode 5 - Introducing Slicers](https://www.youtube.com/watch?v=JwCzKRbxC_8) (7:29)
* Video: [How to export .stl files from Tinkercad](https://www.youtube.com/watch?v=ltXYun0BLWY) (1:03)
* Video: [Which free slicer is best? Slicer battles - best quality print?](https://www.youtube.com/watch?v=WUAmnG-c_mI) (10:20)