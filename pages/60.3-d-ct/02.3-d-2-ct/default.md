---
title: 'Comprendre els principis bàsics del funcionament de les impressores 3D'
length: '150 min'
objectives:
    - 'Conèixer diferents tipus d’impressores 3D: FDM o SLA,  la FDM com la tecnologia més assequible actualment, els diferents tipus d’extrusió en impressores FDM: directa o bowden, senzilla o múltiple i els diferents tipus de filaments i conèixer les seves principals característiques i aplicacions.'
    - 'Facilitar una guia elemental per triar quin tipus d’impressora necessitem relacionada amb el que volem fer i una guia elemental per triar quin tipus de filament seria més útil.'
    - 'Descobrir diferents aplicacions que utilitzen la mateixa tecnologia'
    - 'Motivar l’exploració de la tecnologia més adequada per fer els nostres propis projectes'
---

## Quin tipus d’impressora 3D necessito?
Actualment, hi ha diferents tipus d’impressores 3D. Una de les seves principals diferències està relacionada amb la tecnologia que utilitzen. Els més comuns són FDM (Model de deposició fusionada) i SLA (Estereolitografia).
* [Web: 2020 Types of 3D Printing Technology](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/)
* [Web: FDM vs SLA: The Differences – Simply Explained](https://all3dp.com/fdm-vs-sla/)
* [Video: Round 2! Resin 3D Printer vs FDM 3D Printer | Ender 3 vs Elegoo Mars & MP Mini](https://www.youtube.com/watch?v=CBYs7ArHPKQ) (5:46)

## Si voleu fer-ho millor, utilitzeu-ho
Atenent als diferents tipus de filaments que podem utilitzar amb una impressora 3D basada en la tecnologia FDM, hem de saber quines són les seves principals característiques i capacitats, com a força, rigidesa, resistència al calor, resistència a l’impacte ... i, per descomptat, el seu preu!
* [Web: 2020 3D Printer Filament Buyer’s Guide](https://all3dp.com/1/3d-printer-filament-types-3d-printing-3d-filament/)
* [Video: The BEST 3D printing material? Comparing PLA, PETG & ASA (ABS) - feat. PRUSAMENT by Josef Prusa](https://www.youtube.com/watch?v=ycGDR752fT0) (17:12)
* [Video: Guide to 3D Printing Filament! PLA ABS PETG TPU PEEK ULTEM](https://www.youtube.com/watch?v=Or1FP43zx3I) (19:47)
* [Video: Posem a prova 4 materials 3D: PLA, TPU, PETG i ABS](https://www.youtube.com/watch?v=NIKZkv5QSTk) (7:08)

## Extrusió del filament
Un cop escollit el nostre filament, és hora de saber com farà l’extrusió la nostra impressora 3D. Hi ha algunes consideracions que hem de conèixer sobre l’extrusió, les seves característiques i com afecten els resultats de la nostra impressió.
- [Video: 3D Printing 102 - Extruders](https://www.youtube.com/watch?v=64jnJdbeIjQ) (10:16)

## La mida importa!
Una de les parts més importants d’una impressora 3D és el seu broquet o nozzle. En primer lloc, hem de parar atenció al seu diàmetre, de manera que afectarà no només la qualitat de la nostra impressió, sinó la velocitat que s’imprimeix i la quantitat de filament que utilitzem per imprimir.
A més, es pot construir una broqueta amb diferents materials i afecta quin tipus de filament podem utilitzar.
- [Video: Everything about NOZZLES with a different diameter](https://www.youtube.com/watch?v=XvSNQ7rVDio) (8:12)
## Tothom obre el seu ordinador
Trobeu arreu del món una comparació entre les tecnologies FDM i SLA.
## Treball a casa (autònom)
Proveu breument d’explicar quin tipus d’impressora preferiu i que atengui els avantatges i els contres que tingui per vosaltres. Envieu el document i compartiu-lo amb el grup. Manteniu-lo segur perquè serà la base d’un altre tema.
## References 
* [Web: 2020 Types of 3D Printing Technology](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/)
* [Web: FDM vs SLA: The Differences – Simply Explained](https://all3dp.com/fdm-vs-sla/)
* [Video: Round 2! Resin 3D Printer vs FDM 3D Printer | Ender 3 vs Elegoo Mars & MP Mini](https://www.youtube.com/watch?v=CBYs7ArHPKQ) (5:46)
* [Web: 2020 3D Printer Filament Buyer’s Guide](https://all3dp.com/1/3d-printer-filament-types-3d-printing-3d-filament/)
* [Video: The BEST 3D printing material? Comparing PLA, PETG & ASA (ABS) - feat. PRUSAMENT by Josef Prusa](https://www.youtube.com/watch?v=ycGDR752fT0) (17:12)
* [Video: Guide to 3D Printing Filament! PLA ABS PETG TPU PEEK ULTEM](https://www.youtube.com/watch?v=Or1FP43zx3I) (19:47)
* [Video: Posem a prova 4 materials 3D: PLA, TPU, PETG i ABS](https://www.youtube.com/watch?v=NIKZkv5QSTk) (7:08)
* [Video: 3D Printing 102 - Extruders](https://www.youtube.com/watch?v=64jnJdbeIjQ) (10:16)
* [Video: Everything about NOZZLES with a different diameter](https://www.youtube.com/watch?v=XvSNQ7rVDio) (8:12)