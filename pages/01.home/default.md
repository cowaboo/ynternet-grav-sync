---
title: 'Welcome to the OPENAE Toolkit'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Welcome to the OPENAE Toolkit'
lang_id: 'Welcome to the OPENAE Toolkit'
lang_title: en
---

This document offers an overall and coherent narrative to the OPENAE Toolkit and presents a list of answers to frequently asked questions.

It is organised in the following sections:
* A comprehensive introduction to the Toolkit.
* Frequently Asked Questions.
* Editing the Open-AE Academy.

The first section entitled “A comprehensive introduction to the Toolkit” summarizes the vision, objectives and architecture of the Toolkit.

The second section lists our “Frequently Often asked questions” coming from all partners and their trainers that piloted the OPEN-AE training activities.

The third section is a more technical one, providing the details on how one can edit the Open-AE Academy through an open source and collaborative platform called framagit (see our often asked questions for more). 

The content of this document is an integral part of our OPEN-AE Toolkit published and licenced online. 

<iframe src="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129105-1/" width="800" height="400"></iframe><p><a href="https://slidewiki.org/presentation/129105-1/open-ae-curriculum-modules/129105-1/">Open AE Curriculum Modules </a></p>