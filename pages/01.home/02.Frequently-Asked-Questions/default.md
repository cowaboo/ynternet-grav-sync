---
title: 'Frequently Asked Questions'
---

### _Can anyone use this Toolkit?_
Yes, this work is free and open to everyone to reuse and edit. This goes hand in hand with our FLOSS culture concept (for more, see our learning material [here](https://learning.open-ae.eu/floss-en#overview)). Consequently, we have licensed it under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/) and translated the content to several languages.

### _What are the exact terms of use?_
The [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/) is a copyleft licence used by major FLOSS projects such as [Wikipedia](https://www.wikipedia.org/) and its sister initiatives ([Wikibooks](https://en.wikibooks.org/wiki/Main_Page), [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page) etc). It is one of the Creative Commons (CC) licenses serving as public copyright licenses that enable the free distribution of an otherwise copyrighted "work". A CC license is used when an author wants to give other people the right to share, use, and build upon a work that they (the author) have created (for more, see our learning material [here](https://learning.open-ae.eu/copyleft-en#overview)). 

### _What is FLOSS and FLOSS culture?_
FLOSS refers to Free and Libre Open Source Software as [initially defined](https://www.gnu.org/philosophy/floss-and-foss.en.html) by Richard Stallman  (for more, see our learning material [here](https://learning.open-ae.eu/floss-en#overview)). However, the FLOSS culture goes beyond the software and hardware production area to engage a spectrum of activities including education and art, economy and production, law and copyright, governance and policy making… It is both a theoretical and practical [movement](https://en.wikipedia.org/wiki/Free-culture_movement), expanding horizontally as much to daily practices, as to socioeconomic models. 

### _What can I do with it?_ 
Our main objective is to create a comprehensive space for trainers and trainees to start their learning journey to FLOSS culture. In a nutshell, the Toolkit includes detailed training scenarios, organised in separate learning sessions. Each learning session comes with learning material (definitions, explanations, facts, exercises) and presentations piloted locally during our OPEN-AE piloting activities. 

### _What can I find inside?_
We propose you to think and teach FLOSS culture and skills using 18 autonomous modules. These modules include both generic (for example, The FLOSS culture or DigCompEdu framework for a common and open education), initiative based (for example, Wikidata, SlideWiki), activity specific (for example, Open coding with Scratch, Open robotics with Arduino), or methodological (for example, FLOSS resources for employment E-Learning with FLOSS tools) learning activities. 

### _Why is this different from all the other educational repositories and resources around me?_
We have done some work to define why this work is unique and here are our main arguments:
This a comprehensive place for anyone to get a grip and share, teach on the FLOSS culture themes.
The Toolkit proposes training scenarios, learning sessions and material for an autonomous learning or training approach.
Our architecture (Framagit) allows for participants to edit existing and propose new content.  

### _Sorry, what is Framagit?_
[Framagit](https://framagit.org/) is a web-based collaborative software platform (software) facilitating collaborative programs development. All these services are personal datas and private life respectful. The data is hosted on [Framacloud](https://framacloud.org/). We have used another FLOSS software, [Jekyll](https://en.wikipedia.org/wiki/Jekyll_(software)#:~:text=Jekyll%20is%20a%20simple%2C%20blog,the%20open%20source%20MIT%20license.), to create our user interface for our Toolkit. 

### _What about evaluation and certification?_
Our modules are linked to [badges](https://en.wikipedia.org/wiki/Digital_badge) and every certificate issued from the OPENAE partners reflects the modules that the trainee has followed and competed. Here is an illustration:
![](template_OPENAE__1594130303_188.62.108.74.jpg)


You can find all these materials in an editable form into our website.  

### _Are there any local communities that I can get in touch with?_
Oh yes, there are so many. We have done an initial work to map them to our IO1 (online link needed). But the best way throughout this is to contact one our partners in Italy, Spain, Belgium and Switzerland (see how [here](https://open-ae.eu/partners/)).

### _Was the FLOSS movement active during the COVID 19 pandemic ?_
Definitely ! Some community driven examples include:
* [Genomic analysis of COVID-19 spread](https://nextstrain.org/narratives/ncov/sit-rep/en/2020-03-20)
* [The COVID 19 case mapping](https://chschoenenberger.shinyapps.io/covid19_dashboard/)
* [3D PRINTERS ARE ON THE FRONT LINES OF THE COVID-19 PANDEMIC](https://www.theverge.com/2020/5/25/21264243/face-shields-diy-ppe-3d-printing-coronavirus-covid-maker-response)

### _At the end of the day, can I edit the content of the Toolkit, now?_
Yes, we have created a step by step pathway to this. You can find it in the next section of this document.