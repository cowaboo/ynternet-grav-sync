---
title: 'Editing the Open-AE Academy'
---

### 1.Go to the link: [https://framagit.org/cowaboo/ynternet-grav-sync](https://framagit.org/cowaboo/ynternet-grav-sync) and sign in or register (top right corner)

![](1.png)



Here is how the sign in page looks like (several options are possible).

![](2.png)



### 2.- Choose the page you want to edit by clicking on the folder “pages”. Please make sure that you are [here](https://framagit.org/cowaboo/ynternet-grav-sync/-/tree/master/pages) and you are editing the right page / language:

![](3.png)



Every scenario (work in progress) has six folders, one for every language (English, French, Italian, Dutch, Spanish and Catalan).

The scenarios may not be presented in the right order, pay attention to choose the **right scenario** and the **right language**.

![](4.png)



### 3.- Some pages have sub-pages. Here, for example, you have 2 folders (with the 2 sub-pages) and the “default.me” to edit the scenario.

![](5.png)



### 4.- How to edit:
a) Go to “default.md” of the selected page

![](6.png)



b) You can see the content of the page. Click on “Edit”.

![](7.png)



c) Now you can edit the page

![](8.png)



d) Don’t forget to “Commit changes” (in the bottom of the page) if you want to save your work ;)

![](9.png)



Note that changes take a few minutes before appearing on [https://learning.open-ae.eu/](https://learning.open-ae.eu/) (synchronization process). You can repeat this again and again...
Enjoy !!!