---
title: 'Personal o grupal development of a learning/teaching project'
objectives:
    - 'Provide an introduction to'
    - 'Facilitate '
    - Discover
    - Motivate
lenght: '140 min'
---

## Introduction
The trainer will talk about the questions that may be arised after the previous session will comment on homework. After that will introduce the content and the main activities of the session.

The objective of this session is facilitate the participants the creation of their own project, based on some of the methods presented on the previous sessions. 
Trainer will make a recap of all the content and methods that have been presented and invite de participants to build up teams or to choose to work individually on a training plan. They should select a target group and a topic and then start to design their own training. 
They should think and decide on: 
* Target group.
* Goals / objectives / skills and competencies.
* Background.
* Calendar.
* Main method and strategy.
* Introduction to the training.
* Description of the main activities and the training sequence.

Participants are invited to integrate the new knowledge acquired thanks to the Open-AE training.
## Everyone opens his/her laptop
Trainer will invite the individuals/groups to present their project. After the first round of presentation, a peer-to-peer assessment will be organized. 
* One of the possibilities is to do it in a small format: a group meets with another group (or an individual with another individual) and comments, constructively, strengths and weaknesses of the project, open software or hardware resources that they can use and others complementary details.
* The other possibility is to do it with the group-class as a brainstorming. Any member of the class can send questions to the person / people who has presented their project, make suggestions and contribute to the peer-to-peer assessment.

## Debriefing
This is the final part of this module (and of the training, if possible) 
Review of what we have learned and the individual commitments acquired, if any, and final group photo. Good luck and see you very soon!

#Homework
Keep working on your project and make it real in your class. Assesses the result based on the degree of satisfaction of the participants and the academic results obtained. Do not forget that the results of the active methodologies are not evaluated with the traditional exams: you will have to work with the students' portfolios and assess to what extent they are capable of applying the new knowledge and skills acquired.

#References
Yasemin Gülbahar & Hasan Tinmaz (2006) Implementing Project-Based Learning And E-Portfolio Assessment In an Undergraduate Course, Journal of Research on Technology in Education, 38:3, 309-327, DOI: 10.1080/15391523.2006.10782462

Brigid J.S. Barron, Daniel L. Schwartz, Nancy J. Vye, Allison Moore, Anthony Petrosino, Linda Zech & John D. Bransford (1998) Doing With Understanding: Lessons From Research on Problem- and Project-Based Learning, Journal of the Learning Sciences, 7:3-4, 271-311, DOI: 10.1080/10508406.1998.9672056 