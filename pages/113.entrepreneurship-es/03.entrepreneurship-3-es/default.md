---
title: 'Definición del mercado'
objectives:
    - 'Entender qué es y cómo afecta la segmentación geográfica'
    - 'Entender qué es y cómo afecta la segmentación de clientes'
    - 'Conocer la competencia'
    - 'Analizar la madurez del mercado y cómo nos afecta.'
---

## Segmentación geográfica
¿Qué es la segmentación geográfica? La segmentación geográfica ocurrec cuando una empresa divide su mercado en función de la ubicación. Hay varias maneras de segmentar geográficamente un mercado: se puede dividir por zonas geográficas, (ciudad, condado, estado, región, país o región internacional); se puede dividir en segmentos de mercado rural, suburbano y urbano; y se puede segmentar un mercado en relación al clima o la población total de cada área. 

Algunas ventajas de la segmentación geográfica:
- Es un enfoque eficaz para empresas con grandes mercados nacionales o internacionales, para atender a consumidores de diferentes regiones que tienen necesidades, necesidades y características culturales diferentes.
- También puede ser un enfoque eficaz para pequeñas empresas con presupuestos limitados. Pueden centrarse en un área definida y no invertir en estrategias de marketing innecesarias con enfoques poco adecuados para su segmento geográfico objetivo.
- Funciona bien en diferentes áreas con una alta densidad de población. Los consumidores de un entorno urbano a menudo tienen necesidades y deseos diferentes que los que se encuentran en entornos rurales y suburbanos. Incluso hay diferencias culturales entre estas tres áreas.
- Es relativamente fácil desglosar un mercado en segmentos geográficos.

## Segmentación de clientes
Segmentar un mercado es una buena práctica. Nos permite desarrollar un conocimiento más profundo de los clientes y descubrir qué les hace consumir. Cuando estamos comunicando un mensaje, tendremos en cuenta que será más efectivo si el destinatario del mensaje lo encuentra relevante. La segmentación es simplemente una manera de organizar los clientes en grupos más reducidos según su tipología. Estos diferentes sub-grupos o segmentos deben caracterizarse por atributos particulares, de manera que podemos orientar mensajes de marketing específicos y relevantes para cada grupo.

Cómo comunicar también es vital, y la segmentación requiere a menudo una estrategia de marketing cuidadosamente estructurada. Esto se debe a que algunos clientes pueden preferir el enfoque directo, como el marketing telefónico, mientras que otros responden mejor a una campaña publicitaria local.

Primeros pasos para la segmentación de clientes. La segmentación no debe ser compleja. Para una pequeña empresa, se podría tratar de reconocer que tiene dos o tres tipos de clientes diferentes con necesidades diferentes. Es buena idea empezar siempre con una simple pregunta: ¿con quien queremos hablar? La respuesta puede ser sencilla: con clientes. Los principios de segmentación pueden añadir varias capas basadas en las diferenciales clave, tales como:

- patrones de gasto
- género
- donde viven
- edad
- grupo socioeconómico


Lo importante no son las diferencias superficiales, sino aquellas que afectan realmente el comportamiento de compra. ¿Qué motiva a cada persona a comprar? Si se trata de un salón de peluquería, por ejemplo, el tipo de ofertas que se pueden hacer a los grupos de clientes diferirán según el sexo y la edad. Si se tiene un negocio de pedidos de correo electrónico, podremos analizar los patrones de compra y dividir los clientes en grupos según la cantidad que gastan, la frecuencia de compra o sobre qué productos tienen más interés. 

Al aumentar la comprensión de lo que compran sus clientes, también podemos maximizar las oportunidades de venta cruzada o venta mayor. Agrupando a todos los clientes que compran regularmente determinados productos, se puede orientar la compra realizando ofertas relevantes que les animen a aumentar su gasto.

Además, todo esto está relacionado con un buen servicio de atención al cliente. Un mensaje de comunicación que reconozca lo que se ha comprado y cuando se ha realizado la compra, tiene un impacto mucho mayor que un mensaje generalizado. Es más, si se trata de un cliente habitual, un mensaje orientado muestra que es apreciado y valorado. Por el contrario, un mensaje general que reconoce los antecedentes de compra, podría hacer que el cliente se sienta desilusionado, porque ha recibido un trato poco personalizado. 

## Competencia
Los cuatro tipos de estructuras básicas de mercado a tener cuenta:  competencia perfecta, competencia monopolística, oligopolio y monopolio. Cada una de ellas tiene su propio conjunto de características y presupuestos, que a su vez afectan la toma de decisiones de las empresas y los beneficios que pueden obtener.

Es importante tener en cuenta que no todas estas estructuras de mercado existen en realidad, ya que algunas de ellas son sólo construcciones teóricas. Sin embargo, tienen una importancia crítica, porque pueden ilustrar aspectos relevantes de la toma de decisiones de las empresas en relación a la competencia. Por lo tanto,  ayudarán a comprender los principios económicos subyacentes.  

### competencia perfecta
La competencia perfecta describe una estructura de mercado donde un gran número de pequeñas empresas compiten entre ellas. En este escenario, una sola empresa no tiene ningún poder de mercado significativo. Como resultado, la industria en general produce un nivel de producción socialmente óptimo, ya que ninguna de las empresas tiene la capacidad de influir en los precios del mercado.

La idea de competencia perfecta se basa en varios supuestos: (1) todas las empresas maximizan los beneficios (2) hay entrada y salida gratuita en el mercado, (3) todas las empresas venden mercancías completamente idénticas (es decir, homogéneas), (4) allí no hay preferencias del consumidor. Si nos fijamos en estos supuestos, se hace obvio que casi nunca encontraremos competencia perfecta en la realidad. Este es un aspecto importante, porque es la única estructura del mercado que puede (teóricamente) dar lugar a un nivel de producción socialmente óptimo. Probablemente el mejor ejemplo de mercado con competencia casi perfecta que podemos encontrar en realidad es el mercado de valores.  

### competencia monopolística
La competencia monopolística también hace referencia a una estructura de mercado donde un gran número de pequeñas empresas compiten entre ellas. Sin embargo, a diferencia de la competencia perfecta, las empresas en competencia monopolística venden productos similares, pero ligeramente diferenciados. Esto les proporciona un cierto grado de poder del mercado que les permite utilizar precios más altos dentro de un determinado intervalo.

La competencia monopolista se basa en los siguientes supuestos: (1) todas las empresas maximizan los beneficios (2) hay entrada y salida gratuita en el mercado, (3) las empresas venden productos diferenciados (4) los consumidores pueden preferir un producto a otro. Estas hipótesis están un poco más cerca de la realidad que las que mirábamos en el apartado anterior. Sin embargo, esta estructura de mercado dejará de estar en un nivel de producción socialmente óptimo, ya que las empresas tienen más poder y pueden influir hasta cierto punto en los precios del mercado. Un ejemplo de competencia monopolística es el mercado del café. Hay un gran número de marcas diferentes (por ejemplo: La Estrella, Bonka, Día, Hacendado, Alipende... ), que presentan gustos similares aunque diferentes, pero, al final, todas comercializan el mismo producto: café. 

### oligopolio
Un oligopolio describe una estructura de mercado que sólo es dominada por un pequeño número de empresas. Esto resulta en un estado de competencia limitada. Las empresas pueden competir entre ellas o colaborar. Al hacerlo, pueden utilizar su poder de mercado colectivo para aumentar los precios y obtener más beneficios.

La estructura del mercado oligopolista se basa en los siguientes supuestos: (1) todas las empresas maximizan los beneficios, (2) los oligopolios pueden fijar precios, (3) hay barreras de entrada y salida al mercado, (4) los productos pueden ser homogéneos o diferenciados, y (5) sólo hay algunas empresas que dominan el mercado. Desgraciadamente, no se define claramente qué quiere decir exactamente “unas pocas empresas”. Como regla general, decimos que un oligopolio consiste normalmente en unas 3-5 empresas dominantes.

Por poner un ejemplo de oligopolio, miramos el mercado de las consolas de juegos. Este mercado está dominado por tres empresas potentes: Microsoft, Sony y Nintendo. 

### Monopolio
Un monopolio hace referencia a una estructura de mercado donde una sola empresa controla todo el mercado. En este caso, la firma tiene el nivel más alto de potencia del mercado, ya que los consumidores no tienen alternativas. Como resultado, estas empresas a menudo reducen la producción para aumentar los precios y obtener más beneficios.

Los siguientes supuestos se hacen cuando hablamos de monopolios: (1) el monopolista maximiza los beneficios, (2) puede fijar el precio, (3) hay grandes barreras de entrada y salida, (4) sólo hay una empresa que domina todo el mercado.

Desde la perspectiva de la sociedad, la mayoría de los monopolios no suelen ser deseables, porque dan como resultado unos rendimientos más bajos y unos precios más altos en comparación con los mercados competitivos. Por lo tanto, a menudo son regulados por el gobierno. Un ejemplo de monopolio de la vida real podría ser Monsanto. Esta empresa tiene la marca comercial alrededor del 80% del total de maíz procedente en EEUU. Esto proporciona a Monsanto un nivel de poder extremadamente alto. 

## Madurez del mercado
Después de las fases de introducción y crecimiento, un producto pasa a la fase de madurez. La tercera de las etapas del ciclo de vida del producto puede ser un momento muy difícil para los fabricantes. En las dos primeras etapas, las empresas intentan establecer un mercado y, a continuación, aumentan las ventas de su producto para conseguir una parte tan grande como sea posible. Sin embargo, durante la etapa de madurez, el objetivo principal de la mayoría de las empresas será mantener su cuota de mercado ante diversos retos diferentes.
![](image9.jpg)
### Retos de la etapa de madurez
- Volúmenes comerciales máximos: Tras el incremento constante de las ventas durante la etapa de crecimiento, el mercado empieza a saturarse ya que hay menos clientes nuevos. La mayoría de los consumidores que siempre van a comprar el producto ya lo han hecho.
- Disminución de la cuota de mercado: Otra característica de la etapa de madurez es el gran volumen de fabricantes que compiten por una parte del mercado. Con esta etapa del ciclo de vida del producto, que a menudo presenta los niveles más altos de competencia, es cada vez más difícil para las empresas mantener su cuota de mercado.
- Los beneficios comienzan a disminuir: Si bien esta etapa se puede producir cuando el mercado en general obtiene el máximo beneficio, a menudo es la parte del ciclo de vida del producto donde la existencia de muchos fabricantes puede provocar la disminución de beneficios, ya que éstos deberán repartirse entre todos los competidores del mercado y es probable que un fabricante que pierda cuota de mercado y tenga una caída de las ventas produzca una caída posterior de los beneficios. Esta disminución de los beneficios se puede agravar con la caída de los precios que a menudo se ve cuando el número reducido de competidores obliga algunos de ellos a intentar atraer más clientes compitiendo en el precio.

### Beneficios de la etapa de madurez
- Reducción continuada de costes: de la misma manera que las economías de escala en la etapa de crecimiento contribuyeron a reducir costes, los desarrollos de la producción pueden conducir a formas más eficientes de fabricar grandes volúmenes de un determinado producto, ayudando a reducir aún más los costes. 
- Mayor participación de mercado mediante la diferenciación: si bien el mercado puede llegar a la saturación durante la etapa de madurez, los fabricantes podrían aumentar su cuota de mercado y aumentar los beneficios de otras maneras. Mediante el uso de campañas de marketing innovadoras y ofreciendo funciones de producto más diversas, las empresas pueden mejorar su cuota de mercado mediante la diferenciación, por ejemplo.  

## Gestión del ciclo de vida del producto en fase madurez
La etapa de madurez del ciclo de vida del producto presenta los fabricantes una amplia gama de retos. Si las ventas llegan a su punto álgido y el mercado se satura, puede ser muy difícil que las empresas mantengan o intenten aumentar sus beneficios. Durante esta etapa, las organizaciones buscan maneras innovadoras de hacer más atractivo su producto para el consumidor y así poder mantener, y tal vez incluso aumentar, su cuota de mercado.