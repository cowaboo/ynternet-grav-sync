---
title: 'Emprendimiento en línea con herramientas FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: es
length: '10 horas'
objectives:
    - 'Planificando tu negocio'
    - 'Ciclo de vida del proyecto'
    - 'Introducción a los presupuestos'
materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a Internet'
    - Proyector
    - 'Nociones básicas de las competencias FLOSS'
skills:
    'Palabras clave':
        subs:
            'Hacer negocios en línea': null
            Emprendimiento: null
---

## Introducción
Las ventas en línea nunca fueron mejores, y la predicción es que continuarán creciendo hasta 2021, como muestran las estadísticas del  ‘Statista – The Statistics Portal’. Este tipo de comercio ha ido evolucionando año tras año y consiguiendo nuevos adeptos. Las tiendas virtuales no son más que escaparates llenos de productos disponibles para la venta y el mercado digital ha detectado la necesidad de invertir en estrategias de marketing para conocer qué piensan los clientes al respecto de sus productos y también para hacer el proceso postventa.
Cualquier negocio se puede poner en línea: desde las clases deportivas hasta el aprendizaje de idiomas, la consultoría de negocios, los entrenadores personales, los coach, los servicios de catering, los artesanos...
El comercio electrónico es una de las actividades digitales más populares en el mundo; sólo en 2016 las ventas minoristas de comercio electrónico ascendieron a 1,86 billones de dólares en todo el mundo. Para el año 2021, se espera que las ventas en línea crecerán a 4.48 billones de dólares. Eso muestra cuánto pueden beneficiarse las personas emprendedoras y sus negocios de la Era digital, especialmente para escalar el negocio, sin importar en qué área.

El número total de compradores digitales en todo el mundo también está aumentando. Creció en más de 100 millones entre 2011 y 2012, y siguió creciendo. La flexibilidad de los horarios para hacer las compras permite un mejor equilibrio entre la vida laboral y personal y, como consecuencia, las mujeres son en buena parte responsable del aumento del número personas compradoras (realizan una doble jornada, la laboral y la del hogar). En 2012, una encuesta de Greenfield encontró que las mujeres representan el 58% del gasto en línea en los EE. UU.
## Contexto
Este módulo permitirá a la persona participante: 
* Enseñar habilidades para desarrollar una idea de negocio basadas en un caso real. 
* Mostrar como se usa el análisis DAFO.
* Entender el mercado y los diferentes tipos de competencia que enfrenta una empresa.
* Mostrar el ciclo de vida del producto.
* Enseñar conceptos básicos sobre marketing.
* Enseñar conceptos básicos sobre presupuestos.

Metodologías:
* Deliberación: grupos de discusión y foros de discusión.
* Ejecución: elaboración de planes por parte de los alumnos.

## Sesiones
### Primera sesión: Desarrollo y evaluación de ideas de negocio mediante mapas mentales Análisis DAFO 
En esta sesión trabajaremos en el análisis de una idea de negocio utilizando análisis DAFO y mapas mentales.
### Segunda sesión: Definiendo el mercado
Esta sesión se centrará en la segmentación del cliente y en la comprensión de la competencia.
### Tercera sesión: Presupuestos
¿Qué es un presupuesto y cuáles son los diez pasos para un buen seguimiento?