---
title: 'Análisis DAFO'
objectives:
    - 'Saber cómo realizar un análisis DAFO (en inglés, SWOT)'
    - 'Conocer las preguntas clave para hacer durante un análisis DAFO'
    - 'Utilizar un ejemplo de análisis DAFO'
lenght: '120 min'
---

## Introducción
DAFO es un acrónimo que se refiere a Fortalezas, Debilidades, Oportunidades y Amenazas. El análisis DAFO es una lista organizada de los puntos fuertes, debilidades, oportunidades y amenazas de su negocio. Los puntos fuertes y los puntos débiles son internos de la empresa (como por ejemplo: reputación, patentes, ubicación). Pueden cambiar con el tiempo, pero no sin trabajo. Las oportunidades y las amenazas son externas (por ejmplo: proveedores, competidores, precios); están en el mercado, tanto si nos gusta como si no. No podemos cambiarlas, pero es importante tenerlas en cuenta.

Las empresas que ya existen pueden utilizar un análisis DAFO en cualquier momento para evaluar un entorno cambiante y responder de manera proactiva. De hecho, se recomienda realizar una reunión de revisión de estrategias como mínimo una vez al año, y comenzar por un análisis DAFO. Las empresas nuevas deben utilizar un análisis DAFO como parte de su proceso de planificación. No hay ningún plan único para una empresa, y pensar en un nuevo negocio en términos de sus "DAFO" exclusivos nos situará en el camino correcto  y nos ahorrará muchos dolores de cabeza más adelante.

## Cómo realizar un análisis DAFO
Para obtener resultados más completos y objetivos, un grupo de personas con diversas perspectivas y participaciones en una empresa pueden realizar un análisis DAFO. Gestión, ventas, atención al cliente y, incluso, clientes pueden aportar información válida. Además, el proceso de análisis DAFO es una oportunidad para unir un equipo y fomentar su participación y la adhesión a la estrategia resultante de la empresa. El análisis DAFO se realiza normalmente mediante una plantilla de análisis DAFO de cuatro cuadrados, pero también se pueden hacer listas para cada categoría. 

Se recomienda hacer una sesión de lluvia de ideas para identificar los factores de cada una de las cuatro categorías. Como alternativa, se podría pedir a los miembros del equipo que completaran individualmente una plantilla de análisis DAFO y que, después, se reunieran para discutir y recopilar los resultados. No es necesario profundizar mucho a medida que se trabaja en cada una de las categorías. Detectar los puntos clave pueden ser la mejor manera de empezar. Sólo hay que capturar los factores que se cree que son relevantes en cada una de las cuatro áreas. 

Una vez terminada la lluvia de ideas, se crea una versión final y priorizada del análisis DAFO, enumerando los factores de cada categoría, colando primero los que tienen la máxima prioridad. 

### Preguntas para hacer durante un análisis DAFO
A continuación presentamos algunas preguntas que contribuyen a desarrollar cada sección del análisis DAFO. Hay, ciertamente, otras preguntas que podría hacer pero ésta es una buena manera de comenzar. 

### fortalezas
Las fortalezas describen los atributos positivos, tangibles e intangibles, internos de una organización. Están bajo nuestro control.
* ¿Qué hacemos bien?
* ¿Qué recursos internos tenemos? Se pueden tener en cuenta los siguientes: Atributos positivos de personas, tales como conocimiento, antecedentes, educación, credenciales, red, reputación o habilidades. Activos materiales de la empresa, tales como capital, crédito, clientes existentes o canales de distribución, patentes o tecnología.
* ¿Qué ventajas tenemos respecto a la competencia?
* ¿Tenemos una fuerte capacidad de investigación y desarrollo? ¿Instalaciones para la fabricación o el almacenamiento?
* ¿Qué otros aspectos positivos, internos para el negocio, aportan valor añadido u ofrecen una ventaja competitiva?

### Debilidades (factores internos, negativos)
Los puntos débiles son aspectos de un negocio que perjudican el valor que éste ofrece o lo sitúan en una desventaja competitiva. Debemos mejorar estas áreas para ser más competitivos. 
* ¿Qué factores, entre los que se encuentran bajo nuestro control, perjudican nuestra capacidad de obtener o mantener una ventaja competitiva?
* ¿Qué áreas necesitamos mejorar para alcanzar nuestros objetivos o competir con el competidor más fuerte?
* ¿Qué le falta a nuestro negocio (por ejemplo, experiencia o acceso a habilidades o tecnología)?
* Nuestra empresa ¿tiene recursos limitados?
* Nuestra empresa ¿está en una ubicación desfavorecida?

### Oportunidades (factores externos, positivos)
Las oportunidades son factores atractivos externos que representan elementos que probablemente provocarán que prospere nuestro negocio.
* ¿Cuales son sus competidores actuales o posibles?
* La percepción de nuestro negocio ¿es positiva?
* ¿Ha habido un crecimiento reciente del mercado u otros cambios que generen una oportunidad?
* ¿La oportunidad sigue abierta, o sólo hay una pequeña ventana?

### Amenazas (factores externos, negativos)
Las amenazas incluyen factores externos fuera de nuestro control que podrían poner en riesgo la estrategia o el propio negocio. No tenemos ningún control sobre ellas, pero podemos beneficiarnos de los planes de contingencia para hacerles frente si fuera oportuno. 
* ¿Cuales son sus competidores actuales o posibles?
* ¿Qué factores fuera de nuestro control pueden poner en riesgo la empresa?
* ¿Hay retos creados por una tendencia o desarrollo desfavorables que puedan conducir a disminuir los ingresos o beneficios?
* ¿Qué situaciones pueden amenazar los esfuerzos de marketing?
* ¿Ha habido un cambio importante de los precios de los proveedores o en la disponibilidad de materias primas?
* ¿Hay cambios en el comportamiento de los consumidores, en la economía o en las regulaciones gubernamentales, que pudieran afectar a las ventas?
* ¿Se ha introducido un nuevo producto o tecnología que haga que los productos, equipos o servicios queden obsoletos?

## Ejemplos de análisis DAFO
A modo de ilustración, presentamos un breve ejemplo DAFO de un hipotético almacén de ordenadores de tamaño medio en Estados Unidos (en inglés):

![](image1a.jpg)

**Análisis TOWS: **

Ayuda al desarrollo de estrategias a partir del análisis DAFO. Una vez identificados y priorizados los resultados DAFO, podemos utilizar este tipo de análisis para desarrollar estrategias a corto y largo plazo para el negocio. Al fin y al cabo, el verdadero valor de este ejercicio es utilizar los resultados para maximizar las influencias positivas sobre su negocio, y minimizar las negativas.

Pero, ¿cómo se convierten los resultados DAFO en estrategias? Una manera de hacerlo es considerar cómo los puntos fuertes, los puntos débiles, las oportunidades y las amenazas de la compañía se superponen entre sí (análisis TOWS). Por ejemplo, una vez identificados los puntos fuertes, analizarlos para ver cómo  maximizar las oportunidades. Después, podemos analizar cómo se pueden utilizar estos mismos puntos fuertes para minimizar las amenazas identificadas. 

Continuando este proceso, podemos utilizar las oportunidades que hemos identificado para desarrollar estrategias que minimicen las debilidades o eviten las amenazas. 

La tabla siguiente (en inglés) puede ayudarnos a organizar las estrategias de cada área:

![](image2a.jpg)

Una vez que se han desarrollado las estrategias y se han incluido en el plan estratégico, nos aseguraremos de programar reuniones de revisión periódicas. Utilizar estas reuniones para hablar de porqué los resultados de las estrategias son diferentes a lo planificado (siempre será así) y decidir los próximos pasos, es siempre una buena estrategia. 

## Resumen DAFO (en inglés)
![](image3a.png)

## Trabajo en casa (autónomo)
Utilizar, por ejemplo un producto FLOSS como el Libro Office, y hacer una práctica DAFO/TOWS.

## Referencias:
[Smart Women Project](http://smartwomenproject.eu/).