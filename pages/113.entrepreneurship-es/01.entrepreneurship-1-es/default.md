---
title: 'Negocio y desarrollo empresarial. Mapas mentales y DAFO'
objectives:
    - 'Definición de la idea de negocio mediante mapas mentales'
    - 'Analizar la idea mediante un análisis DAFO'
lenght: '120 min'
---

## Introducción
Quienes somos? Presentación individual respondiendo brevemente estas cuatro preguntas:
1. ¿Cuál es tu idea de negocio? 
2. ¿Cuál es su producto? 
3. ¿A quién va dirigido su producto? (¿Quién serán los clientes?) 
4. ¿Dónde estamos ahora mismo? 

Ejemplo: "Mi idea de negocio es empezar a vender productos caseros. Me gustaría especializarme en mermeladas sin azúcar, dirigidas a personas que siguen dietas bajas en azúcar o diabéticos. Ahora mismo estoy preparando estas mermeladas para mis familiares y amigos y, a veces, como regalos. "

Ahora trabajaremos en el desarrollo de estas ideas utilizando una herramienta de lluvia de ideas, lo que nos ayudará a desarrollar una amplia gama de ideas. Comenzaremos con una palabra clave y anotaremos todos los puntos / ideas que se nos puedan ocurrir. No criticaremos ninguna idea (si hacemos este ejercicio en grupo).

Ejemplo:

![](image1.png)

## Un enfoque potente para la toma de notas
Mind Mapping (elaborar mapas mentales) es una técnica útil que ayuda a aprender de manera más eficaz, mejora la manera de registrar información y apoya y mejora la resolución creativa de problemas. Con el uso de Mind Maps, se puede identificar y entender rápidamente la estructura de un tema, facilitando la visualización de toda la información. Aún más, los mapas mentales ayudan a recordar la información, ya que la mantienen en un formato que resulta fácil de recordar, y rápido de revisar. Herramienta FLOSS recomendada: [CMapTools](cmap.ihmc.us/)

Además, los mapas mentales ayudan a desglosar grandes proyectos o temas en fragmentos manejables, de manera que se favorece la planificación eficaz, evitando sentirnos abrumados por el exceso de información; nos ayudan a tener presente toda la información relevante. 

Un buen mapa mental muestra la "esencia" del tema, la importancia relativa de los puntos individuales y la forma en que los hechos se relacionan entre ellos. Esto significa que su revisión es muy rápida y fácil de hacer, echando un rápido vistazo. Pueden ser instrumentos mnemónicos muy eficaces; recordar la forma y la estructura de un Mind Map puede ser la clave para recordar la información. 

Cuando se crean con colores, imágenes y dibujos, un mapa mental puede parecer una obra de arte.

Los mapas mentales son útiles para:
* Hacer lluvias de ideas: individualmente y en grupo.
* Resumir de información y toma de notas.
* Consolidar información de diferentes fuentes de investigación.
* Pensar mediante conceptos y problemas complejos.
* Presentar la información en un formato que muestre la estructura general del tema.
* Estudiar y memorizar información.

### Dibujo básico de mapas mentales
Para dibujar un mapa mental, podemos seguir estos pasos:
1. Escribir el título del tema que exploramos el centro de la página y dibujar un círculo a su alrededor, tal como se muestra en el círculo marcado en la figura 1, a continuación. (Nuestro ejemplo, sencillo, muestra las acciones necesarias para hacer una lluvia de ideas para hacer una presentación exitosa.) 

![](image2.jpg)**Figura 1**

2. A medida que encontramos grandes subdivisiones o subtítulos del tema (o hechos importantes relacionados con el mismo) marcamos líneas que surgen de este círculo. Etiquetamos estas líneas con estas subdivisiones o subtítulos. (Véase la figura 2, a continuación). 

![](image3.jpg)**Figura 2**

3. A medida que nos introducimos en el tema y descubrimos otro nivel de información (otros subtítulos o hechos individuales) pertenecientes a los sub-apartados, los dibujamos como líneas enlazadas con las líneas de los subtítulos. Estos se muestran en la figura 3. 

![](image4.jpg)**Figura 3**

4. A continuación, para hechos o ideas individuales, marcamos nuevas líneas, como se muestra en la figura 4. 

![](image5.jpg)**Figura 4**

5. A medida que vamos encontrando información nueva, la enlazamos en el mapa mental.

Un mapa mental completo puede tener las líneas temáticas principales que irradian en todas las direcciones del centro y, a partir de aquí, enunciados y hechos se separarán, como si fueran las ramas de un árbol. No tendremos que preocuparnos de la estructura que producimos, ya que evolucionará según avanzamos.

### Utilizar mapas mentales de forma eficaz
Una vez que hemos comprendido cómo tomar notas en formato Mind Map, podemos desarrollar sus convenciones. Las sugerencias siguientes pueden ayudarnos a dibujar mapas de ánimo impactantes:
* Utilizar palabras simples o frases simples, para asegurar que la información se transmite en el contexto correcto y en un formato amigable para leer. En los mapas mentales, las palabras simples y las frases cortas y significativas pueden transmitir el mismo significado con más potencia. Demasiadas palabras desordenarán el mapa mental. 
* Utilizar el color para separar ideas diferentes: nos ayudará a separar las ideas cuando sea necesario. También contribuirá a visualizar y comprender mejor el mapa mental. El color puede ayudar a mostrar la organización del tema. 
* Utilizar símbolos e imágenes que contribuyan a recordar la información con más eficacia que las palabras.
* Utilizar enlaces cruzados: la información de una parte de un mapa mental puede relacionarse con otra parte. En este caso, podemos dibujar líneas para mostrar los vínculos cruzados.  

## Trabajo en casa (autónomo)
Ejemplo: Analizamos el tema de "salud"

![](image6.png)

![](image7.png)