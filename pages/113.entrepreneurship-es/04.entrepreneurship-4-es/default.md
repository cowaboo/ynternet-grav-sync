---
title: 'El presupuesto'
objectives:
    - 'Entender la declaración que muestra los costos estimados del proyecto para apoyar el proyecto.'
    - 'Elaborar un presupuesto e  incluir todos los costes directos e indirectos necesarios para llevar a cabo el proyecto.'
    - 'Conocer los costes directos: los gastos que se asocian específicamente al producto o servicio.'
---

## Trabajo en grupo
Cada grupo elige cualquier producto que desee y hace una lista de los costes directos e indirectos que comporta su producción.

![](image13.png)

## Análisis coste-beneficio
Antes de asumir un nuevo proyecto, realizar un análisis coste-beneficio ayudará a evaluar todos los costes e ingresos potenciales que se puedan generar si el proyecto se finaliza. El resultado del análisis determinará si el proyecto es factible económicamente o si se ha de llevar a cabo otro proyecto. Se realiza un análisis coste-beneficio en tres pasos:
1. Recopilar una lista completa de todos los costes y beneficios asociados al proyecto.
2. En la partida de costes, incluir los costes directos e indirectos, los intangibles, los costes de oportunidad y el coste de los posibles riesgos.
3. En la partida de beneficios: incluir todos los ingresos directos e indirectos y beneficios intangibles, y el aumento de las ventas de los clientes.

El presupuesto debe ser realista y no subestimar los costes ni sobreestimar los beneficios. El último paso es comparar cuantitativamente los resultados de los costes y beneficios para determinar si los beneficios superan los costos. En caso afirmativo, la decisión es sacar adelante el proyecto. Si no es así, hay que hacer una revisión del proyecto para ver si se pueden hacer ajustes para aumentar los beneficios y / o disminuir los costes para hacer el proyecto viable. Si esto último no es posible, el proyecto debería ser abandonado.

![](image14.png)

### Ejemplo de Análisis de beneficios

![](image15.png)

## Los 10 pasos para desarrollar un presupuesto con éxito:

1. ** Plan estratégico**. Todas las organizaciones deben tener un objetivo a cumplir (la Declaración de visión y misión). Un plan estratégico es la forma en que la organización planea alcanzar su misión. El primer paso en el proceso de elaborar el presupuesto es tener un plan estratégico escrito. De esta manera se garantiza que los recursos organizativos se utilicen para apoyar la estrategia y el desarrollo de la organización.
2. **Objetivos empresariales**. Para implementar su plan estratégico, una organización toma los objetivos de negocio anuales y los desarrolla para poder alcanzarlos. El presupuesto proporciona los recursos financieros para alcanzar estos objetivos. Ejemplo: si una organización necesita ampliar sus instalaciones, es necesario que haya dinero presupuestado para que esto ocurra.
3. **Proyecciones de ingresos**. Las proyecciones de ingresos (beneficio) deben basarse en el rendimiento financiero anterior, así como en los ingresos previstos para el crecimiento. El crecimiento previsto puede estar ligado a objetivos organizativos e iniciativas planificadas que inicien el crecimiento empresarial. Ejemplo: Si hay un objetivo de aumentar las ventas un 10%, estas proyecciones de ventas deberían formar parte de las proyecciones de ingresos del año.
4. **Proyecciones de costes fijos**: son los costos mensuales previsibles que no cambian, como los costes de compensación de los empleados, los gastos de las instalaciones, los servicios públicos, los pagos hipotecarios / alquileres, los seguros, etc. Los costos fijos no cambian y son un gasto mínimo que hay que financiar en el presupuesto.
5. **Proyecciones de coste variable**. Los costes variables son costes que oscilan de mes en mes, como los costes de suministro, las horas extraordinarias, etc. Son gastos que se deben presupuestar y controlar. Ejemplo: se debería presupuestar un aumento temporal de las horas extras por ventas de Navidad.
6. **Objetivo anual de gastos**. Los proyectos relacionados con objetivos también deberían recibir presupuestos e incorporarlos al presupuesto anual. Las proyecciones de costes se deben identificar, establecer e incorporar al presupuesto departamental encargado de cumplir el objetivo. Ejemplo: Si el departamento de ventas tiene el objetivo de aumentar las ventas un 10%, se deberían incorporar al presupuesto los costes asociados al incremento de ventas (materiales de marketing adicionales, viajes, entretenimiento).
7. **Margen de beneficio objetivo**. Los beneficios son importantes para todas las organizaciones y los márgenes de beneficio saludables son un fuerte indicador de la fortaleza de una organización. Todas las organizaciones, ya sean sin ánimo de lucro o sin ánimo de lucro, deberían tener un margen de beneficio orientado. Los márgenes de beneficio permiten reinvertir en las instalaciones y en el desarrollo de la organización.
8. **Aprobación del Consejo de administración o dirección de la entidad**. La dirección de la organización debe aprobar el presupuesto y mantenerse al día con el rendimiento del presupuesto. La dirección también debe revisar los estados financieros mensuales  y supervisar el rendimiento del presupuesto: conocer todos los gastos., proteger la organización contra la apropiación indebida de fondos o fraudes, etc. 
9. **Revisión del presupuesto**. Una comisión de revisión del presupuesto debería reunirse mensualmente para supervisar el rendimiento en relación a los objetivos. Este comité debería revisar las diferencias presupuestarias y evaluar los problemas relacionados con ellas. Es importante hacerlo mensualmente para que haya una corrección en el gasto o una  modificación del presupuesto, si es necesario. Esperar hasta finales del ejercicio para hacer correcciones podría tener un impacto negativo en el resultado del presupuesto final.
10. **Hacer frente a las variaciones presupuestarias**. Las variaciones presupuestarias deben revisarse para poder analizar qué es lo que ha provocado la diferencia. A veces se presentan situaciones imprevistas que no se pueden evitar, por lo que también es importante disponer de un fondo de emergencia para ayudar a estos gastos no previstos. Un buen sistema de gestión del presupuesto puede ayudar a que la organización se desarrolle, mientras  uno malo, con un seguimiento deficiente, pueden blindar una organización y afectar su salud financiera, su viabilidad y, eventualmente, causar la pérdida de clientes.

## Definiciones clave

### un balance
Es una declaración financiera que da a los inversores una idea sobre lo que la compañía tiene y debe en un momento determinado. Se compone de tres segmentos que son:
- el activo de la empresa,
- el pasivo de la empresa y
- la cantidad invertida por los accionistas (patrimonio del propietario).

### el activo
Un activo es un recurso con valor económico que una empresa posee con la expectativa de que proporcionará beneficios futuros. Los activos se registran en el balance de la empresa. Se puede pensar en un activo como algo que en el futuro puede generar flujo de caja, reducir gastos, mejorar las ventas y añadir valor a la empresa.

### el pasivo
El pasivo es una deuda u obligación financiera de la empresa que se derivan durante el desarrollo de sus operaciones comerciales. Se liquida con el paso del tiempo mediante la transferencia de beneficios económicos incluyendo dinero, bienes o servicios. El pasivo incluye préstamos, hipotecas, etc. y se registra en el balance.

### el capital del propietario
Este es el valor de un activo menos el importe de todos los pasivos de este activo. Se puede representar con la ecuación contable: Activos - Pasivo = Capital. Ejemplo: Un coche o una casa que no tiene deuda pendiente se considera completamente el patrimonio del propietario porque puede vender fácilmente el artículo en efectivo y envolver la suma resultante.

### Requisitos de capital
Esta es la suma de fondos que necesita su empresa para alcanzar sus objetivos.

- ¿Cuánto dinero necesita hasta que su negocio esté en funcionamiento?
- Se puede calcular los requisitos de capital añadiendo gastos fundacionales (tasas notariales, honorarios legales, tasas de registro, gastos de corredores inmobiliarios, etc.), inversiones y costes iniciales.
- Restando el capital patrimonial (patrimonio del propietario) de los requisitos de capital, se puede calcular cuánto capital externo necesitará: Patrimonio neto - Requerimientos de capital = Cal capital externo

La planificación de los requisitos de capital está estrechamente relacionada con las otras partes del plan de negocio, porque hay que tener en cuenta los costes de seguimiento en la planificación.

## Consideraciones clave para definir el capital necesario para iniciar un negocio
Hay que tener en cuenta los gastos de inicio. Para la mayoría de startups, los ingresos de los primeros meses no son suficientes para cubrir el coste de funcionamiento, ya que suelen estar ocupados en conseguir clientes y procesar pedidos antes de que finalmente pueda enviar sus primeras facturas y obtener los pagos correspondientes; esto resulta muy relevante y se debe tener en cuenta al realizar el plan de negocio. 

No debemos olvidar tener en cuenta los gastos de intereses y las amortizaciones; la planificación debe incluir tanto los costes de explotación como los pagos hipotecarios. También es importante incluir una reserva para contingencias, tales como pedidos retrasados, mayores gastos de renovación o nuevos activos no planificados. Todo debe estar calculado en el plan de negocio y en el presupuesto. 