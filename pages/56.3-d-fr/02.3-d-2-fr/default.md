---
title: 'Première approximation'
objectives:
    - 'Fournir une introduction à connaître différents types d''imprimantes 3D: FDM ou SLA'
    - 'Fournir une introduction à connaître FDM comme la technologie la plus abordable de nos jours'
    - 'Fournir une introduction à connaître les différents types d''extrusion sur imprimantes FDM: direct ou bowden, simple ou multiple'
    - 'Fournir une introduction à connaître les différents types de filaments et connaître leurs principales caractéristiques et applications'
    - 'Faciliter un guide élémentaire pour choisir le type d''imprimante dont nous avons besoin par rapport à ce que nous voulons faire'
    - 'Faciliter un guide élémentaire pour choisir quel type de filament serait plus utile'
    - 'Découvrir différentes applications utilisant la même technologie'
    - 'Motiver d''explorer quelle est la technologie la plus appropriée pour réaliser nos propres projets'
---

## De quel type d'imprimante 3D ai-je besoin?
Aujourd'hui, il existe différents types d'imprimantes 3D. L'une de leurs principales différences est liée à la technologie qu'ils utilisent. Les plus courants sont FDM (Fused Deposition Modeling) et SLA (Stereolithography Apparatus).
* [2020 Types of 3D Printing Technology](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/) 
* [FDM vs SLA: The Differences – Simply Explained](https://all3dp.com/fdm-vs-sla/)
* [Round 2! Resin 3D Printer vs FDM 3D Printer | Ender 3 vs Elegoo Mars & MP Mini](https://www.youtube.com/watch?v=CBYs7ArHPKQ)

## Si vous devez mieux faire cela, vous l'utilisez.
Concernant les différents types de filaments que nous pouvons utiliser avec une imprimante 3D basée sur la technologie FDM, nous devons savoir quelles sont leurs principales caractéristiques et capacités, comme la résistance, le raidissement, la résistance à la chaleur, la résistance aux chocs… et bien sûr leur prix!
* [2020 3D Printer Filament Buyer’s Guide](https://all3dp.com/1/3d-printer-filament-types-3d-printing-3d-filament/).
* [The BEST 3D printing material? Comparing PLA, PETG & ASA (ABS) - feat. PRUSAMENT by Josef Prusa](https://www.youtube.com/watch?v=ycGDR752fT0).
* [Guide to 3D Printing Filament! PLA ABS PETG TPU PEEK ULTEM](https://www.youtube.com/watch?v=Or1FP43zx3I).
* [Posem a prova 4 materials 3D: PLA, TPU, PETG i ABS](https://www.youtube.com/watch?v=NIKZkv5QSTk).

## Extrusion du filament.
Une fois que nous avons choisi notre filament, il est temps de savoir comment notre imprimante 3D va l'extruder. Il y a certaines considérations que nous devons connaître sur l'extrusion, ses caractéristiques et comment elles affectent les résultats de notre impression.
* [3D Printing 102 - Extruders](https://www.youtube.com/watch?v=64jnJdbeIjQ).

## La taille compte!
L'une des parties les plus importantes d'une imprimante 3D est sa buse. Tout d'abord, nous devons faire attention à son diamètre, donc cela affectera non seulement la qualité de notre impression, mais aussi sa vitesse d'impression et la quantité de filament que nous utilisons pour imprimer.
De plus, une buse peut être construite en différents matériaux et cela affecte le type de filament que nous pouvons utiliser.
* [Everything about NOZZLES with a different diameter](https://www.youtube.com/watch?v=XvSNQ7rVDio).

## Devoirs
Essayez brièvement d'expliquer quel type d'imprimante préférez-vous pour les avantages et les inconvénients qu'elle a pour vous. Envoyez le document et partagez-le avec le groupe. Gardez-le en sécurité afin qu'il soit la base d'un autre sujet.
## Références
* [Everything about NOZZLES with a different diameter](https://www.youtube.com/watch?v=XvSNQ7rVDio).
* [3D Printing 102 - Extruders](https://www.youtube.com/watch?v=64jnJdbeIjQ).
* [2020 3D Printer Filament Buyer’s Guide](https://all3dp.com/1/3d-printer-filament-types-3d-printing-3d-filament/).
* [The BEST 3D printing material? Comparing PLA, PETG & ASA (ABS) - feat. PRUSAMENT by Josef Prusa](https://www.youtube.com/watch?v=ycGDR752fT0).
* [Guide to 3D Printing Filament! PLA ABS PETG TPU PEEK ULTEM](https://www.youtube.com/watch?v=Or1FP43zx3I).
* [Posem a prova 4 materials 3D: PLA, TPU, PETG i ABS](https://www.youtube.com/watch?v=NIKZkv5QSTk).
* [2020 Types of 3D Printing Technology](https://all3dp.com/1/types-of-3d-printers-3d-printing-technology/) .
* [FDM vs SLA: The Differences – Simply Explained](https://all3dp.com/fdm-vs-sla/).
* [Round 2! Resin 3D Printer vs FDM 3D Printer | Ender 3 vs Elegoo Mars & MP Mini](https://www.youtube.com/watch?v=CBYs7ArHPKQ).