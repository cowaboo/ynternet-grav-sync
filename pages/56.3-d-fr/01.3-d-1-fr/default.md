---
title: Introduction
objectives:
    - 'Fournir une introduction à connaître la philosophie de base de l''AM'
    - 'Fournir une introduction à savoir comment l''AM a commencé: qui, quand, où, pourquoi'
    - 'Fournir une introduction à connaître les principales caractéristiques du bricolage basé sur AM: de nouveaux artisans'
    - 'Faciliter un aperçu de ce type de fabrication'
    - 'Découvrir comment font les nouveaux artisans'
    - 'Découvrir qui sont de nouvelles motivations d''artisans'
    - 'Motiver à explorer, concevoir et créer leurs propres projets de bricolage basés sur AM'
---

## Dialogue - Déglaçage basé sur un apprentissage significatif
Que savez-vous d'AM?
Connaissez-vous une expérience AM dans le monde?
Connaissez-vous une personne liée à vous liée à AM?
Avez-vous une imprimante 3D? Es tu heureux avec? Pourquoi?
## L'impression 3D est-elle vraiment utile?
* [What Is 3D Printing and How Does It Work? | Mashable Explains](https://www.youtube.com/watch?v=Vx0Z6LplaMU).
* [Science of Innovation: 3D Bioprinting](https://www.youtube.com/watch?v=FjjYHwwT73k).
* [The biggest 3d printed building](https://www.youtube.com/watch?v=69HrqNnrfh4).
* [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE).
* [Precious plastic](https://www.youtube.com/watch?v=8J7JZcsoHyA).
* [Pellet extruder](https://www.youtube.com/watch?v=LHa2iFAzlQE).
* [Top 10 3D Printing Ideas For Your Office](https://www.youtube.com/watch?v=ZVHbyNAcQB0).

Question: Quel est le plus intéressant que vous ayez vu en regardant ces vidéos?
## Trouvez quelque chose d'utile - Thingiverse
L'un des plus pertinents des nouveaux artisans est que la plupart d'entre eux partagent tout ce qu'ils conçoivent et créent, et il est disponible gratuitement sur des sites comme thingiverse.com sous licence Creative Commons.
Les participants utiliseront leur navigateur pour visiter thingiverse.com et chercher quelque chose d'utile à la maison. Ils doivent accorder une attention particulière à la licence de création.

Activité:
Essayez de trouver quelque chose d'utile pour votre maison, en utilisant Thingiverse comme source principale.
Pensez à des choses que vous utilisez couramment comme: supports mobiles, sous-verres, porte-clés, porte-clés, pot à stylos, boîtes, jouets (par exemple, puzzles) ...
Vous n'avez pas encore besoin d'un compte utilisateur pour utiliser Thingiverse mais gardez à l'esprit qu'il sera utile à l'avenir de partager vos créations.
## RepRap
Description de l'activité: RepRap ou machines à réplication automatique
RepRap est un projet communautaire sur la fabrication de machines à réplication automatique. RepRap a donc été la première des imprimantes 3D à faible coût et le projet RepRap a lancé la révolution des imprimantes 3D open source. Elle est devenue l'imprimante 3D la plus utilisée parmi les membres mondiaux de la communauté Maker.
* [Site RepRap.org](reprap.org)
* [Video sur Reprap](https://vimeo.com/5202148)

## Devoirs
Trouver dans le monde entier une expérience AM fondée sur la solidarité. Par exemple: [Ayudame3D](https://ayudame3d.org/en/home/).
Envoyez l'URL à partager avec le groupe.
## References
* [RepRap.org](reprap.org)
* [What Is 3D Printing and How Does It Work? | Mashable Explains](https://www.youtube.com/watch?v=Vx0Z6LplaMU).
* [Science of Innovation: 3D Bioprinting](https://www.youtube.com/watch?v=FjjYHwwT73k).
* [The biggest 3d printed building](https://www.youtube.com/watch?v=69HrqNnrfh4).
* [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE).
* [Precious plastic](https://www.youtube.com/watch?v=8J7JZcsoHyA).
* [Pellet extruder](https://www.youtube.com/watch?v=LHa2iFAzlQE).
* [Top 10 3D Printing Ideas For Your Office](https://www.youtube.com/watch?v=ZVHbyNAcQB0).