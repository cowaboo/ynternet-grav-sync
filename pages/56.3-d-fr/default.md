---
title: 'Technologies d''impression 3D open source'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open-source 3-D printing Technologies'
lang_id: 'Open-source 3-D printing Technologies'
lang_title: fr
length: '6 heures'
objectives:
    - 'Identifier les principales fonctionnalités et avantages de la AM'
    - 'Permettre aux apprenants d''augmenter leurs compétences informatiques en utilisant des logiciels et du matériel ouverts pour l''impression 3D'
    - 'Utiliser des technologies numériques ouvertes pour concevoir, développer et personnaliser des produits'
    - 'Permettre la production et le partage de modèles 3D et de prototypes'
    - 'Explorer des méthodes de collaboration dans des activités en petits groupes pour apprendre les uns des autres'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - 'Cartes SD'
    - 'Lecteur USB pour cartes SD'
    - 'Imprimante 3D (au moins une)'
    - 'Outils d''imprimante 3D (tels que pincettes, couteaux à palette, couteau et tapis de coupe, pinces et tournevis à clé hexagonale)'
    - Beamer
    - 'Papier et stylos'
    - Flipchart
    - 'Slidewiki Academy'
skills:
    'Mots clés':
        subs:
            Reprap: null
            'Impression 3D': null
            'Conception 3D': null
            Prototypage: null
            Slicer: null
            'Fabrication additive (AM)': null
            'Open source': null
---

## Introduction
L'impression 3D a des applications illimitées grâce auxquelles elle est si populaire. Ce module présentera aux élèves la fabrication additive ou l'impression et la conception tridimensionnelles (3D), comment cela fonctionne et comment il peut être utilisé dans divers contextes et industries.

Ce module fonctionne dans une variété de formats, comprenant des conférences / démonstrations, des présentations sur l'impression 3D, des démonstrations physiques et une activité de travail d'équipe où les apprenants présentent des idées commerciales d'impression 3D en groupes.
## Contexte
Les imprimantes 3D deviennent rapidement un appareil de fabrication largement utilisé qui a révolutionné l'industrie manufacturière. Le but de cette formation est de présenter aux participants la MA et les avantages les plus puissants des technologies d'impression 3D et de l'utilisation des technologies numériques ouvertes.
À la fin des sessions, les participants seront capables de créer des objets 3D en utilisant des technologies numériques ouvertes, en partageant leurs propres projets et en utilisant les projets d'autres utilisateurs.
## Séances
### Première séance : Introduction
Décrivez brièvement le contexte, l'histoire et les caractéristiques de la AM
### Deuxième séance : Première approximation
Comprendre les principes de base du fonctionnement des imprimantes 3D
### Troisième séance : Concevoir et produire
Explorez le potentiel de la AM et des technologies numériques ouvertes comme outil pour relever les défis de la transformation numérique.
### Quatrième séance : Conception, tranche, impression
Explorez les paramètres du logiciel d'impression 3D
### Cinquième séance : Partage et mixage de modèles
Reconnaître les principes de conception de modèles pour l'impression 3D
### Sixième séance : Les fabricants pensent différemment
Présentez en collaboration une idée d'entreprise d'impression 3D