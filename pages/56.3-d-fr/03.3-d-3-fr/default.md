---
title: 'Concevoir et produire'
objectives:
    - 'Fournir une introduction à connaître les ressources gratuites liées à la AM et aux technologies numériques ouvertes'
    - 'Fournir une introduction à savoir créer un compte pour utiliser des ressources gratuites: Thingiverse et Tinkercad'
    - 'Fournir une introduction à connaître les bases des licences Creative Commons'
    - 'Fournir une introduction à connaître les bases de la conception 3D à l''aide de Tinkercad'
    - 'Faciliter l''accès aux ressources disponibles gratuitement pour créer leurs propres collections d''objets / projets inspirants qu''ils trouvent dans Thingiverse'
    - 'Découvrir ce que les autres artisans font en utilisant les technologies AM pour résoudre leurs défis quotidiens'
    - 'Découvrir comment il est facile de créer leurs premiers objets 3D et de les partager'
    - 'Motiver pour créer leurs propres objets 3D'
    - 'Partager leurs propres créations sous licences Creative Commons comme stratégie gagnant-gagnant'
---

## Recherche d'inspiration
Tout d'abord, nous visiterons le Web Thingiverse et nous essaierons de trouver des objets liés à nos hobbies. Ensuite, nous essaierons de trouver des objets liés à nos besoins quotidiens. Une fois que nous avons choisi un objet particulier, nous allons examiner leurs détails: résumé, paramètres d'impression, licence ...
* [Thingiverse.com](https://www.thingiverse.com)

## Envie de créer?
En utilisant le compte du professeur, nous allons faire un tour dans Tinkercad: ses principales fonctionnalités et possibilités.
* [Welcome to Tinkercad!](https://www.youtube.com/watch?v=LrU2zm_g7lE&feature=emb_logo)
* [Tinkercad Tutorial Video](https://www.youtube.com/watch?v=MwjWT-EvKSU&list=PLV6cmKvnKRs6Wn2MOvWGddURlJ0nvCzIP&index=7).
* [Tinkercad.com](https://www.tinkercad.com/)

## Nous voulons faire partie de Thingiverse et Tinkercad
Après l'introduction, nous examinerons le processus de participation de Thingiverse et Tinkercad: à qui appartient-il? ce dont nous avons besoin pour participer? et créer un compte individuel? À Tinkercad, nous choisissons un compte enseignant, qui nous permet de créer nos propres salles de classe. Les deux ont besoin d'un compte de email actif et accès.
* [Thingiverse.com](https://www.thingiverse.com)
* [Tinkercad.com](https://www.tinkercad.com/)

**Activité**
Dans notre compte Thingiverse, nous créerons deux collections différentes. Quelque chose comme "Trucs de classe" ou "Trucs sympas".
## Devoirs
Comme nous avons déjà un compte Thingiverse, nous allons créer deux collections différentes. La première collection doit avoir deux objets liés à nos hobbies et la seconde deux objets liés à nos besoins quotidiens. Envoyez une capture d'image de ces deux collections avec une petite description relative aux deux.
## Références
* [Thingiverse.com](https://www.thingiverse.com)
* [Tinkercad.com](https://www.tinkercad.com/)
* [Welcome to Tinkercad!](https://www.youtube.com/watch?v=LrU2zm_g7lE&feature=emb_logo)
* [Tinkercad Tutorial Video](https://www.youtube.com/watch?v=MwjWT-EvKSU&list=PLV6cmKvnKRs6Wn2MOvWGddURlJ0nvCzIP&index=7).
* [Tinkercad.com](https://www.tinkercad.com/)
* [Thingiverse.com](https://www.thingiverse.com)