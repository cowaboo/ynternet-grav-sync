---
title: 'Partage et mixage de modèles'
objectives:
    - 'Fournir une introduction à comment importer et exporter des fichiers pour l''impression 3D. Formats'
    - 'Fournir une introduction à comment communiquer avec l''imprimante 3D: logiciel de conception + logiciel de découpage'
    - 'Fournir une introduction à comment faire la configuration de base du logiciel de découpage'
    - 'Faciliter la compréhension de la différence entre un logiciel de conception et un logiciel de découpage'
    - 'Découvrir l''importance de faire une bonne configuration pour obtenir une bonne impression'
    - 'Explorer les différents paramètres du processus de découpe et de comprendre comment ils affectent le résultat final de l''impression'
---

## Nous voulons imprimer!
Après avoir conçu notre trousseau de clés (peut-être d'autres choses), nous devons exporter nos créations en utilisant le format STL (stéréolithographie), comme format le plus courant. Mais celui-ci n'est pas pris en charge par les imprimantes 3D, ils ne comprennent pas car les imprimantes 3D ont besoin d'informations supplémentaires liées à des fonctionnalités spécifiques: qualité d'impression, remplissage, périmètres, température, refroidissement… et chaque imprimante a ses propres fonctionnalités ou profil d'imprimante: buse diamètre, dimensions du lit, types de filaments pris en charge et diamètre spécifique de… Parfois, les fabricants de matériel facilitent les profils d'imprimante pour leurs imprimantes et ces profils peuvent être importés à partir du logiciel de découpage.
* [STL file format](https://en.wikipedia.org/wiki/STL_(file_format)#Use_in_3D_printing).
* [3D Printing - Introduction to Slicing](https://www.youtube.com/watch?v=8sxlyuN6xso).
* [A Noob's 3D Printing Guide - Episode 5 - Introducing Slicers](https://www.youtube.com/watch?v=JwCzKRbxC_8).

## Du virtuel au réel!
Utilisons Tinkercad pour exporter une de vos créations. Après cela, nous allons jeter un œil à différents logiciels de découpage gratuits pour convertir nos fichiers STL en Gcode.
* [How to export .stl files from Tinkercad](https://www.youtube.com/watch?v=ltXYun0BLWY).
* [Which free slicer is best? Slicer battles - best quality print?](https://www.youtube.com/watch?v=WUAmnG-c_mI).

## Tranchons!

Il est temps de découper notre fichier STL exporté de Tinkercad avec l'un des logiciels de découpage les plus connus, complets et gratuits: Cura. Nous devons télécharger sur notre ordinateur les fonctionnalités de notre machine et le système d'exploitation que nous utilisons. Cura est disponible pour GNU / Linux, Windows et Mac.
* [A Noob's 3D Printing Guide - Episode 6 - Setting Up Your Printer Profile In Cura](https://www.youtube.com/watch?v=FpjO_czqqfg).
* [A Noob's 3D Printing Guide - Episode 7 - Introduction To Cura](https://www.youtube.com/watch?v=ptU21CdyqkE).
* [A Noob's 3D Printing Guide - Episode 8 - Slicing Your First Model In Cura](https://www.youtube.com/watch?v=5zymO3JVIrI).

## Le moment de vérité
* Téléchargez la version la plus récente de Cura (https://ultimaker.com/software/ultimaker-cura) et installez-la sur notre ordinateur.
* Essayez de trouver le profil d'imprimante approprié de notre imprimante 3D. Souvent, nous pouvons le trouver sur le site officiel. Une fois téléchargé, conservez-le en lieu sûr.
* Exportez une conception de votre choix depuis Tinkercad au format STL.
* Ouvrez le fichier STL de Cura.
* Maintenant, nous configurons notre imprimante dans Cura en utilisant le menu Paramètres - Imprimantes - Gérer les imprimantes
 * Ajouter une imprimante: cela nous permettra de choisir notre imprimante dans une grande liste. Si ce n'est pas le cas, nous pouvons définir notre propre imprimante après «Imprimante FFF personnalisée». Les valeurs que nous devons spécifier sont généralement définies dans le guide d'utilisation de notre imprimante.
 * Profils: nous pouvons importer le profil d'imprimante que nous avons téléchargé il y a quelques instants. Parfois, ce fichier contient plusieurs profils: brouillon, moyen ou standard et fin. Nous devons les importer un par un. Chaque profil contient des spécifications pour l'impression.
* Testez votre profil d'imprimante. Portez une attention particulière au diamètre de la buse (le plus courant est 0,4), au diamètre du filament (le plus courant est 1,75) et aux dimensions du lit (le plus courant entre 200x200x210 et 220x220x240).
* Configurons nos paramètres d'impression Cura en tenant compte des paramètres suivants:
 * Type de filament: choisissez le bon (le plus courant devrait être le PLA).
 * Réglez la qualité d'impression sur normal / moyen ou 0,15 mm par couche (environ).
 * Réglez la température de la buse en fonction des spécifications du filament (elle varie en fonction du type de filament, de la marque, de la couleur…).
 * Réglez la température du lit en fonction du type de filament.
      * Si vous utilisez du PLA, réglez la température du lit à 5º juste pour la première couche.
      * Si vous utilisez du PLA, réglez le ventilateur de refroidissement sur 0 sur la première couche et à 100% pour cent sur la deuxième couche et au-dessus.
* Cliquez ensuite sur le bouton Slice et voyez ce qui se passe.
* Cura indiquera le temps nécessaire à l'impression et la quantité de matériel dont nous aurons besoin.
* Enregistrez le fichier Gcode sur une carte SD / clé USB (cela dépend de l'imprimante que nous utilisons).
 * Réglez le remplissage à 15%.
 * Réglez l'adhésion de la plaque de construction sur le bord et la largeur du bord sur 5.
 * Activez les supports Partout.

## Débriefing et questions / introduction au module suivant (formateur)
L'AM est un processus complexe qui nous fait passer du virtuel au réel.
La conception 3D est la base de l'impression FDM ou SLA, mais elle est disponible gratuitement à l'aide d'applications telles que Tinkercad ou similaires. Ce logiciel peut exporter des fichiers STL (comme les plus couramment utilisés pour l'impression 3D), mais ces fichiers doivent être convertis en un type de fichier spécifique: Gcode.
Cette conversion est effectuée en découpant des applications telles que Cura.
Les fichiers Gcode contiennent des informations sur notre conception, mais ils contiennent souvent des informations liées exclusivement aux fonctionnalités de l'imprimante 3D que nous utilisons. Chaque imprimante a ses propres spécifications liées à ses dimensions, diamètre de buse, diamètre de filament…
De plus, chaque imprimante possède ses propres profils d'impression. Souvent disponible sur le site Web des fabricants. Ces profils d'impression peuvent être modifiés selon nos besoins, ou créer des profils dérivés.
## Devoirs
Essayez de créer un nouveau design à l'aide de Tinkercad et exportez-le en tant que fichier STL. Si ce n'est pas encore fait, téléchargez la version la plus récente de Cura selon les spécifications de notre matériel et installez-la.
Ouvrez le fichier STL dans Cura et créez (ou importez) un profil d'imprimante en fonction de l'imprimante sur laquelle nous allons imprimer. Modifiez les paramètres en fonction de ce que nous avons fait en classe.
Slice it! ... et générez le fichier Gcode et envoyez-le au courrier électronique de l'enseignant.
## Références
* [A Noob's 3D Printing Guide - Episode 6 - Setting Up Your Printer Profile In Cura](https://www.youtube.com/watch?v=FpjO_czqqfg).
* [A Noob's 3D Printing Guide - Episode 7 - Introduction To Cura](https://www.youtube.com/watch?v=ptU21CdyqkE).
* [A Noob's 3D Printing Guide - Episode 8 - Slicing Your First Model In Cura](https://www.youtube.com/watch?v=5zymO3JVIrI).
* [How to export .stl files from Tinkercad](https://www.youtube.com/watch?v=ltXYun0BLWY).
* [Which free slicer is best? Slicer battles - best quality print?](https://www.youtube.com/watch?v=WUAmnG-c_mI).
* [STL file format](https://en.wikipedia.org/wiki/STL_(file_format)#Use_in_3D_printing).
* [3D Printing - Introduction to Slicing](https://www.youtube.com/watch?v=8sxlyuN6xso).
* [A Noob's 3D Printing Guide - Episode 5 - Introducing Slicers](https://www.youtube.com/watch?v=JwCzKRbxC_8).