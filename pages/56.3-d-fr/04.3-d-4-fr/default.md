---
title: 'Conception, tranche, impression'
objectives:
    - 'Fournir une introduction à connaître l''interface Tinkercad'
    - 'Fournir une introduction à créer un nouveau design'
    - 'Fournir une introduction à connaître les outils de base de Tinkercad'
    - 'Créer leurs propres designs'
    - 'Découvrir créations d''autres personnes utilisant les mêmes outils'
    - 'Motiver à créer des conceptions complexes basées sur des formes simples'
    - 'Motiver à omprendre qu''il n''y a pas de gros problèmes, juste une somme de petits'
---

## Commençons!
Après avoir créé notre compte Tinkercad, nous nous connectons et nous pouvons jeter un œil à notre tableau de bord (maintenant vide), à notre bibliothèque de projets (vide aussi) ou à la galerie de modèles d'autres personnes (pour trouver de l'inspiration). Ces autres modèles nous montrent un résumé, des tags, une licence… de chaque modèle, et ils sont un bon exemple de la façon dont les gens partagent leurs créations.
C’est aussi une bonne idée de jeter un œil sur le blog de Tinkercad.
* [Tinkercad Gallery](https://www.tinkercad.com/things)
* [Tinkercad Blog](https://blog.tinkercad.com/)

## Premier contact - La facilité et la disponibilité de la création de nos propres objets 3D
À partir de notre tableau de bord Tinkercad, nous créons un nouveau design en utilisant le bouton du même nom. Nous allons maintenant essayer de créer un nouvel objet. Il est important d'essayer avec des formes simples. La création d'un trousseau de clés est un bon point de départ. Jetez un œil à quelques exemples dans Thingiverse.
* [Thingiverse. Keyword: keyring](https://www.thingiverse.com/search?q=keyring&dwh=125e4bb44042b6e).

**Activité**
Créez la forme principale de votre trousseau en utilisant le menu Formes de base comme boîte ou cylindre.
Modifiez leurs dimensions par défaut aux dimensions souhaitées à l'aide des poignées. Gardez à l'esprit les vrais modèles.
Nous pouvons transformer leurs dimensions en utilisant les poignées proportionnellement.

Portez une attention particulière à la hauteur de l'objet, car il grandira également proportionnellement. Dans ce cas, nous pouvons modifier la hauteur de l’objet à l’aide de la poignée de l’axe Z ou, au lieu d’utiliser la poignée, utiliser des mesures spécifiques pour la dimension Z en modifiant simplement le nombre qui apparaît sur l’axe Z.
## Réalisation de notre premier design 3D
* Créez un trou de cylindre de 5x5 mm où l'anneau sera placé. Utilisez les conseils pour créer une forme proportionnelle.
* Nous pouvons déplacer le trou dans sa position finale à l'aide de nos touches fléchées du clavier ou de la souris.
* Déplacez le trou au bon endroit où nous voulons qu'il soit. Les mouvements sont liés à la grille d'accrochage. Modifiez la précision de la grille d'accrochage en utilisant le roll up dans le coin inférieur droit pour effectuer des mouvements plus ou moins grands.
* Maintenant, nous alignons le trou au centre droit / gauche de la forme de base à l'aide de l'outil d'alignement (menu supérieur droit).
* Après le placement final, n'oubliez pas de regrouper la forme de base et la forme du trou pour rendre le trou actif.
* Utilisez l'outil Texte pour écrire votre nom. Après cela, nous le plaçons sur la forme de base.
* Nous pouvons modifier les dimensions de notre texte pour l'adapter à la forme de base de notre trousseau de clés. Utilisez les mêmes conseils pour les changer à l'aide des poignées mais proportionnellement. N'oubliez pas que la hauteur augmente / diminue également.
* Maintenant, nous regroupons tous en utilisant l'outil de groupe (menu en haut à droite).
* Enfin, nous changeons le nom de notre design. Tinkercad a donné un hasard comme "Spectacular Rottis-Luulia" qui (peut-être) ne signifie rien pour nous.
* Revenons à notre tableau de bord en utilisant le logo Tinkercad (en haut à gauche).

## Allons-y!

Nous mettons nos étudiants au défi de créer leur propre trousseau de clés (ou peut-être quelque chose de différent) qui comprend au moins:
* Deux formes différentes.
* Un trou actif.
* Un texte.
* Dimensions réelles (et proportionnelles si nécessaire).
* Formes bien alignées et groupées.

## Devoirs
Terminez le design que nous venons de commencer en classe. Partagez le design.
## Références
* [Thingiverse. Keyword: keyring](https://www.thingiverse.com/search?q=keyring&dwh=125e4bb44042b6e).
* [Tinkercad Gallery](https://www.tinkercad.com/things)
* [Tinkercad Blog](https://blog.tinkercad.com/)