---
title: 'Les créateurs pensent différemment'
objectives:
    - 'Fournir une introduction à la conception du projet'
    - 'Faciliter les ressources et les éléments clés de la gestion de projet'
    - 'Découvrir les types d''activités pouvant être réalisées dans le domaine de la fabrication numérique'
    - 'Pour motiver le développement d''une activité ou d''un projet au sein d''un espace maker ou pédagogique'
---

## La limite est le ciel?
Il faut qu'on parle. Après avoir vu la prochaine vidéo sur l'industrie de l'impression 3D, il est temps de réfléchir: ce n'est pas seulement pour les milliardaires, n'est-ce pas? Y a-t-il quelque chose d'inspirant pour faire grandir des projets sociaux?
* [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs).
* [15 Things You Didn't Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E).

## Projets d'impression 3D
Les deux premières vidéos sont liées à l'apprentissage par projet en tant que stratégie éducative, de la théorie à la pratique. Plus lié à une personne qui a expérimenté une idée et l'a poussée à l'objectif en utilisant l'impression 3D comme outil principal ... et quelques applications inattendues. Peut-être pas facile, peut-être pas bref.
* [Project Based Learning: Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw).
* [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g).
* [3D Printing Fashion: How I 3D-Printed Clothes at Home](https://www.youtube.com/watch?v=3s94mIhCyt4).
* [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE)

Chaque personne doit préparer la présentation d'une activité ou d'un projet basé sur la conception et l'impression 3D. Des objectifs pédagogiques et une méthodologie seront appréciés.

Chacun disposera de 2 minutes pour le présenter, puis recevra les commentaires du reste du groupe sur les aspects à améliorer ou sur le potentiel de l'idée.
## Devoirs
Rechercher un espace ou un lieu où l'idée d'un projet ou d'une activité pourrait être mise en œuvre
## Références
* [Project Based Learning: Why, How, and Examples](https://www.youtube.com/watch?v=EuzgJlqzjFw).
* [Students make prosthetic hand for classmate using 3D printer](https://www.youtube.com/watch?v=ymxcKDuH87g).
* [3D Printing Fashion: How I 3D-Printed Clothes at Home](https://www.youtube.com/watch?v=3s94mIhCyt4).
* [Dinner is printed: is 3D technology the future of food?](https://www.youtube.com/watch?v=B0Ty6wgM8KE)
* [How to get money on 3D Printing business - 10 Ideas](https://www.youtube.com/watch?v=kIdSftmUENs).
* [15 Things You Didn't Know About the 3D Printing Industry](https://www.youtube.com/watch?v=L5_a6diPI0E).