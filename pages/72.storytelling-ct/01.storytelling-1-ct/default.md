---
title: 'Una introducció a DS: antecedents i avantatges'
length: '300 min'
objectives:
    - 'Descobrir el fons, la història, les funcions i els passos de DS'
    - 'Explorar el potencial de DS com a mètode per explicar històries mitjançant l''ús de tecnologies digitals obertes'
    - 'Reconèixer què és i què no és una història digital'
    - 'Empoderar les habilitats del segle XXI'
---

## Introducció
### Ice breaker: narració creativa
L’entrenador començarà la introducció al mòdul demanant als participants que treguin una targeta Dixit a l’atzar i que n’expliquin una història. A continuació, es demana als participants que treballin en grups reduïts i treguin una targeta aleatòria. L’objectiu del grup és construir col·lectivament una història amb la seva targeta Dixit. Les respostes es documentaran i es reutilitzaran per personalitzar el material de formació.
## L’art de la narració digital: història i marc
“La narració digital és la manifestació de l’antic art de la narració. Utilitza suports digitals per crear històries visuals per mostrar, compartir i conservar en el procés com a història. Les històries digitals obtenen el seu impacte teixint imatges, música, narrativa i veu per donar profunditat i dimensió a la narració. Mitjançant l’ús d’Internet i altres formes de distribució digital, es poden veure històries digitals a través de distàncies i límits per crear noves comunitats a través d’un sentit de significat compartit (Marsha Rossiter i Penny A. Garcia, Universitat de Wisconsin, EUA 2008).

El moviment DS va ser iniciat per Joe Lambert i la difunta Dana Atchley, que estaven interessats a fer que la producció de mitjans estigués disponible per a la "gent normal". Des dels anys noranta, Lambert i els seus col·legues del _Center for Digital Storytelling_ (Berkley, CA) han continuat experimentant amb el Digital Storytelling Workshop.

A Europa, el mètode es va introduir formalment el 2003, quan el servei regional BBC Wales ha iniciat el seu propi programa i centre basant-se en les experiències d'altres cursos de narrativa digital realitzats en diversos llocs.
## El que fa que DS sigui únic
El procés de DS es basa en els passos següents:
* Troba-la: la història que és important per a tu.
* Digueu-ho, amb les vostres pròpies paraules i fotografies.
* Compartiu-lo entre vosaltres o amb qualsevol persona a través d'Internet.

A més, a l’hora de produir una història digital, els creadors han de pensar en el propòsit del projecte i elaborar-los amb un objectiu. Un dels punts forts més importants de la DS és la capacitat de desenvolupar el pensament crític i les habilitats de presa de decisions dels aprenents. Aprendre l’habilitat d’explicar històries de la vida ens ajuda a sentir-nos més còmodes en la nostra pròpia vida i incidir en la vida dels que ens envolten. La narració de contes fomenta l’escriptura creativa, el pensament creatiu i la resolució de problemes. Així doncs, DS pot oferir als participants la possibilitat de conèixer-se millor a si mateixos i els uns als altres. També pot ser crucial per compartir i col·laborar, ja que permet als usuaris treballar en grup, aprenent els uns dels altres.

DS aporta grans avantatges. Aquesta és la raó per la qual la metodologia DS s'utilitza en una àmplia gamma de contextos, des de l'educació (formal i no formal) fins a l'empresa, fins a propòsits individuals, autodesenvolupament i millora de les habilitats creatives.

La creació de pel·lícules curtes combinant imatges basades en ordinador, text, narració d’àudio gravada, videoclips i música per presentar informació sobre diversos temes també és crucial en termes de poder. De fet, DS pot ser una potent eina tecnològica per promoure la competència amb tecnologia i fluïdesa digital.
## Treball a casa (autònom)
Tria el tema de la història i dóna resposta a les preguntes següents:
* Quina és la part més rellevant de la història? Per què?
* És comprensible la història?
* Quina durada té la història?

Després, estructura la història responent a aquestes preguntes:
* Qui són els personatges de la història (Qui)?
* De què tracta la història (Què)?
* On transcorre la història (On)?
* Quan té lloc la història (Quan)?
* Quin és el punt de partida de la història (per què)?

## Referències
* [Wikipedia](https://en.wikipedia.org/wiki/Digital_storytelling).
* [English and Digital Literacies](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf).
* [The history of digital storytelling](https://digitalistortenetmeseles.hu/en/history/).
* [Introduction to Digital Storytelling](https://opencourses.uoa.gr/modules/document/file.php/ENL10/Instructional Package/PDFs/Unit3a_Digital_storytelling.pdf).
* [StoryCenter: What’s your Story?](https://www.storycenter.org/about).
* [Digital Storytelling Toolkit](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf).
* [Learning through Arts and Technology](https://www.faae.org/learning-through-arts-and-technology-digital-storytelling).