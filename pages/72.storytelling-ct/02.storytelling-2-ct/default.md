---
title: 'Crea les teves potents històries digitals tecnologies digitals obertes!'
length: '420 min'
objectives:
    - 'Escriure un guió i realitzar un guió gràfic'
    - 'Crear i recopilar materials rellevants per a la història digital (imatges, veu, música, sons, textos, títols) mitjançant tecnologies digitals obertes'
    - 'Gravar i editar àudio digital'
    - 'Cercar i baixar sons i música sense drets d''autor'
    - 'Reconèixer el material de llicència gratuït al web'
    - 'Compartir amb seguretat la teva història digital als mitjans de comunicació'
    - 'Donar forma a les idees creatives amb aplicacions digitals'
    - 'Produir una història digital (audiovisual)'
    - 'Compartir les històries digitals amb els altres'
---

## Guions gràfics en narració digital
Després de decidir la història que volem explicar i reunir els nostres materials, podem començar a escriure el nostre guió i crear un guió gràfic, un lloc per planificar una història visual en dos nivells: 
1) Temps - Què passa en quin ordre? i 
2) Interacció: com funciona la veu en off i la música amb les imatges o el vídeo?

Fer un guió gràfic és una guia, un mapa per imaginar el flux de la vostra història i esbossar les millors idees de principi a fi. El Storyboard in Digital Storytelling només hi és per ajudar els estudiants a dissenyar una estructura que faciliti la part d’edició que segueix aquesta fase.

Un storyboard és una representació escrita / gràfica de tots els elements que s’inclouran en una història digital. Normalment es crea abans que comenci el treball real sobre la creació de la història digital i s’afegeixi al guió gràfic una descripció escrita i una representació gràfica dels elements de la història, com ara imatges, text, narració, música, transicions, etc.

Els guions gràfics es poden crear de diverses maneres, tant digitalment com manualment, en paper o en pissarra d'artistes. Si es desenvolupen guions gràfics en un ordinador, es poden utilitzar diversos programes de programari, com Microsoft Word, Excel i PowerPoint.
## Selecció i ús d'imatges, animacions i sons per a la vostra història digital
Es tracta de fer de la selecció i la utilització d’imatges, animacions i sons a l’hora de produir una història digital és crucial.
Per crear projectes de suports digitals, és molt important enregistrar una narració bàsica, editar àudio per ajustar el volum, eliminar sons no desitjats, esvair l’entrada o la sortida de l’àudio, etc.

Els drets d'autor també tenen un paper rellevant en la narració digital. Els narradors/es digitals han de ser conscients del tipus d’imatges, sons i altres suports que poden utilitzar sense infringir els drets d’autor dels altres. Molts llocs web, per exemple, proporcionen música sota una llicència de Creative Commons on els músics regalen la seva música per a certs propòsits no comercials o situen la seva música en el domini públic on es pot utilitzar sense cap restricció.

L’objectiu d’aquesta sessió és ajudar els estudiants a utilitzar programari de codi obert (iMovie, Da Vinci Resolve, etc.) i eines per produir històries digitals personals. Els temes principals de la sessió seran els següents:

* Escriptura de guions i storyboarding: propietat i consentiment del públic.
* Paper del so (gravar una veu en off / afegir música).
* Paper de les imatges / digitalitzar els suports.
* Edició de contes i compartició de pel·lícules

## Treball a casa (autònom)
* Utilitzant el guió creat a la sessió anterior, estructureu la vostra història de manera que la cerca d'imatges sigui més fàcil després: utilitzeu un guió gràfic. Pinteu imatges als quadres del guió gràfic o escriviu termes de cerca per a imatges als quadres. El guió serveix per estructurar la història i és una eina per crear la primera connexió entre text i imatge.
* Cerqueu imatges. El nombre total d’imatges hauria de correspondre als guions gràfics dels participants i a la durada dels guions. Seleccioneu imatges utilitzables de col·leccions personals, així com fonts d’Internet. Editeu les imatges seleccionades a [Gimp](https://www.gimp.org/ù) (retallada, desenfocament i ombra).
* Prepareu un guió final, llest per gravar i un guió gràfic final. Aneu a [Audacityteam](https://www.audacityteam.org/) i instal·leu el programari. Enregistreu la vostra pròpia veu i deseu el fitxer.
* Deseu i exporteu pel·lícules i fitxers mitjançant MovieMaker o similar.

## Referències
* [Usos educatius de la narració digital](http://digitalstorytelling.coe.uh.edu/page.cfm?id=23&cid=23&sublinkid=37).
* [Guia de contes de contes digitals](https://uow.libguides.com/ld.php?content_id=42727880).
* [Contes digitals en 10 passos](https://www.socialbrite.org/2010/07/15/digital-storytelling-a-tutorial-in-10-easy-steps/).
* [Brights MOOC](http://www.brights-project.eu/en/results/brights-mooc/).
* [Brights Digital Storytelling](https://vimeo.com/showcase/4856060).
* [Kit d’eines de narració digital](https://www.womenwin.org/files/pdfs/DST/DST_Toolkit_june_2014_web_version.pdf).
* [El meu futur en quarantena](https://vimeo.com/107569681).
* [Com realitzar un storyboard](http://www.brights-project.eu/en/results/brights-mooc/).
* [Plantilla Storyboard](https://www.the-flying-animator.com/storyboard-template.html).
* [Storyboard That](https://www.storyboardthat.com/).
* [Recopilació i producció de materials per a relats digitals](https://mooc.cti.gr/mod/).