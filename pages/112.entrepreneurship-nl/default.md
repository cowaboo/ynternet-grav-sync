---
title: 'Online ondernemerschap met FLOSS tools'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Online entrepreneurship with FLOSS tools'
lang_id: 'Online entrepreneurship with FLOSS tools'
lang_title: nl
length: '10 hours'
objectives:
    - 'Planning van uw bedrijf'
    - 'Levenscyclus van het project'
    - 'Inleiding tot de begrotingen'
materials:
    - 'Computer en internet'
    - 'Basisidee van FLOSS-competenties'
skills:
    Sleutelwoorden:
        subs:
            'Online zakendoen': null
            Ondernemerschap: null
---

## Introduction
De onlineverkoop is nog nooit zo goed geweest en de voorspelling is dat deze blijft groeien tot 2021, zoals de statistieken van het 'Statista - The Statistics Portal' laten zien. Deze vorm van handel is jaar na jaar aan het evolueren en blijft nieuwe fans krijgen. Virtuele winkels zijn niets meer dan etalages vol met producten die te koop zijn, de online verkoopmarkt heeft de noodzaak gemerkt om te investeren in marketingstrategieën om te weten wat klanten van uw producten vinden en het after-sales proces te volgen. 
Elk bedrijf kan online worden gezet, van sportlessen tot het leren van talen, bedrijfsconsultants, lifecoaches, cateraars, ambachtslieden, ...
Online winkelen is een van de populairste online activiteiten ter wereld en alleen al in 2016 steeg de wereldwijde verkoop van e-commerce in de detailhandel tot 1,86 biljoen dollar. Voor het jaar 2021 wordt verwacht dat de online verkoop zal groeien tot 4,48 biljoen dollar. Dat laat zien hoeveel een ondernemer en zijn bedrijf kunnen profiteren van het digitale tijdperk om het bedrijf te laten escaleren, op welk gebied dan ook.

Het totale aantal digitale shoppers wereldwijd neemt ook toe. Het is tussen 2011 en 2012 met meer dan 100 miljoen gegroeid en het bleef maar groeien. En vrouwen hebben hier een groot aandeel in dankzij de flexibele uren die een betere balans tussen werk en privéleven mogelijk maken. In 2012 bleek uit een Greenfield-onderzoek dat vrouwen 58% van de online uitgaven in de VS voor hun rekening nemen.
## Context
Deze module stelt een gebruiker in staat om te weten hoe hij moet lesgeven over:
* Vaardigheden voor het ontwikkelen van een bedrijfsidee tot een echt bedrijf.
* Het aanleren van de SWOT-analyse.
* De leerlingen ondersteunen bij het definiëren en begrijpen van de markt en de verschillende soorten concurrentie waar een bedrijf mee te maken heeft.
* De levenscyclus van het product.
* De marketingmix.
* Basiselementen van het budgetteren aanleren.
 
Methodologie:
* Overleg: Discussiegroepen en discussiefora.
* Uitvoering: opstellen van plannen door studenten.

## Sessies
### Eerste sessie: Business Ideeënontwikkeling en evaluatie door middel van Mindmaps SWOT-analyse
In deze sessie werken we aan de analyse van een bedrijfsidee met behulp van SWOT-analyse en mindmaps. 
### Tweede sessie: Het definiëren van de markt
Deze sessie zal zich richten op de segmentatie van klanten en het begrijpen van de concurrentie. 
### Derde sessie: Budgettering
Wat is een budget en wat zijn tien stappen om een budget te volgen.