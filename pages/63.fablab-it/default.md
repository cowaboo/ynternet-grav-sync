---
title: 'Come gestire un Fablab'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'How to run a FabLab'
lang_id: 'How to run a FabLab'
lang_title: it
Materials:
    - 'Personal computer'
    - 'Internet connexion'
length: '10 ore'
objectives:
    - 'Identificare cos''è un FabLab e la cultura implicata'
    - 'Essere in grado di avviare un''iniziativa FabLab, capire quali sono i primi passi e quali sono le tecnologie più adatte per iniziare. Individuare quali sono le tecnologie aperte e gli strumenti più adatti in un ambiente FabLab'
    - 'Conoscere, individuare e saper utilizzare le fonti di informazione che forniscono risorse utili nel contesto di un fablab'
    - 'Esplorare le comunità di pratica Fablab che utilizzano tecnologie e strumenti aperti'
    - 'Consentire al partecipante di fornire informazioni, risorse, modelli alle community Fablab. Open design. Sostenere i partecipanti di un FabLab nel processo di apprendimento e produzione'
skills:
    'Parole chiave':
        subs:
            FabLab: null
            MakerSpace: null
            HackSpace: null
            'Open Design': null
            'Open Hardware': null
            'Open Software': null
            'Do it Yourself (DIY)': null
            'Learning by doing (LBD)': null
            PBL: null
---

## Introduction
La cultura Fablab è strettamente colllegata alla Maker Culture e al DIY, così come il FLOSS; è sempre qualcosa "in costruzione" e per avviare un Fablab non è necessario avere molti strumenti o macchinari, ma solo l'atteggiamento giusto. Un Fablab è un concetto, e avviare un Fablab è lavorare secondo una filosofia concreta. 

Questo Modulo presenterà ai partecipanti il concetto di ciò che è considerato un Fablab e che tipo di attività vi si può svolgere.
## Contesto
Quando si immagina un Fablab, si pensa a uno spazio di creazione per collaborare, utilizzare tecnologie aperte, sfruttare le risorse del territorio più vicino e le fonti di informazione e le risorse di Internet. Uno spazio in continua crescita, con alcune tecnologie che possono crescere nel tempo. Uno spazio di condivisione di persone che giungono con un atteggiamento positivo e curioso. Un punto di partenza che, se inserito e legato alla comunità locale, attirerà diversi tipi di pubblico e che sarà ricco di creatività e favorirà l'autonomia e l'apprendimento individuale e individualizzato delle persone. 	 	 	
L'obiettivo della sessione è quello di dimostrare e coinvolgere concretamente i discenti su:
* Identificare cos'è un FabLab e la cultura implicata.
* Essere in grado di avviare un'iniziativa FabLab, capire quali sono i primi passi e quali sono le tecnologie più adatte per iniziare. Individuare quali sono le tecnologie aperte e gli strumenti più adatti in un ambiente FabLab.
* Conoscere, individuare e saper utilizzare le fonti di informazione che forniscono risorse utili nel contesto di un fablab. 
* Esplorare le comunità di pratica Fablab che utilizzano tecnologie e strumenti aperti.
* Consentire al partecipante di fornire informazioni, risorse, modelli alle community Fablab. Open design. Sostenere i partecipanti di un FabLab nel processo di apprendimento e produzione.               	 
* Esplorare la comunità locale per creare il legame tra azienda - università - cittadinanza e pubblica amministrazione (promuovere il modello a quadrupla elica).

## Sessioni
### Prima sessione: Cos'è un Fablab?
Fabbricazione digitale, collaborazione, learning by doing DIY (Do it Yourself). Maker culture
### Seconda sessione: Community design e collaborazione
Riconoscere il potenziale di gestire un fablab in una comunità. I bisogni personali e della comunità rispetto alla produzione di massa..
### Terza sessione: Tecnologie Fablab
Esplorare il potenziale dell'uso di hardware e software open source nell'ambito di un Fablab. 4. Individuare le tecnologie adatte per avviare un'iniziativa FabLab.
### Cuarta sessione: Tecnologie Fablab
### Quinta sessione: Primi passi...
Creare il proprio Fablab. Progettare in modo collaborativo una nuova comunità Fablab. 
### Sesta sessione: Peer review del design della comunità Fablab
Prime attività da sviluppare.
