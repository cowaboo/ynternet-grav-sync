---
title: 'Filosofia Scratch: “Imagineu, programeu, compartiu”'
length: '150 min'
objectives:
    - 'Descobrir els fonaments de la Filosofia Scratch: els principis per crear, programar i compartir material'
    - 'Examinar l’ús i els avantatges de la Filosofia Scratch en l’educació d’adults'
---

## Discussió en grup
La persona docent iniciarà la introducció al mòdul preguntant a les persones participants sobre la seva experiència I nivell de programació. Les respostes es recolliran i es reutilitzaran per personalitzar el material d’aprenentatge.
## Imagineu, programeu, compartiu
El llenguatge de programació educativa Scratch va ser desenvolupat pel Lifelong Kindergarten Group al Massachusetts Institute of Technology (MIT) i es proporciona de forma gratuïta.

Scratch és un llenguatge de programació visual basat en blocs i que té una nombrosa comunitat en línia al darrere. Tot i que està dissenyat especialment per a entre 8 i 16 anys, Scratch és utilitzat per persones de totes les edats. Milions de persones creen projectes Scratch en una gran varietat d’entorns.
La primera versió d’Scratch disponible per al públic es va publicar el 2013. La filosofia d’Scratch fomenta la compartició, la reutilització i la combinació de codi, tal com indica el lema de l'equip, "Imagine, Program, Share".

El lema d’Scratch segueix el principi bàsic de crear un projecte: es presenta una idea ("Imagine"), es programa la idea amb Scratch ("Programa"), i després es publica a la comunitat ("Comparteix"). Des del llançament d’Scratch 2.0, el lema ha estat menys evident a tot el lloc web; la portada ja no té el lema, sinó una descripció del què és Scratch.

La programació amb Scratch permet a les persones usuaris desenvolupar els seus propis projectes i utilitzar el projecte d’una altra persona. Els projectes creats i remesclats amb Scratch estan llicenciats Creative Commons “Reconeixement-Compartir igual”. Scratch concedeix automàticament els crèdits a l’usuari que ha creat el projecte i programa original.

Per acabar la sessió, la persona docent facilitarà un moment de debat on s’animi els participants a expressar les seves preguntes, dubtes, idees i sentiments envers els temes tractats.

## Treball a casa (autònom)
Les persones participants han d’explorar el lloc web d’Scratch (https://scratch.mit.edu/) en l’idioma que trïin. Aleshores, se'ls demana que mirin els vídeos i que prenguin notes mentre vegin els vídeos:

- 5 raons per les quals heu d'aprendre a programar amb Scratch
- L’Scratch per a un aprenentatge creatiu

Es demana a les persones participants que expressin les seves pròpies opinions sobre el què consideren fonamental de la Filosofia Scratch i els seus principals avantatges.

## Referències
* [Scratch Wiki](https://en.scratch-wiki.info/wiki/Scratch_Wiki)
* [Viquipèdia](https://en.wikipedia.org/wiki/Scratch_(programming_language))
* [Lloc web de Scratch](https://scratch.mit.edu/about)