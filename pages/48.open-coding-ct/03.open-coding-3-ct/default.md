---
title: 'L''efectivitat de Scratch en la creació d’un entorn de programació col·laborativa'
length: '150 min'
objectives:
    - 'Identificar els avantatges de la comunitat en línia de Scratch'
    - 'Fomentar l’ús compartit, la reutilització i la combinació de codi'
    - 'Afavorir l''aprenentatge col·laboratiu dels estudiants'
---

## Uneix-te i beneficia't de la comunitat en línia de Scratch
Presentació i anàlisi dels beneficis de la comunitat en línia d’Scratch. Té una comunitat activa i nombrosa, dissenyada per a persones usuàries que vulguin crear i compartir projectes.

A més de compartir un projecte amb l’editor en línia d’Scratch, les persones usuàries també poden veure i combinar amb el projecte d’un altre usuari, inspirant-se en altres persones al mateix temps.

Per acabar la sessió, la persona docent facilitarà un moment de debat on s’animi els participants a expressar les seves preguntes, dubtes, idees i sentiments envers els temes tractats.
## Treball a casa (autònom)
- Vés a http://www.scratch.mit.edu i inicia la sessió. Selecciona el títol del projecte per compartir i fes-ho accessible per totes les persones usuàries.
- Vés a la Biblioteca d’Scratch, tria un projecte i combina’l.
 
## Referències
* [Directrius de la comunitat Scratch](https://ca.scratch-wiki.info/wiki/Community_Guidelines)
* [Compartició de projectes](https://ca.scratch-wiki.info/wiki/Project_Sharing)
* [Combinació de projectes](https://ca.scratch-wiki.info/wiki/Remix)