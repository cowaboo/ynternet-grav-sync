---
title: 'Codi obert amb la programació visual d’Scratch'
length: '150 min'
objectives:
    - 'Descobrir els avantatges d’Scratch per al desenvolupament d’habilitats del segle XXI'
    - 'Dissenyar històries, jocs i animació interactius'
    - 'Aprendre de manera autònoma a partir de tutorials i vídeos'
    - 'Accedir a recursos de suport per a Scratch'
---

## Discussió en grup sobre "Programació en Scratch?"
El formador començarà la introducció al mòdul preguntant a les persones participants sobre la seva experiència en llenguatges de programació.
## Dissenyar, crear i inventar amb nous suports
“La fluïdesa digital no només requereix la possibilitat de xatejar, navegar i interactuar, sinó també la capacitat de dissenyar, crear i inventar amb nous suports” (...). La capacitat de programar proporciona avantatges importants. En particular, la programació és compatible amb el "pensament computacional", ajudant-nos a aprendre estratègies importants de resolució de problemes i de disseny aplicables a àmbits fora de la programació. (Scratch: Programació per a tots).
Presentació i anàlisi dels avantatges de la programació. La programació amb Scratch ajuda les persones usuàries a pensar de manera creativa, raonar sistemàticament i treballar col·laborativament: habilitats essencials per a la vida al segle XXI.

- [Aprendre a programar, programar per aprendre](https://www.youtube.com/watch?v=Ok6LbV6bqaE)
- [Scratch: Desfermar la creativitat mitjançant la programació](https://www.youtube.com/watch?v=pwtN0DHB7RY) 
- [Per què és tan important la programació](https://www.forbes.com/sites/quora/2018/08/16/why-is-coding-so-important/#3866fc362adc)
- [Què és el pensament computacional?](https://www.youtube.com/watch?time_continue=1&v=sxUJKn6TJOI)

## Anem a programar!
Introducció als conceptes bàsics d’Scratch (navegueu per la interfície: àrea d'etapa, paleta de blocs i una àrea de codificació per situar i ordenar els blocs en scripts executables; crear personatges –sprites - i fons, etc).

Activitat 1. El gat ens dóna la benvinguda
Les persones participants es registren al lloc web de Scratch (procediment senzill).
Es demana a aquestes que inicïin la sessió al seu compte i que realitzin les tasques següents:
1. Ves al lloc web d’Scratch (https://scratch.mit.edu/) i inicia la sessió al teu compte.
2. Crea un projecte / fitxer anomenat “El gat ens dóna la benviguda”
3. Estableix el fons i dóna instruccions a l’sprite (personatge) del gat amb els blocs de codi de colors de l’Scratch per preguntar-te "Com et dius?", i després que et doni la benvinguda amb “Molt de gust + nom a la resposta”
4. Quan hagis acabat, obre l’informe de l’alumne “El gat ens dóna la benvinguda” i respon.

Com a suggeriment, el professor pot proporcionar als estudiants l'[enllaç al projecte compartit](https://scratch.mit.edu/projects/306876279/)

Activitat 2. El gat pregunta
Es demana a les persones participants que facin les tasques següents:
1. Ves al lloc web d’Scratch (https://scratch.mit.edu/) i inicia la sessió al teu compte.
2. Crea un projecte / fitxer anomenat “El gat pregunta”
3. Posa el fons i indica al personatge del gat digui "Hola" utilitzant els blocs de codi de color, i després que faci la pregunta "Quan va descobrir Amèrica Cristòfor Colom?". Si la resposta és incorrecta, el gat diu "Reintenta". Si la resposta és correcta (1492) el gat diu "Molt bé!!"
4. Quan hagis acabat, obre l’informe de l’alumne “El gat pregunta” i respon.

Com a suggeriment, el professor pot proporcionar als estudiants l'[enllaç al projecte compartit](https://scratch.mit.edu/projects/306876445/)

## Accés a suport i recursos per a Scratch 
Les persones participants aprendran a codificar amb Scratch amb el suport de tutorials i recursos en línia.

- [Aprendre a programar, codificar per aprendre](https://scratch.mit.edu/projects/31407152)
- [Tutorial de Scratch: feu el vostre primer programa](https://www.youtube.com/watch?v=VIpmkeqJhmQ)
- [Tutorial d’introducció](https://scratch.mit.edu/help/videos/#Introductive%20Tutorials)

## Treball a casa (autònom)
Es demana a les persones participants que facin les tasques següents:
1. Ves al lloc [web d’Scratch](urlhttps://scratch.mit.edu/) i inicia la sessió al teu compte.
2. Crea un projecte / fitxer anomenat “Ratolí a la graella”.
3. Tria un ratolí com a personatge i una graella com a teló de fons.
4. Fes que el ratolí es mogui amb les fletxes de cursor del teclat dintre de la graella: cap amunt o avall, cap a la dreta o a l'esquerra. Quan hagis acabat, obre l’informe de l’alumne “Ratolí a la graella” i respon.
Com a suggeriment, la persona docent pot proporcionar a les persones participants l'enllaç al [projecte compartit](https://scratch.mit.edu/projects/306876524/)

Es demana a les persones participants que cerquin tutorials en línia necessaris per dur a terme la tasca.
Per acabar la sessió, la persona docent facilitarà un moment de debat on s’animi les persones participants a expressar les seves preguntes, dubtes, idees i sentiments cap a les activitats pràctiques que es duen a terme.

## Referències
* [Com començar amb Scratch](https://cdn.scratch.mit.edu/scratchr2/static/__6fc1c81d1c536d64cab16bd429100958__/pdfs/help/Getting-Started-Guide-Scratch2.pdf)
* [Tutorials per ajudar-vos](https://scratch.mit.edu/studios/174756/)
* [Programació amb Scratch](https://anthsperanza.com/2018/05/01/scratch-educator-guide/)