---
title: 'Begrepen? Begrip en gebruik van FLOSS-vergunningen'
objectives:
    - 'Het in kaart brengen en verkennen van de belangrijkste concepten achter FLOSS-vergunningen'
    - 'De onderlinge afhankelijkheid van verschillende FLOSS- en niet-FLOSS-vergunningen te begrijpen'
    - 'Om de ethische, juridische, sociale, economische en impactargumenten voor en tegen FLOSS-vergunningen te ontdekken'
    - 'Om te beslissen welke licentie/platforms/ het meest nuttig zijn voor zichzelf en hun project, en voor de gemeenschap'
---

## Groepsdiscussie
De trainer start de introductie van de module door de deelnemers te vragen naar hun huidige en eerdere ervaringen met auteursrechten en copyleft. De antwoorden worden gedocumenteerd en vervolgens hergebruikt om het trainingsmateriaal aan te passen.
## De opkomst van auteursplicht
Netwerken vormen de nieuwe sociale morfologie van onze samenlevingen, en de verspreiding van de netwerklogica verandert de werking en de resultaten in processen van productie, ervaring van macht en cultuur aanzienlijk", zegt filosoof Manuel Castells. Dit proces is tegelijkertijd een bedreiging en een kans rond "het omsluiten van de commons of the mind" (zie: J. Boyle, The Public Domain: Enclosing the Commons of the Mind (New Haven, CT: Yale University Press, 2008), 221, http://www.thepublicdomain.org/download).
Een tegenhanger voor het insluiten van de commons of the mind is de opkomst van copyleft-licenties. Een auteurslicentie maakt gebruik van het auteursrecht om de openheid van het intellectueel eigendom te behouden. De eerste licentie, de GNU General Public Licenses (GNU GPL), is ontstaan uit de FLOSS-beweging. Een open content licentiesysteem van Creative Commons (CC) deed voor culturele inhoud wat de GNU GPL deed voor software; de openheid ervan behouden. Het doel van deze licenties is om het gebruik van informatie te maximaliseren en tegelijkertijd de transactiekosten te minimaliseren.
## Onderscheid maken van een copyleftlicentie
In deze activiteit zullen we de documentatieresultaten van de introductiesessie opnieuw bekijken om een auteurslicentie te onderscheiden.
## Huiswerk
Deelnemers moeten het Wikipedia Copyleft-artikel (https://en.wikipedia.org/wiki/Copyleft) bestuderen in de taal van hun keuze en vervolgens de Externe Links-sessie met een eigen voorbeeld afronden.
## Referenties
* Een verzameling van gerelateerde bronnen is beschikbaar op een collectieve - sociale bookmarking ruimte : https://groups.diigo.com/group/e_culture
* CNU - Licenties - Copyleft, https://www.gnu.org/licenses/copyleft.html
* De ethische open source-licentie: https://firstdonoharm.dev