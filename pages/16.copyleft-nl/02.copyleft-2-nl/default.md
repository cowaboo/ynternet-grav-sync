---
title: 'FLOSS-vergunningen in real life scenario''s'
objectives:
    - 'Voorbeelden te geven van FLOSS-vergunningen in reële scenario''s'
    - 'Het effect van het gebruik van FLOSS-vergunningen te analyseren'
    - 'Het verband te ontdekken met het beleidskader van de EU op het gebied van het auteursrecht'
    - 'Het motiveren van een educatieve beleidsbijdrage op het gebied van FLOSS'
---

## Groepsdiscussie over "hoe wordt uw project vandaag gelicentieerd?
De trainer begint de introductie van de module door de deelnemers te vragen naar de licentie-initiatieven om hen heen - meestal afkomstig van andere organisaties of de EU. De antwoorden worden gedocumenteerd en vervolgens hergebruikt om het trainingsmateriaal aan te passen.
## FLOSS in real life scenario's
Hoe zijn de onderstaande projecten gelicenseerd?
* [Boerderij Hack](https://farmhack.org/tools)
* [Open motoren](https://www.openmotors.co/)
* [Fold-it](https://fold.it/portal/)
* [Open Insulineproject](https://openinsulin.org/)
* [Wikihuis](https://www.wikihouse.cc/)
* Elk wikipedia-artikel

Meer licenties en concepten:
* [De Peer Production License](https://wiki.p2pfoundation.net/Peer_Production_License)
* [Het geval van Copyfarleft](https://wiki.p2pfoundation.net/Copyfarleft)

## Een EU-beleidskader voor FLOSS-vergunningen
De presentatie en analyse van de belangrijkste beleidsinitiatieven van de EU op het gebied van FLOSS, met inbegrip van de openbare vergunning van de Europese Unie (de "EUPL"), is van toepassing op de werkzaamheden (zoals hieronder gedefinieerd) die onder de voorwaarden van deze vergunning worden verricht. Elk gebruik van het werk, anders dan toegestaan onder deze licentie, is verboden (voor zover dit gebruik valt onder een recht van de houder van het auteursrecht op het werk). Compatibiliteit en interoperabiliteit
https://joinup.ec.europa.eu/collection/eupl/news/understanding-eupl-v12
https://joinup.ec.europa.eu/collection/eupl/news/eupl-or-gplv3-comparison-t
## Uw licentie om de wereld te redden
Sluit u aan bij een groep om uw beleidsvoorstel voor een licentie te ontwerpen en te delen, waarvan u denkt dat deze de wereld een betere plek om te leven zal maken. Kies een thema en probeer manieren en oplossingen te vinden voor het licenseren van producten met uw intentie. We zullen platformontwerptechnieken gebruiken om ideeën en uitdagingen voor deze licenties te documenteren.
## Huiswerk
Zie in de volgende video: Vrije software, vrije maatschappij: [Richard Stallman op TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM) en plaats dan in de commentaarsectie een andere video die uitlegt - toont vergelijkbare argumenten. Deel je gedachten over de nieuwe video
## Referenties
* "Hoe kies je een licentie voor je eigen werk?" Free Software Foundation's Licensing and Compliance Lab, https://www.gnu.org/licenses/license-recommendations.html#libraries.
* Vrije software, vrije maatschappij: [Richard Stallman op TEDxGeneva 2014](https://www.youtube.com/watch?v=Ag1AKIl_2GM)
* Het [open data instituut](https://theodi.org)