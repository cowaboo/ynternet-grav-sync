---
title: 'FLOSS-vergunningen voor open onderwijs, kunst en collectief leren'
objectives:
    - 'Een kritische analyse maken van het belang van FLOSS-vergunningen voor open onderwijs, kunst en collectief leren'
    - 'Het gebruik en de voordelen van FLOSS-vergunningen in het onderwijs en de kunst te onderzoeken'
    - 'Het bevorderen van de actieve en creatieve betrokkenheid van de lerenden door middel van FLOSS-vergunningen'
    - 'De deelnemers te motiveren om FLOSS-vergunningen in te voeren in hun huidige en toekomstige onderwijsactiviteiten'
---

## Groepsdiscussie: Inleiding tot de sessie
De deelnemers zullen brainstormen over licenties en onderwijsinitiatieven in hun omgeving. De antwoorden worden gedocumenteerd en vervolgens hergebruikt om het trainingsmateriaal op maat te maken.
## FLOSS-vergunningen in kunst en onderwijs
In 1999 verscheen de roman Q onder de naam Luther Blissett, voorheen bekend als de verzamelnaam van een Italiaans media grappenmakersproject. Dit allegorische verslag van de Italiaanse subcultuur in de vorm van een historische thriller die zich afspeelt in het 16e eeuwse Italië, werd Q een nationale bestseller nr. 1 en verscheen vervolgens in Franse, Duitse en Engelse vertalingen. Het is duidelijk dat de verkoop helemaal niet te lijden had onder het feit dat de afdruk van het boek iedereen toestond het boek vrijelijk te kopiëren voor niet-commerciële doeleinden. Het boek werd niet uitgegeven door een ondergrondse uitgeverij, maar door de gerenommeerde uitgeverijen Einaudi in Italië, Editions du Seuil in Frankrijk en Piper in Duitsland, die het blijkbaar niet erg vonden om de traditionele, auteursrechtelijk toegestane distributiemodellen op te geven voor een veelbelovende publicatie.

Een ander voorbeeld, in een ander opzicht, is de Europese (als open onderwijs en kunstbibliotheek). Het Europeana Licensing Framework standaardiseert en harmoniseert bijvoorbeeld informatie en praktijken met betrekking tot rechten in het kader van een overeenkomst voor gegevensuitwisseling op maat. Het DEA structureert de relatie tussen Europeana en haar gegevensverstrekkers. Het DEA stelt vast dat Europeana metagegevens publiceert die het van zijn gegevensverstrekkers ontvangt onder de voorwaarden van de Creative Commons Zero Universal Public Domain Dedication (CC0). Lees meer over CC0 en de richtlijnen voor gegevensgebruik.

Kennis en creativiteit zijn middelen die, om trouw te zijn aan zichzelf, vrij moeten blijven, d.w.z. een fundamentele zoektocht blijven die niet direct verband houdt met een concrete toepassing. Creëren betekent het onbekende ontdekken, betekent een realiteit uitvinden zonder rekening te houden met het realisme. Het object (ive) van de kunst is dus niet gelijkwaardig aan het afgewerkte en gedefinieerde kunstobject. Dit is het basisdoel van deze Free Art License: het bevorderen en beschermen van de kunstpraktijk die bevrijd is van de regels van de markteconomie.
[Free Art License Preamble versie 1.2](http://www.copyleftlicense.com/licenses/free-art-license-(fal)-versie-12/view.php)
## Bekijk het gesprek van Gwenn Seemel: [Ter verdediging van de imitatie](https://www.tedxgeneva.net/talks/gwenn-seemel-in-defense-of-imitation/)
Bestudeer, documenteer en bespreek haar argumenten. Ontdek hoe de kunstenares haar brood verdient. Maak een gemeenschappelijk document met alle deelnemers, deel het met anderen.
## (alternatief) Collectieve scenario's voor OEP - Open Educatiescenario's
De activiteit zal zich richten op een voorbeeldige selectie van toepassingen van het paradigma van open onderwijspraktijken.
## Huiswerk
Zoek in de [Zenodo](https://zenodo.org/) open repository en vind je drie favoriete papers over FLOSS en onderwijs. Documenteer en/of plaats er een commentaar op.
## Referenties
* Gids voor het openen van contentlicenties. Door Lawrence Liang, online beschikbaar op https://monoskop.org/images/1/1e/Liang_Lawrence_Guide_to_Open_Content_Licenses_v1_2_2015.pdf.
* Steel deze film en projecten: http://www.stealthisfilm.com/Part2/projects.php
* Biella Coleman, De sociale creatie van productieve vrijheid: Vrije Software Hacking en de herdefinitie van Arbeid, Auteurschap en Creativiteit, http://www.healthhacker.com/biella/proposal2.html/
* Lijm (v. t.) 1. 2. Om een auteur of uitgever te betalen voor het uitgeven van een Creative Commons ebook. https://unglue.it/
* [DIS](https://dis.art/) 
* De toekomst van het leren is veel belangrijker dan de toekomst van het onderwijs. DIS is een streaming platform voor entertainment en onderwijs - edutainment. We schakelen vooraanstaande kunstenaars en denkers in om het bereik van belangrijke gesprekken te vergroten door middel van hedendaagse kunst, cultuur, activisme, filosofie en technologie. Elke video stelt iets voor - een oplossing, een vraag - een manier om na te denken over onze veranderende realiteit.