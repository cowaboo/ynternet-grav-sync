---
title: 'Editar Wikidata'
length: '180 minutos'
objectives:
    - 'Comprender cómo se recopila y se añade el contenido de Wikidata en la base de datos'
    - 'Aprender a añadir contenido a Wikidata y a hacer adiciones masivas a Wikidata'
    - 'Descubrir como colabora la comunidad Wikidata para construir la base de datos'
    - 'Comprender cómo se incorporan los procesos de calidad al proyecto'
    - 'Conocer las maravillas de obtener respuestas a preguntas complicadas con sistemas de consulta'
---

## Introducción a la edición
Preguntas sobre la última sesión y discusión sobre los deberes.
### Edición general y revisión de la estructura de datos
La sección "Introducción a la edición" normalmente garantiza que los participantes puedan:
* Buscar el botón de edición y contribuir a la etiqueta y en la descripción de un elemento de Wikidata.
* Buscar el botón de edición de una propiedad existente, modificar con un calificador y añadir referencias a un elemento de Wikidata.

Revisión de la parte fundamental de un elemento de Wikidata (etiqueta, identificadores, declaraciones, calificativos, referencias, propiedades y valores)
Demostración de cómo editar.
La herramienta Discovery of Recoin, destinada a identificar la información que falta sobre un elemento.

### Actividad de edición (en el ordenador)
* Con los tutoriales de Wikidata: cree un elemento y añada una declaración
* A continuación, cree o mejore un elemento
* Analizar algunos elementos con Recoin
* Completar más elementos (los estudiantes también deberían añadir referencias y fuentes)
* Feedback: informe colectivo de cada estudiante sobre lo que se hizo, las dificultades alcanzadas, las preguntas, los pensamientos.

### Consejos
* Debemos demostrar que se editó un elemento que se había identificado antes del evento, incluida la adición de una etiqueta, una descripción y una propiedad común para este tipo de elemento. Recuerda indicar donde se encuentran los botones de edición para cada tipo de edición.
* Debemos demostrar cómo añadir una referencia a una declaración existente. Gran parte del valor de Wikidata a largo plazo proviene de la capacidad de referenciar declaraciones a fuentes fiables de material. Al igual que cuando se demuestra la referencia durante los talleres de edición de Wikipedia, es importante preparar la referencia con antelación. Recuerda que debes describir los campos más habituales para referencias a Wikidata (si tiene aparatos personalizados para copiar referencias o rellenar referencias mediante Citoid, asegúrese de describir cómo cambian la interfaz).
* Si tienes instalados gadgets o herramientas personalizados en su cuenta de Wikidata, considere desactivarlos o discutir en la que su interfaz es diferente de la interfaz estándar de Wikidata.

### Lecturas
* [Wikidata:Planning a Wikidata Workshop](https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_Workshop/Adding_basic_statements_to_Wikidata_items_by_engaging_newbies)
* [Wikidata Tours](https://www.wikidata.org/wiki/Wikidata:Tours)

## Procesos de edición de Wikidata
El objetivo de esta sección es entender mejor los procesos de edición de Wikidata, en particular insistiendo en la transparencia y la posibilidad de hacer un seguimiento de cada edición.

### Presentación de las funciones de edición y colaboración
Hacer un recorrido por varias funciones de Wikidata, útiles para el editor: página de usuario, TalkPage, lista de seguimiento, contribuciones propias.
Hacer un recorrido por las funciones útiles para la comunidad para hacer un seguimiento de la actividad en el sitio: RecentChange, página del historial del ítem, páginas de otros usuarios, interacción con los usuarios a sus páginas de discusión , lista de otras contribuciones de los participantes.
Presentación de la estrategia Wikidata basada en SoftSecurity y HardSecurity.
Demostración de las contribuciones aportadas por los participantes durante la actividad anterior.

### Actividad (en el ordenador)
Cada estudiante toma el tiempo para explorar lo que se acaba de describir. El formador puede pedir a cada participante que escriba un mensaje en la página de discusión de otro participante, etc.

## Calidad de datos y evaluación de datos
### Presentación de calidad y evaluación
* Proporcionar una visión general del contenido que ya está disponible en Wikidata.
* Cómo evaluar la calidad de un artículo.
* Utilización de elementos ShowCase para crear un elemento.
* Explorar más fuentes y citas (como tratar hechos conflictivos, que es una buena fuente de datos, que no).
* Descubrimiento de herramientas para ayudar a llenar vacíos, en profundidad y en amplia extensión (los ejemplos pueden incluir tabernáculo, Mix'n'Match, el juego Wikidata, The
* Juego distribuido, Wikishootme, Terminator (a elegir en función del público).
* Introducción a una herramienta de edición masiva: quickstatements.
* La presentación incluirá debates, preguntas y respuestas.

### Consejos
Las demostraciones suelen incluir:
* Mix'n'Match: las entradas de la lista de herramientas de algunas bases de datos externas y permiten a los usuarios hacerlas coincidir con los elementos de Wikidata
* WikiShootMe: es una herramienta para mostrar artículos de Wikidata, artículos de Wikipedia y imágenes de Commons con coordenadas, todo al mismo mapa.

Facilita la adición de imágenes a los proyectos de Wikimedia.
* Juegos de Wikidata: conjunto de juegos para añadir declaraciones rápidamente a Wikidata.

### Lecturas
* [Wikidata Game](https://tools.wmflabs.org/wikidata-game/)
* [Distribution](https://tools.wmflabs.org/wikidata-game/distributed/)
* [Mix-n-match](https://tools.wmflabs.org/mix-n-match/#/)
* [WikiShootMe](https://tools.wmflabs.org/wikishootme/#lat=43.35953739293013&lng=5.325894166828497&zoom=15)
* [Quick Statements](https://tools.wmflabs.org/quickstatements/#/)

## Introducción básica a las consultas Wikidata mediante Query Helper
Los participantes podrán:
* Comprendéis como leer y modificar una consulta básica de Wikidata mediante SPARQL y el Asistente de consultas.
* Comprender cómo construir consultas súper básicas que retornen diversas variables y etiquetas.
* Comprendéis qué tipos de visualizaciones están disponibles para utilizar las consultas de Wikidata.
* ¿Qué son las consultas y qué es SPARQL

### Descubrimiento del servicio de consulta de Wikidata
Demostración con una consulta sencilla a partir de ejemplos y modificación de la consulta inicial. Muestra diferentes visualizaciones.

### Actividad (en el ordenador portátil si el tiempo lo permite)
Los participantes juegan con el Servicio de consultas: explorando los ejemplos con diferentes opciones de visualización, modificando los ejemplos para los más atrevidos.

## Resumen y preguntas
Para terminar la sesión, el formador o formadora facilitará un momento de debriefing donde los participantes se animan a expresar sus preguntas, dudas, ideas y sentimientos sobre los temas tratados.
Introducción a la próxima sesión.
La siguiente sesión se dedicará a consultas.

## Trabajo en casa (autónomo)
* Busque 3 ideas de preguntas de consulta para enviar las próximas sesiones
* Lea [Glosario de Wikidata] (https://www.wikidata.org/wiki/Wikidata:Glossary)

## Referencias
* [Planificació] (https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop).
* [Professional] (https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata).
* [Ajuda] (https://www.wikidata.org/wiki/Help:Contents).
* [Eines] (https://www.wikidata.org/wiki/Wikidata:Tools).
* [Recursos educatius] (https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources).