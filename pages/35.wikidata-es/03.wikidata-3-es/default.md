---
title: 'Hacer consultas a Wikidata'
length: '120 minutos'
objectives:
    - 'Evaluar el interés del lenguaje de consulta y las herramientas para obtener respuestas a preguntas complicadas'
    - 'Motivar a los y las participantes para que aprendan a hacer consultas básicas o incluso consultas avanzadas'
    - 'Aprender a hacer consultas a SPARQL'
---

## Consultas
### Entender el servicio de consultas
El servicio de consultas ha sido introducido muy brevemente durante la sesión 2. Se verá más a fondo en esta sesión.
### Descubrir a fondo SPARQL y saber hacer consultas
Sin embargo, con muchas audiencias en situaciones profesionales, es necesario proporcionar un entorno separado para comprender mejor los idiomas SPARQL y cómo crear consultas fuertes.
### Los participantes crean sus propias consultas (en el ordenador)
Permite que el público escriba sus propias consultas con la ayuda del Servicio de consultas
### Consejos
* Recuerda que, para muchos públicos, será la primera vez que trabajan con un lenguaje de consulta (SQL, SPARQL, etc.). Por lo tanto, es muy importante que empiecen por los elementos más básicos y que aumenten de forma incremental la complejidad de la información. Una buena táctica para hacerlo es crear varias consultas de diferente complejidad ante el público. Por ejemplo, consultas, véase Wikidata: Servicio de consultas SPARQL / Construir una consulta.
* Asegúrate de demostrar cómo usar un ejemplo de consulta y modificarlo para adaptarlo a una necesidad diferente. Muchas personas no escribirán inmediatamente consultas de Wikidata desde cero, sino que modificarán las consultas existentes. El asistente de consultas de Wikidata es particularmente útil para el público que modifica consultas.
* Asegúrate de incluir tanto la redacción guiada de consultas, donde los instructores guían la redacción de la consulta, como un período de tiempo para que el público pueda escribir sus propias consultas. Hacer que los participantes escriben su propia consulta, refuerza el proceso y las habilidades de modificar o escribir una consulta, de manera que puedan hacerlo con confianza en el futuro.
* Si el público es mayoritariamente Wikimedians, asegúrese de pasar tiempo mostrándolos como aplicar las consultas de Wikidata a varias herramientas utilizadas por la comunidad Wikimedia, incluidos Petscan, Listeriabot y Histropedia.

## Resumen y preguntas
Para terminar la sesión, la persona a cargo del taller facilitará un momento de debriefing donde los participantes se animan a expresar sus preguntas, dudas, ideas y sentimientos sobre los temas tratados.
## Deberes
Configura una herramienta [campaña en ISA] (https://tools.wmflabs.org/isa/campaigns) y pide a los participantes que participen en la campaña antes de la próxima sesión.
## Referencias
* [Ajuda de consulta] (https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/Wikidata_Query_Help)
* [Exemples] (https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples)
* [Vídeo per a principiants] (https://www.wikidata.org/wiki/File:Querying_Wikidata_with_SPARQL_for_Absolute_Beginners.webm)