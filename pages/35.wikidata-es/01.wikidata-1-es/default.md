---
title: 'Descubrir y entender WikiData'
length: '180 minutos'
objectives:
    - 'Dar una primera impresion de ¿Qué es Wikidata?'
    - 'Explicar como son almacenados los datos en el proyecto y porqué'
    - 'Descubrir la comunidad Wikidata y alguno elementos clave de su gobernabilidad'
    - 'Entender el interfaz y crear una cuenta'
---

## Introducción
### Trabajo en grupo
El formador comenzará la introducción del módulo preguntando a los participantes sobre su experiencia actual y anterior con Wikidata o con cualquier proyecto wikimedia. También se les invitará a compartir información sobre el contexto de su participación en la sesión.
### Hacer una consulta
El objetivo sería ayudarles a ver cómo Wikidata puede afectar de forma pràactica su vida diaria y atraer su atención. Muestre como Wikidata puede ayudar a "responder" a preguntas que antes sería muy difícil hacer en proyectos específicos, en dominio o no de Wikimedia. Considere compartir algunos ejemplos de preguntas a las que Wikipedia teóricamente podría responder, pero que requerirían leer una gran cantidad de pààgines de Wikipedia (u otras fuentes) para recopilar esta información. Adapte las consultas de ejemplo al público.

Se propondrán los resultados de una o dos consultas.
1. Primera demostración
Preguntar a las personas participantes sobre el alcalde de su ciudad. ¿Es una mujer? Si no lo saben, ¿donde pueden encontrar la información? mencionar google, pero también asistentes personales como Siri.
Preguntar a los participantes sobre las ciudades de su país con mujeres alcaldesas. ¿Dónde o cómo pueden encontrar la información?
Si necesitamos la lista de países ordenados por el número de ciudades con alcaldesa... ¿dónde o cómo pueden encontrar la información?
Ejecutar la consulta. "Lista de países ordenados por el número de ciudades con alcaldesa" para demostrar el uso de los datos alojados en Wikidata.

2. Segunda demo (dirigida a un público profesional)
Ejecutar la lista de consultas de artículos académicos con la palabra "Zika" al título.
Usarlo para demostrar el enlace de Wikidata con otras bases de datos: (aquí PubMed_ID)
Muestra la reutilización de terceros a [scholia](https://tools.wmflabs.org/scholia/topic/Q202864).

## Visión general de Wikidata
### Introducción a Wikidata
Normalmente, esta sección garantiza que los participantes puedan:
    1. Comprender los objetivos de Wikidata
    2. Entender cómo se relaciona Wikidata con otros proyectos de Wikimedia, especialmente con Wikipedia
    3. Comprender por qué deberían estar motivados para contribuir o utilizar Wikidata, especialmente en el contexto de su propio trabajo
    4. Los elementos que incluiría son:
* Conceptos clave: descripción de las características básicas de Wikidata (CC0 gratuito, colaborativo, multilingüe y agnóstico del lenguaje, interconectado) y sus objetivos (depósito de conocimiento humano, apoyo para proyectos de Wikimedia y terceros, hub en la web de datos abiertos enlazado) estadísticas de contenido + estadísticas de acceso general.
* Como sirve Wikidata a los proyectos de Wikimedia y como llena importantes huecos de infraestructura para la comunidad. Es decir, Los enlaces interlingüísticos o que ayudan a prevenir las cajas de información de la Wikipedia no están actualizados en los Wikis en idioma local, especialmente por el conocimiento que no es "local" de la lengua (es decir, un político de otra lengua / cultura). Esto debería incluir la historia de la creación de Wikidata y ejemplos de impactos en Wikipedia y Wikimedia Commons (datos estructurados para Commons).
* The Wikidata community (WM DE, quien lo dirige, breve resumen de gobernanza).
* Por qué Wikidata es importante para Internet (ejemplos: la licencia CC0 permite que WD sea la base de conocimiento de muchas IA como Siri o Alexa; los datos de instituciones culturales y la información cívica son más accesibles, transparentes y neutrales).
 
### Consulta y restitución
Algunas preguntas con varias respuestas para seleccionar. Se puede hacer en pareja. Una vez finalizada, devolución colectiva y otras explicaciones si es necesario.
## Estructura de los datos y cómo se almacenan los datos a Wikidata
### Presentación de la estructura de datos a Wikidata
Normalmente, el objetivo de esta sección garantiza que los participantes puedan:
    1. Conozca el modelo básico de datos semánticas de Resource Description Framework (RDF) y cómo se compara con el modelo completo de Wikidata.
    2. Comprender diferentes conceptos relevantes para editar Wikidata y cómo averiguar qué contenido pertenece a estos campos, incluidos: etiqueta, descripción, declaración, propiedad, calificador y referencias.
    3. Comprender la relación entre "información legible por humanos" (es decir, etiquetas y descripciones) y "datos legibles por máquina" (es decir, Q # s y P # s) en la interfaz de Wikidata.

### Elementos clave
* Vocabulario (tipo de bases de datos, identificadores, datos enlazados, datos abiertos).
* Las ventajas y desventajas de los datos enlazados.
* Presentación de los diferentes elementos de Wikidata (ítems, propiedades, declaraciones, identificadores, calificadores, referencias y rango).
* Lectura de Wikidata. Compare una entrada de Wikipedia, una entrada de Wikidata y una entrada de Reasonator. Comprensión de la interfaz de Wikidata.

### Actividad
Individualmente, se pide a los participantes que:
* Vaya a la Q4859840. ¿Qué es? Con que se podría confundir?
* Ahora vaya a la Q1492. En cuántos lugares se utiliza la información sobre W1492?
* Utilice la barra de búsqueda para encontrar un elemento de Wikidata deseado. Explore sus propiedades y referencias. Fíjese en cómo se organiza, que puede faltar, si hay fuentes, etc. Piense como mejorarlo.

### Consejos
* Una de las mejores maneras de introducir el modelo de datos RDF / Triple está fuera de la interfaz de Wikidata. Por ejemplo, a partir de enunciados que describen el mundo en lenguaje (es decir, la Tierra → punto más alto → Montón Everest) transitando a enunciados con etiquetas (es decir, Tierra (Q2) → punto más alto ( P610) → Montón Everest (Q513)) y que muestran instrucciones finales legibles por máquina (Q2 → P610 → Q513). Adaptarse al público.
* Presentación del modelo de datos con un elemento de calidad (muchas presentaciones utilizan Q42 para la broma de la cultura Geek asociada al elemento; se pueden encontrar otros artículos de calidad a Showcase_items.

### Fuentes de información
* https://www.wikidata.org/wiki/Wikidata:In_one_page
* https://www.wikidata.org/wiki/Wikidata:Showcase_items

## Wikidata se crea mediante la colaboración
### Comunidad de Wikidata
El objetivo de esta sección es asegurar que los participantes podrán entender que Wikidata es principalmente una construcción social.
* Quien añade contenido a Wikidata (humanos, máquinas gestionadas por humanos, otras fuentes)
* Quién define las reglas y cómo. Los diferentes roles de la comunidad.
* Comportamiento y normas sociales.
* Normas editoriales. Lo que entra y lo que no. Referencias.
* Ventajas de tener una cuenta
Consejos:
     • Si su público está interesado en contribuir a Wikidata a escala o con fines profesionales, es importante explicar cómo decide Wikidata cuáles son el modelo de datos, las propiedades y los elementos. Por ejemplo, es importante explicar la dinámica social de "cualquiera puede crear un número Q, pero los números P se someten a un proceso de consulta comunitaria estrechamente controlado".

### Primeros pasos de edición (en el ordenador)
Haga que los estudiantes crean su propia cuenta (o planifique con antelación, hay un límite para la creación diaria de cuentas desde la misma IP). Añadir un mensaje en su página de discusión, con un enlace a la "página de la sesión de entrenamiento" configurada con antelación. Asegúrese de recoger todos los nombres de usuario creados durante la sesión. Sugerencias a los usuarios:
1. Cree una cuenta. Preséntese en la página de usuario.
2. Cambie las preferencias para establecer el idioma preferido.
3. Añada gadgets a las preferencias: "Recoin", "Reasonator" y "labelLister"
4. Suelte un mensaje en la página de entrenamiento
5. Utilice la barra de búsqueda para encontrar un elemento de Wikidata deseado. Compruebe su finalización con Recoin.

## Resumen y preguntas / introducción a la próxima sesión
Para terminar la sesión, el entrenador facilitará un momento de debriefing donde los participantes se animan a expresar sus preguntas, dudas, ideas y sentimientos sobre los temas tratados.
La próxima sesión tratará sobre cómo añadir contenido a Wikidata.
## Trabaja en casa (autónomo)
Antes de la segunda sesión, se pedirá al participante que piense qué entradas le gustaría mejorar, que busque información, incluidas las referencias y las fuentes, que se presentarán al grupo al principio de la próxima sesión.
## Referencias
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources
```