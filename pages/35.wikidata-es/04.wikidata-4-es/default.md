---
title: 'Aplicabilidad de Wikidata'
length: '120 minutos'
objectives:
    - 'Identificar el valor aplicado de Wikidata, especialmente para aplicaciones como la visualización de datos complejos'
    - 'Identificar las posibles aplicaciones de Wikidata en su propio trabajo, ya sea mediante la visualización, incrustando Wikidata a su propio software'
    - 'Saber cómo contactar y conectarse con miembros de la comunidad de Wikidata'
---

## Preguntas sobre la última sesión y discusión sobre el trabajo en casa
Opiniones sobre SPARQL y consultas.
Resultados de la campaña ISA y relación con la sesión del día (uso de la información de Wikidata para mejorar la descripción de la imagen con datos estructurados).

## El potencial del valor aplicado de Wikidata
Visión general y escaparates.
Esta sección se puede dividir en 3 partes:
* La primera podría proporcionar una visión general del contenido ya disponible en Wikidata: cifras y estadísticas por tema.
* La segunda podría proporcionar ejemplos de cómo se utilizan Wikidata por los proyectos de Wikimedia.

Algunos ejemplos sugeridos incluyen (a seleccionar en función del público):
1. una herramienta para explorar la brecha de género del contenido en la Wikipedia, como Denelezh.
2. el uso de Listeria bot para crear listas rojas de artículos que faltan. Véase Categoría: Llistes_basades_en_Wikidata.
3. el uso de Wikidata para añadir avisos de autoridad a los artículos de Wikipedia. Ejemplo de Doris Day.
4. el uso de Wikidata para mantener las cajas de información en la Wikipedia actualizadas. Ejemplo de Jack Spicer.
5. el uso de la función de analizador para mostrar directamente información de Wikidata los artículos (véase Com_utilitzar_dades_en_projectes_Wikimedia).
6. el uso de informáticos mágicos en Wikipedia (como el uso de {{infobox queso}} o {{infobox biographies2}} en la Wikipedia francesa).

* La tercera proporcionaría ejemplos de cómo Wikidata es utilizada por servicios de terceros.

Algunos ejemplos que sugerimos presentar son (se seleccionarán en función del público):
1. diversas herramientas de visualización tales como: El creador de gráficos de Wikidata o Histropedia (aplicación de cronología basada en Wikidata)
2. crotos, un motor de búsqueda y visualización de obras de arte visuales basado en Wikidata y que utiliza archivos Wikimedia Commons
3. Scholia: un motor de búsqueda y visualización para académicos basado en Wikidata

### Lecturas
* [Denelezh](https://www.denelezh.org)
* [Lists based on Wikidata](https://en.wikipedia.org/wiki/Category:Lists_based_on_Wikidata)
* [Doris Day](https://en.wikipedia.org/wiki/Doris_Day)
* [Jack Spicer](https://en.wikipedia.org/wiki/Jack_Spicer)
* [How to use fecha donde Wikimedia projects](https://www.wikidata.org/wiki/Wikidata:How_to_use_data_on_Wikimedia_projects)
* [Showcases](https://phabricator.wikimedia.org/T168057)
* [Wikidata knowledge as a service](https://www.wikidata.org/wiki/File:Wikidata_Knowledge_as_a_Service_slides_OeRC_Feb2018.pdf)
* [Histropedia](http://histropedia.com)
* [Angryloki](https://angryloki.github.io/wikidata-graph-builder/?property=P279&item=Q5)
* [Crotos](http://zone47.com/crotos/)
* [Sholes](https://tools.wmflabs.org/scholia/)

### Actividad
Proporcionar a los participantes la lista de herramientas seleccionadas y dejarlos explorar.

## Conectarse con la comunidad Wikidata
### Cómo conectarse
El objetivo de esta sección es saber cómo contactar y conectarse con miembros de la comunidad de Wikidata. Como mantenerse en contacto con las últimas novedades del proyecto y donde encontrar recursos.
Revisa las opciones para discutir con un Wikidata en una página de discusión.
Introduce WikiProject y explica cómo unirse o interactuar con sus miembros. Explica las ventajas de estos WikiProject, pero también su gobernanza (libertad para unirse, etc.) Idealmente elige mostrar un Wikiproyecto relevante para el público.
Sugiere unirse a la lista de correo de Wikidata, listas de correo técnicas, canal de IRC, telegrama, etc. Interactúa con el público para ver qué es lo más adecuado para ellos.
Un punto de entrada interesante para mostrar cómo interactúa la comunidad es mostrar la página Solicitud de comentario.
Sugiere un contacto con el capítulo local si hay alguno en el país y con el grupo de usuarios de la comunidad Wikidata.
Apunta a la conferencia anual de Wikidata, navega por el programa y vaya a las presentaciones proporcionadas.

### Lecturas
* [Wikidata WikiProject](https://dashboard.wikiedu.org/training/wikidata-professional/wikidata-wikiprojects)
* [Requests for comment](https://www.wikidata.org/wiki/Wikidata:Requests_for_comment)
* [WikiDataCon 2019](https://www.wikidata.org/wiki/Wikidata:WikidataCon_2019)
* [Wikidata Events](https://www.wikidata.org/wiki/Wikidata:Events)
* [Wikidata movimiento affiliates](https://meta.wikimedia.org/wiki/Wikimedia_movement_affiliates)
* [Wikidata Community User Group](https://meta.wikimedia.org/wiki/Wikidata_Community_User_Group)

## Trabajo en casa (autónomo)
Busca en el depósito abierto de [Zenodo](https://zenodo.org/) y encuentra sus tres artículos preferidos sobre FLOSS y educación. Documenta y / o publica un comentario sobre ellos.
## Resumen
Para terminar la sesión, facilita un momento de debriefing donde los participantes se animan a expresar sus preguntas, dudas, ideas y sentimientos sobre los temas tratados.
## Referencias
* [Planning](https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_wor)