---
title: 'E-Learning avec les outils FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: fr
length: '9 heures et 30 minutes'
objectives:
    - 'Reconnaître les avantages de l''apprentissage en ligne dans l''éducation'
    - 'Pour explorer le principal système de gestion de l''apprentissage Open Source : MOODLE'
    - 'Pour découvrir et bénéficier de la communauté Moodle'
materials:
    - 'Ordinateur personnel'
    - 'Connexion Internet'
    - Moodle
skills:
    'Mots clés':
        subs:
            'Apprentissage tout au long de la vie': null
            'Système de gestion de l''apprentissage (LMS)': null
            'Apprentissage asynchrone': null
            Moodle: null
            MOOC: null
---

## Introduction
E-Learning exploite des technologies interactives et des systèmes de communication pour améliorer l'expérience d'apprentissage. Il a le potentiel de transformer la façon dont nous enseignons et apprenons à tous les niveaux. Il peut élever les normes et élargir la participation à l'apprentissage tout au long de la vie. Il ne peut pas remplacer les enseignants et les enseignants, mais en plus des méthodes existantes, il peut améliorer la qualité et la portée de leur enseignement et réduire le temps consacré à l'administration. Il peut permettre à chaque apprenant de réaliser son potentiel et aider à créer un personnel éducatif habilité à changer. Il rend possible un système éducatif vraiment ambitieux pour une future société apprenante. Vers une stratégie unifiée d'apprentissage en ligne, l'unité de stratégie d'apprentissage en ligne du DfES, 2003

Le scénario de formation fournit une introduction aux méthodes et outils de formation en ligne. Alors que les outils d'apprentissage en ligne ont repensé l'environnement éducatif, les sessions sont conçues pour donner un aperçu du contexte de l'apprentissage en ligne, des fonctionnalités et des méthodes clés.

La formation passera par certains avantages de l'apprentissage en ligne. En particulier, il permettra aux apprenants modernes d'explorer le potentiel de l'apprentissage en ligne en tant qu'outil pour favoriser la collaboration en ligne. En fait, le eLearning peut être un outil efficace pour les apprenants et les éducateurs, en particulier pour promouvoir la collaboration en ligne (plates-formes d'apprentissage en ligne). Les participants découvriront la plateforme d'apprentissage Moodle pour travailler et apprendre ensemble dans des forums, des wikis, des glossaires, des activités de base de données et bien plus encore. La formation fournira aux apprenants une compréhension critique du processus d'apprentissage.
## Contexte
Le but de la session est de démontrer et d'engager les apprenants sur la façon dont:
* Reconnaître les avantages de l'apprentissage en ligne dans l'éducation
* Pour explorer le principal système de gestion de l'apprentissage Open Source: MOODLE
* Pour découvrir et bénéficier de la communauté Moodle

Les changements évolutifs de la technologie éducative ont changé le concept d'enseignement et d'apprentissage tel que nous le connaissons.
Il y a certainement de nombreux avantages à enseigner / apprendre dans un environnement en ligne. L'apprentissage en ligne peut rendre l'apprentissage plus simple, plus facile et plus efficace. En plus de servir différents styles d'apprentissage, l'apprentissage en ligne est également un moyen de fournir une livraison rapide des leçons. C'est également un outil pertinent en termes de collaboration et de renforcement communautaire. En fait, le e-learning ne doit pas être un voyage individuel. Grâce à des fonctionnalités telles que les forums et les didacticiels en direct, les utilisateurs peuvent avoir accès à d'autres membres de la communauté d'apprentissage. L'engagement avec d'autres utilisateurs favorise la collaboration et la culture d'équipe, ce qui présente des avantages au-delà de l'environnement de formation.
À la fin des sessions, les participants seront en mesure d'utiliser et de bénéficier de Moodle, l'une des principales plates-formes d'apprentissage open source qui offre un ensemble puissant d'outils centrés sur l'apprenant et des environnements d'apprentissage collaboratif qui permettent à la fois l'enseignement et l'apprentissage.
### Séances
### Première séance : Bases de l'apprentissage en ligne, contexte et évolution
L'apprentissage en ligne est l'utilisation de la technologie pour faciliter et améliorer l'apprentissage. Le but de l'apprentissage en ligne est de permettre aux gens d'apprendre sans fréquenter physiquement un cadre éducatif traditionnel. Pour mieux comprendre les avantages de l'apprentissage en ligne aujourd'hui pour les enseignants et les apprenants, il est utile de se pencher sur son passé. Le terme «E-Learning» n'existe que depuis 1999, lorsqu'il a été utilisé pour la première fois lors d'un séminaire sur les systèmes CBT. L'expert en technologie éducative Elliott Maisie a inventé le terme «E-Learning» en 1999, marquant la première fois que l'expression a été utilisée professionnellement. Cependant, les principes qui sous-tendent l'apprentissage en ligne ont été bien documentés tout au long de l'histoire, et il existe même des preuves qui suggèrent que les premières formes d'apprentissage en ligne existaient dès le XIXe siècle. Aujourd'hui, le e-learning est plus populaire que jamais.

La séance donne également un aperçu de certaines des technologies impliquées dans la formation en ligne, telles que la création de contenu, LMS et TMS. À l'ère de l'information, l'apprentissage tout au long de la vie est considéré comme la clé du succès continu de la société moderne. L'e-learning est considéré par beaucoup comme la meilleure solution viable au problème de la fourniture des ressources nécessaires pour faciliter l'apprentissage tout au long de la vie. Quels facteurs ont facilité l'apprentissage en ligne pour devenir le moyen le plus populaire de dispenser une formation aujourd'hui? Pourquoi utiliser l'e-learning? Cette session donne un aperçu de l'étendue du potentiel et des avantages de l'apprentissage en ligne.
### Deuxième séance : Prêt à l'emploi E-learning! Utiliser l'e-learning pour promouvoir une éducation ouverte
Cette séance fournira un large aperçu de ce que Moodle peut faire. Il explorera l'utilisation et les avantages de Moodle, l'un des systèmes de gestion de l'apprentissage open source (LMS) les plus populaires. La formation fournira les informations de base nécessaires aux apprenants pour démarrer avec la plate-forme d'apprentissage et bénéficier de l'assistance en ligne des utilisateurs du monde entier et du partage d'idées.

Les changements évolutifs de la technologie éducative ont changé le concept d'enseignement et d'apprentissage tel que nous le connaissons. Il y a certainement de nombreux avantages à enseigner / apprendre dans un environnement en ligne. L'apprentissage en ligne peut rendre l'apprentissage plus simple, plus facile et plus efficace. En plus de servir différents styles d'apprentissage, l'apprentissage en ligne est également un moyen de fournir une livraison rapide des leçons. C'est également un outil pertinent en termes de collaboration et de renforcement communautaire. En fait, le e-learning ne doit pas être un voyage individuel. Grâce à des fonctionnalités telles que les forums et les didacticiels en direct, les utilisateurs peuvent avoir accès à d'autres membres de la communauté d'apprentissage. L'engagement avec d'autres utilisateurs favorise la collaboration et la culture d'équipe, ce qui présente des avantages au-delà de l'environnement de formation.

À la fin des séances, les participants seront en mesure d'utiliser et de bénéficier de Moodle, l'une des principales plates-formes d'apprentissage open source qui offre un ensemble puissant d'outils centrés sur l'apprenant et des environnements d'apprentissage collaboratif qui renforcent l'enseignement et l'apprentissage.