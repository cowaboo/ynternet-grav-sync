---
title: 'Découvrir et comprendre SlideWiki'
objectives:
    - 'Donner un aperçu de ce qu''est SlideWiki et comprendre le cadre théorique autour des applications Web pour gérer le contenu éducatif'
    - 'Expliquer en quoi l''outil de méthodologie est un atout dans le projet et pourquoi l''utilisation de l''application Web SlideWiki dans Open Adult Education est recommandée'
    - 'Découvrir la communauté SlideWiki et quelques éléments clés de la communauté'
    - 'Comprendre l''interface: créer un compte et favoriser la (co)création et le partage de contenus pédagogiques'
---

## Introduction
Le formateur commencera l'introduction du module en interrogeant les participants sur leur expérience actuelle et précédente avec SlideWiki ou tout autre projet de la famille wiki. Ils seront également invités à partager des informations sur le contexte de leur participation à la séance. Le but de cette première conversation est de les aider à voir comment SlideWiki peut avoir un impact pratique sur leur vie quotidienne et attirer leur attention. Avec SlideWiki, l'objectif est de rendre le contenu éducatif considérablement plus accessible, interactif, engageant et qualitatif dans une culture ouverte et gratuite.

Cette section garantit généralement que les participants seront en mesure de:
* Comprendre les objectifs de SlideWiki.
* Comprendre comment SlideWiki est lié à d'autres projets Wikimedia et à d'autres outils numériques.
* Comprendre pourquoi ils devraient être motivés à contribuer ou à utiliser des présentations SlideWiki, en particulier dans le contexte de leur propre travail.
* Comprenez que SlideWiki est avant tout une construction sociale:
    * Qui ajoute du contenu à SlideWiki (humains, machines gérées par des humains, autres sources).
    * Qui définit les règles et comment. Les différents rôles de la communauté.
* Comportement et règles sociales sur SlideWiki.
* Règles éditoriales. Ce qui entre et ce qui ne rentre pas. Les références bibliographiques.
* Avantages d'avoir un compte.

### Concepts clés
Décrire les caractéristiques de base de SlideWiki (cc0 gratuit, collaboratif, multilingue et indépendant de la langue, interconnecté) et ses objectifs (référentiel de connaissances humaines, support pour les projets Wikimedia et tiers, hub dans le web de données ouvertes lié) + statistiques générales sur le contenu + statistiques générales sur l'accès.
### Utilité de SlideWiki
Comment SlideWiki sert les projets Wikimedia et la communauté en fournissant une plate-forme pour rendre le contenu éducatif considérablement plus accessible, interactif, engageant et qualitatif.
### Autres utilités
Comment SlideWiki sert les objectifs du projet européen Open AE et comment il est important de créer une communauté de pratique pour ce projet autour de cet outil ouvert collaboratif.
## Premières étapes d'édition
Demandez aux étudiants de créer leur propre compte. Assurez-vous de collecter tous les noms d'utilisateur créés pendant la session. Suggestions aux utilisateurs:
1) Créez un compte. Présentez-vous dans la page utilisateur.
2) Explorez la plateforme et commencez à découvrir le contenu et les mécanismes de l'outil de collaboration en ligne.
3) Rechercher du contenu déjà disponible sur SlideWiki on sur tout sujet présentant un intérêt pour le participant. Découvrez le type de présentations sont déjà disponibles.
4) Discuter de la valeur de la collaboration en ligne pour rendre le contenu disponible.

## Devoirs
Avant la deuxième session, le participant sera invité :
* à réfléchir au champ de saisie qu'il souhaite améliorer,
* à rechercher des informations, y compris des références et des sources, qui seront prochainement présentées au groupe au début de la prochaine session.

## Références
* [SlideWiki: revolutionise how educational materials can be authored, shared and reused](http://aksw.org/Projects/SlideWiki.html).
* [Wikipedia](https://en.wikipedia.org/wiki/SlideWiki).
* [SlideWiki](https://slidewiki.org/).
* [SlideWiki - Towards a Collaborative and Accessible Platform for Slide Presentations](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&ved=2ahUKEwiU9ZnHsOTlAhUK86YKHfzFDdwQFjAIegQIBBAC&url=http%3A%2F%2Fceur-ws.org%2FVol-2193%2Fpaper6.pdf&usg=AOvVaw0WHW7Nrzf7KuBJhJ6G4Klx).