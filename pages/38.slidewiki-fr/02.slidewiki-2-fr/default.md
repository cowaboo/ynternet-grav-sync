---
title: 'Expérimenter SlideWiki: création et collaboration sur SlideWiki'
objectives:
    - 'Comprendre comment le contenu SlideWiki est créé, partagé et importé'
    - 'Apprendre comment ajouter du contenu à SlideWiki et comment contribuer à la communauté pour construire la base de données'
    - 'Promouvoir l''utilisation de systèmes de création OpenCourseWare ouverts sur le Web'
    - 'Apprendre de manière autonome à partir de tutoriels et de vidéos'
---

Le formateur animera un moment de débriefing où les participants sont encouragés à exprimer leurs questions, doutes, idées et sentiments à l'égard des sujets abordés dans la leçon précédente.
En outre, le formateur présentera le contenu de cette session, qui consistera à apprendre à ajouter du contenu à SlideWiki en utilisant la présentation de la communauté Slidewiki.
## Importation des présentations et aperçu Editing
Les participants seront invités à apprendre comment importer leurs propres présentations (dans différents formats: PowerPoint, diapositives, etc.) vers de nouveaux decks dans SlideWiki. Ils apprendront comment créer et éditer de nouveaux decks et aussi comment les gérer les decks principaux (privés, publics, partagés en groupe, etc.). Le but de cette activité est de présenter les principales caractéristiques de l'outil collaboratif et de guider les participants dans un premier temps.
## Activité d'édition
Les participants seront encouragés à développer davantage leurs présentations précédemment téléchargées sur slidewiki. Le but est de modifier et d'ajouter du contenu à la présentation, en explorant les différents outils que la plate-forme permet, comme importer différentes présentations dans le même jeu, ou importer la présentation de la communauté SlideWiki.
Les participants sont encouragés à explorer la plateforme librement et à poser des questions.
## Processus de collaboration SlideWiki
### SlideWiki un outil de collaboration et de crowdsourcing (30min)
Le formateur guidera les participants dans la découverte du côté collaboratif de l'outil SlideWiki. Les participants exploreront les présentations disponibles dans le référentiel mondial et apprendront comment importer la présentation d'autres auteurs. La possibilité de travailler sur le travail de quelqu'un d'autre apporte de nombreux avantages comme gagner du temps, de l'argent et éviter de recommencer à zéro.
Les participants apprendront comment non seulement prendre de la communauté, mais aussi comment partager leur propre travail avec la même communauté.
### Présentation des fonctionnalités d'édition et de collaboration (20 min)
Le but de cette section est de mieux comprendre les processus d'édition de SlideWiki, en particulier nous nous concentrerons sur certaines fonctions de la plateforme comme les licences, la transparence et la possibilité de suivre chaque édition et les traductions automatiques. Les participants apprendront également comment créer des groupes d'éditeurs et une liste de lecture de decks à l'intérieur de ces groupes. Il s'agit d'une étape cruciale lors de la collaboration en ligne à l'aide de cette plateforme.
## Apprenez de manière autonome à partir de didacticiels et de vidéos (participants)
Les participants seront invités à rechercher et importer les decks didacticiels de SlideWiki sur leurs decks personnels. Après cette étape, les participants naviguent dans les didacticiels afin de découvrir davantage le potentiel de l'outil collaboratif. Les vidéos YouTube et les articles Wiki sont également encouragés à être utilisés, car les participants découvrent de nouvelles façons d'apprendre.
## Devoirs
Les participants organiseront tous les jeux qu'ils ont importés de la communauté et les jeux qu'ils ont créés par thème / sujet d'intérêt. L'idée est de regrouper les présentations du même sujet dans le même deck de manière structurée.
## Références
* [Webinar Slidewiki Platform](https://www.youtube.com/watch?v=WIdbPToAwNs).