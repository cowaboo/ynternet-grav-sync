---
title: 'Créer des cours en ligne en utilisant SlideWiki et un contenu d''apprentissage organisé'
objectives:
    - 'Créer un espace collaboratif pour l''éducation des adultes'
    - 'Motiver les participants à apprendre à créer des cours de base en ligne ou même des cours avancés sur SlideWiki'
    - 'Apprendre à planifier, structurer, créer et dispenser des cours en ligne en utilisant SlideWiki comme outil d''éducation ouverte pour adultes'
---

Créer et dispenser des cours en ligne en utilisant SlideWiki comme outil d'éducation ouverte pour adultes (formateur).
## Comprendre le SlideWiki comme un espace collaboratif pour l'éducation des adultes
Les participants discuteront de la façon dont SlideWiki peut aider à créer des espaces pour que les gens apprennent et partagent leurs connaissances. Comment cet outil collaboratif peut-il promouvoir l'éducation des adultes en ligne en étant un lieu de partage de contenu mais aussi d'importation. Le formateur favorisera la compréhension de la façon dont SlideWiki peut être un espace pour les gens à apprendre et même à suivre un programme complet en ligne.
## Les participants créent leur propre cours en ligne
Le formateur invitera les participants à développer leur propre cours en ligne (sur n'importe quel sujet de leur choix) en utilisant SlideWiki comme outil principal pour promouvoir l'éducation des adultes en ligne. Le formateur guidera les participants sur les principales étapes à considérer lors de la préparation d'un cours de formation en ligne. De plus, le formateur aidera les participants à commencer à créer leurs sessions de planification et leur matériel. L'idée est d'aider les participants à créer leurs propres idées de cours et de les aider à développer ces cours.
## Auto-apprentissage
Les participants naviguent dans les tutoriels afin de découvrir davantage le potentiel de l'outil collaboratif lors de la création d'ateliers et de cours en ligne. Les vidéos YouTube et les articles Wiki sont également encouragés à être utilisés, car les participants découvrent de nouvelles façons de créer des ressources pédagogiques. C'est un moment pour les participants d'explorer la plateforme avec un certain niveau d'autonomie et d'autonomisation, car ce sont eux qui guident leurs besoins et leur cours en ligne.
## Devoirs
Les participants feront leur cours en ligne comme une plate-forme publique et les enverront au groupe mettant le contenu à disposition de la classe mais aussi de la plus grande communauté de SlideWiki.
## Références
* https://slidewiki.eu/about/
* https://slidewiki.org/
* https://cordis.europa.eu/project/id/688095
* https://www.researchgate.net/publication/328063940_SlideWiki_towards_a_collaborative_and_accessible_platform_for_slide_presentations
