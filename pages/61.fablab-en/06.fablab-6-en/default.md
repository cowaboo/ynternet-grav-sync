---
title: 'Peer review'
objectives:
    - 'Provide an introduction to Peer review of the design of the community Fablab'
    - 'Provide an introdution to First activities to be developed'
    - 'Facilitate Draft project guide to start implementing a Makerspace/FabLab'
    - 'Discover necessary resources and sources of funding and knowledge'
    - 'Motivate to open and promote a makerspace and maker’s activities in their own organizations'
lenght: '90 min'
---

## Introduction
The trainer will talk about the questions that may have arose after the previous session and will comment on homework. 
After that, trainer will introduce the content and the main activities of the session.
## First part
The goal of this section is to get sure that every group has developed a “realistic” project to start with the communitarie FabLab or maker Space. Trainer will present the following concepts:
* It’s needed to define material and human needs from a realistic point of view. What can we achieve? Project should be very well dimensioned.
* List of machines and budget.
* It’s very useful to make a quadrant of schedules and base activities.

## Second part
Trainer will explain that we have “some news”. Groups are going to make the peer review activity. Every training group will be splitted in two. 
1. On part of the group will “defend” the project in front of the evaluation committee. 
2. The other part of the group will conform the evaluation committee of the other groups project. 

In that way, all the projects will be reviewed and improved by a peer-to-peer strategy. 
## Third part
Working in groups:
* Each group must prepare the presentation of their own makerspace project.
* Each group will have 2 minutes to pitch their idea (if this is an e-learning activity, the group can create a video with their pitch elevator), and then will receive feedback from the rest of the groups on aspects to improve or about the potential of the idea.

##Homework
Think about the whole process and start to make it for real. Step one, step two... and you will have a new FabLab! Then... think about the opportunity to be part of a global network as [FabFoundation](https://fabfoundation.org/getting-started/#fab-lab-questions) 

##References
[FabFoundation](https://fabfoundation.org/getting-started/#fab-lab-questions) 
[FAQ of FabFoundation](https://fabfoundation.org/getting-started/#fab-lab-questions)