---
title: 'FLOSS licences for open education, art and collective learning'
length: '240 min'
objectives:
    - 'Provide a critical analysis of the importance of FLOSS licences for open education, art and collective learning.'
    - 'Examine the use and advantages of FLOSS licences in education and art'
    - 'Foster learners''active and creative engagement through FLOSS licences'
    - 'Motivate participants to adopt FLOSS licences in their current and future education activities'
---

## Introduction
Participants will brainstorm on licences and education initiatives around them. Answers will be documented and then reused to customize the training material.
## FLOSS licensing in art and education
In 1999, the novel Q appeared under the name of Luther Blissett, known previously as the collective moniker of an Italian media prankster project. This allegorical account of Italian subculture in the form of a historical thriller set in 16th century Italy, Q became a national no.1 best-seller and subsequently appeared in French, German and English translations. Obviously, the sales didn’t suffer at all from the fact that the imprint of the book permitted anyone to freely copy it for non-commercial purposes. The book was not released by an underground publisher, but by the well-established publishing houses Einaudi in Italy, Editions du Seul in France and Piper in Germany, amongst others who apparently didn’t mind giving up traditional copyright-granted distribution models for a promising publication.

Another example, in a different, focus is the European (as open education and art repository).The Europeana Licensing Framework for example standardises and harmonise rights related information and practices under a custom data exchange agreement. The DEA structures the relationship between Europeana and its data providers. The DEA establishes that Europeana publishes metadata it receives from its data providers under the terms of the Creative Commons Zero Universal Public Domain Dedication (CC0). Read more about CC0 and data use guidelines.

Knowledge and creativity are resources which, to be true to themselves, must remain free, i.e. remain a fundamental search which is not directly related to a concrete application. Creating means discovering the unknown, means inventing a reality without any heed to realism. Thus, the object(ive) of art is not equivalent to the finished and defined art object. This is the basic aim of this Free Art License: to promote and protect artistic practice freed from the rules of the market economy.
[Free Art License Preamble version 1.2](http://www.copyleftlicense.com/licenses/free-art-license-(fal)-version-12/view.php).
## Examples
1. Watch the talk of Gwenn Seemel: [In defense of imitation](https://www.tedxgeneva.net/talks/gwenn-seemel-in-defense-of-imitation/).
Study, document and discuss her arguments. Explore on how the artist is making her living. Create a common document with all participants, share it with others. 
(alternative).
2. Collective scenarios for OEP - Open Education scenarios (group work).
The activity will focus on an exemplary selection of applications of the paradigm of open educational practices.

## Homework
Search in the [Zenodo open repository](https://zenodo.org/) and find your three favourite papers on FLOSS and education. Document and/or post a comment on them.
## References
* [Guide To Open Content Licenses](https://monoskop.org/images/1/1e/Liang_Lawrence_Guide_to_Open_Content_Licenses_v1_2_2015.pdf) by Lawrence Liang.
* [Steal this Film and projects](http://www.stealthisfilm.com/Part2/projects.php).
* Biella Coleman, [The Social Creation of Productive Freedom: Free Software Hacking and the Redefinition of Labor, Authorship, and Creativity](http://www.healthhacker.com/biella/proposal2.html/).
* [unglue.it](https://unglue.it/): To pay an author or publisher for publishing a Creative Commons ebook.
* [DIS](https://dis.art/): The future of learning is much more important than the future of education. DIS is a streaming platform for entertainment and education — edutainment. We enlist leading artists and thinkers to expand the reach of key conversations bubbling up through contemporary art, culture, activism, philosophy, and technology. Every video proposes something — a solution, a question — a way to think about our shifting reality.