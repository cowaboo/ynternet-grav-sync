---
title: 'Éditer WikiData'
length: '180 min'
objectives:
    - 'comprendre comment le contenu de Wikidata est collecté et ajouté à la base de données'
    - 'apprendre comment ajouter du contenu à Wikidata, et comment faire des ajouts de masse à Wikidata'
    - 'découvrir comment la communauté wikidata collabore à la construction de la base de données'
    - 'comprendre comment les processus de qualité sont intégrés au projet'
    - 'se familiariser avec les merveilles d''obtenir des réponses à des questions compliquées avec les systèmes de requêtes'
---

##1.- Questions sur le dernier module et discussion sur les devoirs (10 min)

##2.- Introduction à l'édition (formateur) (45 min)
→ Aperçu de l'édition et de la révision de la structure des données (20 min)
La section "Introduction à l'édition" permet de s'assurer que les participants seront en mesure de faire :
* Trouver le bouton d'édition et contribuer à l'étiquetage et à la description d'un élément Wikidata
* Trouvez le bouton d'édition d'une propriété existante, modifiez la propriété avec un qualificatif et ajoutez une référence à un élément Wikidata.
* Révision de la partie fondamentale d'un élément WikiData (étiquette, identificateurs, énoncés, qualificatifs, références, propriétés et valeurs)
* Démonstration de la façon d'éditer
* Découverte de l'outil Recoin, destiné à identifier les informations manquantes sur un objet
→ Activité d'édition (sur ordinateur portable, 25 min)
* Suivez les tutoriels WikiData : créer un élément et ajouter une instruction
* Puis créer ou améliorer un article
* Analysez quelques articles avec Recoin
* Remplir d'autres éléments (les élèves doivent également ajouter des références et des sources).
* Retour d'information : rapport collectif de chaque élève sur ce qui a été fait, les difficultés rencontrées, les questions, les réflexions.
Astuces :
Vous devez faire la démonstration de l'édition d'un élément que vous avez identifié avant l'événement, y compris l'ajout d'une étiquette, d'une description et d'un bien commun pour ce type d'élément. N'oubliez pas d'indiquer où se trouvent les boutons de modification pour chaque type de modification.
Vous devriez montrer comment ajouter une référence à un énoncé existant. Une grande partie de la valeur de Wikidata à long terme, vient de la capacité de faire référence à des sources fiables de matériel. Comme pour la démonstration du référencement lors des ateliers de rédaction de Wikipédia, il est important de préparer la référence à l'avance. N'oubliez pas de décrire les champs communs pour les références sur Wikidata (si vous avez des gadgets personnalisés pour copier des références ou remplir des références en utilisant Citoid, assurez-vous de décrire comment ils changent l'interface).
Si vous avez des gadgets ou des outils personnalisés installés sur votre compte Wikidata, pensez à les désactiver ou à discuter de la différence entre votre interface et l'interface standard de Wikidata.
Lire : 
*https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_Workshop/Adding_basic_statements_to_Wikidata_items_by_engaging_newbies
*https://www.wikidata.org/wiki/Wikidata:Tours

##3.- Processus d'édition Wikidata (formateur) (30 min)
Le but de cette section est de mieux comprendre les processus d'édition de Wikidata, en insistant en particulier sur la transparence et la possibilité de suivre chaque édition.
→ Montage et présentation des fonctionnalités de collaboration (20 min)
Visite guidée des différentes fonctionnalités de WikiData, utile pour l'éditeur : Userpage, TalkPage, WatchList, Contributions personnelles
Visite guidée d'éléments utiles à la communauté pour suivre l'activité sur le site : Modifications Récentes, historique des éléments de la page, pages des autres utilisateurs, Interaction avec les utilisateurs sur leurs pages de discussion, liste des contributions des autres participants
Présentation de la stratégie de WikiData basée sur la SécuritéDouce et la SécuritéDure
Démonstration des contributions fournies par les participants à l'activité précédente
→ Activité  (sur ordinateur portable, 10 min)
* Chaque étudiant prend le temps d'explorer ce qui vient d'être décrit. Le formateur peut demander à chaque participant d'écrire un message sur la page de discussion d'un autre participant, etc.

##4.- Qualité des données et évaluation des données (formateur) (35 min)
→ Présentation de la qualité et de l'évaluation (30 min)
Fournir un aperçu du contenu déjà disponible sur WikiData
Comment évaluer la qualité d'un article
Utilisation d'éléments ShowCase pour construire un élément
Explorer davantage les sources et les citations (comment traiter les faits contradictoires, ce qui est une bonne source de données, ce qui ne l'est pas).
Découverte d'outils pour aider à combler les lacunes, en profondeur et en profondeur (par exemple TABernacle, Mix'n'Match, le jeu WikiData, le jeu distribué, Wikishootme, Terminator). A choisir en fonction du public)
Introduction à un outil d'édition en masse : quickstatements
La présentation comprendra des discussions, des questions et des réponses.
Astuce :
La présentation ou démonstration peut typiquement inclure :
* Mix'n'Match : les entrées de la liste d'outils de certaines bases de données externes et permettent aux utilisateurs de les comparer aux éléments Wikidata.
* WikiShootMe : il s'agit d'un outil pour afficher les éléments Wikidata, les articles Wikipedia et les images Commons avec coordonnées, tous sur la même carte.
Il facilite l'ajout d'images aux projets Wikimédia.
* Wikidata games : Un ensemble de jeux pour ajouter rapidement des instructions à Wikidata.
Lire :
*https://tools.wmflabs.org/wikidata-game/
*https://tools.wmflabs.org/wikidata-game/distributed/
*https://tools.wmflabs.org/mix-n-match/#/
*https://tools.wmflabs.org/wikishootme/#lat=43.35953739293013&lng=5.325894166828497&zoom=15
*https://tools.wmflabs.org/quickstatements/#/

##5.- Introduction de base aux requêtes WikiData en utilisant Query Helper (formateur) (45 min)
Les participants seront en mesure de :
* Comprendre comment lire et modifier une requête Wikidata de base en utilisant à la fois SPARQL et le Query Helper.
* Comprendre comment construire des requêtes super basiques qui retournent plusieurs variables et étiquettes.
* Comprendre quels types de visualisations sont disponibles pour l'utilisation de Wikidata Queries
* Que sont les requêtes et qu'est-ce que Sparql
→ Découverte du service Wikidata Query Service 
Démonstration avec une simple requête à partir d'exemples et modification de la requête initiale. Afficher différentes visualisations. 
→ Activité (sur ordinateur portable si le temps le permet)
* Les participants jouent avec le Query Service : exploration des exemples avec différentes options de visualisation, modification des exemples pour les plus audacieux.

##6.- Restitution et questions (formateur) (15 min)
Pour conclure le module, le formateur animera une estitution où les participants sont encouragés à exprimer leurs questions, leurs doutes, leurs idées et leurs sentiments sur les sujets abordés.
Introduction au module suivant env. 5 mn
Le module suivant sera dédié aux requêtes

##7.- Devoirs pour préparer le module suivant (1h)
* recherchez 3 idées de questions de recherche à soumettre lors des prochains modules
* lire : https://www.wikidata.org/wiki/Wikidata:Glossary

##Références pour le formateur :
* https://www.wikidata.org/wiki/Wikidata:Planning_a_Wikidata_workshop
* https://dashboard.wikiedu.org/training/wikidata-professional/evaluating-wikidata
* https://www.wikidata.org/wiki/Help:Contents
* https://www.wikidata.org/wiki/Wikidata:Tools
* https://www.wikidata.org/wiki/Wikidata:Wikidata_educational_resources