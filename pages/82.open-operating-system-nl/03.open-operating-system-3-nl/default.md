---
title: 'En wat nu?'
objectives:
    - 'Lees meer over de belangrijkste mechanismen voor de installatie van software'
    - 'Richtlijnen opstellen voor de installatie van updates als basis voor goed onderhoud'
    - 'Het samenstellen van registers met referentiedocumentatie'
    - 'Ken het beleid van administratieve machtigingen en gebruikers van uw OS'
---

## Hoe installeer ik nieuwe programma's?
(tentoonstelling, presentatie of web)
## Upgradebeleid
(tentoonstelling, presentatie of web)
## Referentiedocumentatie
We vragen u om een dynamiek van informatieverzameling en -deling (via wiki en pad) op te zetten.
## Gebruikers en beheerders 

## Huiswerk

## Referenties