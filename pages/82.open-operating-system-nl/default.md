---
title: 'Open besturingssysteem als overgang naar FLOSS: GNU / Linux'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: nl
length: '9 uur en 30 minuten'
objectives:
    - 'Om een veilige omgeving te bieden aan deelnemers om te migreren naar GNU / Linux in hun werkruimtes'
    - 'Het creëren van een ondersteunend netwerk onder de deelnemers'
    - 'De oprichting van een "peer-to-peer"-netwerk te bevorderen, om steun te verlenen aan de verschillende actoren in het gebied die betrokken zijn bij de verspreiding van PLL'
materials:
    - 'Computer met internetverbinding'
    - Browser
    - 'PC waar je installaties op kan testen (met data die je kan verliezen)'
    - '1 of 2 USB poorten en sleutels voor OS installatie'
skills:
    Sleutelwoorden:
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            Besturingssysteem: null
            Migration: null
            'Systematische administratie': null
---

## Introductie
De technologische infrastructuur van een telecentrum of een digitaal opleidingscentrum is het uitgangspunt voor de ontwikkeling van de pedagogische activiteiten. Het ontwerp van de infrastructuur zal het interventiebeleid bepalen dat we zullen kunnen ontwikkelen en de onderwerpen waaraan we zullen kunnen werken, evenals de perspectieven van ons werk.

Of we nu werken aan basisprojecten op het gebied van digitale geletterdheid of in een meer complexe digitale specialiteit (digitale video-editie, makerswereld, geavanceerde codeervaardigheden...), uiteindelijk hebben we specifieke digitale producten en technologieën aangeleerd. De overgang naar het werken met vrije technologische hulpmiddelen (FLOSS) kan (moet?) gepaard gaan met het werken, ook met vrije besturingssystemen, die het vermogen bieden om de technologie te begrijpen als een open en gedeelde kennis die verder gaat dan de gesloten mogelijkheden die de markt biedt.
## Context
Doelstellingen van de sessie:
* Zorg voor een veilige omgeving voor deelnemers om te migreren naar GNU / Linux in hun werkruimtes.
* Creëer een ondersteunend netwerk onder de deelnemers.
* Promoot de oprichting van een peer-to-peer netwerk om de verschillende actoren in het gebied die betrokken zijn bij de verspreiding van het PLL te ondersteunen.

Tijdens deze module worden twee verschillende methodieken gebruikt:
* **Overleg**: Discussiegroepen en discussiefora.
* **Uitvoering**: echte praktijken in computers geleverd door studenten.

## Sessies
### Eerste sessie: Onze behoeften
In deze sessie werken we vanuit een analyse van de behoeften van elk telecenter/digitaal trainingscentrum, en situeren we het in zijn context. U brengt de verschillende partners in kaart met wie u zich in verbinding kunt stellen en uw rollen: overheidsadministratie, gemeenschappen van vrije software ontwikkeling, universiteiten, andere opleidingscentra. Ook zullen de FLOSS-tools worden gespecificeerd, die kunnen beantwoorden aan de educatieve behoeften van elke ruimte.
### Tweede sessie: De stappen die u moet geven
Op basis van de gedetecteerde behoeften zullen we een migratieplan ontwerpen, waarbij we de beste SO kiezen die aan de behoeften kan voldoen. Verschillende migratiemechanismen zullen worden geanalyseerd en beoordeeld, afhankelijk van de behoeften van elk centrum. Er zal een testinstallatie worden uitgevoerd en er zullen onderhouds- en update-richtlijnen worden ontwikkeld om ervoor te zorgen dat het centrum in goede staat blijft.
### Derde sessie: En wat nu?
Zodra de installatie is voltooid en de onderhouds- en update-regels zijn vastgesteld, zullen we leren hoe we nieuwe programma's veilig kunnen installeren.
### Vierde sessie: Ontwaken en verspreiden
Het is noodzakelijk om te werken op een pedagogisch niveau, zoals het uitleggen van hoe dit werkt aan de mensen die deelnemen aan elk centrum en de belangrijkste redenen om dit soort technologie te gebruiken. We zullen strategieën bestuderen en ontwikkelen voor de verspreiding van FLOSS-systemen en het didactisch materiaal van elk centrum aanpassen om zijn hoofdtaak voort te zetten: digitale geletterdheid of opleiding in digitale competenties.