---
title: 'De te nemen stappen'
objectives:
    - 'Zorg voor de nodige hulpmiddelen om met succes te migreren naar GNU / Linux'
    - 'Het vergemakkelijken van technologische hulpmiddelen die ons kunnen helpen om informatie te bewaren en terug te vinden'
    - 'Vergemakkelijken van de pedagogische hulpmiddelen voor het plannen van de migratie'
    - 'Stimuleer migratie, verlies angst. Het is maar een machine!'
---

## Migratiestrategieën
Analyseer de voors en tegens van de verschillende migratiestrategieën in GNU / Linux.
1. 1. Stapsgewijze migratie. Ga eerst elke tool migreren, eindig met het migreren van het OS wanneer mensen al gewend zijn om met de tools te werken.
2. 2. Gemengde migratie: gedeeltelijke migratie van het centrum; je doet het alleen bij sommige machines om mensen te laten wennen.
3. 3. Dubbele migratie: installeer de twee systemen parallel, zodat elke gebruiker op elk moment kan kiezen welk systeem hij wil gebruiken.
4. 4. Harde migratie: het hele systeem wordt voor eens en altijd gemigreerd. De mensen die door de verandering worden getroffen, worden geïnformeerd (of niet).
Doel: om te zien hoe, in feite, de stapsgewijze migratie al gebeurt, zijn veel van de tools die we op dit moment gebruiken FLOSS (Mozilla Firefox, Gimp, LibreOffice ...). Het aanmoedigen van harde migraties.

Online activiteit, discussieforum over elke strategie. Aan het einde van de activiteit moeten de deelnemers een migratiesysteem kunnen kiezen.
## Back-up amb clonezilla
Deze activiteit is optioneel.
Clonezilla is een krachtige en intuïtieve tool, die ons helpt bij het maken van back-ups van complete installaties. Het zal nuttig zijn voor:
* Maak kopieën van een computer (besturingssysteem en inhoud) en bewaar deze op een harde schijf.
* Maak snel kopieën en repliceer een installatie, in plaats van herhaaldelijk dezelfde installatie en configuratie te maken.

De activiteit bestaat uit het installeren van een bootable USB met clonezilla, volgens de instructies van de software zelf. Mensen die deze activiteit uitvoeren zullen uiteindelijk een bootable usb met clonezilla hebben, en ze moeten in staat zijn om een PC op te starten met deze USB. Ze kunnen proberen een back-up te maken van de bestaande installatie.
Er wordt een ondersteuningsforum geopend om de technische twijfels die kunnen ontstaan op te lossen.
## Mijn SO
Download en installeer het eerder gekozen besturingssysteem op een USB-bootable. Dit zal gebeuren volgens de instructies van elk systeem.

Elke deelnemer moet zijn opstartbaar besturingssysteem van een extern medium hebben.
## Laten we het doen
Hoe u kunt opstarten met behulp van het BIOS (ondersteuning: presentatie of web) wordt uitgelegd in dit deel van de les.
* Wat is het BIOS, hoe krijg je toegang tot het BIOS en hoe kies je de opstartvolgorde.
* UEFI En zijn compatibiliteit met ubuntu.

Zodra de USB is voorbereid, wordt de installatie van de SO uitgevoerd.
* Het is aan te bevelen om een enkele (niet-dubbele) installatie uit te voeren, omdat dit het proces vereenvoudigt.
* Als u ervoor kiest om een dubbele installatie uit te voeren, is het raadzaam om de door de installateur voorgestelde partitionering te volgen. In geval van twijfel, vraag het aan de trainer!

Als er geen probleem is, wordt aan het einde van het proces de computer opnieuw opgestart en werkt de nieuwe SO.
## Huiswerk

## Referenties