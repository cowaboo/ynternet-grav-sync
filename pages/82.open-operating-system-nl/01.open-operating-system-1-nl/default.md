---
title: 'Onze behoeften'
objectives:
    - 'Ken verschillende ervaringen van telecentra, digitale trainingscentra en soortgelijke ruimtes die hebben besloten om 100% met GNU / Linux te werken'
    - 'Zorg voor richtlijnen en criteria bij de keuze van de software'
    - 'Ontdek welke FLOSS-oplossingen het beste aansluiten bij onze behoeften'
    - 'Laten zien dat het migreren en beheren van een telecentrum met FLOSS binnen ons bereik ligt'
---

## Introductie
Beschrijving van de doelstellingen van de sessie.
Om succesvol te kunnen migreren naar GNU / Linux is het belangrijk om het niet alleen te doen. Het is belangrijk om te weten welke netwerken we in onze omgeving hebben, en om te weten op wie we kunnen rekenen om deze stappen te ondernemen.
* Lokaliseer agenten uit het gebied die deel kunnen uitmaken van dit ondersteuningsnetwerk.
* Breng onze behoeften in kaart om de beste FLOSS-oplossingen voor ons centrum te vinden.

Laten we een succesvolle migratie naar GNU / Linux als voorbeeld nemen.
(suport: Video / Slide - xarxantoni? / PO La Mina?) 
## Contactopname
We voeren een presentatiewiel uit waarbij elk van de deelnemers moet aangeven uit welk centrum ze komen, hun actieradius contextualiseren naar het sociale niveau en de mate van kennis over GNU/Linux aangeven die ze hebben.
Het doel van deze eerste dynamiek is dat de trainer de verschillende niveaus van de deelnemers kan kennen.
Face-to-face formaat: presentatiewiel. Online formaat: presentatieforum.
Deelname wordt geëvalueerd
## In kaart brengen van de actoren 
Het doel van deze dynamiek is om de actoren te identificeren waarmee elk centrum kan tellen om te migreren naar GNU / Linux. We zorgen voor een raster voor deze kaart, en de verschillende actoren zullen worden geplaatst.

* Overheidsinstellingen: wat is de relatie van uw telecentrum met de overheid? Is dit direct afhankelijk? Als het beheer privé is, werkt het dan samen met de administratie? Zijn er PLL-promotieprogramma's in uw administratieve sfeer?
* Particuliere initiatieven: is er in uw regio een bedrijf dat technologische ondersteuning biedt aan de kleine bedrijven? Uw centrum, maakt u gebruik van dit soort bedrijven om het goed te laten functioneren? Is er een specialisatie in open licenties? Welke samenwerkingsverbanden heeft u op dit moment, en het belangrijkste, wat zou u kunnen hebben?
* Burgerschap: Is er een lokale agent -niet winstgevend, klein bedrijf- gericht op het promoten van het gratis programma in uw omgeving? Zeker, lokaliseer het, bestudeer wat voor allianties u kunt bereiken!
Face-to-face formaat: de deelnemers worden gegroepeerd volgens het lokale nabijheidstype of het administratieve type (de activiteit wordt in kleine groepen uitgevoerd).
Online formaat: Afhankelijk van de virtuele omgeving die gebruikt kan worden, kunt u de samenwerking tussen de verschillende deelnemers verbeteren. Het maken van een wiki om gezamenlijk in kaart te brengen is optioneel, maar u kunt ook sjablonen aanleveren die individueel worden aangeleverd.
Ondersteunend materiaal: matrix van actoren (sjabloon)
## Kaartgereedschap
Met deze activiteit wilt u aan de slag met de selectie van FLOSS tools die voor u nuttig zullen zijn. Het doel van de sessie is het bepalen van de mate van specialisatie die elk centrum heeft en het selecteren van het juiste OS voor elk geval.
Het werkt vanuit een raster met twee kolommen:
* Welk gereedschap gebruikt u vandaag de dag voornamelijk?
* Welke FLOSS oplossingen kunt u vinden?

In deze dynamiek moet de trainer de zoektocht naar informatie en de selectie van hulpmiddelen voor elke behoefte begeleiden.
Aan het einde van de sessie worden de meest voorkomende OS-oplossingen met voor- en nadelen (ondersteuning: presentatie of web) gepresenteerd.
Generalisten:
* Ubuntu voor het gemak beheer en de stabiliteit (aanbevolen voor deze cursus).
* Linux Mint / ElementaryOS voor Windows / Mac overgangen.
* Lubuntu voor lichtgewicht apparatuur.

Specifiek:
* Ubuntu Studio voor audiovisuele middelen.
* Staarten voor veiligheid en privacy.

## Huiswerk
Als de training online wordt gedaan, zullen de deelnemers de taken individueel moeten uitvoeren.
* Het in kaart brengen van de actoren.
* Toolmapping.