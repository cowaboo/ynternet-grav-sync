---
title: 'Promotie en verspreiding'
objectives:
    - 'Wees je bewust van de invloed die deze migratie kan hebben op de mensen die deelnemen aan elk centrum'
    - 'Didactische strategieën op te zetten om te werken aan de nieuwe SO'
    - 'Strategieën voor te stellen voor de verspreiding van FLOSS'
    - 'Ontwerp strategieën om het didactisch materiaal aan te passen'
---

## De eerste indrukken 
Discussieforum, om de deelnemers te vragen hun ervaringen te delen en de eerste reacties van de deelnemers in elk centrum te presenteren.
Het bouwen van (alles bij elkaar genomen) enkele antwoorden om de moeilijkheden die zich hebben voorgedaan in evenwicht te brengen.
On-lineactiviteit of discussiegroepen ter plaatse.
## Set van redenen gegeven met als doel anderen ervan te overtuigen dat de migratie een goed idee is en een antwoord biedt op de problemen
Er zijn twee scenario's:
a)	De deelnemers zijn regelmatig in het centrum. Heb je al eerder aangegeven dat er een wijziging zal worden doorgevoerd? Hoe heb je dat gedaan? Wat heb je hen uitgelegd?
b)	De deelnemers zijn nieuw in het centrum. Hoe ga je met deze nieuwe technologie werken?

De stagiair moet kennis nemen van de meest voorkomende strategieën en deze aan het einde van de sessie samenvatten. In geval b) is het belangrijk om te beseffen dat als het OS niet wordt uitgelegd bij het werken met Windows of Mac, het ook niet zinvol is om het uit te leggen bij het werken met GNU / Linux.
Face-to-face formaat: rollenspel
Online formaat: discussieforums
## Het didactisch materiaal 
Tot slot wordt er aandacht besteed aan de noodzaak om het bestaande lesmateriaal aan te passen, zodat de sessies in elk centrum kunnen worden voortgezet.

Voorgesteld wordt het bestaande materiaal opnieuw te gebruiken om te werken aan digitale competenties en niet zozeer aan instrumenten. P.e, om te praten over office suites I niet van MSOffice; om te praten over tekstverwerkers en niet over Word...

Elke deelnemer is verplicht om een activiteit voor te stellen die in zijn of haar klas plaatsvindt en om het gebruikte ondersteunend materiaal te presenteren. De oefening zal bestaan uit het actualiseren van dit materiaal in functie van de nieuwe omgeving en het nieuwe perspectief. De trainer geeft advies en begeleiding bij de aanpassing van het vorige voorstel.

Het aanleveren van dit materiaal gebeurt in een online en open omgeving, zodat het dient als leermiddel voor de andere deelnemers.
## Huiswerk

## Referenties