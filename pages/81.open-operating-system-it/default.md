---
title: 'Sistemi operative aperti come transizione al FLOSS: GNU/Linux'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_id: 'Open operating system as a transition to FLOSS: GNU / Linux'
lang_title: it
length: '19 ore e 30 minuti'
objectives:
    - 'Mappatura dei bisogni e degli attori con cui collaborare'
    - 'Preparazione ed esecuzione tecnica della migrazione'
    - 'Routines di manutenzione di un’aula con GNU / Linux'
    - 'Strategie da seguire con i cittadini'
materials:
    - 'Computer con connessione internet'
    - 'Web browser aggiornato'
    - 'Un PC dove fare test di installazione (puoi perdere I dati salvati)'
    - '1 o 2 USB per installazioni OS'
skills:
    'Parole chiave':
        subs:
            'GNU / Linux': null
            Linus: null
            OS: null
            'Migrazione dei sistemi operativi': null
            'Amministrazione di sistema': null
---

## Introduzione
In questo modulo lavoreremo su livelli diversi, come la migrazione a GNU/Linux. Non coinvolgeremo le motivazioni politiche o teoriche che stanno alla base dell’utilizzo dei sistemi liberi, ma insieme ai partecipanti esamineremo le competenze tecniche necessarie e le strategie di insegnamento per la disseminazione. 
  
  L'infrastruttura tecnologica di un telecentro o di un centro di formazione digitale è il punto di partenza da cui sviluppare le proprie
  attività pedagogiche. La progettazione dell'infrastruttura definirà la politica di intervento che saremo in grado di sviluppare, i temi sui quali saremo in grado di lavorare,e le relative prospettive. Che si tratti di progetti di alfabetizzazione digitale di base o di una specializzazione digitale più complessa (video-edizione digitale, mondo dei produttori, competenze di programmazione avanzata.....) abbiamo finito per insegnare prodotti e tecnologie digitali specifiche. Il passaggio al lavoro con strumenti tecnologici liberi (FLOSS) può essere (deve essere?) accompagnato anche dal lavoro, con sistemi operativi liberi, offrendo la possibilità di comprendere la tecnologia come conoscenza aperta e condivisa al di là delle opzioni chiuse offerte dal mercato.
## Contesto
Obiettivi della sessione:
* Fornire  un ambiente sicuro per I partecipanti, permettendogli di passare a GNU / Linux nei loro spazi di lavoro.
* Creare un network di supporto tra i partecipanti.
* Promuovere la creazione di un network peer-to-peer, per fornire sostegno ai diversi agenti del territorio coinvolti nella disseminazione del PLL.

Metodologia:
* **Discussione**: Gruppi e forum di discussione.
* **Esecuzione**: attività pratiche svolte sui computer degli studenti.

## Sessioni
### Prima sessione: Le nostre esigenze
In questa sessione lavoreremo analizzando le esigenze di ogni telecentro/centro di formazione digitale, e lo collocheremo nel suo contesto. Saranno mappati i diversi agenti con cui ci si può relazionare e i propri ruoli: pubblica amministrazione, comunità di sviluppo di software libero, università, altri centri di formazione. Inoltre, saranno specificati gli strumenti FLOSS, quelli che possono rispondere alle esigenze educative di ogni spazio.
### Seconda sessione: I passi da fare
Sulla base delle esigenze rilevate, progetteremo un piano di migrazione, scegliendo la migliore SO che possa soddisfare le esigenze. Saranno analizzati e valutati diversi meccanismi di migrazione, in base alle esigenze di ogni centro. Verrà effettuata un'installazione di prova e verranno sviluppate linee guida di manutenzione e aggiornamento per garantire che il centro si mantenga in buona forma.
### Terza sessione: E ora?
Una volta completata l'installazione, e stabilite le regole di manutenzione e aggiornamento, impareremo come installare nuovi programmi in modo sicuro.
### Quarta sessione: Rinnovamento e disseminazione
È necessario lavorare a livello pedagogico, ad esempio spiegando come funziona alle persone che partecipano ad ogni centro e le principali ragioni dell'utilizzo di questo tipo di tecnologia. Studieremo e svilupperemo strategie per la diffusione dei sistemi FLOSS e per adattare il materiale didattico di ogni centro per continuare il suo compito principale: l'alfabetizzazione digitale o la formazione alle competenze digitali.