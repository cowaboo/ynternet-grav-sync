---
title: '¡Preparado para iniciar el aprendizaje electrónico! El uso del aprendizaje electrónico para promover la educación abierta'
length: '360 min'
objectives:
    - 'Proporcionar una visión general de la historia, desarrollo y filosofía de Moodle'
    - 'Reconocer los principales beneficios de Moodle'
    - 'Explorar las principales características de Moodle'
    - 'Utilizar tecnologías digitales abiertas para obtener apoyo de la comunidad Moodle'
    - 'Fomentar la implicación en la comunidad en línea y el desarrollo de software de código abierto'
---

## Debate grupal sobre Moodle
El formador iniciará la introducción al módulo preguntando a los participantes sobre su experiencia en el campo de algun sistema de gestión del aprendizaje de código fuente abierto para la educación y / o la formación laboral.
## Introducción a Moodle LMS: Libertad para aprender 
Moodle es una plataforma activa y en evolución, iniciada por el educador y programador informático australiano Martin Dougiama.

"Me comprometo a continuar con mi trabajo en Moodle y mantenerlo abierto y gratuito. Creo profundamente en la importancia de la educación sin restricciones y la enseñanza apoderado, y Moodle es la principal forma de contribuir a la realización de estos ideales"

Es el primer sistema de gestión de aprendizaje de código fuente abierto (LMS) publicado al público en 2002. Moodle 1.0 fue lanzado oficialmente en 2002 y se originó como plataforma para proporcionar a los educadores la tecnología para proporcionar aprendizaje en línea en entornos personalizados que fomenten la interacción, la investigación y colaboración Desde entonces ha habido series constantes de nuevos lanzamientos añadiendo nuevas funciones, una mejor escalabilidad y un mejor rendimiento.
Hoy en día, Moodle es el sistema de gestión de aprendizaje más popular y más utilizado del mundo. Con 100 millones de usuarios (y en crecimiento) y más de 100.000 sitios Moodle desplegados en todo el mundo, esta plataforma de aprendizaje electrónico, muy fácil de usar, sirve a las necesidades de aprendizaje y de formación de todo tipo de organizaciones en más de 225 países del mundo.
## ¿Por qué Moodle es el sistema de gestión de aprendizaje más utilizado?
Además de ser muy personalizable y flexible, Moodle nunca deja de mejorar. De hecho, al ser un proyecto de código abierto, Moodle es un esfuerzo colaborativo y cuenta con el apoyo de una fuerte comunidad global. Así, los desarrolladores de todo el mundo pueden acceder al código y modificarlo para mejorar el software y hacerlo más seguro.

Originalmente diseñado para la educación superior, Moodle se utiliza no sólo en centros de secundaria, escuelas primarias, organizaciones sin ánimo de lucro, empresas privadas, por profesores independientes e incluso por familias en el ámbito de la educación doméstica. Desde la educación, Moodle ha llegado al mundo corporativo, y ahora también está ganando popularidad en el sector de la salud.
## Características básicas de Moodle
Introducción a:
* Interfaz
* Panel
* Herramientas y actividades colaborativas
* Calendario todo en uno (all-in-one)
* Gestión de archivos
* Gestión de textos
* Notificación
* Seguimiento del progreso

## Contribuir al desarrollo del software de código abierto
Al ser una plataforma de aprendizaje de código abierto, Moodle nos ofrece las posibilidades de contribuir a la plataforma de muchas maneras diferentes, tales como:
* Asistencia al usuario: obtenga apoyo de la comunidad, compartir ideas y conoce los demás a través del [the Community Forum](https://moodle.org/course/) 
* Testeo: ayudar a probar e informar de errores a través de Moodle Tracker. 
* Usabilidad: realizar algunas pruebas de usabilidad y informar de los resultados. 
* Traducción: ayudar con la traducción de Moodle, incluida la contribución de traducción / corrección. 
* Social Media: ayudar a concienciar al proyecto Moodle y compartir las novedades y eventos del mundo Moodle. 

Además, los usuarios tienen la posibilidad de unirse a [Moodle Users Association](https://moodleassociation.org/), una organización sin ánimo de lucro dedicada a contribuir al desarrollo de Moodle. La asociación de usuarios de Moodle apoya el crecimiento de Moodle proporcionando una voz fuerte y unida a los usuarios, dando dirección y recursos para los nuevos desarrollos.
## Trabajo en casa (autónomo)
Desde el sitio web de Moodle, vaya a la sección de [demo](https://moodle.org/demo/) y explora [Mount Orange school Moodle](https://school.moodledemo.net/mod/page/view.php?id=45). A partir de ahí, utilizando las credenciales (inicie sesión como 'profesor', contraseña 'moodle'), puedes navegar dentro de una plataforma Moodle, configurar algunas actividades y, después, iniciar la sesión como estudiante (nombre de usuario: estudiante; contraseña: moodle) para probarlas.
## Referencias
* [What is Moodle?](https://www.youtube.com/watch?v=wop3FMhoLGs)
* [Introduction to Moodle](https://www.lambdasolutions.net/guides-and-research/moodle-user-guide-intro-to-moodle).
* [Using Moodle](https://support.moodle.com/hc/en-us/categories/115000822747-Using-Moodle).
* [Moodle’s core features](https://docs.moodle.org/37/en/Features).
* [Moodle Dashboard](https://docs.moodle.org/37/en/Dashboard).
* [Moodle Activities](https://docs.moodle.org/37/en/Activities).
* [Moodle Calendar](https://docs.moodle.org/37/en/Calendar).
* [Working with files](https://docs.moodle.org/37/en/Working_with_files).
* [Moodle Text editor](https://docs.moodle.org/37/en/Text_editor).
* [Moodle Messaging](https://docs.moodle.org/37/en/Messaging).
* [Tracking progress](https://docs.moodle.org/37/en/Tracking_progress).
* [Contributing to Moodle](https://docs.moodle.org/dev/Contributing_to_Moodle).
* [Moodle Users Associations](https://moodleassociation.org/).
* [Moodle Community Forums](https://moodle.org/course/).