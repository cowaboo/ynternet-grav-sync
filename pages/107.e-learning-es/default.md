---
title: 'E-Learning y FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'E-Learning with FLOSS tools'
lang_id: 'E-Learning with FLOSS tools'
lang_title: es
length: '9 horas y 30 minutos'
objectives:
    - 'Reconocer las ventajas del E-Learning en educación'
    - 'Explorar el principal sistema de gestión de aprendizaje de código abierto: MOODLE'
    - 'Descubrir y beneficiarse de la comunidad Moodle'
materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a Internet'
    - Proyector
    - Moodle
skills:
    'Palabras clave':
        subs:
            'Aprendizaje permanente': null
            'Sistema de gestión de aprendizaje (LMS)': null
            'Aprendizaje asincrónico': null
            Moodle: null
            MOOC: null
---

# Introducción
_E-Learning explota tecnologías interactivas y sistemas de comunicación para mejorar la experiencia de aprendizaje. Tiene el potencial de transformar la forma en que enseñamos y aprendemos en todos los ámbitos. Puede elevar los estándares y ampliar la participación en el aprendizaje permanente. No puede reemplazar a las personas docentes, pero junto con los métodos existentes puede mejorar la calidad y el alcance de su enseñanza y reducir el tiempo dedicado a la gestión. Puede permitir a cada estudiante participante alcanzar su potencial y ayudar a construir una fuerza laboral educativa capacitada para realizar cambios. Hace posible un sistema educativo verdaderamente ambicioso para una futura sociedad de aprendizaje mejorada_
**Towards a Unified e-Learning Strategy, The DfES e-Learning Strategy Unit, 2003.**

Este escenario de capacitación proporciona una introducción a las metodologías y herramientas de E-Learning. Teniendo en cuenta que las herramientas de e-learning rediseñaron el entorno educativo, las sesiones están diseñadas para ofrecer una visión general de los antecedentes del aprendizaje electrónico, sus características y los métodos clave.

La formación analizará alguna de las ventajas del aprendizaje en línea. En particular, permitirá a las estudiantes explorar el potencial del E-Learning como una herramienta para fomentar la colaboración en línea. De hecho, eLearning puede ser una herramienta efectiva tanto para estudiantes como para educadores, especialmente para promover la colaboración en línea (plataformas de e-Learning). Las personas participantes probarán el funcionamiento de la plataforma de aprendizaje Moodle para trabajar y aprender colaborativa y cooperativamente a través de foros, wikis, glosarios, actividades de bases de datos y mucho más. La capacitación proporcionará a las alumnas una comprensión crítica del proceso de aprendizaje.
## Contexto
Los cambios evolutivos en la tecnología educativa han cambiado el concepto de enseñanza y aprendizaje tal como lo conocemos.
Ciertamente, hay muchos beneficios para la enseñanza / aprendizaje en un entorno en línea. E-Learning puede hacer que el aprendizaje sea más simple, más fácil y más efectivo. Además de adecuarse a diferentes estilos de aprendizaje, el e-Learning también es una forma de proporcionar una rápida implementación de la formación. También es una herramienta relevante en términos de colaboración y desarrollo comunitario. De hecho, los procesos de e-Learning no tienen que ser un viajes individuales. A través de recursos como foros y tutoriales en directo, las personas participantes pueden tener contacto con otras personas en la comunidad de aprendizaje. El compromiso con otras personas promueve la colaboración y la cultura del equipo, lo que tiene beneficios más allá del entorno de capacitación.
Al final de las sesiones, las participantes podrán usar y beneficiarse de Moodle, una de las principales plataformas de aprendizaje de código abierto que ofrece un poderoso conjunto de herramientas centradas en el alumnado y en los entornos de aprendizaje colaborativo que potencian tanto la enseñanza como el aprendizaje.
## Sesiones
### Primera sesión: Conceptos básicos de E-Learning: antecedentes y evolución
El e-Learning utiliza la tecnología para ayudar y mejorar el aprendizaje. El propósito del e-Learning es permitir que las personas aprendan sin asistir físicamente a un entorno educativo tradicional. Para comprender mejor cómo el e-Learning beneficia tanto a las personas educadoras como a las estudiantes hoy en día resulta muy útil conocer sus antecedentes. El término 'e-Learning' existe desde 1999, cuando se utilizó por primera vez en un seminario de sistemas de TCC. El experto en tecnología educativa Elliott Maisie acuñó el término ""e-Learning"" en 1999. Sin embargo, los principios detrás del aprendizaje electrónico han sido bien documentados a lo largo de la historia, e incluso hay evidencia que sugiere que existían formas tempranas de aprendizaje electrónico desde el siglo XIX. Hoy, el e-Learning es más popular que nunca.

La sesión también ofrece una visión general de algunas de las tecnologías involucradas en e-Learning, como la creación de contenido, LMS y TMS. En la ""era de la información"", el aprendizaje permanente se considera clave para el éxito continuo de la sociedad moderna. El e-Learning es considerado por muchos como la mejor solución viable para facilitar la puesta a disposición de los recursos necesarios para promover el aprendizaje permanente. ¿Qué factores han facilitado que e-Learning se haya convertido en la forma más popular de impartir formación hoy en día? ¿Por qué usar E-Learning? Esta sesión ofrece una visión general del alcance potencial y las ventajas del e-Learning.
### Segunda sesión: ¡Listo para comenzar a aprender en línea! Usando el e-Learning para promover la Educación Abierta
Esta sesión proporcionará una visión general amplia de lo que puede hacer Moodle. Explorará el uso y los beneficios de Moodle, uno de los sistemas de gestión de aprendizaje (LMS) de código abierto más populares. La capacitación proporcionará la información básica necesaria para que los alumnos comiencen con la plataforma de aprendizaje y se beneficien de la asistencia en línea y del intercambio de ideas de las personas usuarias de todo el mundo.

Al final de las sesiones, las personas participantes podrán usar y beneficiarse de Moodle, una de las principales plataformas de aprendizaje de código abierto que ofrece un poderoso conjunto de herramientas centradas en el alumno y entornos de aprendizaje colaborativo que potencian tanto la enseñanza como el aprendizaje.