---
title: 'Bases del aprendizaje electrónico o digital - antecedentes y evolución'
length: '210 min'
objectives:
    - 'Identificar las perspectivas históricas y el aumento del aprendizaje electrónico'
    - 'Descubrir las principales herramientas de aprendizaje electrónico'
    - 'Explorar el potencial de los diferentes programas de aprendizaje electrónico'
    - 'Evaluar los pros y los contras de los diferentes programas de aprendizaje electrónico'
    - 'Descubrir las ventajas del aprendizaje electrónico para satisfacer una amplia gama de necesidades de aprendizaje'
    - 'Identificar la eficacia de los entornos de aprendizaje electrónico'
    - 'Explorar el potencial del aprendizaje electrónico para promover el aprendizaje a lo largo de la vida'
---

## Introducción
El formador iniciará la introducción al módulo preguntando a los participantes sobre sus conocimientos / experiencia anteriores en el campo del aprendizaje en línea. Se pedirá a los participantes que se centren en las ventajas y las desventajas del aprendizaje en línea.
## Fundamentos de aprendizaje electrónico
En 1999, durante el seminario CBT Systems en EEUU, se utilizó por primera vez una palabra nueva en un entorno profesional: "Aprendizaje electrónico". Asociada a expresiones como "aprendizaje en línea" o "aprendizaje virtual", esta palabra tenía la intención de calificar "una manera de aprender basada en el uso de nuevas tecnologías que permitan acceder a una formación en línea, interactiva y en ocasiones personalizada a través de Internet u otros medios electrónicos (intranet, TV interactiva, etc.), a fin de desarrollar competencias mientras que el proceso de aprendizaje sea independiente del tiempo y del lugar ".

Hoy en día, gracias a la introducción de ordenador e internet, se han ampliado las herramientas de aprendizaje electrónico y los métodos de impartición. Hoy en día, el E-Learning o aprendizaje electrónico es más popular que nunca, ya que muchas personas se dan cuenta de las ventajas que el aprendizaje en línea puede ofrecer. Las empresas también han comenzado a utilizar e-Learning para formar a sus empleados, dándoles la posibilidad de mejorar su base de conocimiento del sector y ampliar sus conocimientos. En casa, los individuos tienen acceso a programas que les ofrecen una capacidad de obtener titulaciones en línea y enriquecer la vida mediante un conocimiento amplio.

![](elearning1.png)

[eFront learning](https://www.efrontlearning.com/blog/2013/08/a-brief-history-of-elearning-infographic.html)
## Exploración de las herramientas de aprendizaje electrónico
Hay un montón de tecnologías relacionadas con el aprendizaje electrónico. Se pueden dividir en tres áreas clave: creación de contenido, LMS y TMS. Antes de comenzar a hablar del uso de herramientas de aprendizaje electrónico, es importante conocer la diferencia entre las diferentes herramientas que se utilizan para crear, desarrollar e impartir contenido a través del aprendizaje electrónico.
* Autoría de contenido: una herramienta de autoría de contenidos de aprendizaje electrónico es un paquete de software que los desarrolladores utilizan para crear y empaquetar contenidos de aprendizaje electrónico que se pueden entregar a los usuarios finales. Una herramienta de autoria de contenido es una aplicación de software usada para crear contenido multimedia normalmente para su publicación en la World Wide Web.
* Un LMS (Learning Management System) es una aplicación de software para la administración, documentación, seguimiento, informes e impartición de cursos educativos, programas de capacitación o programas de aprendizaje y desarrollo. Por lo tanto, LMS es una solución de software que crea y ofrece entornos de aprendizaje personalizados para los estudiantes. Los LMS presentan material educativo en múltiples formas y a través de una variedad de actividades, como cuestionarios, contenido de vídeo y material, para que los estudiantes trabajan a su ritmo. Los LMS se centran en la realización de enseñanza y aprendizaje en línea, pero admiten una variedad de usos, actuando como una plataforma para el contenido en línea, incluidos los cursos, tanto asíncronos como sincrónicos. Un LMS entrega y administra todo tipo de contenido, incluidos vídeos, cursos y documentos. En los ámbitos de la educación  primària, secundaria y superior, un LMS incluirá una variedad de funcionalidades similares a las corporativas, pero tendrá características como rúbricas, aprendizaje facilitado por docentes e instructores, un panel de discusión y, a menudo, un programa de formativo. Un programa de estudios rara vez es una característica en el LMS corporativo, aunque los cursos pueden comenzar con un índice para dar a los estudiantes una visión general de los temas cubiertos.
El LMS se puede usar para crear contenido de curso estructurado profesional. El profesor puede añadir texto, imágenes, tablas, enlaces y formato de texto; así como pruebas interactivas, presentaciones de diapositivas, etc. Ayuda a controlar a qué contenido puede acceder un estudiante; a realizar un seguimiento del progreso del estudio e involucrar al alumno con las herramientas de comunicación. Los maestros pueden administrar cursos y módulos, inscribir estudiantes o configurar la autoinscripción, ver informes sobre los estudiantes e importar estudiantes a sus clases en línea.
* El TMS (Training Management System) se utiliza por los proveedores de formación con el fin de gestionar los aspectos comerciales de sus operaciones formativas. Incluye la gestión de fechas y sesiones de formación, las inscripciones y el pago en línea, la promoción de cursos y la comunicación con las personas inscritas.

## Diferentes tipos de aprendizaje electrónico 
Cuanto más nuestros aprendizajes electrónicos se ajusten a nuestras necesidades y hábitos de aprendizaje, más fácil será para nosotros beneficiarnos de las herramientas de aprendizaje electrónico.
Alinear nuestro enfoque de aprendizaje electrónico según nuestras necesidades es crucial para sacar el máximo partido a su entorno de aprendizaje. Los programas de aprendizaje electrónico pueden adoptar muchas formas, tales como:
* Aprendizaje en línea mixto: es un enfoque a la educación que combina materiales educativos en línea y oportunidades de interacción en línea con métodos tradicionales basados en aulas presenciales. Requiere la presencia física tanto del profesor como del alumno, con algunos elementos de control del alumnado (tiempo invertido, el lugar, etc.) Permite la retroalimentación inmediata y el análisis de los resultados que pueden ayudar a los estudiantes a identificar sus errores, en las zonas débiles y a mantenerse involucrados. Las ventajas del aprendizaje mixto conllevan la interacción entre grupos más grandes, la interacción cara a cara, el aprendizaje autónomo y el coste reducido.
* Aprendizaje en línea asíncrono: este tipo de aprendizaje electrónico no depende ni de la ubicación ni del tiempo. El aprendizaje electrónico asincrónico se gestiona mejor mediante un sistema de gestión de aprendizaje (LMS) que permite hacer un seguimiento del progreso de cada aprendiz. Generalmente, la interacción se produce a través de los bloques y los tableros de discusión que sustituyen el elemento presencial o la reunión de clase. Los usuarios reciben el contenido y los detalles relevantes del trabajo realizado, así como las evaluaciones. El aprendizaje electrónico asíncrono es un enfoque centrado en los aprendizajes. El aprendiz decide cuando quiere abordar una nueva pieza de aprendizaje electrónico, cuando quiere hacer una pausa y qué parte de aprendizaje electrónico quiere saltar porque el conocimiento / competencia ya está logrado.
* Aprendizaje en línea sincrónico: se produce en tiempo real, lo que significa que tanto los estudiantes como el formador se reunirán al mismo tiempo en un aula virtual. El encuentro entre los estudiantes y el facilitador, independientemente de su ubicación, es una de los mayores ventajas del aprendizaje electrónico sincrónico en comparación con el anterior, siempre que haya una buena conexión a Internet. De esta manera se produce un ahorro de recursos (en términos de tiempo y costes de desplazamientos) tanto para los estudiantes o participantes como para el dinamizador/a.

![](elearning2.png)

[MDI Training](https://www.mdi-training.com/blog/blog/elearning/)
## Aprendizaje permanente
Los desarrollos tecnológicos han contribuido considerablemente a los importantes cambios que se han producido en casi cualquier sector de nuestra vida, incluida la educación. En un mundo tan globalizado, hay una demanda creciente de nuevas habilidades y capacidades. "Las habilidades TIC son una necesidad imprescindible en el mercado global y un aprendizaje permanente y flexible es necesario para mantenerse competitivos. La educación abierta y a distancia (ODE) es suficientemente flexible para dar respuestas al reto del aprendizaje a lo largo de la vida en la sociedad global "(Bodil Ask, Sven Ake Bjorke, UNU / Global Virtua University, Noruega).
## Trabajo en casa (autónomo)
Para tener una mejor idea de los pros y los contras de la enseñanza en línea, se pide a los participantes que miren algunos recursos y que tomen notas mientras vean los vídeos:
* Ventajas y desventajas del aprendizaje en línea [Online learning advantaged ad disadvantages](https://www.youtube.com/watch?v=tcb9puhA3fo).
* Las ventajas del aprendizaje electrónico [The e-learning advantages](https://www.youtube.com/watch?v=nzV1NmhC7ik).

Después, se pide a los estudiantes que generen una lista de pros y contras.
## Referencias
* [History of E-Learning](http://www.leerbeleving.nl/wbts/1/history_of_elearning.html).
* [Introduction to eLearning, Course Authoring, LMS](https://www.youtube.com/watch?v=nEYAWU5fQOs).
* [What are content authoring tools and how can we use them?](https://www.talentlms.com/elearning/what-is-a-content-authoring-tool).
* [LMS vs CMS vs LCMS…What’s the Difference?](https://www.lessonly.com/blog/lms-vs-cms-vs-lcms-whats-difference/).
* [What is a Training Management System?](https://elearningindustry.com/training-management-system-tms).
* [LMS -Wikipedia](https://en.wikipedia.org/wiki/Learning_management_system).
* [Types of E-Learning](https://www.youtube.com/watch?v=XL5gZYqHO3M).
* [E-Learning ways](https://elearningways.com/what-is-e-learning-why-does-it-matter-lets-explore/).