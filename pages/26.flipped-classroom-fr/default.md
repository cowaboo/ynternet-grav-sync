---
title: 'Classe inversée et apprentissage par projet et par problème'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: fr
length: '9 heures'
objectives:
    - 'Comprendre le cadre théorique et les facteurs clés des 3 méthodes / stratégies: classe inversée, apprentissage par problème et apprentissage par projet'
    - 'Apprenez à concevoir ou à repenser la stratégie d''nseignement-apprentissage à l''ide de sources et d''outils ouverts'
    - 'Créer et évaluer des activités basées sur ces méthodes'
materials:
    - 'Ordinateur personnel (smartphone ou tablette connecté à Internet)'
    - 'Connexion Internet'
    - Bearmer
    - 'Papier et stylos'
    - 'Tableau à feuilles'
    - Haut-parleurs
skills:
    'Mots clés':
        subs:
            'Classe inversée (FC)': null
            'Apprentissage par problèmes (APB)': null
            'Apprentissage par projets (Projet BL)': null
            Ouvert: null
            Méthodes: null
            Continu: null
            Volontaire: null
            Auto-motivé: null
---

## Introduction
Classe inversée, apprentissage par projet, apprentissage par problème ... différentes méthodes, techniques ou méthodologies qui ont quelque chose en commun: la position dans laquelle les élèves sont placés est proactive, dans la gestion de la puissance de leur besoin d'apprendre. Si les élèves travaillent en groupe, comme dans les scénarios réels, l'apprentissage devient plus significatif. Avec ces stratégies, le protagonisme est placé chez les personnes désireuses d'apprendre et le rôle de l'enseignant / formateur est celui d'un compagnon qui a certaines connaissances mais qui veut aussi apprendre avec les élèves.

En bref, **The Flipped Classroom** (FC) est un modèle pédagogique qui déplace le travail de certains processus d'apprentissage en dehors de la classe et investit le temps de la session en classe, ainsi que l'expérience de l'enseignant, pour promouvoir et stimuler d'autres processus d'acquisition et de pratique des connaissances à l'intérieur. la salle de classe.
Avec l'**apprentissage par problèmes** (APB), l'enseignant pose une question ou un problème et les élèves doivent donner une réponse ou trouver un moyen de le résoudre à travers un processus; eux, le groupe des enseignants et des étudiants, établissent tous ensemble une étape importante dans le processus d'apprentissage.
Dans **Project Based Learning** (Project BL), les étudiants sont également au centre du processus d'apprentissage, en tant que protagoniste capable de générer des solutions en réponse aux différentes opportunités; le processus est étroitement lié à l'environnement de travail et le principal résultat est un produit.
Le module se concentrera sur ces stratégies, montrant les idées et les facteurs clés les plus pertinents et mettra les participants au défi de repenser leurs pratiques pédagogiques.
## Contexte

## Séances
### Première séance : Basée sur la compréhension de la stratégie de la classe inversée
Nous passerons en revue les composants, les étapes et les phases. Développement de ressources pour la classe inversée avec les technologies, outils et stratégies FLOSS.
### Deuxième séance : Axée sur la méthode d'apprentissage par problèmes
Étapes. Approche des problèmes pertinents pour les étudiants. Problèmes de brainstorming à résoudre. Ressources open source disponibles pour la résolution des problèmes. Outils habituels.
### Troisième séance : Pour objectif de parler de la stratégie d'apprentissage par projet: composantes, étapes, phases
Travailler dans des situations réelles. Le défi comme moteur d'apprentissage. Collaboration et coopération entre étudiants.
### Quatrième séance : Dédiée au projet personnel ou grupal