---
title: 'Omgedraaid klaslokaal'
objectives:
    - 'Om een inleiding te geven op Flipped Classroom en de belangrijkste concepten die daarbij komen kijken'
    - 'Om de transformatie van een training in geflipt klaslokaal te faciliteren'
    - 'Het ontdekken van de Bloom''s Taxonomy en de verschillen tussen de vaardigheden van het denken op laag niveau en het denken op hoog niveau, evenals de verschillende categorieën van vaardigheden (kennis, begrip, toepassing, analyse, synthese en evaluatie)'
    - 'Om studenten te motiveren hun onderwijsmethode te veranderen'
---

## Introductie
Het doel van dit eerste deel is de voorkennis van de groep naar voren te brengen. De deelnemers worden ook uitgenodigd om informatie te delen over de context van hun deelname aan de sessie (motivatie, persoonlijke doelen, enz.).
Daarna zal de trainer het "The Flipped Classroom (FC) concept presenteren door gebruik te maken van de dia's die op Slidewiki zijn voorbereid. 

In het kort: "De Flipped Classroom (FC) is een pedagogisch model dat het werk van bepaalde leerprocessen buiten het klaslokaal verplaatst en de tijd van de klassikale sessie investeert, samen met de ervaring van de leerkracht, om andere processen van verwerving en kennisbeoefening binnen het klaslokaal te bevorderen en te stimuleren" (http://www.theflippedclassroom.es/what-is-innovacion-educativa/). De trainer gebruikt de beschikbare digitale middelen en tools om de leerlingen op voorhand een inkijk te bieden op de noodzakelijke inhoud, die ze zelf zullen beoordelen voordat ze de les bijwonen.

Als de leerlingen eenmaal in de klas zijn, in contact met hun medeleerlingen en de leerkracht(en), investeren ze hun tijd in het werken met de vragen die ze in het vorige stadium niet zelf hebben kunnen oplossen. Peer to peer support is essentieel in deze methode. 

Door gebruik te maken van een Flipped Classroom model...
* Studenten leren online nieuwe inhoud door het bekijken van videocolleges, meestal thuis → begrip en kennisactiviteiten
* En wat vroeger huiswerk was (toegewezen problemen) wordt nu in de klas gedaan met een leraar die meer persoonlijke begeleiding en interactie met de leerlingen biedt. → Toepassing - Analyse - Synthese- en evaluatieactiviteiten.

Vergelijking:
![](traditional.jpg)
![](traditional.jpg)
Bron voor de beelden: http://uaflipped.com/blooms-taxonomy/

## Groepsdiscussie
In kleine groepen werken de deelnemers aan de taxonomie van Bloom en denken ze na over verschillende activiteiten die thuis / in de klas ontwikkeld kunnen worden, volgens het Flipped Classroom-model. Na 5' individuele reflectie begint de trainer met een brainstorm en maakt hij aantekeningen over de suggestie van de deelnemer (10'). 
Tot slot zal de trainer de taxonomie van Bloom in detail laten zien/uitdelen en uitleggen hoe deze werkt (5'):
![](taxonomy.png)

De deelnemers nemen (individueel of in groep) het bewerkbare document met de taxonomie mee en voegen een nieuwe rij toe, om de digitale hulpmiddelen, software of andere digitale bronnen toe te voegen die nuttig kunnen zijn om de "acties" en de "resultaten" (derde rij van het document) te bereiken. Trainer zal de nadruk leggen op het gebruik van FLOSS en open en vrije technologieën. 
De deelnemers zullen ook nadenken over welk deel van de taxonomie "thuis" of "in de klas" kan worden ontwikkeld en proberen (in abstracte vorm) enkele activiteiten voor te stellen die tijdens een specifieke training kunnen worden gedaan (aan de hand van een concreet voorbeeld kan dit zeer nuttig zijn).
bijvoorbeeld 
thuis: 
* Kennisgerelateerd (uitgebreid lezen, invullen van vragenlijsten, opstellen van een woordenlijst, etc.)
* Begrijpen-gerelateerd: het maken van een infographic, een, samenvatting, een post, een digitale presentatie...

In de klas, met collega's en leerkrachten
* Sollicitatiegerelateerd: een interview met een expert, een virtueel scenario, een tijdlijn met de volgorde van een actieplan...
* Analyse-gerelateerd: conceptuele kaart, vergelijkende grafiek...
* Beoordeling: discussiëren, debatteren, echte of gesimuleerde proeven of experimenten, enz.
* Creatie-gerelateerd: video, postcast...

Na 45' zullen de deelnemers hun bevindingen met de groep delen en commentaar geven op hun ervaringen. 
## Huiswerk
Denk aan een training die je eerder hebt gegeven en de activiteiten die je aan je leerlingen hebt voorgesteld: als je het Flipped Classroom-model zou toepassen, welk deel zou dan thuis worden ontwikkeld en welk deel zou in de klas worden gedaan?