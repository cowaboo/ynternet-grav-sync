---
title: 'Probleemgestuurd Leren'
objetives:
    - 'Om een inleiding te geven op Probleemgestuurd Leren'
    - 'Het werk te faciliteren met open leer/onderwijsmethoden'
    - 'Het ontdekken van nieuwe benaderingen van het leerproces en het stimuleren van de motivatie en betrokkenheid van de deelnemers. Het ontdekken van de voordelen van student-centered methoden'
    - 'Om studenten te motiveren om hun onderwijsmethode te veranderen'
---

## Introductie
De trainer zal het "Probleemgestuurd Leren" concept en haar methodes introduceren. Uitgangspunt is een "case review" en we stellen voor om gebruik te maken van het "Technovation Girls" project (zie https://technovationchallenge.org/) , een project dat teams van meisjes uitdaagt om na te denken over lokale problemen en een oplossing te coderen (ze bouwen een app). Het is beter om een lokaal voorbeeld te vinden, dus we raden de trainer aan om te zoeken naar lokale voorbeelden op YouTube door te zoeken op "Technovation + naam van het land"

Na het spelen van een aantal van de beschikbare video's, zal de trainer een aantal elementen van dit (of een soortgelijk) project belichten: 
* het is een uitdaging 
* ze werken in groepen
* ze werken aan het oplossen van een echt probleem, maar het is niet nodig dat het probleem aanwezig is in de context van de studenten (ze kunnen werken aan andermans problemen)
* ze leren wat ze nodig hebben om een oplossing te bieden (dat is niet hetzelfde voor de verschillende groepen)
Trainer zal uitleggen dat het tegenwoordig heel gemakkelijk is om echte problemen te identificeren, door gebruik te maken van de 17 doelen om onze wereld te transformeren: https://www.un.org/sustainabledevelopment/sustainable-development-goals/
![](sustainable.png)

Probleemgestuurd leren is een methode die begint met het opstellen van een vraag of een probleem door de leerkracht en die de leerlingen uitdaagt om een antwoord te geven op de vraag of om een manier te vinden om het probleem op te lossen. Soms nodigt de leerkracht de leerlingen uit om na te denken over het probleem. PBL stelt de leerkracht en de leerling in staat om een mijlpaal in het leerproces te bereiken, waarbij de leerling de hoofdrolspeler is.

Door gebruik te maken van de Problem Based Learning-methodologie...
* Leer om beslissingen te nemen, individueel of in onderhandeling (met de groep)
* Interactie met collega's en ook met docenten
* Gebruik de informatie van hun nabije context
* Zoek de informatie die ze nodig hebben
* Ideeën produceren en delen
* Bespreek andere mogelijke oplossingen en ook de mechanismen om deze te vinden.
De problemen moeten in overeenstemming zijn met de capaciteiten van de studenten en moeten voor hen belangrijk zijn. Problemen worden uit de echte wereld genomen en kunnen "echt" of "potentieel" zijn. 
Het is beter om een echt scenario te gebruiken om de zaken makkelijker te maken.

●	Het probleem moet de studenten motiveren om te zoeken naar een dieper begrip van de concepten.
●	Het probleem zou van de studenten moeten eisen dat zij op basis van redenen beslissingen nemen en deze verdedigen.
●	Het probleem moet de inhoudelijke doelstellingen zodanig integreren dat het aansluit bij eerdere cursussen/kennis.
●	Als het probleem wordt gebruikt voor een groepsproject, moet het een niveau van complexiteit hebben dat ervoor zorgt dat de studenten samen moeten werken om het op te lossen.
●	Indien gebruikt voor een meerfaseproject, moeten de eerste stappen van het probleem open en boeiend zijn om de studenten in het probleem te betrekken. (Duch, Groh, en Allen, 2001)
![](gold.png)
Bron van de afbeelding: https://www.pblworks.org/blog/gold-standard-pbl-essential-project-design-elements
Met deze methode kunnen studenten hun kennis, vaardigheden en attitudes verbeteren/veranderen. 
Op dit moment zal de trainer de deelnemers vragen om te brainstormen over enkele problemen die aan hun leerlingen kunnen worden voorgelegd. De trainer maakt aantekeningen op het bord / de flip-over. 
## Ondersteuning: dia's & video & web
De trainer presenteert het PBL-schema en legt uit dat het erom gaat het proces te volgen: eerst wordt het probleem geanalyseerd, daarna zoeken de deelnemers de nodige informatie om het probleem te begrijpen en leren ze de nodige vaardigheden om de informatie te begrijpen. Vervolgens kunnen ze de informatie integreren en kunnen ze het toepassen op een bepaalde hoeveelheid kennis en wijsheid om het probleem op te lossen.
Het is belangrijk om te begrijpen dat elke leerling andere vaardigheden moet leren/krijgen, omdat zijn of haar voorkennis anders is. 
De stadia zijn:
1.	Presentatie van het op te lossen probleem / identificatie van een probleem dat de groep interesseert. 
2.	Analyse van de voorkennis
3.	Identificatie van de kennis en de middelen die nodig zijn om een oplossing te ontwikkelen
4.	Lijst van acties of activiteiten die moeten worden uitgevoerd om een oplossing te vinden.
5.	Voorbereiding van het werkplan
6.	Doen wat gepland is: zoeken naar de nodige informatie, uitvoeren van acties, doorgeven van de deelresultaten
7.	Uitwerking van het antwoord en argumentatie over de gebruikte methode om het te vinden.

Zoals de trainer een voorbeeld heeft gepresenteerd (Technovation girls one), zal het voorbeeld transformeren in fasen, zodat de deelnemers zich kunnen realiseren hoe gemakkelijk het is om het te doen.
## Iedereen opent zijn of haar laptop
Het doel van dit deel van de sessie is om de deelnemers een concreet voorstel te laten uitwerken dat aan de studenten kan worden voorgelegd. De deelnemers werken in groepjes en gebruiken een computer om een digitale presentatie te maken met hun voorstel. 
Zij:
* Zal een zeer concreet probleem in kaart brengen en presenteren, dat verband houdt met ODS, en zal een potentiële groep leerlingen identificeren (leeftijd, achtergrond, speciale behoeften... ).
* Een aantal kennis/vaardigheden te identificeren die zijn/haar leerlingen zouden moeten hebben om het probleem op te lossen (dan zullen de leerlingen mogen identificeren of ze een deel van deze kennis/vaardigheden hebben of moeten leren/verwerven).
* Een eerste lijst opstellen van de activiteiten die moeten worden uitgevoerd. Bijvoorbeeld: een plaats bezoeken, iemand interviewen, zoeken op internet, een onderzoek uitvoeren, een berekening maken met open data; een app of een videogame programmeren, een presentatie maken, een infographic maken...
* Een potentieel werkplan opstellen (dat door de leerlingen zal worden aangepast wanneer ze de activiteit uitvoeren).

Na 40' werken in kleine groepen zullen de groepen hun voorstel met de klas delen.
## Huiswerk
Na het beluisteren van de verschillende voorstellen, denk na over een concreet probleem dat met uw leerlingen in uw klas moet worden opgelost. Denk na over pro's en tegen het gebruik van deze methode in je klas.
## Referenties
* Duch, B. J., Groh, S. E, & Allen, D. E. (Eds.). (2001). De kracht van probleemgestuurd leren. Sterling, VA: Stylus.
* Grasha, A. F. (1996). Lesgeven met stijl: Een praktische gids voor het verbeteren van het leren door het begrijpen van onderwijs en leerstijlen. Pittsburgh: Alliance Publishers.
* Het Centrum voor Innovatie in Onderwijs & Leren (CITL): [Probleemgestuurd Leren (PBL)](https://citl.illinois.edu/citl-101/teaching-learning/resources/teaching-strategies/problem-based-learning-(pbl))
* [PBL](http://www1.udel.edu/inst/) via het Institute for Transforming Undergraduate Education van de Universiteit van Delaware