---
title: 'Flipped classroom en Project- en Probleemgestuurd Leren'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: nl
length: '9 uren'
objectives:
    - 'De basisprincipes van de Flipped Classroom (FC) strategie begrijpen en in staat zijn om deze te implementeren en een mind-map te creëren om deze uit te leggen aan de anderen'
    - 'Ontwerp de structuur van een nieuwe omgedraaide lesmodule (of transformeer een klassieke module) om een onderwerp te onderwijzen/leren, met behulp van open bronnen en tools'
    - 'De basisprincipes van de Problem Based Learning strategie begrijpen en deze kunnen uitleggen aan de studenten en collega''s'
    - 'Ontwerp een PBL-activiteit (of transformeer een reeds bestaande activiteit) die kan worden toegepast om het gebruik van enkele open softwaretools aan te leren'
    - 'De basisprincipes van de Project Based Learning strategie begrijpen'
    - 'Ontwerp een PBL activiteit (of transformeer een reeds bestaande activiteit) die kan worden toegepast om het belang van het promoten van FLOSS te leren'
materials:
    - 'Persoonlijke computer (Smartphone of tablet verbonden met het internet)'
    - Internetverbinding
    - Bearmer
    - 'Papier en pennen'
    - Flipchart
    - Luidsprekers
skills:
    Sleutelwoorden:
        subs:
            'Flipped Classroom (FC)': null
            'Problem Based Learning (PBL)': null
            'Project Based Learning (Project BL)': null
            Open: null
            Methoden: null
            Doorlopend: null
            Vrijwillig: null
            Zelfmotiverend: null
---

## Introductie
"Zeg het me en ik vergeet het, leer het me en ik mag het me herinneren, betrek me erbij en ik leer." Benjamin Franklin. "Het grootste teken van succes voor een leraar... is om te kunnen zeggen: "De kinderen werken nu alsof ik niet besta." Maria Montessori, Italiaanse opvoeder en auteur.

Flipped Classroom, projectgebaseerd leren, probleemgebaseerd leren... verschillende methoden, technieken of methodologieën die iets gemeen hebben: de positie waarin studenten worden geplaatst is proactief, in het beheer van de kracht van hun behoefte om te leren. Als de studenten in groepen werken, zoals in het echte leven, wordt het leren belangrijker. Bij deze strategieën wordt de protagoniek geplaatst in mensen met een leergierigheid en is de rol van de leraar/trainer die van een begeleider die bepaalde kennis heeft, maar ook wil leren met de studenten.

Kortom, The Flipped Classroom (FC) is een pedagogisch model dat het werk van bepaalde leerprocessen buiten het klaslokaal verplaatst en de tijd van de klassikale sessie investeert, samen met de ervaring van de leerkracht, om andere processen van verwerving en kennisbeoefening binnen het klaslokaal te bevorderen en te stimuleren.

Bij Probleemgestuurd Leren (PGO) legt de leerkracht een vraag of een probleem vast en moeten de leerlingen een antwoord geven of een manier vinden om het op te lossen door middel van een proces; zij, leerkracht en leerlingengroep, zetten samen een mijlpaal in het leerproces neer.

In Project Based Learning (Project BL) staan de studenten ook centraal in het leerproces, als een protagonist die in staat is om oplossingen te genereren in reactie op de verschillende mogelijkheden; het proces is nauw verbonden met de werkomgeving en het belangrijkste resultaat is een product. De module richt zich op deze strategieën door de meest relevante ideeën en essentiele factoren te laten zien en daagt de deelnemers uit om opnieuw na te denken over hun  eigen pedagogische praktijken.
## Context
Het doel van deze modules is het theoretisch kader en de sleutelfactoren van de 3 methoden/strategieën te begrijpen: Flipped Classroom, Problem Based Learning en Project Based Learning. 

De nadruk zal liggen op het leren ontwerpen of herontwerpen van de leerstrategie met behulp van open bronnen en tools en het creëren en evalueren van de activiteiten op basis van deze methoden. 

Leerresultaten:
1.	De basisprincipes van de Flipped Classroom (FC) strategie begrijpen en in staat zijn om deze te implementeren en een mind-map te creëren om deze uit te leggen aan de anderen.
2.	Ontwerp de structuur van een nieuwe omgedraaide lesmodule (of transformeer een klassieke module) om een onderwerp te onderwijzen/leren, met behulp van open bronnen en tools.
3.	De basisprincipes van de Problem Based Learning strategie begrijpen en deze kunnen uitleggen aan de studenten en collega's.
4.	Ontwerp een PBL-activiteit (of transformeer een reeds bestaande activiteit) die kan worden toegepast om het gebruik van enkele open softwaretools aan te leren.
5.	De basisprincipes van de Project Based Learning strategie begrijpen.
6.	Ontwerp een PBL activiteit (of transformeer een reeds bestaande activiteit) die kan worden toegepast om het belang van het promoten van FLOSS te leren.
## Sessies
De eerste sessie zal gebaseerd zijn op het begrijpen van de Flipped Classroom strategie. We zullen de onderdelen, de fasen en de fases bekijken. Ontwikkeling van middelen voor de Flipped Classroom met FLOSS-technologieën, -hulpmiddelen en -strategieën.

De tweede sessie zal gericht zijn op de Problem Based Learning methode. Stadia. Aanpak van relevante problemen voor studenten. Op te lossen brainstormproblemen. Open source middelen beschikbaar voor het oplossen van problemen. Gebruikelijke hulpmiddelen.

De derde sessie is gericht op het bespreken van de Project Based Learning strategie: componenten, stadia, fasen. Werken in reële situaties. De uitdaging als motor van het leren. Samenwerking en samenwerking tussen studenten. 

En de vierde en laatste sessie zal volledig gewijd zijn aan het persoonlijke project.