---
title: 'Projectmatig Leren'
objectives:
    - 'Om een inleiding te geven op Project Based Learning (PrBL)'
    - 'Het werk te vergemakkelijken met open leer/onderwijsmethoden'
    - 'Het ontdekken van nieuwe benaderingen van het leerproces en het stimuleren van de motivatie en betrokkenheid van de deelnemers. Het ontdekken van de voordelen van student-centered methoden'
    - 'Om studenten te motiveren hun onderwijsmethode te veranderen en om nieuwe uitdagingen aan te gaan tijdens het lesgeven'
---

## Introductie
Beschrijving van de activiteit
De trainer zal de belangrijkste kenmerken van deze aanpak presenteren: Project Based Learning is een methode waarbij de student centraal staat in het leerproces, als een protagonist die in staat is om oplossingen te genereren in antwoord op de verschillende mogelijkheden.

Een methodiek die nauw samenhangt met de werkomgeving, maar ook met het ondernemerschap. Het onderscheidt zich vooral door studenten aan te sporen om een breed scala aan kennis, capaciteiten, vaardigheden en attitudes in de praktijk te brengen.

Door gebruik te maken van een Project Based Learning methodologie...
* We moeten een (materieel of intellectueel) product krijgen. Dit is erg belangrijk.
* Samenwerking en samenwerking tussen studenten om dit doel te bereiken is een must.
* Bevordert initiatief, proactiviteit, onafhankelijkheid en innovatie op verschillende gebieden: professioneel, sociaal en persoonlijk.
* De uitdaging werkt als een motor (motivatie en vastberadenheid) om het doel te bereiken. Engagement is het belangrijkste kenmerk!

Bij deze aanpak gebeurt het dat: 
* De leerlingen genereren waarde buiten de klasomgeving. Naarmate het resultaat in de publiciteit komt, krijgen de leerlingen feedback uit de echte wereld.
* De motivatie neemt toe met het positieve effect op hun sociale context.
* Hun gevoel van eigenwaarde is ook verbeterd.
* Ze werken aan een reële situatie die deel uitmaakt of zou kunnen uitmaken van de professionele context.

Met deze methode: We kunnen de echte wereld er echt bij betrekken. We kunnen een echte impact hebben op onze sociale context, en leren heeft een zeer goede ondernemerschapsaanpak, niet alleen vanuit economisch (zakelijk) oogpunt, maar ook vanuit sociaal oogpunt. Studenten werden een referentie op het inhoudelijke vlak waar ze aan gewerkt hebben.

Verschillen tussen PBL en PrBL: 
* PBL begint met een gestructureerd probleem. PrBL begint met het bouwen van een product of een artefact in hun hoofd.
* PBL-problemen mogen niet in verband worden gebracht met de werkelijke context van de student. PrBL werkt altijd met reële mogelijkheden, want het resultaat moet bruikbaar zijn in de eigen context.
* PBL gebruikt vragen en oplossingen. PrBL gebruikt producten die aan het einde van het leerproces worden gepresenteerd.
* Succes in PBL wordt bereikt door het vinden van oplossingen voor het probleem. Succes in PrBL wordt bereikt door het creëren van oplossingen voor het probleem en deze te presenteren aan de gemeenschap.

Na de presentatie van de informatie zal de trainer de deelnemers vragen om te brainstormen over enkele mogelijkheden die aan hun leerlingen kunnen worden gepresenteerd (bijvoorbeeld om een lokale krant te maken, om een APP te maken...). De trainer maakt aantekeningen op het bord / de flip-over. 

De trainer kan de deelnemers een uitdagende vraag stellen: welk deel van de Technovation Girls studiecasus is "PBL" en welk deel is "PrBL"? Het is belangrijk voor de studenten om te leren dat "100% zuivere methode" niet altijd wordt ingezet en dat ze een deel van de methoden kunnen gebruiken en mengen, om zo een aantal verschillende resultaten te krijgen. De methoden zijn altijd suggesties, maar de trainer moet flexibel zijn.

## Ondersteuning: dia's & video & web
De trainer zal het PrBL-schema presenteren en uitleggen dat het erom gaat het proces te volgen: 
1.	Detectie van een mogelijk thema om aan te werken.
2.	Organisatie van de werkploegen (verschillende profielen, complementair)
3.	Definitief vastleggen van de uitdaging, de te bereiken oplossing.
4.	Voorbereiding van het plan.
5.	Opleiding en informatie onderzoek.
6.	Analyse en synthese. De studenten delen hun werk door ideeën uit te wisselen, oplossingen te bespreken, suggesties te doen, enz.
7.	Uitwerking van het product door toepassing van alles wat ze hebben geleerd.
8.	Presentatie van het product of project.
9.	Uitvoering van verbeteringen, indien nodig.
10.	Beoordeling en zelfevaluatie

## Ledereen opent zijn of haar laptop
Het doel van dit deel van de sessie is om de deelnemers een concreet voorstel te laten uitwerken dat aan de studenten kan worden voorgelegd. De deelnemers werken in groepjes en gebruiken een computer om een digitale presentatie te maken met hun voorstel. 
Zij: 
* Zullen nadenken over mogelijkheden die geschikt zijn om met deze methode te werken.
* Denken na over hoe de groep te organiseren.
* Een ontwerpplan opstellen.
* De kennis identificeren die moet worden onderzocht (onderzoek) en een aantal verschillende methoden voorstellen
* Denken aan verschillende soorten producten die kunnen worden gemaakt bij het werken met de studenten
* Stellen een aantal "presentaties" voor (openbare presentatie, video-opname, enz.)
* Denken na over hoe het werk te beoordelen en zelf te beoordelen.

Na 40' werken in kleine groepen zullen de groepen hun voorstel met de klas delen.
## Huiswerk

## Referenties