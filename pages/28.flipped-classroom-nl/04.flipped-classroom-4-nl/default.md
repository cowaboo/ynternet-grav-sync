---
title: '4e sessie'
objectives:
    - 'Om een inleiding te geven op'
    - 'Om te faciliteren'
    - 'Om te ontdekken'
    - 'Om te motiveren'
---

## Introductie
De trainer zal antwoorden op de vragen die zijn voortgekomen uit de vorige sessie en zal het huiswerk becommentarieren. Vervolgens zal hij de inhoud en de hoofdactiviteit van de sessie introduceren.
## Ledereen opent zijn of haar laptop
Het doel van deze sessie is de deelnemers te helpen bij het opzetten van hun eigen project, op basis van enkele van de methoden die tijdens de vorige sessies zijn gepresenteerd. 
De trainer maakt een samenvatting van de volledige gepresenteerde inhoud en methoden en nodigt de deelnemers uit om teams op te bouwen of te kiezen voor het individueel werken aan een trainingsplan. Ze moeten een doelgroep en een onderwerp selecteren en dan beginnen met het ontwerpen van hun eigen training. 
Ze moeten nadenken en beslissen: 
* Doelgroep.
* Doelen / doelstellingen / vaardigheden en competenties.
* Achtergrond.
* Kalender.
* Belangrijkste methode en strategie.
* Inleiding tot de opleiding.
* Beschrijving van de belangrijkste activiteiten en het opleidingsverloop. 

De deelnemers worden uitgenodigd om de nieuwe kennis die ze dankzij de Open-AE training hebben opgedaan, te integreren.

De trainer zal de personen/groepen uitnodigen om hun project te presenteren. Na de eerste presentatieronde wordt een peer-to-peer beoordeling georganiseerd. 
* Een van de mogelijkheden is om het in een klein formaat te doen: een groep ontmoet een andere groep (of een individu met een ander individu) en geeft commentaar, constructief, sterke en zwakke punten van het project, open software of hardware middelen die ze kunnen gebruiken en andere aanvullende details.
* De andere mogelijkheid is om het te doen met de groepsklas als brainstorm. Elk lid van de klas kan vragen sturen naar de persoon/personen die hun project hebben gepresenteerd, suggesties doen en bijdragen aan de peer-to-peer beoordeling.