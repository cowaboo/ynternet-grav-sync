---
title: 'Project Based Learning, Aprendizaje Basado en Proyectos'
objectives:
    - 'Proporcionar una Introducción. al aprendizaje basado en proyectos PrBL.'
    - 'Facilitar el trabajo con métodos de enseñanza : aprendizaje abiertos.'
    - 'Descubrir nuevos enfoques del proceso de enseñanza aprendizaje y estimular la motivación y el compromiso de los participantes. Descubrir las ventajas de los métodos de centros de estudiantes'
    - 'Motivar a los estudiantes a cambiar su modo de enseñanza y enfrentar nuevos desafíos mientras enseñan.'
length: '120 min'
---

## Introducción
TEl formador presentará las principales características de este enfoque: El Aprendizaje Basado en Proyectos es un método con el que el alumno está en el centro del proceso de aprendizaje, como protagonista capaz de generar soluciones en respuesta a las diferentes oportunidades.
Una metodología muy relacionada con el entorno laboral, y también con el emprendimiento. Destaca especialmente por instar a los estudiantes a poner en práctica un amplio abanico de conocimientos, habilidades, destrezas y actitudes.
Mediante el uso de una metodología de aprendizaje basado en proyectos ...
- Necesitamos conseguir un producto (material o intelectual). Esto es muy importante.
- La cooperación y colaboración entre los estudiantes para lograr este objetivo es imprescindible.
- Promueve la iniciativa, la proactividad, la independencia y la innovación en diferentes áreas: profesional, social y personal.
- El desafío actúa como motor (motivación y determinación) para lograr el objetivo. ¡El compromiso es la característica principal!

Al utilizar este enfoque, sucede que: 
- Los estudiantes generan valor más allá del entorno del aula. A medida que se lanza la publicidad, los estudiantes reciben comentarios del mundo real.
- La motivación aumenta con el efecto positivo en su contexto social.
- También se mejora su autoestima.
- Trabajan sobre situaciones reales que son, o podrían ser, parte del contexto profesional.


Con este método, podemos involucrar al mundo real. Podemos tener un impacto real en nuestro contexto social, y el aprendizaje tiene un muy buen enfoque de emprendimiento, no solo desde el punto de vista económico (empresarial), sino también desde una perspectiva social. Los estudiantes se convirtieron en una referencia en el área de contenido en la que han estado trabajando.

**Diferencias entre PBL y PrBL**: 
- PBL comienza con un problema estructurado. PrBL comienza construyendo un producto o un artefacto en sus mentes.
- Los problemas de ABP pueden no estar relacionados con el contexto real del estudiante. PrBL siempre trabaja con oportunidades de la vida real, ya que el resultado debe ser útil en su propio contexto.
- PBL utiliza preguntas y soluciones. PrBL utiliza productos que se presentarán al final del proceso de aprendizaje.
- El éxito en PBL se logra encontrando soluciones al problema. El éxito en PrBL se logra creando soluciones al problema y presentándolas a la comunidad.

Después de presentar la información, el formador pedirá a los participantes que intercambien ideas sobre algunas oportunidades que podrían presentarse a sus alumnos (por ejemplo, crear un periódico local, crear una aplicación…). El capacitador tomará notas en la pizarra / rotafolio.
El capacitador puede hacer una pregunta desafiante a los participantes: ¿qué parte del caso de estudio de Technovation Girls es “PBL” y qué parte es “PrBL”? Es importante que los estudiantes aprendan que el "método 100% puro" no siempre se implementa y que pueden tomar parte de los métodos y mezclarlos para obtener resultados diferentes. Los métodos son siempre sugerencias, pero el formador debe ser flexible.

## PrBL
El formador presentará el esquema PrBL y explicará que se trata de seguir el proceso: 

1. Detección de la oportunidad de trabajar.
2. Organización de los equipos de trabajo (diferentes perfiles, complementarios)
3. Definición final del desafío, la solución a alcanzar.
4. Elaboración del plan.
5. Investigación en formación e información.
6. Análisis y síntesis. Los estudiantes comparten su trabajo intercambiando ideas, discutiendo soluciones, haciendo sugerencias, etc.
7. Elaboración del producto aplicando todo lo aprendido.
8. Presentación del producto o proyecto.
9. Implementación de mejoras, si es necesario.
10. Evaluación y autoevaluación

## Todos abren su ordenador y... 
El objetivo de esta parte de la sesión es que los participantes construyan una propuesta concreta que se pueda presentar a los estudiantes. Los participantes trabajarán en grupos y utilizarán una computadora para crear una presentación digital con su propuesta, de manera que: 
 
- Pensarán en oportunidades que puedan ser adecuadas para trabajar con este método.
- Pensarán en cómo organizar el grupo
- Orepararán un plan preliminar
- Edentificarán el conocimiento que se va a explorar (investigación) y sugerir algunos métodos diferentes
- Pensarán en varios tipos de productos que podrían crearse al trabajar con los estudiantes.
- Sugerirán algunos modos de "presentaciones" para usar (presentación pública, grabación de video, etc.)
- Pensarán en cómo evaluar y autoevaluar el trabajo.


Después de los 40 'trabajando en pequeños grupos, los grupos compartirán su propuesta con la clase. 

## Trabajo en casa (autónomo)
Después de escuchar las diferentes propuestas, piense en un proyecto concreto que puedan desarrollar sus alumnos en su clase. Piense en los pros y los contra de usar este método en su clase.

## Referencias
Buck Institute for Education PBLWorks website (2020): [What is PBL?](https://www.pblworks.org/what-is-pbl) 

Phyllis C. Blumenfeld, Elliot Soloway, Ronald W. Marx, Joseph S. Krajcik, Mark Guzdial & Annemarie Palincsar (1991) [Motivating Project-Based Learning](https://www.tandfonline.com/doi/abs/10.1080/00461520.1991.9653139): Sustaining the Doing, Supporting the Learning, Educational Psychologist, 26:3-4, 369-398, DOI: 10.1080/00461520.1991.9653139 