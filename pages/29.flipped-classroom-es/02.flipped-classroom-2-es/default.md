---
title: 'Problem Based Learning, Aprendizaje basado en problemas.'
objectives:
    - 'Proporcionar una introducción al Aprendizaje Basado en Problemas.'
    - 'Facilitar el trabajo con métodos de enseñanza : aprendizaje abiertos.'
    - 'Descubrir nuevos enfoques del proceso de enseñanza, aprendizaje y estimular la motivación y el compromiso de los participantes'
    - 'Descubrir las ventajas de los métodos de centros de estudiantes.'
    - 'Motivar a los estudiantes a cambiar su modo de enseñanza.'
lenght: '120 min'
---

## Introducción
El formador hablará sobre las dudas que puedan surgir después de la sesión anterior y comentará el Trabajo en casa (autónomo). A continuación se presentarán los contenidos y las principales actividades de la sesión.

El capacitador presentará el concepto y el método de “aprendizaje basado en problemas”. El punto de partida será una "revisión de caso" y sugerimos utilizar el proyecto "Technovation Girls" (ver en https://technovationchallenge.org/), que es un proyecto que desafía a equipos de niñas a pensar en problemas locales y a codificar una solución (crean una aplicación). Es mejor encontrar un ejemplo local, por lo que sugerimos al capacitador que busque ejemplos locales en YouTube buscando "Technovation + nombre del país". Después de reproducir algunos de los videos disponibles, el capacitador resaltará algunos elementos de este (o similar) proyecto:
* Es un desafío
* Las participantes trabajan en grupos
* Trabajan para resolver un problema real, pero no es necesario que el problema esté presente en el contexto de los estudiantes (pueden trabajar en los problemas de otros)
* Aprenden lo que necesitan saber para poder dar una solución (que no es lo mismo para los diferentes grupos)
El capacitador explicará que hoy en día es muy fácil identificar problemas reales, utilizando los 17 objetivos para transformar nuestro mundo [the 17 goals to transform our world](https://www.un.org/sustainabledevelopment/sustainable-development-goals/):

![](sustainable.png)

**Problem based learning** ¿Aprendizaje basado en problemas? es un método que comienza con el formador/ora planteando una pregunta o un problema y desafiando a los estudiantes a dar una respuesta a la pregunta o encontrar la manera de resolver el problema. A veces, el maestro invita a los estudiantes a pensar en el problema. El ABP permite al profesor y al alumnado a establecer un hito en el proceso de aprendizaje, donde el alumno es el actor principal.

Al utilizar la metodología de aprendizaje basado en problemas, los estudiantes ...

- Aprenden a tomar decisiones, individualmente o negociando (con el grupo)
- Interactuan con compañeros y también con profesores
- Utilizan la información de su contexto cercano
- Buscan la información que necesitan
- Producen y comparten ideas
- Discuten otras posibles soluciones y también los mecanismos para encontrarlas.


Los problemas deben estar en relación con las habilidades de los estudiantes y deben ser significativos para ellos. Los problemas surgen del mundo real y pueden ser "reales" o "potenciales". 
Es mejor utilizar un escenarios reales para facilitar las cosas.

- El problema debe motivar a los estudiantes a buscar una comprensión más profunda de los conceptos.
- El problema debería requerir que los estudiantes tomen decisiones razonadas y las defiendan.
- El problema debe incorporar los objetivos del contenido de tal manera que se conecte con cursos / conocimientos previos.
- Si se usa para un proyecto grupal, el problema necesita un nivel de complejidad alto para garantizar que los estudiantes deban trabajar juntos para resolverlo.
- Si se usa para un proyecto de varias etapas, los pasos iniciales del problema deben ser abiertos y atractivos para atraer a los estudiantes al problema.

(Duch, Groh, and Allen, 2001)

![](gold.png)

[Fuente de la imagen](https://www.pblworks.org/blog/gold-standard-pbl-essential-project-design-elements)

Con este método, los estudiantes pueden mejorar / cambiar sus conocimientos, habilidades y destrezas y actitudes.
En este momento, el capacitador pedirá a los participantes que intercambien ideas sobre algunos problemas que podrían presentarse a sus alumnos. El capacitador tomará notas en la pizarra / rotafolio.

## EL esquema de trabajo PBL
El capacitador presentará el esquema PBL y explicará que se trata de seguir el proceso: el primer paso es el análisis del problema, luego los participantes buscan la información necesaria que necesitan para comprender el problema y aprenden las habilidades necesarias para comprender la información. Luego pueden integrar la información y aplicarla con cierta cantidad de conocimiento y sabiduría para resolver el problema.
Es importante comprender que cada alumno necesita aprender / adquirir habilidades diferentes, ya que su conocimiento previo es diferente.
Las etapas son:
1. Presentación del problema a resolver / identificar un problema que le interesa al grupo.
2. Análisis de los conocimientos previos
3. Identificación del conocimiento y los recursos necesarios para desarrollar una solución.
4. Lista de acciones o actividades que se deben realizar para encontrar una solución.
5. Elaboración del plan de trabajo
6. Hacer lo planeado: buscar la información necesaria, realizar acciones, comunicar los resultados parciales
7. Elaboración de la respuesta y argumentación sobre el método utilizado para encontrarla.


Como la formadora o formador ha presentado un ejemplo (Technovation girls one), el ejemplo se transformará en etapas, para que los participantes se den cuenta de lo fácil que es hacerlo.

## Todos abren su ordenador y... 
El objetivo de esta parte de la sesión es que los participantes construyan una propuesta concreta que se pueda presentar a los estudiantes. Los participantes trabajarán en grupos y utilizarán una computadora para crear una presentación digital con su propuesta.
Ellos y ellas: 
* Identificarán y presentarán un problema muy concreto, relacionado con los ODS, e identificarán un grupo potencial de estudiantes (edad, antecedentes, necesidades especiales ...)
* Identificarán algunos conocimientos / habilidades que sus estudiantes deberían tener para resolver el problema (entonces los estudiantes podrán identificar si tienen o necesitan aprender / adquirir parte de estos conocimientos / habilidades).
* Prepararán una lista inicial de actividades que se deben realizar. Por ejemplo: visitar un lugar, entrevistar a alguien, buscar en Internet, realizar una encuesta, hacer algunos cálculos con datos abiertos; programar una app o un videojuego, hacer una presentación, hacer una infografía ..
* Prepararán un plan de trabajo potencial (que será modificado por los alumnos cuando realicen la actividad)

Después de los 40 'trabajando en pequeños grupos, los grupos compartirán su propuesta con la clase.

## Trabajo en casa (autónomo):
Después de escuchar las distintas propuestas, piensa en un problema concreto a resolver con tus alumnos en tu clase. Piense en los pros y los contra de usar este método en su clase.

## Referencias
* Duch, B. J., Groh, S. E, & Allen, D. E. (Eds.). (2001). The power of problem-based learning. Sterling, VA: Stylus.
* Grasha, A. F. (1996). Teaching with style: A practical guide to enhancing learning by understanding teaching and learning styles. Pittsburgh: Alliance Publishers.
* The Center for Innovation in Teaching & Learning (CITL): [Problem-Based Learning (PBL)](https://citl.illinois.edu/citl-101/teaching-learning/resources/teaching-strategies/problem-based-learning-(pbl))
* [PBL through the Institute for Transforming Undergraduate Education at the University of Delaware](http://www1.udel.edu/inst/)
```