---
title: 'Aula invertida y aprendizaje basado en proyectos y problemas'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_id: 'Flipped classroom / project based-problem based learning (+ peer review)'
lang_title: es
length: '9 horas'
objectives:
    - 'Comprender el marco teórico y los factores clave de los 3 métodos / estrategias: aula invertida, aprendizaje basado en problemas y aprendizaje basado en proyectos'
    - 'Aprender a diseñar o rediseñar la estrategia de enseñanza-aprendizaje utilizando herramientas y fuentes abiertas'
    - 'Crear y evaluar actividades basadas en estos métodos'
materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
    - Altavoces
skills:
    'Palabras clave':
        subs:
            'Aula invertida (FC)': null
            'Aprendizaje basado en problemas (PBL)': null
            'Aprendizaje basado en proyectos (PrBL)': null
            Abierto: null
            Método: null
            'Aprendizaje continuo': null
            Voluntario: null
            Motivación: null
---

## Introducción
Aula invertida, aprendizaje basado en proyectos, aprendizaje basado en problemas ... son diferentes métodos, técnicas o metodologías que tienen algo en común: la posición en la que los estudiantes se colocan es proactiva, en la gestión del poder de su necesidad de aprender. Si los estudiantes trabajan en grupos, como en los escenarios de la vida real, el aprendizaje se vuelve más significativo. Con estas estrategias, el protagonismo se coloca en las personas con ganas de aprender y el papel de la persona que les forma es el de una compañera que tiene cierto conocimiento pero que también quiere aprender con los estudiantes.
En resumen, The Flipped Classroom (FC) o la **Clase Invertida** es un modelo pedagógico que mueve el trabajo de ciertos procesos de aprendizaje fuera del aula e invierte el tiempo de la sesión de clase, junto con la experiencia de la persona formadora, en promover e impulsar otros procesos de adquisición y práctica de conocimiento dentro del aula.

Con el **Aprendizaje Basado en Problemas** (ABP o, en inglés, PBL), la persona formadora formula una pregunta o un problema, y las personas que participan como aprendices tienen que dar una respuesta o encontrar una manera de resolverla a través de un proceso; establecen todos juntos un hito en el proceso de aprendizaje.

En **Aprendizaje Basado en Proyectos** (PrBL, Project Based Learning), las estudiantes también están en el centro del proceso de aprendizaje, como protagonistas capaces de generar soluciones en respuesta a las diferentes oportunidades; el proceso está estrechamente relacionado con el entorno de trabajo y el resultado principal es un producto.
El módulo se centrará en estas estrategias, mostrando las ideas más relevantes y los factores clave, y desafiará a los participantes a repensar sus prácticas pedagógicas. 
## Contexto
El objetivo de este módulo es comprender el marco teórico y los factores clave de los 3 métodos / estrategias: aula invertida, aprendizaje basado en problemas y aprendizaje basado en proyectos.

Nos centraremos en aprender a diseñar o rediseñar la estrategia de enseñanza-aprendizaje utilizando herramientas y fuentes abiertas, y en crear y evaluar las actividades basadas en estos métodos.
Los resultados del aprendizaje serán la adquisición de las capacidades para: 
* Aprender los conceptos básicos de la estrategia Flipped Classroom (FC) y ser capaz de implementarla y crear un mapa mental para explicarlo a los demás.
* Redactar la estructura de un nuevo módulo de aula invertida (o transformar un módulo clásico) para enseñar / aprender un tema, utilizando herramientas y fuentes abiertas.
* Comprender los conceptos básicos de la estrategia de Aprendizaje basado en problemas y sea capaz de explicarlos a los estudiantes y colegas.
* Diseñar una actividad de PBL (o transforme una que ya exista) que podría aplicarse para enseñar el uso de algunas herramientas de software abiertas.
* Comprender los conceptos básicos de la estrategia de aprendizaje basado en proyectos.
* Diseñar una actividad del Proyecto BL (o transforme una que ya exista) que podría aplicarse para enseñar la importancia de promover FLOSS.
## Sesiones
### Primera sesión: Basada en la comprensión de la estrategia Flipped Classroom
Revisaremos los componentes, las etapas y las fases. Desarrollo de recursos para el aula invertida con tecnologías, herramientas y estrategias de FLOSS.
### Segunda sesión: Centrada en el método de aprendizaje basado en problemas
Etapas. Enfoque de problemas relevantes para los estudiantes. Lluvia de ideas de problemas por resolver. Recursos de código abierto disponibles para la resolución de problemas. Herramientas habituales.
### Tercera sesión: Tendrá como objetivo hablar sobre la estrategia de aprendizaje basado en proyectos: componentes, etapas, fases
Trabajando en situaciones reales. El desafío como motor de aprendizaje. Colaboración y cooperación entre estudiantes.
### Cuarta sesión: Totalmente dedicada al proyecto personal o grupal
