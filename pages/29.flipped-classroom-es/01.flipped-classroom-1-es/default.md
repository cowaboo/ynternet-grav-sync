---
title: 'Flipped Classroom'
objectives:
    - 'proporcionar una introducción a Flipped Classroom y los principales conceptos involucrados.'
    - 'facilitar la transformación de una formación en un aula invertida.'
    - 'descubrir la taxonomía de Bloom y las diferencias entre las habilidades de pensamiento de bajo nivel y las habilidades pensamiento de alto nivel, así como las diferentes categorías de habilidades, conocimiento, comprensión, aplicación, análisis, síntesis y evaluación.'
    - 'motivar a los estudiantes a cambiar su modo de enseñanza.'
lenght: '150 min'
---

## Introduccióó
El objetivo de esta primera parte es resaltar los conocimientos previos del grupo. También se invita a los participantes a compartir información sobre el contexto de su participación en la sesión (motivación, objetivos personales, etc.)
En resumen, “[The Flipped Classroom (FC)](http://www.theflippedclassroom.es/what-is-innovacion-educativa/) es un modelo pedagógico que desplaza el trabajo de ciertos procesos de aprendizaje fuera del aula e invierte el tiempo de clase, junto con la experiencia del docente, para promover e impulsar otros procesos de adquisición y práctica del conocimiento dentro del aula "". El formador utiliza los recursos y herramientas digitales disponibles para proporcionar a los alumnos un avance de los contenidos necesarios, que revisarán por su cuenta antes de asistir a la clase.

Una vez que los alumnos están en el aula, en contacto con sus compañeros y docentes, invierten su tiempo en trabajar con las cuestiones que no pudieron resolver por sí mismos en la etapa anterior. El apoyo entre pares es esencial en este método.

Al usar un modelo de Flipped Classroom…
* Los estudiantes aprenden contenido nuevo en línea viendo conferencias en video, generalmente en casa → actividades de comprensión y conocimiento.
*  lo que solía ser Trabajo en casa (deberes, problemas asignados) ahora se hace en clase con el profesor que ofrece orientación e interacción más personalizadas con los estudiantes.  → Aplicación - Análisis - Síntesis y Evaluación: actividades.

Comparació:

![](traditional.jpg)
![](flipped.jpg)

## Debate en grupo
En grupos pequeños, los participantes trabajarán en la taxonomía de Bloom y pensarán en varias actividades diferentes que podrían desarrollarse en casa / en clase, de acuerdo con el modelo Flipped Classroom. Después de 5 'de reflexión individual, el capacitador iniciará una lluvia de ideas y tomará notas sobre la sugerencias de los participantes.
Finalmente, el formador mostrará / distribuirá la taxonomía detallada de Bloom y explicará cómo funciona:
![](taxonomy.png)

## Todos abren su ordenador y... 
Los participantes (individualmente o en grupo) cogerán el documento editable con la taxonomía y agregarán una nueva fila, con el fin de agregar las herramientas digitales, software u otros recursos digitales que puedan ser útiles para lograr las “acciones” y los “resultados” ( tercera fila del documento). El formador hará enfasis en el uso de FLOSS y tecnologías abiertas y libres.
Los participantes también pensarán qué parte de la taxonomía se puede desarrollar “en casa” o “en clase” y tratarán de sugerir (en abstracto) algunas actividades que podrían realizarse durante una formación específica (usar un ejemplo concreto puede ser muy útil)
e.g.: 
1. en casa: 
* Relacionados con el conocimiento (lectura integral, cumplimentación de cuestionarios, creación de un glosario, etc.)
* Relacionado con la comprensión: creación de una infografía, un resumen, una publicación, una presentación digital ...

2. En clase, con compañeros y profesores:
* Relacionados con la aplicación del aprendizaje: una entrevista a un experto, un escenario virtual, un cronograma con la secuencia de un plan de acción ...
* Relacionados con el análisis: mapa conceptual, cuadro comparativo ...
* Relacionados con la evaluación: discusión, debate, pruebas o experimentos reales o simulados, etc.
* Relacionado con la creación: vídeo, postcast ...

Después de los 45 ', los participantes compartirán sus hallazgos con el grupo y comentarán su experiencia.
## Trabajo en casa (autónomo)
Piensa en una formación que hayas impartido antes y las actividades que propusiste a tus alumnos: si aplicaras el modelo Flipped Classroom, ¿qué parte se desarrollaría en casa y qué parte se haría en clase?

## Referencias: 

BLOOM, B.S. (ed.) (1956) Taxonomy of Educational Objectives: The Classification of Educational Goals. Nueva
York: David McKay Company, pag 201-207.

Sprouts (2015): The Flipped Classroom Model. Video available at https://youtu.be/qdKzSq_t8k8

Acer for education (2019): [6 benefits of the Flipped Classroom model](https://acerforeducation.acer.com/education-trends/6-benefits-of-the-flipped-classroom-model/?gclid=CjwKCAjwn9v7BRBqEiwAbq1Ey-QlosaH2KbYwmzqgRIODBYIjQNkNwZeqtGFepWzXz-0sIGSzT8VcxoC3tAQAvD_BwE). 