---
title: 'Desarrollo personal o grupal de un proyecto de aprendizaje / enseñanza'
objectives:
    - 'El objetivo de esta sesión es facilitar a los participantes la creación de su propio proyecto, basado en algunos de los métodos presentados en las sesiones anteriores.'
lenght: '140 min'
---

## Introducción
El formador hablará sobre las dudas que puedan surgir tras la sesión anterior comentará sobre Trabajo en casa (autónomo). Después de eso se presentará el contenido y las principales actividades de la sesión.

El objetivo de esta sesión es facilitar a los y las participantes la creación de su propio proyecto, basado en algunos de los métodos presentados en las sesiones anteriores.
El formador hará un resumen de todo el contenido y los métodos que se han presentado e invitará a los participantes a formar equipos o elegir trabajar individualmente en un plan de formación. Deben seleccionar un grupo objetivo y un tema y luego comenzar a diseñar su propia capacitación.

Deben pensar y decidir sobre:

- El grupo objetivo o grupo diana
- Metas / objetivos / habilidades y competencias
- Antecedentes
- Calendario
- Método y estrategia principal.
- Introducción. a la formación
- Descripción de las principales actividades y secuencia de formación.


Se invita a los participantes a integrar los nuevos conocimientos adquiridos gracias a la formación Open-AE.

## Todos abren su ordenador y... 
El capacitador invitará a las personas / grupos a presentar su proyecto. Después de la primera ronda de presentación, se organizará una evaluación entre pares.
- Una de las posibilidades es hacerlo en un formato pequeño: un grupo se reúne con otro grupo (o un individuo con otro individuo) y comenta, de manera constructiva, fortalezas y debilidades del proyecto, recursos de software o hardware abiertos que pueden utilizar y otros. detalles complementarios.
- La otra posibilidad es hacerlo con la clase grupal como una lluvia de ideas. Cualquier miembro de la clase puede enviar preguntas a la persona o personas que han presentado su proyecto, hacer sugerencias y contribuir a la evaluación entre pares.

## Conclusiones
Esta es la parte final de este módulo (y de la formación, si es posible)
Revisión de lo aprendido y de los compromisos individuales adquiridos, si los hubiere, y foto grupal final. ¡Buena suerte y hasta pronto!

# Trabajo en casa (autónomo)
Sigue trabajando en tu proyecto y hazlo realidad en tu clase. Valora el resultado en base al grado de satisfacción de los participantes y los resultados académicos obtenidos. No olvides que los resultados de las metodologías activas no se evalúan con los exámenes tradicionales: tendrás que trabajar con los portafolios de los alumnos y valorar en qué medida son capaces de aplicar los nuevos conocimientos y habilidades adquiridos.

#References
Yasemin Gülbahar & Hasan Tinmaz (2006) Implementing Project-Based Learning And E-Portfolio Assessment In an Undergraduate Course, Journal of Research on Technology in Education, 38:3, 309-327, DOI: 10.1080/15391523.2006.10782462

Brigid J.S. Barron, Daniel L. Schwartz, Nancy J. Vye, Allison Moore, Anthony Petrosino, Linda Zech & John D. Bransford (1998) Doing With Understanding: Lessons From Research on Problem- and Project-Based Learning, Journal of the Learning Sciences, 7:3-4, 271-311, DOI: 10.1080/10508406.1998.9672056 