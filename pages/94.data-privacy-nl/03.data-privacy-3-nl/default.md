---
title: 'Breng uw praktijk (en die van uw organisatie) in kaart om de bescherming van gegevens te verbeteren'
objectives:
    - 'Een kritische analyse mogelijk te maken van het belang van gegevensbescherming in onze praktijken'
    - 'De grenzen van onze praktijken op het gebied van onderwijs en digitale inclusie te onderzoeken'
    - 'Deelnemers aan te moedigen, en hun organisatie, om actief en creatief deel te nemen via FLOSS-technologieën, zowel in theorie als in de praktijk'
    - 'Deelnemers motiveren om in hun organisatie een charter aan te nemen dat het gegevensbeschermingsbeleid specificeert'
---

## Wie zijn wij?: Lokaliseer en beperk onze praktijken
### **Heeft u te maken met een van deze situaties met uw begunstigden?** (Zo ja, hoe beperkt u uw tussenkomst?)
* Betalen via een app?
* Een wachtwoord herstellen?
* Herstel van een hardwareprobleem?
* Registreren op een sociaal netwerk?
* Leren omgaan met een tekstverwerker?
* Stuur een e-mail naar de gemeente?
* Installeer een app?
* Op zoek naar accommodatie?
* Een vereenvoudigde belastingaangifte invullen op “Tax-on-Web”?
* Toegang geven tot e-gezondheidsinformatie?

**Tussentijdse conclusie**:
Wordt u tijdens uw werk geconfronteerd met andere delicate situaties voor gegevensbescherming?
Zo ja, welke en hoe beperkt u uw tussenkomst?
### Bijeenkomst per organisatie om ons gebruik te lokaliseren
De activiteit zal zich concentreren op het definiëren van het project van onze eigen organisatie, om de praktijk en de doelstellingen ervan te identificeren. Om het beleid van de organisatie inzake de bescherming van persoonsgegevens op lange termijn aan te passen. 
In een subgroep (of individueel indien niet mogelijk): breng de werknemers van dezelfde organisatie samen. Studie van de praktijkcasus van de eigen organisatie.
(als voorbeeld van een reactie, hieronder de reacties voor het ARC-project "informaticien public")
a/
### 1.- Strategie
Wat is het doel en het publiek van het systeem? 
(voorbeeld: mensen ondersteunen die een diepe digitale kloof ervaren door ze te ontmoeten)
Wat is het standpunt van de organisatie over vertrouwelijkheid?
(voorbeeld: de openbare IT reageert op alle verzoeken voor dagelijks gebruik; de ervaring zal positief zijn (de gebruiker vertrekt niet met lege handen)).
### 2.- wie verwerkt persoonsgegevens en hoe?
Welke werknemer of vrijwilliger begeleidt de gebruiker?
(voorbeeld: een ervaren facilitator uit de digitale openbare ruimte (vol vertrouwen en verantwoordelijkheid, maar geen dokter, geen advocaat, geen technicus).
Status van de animator, hoe de vraag van de gebruiker benaderen?
(voorbeeld: Verzoek geformaliseerd door de gebruiker; het proces uitleggen (wat gaan we doen en wat we zien) om een gebruikersovereenkomst te zoeken).
### 3.- Het organisatiekader
Wat is de aard van de organisatie? 
(voorbeeld: een vereniging voor levenslang leren met een digitale openbare ruimte)
Waarom het publiek komt?
(voorbeeld: De gebruiker voelt zich hulpeloos tegenover IT, hij weet (of wil) geen training volgen, hij heeft concrete IT-behoeften en vragen)
### Oefeningen per organisatie: 
a / Beschrijf jezelf in 1, 2 en 3
b / Classificeer verzoeken (4) volgens a / in "Ik behandel" of "Ik behandel niet".
-Op basis van de bovenstaande lijst: vermeld het type verzoek dat is verwerkt en het type verzoek dat niet is verwerkt (voeg voorbeelden toe als vereist).
## Iedereen opent zijn computer
Beschrijving van de activiteit Pooling en collectieve afsluiting:
Heeft u, in het kader van uw werk voor digitale inclusie, baat bij een kader dat uw interventies beperkt en beveiligt?
Bronnen: Gegevensbescherming op school in 7 stappen.pdf 
https://www.jedecide.be/sites/default/files/2018-06/La%20protection%20des%20donnees%20a%20lecole%20en%207%20etapes.pdf
https://www.privacyinonderwijs.be/In7stappen.pdf

**Hoe verwerkt u als opleidingscentrum persoonsgegevens?**
De belangrijkste principes waaraan een leercentrum moet voldoen bij het verwerken van persoonsgegevens zijn de volgende:
* Verwerk persoonsgegevens voor specifieke en legitieme doeleinden. Gebruik persoonlijke gegevens alleen voor dit doel. Voorbeeld: om redenen van studentenadministratie kent een school het woonadres van alle studenten. Het is niet omdat een school over de gegevens beschikt dat ze naar een andere school kunnen worden verzonden of dat de school deze kan gebruiken om een lijst met adressen onder de ouders te verspreiden.
* Wees transparant bij het verwerken van persoonlijke gegevens. Leg uit waarom de school bepaalde persoonsgegevens verwerkt.
* Elke verwerking van persoonsgegevens is alleen legitiem als deze voldoet aan ten minste een van de wettelijke grondslagen.

De belangrijkste juridische grondslagen waarop een school kan vertrouwen zijn:

**Wettelijke verplichting**: indien wettelijk vereist kunnen persoonsgegevens worden verwerkt. Dit zijn bijvoorbeeld administratieve gegevens en studentenondersteuning.

**Het contract**: de persoonsgegevens van studenten en docenten kunnen worden verwerkt als dit nodig is voor de uitvoering van een contract. Bijvoorbeeld: een identiteitsbewijs met foto van een student dat wordt aangevraagd en die op een studentenkaart staat om hem toegang te geven tot allerlei diensten die door de school worden aangeboden.

**Toestemming**: de toestemming van leerlingen of ouders van leerlingen onder de 16 jaar is nodig voor de verwerking van persoonsgegevens voor bepaalde doeleinden. Bijvoorbeeld: om foto's van leerlingen op de website van de school te publiceren, is toestemming vereist.
* Een school verwerkt niet meer persoonsgegevens dan nodig is om het vastgestelde en gerechtvaardigde doel te bereiken. Bijvoorbeeld: bij het inschrijven van een leerling mag de school het inkomen van de ouders niet kennen.
* Persoonlijke gegevens die door een school worden verwerkt, moeten nauwkeurig zijn en kunnen worden gecorrigeerd. Bijvoorbeeld: als een leerling verhuist, kan de school het adres wijzigen.
* Bewaar persoonlijke gegevens niet langer dan nodig. Voor bepaalde studentgegevens geldt een wettelijke bewaartermijn. Respecteer de wettelijke bewaartermijn voor persoonsgegevens.
* Als school, passende maatregelen nemen om persoonsgegevens te beschermen tegen ongeoorloofde verwerking. De school autoriteit is verantwoordelijk voor het respecteren van deze principes en moet dit kunnen aantonen.

Individuele oefening: identificeer in de bovenstaande lijst met welke elementen uw organisatie al rekening heeft gehouden. Identificeer de punten waaraan nog moet worden gewerkt.
## Huiswerk
Wij nodigen u uit om binnen uw organisatie de implementatie van het Algemeen Reglement voor de Bescherming van de Persoonlijke Levenssfeer te bevorderen.

De bovenstaande PDF biedt een 7-stappen methodologie om educatieve organisaties in staat te stellen de nodige procedures op te zetten. Voor deze opdracht vragen we u niet om de 7 stappen zelf uit te voeren, maar om te proberen uw organisatie bij dit proces te betrekken, door uw managers en collega's op te leiden. En door een manier van doen voor te stellen op de agenda van je werkvergaderingen.

Bronnen: Gegevensbescherming op school in 7 stappen.pdf van het College Bescherming Persoonsgegevens (APD) https://www.privacyinonderwijs.be/In7stappen.pdf
Hoe je het moet doen:
STAP 1 - Informeren en sensibiliseren
STAP 2 - Aanwijzing van een DPO en een contactpunt op school
STAP 3 - Een register bijhouden van de verwerkingsactiviteiten
STAP 4 - Contracten met partners 
STAP 5 - Controleren of toestemming nodig is
STAP 6 - Fysieke beveiliging en beveiliging van de ICT-infrastructuur, Aandachtspunten
STAP 7 - Overtredingen van persoonsgegevens en meldingsplicht
## Debriefing
Ter afsluiting van de sessie zal de trainer een moment van debriefing faciliteren waarbij de deelnemers worden aangemoedigd om hun vragen, twijfels, ideeën en gevoelens te uiten. Denk aan de behandelde onderwerpen. 
Wilt u meer weten over de GDPR? Het College voor Gegevensbescherming heeft een groot portaal ontworpen met een thematisch dossier over de GDPR. U kunt het algemene plan ook gefaseerd raadplegen: "GDPR - Bereid je voor in 13 stappen!" https://www.gegevensbeschermingsautoriteit.be/burger
Beoordeling:  De deelnemers zullen een korte vragenlijst beantwoorden om hun leerresultaten te beoordelen.Totale duur van de sessie: ongeveer 2 uur + 1 uur huiswerk.