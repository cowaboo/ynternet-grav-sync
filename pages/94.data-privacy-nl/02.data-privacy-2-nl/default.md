---
title: 'Kan FREE software mij helpen mijn privacy beter te beschermen?'
objectives:
    - 'Licht werpen op het verband tussen FLOSS en gegevensbescherming'
    - 'De strategie van FLOSS-initiatieven begrijpen'
    - 'Over enkele beschikbare FLOSS-tools vliegen (framasoft en uBlock Origin)'
    - 'Goed opgeleide mensen motiveren tot een praktijk met respect voor privacy en vrije wil'
---

## Introductie
De trainer begint de introductie van de module door vragen te stellen aan de deelnemers over de FLOSS-initiatieven om hen heen. Reacties worden gedocumenteerd en vervolgens gebruikt om het trainingsmateriaal te personaliseren.
## Gratis software en gegevensbescherming
**Inleiding**: FLOSS: geschiedenis van een cultuur van delen en gegevensbescherming.
De overgrote meerderheid van de verdedigers van de vrije cultuur komt ook op voor het [recht op privacy](https://en.wikipedia.org/wiki/Privacy), free [toegang tot informatie](https://en.wikipedia.org/wiki/Knowledge_commons) en vrijheid van meningsuiting op internet. Sommige activisten hebben tijdens hun strijd ook illegale handelingen kunnen plegen, zoals inbreuk op het auteursrecht ([The Pirate Bay](https://en.wikipedia.org/wiki/The_Pirate_Bay), [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz)) of de openbaarmaking van vertrouwelijke gegevens die zij van algemeen belang achten ([Edward Snowden](https://en.wikipedia.org/wiki/Edward_Snowden), [Alexandra Elbakyan](https://en.wikipedia.org/wiki/Alexandra_Elbakyan)). 

Rond 1960 werden de termen hacken  geïntroduceerd door [MIT](https://en.wikipedia.org/wiki/Massachusetts_Institute_of_Technology). Ze verwijzen naar hacken en experimenteren voor de lol. In 1969 [John Draper](https://en.wikipedia.org/wiki/John_Draper) slaagde erin om gratis interlokale gesprekken te voeren toen hij in de hoorn floot met een fluitje dat dezelfde toon had als het Amerikaanse telefoonnetwerk. Deze techniek is genoemd, door de maker [phreaking](https://en.wikipedia.org/wiki/Phreaking) en zal een nieuwe golf van inspireren [hackers](https://en.wikipedia.org/wiki/Hacker) computer. Ze zullen proberen een eerste computer aan te passen en te ontwikkelen.

Pas in 1980 begonnen de media artikelen over hacken te publiceren. In het bijzonder met [Kevin Poulsen](https://en.wikipedia.org/wiki/Kevin_Poulsen), die erin slaagt in te breken in een netwerk dat gereserveerd is voor leger, universiteiten en bedrijven. Er was ook de release van de film _[Wargames](https://en.wikipedia.org/wiki/WarGames)_, van het verhaal draait om een hacker die toegang weet te krijgen tot het computersysteem van het Amerikaanse leger. De eerste [computervirus](https://en.wikipedia.org/wiki/Computer_virus) verscheen ook in deze jaren. Hierdoor worden hackers soms gezien als gevaarlijke mensen.

Veel hackers begonnen hun activiteit door te proberen de antikopieerbeperkingen te doorbreken of door de regels van de computerspellen af te leiden vóór de veralgemening van internet, dat vervolgens een bredere horizon voor hun activiteit opende. Maar toen de media aan het begin van [de jaren negentig](https://en.wikipedia.org/wiki/1990s) dat de [Chaos Computer Club Frankrijk](https://en.wikipedia.org/wiki/Chaos_Computer_Club#Fake_Chaos_Computer_Club_France) was een valse groep hackers die samenwerkte met de [gendarmerie](https://en.wikipedia.org/wiki/National_Gendarmerie), de gemeenschap van Franse hackers wendde zich liever tot free [software](https://fr.wikipedia.org/wiki/Logiciel_libre) en er zijn veel gemeenschappen ontstaan.

Het was met de geboorte van de [internet](https://fr.wikipedia.org/wiki/Internet), dat we in de jaren negentig voor het eerst over spraken [cybercriminaliteit](https://en.wikipedia.org/wiki/Cybercrime). De volgers zijn in het begin verdeeld. Er zijn [zwarte hoeden](https://en.wikipedia.org/wiki/Black_hat_(computer_security)) die criminele activiteiten ontplooien en [witte hoeden](https://en.wikipedia.org/wiki/White_hat_(computer_security)) die geen kwaad willen doen, maar zoeken [computer kwetsbaarheden](https://en.wikipedia.org/wiki/Vulnerability_(computing)) om ze openbaar te maken en zo te repareren.

Een van de sterke punten van hacken is het **community-aspect**. De gemeenschapsorganisatie maakt de uitbreiding van het delen van informatie mogelijk, de gemeenschappen die met elkaar verbonden zijn, de verspreiding van informatie is erg snel. Communautaire organisatie maakt wederzijdse hulp mogelijk tussen mensen, maar ook aan mensen van jonge leeftijd die willen leren. De onderlinge verbinding van mensen die elkaar niet kennen, maakt hulp mogelijk die individuen op hetzelfde niveau plaatst, en dit zonder waardeoordeel. **_Dit aspect dringt aan op veralgemening en kennisdeling zonder dat dit gebeurt op basis van criteria als "functie, leeftijd, nationaliteit of diploma's"_**.

Nicolas Auray verklaart deze anonimiteit als volgt: "Door sporen in een" anonieme "modus af te leveren,[de hackersweigerden] om voor politiek-gerechtelijke instellingen te verschijnen, waarbij ze de legitimiteit van hun vonnis aanvechten. Ze zouden verwerpen wat is nog steeds een beetje bij civiele "disobedients" aanvaard: om de legitimiteit van de straf te herkennen en om zichzelf om gestraft te worden"21.

**Mogelijk conclusie**
De gemeenschap van ‘hackers’ is gekoppeld aan de oprichting en ontwikkeling van de FLOSS-cultuur. Prioriteit wordt gegeven aan het delen van bronnen en informatie binnen praktijkgemeenschappen. Maar via "directe actie" -methoden om de gebreken in het systeem zelf te verhelpen, en door maximale bescherming van persoonlijke gegevens bij de bron te garanderen in het "ontwerp" van de tools .
## De bijzonderheid van FLOSS-tools voor gegevensbescherming:
Vrije software heeft verschillende voordelen:
* Een geweldige aanpassing van vrije software aan alle soorten hardware configuraties en specifieke gebruiksvoorwaarden "op aanvraag".
* Permanente schaalbaarheid dankzij een dynamische community van gepassioneerde professionele developers.

Maar voor iedereen, of het nu onder LicenseGnuPublicLicensev3 (GPLv3), FreeSoftware (FS) of CeCiLL valt; dit geeft gebruikers de vrijheid om de software te draaien, kopiëren, verspreiden, bestuderen, wijzigen en verbeteren door toegang tot de broncode.
**Het maakt ook een grotere vertrouwelijkheid van onze gegevens mogelijk:**
enerzijds, aan de ontwikkeling kant, zorgt de ethiek die wordt gevolgd door de ontwikkelaars gemeenschap ervoor dat de code geen functionaliteiten bevat die bedoeld zijn om gegevens door te geven via tussenliggende marketing verwerking servers. Aan de andere kant zorgen de beveiligingsmaatregelen die op servers en netwerken zijn geconfigureerd, voor de distributie van IP-pakketten naar de juiste ontvanger.
![](datapriv1.jpg)
Gegevensbescherming en free software: behoud van de privésfeer met Framasoft

Delen en communicatiediensten worden steeds belangrijker om in contact te blijven met onze dierbaren, in een samenleving waarin de familie en de sociale sfeer steeds meer gedelokaliseerd worden. Met gratis en goed geconfigureerde services behoudt u de controle over onze beurzen.
Om om te gaan met deze alomtegenwoordige verwerking van onze persoonlijke gegevens, **implementeren toegewijde digitale spelers “free” en vaak "gratis" alternatieven** voor het aanbieden van internetdiensten aan iedereen.
De **gemeenschap [Framasoft](https://framasoft.org/) biedt downloads van applicaties die gemakkelijk te installeren zijn dankzij de meegeleverde documentatie**.
Het biedt gratis alternatieve diensten, waaronder:
* Framadrive om opslagruimte, agenda en agenda in de cloud te hebben.
* Framasphère, een server voor toegang tot het sociale netwerk (bijvoorbeeld [Diaspora](https://diasporafoundation.org/)).
* Framaestro om uitwisselingsdiensten te bieden en voor het delen van bronnen.
* Framabee, zoals een alternatief voor de Google-zoekmachine.
* Framawiki, voor het schrijven van documentatie met behulp van SGML (gestructureerde documentatie is gebouwd op gestructureerde elementen: hoofdstukken, secties, alinea's, enzovoort, waarbij alle elementen duidelijk zijn gelabeld voor wat ze zijn: referenties, programma-uitvoer, enz. maakt automatische verwerking van de documenten mogelijk, zonder te wachten op AI-systemen. Het moedigt auteurs aan om zich te concentreren op structuur, wat betekenis overbrengt.)

### Onze aanbevelingen voor veilig gebruik van internet
![](darapriv2.png)

* Lees voor de zekerheid vertrouwelijkheidscontracten bij het gebruik van Google, Yahoo, Microsoft van de privé-informatie die u op de economische markt wilt blootleggen.
* Toegang krijgen tot internetservices die alleen gecodeerde verbindingen via SSL / TLS gebruiken.
* Gebruik beveiligde berichtenservices voor het verzenden van e-mail (vermijd SMTP).
* Gebruik een besturingssysteem dat ongevoelig is voor malware, ransomware en virussen.
* Stel een bescherming in met een firewall, onafhankelijk van het besturingssysteem zoals bijvoorbeeld UFW, om ongewenste services te vermijden.
* Controleer altijd de downloadbron van bestanden met behulp van coderingssleutels, GPG, SHA256 ...
* Gebruik een onafhankelijke browser zoals Firefox van de Mozilla Foundation.
* Maak geavanceerde wachtwoorden en gebruik een wachtwoordbeheerder (KDE Wallet, KeePass).
* Neem zoveel mogelijk deel aan deze gratis alternatieven om hun ontwikkeling op lange termijn voort te zetten.

bron: https://blog.syloe.com/protection-des-donnees-et-logiciel-libres-nos-recommandations/
## Homework
Installation of uBlock Origin

Praktische oefeningen
* **Waarom uzelf beschermen tegen reclame?** Adverteren op internet wordt gebruikt om te [bijhouden](https://en.wikipedia.org/wiki/HTTP_cookie#Tracking) de gebruiker van site naar site gebruikt [cookies](https://en.wikipedia.org/wiki/HTTP_cookie) van sites van derden, dit vanwege de aard van het bedrijfsmodel van internetreclame, waarvoor de vergoeding in ruil is voor de persoonlijke gegevens van de gegevens gebruiker.
* Voor gebruikers, blokkeren [reclame](https://en.wikipedia.org/wiki/Advertising) helpt afleiding of zelfs cognitieve overbelasting te voorkomen als gevolg van ongewenste inhoud op de webpagina's die ze bezoeken. Het maakt ook een snellere en duidelijkere weergave van webpagina's mogelijk en dus vrij van advertenties, een lager verbruik van bronnen ([processor](https://en.wikipedia.org/wiki/Central_processing_unit), [RAM](https://en.wikipedia.org/wiki/Random-access_memory) en [bandbreedte](https://en.wikipedia.org/wiki/Bandwidth_(signal_processing))) en een zekere bescherming van [privacy](https://en.wikipedia.org/wiki/Privacy) door volgsystemen uit te schakelen, [digitale voetafdruk](https://en.wikipedia.org/wiki/Digital_footprint) en analyse geïmplementeerd door [reclamebureaus](https://en.wikipedia.org/wiki/Advertising_network).
* Gebruikers van mobiele [telefoons](https://en.wikipedia.org/wiki/Mobile_telephony) gegevens (met behulp van [GPRS](https://en.wikipedia.org/wiki/General_Packet_Radio_Service), [3G](https://en.wikipedia.org/wiki/3G), [4G-](https://en.wikipedia.org/wiki/4G) en [5G-](https://en.wikipedia.org/wiki/5G) standaarden), evenals gebruikers van het vaste internet in bepaalde landen, die betalen voor de hoeveelheid uitgewisselde gegevens of een beperking daarvan hebben, een financieel belang hebben bij het blokkeren van advertenties [online](https://en.wikipedia.org/wiki/Online_advertising), onder meer door video- en audiobestanden te blokkeren, grote verbruikers van bandbreedte.

**uBlock Origin**
![](datapriv3.png)
## References
* https://blog.syloe.com/protection-des-donnees-et-logiciel-libres-nos-recommandations/
* https://en.wikipedia.org/wiki/UBlock_Origin
* https://en.wikipedia.org/wiki/Free-culture_movement
* https://en.wikipedia.org/wiki/Hacker