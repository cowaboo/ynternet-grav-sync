---
title: 'Data privacy culture: a FLOSS driven view NL'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Data privacy culture: a FLOSS driven view'
lang_id: 'Data privacy culture: a FLOSS driven view'
lang_title: nl
length: '6 uren'
objectives:
    - 'Het wettelijk kader te presenteren en het bewustzijn te vergroten van de kwesties met betrekking tot de bescherming van gebruiksgegevens'
    - 'Bewustwording van de toegevoegde waarde van free software om de bescherming van persoonlijke gebruikersgegevens te vergroten'
    - 'Sta de facilitator toe om hun gebruik en de mate van respect voor persoonlijke gegevens in hun organisatie te lokaliseren'
materials:
    - 'Personal computer (smartphone of tablet verbonden)'
    - Internet
    - Bearmer
    - 'Papier en pennen'
    - Flipchart
    - 'Benodigde Mobiele verbinding Slidewiki Academie'
skills:
    Keywords:
        subs:
            'Bescherming van persoonsgegevens': null
            'GDPR (General Data Protection Regulation)': null
            'Data privacy': null
            'Big data': null
            Surveillance: null
            Advertenties: null
            Manipulatie: null
            Beïnvloedingsgedrag: null
            FLOSS: null
            Free: null
            'Open source': null
---

## Introduction
Hoewel het gebruik van persoonsgegevens voor commerciële doeleinden wijdverbreid is, wordt respect voor privacy door Europa als een fundamenteel recht beschouwd. Vrije software probeert keer op keer zich te verzetten tegen het "zogenaamd vrijwillige" toezicht op ons privéleven. Om de privacy van gegevens direct bij de bron te beschermen, probeert de FLOSS-cultuur bij het ontwerp van de tool niets te verbergen in de software die ons controleert (en dat bewijst dit door toegang te geven tot de broncode). Dit trainings scenario moedigt praktijken aan die zich bewust zijn van de uitdagingen die verband houden met gegevensverzameling, door **fundamentele goede praktijken en Europese wetgeving te presenteren. Ons doel is om leerlingen uit te nodigen om de mogelijkheid te overwegen om free of open source software te gebruiken om de veiligheid van hun persoonlijke gegevens te vergroten. We willen ook de STEM-trainer toelaten om de grenzen van zijn professionele praktijk beter te situeren om respect voor de persoonsgegevens van hun begunstigden aan te moedigen**. Over het geheel genomen herinnert het trainings scenario aan het Europese wetgevingskader en aan de uitdagingen met betrekking tot het profileren van gewoonten door het monitoren van persoonlijk gebruik met het oog op manipulatie van reclame gedrag.
## Context
Het doel van de sessie is om de leerlingen op een bewuste en praktische manier te betrekken om:
* Het wettelijk kader te presenteren en het bewustzijn te vergroten van de kwesties met betrekking tot de bescherming van gebruiksgegevens. De leerling zal zichzelf juridisch kunnen situeren in relatie tot de GDPR (General Data Protection Regulation). Hij zal de algemene problemen en goede gewoonten met betrekking tot de bescherming van persoonsgegevens begrijpen.
* Bewustwording van de toegevoegde waarde van free software om de bescherming van persoonlijke gebruikersgegevens te vergroten. Om bewust te beslissen welke platforms / tools / diensten het meest respectvol zijn voor de privacy en vrijheid van gebruikers.
* Sta de facilitator toe om hun gebruik en de mate van respect voor persoonlijke gegevens in hun organisatie te lokaliseren. Om hem aan te moedigen een weloverwogen keuze te maken uit regels en instrumenten waarvan het "vrije" ontwerp meer vertrouwelijkheid mogelijk maakt.

De sessies zullen het gebruik van free software in niet-formeel volwassenenonderwijs aanmoedigen door opzettelijke deelname aan de FLOSS-cultuur te stimuleren. De deelnemers zullen een kritische analyse maken van de mogelijkheden om FLOSS-tools in hun dagelijks leven te gebruiken. Door zich bewust te worden van het belang van een gegevensbeschermingsbeleid in de eigen organisatie; om hun professionele praktijken op het gebied van digitale inclusie beter te beveiligen.
## Sessies
Deze eendaagse module (7u + 3u individueel werk) wordt verdeeld in 3 sessies.
### Eerste sessie: Basisprincipes van gegevensbescherming
Deze sessie geeft de deelnemers de kans om de belangrijkste concepten van wetgeving en de problemen te verkennen. We zullen de concepten die tijdens de training aan bod komen, koppelen aan free educatieve bronnen (via URL-links). We bespreken de basisprincipes en goede gewoonten om te hebben.
* Fase 1:   Iedereen opent zijn laptop. We kijken samen op internet naar twee korte introductiefilmpjes over de GDPR (General Data Protection Regulation) met eenvoudige visuele informatie die voor iedereen (jong en volwassen) toegankelijk is.
* Fase 2:  We werken in een subgroep aan de specifieke onderwerpen die voor de leerlingen interessant zijn, alle onderwerpen van de aangeboden leermiddelen mogen niet worden behandeld. Het idee hier is om het bewustzijn van de uitdagingen te vergroten en goede, eenvoudige en toegankelijke praktijken voor te stellen. Het is de basis van basis gewoonten om de bescherming van uw gegevens te vergroten. De inhoud is geldig voor jongeren en volwassenen, en geeft ook een inleiding tot de risico's verbonden aan sociale netwerken.
* Fase 3:  Groepsdiscussie ter afsluiting van sessie 1. De trainer begint de collectieve conclusie van de module door de deelnemers te vragen naar hun huidige en eerdere ervaringen met gegevensbescherming. Reacties worden gedocumenteerd en vervolgens gebruikt om het trainingsmateriaal te personaliseren.

### Tweede sessie: kan gratis software mij helpen mijn privacy beter te beschermen?
De tweede sessie is bedoeld om de historische oorsprong en de stand van zaken vast te stellen van vrije software rond gegevensbescherming. FLOSS-praktijken zullen kort worden gepresenteerd (via het voorbeeld van de framasoft- en UBlock Origin-tools). Denk als collectief leren samen na over de keuze van tools om de bescherming van gegevens bij onze activiteiten te vergroten. Experimenteren met een tool (UBlock Origin adblocker).
### Derde sessie: Lokaliseer en beperk uw praktijk (en die van uw organisatie) om de gegevensbescherming te verbeteren 
Deelnemers zullen een kritische analyse maken van de gebruiksmogelijkheden in hun dagelijks leven, en zullen een eerste analyse van hun eigen organisatie ontwikkelen voor naleving van wettelijke richtlijnen in hun digitale inclusie praktijken.
Deze drie sessies maken een kritische analyse van het wettelijk kader en de problematiek mogelijk. Hij zal doorgaan met het onderzoeken van het gebruik en de voordelen van vrij digitale technologieën in niet-formeel onderwijs. Door bewustwording te bevorderen die de actieve en creatieve betrokkenheid van leerlingen via FLOSS-technologieën mogelijk maakt, zowel als theorie als praktijk.