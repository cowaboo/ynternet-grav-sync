---
title: 'Basisprincipes van gegevensbescherming'
objectives:
    - 'Link naar de training van open educatieve bronnen (OER) https://www.jedecide.be/'
    - 'Ontdek de ethische, juridische, sociale, economische en impact argumenten; voor en tegen de exploitatie van persoonsgegevens voor commerciële of politieke manipulatie'
    - 'Wat zijn de basisregels en goede gewoonten voor henzelf en hun gemeenschap'
---

## Introductie
Beschrijving van de activiteit: in deze sessie komen we terug op het wettelijk kader en de belangrijkste kwesties voor gegevens; om de goede praktijken te kunnen identificeren die van toepassing zijn op ons dagelijks online gebruik.  
Cursus Volgorde: de trainer start de introductie door het module programma te presenteren en door de deelnemers te vragen naar hun huidige en eerdere ervaringen op het gebied van bescherming van persoonsgegevens. Reacties worden gedocumenteerd en vervolgens gebruikt om het trainingsmateriaal te personaliseren.
## Fase 1
Beschrijving van de activiteit: Iedereen opent zijn computer draagbare of indien mogelijk groepsscreening. We bekijken samen op internet twee korte introductiefilms met eenvoudige visuele informatie die voor iedereen (jong en volwassen) toegankelijk is.
Door de leerlingen te vragen om individueel (schriftelijk om hen niet te vergeten) de informatie op te sommen waarvan zij denken dat die belangrijk of nieuw voor hen is.
* https://www.1jour1actu.com/info-animee/cest-quoi-la-protection-des-donnees-personnelles/ (1min42)
* https://www.jedecide.be/les-parents-et-lseignement (3min47)

Engelse variaties:
* https://www.priv.gc.ca/media/3609/gn_e.pdf
* https://www.schooleducationgateway.eu/en/pub/resources/tutorials/brief-gdpr-guide-for-schools.htm

Pooling per ronde tafel, met op tafel de belangrijke informatie voor de deelnemers. Organiseer de informatie visueel op een visueel medium door de informatie op thema te groeperen.
Fase 1 samenvatting: De rol van de gegevensbeschermingsautoriteit, die voortvloeit uit de hervorming van de Privacycommissie, werd aan hen uitgelegd.
### Wat is de Algemene Verordening Gegevensbescherming (AVG)?
De Algemene Verordening Gegevensbescherming (AVG) is een nieuwe Europese verordening die regels vastlegt waaraan overheden, bedrijven en alle andere organisaties zich moeten houden bij het verwerken van persoonsgegevens.
De AVG biedt burgers betere bescherming bij het verwerken van hun persoonsgegevens. Deze nieuwe wetgeving is gebaseerd op bestaande privacywetgeving maar versterkt een aantal regels, verduidelijkt of vormt een uitbreiding op bestaande wetgeving. De nieuwe regeling is op 25 mei 2018 in werking getreden.

De opleidingscentra verwerken veel persoonsgegevens.
Voorbeelden zijn gegevens van (oud-) leerlingen en hun ouders, docenten en begeleiders. Als onderdeel van de verwerking van persoonsgegevens vallen Digital Public Spaces (EPN) en trainingscentra onder de AVG.

Wat zijn de belangrijkste veranderingen?
* Meer verantwoordelijkheid van de gegevensverwerker. Het opleidingscentrum moet zelf aantonen dat het persoonsgegevens verwerkt volgens de regels van de AVG.
* De opleidingscentra moeten een gesprekspartner aanwijzen die deze wetgeving kent en die kan helpen bij de implementatie ervan binnen de school.
* De organiserende autoriteit moet een register van behandelactiviteiten bijhouden. In een register van verwerkingsactiviteiten is onder meer opgenomen welke persoonsgegevens door de school (of het leercentrum) worden verwerkt, voor welke doeleinden deze worden gebruikt, waar ze vandaan komen en met wie ze worden gedeeld.
* De wetgeving voorziet in een meldingsplicht bij datalekken. Zo ben je als school (of leercentrum) bij het uitlekken van gevoelige persoonsgegevens verplicht het datalek te melden aan de Autoriteit Persoonsgegevens (en eventueel aan de betrokken personen).

Versterkte controle. Bij niet-naleving van de AVG kan de Gegevensbeschermingsautoriteit ook sancties opleggen.
## Fase 2
Beschrijving van de activiteit: We werken in een subgroep rond de specifieke thema's die de leerders interesseren, al het materiaal van de hieronder voorgestelde leermiddelen mag niet worden behandeld. Het idee hier is om het bewustzijn van de uitdagingen te vergroten en goede, eenvoudige en toegankelijke praktijken voor te stellen. We vliegen over de goede elementaire gewoonten om de bescherming van onze gegevens te vergroten. De inhoud is geldig voor jongeren en volwassenen, en geeft ook een inleiding tot de risico's verbonden aan sociale netwerken.

Oefeningen: We stellen voor dat de groep op basis van hun interesses, elk voor zichzelf,kiest 
een van de 8 thema's die op het educatieve platform worden aangeboden:
* https://www.jedecide.be/les-parents-et-lseignement 

We laten mensen (15 min) de inhoud van de voorgestelde thema's individueel online ontdekken. We groeperen de mensen die hetzelfde thema hebben gekozen in subgroepen, zodat ze samen een beknopte presentatie van belangrijke praktische informatie voorbereiden. Elke groep presenteert aan de anderen de belangrijkste informatie van het gekozen thema. De subgroepen presenteren de inhoud zoals ze willen (nodig ze uit om creatief te zijn in de vorm van delen). Vraag hen om de rollen in de subgroep te identificeren, elk moet een rol hebben tijdens de presentatie (bijvoorbeeld: x verslaggever, x schrijver, x tijdwaarnemer, x visuele ondersteuning, x opname, x presentator, ...) rol, het is aan de groep om te kiezen en om iedereen een rol op het podium te geven tijdens de presentatie (mini theaterstijl max 3 min). Het is natuurlijk ook een moment van delen om andere good practices te ontdekken dan die gepresenteerd op het onderwijsplatform, het doel is om een collectieve intelligentie van de thema's te genereren, en deze met anderen te delen.

8 voorgestelde thema's:
### 1.- [Verbonden speelgoed](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys)
**Bijvoorbeeld**:
* Een verbonden teddybeer of pop die al onze geheimen kent.
* Een drone die onze buren op de foto neemt.
* Een verbonden horloge dat al onze bewegingen registreert.

De vakantie komt snel dichterbij en de periode van winkelen is in volle gang. Hebben uw kinderen u hun cadeaulijst gestuurd? Tijd om te winkelen is gekomen! Auto's met afstandsbediening, avonturenboeken, kleurrijk slijm,… je weet niet meer waar je moet afslaan. En plotseling sta je oog in oog met dit state-of-the-art high-tech speelgoed, "connected toys" genoemd. Dit op internet aangesloten speelgoed is zeer aantrekkelijk en zeer begeerd door kinderen, maar weet u echt hoe het werkt, hoe uw persoonlijke gegevens worden verwerkt, evenals die van uw kinderen, hoe u het moet gebruiken of hoe u het moet configureren om het te beveiligen? In ons themadossier vindt u wat "intelligent" advies over het gebruik ervan.
* [Drones](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/drones).
* [Connected speakers](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/enceintes-connectees).
* [Kijken naar Connected](https://www.jedecide.be/les-parents-et-lenseignement/jouets-connectes/montre-connectee).
* [Robot and doll](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/robot-et-poupee-connectes).
* [Tablet](https://www.jedecide.be/les-parents-et-lenseignement/connected-toys/tablettes).

### 2.- [Online](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne)
**Privacy online**: het Internet is overal, het is "het beste". Speel, luister naar muziek, deel foto's en video's, chat of zoek informatie op voor school, ... Dit alles doen jongeren online. Het is dan ook niet voor niets dat ze worden genoemd digitale generatie.
Maar ondanks het feit dat jongeren opgroeien met internet, zijn er enkele aspecten die ook voor hen niet minder complex zijn. Ze zien dus niet altijd de risico's van internet, zeker niet op het gebied van privacy! Als u uw "digitale leven" niet voldoende beschermt, loopt u het risico dat andere mensen, bedrijven of zelfs overheidsinstanties toegang krijgen tot veel persoonlijke informatie. Wanneer dit gebeurt, wordt internet ineens veel minder "top". 
In dit themablok vind je tips om jouw privacy en die van onze jongeren op internet te beschermen.
* [Een omgeving met respect voor de privacy: hoe?](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/un-environnement-respectueux-de-la-vie-privee)
* [Bescherm uw profiel](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/protegez-votre-profil).
* [Gebruik sterke wachtwoorden](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/utilisez-des-mots-de-passe-forts)!
* [Denk na voordat je iets plaatst](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/reflechissez-avant-de-poster).
* [Beveilig je internetverbinding](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/securisez-votre-connexion-internet).
* [Sociale netwerken: de voordelen](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/reseaux-sociaux-les-avantages).
* [Sociale netwerken: de nadelen](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/les-reseaux-sociaux-les-inconvenients).
* [Cyberpesten](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-en-ligne/cyber-harcelement).

### 3.- [Privacy op school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole)
**Privacy op school**: het recht op privacy stopt niet bij de schoolpoort! De begeleiders moeten ook verschillende regels naleven om de privacy van elke student te respecteren. Zowel ouders als leerkrachten hebben hier veel vragen over: welke informatie kan onze organisatie opvragen? Hoe lang kan ze ze bewaren? Wat doet school met de foto's van mijn kind? Mogen de leerlingen de les filmen en vice versa, kan de docent de klas filmen?

Dit thematische blok biedt een antwoord op deze en vele andere vragen over het privéleven van de leerling.
* [Dossier van de leerling](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/le-dossier-de-leleve).
* [Foto's en video's op school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/photos-et-videos-lecole).
* [Camera's op school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/les-cameras-lecole).
* [GSM en smartphone op school](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/gsm-et-smartphone-lecole).
* [Alcohol en drugs](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/alcool-et-drogue).
* [Nieuwe gadgets](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/nouveaux-gadgets).
* [Adres van de leerkracht](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/ladresse-de-lenseignant).
* [Mijn leraar, mijn Facebook-vriend](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/mon-prof-mon-ami-facebook)?
* [Bijeenkomst van voormalige](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/reunion-danciens).
* [Werving van nieuwe studenten](https://www.jedecide.be/les-parents-et-lenseignement/la-vie-privee-lecole/recrutement-de-nouveaux-eleves).

### 4.- [Foto's en video's ](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos)
**Foto's en video's**: het gebruik van allerlei soorten beeldvormen evolueert razendsnel. Tegenwoordig hebben veel mensen een camera op zak ... hun smartphone! Niets is nu zo triviaal als het maken van foto's en filmen en de verspreiding van deze beelden is nog nooit zo eenvoudig geweest: sociale netwerken en applicaties zijn erop gericht informatie en afbeeldingen te delen. Deze snelle ontwikkeling verraste iedereen en veranderde ingrijpend onze manier van leven en onze samenleving. Vroeger verliep communicatie voornamelijk via woorden. Tegenwoordig worden we overweldigd door een stortvloed aan beelden op van alles en nog wat: onszelf, onze kinderen, ons gezin, onze sportclub, onze school, onze klas.

Maar vormt deze ontwikkeling geen gevaar voor onze privacy? Kunnen we weigeren gefilmd te worden, kunnen we de verspreiding van afbeeldingen waarop we verschijnen verbieden of worden we gedwongen om de trend van de online verspreiding van afbeeldingen te volgen?
* [Het principe: altijd om toestemming vragen](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/le-principe-demandez-toujours-le-consentement).
* [Hoe toestemming verkrijgen](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/comment-obtenir-le-consentement)?
* [Afbeeldingen van schoolactiviteiten](https://www.jedecide.be/les-parents-et-lenseignement/photos-et-videos/images-dactivites-scolaires).

### 5.- [Sexting](https://www.jedecide.be/les-parents-et-lenseignement/sexting)
**Sexting**: sexting is het verzenden van berichten van seksuele aard via sms, via sociale media of telefoondiensten zoals Skype. Deze berichten kunnen dus tekst bevatten maar ook een foto of video van seksuele aard. Dit soort seksuele boodschap wordt simpelweg een "sext" genoemd.

De sexting is op zichzelf niet zo ernstig. Jongeren experimenteren graag, ook op seksueel vlak is dit het geval. Maar ze moeten zich ervan bewust zijn dat sexting ook privacyrisico's met zich meebrengt. Dit is waar het waarschijnlijk uit de hand loopt, en niet alleen onder jongeren ... ook volwassenen zitten in de val. In dit thematische blok vind je meer informatie over sexting. Wat is dat ? Wat te doen bij problemen? Enkele handige tips.
* [Sexting: wat zijn de risico's voor privacy](https://www.jedecide.be/les-parents-et-lenseignement/sexting/sexting-quels-sont-les-risques-pour-la-vie-privee)?
* [Is sexting strafbaar](https://www.jedecide.be/les-parents-et-lenseignement/sexting/le-sexting-est-il-punissable)?
* [Sophie's verhaal](https://www.jedecide.be/les-parents-et-lenseignement/sexting/lhistoire-de-sophie).
* [Tips voor leerkrachten](https://www.jedecide.be/les-parents-et-lenseignement/sexting/astuces-pour-les-enseignants).
* [Tips voor ouders](https://www.jedecide.be/les-parents-et-lenseignement/sexting/astuces-pour-les-parents).
* [Help, mijn blote foto is geplaatst](https://www.jedecide.be/les-jeunes/sexting/au-secours-ma-photo-denudee-ete-diffusee) !!!
* [Ik heb een naakte foto ontvangen, wat moet ik doen](https://www.jedecide.be/les-jeunes/sexting/jai-recu-une-photo-denudee-que-faire)?
* [Tips om problemen te voorkomen](https://www.jedecide.be/les-jeunes/sexting/astuces-pour-eviter-des-problemes).

### 6.- [Sharenting](https://www.jedecide.be/les-parents-et-lenseignement/sharenting)
**Sharenting**: informatie voor jonge ouders en "verbonden" grootouders. Sharenting: zelfs als u dit woord nog niet kent, is de praktijk die het aanduidt waarschijnlijk niet. Je doet het waarschijnlijk heel vaak of misschien maar af en toe. En meer dan één doet het zelfs zonder enige beperking!

Share + Parenting = Sharenting

Sharenting is het delen van foto's en video's van uw kinderen of kleinkinderen op sociale media, vaak zonder de toestemming van de betrokkenen. We hebben bijna allemaal een account op een of ander sociaal netwerk, of het nu Facebook, Instagram, Snapchat, WhatsApp is, ... En elke ouder houdt ervan om de belangrijke gebeurtenissen in het leven van hun kroost te vereeuwigen. Geen dag vakantie, geen schoolfeest, geen sportwedstrijd met onze blonde hoofden gebeurt zonder in beeld te blijven. Afbeeldingen die vervolgens vrolijk worden gedeeld op sociale netwerken.
* [Denk na voordat je iets plaatst](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/reflechissez-avant-de-poster)!
* [Waarom wel, waarom niet](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/pourquoi-oui-pourquoi-non)?
* [Tips voor verstandig delen](https://www.jedecide.be/les-parents-et-lenseignement/sharenting/conseils-pour-pratiquer-un-sharenting-avise).

### 7.- [Smartphones & applicaties](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications)
**Smartphones & applicaties**: Tegenwoordig hebben veel jonge mensen een smartphone of tablet. Sms'en, video's maken, selfies maken, apps testen, naar Facebook gaan, spelen, surfen op internet… het is allemaal mogelijk en heel gemakkelijk. Maar soms is het moeilijk om alle nieuwe vorderingen te volgen of te begrijpen.

Jongeren gebruiken deze technologieën dagelijks, ook op school. Dit themablok biedt ouders en leerkrachten dus basisinformatie over het gebruik van smartphones en applicaties en over de gevolgen voor het respecteren van privacy. U vindt er ook tips over het gebruik van uw smartphone en tablet terwijl u uw privacy beschermt. Op deze manier kunt u jongeren informeren en helpen die met vragen of problemen over dit onderwerp worden geconfronteerd.
* [Toepassingen en persoonlijke informatie](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/applications-et-informations-personnelles).
* [Kan ik als ouder de smartphone / tablet van mijn kinderen bedienen?](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/en-tant-que-parent-puis-je-controler-le)
* [Positie delen](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/partage-de-la-position).
* [Gezondheidsapps](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/les-applis-sante).
* [Om uw privacy verder te beschermen, uw berichten te versleutelen](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/pour-proteger-davantage-votre-vie-privee).
* [Virussen en tips](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/virus-et-astuces).
* [Privacy](https://www.jedecide.be/les-parents-et-lenseignement/smartphones-applications/astuces-de-protection-de-la-vie-privee-pour).

### 8.- [eID](https://www.jedecide.be/les-parents-et-lenseignement/eid)
**eID-toepassingen**: Vanaf 12 jaar hebben jongeren een elektronische identiteitskaart (eID)! Maar het is niet alleen een identiteitskaart! Tegenwoordig wordt de kaart voor heel wat dingen gebruikt: als toegangsbadge, garantiecheque, ... en zelfs als klantenkaart. Er zijn echter regels die u moet volgen! Lees hier waarom.
* [De elektronische identiteitskaart](https://www.jedecide.be/les-parents-et-lenseignement/eid/la-carte-didentite-electronique).
* [Wanneer moet de eID getoond worden](https://www.jedecide.be/les-parents-et-lenseignement/eid/quand-faut-il-montrer-leid)?
* [De eID als loyaliteitskaart](https://www.jedecide.be/les-parents-et-lenseignement/eid/leid-comme-carte-de-fidelite).

### Extra leermiddelen Engels
* https://termly.io/resources/articles/gdpr-for-dummies/
* https://www.sysaid.com/blog/entry/how-to-explain-gdpr-to-a-5-jarige

### Extra leermiddelen Frans/ Nederlands:
* https://www.123digit.be/
* [Ontdek veiligheid op internet](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet).
* [Animatie Frame om je workshop voor te bereiden](https://www.123digit.be/fr/accompagner/competences-numeriques-1#collapsedecouvrir-securite-internet).
* [Interactieve training afzonderlijk of gezamenlijk](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet-fiche).
* [Samenvattingsblad om aan de leerling te geven](https://www.123digit.be/fr/ressources-pedagogiques/decouvrir-la-securite-sur-internet-1).
* [Hoe maak je een veilig wachtwoord aan](https://www.123digit.be/fr/accompagner/competences-numeriques-1#collapsecreer-mot-passe-securise)?
* [Animatie Kader om uw workshop voor te bereiden](https://www.123digit.be/fr/ressources-pedagogiques/comment-creer-un-mot-de-passe-securise).
* [Interactieve training afzonderlijk of gezamenlijk](https://www.123digit.be/fr/ressources-pedagogiques/comment-creer-un-mot-de-passe-securise-1).
* [Samenvattingsblad om de leerling](https://www.123digit.be/fr/ressources-pedagogiques/comment-creer-un-mot-de-passe-securise-fiche).
* [Educatieve ondersteuning te geven](https://www.jedecide.be/les-parents-et-lenseignement/support-pedagogique) (www.jedecide.be).

## Fase 3
Ter afsluiting van sessie 1 : (Trainer) - De trainer begint de collectieve afsluiting van de module door de deelnemers te vragen naar hun huidige en eerdere ervaringen in gegevensbescherming. Reacties worden gedocumenteerd en vervolgens gebruikt om het trainingsmateriaal te personaliseren.
Mogelijke conclusies:

[Denk na voordat u publiceert: zet niet alles en nog wat online](https://www.jedecide.be/les-jeunes/la-vie-privee-en-ligne/reflechis-avant-de-publier)
### Het probleem: informatie die op internet wordt gepubliceerd, is vaak moeilijk te verwijderen
Als er eenmaal informatie op internet is gevonden (zoals uw mening, een foto, een video, enz.), is vaak erg moeilijk om ze te verwijderen. Om uw privacy online te beschermen, is het het beste om hiervan op de hoogte te zijn.

**Een voorbeeld**: een foto die u op internet heeft geplaatst, wordt geüpload door een vriend. Er staat nu dus een kopie van de foto op de harde schijf van zijn computer. Stel nu dat je vriend deze foto naar een andere vriend stuurt, die hem op zijn beurt doorgeeft aan andere mensen, ... er zijn ineens veel kopieën van je foto.

Dit is hoe u de controle over uw eigen persoonlijke informatie verliest. Hoe kan ik alle kopieën van de foto verwijderen? U weet niet waar de foto is opgeslagen. In een dergelijke situatie is het niet langer zinvol om de originele foto van internet te verwijderen. Hiermee kunt u de kopieën die zijn opgeslagen op de apparaten van iedereen die de foto heeft gedownload of ontvangen, niet verwijderen.

Het is daarom belangrijk om uit te leggen dat er een verschil is tussen wat er mondeling wordt gezegd tussen vrienden en wat men op internet publiceert.
### De oplossing: denk na voordat je iets post - stel jezelf 3 vragen
Stel jezelf de volgende voordat je iets op internet:
#### 1.- Is de informatie die ik online zet voor iedereen bedoeld?
Als de informatie niet voor iedereen bedoeld is, is het beter om deze niet op internet te publiceren waar ze voor iedereen zichtbaar is. Gebruik de privacy-instellingen van de website om het aantal mensen dat uw bericht kan zien te beperken, of plaats de informatie gewoon niet op internet.
#### 2.- Zal ik er later geen spijt van krijgen dat ik deze informatie online heb gezet?
Een slechte reactie, een humoristische foto, ...? Houd er rekening mee dat als u eenmaal informatie op internet heeft gepost, het wellicht niet meer mogelijk is om deze te verwijderen. Wees voorzichtig tijdens het chatten. Chatberichten kunnen worden opgeslagen door de ontvanger of door het bedrijf dat de chatapplicatie heeft ontworpen.
#### 3.- Is de informatie die ik op internet wil plaatsen persoonlijke informatie over iemand anders?
Zet nooit persoonlijke informatie zoals foto's, video's, een adres, een telefoonnummer, ... van andere mensen online zonder vooraf hun toestemming te vragen. Als u deze informatie op internet publiceert, verliest het de controle erover. Je zou waarschijnlijk ook niet willen dat het jou overkomt. Bovendien is het zelfs bij wet verboden om zonder toestemming persoonlijke informatie van een andere persoon online te delen. Vraag dus altijd eerst toestemming!

**Iemand heeft iets gepost zonder mijn toestemming, wat kan ik doen?**
* Vraag de persoon in kwestie om de informatie van internet te verwijderen omdat deze zonder uw toestemming is gepubliceerd.
* Vraag de beheerder van de website waarop de informatie is geplaatst om deze te verwijderen op grond van het feit dat deze zonder uw toestemming is gepubliceerd.
* Neem contact op met uw lokale gegevensbeschermingsautoriteit.

## Huiswerk
### Intro
Wist je dat veel mensen geen privacy-instellingen gebruiken op sociale media? Het is echter essentieel. Informatie die op internet wordt gepubliceerd, bereikt veel meer mensen dan offline wordt gedeeld. Maak geen gebruik van de privacy instellingen, hierdoor loopt u het risico dat onbekende mensen aan de andere kant van de wereld toegang krijgen tot bijvoorbeeld de foto's van uw persoonlijke leven.

Controleer alstublieft uw beveiligingsinstellingen op uw tools.
Vermeld ook:
* Wat zijn de voordelen van sociale media voor u?
* Wat zijn voor jou de nadelen van social media?

Mogelijk antwoord op de vraag: **waarom zijn sociale netwerken zo aantrekkelijk?**
De aantrekkingskracht van het hebben van een profielpagina op een sociaal netwerk is dat het een gemakkelijke manier is om in contact te komen met vrienden, kennissen en zelfs grotere groepen mensen. Sociale netwerken stellen je ook in staat om de acties van andere mensen met een simpele muisklik te volgen dankzij de overvloed aan informatie die deze sociale netwerken naar je sturen, via foto's, video's en statusupdates. ,…

Een ander kenmerk dat sociale netwerken zo aantrekkelijk maakt, is dat ze een virtueel podium bieden waar je jezelf in de schijnwerpers kunt plaatsen, met de hele wereld als potentieel publiek. Wilt u een sportdienst laten zien? Een kleine update van uw status of een simpele tweet is voldoende: "Ren gewoon 15 km in 1u30". Nog steeds in de euforie van je briljante optreden, kijk je uit naar de felicitaties die naar aanleiding van je bericht worden gepost. "Well, there are still a lot of my friends who are impressed by my performance!".
### De voordelen
Als we kijken naar de impact van sociale media op jongeren, kunnen we stellen dat het gebruik van sociale netwerken en sociale media over het algemeen zeker enkele voordelen biedt.
Sociale netwerken hebben veel psychosociale voordelen omdat ze voor jongeren een eenvoudige manier zijn:
* Om in contact te komen met vrienden en familie.
* Om hun sociaal kapitaal te ontwikkelen (= de reeks voordelen die men ontleent aan relaties en interacties met anderen).

Nabespreking: ter afsluiting van de sessie zal de trainer een moment van debriefing faciliteren waar deelnemers worden aangemoedigd om hun vragen, hun twijfels, hun ideeën en gevoelens over de besproken onderwerpen te uiten.
## Referenties
* https://www.ikbeslis.be/ouders-leerkrachten