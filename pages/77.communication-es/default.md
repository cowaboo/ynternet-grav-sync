---
title: 'Comunicación interpersonal intencional con FLOSS'
body_classes: 'title-center title-h1h2'
lang_menu_title: 'Intentional interpersonal communication with FLOSS'
lang_id: 'Intentional interpersonal communication with FLOSS'
lang_title: es
objectives:
    - 'Buenas prácticas de comunicación interpersonal intenciona'
    - 'Cómo la cultura del FLOSS se relaciona con las reglas de las redes sociales y los usos comunes de la información'
    - 'Casos de comunicación intencional en línea como la comunidad de Wikipedia'
materials:
    - 'Ordenador personal (teléfono inteligente o tableta conectada a Internet)'
    - 'Conexión a internet'
    - Proyector
    - 'Papel y boligrafos'
    - Rotafolio
    - 'Academia Slidewiki'
skills:
    'Palabras clave':
        subs:
            'Movimiento FLOSS': null
            'Cultura del FLOSS': null
            'La ciudadanía': null
            'Comunicación intencional': null
            'Empoderamiento cívico': null
            'Participación de la comunidad': null
            'Herramientas y habilidades del FLOSS': null
length: '10 horas'
---

## Introducción
La tecnología digital lo ha cambiado todo. ¿Recuerdas tu primera visita a una web? En aquel entonces, leías las páginas de información como si fuera un libro y los mensajes como si fueran una carta postal. Y gradualmente, te dabas cuenta de que no estabas tratando con un medio de comunicación como los demás. El usuario de una oficina de correos o el cliente de una tienda virtual... se convirtieron gradualmente en actores.... Del libro [Citoyens du Net](https://www.ynternet.org/page/livre).

En 2019, la web (World Wide Web) cumplió 30 años. Con sus cientos de millones de foros, wikis, blogs, redes sociales, microblogs instantáneos, ahora está justificado hablar de la webesfera. Google, Facebook, Twitter y Wikipedia son los "planetas" más famosos. Poblada por cientos de miles de millones de contenidos con diferentes campos (título, cuerpo del mensaje, archivos adjuntos, imágenes, número de visitantes, notas), la web es el corazón en el que convergen todas las herramientas digitales que tienen una interfaz abierta, acceso abierto estandarizado y accesible para todas los recursos existentes: teléfonos inteligentes, tabletas, ordenadores y cosas conectadas. Los lugares más interesantes son aquellos donde se puede interactuar comentando, cambiando, agregando texto o imágenes relacionadas con estos. La comunicación intencional para el empoderamiento cívico y el compromiso de la comunidad con el FLOSS establece una alternativa viable a los medios de comunicación tradicionales y a aquellos recursos que presentan restricciones a la libre circulación de las ideas y el conocimiento.
## Contexto
El objetivo de la sesión es demostrar y motivar al alumnado a explorar:
* Las buenas prácticas de comunicación interpersonal intencional.
* Cómo se relaciona la cultura de software libre con las reglas de las redes sociales y los usos comunes de la información.
* Casos de comunicación intencional en línea como la comunidad de Wikipedia.

El alumnado podrá describir los antecedentes, la historia, las características y los pasos de la comunicación intencional en relación con la cultura web. Podrá articular la importancia de pasar de consumidor a actor de la sociedad de la información.
## Sesiones
### Primera sesión: Comunicación interpersonal intencional
Esta sesión demostrará el uso de tecnologías digitales gratuitas y abiertas para la comunicación intencional en la vida cotidiana. Permitirá a las estudiantes tomar conciencia de sus capacidades y desarrollar sus habilidades de comunicación intencional y utilizar tecnologías digitales abiertas y gratuitas para desarrollar prácticas y hábitos de comunicación intencional.
### Segunda sesión: El estilo de comunicación FLOSS: cómo usan las personas las prácticas y herramientas del FLOSS en sus proyectos
Esta sesión se centrará en un enfoque de pensamiento crítico en torno a la comunicación intencional en diversos entornos sociales y culturales. Explorará métodos de colaboración en línea en actividades de grupos pequeños para aprender unos de otros y apoyar una cultura de comunicación intencional.
### Tercera sesión: El FLOSS en tu vida
Esta sesión permitirá a las participantes desarrollar y compartir sus preferencias de comunicación intencional. Analizaremos la comunicación intencional como base de la participación sinérgica (tomando como ejemplo la toma de notas colaborativas) y la vincularemos a las herramientas del FLOSS que pueden servir a los participantes en las actividades actuales.