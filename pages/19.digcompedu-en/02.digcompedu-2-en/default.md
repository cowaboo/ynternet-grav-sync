---
title: 'How to use the framework to map your competences and upskilling'
objectives:
    - 'Identify one’s own needs for becoming digitally competent in teaching'
    - 'Identify the available open resources and tools and link them to the  DigCompEdu areas'
    - 'Design and plan one’s own educational and professional improvement'
    - 'Promote the use of a common European framework for educators'
length: '360 min'
---

## Introduction
Group discussion on “Where do you feel you would like to improve?”
Trainer will start the introduction to this session by asking to participants what of the six areas they feel less competent and they would like to improve based on the conclusions from the first session.
## Identify your gaps and learn about tools and methodologies
“Teachers need to update their competence profiles for 21st century challenges. Teaching strategies need to change and so do the competences teachers need to develop so as to empower 21st‐century learners… It links teachers and students' digital competence development, and can be linked to institutional capacity building. At the same time, the framework is generic enough to apply to different educational settings and to allow for adaptation as technological possibilities and constraints evolve”.
Presentation and analysis of the benefits of mapping one’s own competences (and the organizations’ ones) to the framework and introduction to some open source tools and technologies identified by the Open-AE partnership.
## Let’s build your own upskilling plan!
How to use the framework to plan your upskilling? With the support of the tutor and after having identified their gaps and interests, participants will be required to build their own plan according to the contexts and target groups they work with. Using a grid provided by the tutor, participants will be able to fill in the plan and will then start to identify open source tools and technologies they would like to use in order to improve their competences. The results of the research from the Open-AE project, namely the existing open source tools available for trainers.
In addition, the study case of [The DOCENT project](https://docent-project.eu/) will be presented and discussed in order to provide to participants a concrete example on how the DigCompEdu framework can be used to shape a framework of digital creative teaching competences.
## Homework
Analyse your organization’s needs (staff and collaborators) and design an upskilling plan.
## References
* [DigCompEdu framework](https://ec.europa.eu/jrc/en/publication/eur-scientific-and-technical-research-reports/european-framework-digital-competence-educators-digcompedu).
* [DigComEdu presentation](https://www.slideshare.net/ChristineRedecker/digcompedu-the-european-framework-for-the-digital-competence-of-educators).
* [DigCompEdu supporting material](https://ec.europa.eu/jrc/en/digcompedu/supporting-materials).
* [DigCompEdu Community](https://ec.europa.eu/jrc/communities/en/community/digcompedu-community).
* Aligning teacher competence frameworks to 21st century challenges: [The case for the European Digital Competence Framework for Educators](https://onlinelibrary.wiley.com/doi/full/10.1111/ejed.12345).
* [The DOCENT project](https://docent-project.eu/).